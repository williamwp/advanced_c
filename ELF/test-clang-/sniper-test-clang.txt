wangpeng@dacent:~/tools/3d/161/b/sniper$ ./run-sniper --  ./test-clang
[SNIPER] Warning: Unable to use physical addresses for shared memory simulation.
[SNIPER] Start
[SNIPER] --------------------------------------------------------------------------------
[SNIPER] Sniper using SIFT/trace-driven frontend
[SNIPER] Running full application in DETAILED mode
[SNIPER] --------------------------------------------------------------------------------
[SNIPER] Enabling performance models
[SNIPER] Setting instrumentation mode to DETAILED
[RECORD-TRACE] Using the Pin frontend (sift/recorder)
[TRACE:0] -- DONE --
[SNIPER] Disabling performance models
[SNIPER] Leaving ROI after 718.73 seconds
[SNIPER] Simulated 262.6M instructions, 207.0M cycles, 1.27 IPC
[SNIPER] Simulation speed 365.4 KIPS (365.4 KIPS / target core - 2736.9ns/instr)
[SNIPER] Setting instrumentation mode to FAST_FORWARD
[SNIPER] End
[SNIPER] Elapsed time: 718.79 seconds
