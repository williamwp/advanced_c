wangpeng@dacent:~/tools/compiler-independent/ELF/test-g++-$ valgrind --tool=cachegrind  ./test-g++ 
==8349== Cachegrind, a cache and branch-prediction profiler
==8349== Copyright (C) 2002-2017, and GNU GPL'd, by Nicholas Nethercote et al.
==8349== Using Valgrind-3.17.0 and LibVEX; rerun with -h for copyright info
==8349== Command: ./test-g++
==8349== 
--8349-- warning: L3 cache found, using its data for the LL simulation.
--8349-- warning: specified LL cache: line_size 64  assoc 20  total_size 31,457,280
--8349-- warning: simulated LL cache: line_size 64  assoc 30  total_size 31,457,280
==8349== 
==8349== I   refs:      264,615,196
==8349== I1  misses:      1,060,511
==8349== LLi misses:         69,737
==8349== I1  miss rate:        0.40%
==8349== LLi miss rate:        0.03%
==8349== 
==8349== D   refs:      114,684,822  (57,810,478 rd   + 56,874,344 wr)
==8349== D1  misses:      2,373,809  ( 1,531,088 rd   +    842,721 wr)
==8349== LLd misses:        348,552  (   144,720 rd   +    203,832 wr)
==8349== D1  miss rate:         2.1% (       2.6%     +        1.5%  )
==8349== LLd miss rate:         0.3% (       0.3%     +        0.4%  )
==8349== 
==8349== LL refs:         3,434,320  ( 2,591,599 rd   +    842,721 wr)
==8349== LL misses:         418,289  (   214,457 rd   +    203,832 wr)
==8349== LL miss rate:          0.1% (       0.1%     +        0.4%  )
wangpeng@dacent:~/tools/compiler-independent/ELF/test-g++-$ 
