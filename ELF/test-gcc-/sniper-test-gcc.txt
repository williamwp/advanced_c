wangpeng@dacent:~/tools/3d/161/b/sniper$ ./run-sniper --  ./test-gcc
[SNIPER] Warning: Unable to use physical addresses for shared memory simulation.
[SNIPER] Start
[SNIPER] --------------------------------------------------------------------------------
[SNIPER] Sniper using SIFT/trace-driven frontend
[SNIPER] Running full application in DETAILED mode
[SNIPER] --------------------------------------------------------------------------------
[SNIPER] Enabling performance models
[SNIPER] Setting instrumentation mode to DETAILED
[RECORD-TRACE] Using the Pin frontend (sift/recorder)
[TRACE:0] -- DONE --
[SNIPER] Disabling performance models
[SNIPER] Leaving ROI after 708.97 seconds
[SNIPER] Simulated 263.0M instructions, 206.9M cycles, 1.27 IPC
[SNIPER] Simulation speed 371.0 KIPS (371.0 KIPS / target core - 2695.6ns/instr)
[SNIPER] Setting instrumentation mode to FAST_FORWARD
[SNIPER] End
[SNIPER] Elapsed time: 709.02 seconds
wangpeng@dacent:~/tools/3d/161/b/sniper$
