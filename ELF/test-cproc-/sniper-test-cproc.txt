wangpeng@dacent:~/tools/3d/161/b/sniper$ ./run-sniper --  ./test-cproc 
[SNIPER] Warning: Unable to use physical addresses for shared memory simulation.
[SNIPER] Start
[SNIPER] --------------------------------------------------------------------------------
[SNIPER] Sniper using SIFT/trace-driven frontend
[SNIPER] Running full application in DETAILED mode
[SNIPER] --------------------------------------------------------------------------------
[SNIPER] Enabling performance models
[SNIPER] Setting instrumentation mode to DETAILED
[RECORD-TRACE] Using the Pin frontend (sift/recorder)
[TRACE:0] -- DONE --
[SNIPER] Disabling performance models
[SNIPER] Leaving ROI after 747.19 seconds
[SNIPER] Simulated 263.1M instructions, 206.8M cycles, 1.27 IPC
[SNIPER] Simulation speed 352.1 KIPS (352.1 KIPS / target core - 2840.4ns/instr)
[SNIPER] Setting instrumentation mode to FAST_FORWARD
[SNIPER] End
[SNIPER] Elapsed time: 747.22 seconds
wangpeng@dacent:~/tools/3d/161/b/sniper$
