# RISC-V RV32X Graphics Extension in LLVM

This is the RISC-V RV32X Graphics Extension in LLVM implemented by William from Chinese Academy of Sciences.

The assembly instructions of RV32X is released and defined in this depository now.


# LLVM 

The goal of this project is to support the instruction set architecture of RV32X Graphics based on llvm. On the basis of riscv standard instruction set architecture, RV32X Graphics adds user-defined instructions, including vertex instruction subset, texture instruction subset, pixel operation instruction subset and graphics pipeline operation instruction subset. These extensions are mainly supported in the back-end project of LLVM.

## (1) The instructions supported by the implemented llvm MC assembler are listed below according to different instruction subset extensions.

                        

| Extended Subset Name      | Subset Functional Description             | Status                    |
| ------- | ---------------- | ----------------------- |
| Vertex      | Graphics primitives handling              | :ballot_box_with_check: |
| Texture    | Materials texture handling     | :ballot_box_with_check: |
| Pixel    | Graphics resolutions handling     | :ballot_box_with_check: |
| Graphics Pipeline    | Graphics pipeline process handling     | :ballot_box_with_check: |


**Texture Instruction subset**

| instruction  | Description                 | Status                    |
| ----- | -------------------- | ----------------------- |
| GATHER_DEPTH | Fetch unfiltered texels instruction   | :ballot_box_with_check: |
| GATHER_GRADIENT_V  | Retrieve lopes relative to vertical instruction           | :ballot_box_with_check: |
| GET_RESOLUTION_X  | Get picture resolution RGB instruction           | :ballot_box_with_check: |
| GET_RESOLUTION_Y  | Get picture resolution RGB instruction           | :ballot_box_with_check: |
| GET_RESOLUTION_Z  | Get picture resolution RGB instruction           | :ballot_box_with_check: |
| GET_RESOLUTION_W  | Get picture resolution transparent instruction           | :ballot_box_with_check: |
| SEMATIC  | Semantic vertex fetch instruction           | :ballot_box_with_check: |


**Vertex Instruction subset**

| instruction    | Description             | Status                    |
| ------- | ---------------- | ----------------------- |
| DVERTEX_CALL    | Vertex shader command to clear all dirty entries instruction         | :ballot_box_with_check: |
| DVERTEX_IALL    | Invalidate all vertex shader for L1 data cache entries instruction         | :ballot_box_with_check: |
| IVERTEX_IALL    | Invalidate all vertex shader for L1 instruction cache entries instruction         | :ballot_box_with_check: | 
| IVERTEX_IALLS    | Broadcast other cores to invalidate  all vertex shader for L1 instruction cache entries instruction         | :ballot_box_with_check: |
| L2VERTEX_CALL     | Vertex shader for L2 cache to clear all dirty entries instruction         | :ballot_box_with_check: | 
| L2VERTEX_CIALL    | Invalidate all L2 entries after writing all dirty entries in L2 cache back to the next level of storage instruction         | :ballot_box_with_check: |
| L2VERTEX_IALL    | Invalidate all vertex shader for L2 data cache entries instruction         | :ballot_box_with_check: | 
| DVERTEX_CIALL    | Invalidate all L1 entries after writing all dirty entries in L1 data cache back to the next level of storage instruction         | :ballot_box_with_check: |
| VERTEX_FETCH    | Fetch the vertex of the primitives instruction         | :ballot_box_with_check: | 
| GET_BUFFER    | Return number of elements in a buffer instruction         | :ballot_box_with_check: |
| GET_LOD    | Get computed level of detail for pixels instruction         | :ballot_box_with_check: | 
| SAMPLE_C    | Sample texture with comparison instruction         | :ballot_box_with_check: |
| DVERTEX_CPAL1    | Write the dcache entry corresponding to the physical address in rs1 back to the next level of storage instruction         | :ballot_box_with_check: | 
| DVERTEX_CVA    | Write the dcache/l2cache entry corresponding to the virtual address in rs1 back to the next level of storage instruction         | :ballot_box_with_check: |
| DVERTEX_CVAL1    | Write the dcache entry corresponding to the virtual address in rs1 back to the next level of storage instruction         | :ballot_box_with_check: | 
| DVERTEX_IPA    | Invalidate the dcache/l2 cache entry corresponding to the physical address in rs1 instruction         | :ballot_box_with_check: |
| DVERTEX_ISW    | Invalid dcache entry specifying SET and WAY instruction         | :ballot_box_with_check: | 
| DVERTEX_IVA    | Invalidate the dcache/l2 cache entry corresponding to the virtual address in rs1 instruction         | :ballot_box_with_check: | 
| IVERTEX_IPA    | Invalidate the icache/l2 cache entry corresponding to the physical address in rs1 instruction         | :ballot_box_with_check: | 
| IVERTEX_IVA    | Invalidate the icache/l2 cache entry corresponding to the virtual address in rs1 instruction         | :ballot_box_with_check: | 


**Pixel Instruction subset**

| instruction | Description                           | Status                    |
| ---- | ------------------------------ | ----------------------- |
| PIXEL_MIX_BUFFER  | Mix graphics pixel of frame buffer instruction | :ballot_box_with_check: |
| PIXEL_H | Get primitives height pixel instruction     | :ballot_box_with_check: |
| PIXEL_W | Get primitives width pixel instruction     | :ballot_box_with_check: |
| PIXEL_POINT | Set primitive point rendering instruction     | :ballot_box_with_check: |
| PIXEL_TRI | Set primitive triangle rendering instruction     | :ballot_box_with_check: |
| PIXEL_DATA | Set primitive data instruction     | :ballot_box_with_check: |
| PIXEL_MIX  | Move primitives between different color instruction     | :ballot_box_with_check: |
| PIXEL_BUFFER | Set test buffer rendering instruction     | :ballot_box_with_check: |
| PIXEL_SAMPLE | Set 2D graphics sample instruction     | :ballot_box_with_check: |
| PIXEL_SRRI | Shift right pixel movement instruction     | :ballot_box_with_check: |
| PIXEL_SRRIW | Low 32-bit shift right pixel movement instruction     | :ballot_box_with_check: |
| PIXEL_OP1 | Set operator in the first channel instruction     | :ballot_box_with_check: |
| PIXEL_OP2 | Set operator in the second channel instruction     | :ballot_box_with_check: |
| PIXEL_SCATTER | Access scatter buffer instruction     | :ballot_box_with_check: |
| PIXEL_SCRATCH  | Access scratch buffer instruction     | :ballot_box_with_check: |


**Graphics Pipeline Instruction subset**

| instruction | Description                           | Status                    |
| ---- | ------------------------------ | ----------------------- |
| GRAPHICS_LRB  | Register shift sign extend byte load instruction | :ballot_box_with_check: |
| GRAPHICS_LRH | Register shift sign bit extended halfword load instruction     | :ballot_box_with_check: |
| GRAPHICS_LRW | Register shift sign bit extension word loading instruction     | :ballot_box_with_check: |
| GRAPHICS_SRB | Register shift byte storage instruction     | :ballot_box_with_check: |
| GRAPHICS_SRH | Register shift halfword storage instruction     | :ballot_box_with_check: |
| GRAPHICS_SRW | Register shift word storage instruction     | :ballot_box_with_check: |
| GRAPHICS_SURB | Register low 32-bit byte storage instruction     | :ballot_box_with_check: |
| GRAPHICS_SURD | Register low 32-bit shifted doubleword storage instruction     | :ballot_box_with_check: |
| GRAPHICS_SURH | Register low 32-bit half word storage instruction     | :ballot_box_with_check: |
| GRAPHICS_SURW | Register low 32-bit word storage instruction     | :ballot_box_with_check: |
| GRAPHICS_LDD | Double registers load instruction     | :ballot_box_with_check: |
| GRAPHICS_LWD | Sign bit extended double register word loading instruction     | :ballot_box_with_check: |
| GRAPHICS_SDD | Double registers store instruction     | :ballot_box_with_check: |
| GRAPHICS_SWD | Double register low 32-bit storage instruction     | :ballot_box_with_check: |


## (2) Some control and status registers of RV32X Graphics are extended for the processor. The following lists the registers that have been defined, including:

Machine Mode Graphics Extended Status Register (GCSR)  
Machine Mode Image Control Status Register (GHCR)  
Machine Mode Image Clear Status Register (GFLUSH)  
Machine Mode Image Set Status Register (GCINS)  
Machine Mode Image Transparency Set Status Register (GTCINS)  
Machine Mode Image Frame Buffer Status Register (GZBUFFER)  




## (3) Compile LLVM RV32X and run tests 

**compile:**

```
$ git clone https://gitlab.com/williamwp/riscv-rv32x-llvm.git
$ cd rv32x-llvm
$ mkdir build
$ cd build
$ cmake -DLLVM_TARGETS_TO_BUILD="RISCV" -G "Unix Makefiles" ..
$ make -j $(nproc)
```

**Execute test cases:**

```
We added 3 test files for RV32X instructions:

$ ./bin/llvm-lit -v ../test/MC/RISCV/rv32x-valid.s
$ ./bin/llvm-lit -v ../test/MC/RISCV/rv32x-invalid.s
$ ./bin/llvm-lit -v ../test/MC/RISCV/machine-csr-names.s

```

The results of the implementation are as follows:

```
-- Testing: 3 tests, 3 workers --
PASS: LLVM :: MC/RISCV/rv32x-valid.s (1 of 3)
PASS: LLVM :: MC/RISCV/rv32x-invalid.s (2 of 3)
PASS: LLVM :: MC/RISCV/machine-csr-names.s (3 of 3)

Testing Time: 0.30s
  Expected Passes    : 3
```
You can also use **FileCheck** to run each test seperately:

	$ llvm-mc %s -triple=riscv32 --mattr=+rv32x -riscv-no-aliases -show-encoding \
	$  | FileCheck -check-prefixes=CHECK-ASM,CHECK-ASM-AND-OBJ %s

## (4) How to Assemble RV32X Assembly Files

You can use **llvm-mc** to assemble RV32X assembly file and generate an object file:

	$ llvm-mc test.s -triple=riscv32 --mattr=+rv32x -riscv-no-aliases -show-encoding --filetype=obj -o test.o  
	
Note: "test.s" here means an assembly file containing riscv rv32x extension instructions, you can use inline assembly in c++ code to add a riscv rv32x instruction and then use clang to compile and generate test.s assembly file. 

    $ clang --target=riscv64-unknown-elf -S  test.c -march=rv64gv -mabi=lp64 --sysroot=/opt/riscv64/riscv64-unknown-elf  --gcc-toolchain=/opt/riscv64  -o test.s `


```
int main(){
  int a,b,c;
  a = 1;
  b = 2;
  asm volatile
  (
    "PIXEL_BUFFER   %[z], %[x], %[y]\n\t"
    : [z] "=r" (c)
    : [x] "r" (a), [y] "r" (b)
  );
  if ( c == 0 ){
     return -1;
  }
  return 0;
  ```

## (5) How to Disassemble RV32X Object code

You can use **llvm-objdump** to disassemble RV32X object code file and generate assembly file:

	$ llvm-objdump test.o --mattr=+rv32x 

























# C++ primary notes  
2021/11/09  


# CCF B
# TACO ACM Transactions on Architecture and Code Optimization ACM http://dblp.uni-trier.de/db/journals/taco/
# ACM Transactions on Architecture and Code Optimization
# JCR Q4  

# Advanced_C

c++ codes

# IEEE TRANSACTIONS ON COMPUTERS  
# JCR 2区
https://www.iikx.com/sci/technology/13009.html  

# 官方网站 ieeexplore.ieee.org/xpl/RecentIssue.jsp?punumber=12  

# CCF B  
# SIGMETRICS
International Conference on Measurement and Modeling of
Computer Systems
ACM http://dblp.uni-trier.de/db/conf/sigmetrics/
https://dblp.uni-trier.de/db/conf/sigmetrics/sigmetrics2019.html#AkramSME19


# CCF A  
TPDS IEEE Transactions on Parallel and Distributed Systems IEEE http://dblp.uni-trier.de/db/journals/tpds/
https://dblp.uni-trier.de/db/journals/tpds/tpds29.html#YuXEBMX18


# CCF A
HPCA High Performance Computer Architecture IEEE http://dblp.uni-trier.de/db/conf/hpca/  
https://dblp.uni-trier.de/db/conf/hpca/hpca2018.html#JahreE18  

# CCF B  
# JPDC Journal of Parallel and Distributed Computing Elsevier http://dblp.uni-trier.de/db/journals/jpdc/
https://dblp.uni-trier.de/db/journals/jpdc/jpdc100.html#JhaHFTGE17  


# CCF B  
# PACT
International Conference on Parallel Architectures and
Compilation Techniques
IEEE/ACM http://dblp.uni-trier.de/db/conf/IEEEpact/
https://dblp.uni-trier.de/db/conf/IEEEpact/pact2017.html#LiuZYWWLE17  


# CCF B   
ICS International Conference on Supercomputing ACM http://dblp.uni-trier.de/db/conf/ics/  


# CCF B
IPDPS International Parallel & Distributed Processing Symposium IEEE http://dblp.uni-trier.de/db/conf/ipps/


# CCF B
ICCD International Conference on Computer Design IEEE http://dblp.uni-trier.de/db/conf/iccd/  

# CCF A  
DAC Design Automation Conference ACM https://dblp.uni-trier.de/db/conf/dac/
  



  
