// Note
// "g++ deque.cpp" is ok
// but "gcc deque.cpp" is not

#include<iostream>
#include<deque>
#include<algorithm>

using namespace std;

int main()
{
    deque<int> a;  //declare object a

    cout << "deque " << endl;

    a.push_back(3);
    a.push_back(4);
    a.push_back(5);  // add the elements at the end


    a.push_front(2);  // add the element at the front side

    for(size_t nCount; nCount < a.size(); ++nCount)
    {
        cout << "size_t nCount method" << endl;
        cout << "a [ " << nCount << " ] = ";
	cout << a[nCount] << endl;
    
    }

    deque<int>::iterator iElementLocater;

    for(iElementLocater = a.begin(); iElementLocater != a.end(); ++iElementLocater)
    {
        cout << *iElementLocater << endl;
    
    }

    cout << endl << endl;

    // get the distance(begin(), iterator) function from #include<algorithm>
    for(iElementLocater = a.begin(); iElementLocater != a.end(); ++iElementLocater)
    {
        size_t nOffset = distance(a.begin(), iElementLocater);
	cout << "a[ " << nOffset << " ] = ";
	cout << *iElementLocater << endl;
    }

    cout << endl << endl;

    cout << "size_t nCount method" << endl;
    // There is no value if not declared the initial value of nCount
    for(size_t nCount = 0; nCount < a.size(); ++nCount)
    {
         cout << "a [ " << nCount << " ] = ";
         cout << a[nCount] << endl;
    }



    return 0;






}
