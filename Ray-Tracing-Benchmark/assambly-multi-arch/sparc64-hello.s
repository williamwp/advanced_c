	.file	"hello.cpp"
	.section	".text"
	.section	".rodata"
	.type	_ZStL19piecewise_construct, #object
	.size	_ZStL19piecewise_construct, 1
_ZStL19piecewise_construct:
	.skip	1
	.local	_ZStL8__ioinit
	.common	_ZStL8__ioinit,1,1
	.align 8
.LC0:
	.asciz	"hello \n "
	.section	".text"
	.align 4
	.global main
	.type	main, #function
	.proc	04
main:
.LFB1521:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	sethi	%hi(.LC0), %g1
	or	%g1, %lo(.LC0), %o0
	call	printf, 0
	 nop
	mov	0, %g1
	sra	%g1, 0, %g1
	mov	%g1, %i0
	return	%i7+8
	 nop
	.cfi_endproc
.LFE1521:
	.size	main, .-main
	.align 4
	.type	_Z41__static_initialization_and_destruction_0ii, #function
	.proc	020
_Z41__static_initialization_and_destruction_0ii:
.LFB2002:
	.cfi_startproc
	.register	%g2, #scratch
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	mov	%i0, %g1
	mov	%i1, %g2
	st	%g1, [%fp+2175]
	mov	%g2, %g1
	st	%g1, [%fp+2183]
	ld	[%fp+2175], %g1
	cmp	%g1, 1
	bne	%icc, .L5
	 nop
	ld	[%fp+2183], %g2
	sethi	%hi(64512), %g1
	or	%g1, 1023, %g1
	cmp	%g2, %g1
	bne	%icc, .L5
	 nop
	sethi	%hi(_ZStL8__ioinit), %g1
	or	%g1, %lo(_ZStL8__ioinit), %o0
	call	_ZNSt8ios_base4InitC1Ev, 0
	 nop
	sethi	%hi(__dso_handle), %g1
	or	%g1, %lo(__dso_handle), %o2
	sethi	%hi(_ZStL8__ioinit), %g1
	or	%g1, %lo(_ZStL8__ioinit), %o1
	sethi	%hi(_ZNSt8ios_base4InitD1Ev), %g1
	or	%g1, %lo(_ZNSt8ios_base4InitD1Ev), %o0
	call	__cxa_atexit, 0
	 nop
.L5:
	nop
	return	%i7+8
	 nop
	.cfi_endproc
.LFE2002:
	.size	_Z41__static_initialization_and_destruction_0ii, .-_Z41__static_initialization_and_destruction_0ii
	.align 4
	.type	_GLOBAL__sub_I_main, #function
	.proc	020
_GLOBAL__sub_I_main:
.LFB2003:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	mov	-1, %g1
	srlx	%g1, 48, %o1
	mov	1, %o0
	call	_Z41__static_initialization_and_destruction_0ii, 0
	 nop
	return	%i7+8
	 nop
	.cfi_endproc
.LFE2003:
	.size	_GLOBAL__sub_I_main, .-_GLOBAL__sub_I_main
	.section	.ctors,"aw",@progbits
	.align 8
	.xword	_GLOBAL__sub_I_main
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.4.0-1ubuntu1~20.04) 9.4.0"
	.section	.note.GNU-stack,"",@progbits
