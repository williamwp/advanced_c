	.LEVEL 1.1
	.text
	.section	.rodata
	.type	_ZStL19piecewise_construct, @object
	.size	_ZStL19piecewise_construct, 1
_ZStL19piecewise_construct:
	.zero	1
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.align 4
.LC0:
	.stringz	"hello \n "
	.text
	.align 4
.globl main
	.type	main, @function
main:
	.PROC
	.CALLINFO FRAME=64,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB1518:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldil LR'.LC0,%r28
	ldo RR'.LC0(%r28),%r26
	bl printf,%r2
	nop
	ldi 0,%r28
	ldw -20(%r3),%r2
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE1518:
	.size	main, .-main
	.section	.rodata
	.align 4
.LC1:
	.word	P%_ZNSt8ios_base4InitD1Ev
	.text
	.align 4
	.type	_Z41__static_initialization_and_destruction_0ii, @function
_Z41__static_initialization_and_destruction_0ii:
	.PROC
	.CALLINFO FRAME=64,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB1999:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	stw %r25,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	comib,<>,n 1,%r28,.L5
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	ldw 0(%r28),%r19
	zdepi -1,31,16,%r28
	comb,<>,n %r28,%r19,.L5
	addil LR'_ZStL8__ioinit-$global$,%r27
	copy %r1,%r28
	ldo RR'_ZStL8__ioinit-$global$(%r28),%r26
	bl _ZNSt8ios_base4InitC1Ev,%r2
	nop
	ldil LR'.LC1,%r28
	ldo RR'.LC1(%r28),%r28
	ldw 0(%r28),%r19
	addil LR'__dso_handle-$global$,%r27
	copy %r1,%r28
	ldo RR'__dso_handle-$global$(%r28),%r24
	addil LR'_ZStL8__ioinit-$global$,%r27
	copy %r1,%r28
	ldo RR'_ZStL8__ioinit-$global$(%r28),%r25
	copy %r19,%r26
	bl __cxa_atexit,%r2
	nop
.L5:
	nop
	ldw -20(%r3),%r2
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE1999:
	.size	_Z41__static_initialization_and_destruction_0ii, .-_Z41__static_initialization_and_destruction_0ii
	.align 4
	.type	_GLOBAL__sub_I_main, @function
_GLOBAL__sub_I_main:
	.PROC
	.CALLINFO FRAME=64,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB2000:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	zdepi -1,31,16,%r25
	ldi 1,%r26
	bl _Z41__static_initialization_and_destruction_0ii,%r2
	nop
	ldw -20(%r3),%r2
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE2000:
	.size	_GLOBAL__sub_I_main, .-_GLOBAL__sub_I_main
	.section	.ctors,"aw",@progbits
	.align 4
	.word	P%_GLOBAL__sub_I_main
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.4.0-1ubuntu1~20.04) 9.4.0"
