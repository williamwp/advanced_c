	.file	1 "7-metal-reflection.cpp"
	.section .mdebug.abi32
	.previous
	.nan	legacy
	.module	fp=xx
	.module	nooddspreg
	.abicalls
	.text
	.rdata
	.align	2
	.type	_ZStL19piecewise_construct, @object
	.size	_ZStL19piecewise_construct, 1
_ZStL19piecewise_construct:
	.space	1
	.section	.text._ZNKSt9type_infoeqERKS_,"axG",@progbits,_ZNKSt9type_infoeqERKS_,comdat
	.align	2
	.weak	_ZNKSt9type_infoeqERKS_
$LFB727 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZNKSt9type_infoeqERKS_
	.type	_ZNKSt9type_infoeqERKS_, @function
_ZNKSt9type_infoeqERKS_:
	.frame	$fp,32,$31		# vars= 0, regs= 2/0, args= 16, gp= 8
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-32
	.cfi_def_cfa_offset 32
	sw	$31,28($sp)
	sw	$fp,24($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	move	$fp,$sp
	.cfi_def_cfa_register 30
	lui	$28,%hi(__gnu_local_gp)
	addiu	$28,$28,%lo(__gnu_local_gp)
	.cprestore	16
	sw	$4,32($fp)
	sw	$5,36($fp)
	lw	$2,32($fp)
	lw	$3,4($2)
	lw	$2,36($fp)
	lw	$2,4($2)
	beq	$3,$2,$L2
	nop

	lw	$2,32($fp)
	lw	$2,4($2)
	lb	$3,0($2)
	li	$2,42			# 0x2a
	beq	$3,$2,$L3
	nop

	lw	$2,32($fp)
	lw	$3,4($2)
	lw	$2,36($fp)
	lw	$2,4($2)
	move	$5,$2
	move	$4,$3
	lw	$2,%call16(strcmp)($28)
	move	$25,$2
	.reloc	1f,R_MIPS_JALR,strcmp
1:	jalr	$25
	nop

	lw	$28,16($fp)
	bne	$2,$0,$L3
	nop

$L2:
	li	$2,1			# 0x1
	.option	pic0
	b	$L4
	nop

	.option	pic2
$L3:
	move	$2,$0
$L4:
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,28($sp)
	lw	$fp,24($sp)
	addiu	$sp,$sp,32
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZNKSt9type_infoeqERKS_
	.cfi_endproc
$LFE727:
	.size	_ZNKSt9type_infoeqERKS_, .-_ZNKSt9type_infoeqERKS_
	.section	.text._ZnwjPv,"axG",@progbits,_ZnwjPv,comdat
	.align	2
	.weak	_ZnwjPv
$LFB769 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZnwjPv
	.type	_ZnwjPv, @function
_ZnwjPv:
	.frame	$fp,8,$31		# vars= 0, regs= 1/0, args= 0, gp= 0
	.mask	0x40000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-8
	.cfi_def_cfa_offset 8
	sw	$fp,4($sp)
	.cfi_offset 30, -4
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,8($fp)
	sw	$5,12($fp)
	lw	$2,12($fp)
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$fp,4($sp)
	addiu	$sp,$sp,8
	.cfi_restore 30
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZnwjPv
	.cfi_endproc
$LFE769:
	.size	_ZnwjPv, .-_ZnwjPv
	.section	.text._ZdlPvS_,"axG",@progbits,_ZdlPvS_,comdat
	.align	2
	.weak	_ZdlPvS_
$LFB771 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZdlPvS_
	.type	_ZdlPvS_, @function
_ZdlPvS_:
	.frame	$fp,8,$31		# vars= 0, regs= 1/0, args= 0, gp= 0
	.mask	0x40000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-8
	.cfi_def_cfa_offset 8
	sw	$fp,4($sp)
	.cfi_offset 30, -4
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,8($fp)
	sw	$5,12($fp)
	nop
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$fp,4($sp)
	addiu	$sp,$sp,8
	.cfi_restore 30
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZdlPvS_
	.cfi_endproc
$LFE771:
	.size	_ZdlPvS_, .-_ZdlPvS_
	.section	.data.rel.ro,"aw"
	.align	2
	.type	_ZZL18__gthread_active_pvE20__gthread_active_ptr, @object
	.size	_ZZL18__gthread_active_pvE20__gthread_active_ptr, 4
_ZZL18__gthread_active_pvE20__gthread_active_ptr:
	.word	_ZL28__gthrw___pthread_key_createPjPFvPvE
	.section	.text._ZL18__gthread_active_pv,"axG",@progbits,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv,comdat
	.align	2
$LFB945 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZL18__gthread_active_pv
	.type	_ZL18__gthread_active_pv, @function
_ZL18__gthread_active_pv:
	.frame	$fp,8,$31		# vars= 0, regs= 1/0, args= 0, gp= 0
	.mask	0x40000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-8
	.cfi_def_cfa_offset 8
	sw	$fp,4($sp)
	.cfi_offset 30, -4
	move	$fp,$sp
	.cfi_def_cfa_register 30
	lui	$28,%hi(__gnu_local_gp)
	addiu	$28,$28,%lo(__gnu_local_gp)
	li	$3,1			# 0x1
	lw	$2,%got(_ZL28__gthrw___pthread_key_createPjPFvPvE)($28)
	bne	$2,$0,$L10
	nop

	move	$3,$0
$L10:
	andi	$2,$3,0x00ff
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$fp,4($sp)
	addiu	$sp,$sp,8
	.cfi_restore 30
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZL18__gthread_active_pv
	.cfi_endproc
$LFE945:
	.size	_ZL18__gthread_active_pv, .-_ZL18__gthread_active_pv
	.section	.text._ZN9__gnu_cxxL18__exchange_and_addEPVii,"axG",@progbits,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv,comdat
	.align	2
$LFB974 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZN9__gnu_cxxL18__exchange_and_addEPVii
	.type	_ZN9__gnu_cxxL18__exchange_and_addEPVii, @function
_ZN9__gnu_cxxL18__exchange_and_addEPVii:
	.frame	$fp,8,$31		# vars= 0, regs= 1/0, args= 0, gp= 0
	.mask	0x40000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-8
	.cfi_def_cfa_offset 8
	sw	$fp,4($sp)
	.cfi_offset 30, -4
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,8($fp)
	sw	$5,12($fp)
	lw	$3,12($fp)
	lw	$2,8($fp)
	.set	noat
	sync
1:
	ll	$4,0($2)
	addu	$1,$4,$3
	sc	$1,0($2)
	beq	$1,$0,1b
	nop
	sync
	.set	at
	move	$2,$4
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$fp,4($sp)
	addiu	$sp,$sp,8
	.cfi_restore 30
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZN9__gnu_cxxL18__exchange_and_addEPVii
	.cfi_endproc
$LFE974:
	.size	_ZN9__gnu_cxxL18__exchange_and_addEPVii, .-_ZN9__gnu_cxxL18__exchange_and_addEPVii
	.section	.text._ZN9__gnu_cxxL25__exchange_and_add_singleEPii,"axG",@progbits,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv,comdat
	.align	2
$LFB976 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZN9__gnu_cxxL25__exchange_and_add_singleEPii
	.type	_ZN9__gnu_cxxL25__exchange_and_add_singleEPii, @function
_ZN9__gnu_cxxL25__exchange_and_add_singleEPii:
	.frame	$fp,16,$31		# vars= 8, regs= 1/0, args= 0, gp= 0
	.mask	0x40000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-16
	.cfi_def_cfa_offset 16
	sw	$fp,12($sp)
	.cfi_offset 30, -4
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,16($fp)
	sw	$5,20($fp)
	lw	$2,16($fp)
	lw	$2,0($2)
	sw	$2,4($fp)
	lw	$2,16($fp)
	lw	$3,0($2)
	lw	$2,20($fp)
	addu	$3,$3,$2
	lw	$2,16($fp)
	sw	$3,0($2)
	lw	$2,4($fp)
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$fp,12($sp)
	addiu	$sp,$sp,16
	.cfi_restore 30
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZN9__gnu_cxxL25__exchange_and_add_singleEPii
	.cfi_endproc
$LFE976:
	.size	_ZN9__gnu_cxxL25__exchange_and_add_singleEPii, .-_ZN9__gnu_cxxL25__exchange_and_add_singleEPii
	.section	.text._ZN9__gnu_cxxL27__exchange_and_add_dispatchEPii,"axG",@progbits,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv,comdat
	.align	2
$LFB978 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZN9__gnu_cxxL27__exchange_and_add_dispatchEPii
	.type	_ZN9__gnu_cxxL27__exchange_and_add_dispatchEPii, @function
_ZN9__gnu_cxxL27__exchange_and_add_dispatchEPii:
	.frame	$fp,32,$31		# vars= 0, regs= 2/0, args= 16, gp= 8
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-32
	.cfi_def_cfa_offset 32
	sw	$31,28($sp)
	sw	$fp,24($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,32($fp)
	sw	$5,36($fp)
	.option	pic0
	jal	_ZL18__gthread_active_pv
	nop

	.option	pic2
	sltu	$2,$0,$2
	andi	$2,$2,0x00ff
	beq	$2,$0,$L17
	nop

	lw	$5,36($fp)
	lw	$4,32($fp)
	.option	pic0
	jal	_ZN9__gnu_cxxL18__exchange_and_addEPVii
	nop

	.option	pic2
	.option	pic0
	b	$L18
	nop

	.option	pic2
$L17:
	lw	$5,36($fp)
	lw	$4,32($fp)
	.option	pic0
	jal	_ZN9__gnu_cxxL25__exchange_and_add_singleEPii
	nop

	.option	pic2
	nop
$L18:
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,28($sp)
	lw	$fp,24($sp)
	addiu	$sp,$sp,32
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZN9__gnu_cxxL27__exchange_and_add_dispatchEPii
	.cfi_endproc
$LFE978:
	.size	_ZN9__gnu_cxxL27__exchange_and_add_dispatchEPii, .-_ZN9__gnu_cxxL27__exchange_and_add_dispatchEPii
	.rdata
	.align	2
	.type	_ZN9__gnu_cxxL21__default_lock_policyE, @object
	.size	_ZN9__gnu_cxxL21__default_lock_policyE, 4
_ZN9__gnu_cxxL21__default_lock_policyE:
	.word	2
	.align	2
	.type	_ZStL13allocator_arg, @object
	.size	_ZStL13allocator_arg, 1
_ZStL13allocator_arg:
	.space	1
	.align	2
	.type	_ZStL6ignore, @object
	.size	_ZStL6ignore, 1
_ZStL6ignore:
	.space	1
	.weak	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag
	.section	.rodata._ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag,"aG",@progbits,_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag,comdat
	.align	2
	.type	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag, @object
	.size	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag, 8
_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag:
	.space	8
	.section	.text._ZNSt19_Sp_make_shared_tag5_S_tiEv,"axG",@progbits,_ZNSt19_Sp_make_shared_tag5_S_tiEv,comdat
	.align	2
	.weak	_ZNSt19_Sp_make_shared_tag5_S_tiEv
$LFB1942 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZNSt19_Sp_make_shared_tag5_S_tiEv
	.type	_ZNSt19_Sp_make_shared_tag5_S_tiEv, @function
_ZNSt19_Sp_make_shared_tag5_S_tiEv:
	.frame	$fp,8,$31		# vars= 0, regs= 1/0, args= 0, gp= 0
	.mask	0x40000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-8
	.cfi_def_cfa_offset 8
	sw	$fp,4($sp)
	.cfi_offset 30, -4
	move	$fp,$sp
	.cfi_def_cfa_register 30
	lui	$2,%hi(_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag)
	addiu	$2,$2,%lo(_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag)
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$fp,4($sp)
	addiu	$sp,$sp,8
	.cfi_restore 30
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZNSt19_Sp_make_shared_tag5_S_tiEv
	.cfi_endproc
$LFE1942:
	.size	_ZNSt19_Sp_make_shared_tag5_S_tiEv, .-_ZNSt19_Sp_make_shared_tag5_S_tiEv
	.rdata
	.align	3
	.type	_ZL2pi, @object
	.size	_ZL2pi, 8
_ZL2pi:
	.word	1074340347
	.word	1413754136
	.weak	_ZZ13random_doublevE12distribution
	.section	.bss._ZZ13random_doublevE12distribution,"awG",@nobits,_ZZ13random_doublevE12distribution,comdat
	.align	3
	.type	_ZZ13random_doublevE12distribution, @object
	.size	_ZZ13random_doublevE12distribution, 16
_ZZ13random_doublevE12distribution:
	.space	16
	.weak	_ZGVZ13random_doublevE12distribution
	.section	.bss._ZGVZ13random_doublevE12distribution,"awG",@nobits,_ZGVZ13random_doublevE12distribution,comdat
	.align	3
	.type	_ZGVZ13random_doublevE12distribution, @object
	.size	_ZGVZ13random_doublevE12distribution, 8
_ZGVZ13random_doublevE12distribution:
	.space	8
	.weak	_ZZ13random_doublevE9generator
	.section	.bss._ZZ13random_doublevE9generator,"awG",@nobits,_ZZ13random_doublevE9generator,comdat
	.align	2
	.type	_ZZ13random_doublevE9generator, @object
	.size	_ZZ13random_doublevE9generator, 2500
_ZZ13random_doublevE9generator:
	.space	2500
	.weak	_ZGVZ13random_doublevE9generator
	.section	.bss._ZGVZ13random_doublevE9generator,"awG",@nobits,_ZGVZ13random_doublevE9generator,comdat
	.align	3
	.type	_ZGVZ13random_doublevE9generator, @object
	.size	_ZGVZ13random_doublevE9generator, 8
_ZGVZ13random_doublevE9generator:
	.space	8
	.section	.text._Z13random_doublev,"axG",@progbits,_Z13random_doublev,comdat
	.align	2
	.weak	_Z13random_doublev
$LFB3288 = .
	.cfi_startproc
	.cfi_personality 0x80,DW.ref.__gxx_personality_v0
	.cfi_lsda 0,$LLSDA3288
	.set	nomips16
	.set	nomicromips
	.ent	_Z13random_doublev
	.type	_Z13random_doublev, @function
_Z13random_doublev:
	.frame	$fp,48,$31		# vars= 0, regs= 4/0, args= 24, gp= 8
	.mask	0xc0030000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-48
	.cfi_def_cfa_offset 48
	sw	$31,44($sp)
	sw	$fp,40($sp)
	sw	$17,36($sp)
	sw	$16,32($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	.cfi_offset 17, -12
	.cfi_offset 16, -16
	move	$fp,$sp
	.cfi_def_cfa_register 30
	lui	$28,%hi(__gnu_local_gp)
	addiu	$28,$28,%lo(__gnu_local_gp)
	.cprestore	24
	lui	$2,%hi(_ZGVZ13random_doublevE12distribution)
	lbu	$2,%lo(_ZGVZ13random_doublevE12distribution)($2)
	sync
	andi	$2,$2,0x00ff
	sltu	$2,$2,1
	andi	$2,$2,0x00ff
	beq	$2,$0,$L22
	nop

	lui	$2,%hi(_ZGVZ13random_doublevE12distribution)
	addiu	$4,$2,%lo(_ZGVZ13random_doublevE12distribution)
	lw	$2,%call16(__cxa_guard_acquire)($28)
	move	$25,$2
	.reloc	1f,R_MIPS_JALR,__cxa_guard_acquire
1:	jalr	$25
	nop

	lw	$28,24($fp)
	sltu	$2,$0,$2
	andi	$2,$2,0x00ff
	beq	$2,$0,$L22
	nop

	move	$16,$0
	lui	$2,%hi($LC0)
	ldc1	$f0,%lo($LC0)($2)
	sdc1	$f0,16($sp)
	move	$7,$0
	move	$6,$0
	lui	$2,%hi(_ZZ13random_doublevE12distribution)
	addiu	$4,$2,%lo(_ZZ13random_doublevE12distribution)
$LEHB0 = .
	.option	pic0
	jal	_ZNSt25uniform_real_distributionIdEC1Edd
	nop

	.option	pic2
$LEHE0 = .
	lw	$28,24($fp)
	lui	$2,%hi(_ZGVZ13random_doublevE12distribution)
	addiu	$4,$2,%lo(_ZGVZ13random_doublevE12distribution)
	lw	$2,%call16(__cxa_guard_release)($28)
	move	$25,$2
	.reloc	1f,R_MIPS_JALR,__cxa_guard_release
1:	jalr	$25
	nop

	lw	$28,24($fp)
$L22:
	lui	$2,%hi(_ZGVZ13random_doublevE9generator)
	lbu	$2,%lo(_ZGVZ13random_doublevE9generator)($2)
	sync
	andi	$2,$2,0x00ff
	sltu	$2,$2,1
	andi	$2,$2,0x00ff
	beq	$2,$0,$L23
	nop

	lui	$2,%hi(_ZGVZ13random_doublevE9generator)
	addiu	$4,$2,%lo(_ZGVZ13random_doublevE9generator)
	lw	$2,%call16(__cxa_guard_acquire)($28)
	move	$25,$2
	.reloc	1f,R_MIPS_JALR,__cxa_guard_acquire
1:	jalr	$25
	nop

	lw	$28,24($fp)
	sltu	$2,$0,$2
	andi	$2,$2,0x00ff
	beq	$2,$0,$L23
	nop

	move	$16,$0
	lui	$2,%hi(_ZZ13random_doublevE9generator)
	addiu	$4,$2,%lo(_ZZ13random_doublevE9generator)
$LEHB1 = .
	.option	pic0
	jal	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC1Ev
	nop

	.option	pic2
$LEHE1 = .
	lw	$28,24($fp)
	lui	$2,%hi(_ZGVZ13random_doublevE9generator)
	addiu	$4,$2,%lo(_ZGVZ13random_doublevE9generator)
	lw	$2,%call16(__cxa_guard_release)($28)
	move	$25,$2
	.reloc	1f,R_MIPS_JALR,__cxa_guard_release
1:	jalr	$25
	nop

	lw	$28,24($fp)
$L23:
	lui	$2,%hi(_ZZ13random_doublevE9generator)
	addiu	$5,$2,%lo(_ZZ13random_doublevE9generator)
	lui	$2,%hi(_ZZ13random_doublevE12distribution)
	addiu	$4,$2,%lo(_ZZ13random_doublevE12distribution)
$LEHB2 = .
	.option	pic0
	jal	_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEEEdRT_
	nop

	.option	pic2
	lw	$28,24($fp)
	.option	pic0
	b	$L31
	nop

	.option	pic2
$L29:
	lw	$28,24($fp)
	move	$17,$4
	bne	$16,$0,$L26
	nop

	lui	$2,%hi(_ZGVZ13random_doublevE12distribution)
	addiu	$4,$2,%lo(_ZGVZ13random_doublevE12distribution)
	lw	$2,%call16(__cxa_guard_abort)($28)
	move	$25,$2
	.reloc	1f,R_MIPS_JALR,__cxa_guard_abort
1:	jalr	$25
	nop

	lw	$28,24($fp)
$L26:
	move	$2,$17
	move	$4,$2
	lw	$2,%call16(_Unwind_Resume)($28)
	move	$25,$2
	.reloc	1f,R_MIPS_JALR,_Unwind_Resume
1:	jalr	$25
	nop

$L30:
	lw	$28,24($fp)
	move	$17,$4
	bne	$16,$0,$L28
	nop

	lui	$2,%hi(_ZGVZ13random_doublevE9generator)
	addiu	$4,$2,%lo(_ZGVZ13random_doublevE9generator)
	lw	$2,%call16(__cxa_guard_abort)($28)
	move	$25,$2
	.reloc	1f,R_MIPS_JALR,__cxa_guard_abort
1:	jalr	$25
	nop

	lw	$28,24($fp)
$L28:
	move	$2,$17
	move	$4,$2
	lw	$2,%call16(_Unwind_Resume)($28)
	move	$25,$2
	.reloc	1f,R_MIPS_JALR,_Unwind_Resume
1:	jalr	$25
	nop

$LEHE2 = .
$L31:
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,44($sp)
	lw	$fp,40($sp)
	lw	$17,36($sp)
	lw	$16,32($sp)
	addiu	$sp,$sp,48
	.cfi_restore 16
	.cfi_restore 17
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_Z13random_doublev
	.cfi_endproc
$LFE3288:
	.globl	__gxx_personality_v0
	.section	.gcc_except_table._Z13random_doublev,"awG",@progbits,_Z13random_doublev,comdat
$LLSDA3288:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 $LLSDACSE3288-$LLSDACSB3288
$LLSDACSB3288:
	.uleb128 $LEHB0-$LFB3288
	.uleb128 $LEHE0-$LEHB0
	.uleb128 $L29-$LFB3288
	.uleb128 0
	.uleb128 $LEHB1-$LFB3288
	.uleb128 $LEHE1-$LEHB1
	.uleb128 $L30-$LFB3288
	.uleb128 0
	.uleb128 $LEHB2-$LFB3288
	.uleb128 $LEHE2-$LEHB2
	.uleb128 0
	.uleb128 0
$LLSDACSE3288:
	.section	.text._Z13random_doublev,"axG",@progbits,_Z13random_doublev,comdat
	.size	_Z13random_doublev, .-_Z13random_doublev
	.section	.text._ZplRK4vec3S1_,"axG",@progbits,_ZplRK4vec3S1_,comdat
	.align	2
	.weak	_ZplRK4vec3S1_
$LFB3924 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZplRK4vec3S1_
	.type	_ZplRK4vec3S1_, @function
_ZplRK4vec3S1_:
	.frame	$fp,48,$31		# vars= 0, regs= 2/0, args= 32, gp= 8
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-48
	.cfi_def_cfa_offset 48
	sw	$31,44($sp)
	sw	$fp,40($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	move	$fp,$sp
	.cfi_def_cfa_register 30
	lui	$28,%hi(__gnu_local_gp)
	addiu	$28,$28,%lo(__gnu_local_gp)
	.cprestore	32
	sw	$4,48($fp)
	sw	$5,52($fp)
	sw	$6,56($fp)
	lw	$2,52($fp)
	ldc1	$f2,0($2)
	lw	$2,56($fp)
	ldc1	$f0,0($2)
	add.d	$f6,$f2,$f0
	lw	$2,52($fp)
	ldc1	$f2,8($2)
	lw	$2,56($fp)
	ldc1	$f0,8($2)
	add.d	$f0,$f2,$f0
	lw	$2,52($fp)
	ldc1	$f4,16($2)
	lw	$2,56($fp)
	ldc1	$f2,16($2)
	add.d	$f2,$f4,$f2
	sdc1	$f2,24($sp)
	sdc1	$f0,16($sp)
	mfc1	$7,$f6
	mfhc1	$6,$f6
	lw	$4,48($fp)
	lw	$2,%call16(_ZN4vec3C1Eddd)($28)
	move	$25,$2
	.reloc	1f,R_MIPS_JALR,_ZN4vec3C1Eddd
1:	jalr	$25
	nop

	lw	$28,32($fp)
	lw	$2,48($fp)
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,44($sp)
	lw	$fp,40($sp)
	addiu	$sp,$sp,48
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZplRK4vec3S1_
	.cfi_endproc
$LFE3924:
	.size	_ZplRK4vec3S1_, .-_ZplRK4vec3S1_
	.section	.text._ZmiRK4vec3S1_,"axG",@progbits,_ZmiRK4vec3S1_,comdat
	.align	2
	.weak	_ZmiRK4vec3S1_
$LFB3925 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZmiRK4vec3S1_
	.type	_ZmiRK4vec3S1_, @function
_ZmiRK4vec3S1_:
	.frame	$fp,48,$31		# vars= 0, regs= 2/0, args= 32, gp= 8
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-48
	.cfi_def_cfa_offset 48
	sw	$31,44($sp)
	sw	$fp,40($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	move	$fp,$sp
	.cfi_def_cfa_register 30
	lui	$28,%hi(__gnu_local_gp)
	addiu	$28,$28,%lo(__gnu_local_gp)
	.cprestore	32
	sw	$4,48($fp)
	sw	$5,52($fp)
	sw	$6,56($fp)
	lw	$2,52($fp)
	ldc1	$f2,0($2)
	lw	$2,56($fp)
	ldc1	$f0,0($2)
	sub.d	$f6,$f2,$f0
	lw	$2,52($fp)
	ldc1	$f2,8($2)
	lw	$2,56($fp)
	ldc1	$f0,8($2)
	sub.d	$f0,$f2,$f0
	lw	$2,52($fp)
	ldc1	$f4,16($2)
	lw	$2,56($fp)
	ldc1	$f2,16($2)
	sub.d	$f2,$f4,$f2
	sdc1	$f2,24($sp)
	sdc1	$f0,16($sp)
	mfc1	$7,$f6
	mfhc1	$6,$f6
	lw	$4,48($fp)
	lw	$2,%call16(_ZN4vec3C1Eddd)($28)
	move	$25,$2
	.reloc	1f,R_MIPS_JALR,_ZN4vec3C1Eddd
1:	jalr	$25
	nop

	lw	$28,32($fp)
	lw	$2,48($fp)
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,44($sp)
	lw	$fp,40($sp)
	addiu	$sp,$sp,48
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZmiRK4vec3S1_
	.cfi_endproc
$LFE3925:
	.size	_ZmiRK4vec3S1_, .-_ZmiRK4vec3S1_
	.section	.text._ZmldRK4vec3,"axG",@progbits,_ZmldRK4vec3,comdat
	.align	2
	.weak	_ZmldRK4vec3
$LFB3927 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZmldRK4vec3
	.type	_ZmldRK4vec3, @function
_ZmldRK4vec3:
	.frame	$fp,48,$31		# vars= 0, regs= 2/0, args= 32, gp= 8
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-48
	.cfi_def_cfa_offset 48
	sw	$31,44($sp)
	sw	$fp,40($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	move	$fp,$sp
	.cfi_def_cfa_register 30
	lui	$28,%hi(__gnu_local_gp)
	addiu	$28,$28,%lo(__gnu_local_gp)
	.cprestore	32
	sw	$4,48($fp)
	sw	$7,60($fp)
	sw	$6,56($fp)
	lw	$2,64($fp)
	ldc1	$f2,0($2)
	ldc1	$f0,56($fp)
	mul.d	$f6,$f2,$f0
	lw	$2,64($fp)
	ldc1	$f2,8($2)
	ldc1	$f0,56($fp)
	mul.d	$f0,$f2,$f0
	lw	$2,64($fp)
	ldc1	$f4,16($2)
	ldc1	$f2,56($fp)
	mul.d	$f2,$f4,$f2
	sdc1	$f2,24($sp)
	sdc1	$f0,16($sp)
	mfc1	$7,$f6
	mfhc1	$6,$f6
	lw	$4,48($fp)
	lw	$2,%call16(_ZN4vec3C1Eddd)($28)
	move	$25,$2
	.reloc	1f,R_MIPS_JALR,_ZN4vec3C1Eddd
1:	jalr	$25
	nop

	lw	$28,32($fp)
	lw	$2,48($fp)
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,44($sp)
	lw	$fp,40($sp)
	addiu	$sp,$sp,48
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZmldRK4vec3
	.cfi_endproc
$LFE3927:
	.size	_ZmldRK4vec3, .-_ZmldRK4vec3
	.section	.text._Zdv4vec3d,"axG",@progbits,_Zdv4vec3d,comdat
	.align	2
	.weak	_Zdv4vec3d
$LFB3929 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_Zdv4vec3d
	.type	_Zdv4vec3d, @function
_Zdv4vec3d:
	.frame	$fp,56,$31		# vars= 16, regs= 2/0, args= 24, gp= 8
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-56
	.cfi_def_cfa_offset 56
	sw	$31,52($sp)
	sw	$fp,48($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	move	$fp,$sp
	.cfi_def_cfa_register 30
	lui	$28,%hi(__gnu_local_gp)
	addiu	$28,$28,%lo(__gnu_local_gp)
	.cprestore	24
	sw	$4,36($fp)
	sw	$6,64($fp)
	sw	$7,68($fp)
	lw	$2,%got(__stack_chk_guard)($28)
	lw	$2,0($2)
	sw	$2,44($fp)
	ldc1	$f0,88($fp)
	lui	$2,%hi($LC0)
	ldc1	$f2,%lo($LC0)($2)
	div.d	$f0,$f2,$f0
	lw	$2,36($fp)
	addiu	$3,$fp,64
	sw	$3,16($sp)
	mfc1	$7,$f0
	mfhc1	$6,$f0
	move	$4,$2
	.option	pic0
	jal	_ZmldRK4vec3
	nop

	.option	pic2
	lw	$28,24($fp)
	lw	$2,%got(__stack_chk_guard)($28)
	lw	$3,44($fp)
	lw	$2,0($2)
	beq	$3,$2,$L40
	nop

	lw	$2,%call16(__stack_chk_fail)($28)
	move	$25,$2
	.reloc	1f,R_MIPS_JALR,__stack_chk_fail
1:	jalr	$25
	nop

$L40:
	lw	$2,36($fp)
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,52($sp)
	lw	$fp,48($sp)
	addiu	$sp,$sp,56
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_Zdv4vec3d
	.cfi_endproc
$LFE3929:
	.size	_Zdv4vec3d, .-_Zdv4vec3d
	.section	.text._Z11unit_vector4vec3,"axG",@progbits,_Z11unit_vector4vec3,comdat
	.align	2
	.weak	_Z11unit_vector4vec3
$LFB3932 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_Z11unit_vector4vec3
	.type	_Z11unit_vector4vec3, @function
_Z11unit_vector4vec3:
	.frame	$fp,72,$31		# vars= 16, regs= 2/0, args= 40, gp= 8
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-72
	.cfi_def_cfa_offset 72
	sw	$31,68($sp)
	sw	$fp,64($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	move	$fp,$sp
	.cfi_def_cfa_register 30
	lui	$28,%hi(__gnu_local_gp)
	addiu	$28,$28,%lo(__gnu_local_gp)
	.cprestore	40
	sw	$4,52($fp)
	sw	$6,80($fp)
	sw	$7,84($fp)
	lw	$2,%got(__stack_chk_guard)($28)
	lw	$2,0($2)
	sw	$2,60($fp)
	addiu	$2,$fp,80
	move	$4,$2
	lw	$2,%call16(_ZNK4vec36lengthEv)($28)
	move	$25,$2
	.reloc	1f,R_MIPS_JALR,_ZNK4vec36lengthEv
1:	jalr	$25
	nop

	lw	$28,40($fp)
	lw	$8,52($fp)
	sdc1	$f0,32($sp)
	lw	$5,88($fp)
	lw	$4,92($fp)
	lw	$3,96($fp)
	lw	$2,100($fp)
	sw	$5,16($sp)
	sw	$4,20($sp)
	sw	$3,24($sp)
	sw	$2,28($sp)
	lw	$6,80($fp)
	lw	$7,84($fp)
	move	$4,$8
	.option	pic0
	jal	_Zdv4vec3d
	nop

	.option	pic2
	lw	$28,40($fp)
	lw	$2,%got(__stack_chk_guard)($28)
	lw	$3,60($fp)
	lw	$2,0($2)
	beq	$3,$2,$L43
	nop

	lw	$2,%call16(__stack_chk_fail)($28)
	move	$25,$2
	.reloc	1f,R_MIPS_JALR,__stack_chk_fail
1:	jalr	$25
	nop

$L43:
	lw	$2,52($fp)
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,68($sp)
	lw	$fp,64($sp)
	addiu	$sp,$sp,72
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_Z11unit_vector4vec3
	.cfi_endproc
$LFE3932:
	.size	_Z11unit_vector4vec3, .-_Z11unit_vector4vec3
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,4
	.section	.text._ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align	2
	.weak	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED2Ev
$LFB3938 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED2Ev
	.type	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED2Ev:
	.frame	$fp,32,$31		# vars= 0, regs= 2/0, args= 16, gp= 8
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-32
	.cfi_def_cfa_offset 32
	sw	$31,28($sp)
	sw	$fp,24($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,32($fp)
	lw	$2,32($fp)
	addiu	$2,$2,4
	move	$4,$2
	.option	pic0
	jal	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED1Ev
	nop

	.option	pic2
	nop
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,28($sp)
	lw	$fp,24($sp)
	addiu	$sp,$sp,32
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED2Ev
	.cfi_endproc
$LFE3938:
	.size	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED1Ev
	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED1Ev = _ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt10shared_ptrI8materialED2Ev,"axG",@progbits,_ZNSt10shared_ptrI8materialED5Ev,comdat
	.align	2
	.weak	_ZNSt10shared_ptrI8materialED2Ev
$LFB3940 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZNSt10shared_ptrI8materialED2Ev
	.type	_ZNSt10shared_ptrI8materialED2Ev, @function
_ZNSt10shared_ptrI8materialED2Ev:
	.frame	$fp,32,$31		# vars= 0, regs= 2/0, args= 16, gp= 8
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-32
	.cfi_def_cfa_offset 32
	sw	$31,28($sp)
	sw	$fp,24($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,32($fp)
	lw	$2,32($fp)
	move	$4,$2
	.option	pic0
	jal	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED2Ev
	nop

	.option	pic2
	nop
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,28($sp)
	lw	$fp,24($sp)
	addiu	$sp,$sp,32
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZNSt10shared_ptrI8materialED2Ev
	.cfi_endproc
$LFE3940:
	.size	_ZNSt10shared_ptrI8materialED2Ev, .-_ZNSt10shared_ptrI8materialED2Ev
	.weak	_ZNSt10shared_ptrI8materialED1Ev
	_ZNSt10shared_ptrI8materialED1Ev = _ZNSt10shared_ptrI8materialED2Ev
	.section	.text._ZN10hit_recordC2Ev,"axG",@progbits,_ZN10hit_recordC5Ev,comdat
	.align	2
	.weak	_ZN10hit_recordC2Ev
$LFB3942 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZN10hit_recordC2Ev
	.type	_ZN10hit_recordC2Ev, @function
_ZN10hit_recordC2Ev:
	.frame	$fp,32,$31		# vars= 0, regs= 2/0, args= 16, gp= 8
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-32
	.cfi_def_cfa_offset 32
	sw	$31,28($sp)
	sw	$fp,24($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	move	$fp,$sp
	.cfi_def_cfa_register 30
	lui	$28,%hi(__gnu_local_gp)
	addiu	$28,$28,%lo(__gnu_local_gp)
	.cprestore	16
	sw	$4,32($fp)
	lw	$2,32($fp)
	move	$4,$2
	lw	$2,%call16(_ZN4vec3C1Ev)($28)
	move	$25,$2
	.reloc	1f,R_MIPS_JALR,_ZN4vec3C1Ev
1:	jalr	$25
	nop

	lw	$28,16($fp)
	lw	$2,32($fp)
	addiu	$2,$2,24
	move	$4,$2
	lw	$2,%call16(_ZN4vec3C1Ev)($28)
	move	$25,$2
	.reloc	1f,R_MIPS_JALR,_ZN4vec3C1Ev
1:	jalr	$25
	nop

	lw	$28,16($fp)
	lw	$2,32($fp)
	addiu	$2,$2,48
	move	$4,$2
	.option	pic0
	jal	_ZNSt10shared_ptrI8materialEC1Ev
	nop

	.option	pic2
	lw	$28,16($fp)
	nop
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,28($sp)
	lw	$fp,24($sp)
	addiu	$sp,$sp,32
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZN10hit_recordC2Ev
	.cfi_endproc
$LFE3942:
	.size	_ZN10hit_recordC2Ev, .-_ZN10hit_recordC2Ev
	.weak	_ZN10hit_recordC1Ev
	_ZN10hit_recordC1Ev = _ZN10hit_recordC2Ev
	.section	.text._ZN10hit_recordD2Ev,"axG",@progbits,_ZN10hit_recordD5Ev,comdat
	.align	2
	.weak	_ZN10hit_recordD2Ev
$LFB3945 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZN10hit_recordD2Ev
	.type	_ZN10hit_recordD2Ev, @function
_ZN10hit_recordD2Ev:
	.frame	$fp,32,$31		# vars= 0, regs= 2/0, args= 16, gp= 8
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-32
	.cfi_def_cfa_offset 32
	sw	$31,28($sp)
	sw	$fp,24($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,32($fp)
	lw	$2,32($fp)
	addiu	$2,$2,48
	move	$4,$2
	.option	pic0
	jal	_ZNSt10shared_ptrI8materialED1Ev
	nop

	.option	pic2
	nop
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,28($sp)
	lw	$fp,24($sp)
	addiu	$sp,$sp,32
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZN10hit_recordD2Ev
	.cfi_endproc
$LFE3945:
	.size	_ZN10hit_recordD2Ev, .-_ZN10hit_recordD2Ev
	.weak	_ZN10hit_recordD1Ev
	_ZN10hit_recordD1Ev = _ZN10hit_recordD2Ev
	.text
	.align	2
$LFB3934 = .
	.cfi_startproc
	.cfi_personality 0x80,DW.ref.__gxx_personality_v0
	.cfi_lsda 0,$LLSDA3934
	.set	nomips16
	.set	nomicromips
	.ent	_ZL9ray_colorRK3rayRK8hittablei
	.type	_ZL9ray_colorRK3rayRK8hittablei, @function
_ZL9ray_colorRK3rayRK8hittablei:
	.frame	$fp,344,$31		# vars= 280, regs= 3/2, args= 32, gp= 8
	.mask	0xc0010000,-12
	.fmask	0x00300000,-8
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-344
	.cfi_def_cfa_offset 344
	sw	$31,332($sp)
	sw	$fp,328($sp)
	sw	$16,324($sp)
	sdc1	$f20,336($sp)
	.cfi_offset 31, -12
	.cfi_offset 30, -16
	.cfi_offset 16, -20
	.cfi_offset 52, -4
	.cfi_offset 53, -8
	move	$fp,$sp
	.cfi_def_cfa_register 30
	lui	$28,%hi(__gnu_local_gp)
	addiu	$28,$28,%lo(__gnu_local_gp)
	.cprestore	32
	sw	$4,52($fp)
	sw	$5,48($fp)
	sw	$6,44($fp)
	sw	$7,356($fp)
	lw	$2,%got(__stack_chk_guard)($28)
	lw	$2,0($2)
	sw	$2,316($fp)
	addiu	$2,$fp,240
	move	$4,$2
$LEHB3 = .
	.option	pic0
	jal	_ZN10hit_recordC1Ev
	nop

	.option	pic2
$LEHE3 = .
	lw	$28,32($fp)
	lw	$2,356($fp)
	bgtz	$2,$L49
	nop

	sw	$0,28($sp)
	sw	$0,24($sp)
	sw	$0,20($sp)
	sw	$0,16($sp)
	move	$7,$0
	move	$6,$0
	lw	$4,52($fp)
	lw	$2,%call16(_ZN4vec3C1Eddd)($28)
	move	$25,$2
$LEHB4 = .
	.reloc	1f,R_MIPS_JALR,_ZN4vec3C1Eddd
1:	jalr	$25
	nop

	lw	$28,32($fp)
	.option	pic0
	b	$L50
	nop

	.option	pic2
$L49:
	lui	$2,%hi($LC1)
	ldc1	$f0,%lo($LC1)($2)
	sdc1	$f0,56($fp)
	lw	$2,44($fp)
	lw	$2,0($2)
	lw	$2,0($2)
	lui	$3,%hi($LC2)
	ldc1	$f2,%lo($LC2)($3)
	addiu	$3,$fp,240
	sw	$3,24($sp)
	ldc1	$f0,56($fp)
	sdc1	$f0,16($sp)
	mfc1	$7,$f2
	mfhc1	$6,$f2
	lw	$5,48($fp)
	lw	$4,44($fp)
	move	$25,$2
	jalr	$25
	nop

	lw	$28,32($fp)
	beq	$2,$0,$L51
	nop

	addiu	$3,$fp,168
	addiu	$2,$fp,240
	addiu	$2,$2,24
	addiu	$4,$fp,240
	move	$6,$2
	move	$5,$4
	move	$4,$3
	.option	pic0
	jal	_ZplRK4vec3S1_
	nop

	.option	pic2
	lw	$28,32($fp)
	addiu	$2,$fp,192
	move	$4,$2
	lw	$2,%call16(_Z18random_unit_vectorv)($28)
	move	$25,$2
	.reloc	1f,R_MIPS_JALR,_Z18random_unit_vectorv
1:	jalr	$25
	nop

	lw	$28,32($fp)
	addiu	$2,$fp,120
	addiu	$4,$fp,192
	addiu	$3,$fp,168
	move	$6,$4
	move	$5,$3
	move	$4,$2
	.option	pic0
	jal	_ZplRK4vec3S1_
	nop

	.option	pic2
	lw	$28,32($fp)
	addiu	$2,$fp,144
	addiu	$4,$fp,240
	addiu	$3,$fp,120
	move	$6,$4
	move	$5,$3
	move	$4,$2
	.option	pic0
	jal	_ZmiRK4vec3S1_
	nop

	.option	pic2
	lw	$28,32($fp)
	addiu	$4,$fp,144
	addiu	$3,$fp,240
	addiu	$2,$fp,192
	move	$6,$4
	move	$5,$3
	move	$4,$2
	lw	$2,%call16(_ZN3rayC1ERK4vec3S2_)($28)
	move	$25,$2
	.reloc	1f,R_MIPS_JALR,_ZN3rayC1ERK4vec3S2_
1:	jalr	$25
	nop

	lw	$28,32($fp)
	lw	$2,356($fp)
	addiu	$2,$2,-1
	addiu	$3,$fp,168
	addiu	$4,$fp,192
	move	$7,$2
	lw	$6,44($fp)
	move	$5,$4
	move	$4,$3
	.option	pic0
	jal	_ZL9ray_colorRK3rayRK8hittablei
	nop

	.option	pic2
	lw	$28,32($fp)
	lw	$3,52($fp)
	lui	$2,%hi($LC3)
	ldc1	$f0,%lo($LC3)($2)
	addiu	$2,$fp,168
	sw	$2,16($sp)
	mfc1	$7,$f0
	mfhc1	$6,$f0
	move	$4,$3
	.option	pic0
	jal	_ZmldRK4vec3
	nop

	.option	pic2
	lw	$28,32($fp)
	.option	pic0
	b	$L50
	nop

	.option	pic2
$L51:
	addiu	$2,$fp,192
	lw	$5,48($fp)
	move	$4,$2
	lw	$2,%call16(_ZNK3ray9directionEv)($28)
	move	$25,$2
	.reloc	1f,R_MIPS_JALR,_ZNK3ray9directionEv
1:	jalr	$25
	nop

	lw	$28,32($fp)
	addiu	$8,$fp,72
	lw	$5,200($fp)
	lw	$4,204($fp)
	lw	$3,208($fp)
	lw	$2,212($fp)
	sw	$5,16($sp)
	sw	$4,20($sp)
	sw	$3,24($sp)
	sw	$2,28($sp)
	lw	$6,192($fp)
	lw	$7,196($fp)
	move	$4,$8
	.option	pic0
	jal	_Z11unit_vector4vec3
	nop

	.option	pic2
	lw	$28,32($fp)
	addiu	$2,$fp,72
	move	$4,$2
	lw	$2,%call16(_ZNK4vec31yEv)($28)
	move	$25,$2
	.reloc	1f,R_MIPS_JALR,_ZNK4vec31yEv
1:	jalr	$25
	nop

	lw	$28,32($fp)
	mov.d	$f2,$f0
	lui	$2,%hi($LC0)
	ldc1	$f0,%lo($LC0)($2)
	add.d	$f2,$f2,$f0
	lui	$2,%hi($LC3)
	ldc1	$f0,%lo($LC3)($2)
	mul.d	$f0,$f2,$f0
	sdc1	$f0,64($fp)
	lui	$2,%hi($LC0)
	ldc1	$f2,%lo($LC0)($2)
	ldc1	$f0,64($fp)
	sub.d	$f20,$f2,$f0
	lui	$2,%hi($LC0)
	ldc1	$f2,%lo($LC0)($2)
	addiu	$3,$fp,96
	lui	$2,%hi($LC0)
	ldc1	$f0,%lo($LC0)($2)
	sdc1	$f0,24($sp)
	lui	$2,%hi($LC0)
	ldc1	$f0,%lo($LC0)($2)
	sdc1	$f0,16($sp)
	mfc1	$7,$f2
	mfhc1	$6,$f2
	move	$4,$3
	lw	$2,%call16(_ZN4vec3C1Eddd)($28)
	move	$25,$2
	.reloc	1f,R_MIPS_JALR,_ZN4vec3C1Eddd
1:	jalr	$25
	nop

	lw	$28,32($fp)
	addiu	$3,$fp,120
	addiu	$2,$fp,96
	sw	$2,16($sp)
	mfc1	$7,$f20
	mfhc1	$6,$f20
	move	$4,$3
	.option	pic0
	jal	_ZmldRK4vec3
	nop

	.option	pic2
	lw	$28,32($fp)
	lui	$2,%hi($LC3)
	ldc1	$f2,%lo($LC3)($2)
	addiu	$3,$fp,144
	lui	$2,%hi($LC0)
	ldc1	$f0,%lo($LC0)($2)
	sdc1	$f0,24($sp)
	lui	$2,%hi($LC4)
	ldc1	$f0,%lo($LC4)($2)
	sdc1	$f0,16($sp)
	mfc1	$7,$f2
	mfhc1	$6,$f2
	move	$4,$3
	lw	$2,%call16(_ZN4vec3C1Eddd)($28)
	move	$25,$2
	.reloc	1f,R_MIPS_JALR,_ZN4vec3C1Eddd
1:	jalr	$25
	nop

	lw	$28,32($fp)
	addiu	$3,$fp,168
	addiu	$2,$fp,144
	sw	$2,16($sp)
	lw	$7,68($fp)
	lw	$6,64($fp)
	move	$4,$3
	.option	pic0
	jal	_ZmldRK4vec3
	nop

	.option	pic2
	lw	$28,32($fp)
	lw	$2,52($fp)
	addiu	$4,$fp,168
	addiu	$3,$fp,120
	move	$6,$4
	move	$5,$3
	move	$4,$2
	.option	pic0
	jal	_ZplRK4vec3S1_
	nop

	.option	pic2
$LEHE4 = .
	lw	$28,32($fp)
$L50:
	addiu	$2,$fp,240
	move	$4,$2
	.option	pic0
	jal	_ZN10hit_recordD1Ev
	nop

	.option	pic2
	lw	$28,32($fp)
	lw	$2,%got(__stack_chk_guard)($28)
	lw	$3,316($fp)
	lw	$2,0($2)
	beq	$3,$2,$L54
	nop

	.option	pic0
	b	$L56
	nop

	.option	pic2
$L55:
	lw	$28,32($fp)
	move	$16,$4
	addiu	$2,$fp,240
	move	$4,$2
	.option	pic0
	jal	_ZN10hit_recordD1Ev
	nop

	.option	pic2
	lw	$28,32($fp)
	move	$2,$16
	move	$4,$2
	lw	$2,%call16(_Unwind_Resume)($28)
	move	$25,$2
$LEHB5 = .
	.reloc	1f,R_MIPS_JALR,_Unwind_Resume
1:	jalr	$25
	nop

$LEHE5 = .
$L56:
	lw	$2,%call16(__stack_chk_fail)($28)
	move	$25,$2
	.reloc	1f,R_MIPS_JALR,__stack_chk_fail
1:	jalr	$25
	nop

$L54:
	lw	$2,52($fp)
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,332($sp)
	lw	$fp,328($sp)
	lw	$16,324($sp)
	ldc1	$f20,336($sp)
	addiu	$sp,$sp,344
	.cfi_restore 52
	.cfi_restore 53
	.cfi_restore 16
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZL9ray_colorRK3rayRK8hittablei
	.cfi_endproc
$LFE3934:
	.section	.gcc_except_table,"aw",@progbits
$LLSDA3934:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 $LLSDACSE3934-$LLSDACSB3934
$LLSDACSB3934:
	.uleb128 $LEHB3-$LFB3934
	.uleb128 $LEHE3-$LEHB3
	.uleb128 0
	.uleb128 0
	.uleb128 $LEHB4-$LFB3934
	.uleb128 $LEHE4-$LEHB4
	.uleb128 $L55-$LFB3934
	.uleb128 0
	.uleb128 $LEHB5-$LFB3934
	.uleb128 $LEHE5-$LEHB5
	.uleb128 0
	.uleb128 0
$LLSDACSE3934:
	.text
	.size	_ZL9ray_colorRK3rayRK8hittablei, .-_ZL9ray_colorRK3rayRK8hittablei
	.section	.text._ZN13hittable_listD2Ev,"axG",@progbits,_ZN13hittable_listD5Ev,comdat
	.align	2
	.weak	_ZN13hittable_listD2Ev
$LFB3949 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZN13hittable_listD2Ev
	.type	_ZN13hittable_listD2Ev, @function
_ZN13hittable_listD2Ev:
	.frame	$fp,32,$31		# vars= 0, regs= 2/0, args= 16, gp= 8
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-32
	.cfi_def_cfa_offset 32
	sw	$31,28($sp)
	sw	$fp,24($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	move	$fp,$sp
	.cfi_def_cfa_register 30
	lui	$28,%hi(__gnu_local_gp)
	addiu	$28,$28,%lo(__gnu_local_gp)
	.cprestore	16
	sw	$4,32($fp)
	lw	$2,%got(_ZTV13hittable_list)($28)
	addiu	$3,$2,8
	lw	$2,32($fp)
	sw	$3,0($2)
	lw	$2,32($fp)
	addiu	$2,$2,4
	move	$4,$2
	.option	pic0
	jal	_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED1Ev
	nop

	.option	pic2
	lw	$28,16($fp)
	nop
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,28($sp)
	lw	$fp,24($sp)
	addiu	$sp,$sp,32
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZN13hittable_listD2Ev
	.cfi_endproc
$LFE3949:
	.size	_ZN13hittable_listD2Ev, .-_ZN13hittable_listD2Ev
	.weak	_ZN13hittable_listD1Ev
	_ZN13hittable_listD1Ev = _ZN13hittable_listD2Ev
	.section	.text._ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align	2
	.weak	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED2Ev
$LFB3953 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED2Ev
	.type	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED2Ev:
	.frame	$fp,32,$31		# vars= 0, regs= 2/0, args= 16, gp= 8
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-32
	.cfi_def_cfa_offset 32
	sw	$31,28($sp)
	sw	$fp,24($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,32($fp)
	lw	$2,32($fp)
	addiu	$2,$2,4
	move	$4,$2
	.option	pic0
	jal	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED1Ev
	nop

	.option	pic2
	nop
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,28($sp)
	lw	$fp,24($sp)
	addiu	$sp,$sp,32
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED2Ev
	.cfi_endproc
$LFE3953:
	.size	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED1Ev
	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED1Ev = _ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt10shared_ptrI6sphereED2Ev,"axG",@progbits,_ZNSt10shared_ptrI6sphereED5Ev,comdat
	.align	2
	.weak	_ZNSt10shared_ptrI6sphereED2Ev
$LFB3955 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZNSt10shared_ptrI6sphereED2Ev
	.type	_ZNSt10shared_ptrI6sphereED2Ev, @function
_ZNSt10shared_ptrI6sphereED2Ev:
	.frame	$fp,32,$31		# vars= 0, regs= 2/0, args= 16, gp= 8
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-32
	.cfi_def_cfa_offset 32
	sw	$31,28($sp)
	sw	$fp,24($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,32($fp)
	lw	$2,32($fp)
	move	$4,$2
	.option	pic0
	jal	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED2Ev
	nop

	.option	pic2
	nop
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,28($sp)
	lw	$fp,24($sp)
	addiu	$sp,$sp,32
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZNSt10shared_ptrI6sphereED2Ev
	.cfi_endproc
$LFE3955:
	.size	_ZNSt10shared_ptrI6sphereED2Ev, .-_ZNSt10shared_ptrI6sphereED2Ev
	.weak	_ZNSt10shared_ptrI6sphereED1Ev
	_ZNSt10shared_ptrI6sphereED1Ev = _ZNSt10shared_ptrI6sphereED2Ev
	.section	.text._ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align	2
	.weak	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED2Ev
$LFB3959 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED2Ev
	.type	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED2Ev:
	.frame	$fp,32,$31		# vars= 0, regs= 2/0, args= 16, gp= 8
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-32
	.cfi_def_cfa_offset 32
	sw	$31,28($sp)
	sw	$fp,24($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,32($fp)
	lw	$2,32($fp)
	addiu	$2,$2,4
	move	$4,$2
	.option	pic0
	jal	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED1Ev
	nop

	.option	pic2
	nop
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,28($sp)
	lw	$fp,24($sp)
	addiu	$sp,$sp,32
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED2Ev
	.cfi_endproc
$LFE3959:
	.size	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED1Ev
	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED1Ev = _ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt10shared_ptrI8hittableED2Ev,"axG",@progbits,_ZNSt10shared_ptrI8hittableED5Ev,comdat
	.align	2
	.weak	_ZNSt10shared_ptrI8hittableED2Ev
$LFB3961 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZNSt10shared_ptrI8hittableED2Ev
	.type	_ZNSt10shared_ptrI8hittableED2Ev, @function
_ZNSt10shared_ptrI8hittableED2Ev:
	.frame	$fp,32,$31		# vars= 0, regs= 2/0, args= 16, gp= 8
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-32
	.cfi_def_cfa_offset 32
	sw	$31,28($sp)
	sw	$fp,24($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,32($fp)
	lw	$2,32($fp)
	move	$4,$2
	.option	pic0
	jal	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED2Ev
	nop

	.option	pic2
	nop
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,28($sp)
	lw	$fp,24($sp)
	addiu	$sp,$sp,32
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZNSt10shared_ptrI8hittableED2Ev
	.cfi_endproc
$LFE3961:
	.size	_ZNSt10shared_ptrI8hittableED2Ev, .-_ZNSt10shared_ptrI8hittableED2Ev
	.weak	_ZNSt10shared_ptrI8hittableED1Ev
	_ZNSt10shared_ptrI8hittableED1Ev = _ZNSt10shared_ptrI8hittableED2Ev
	.rdata
	.align	2
$LC8:
	.ascii	"chapter8.ppm\000"
	.align	2
$LC9:
	.ascii	"P3\012\000"
	.align	2
$LC10:
	.ascii	"\012255\012\000"
	.align	2
$LC11:
	.ascii	"\015Scanlines remaining: \000"
	.align	2
$LC14:
	.ascii	"\012Done.\012\000"
	.text
	.align	2
	.globl	main
$LFB3947 = .
	.cfi_startproc
	.cfi_personality 0x80,DW.ref.__gxx_personality_v0
	.cfi_lsda 0,$LLSDA3947
	.set	nomips16
	.set	nomicromips
	.ent	main
	.type	main, @function
main:
	.frame	$fp,720,$31		# vars= 648, regs= 3/2, args= 40, gp= 8
	.mask	0xc0010000,-12
	.fmask	0x00300000,-8
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-720
	.cfi_def_cfa_offset 720
	sw	$31,708($sp)
	sw	$fp,704($sp)
	sw	$16,700($sp)
	sdc1	$f20,712($sp)
	.cfi_offset 31, -12
	.cfi_offset 30, -16
	.cfi_offset 16, -20
	.cfi_offset 52, -4
	.cfi_offset 53, -8
	move	$fp,$sp
	.cfi_def_cfa_register 30
	lui	$28,%hi(__gnu_local_gp)
	addiu	$28,$28,%lo(__gnu_local_gp)
	.cprestore	40
	lw	$2,%got(__stack_chk_guard)($28)
	lw	$2,0($2)
	sw	$2,692($fp)
	lui	$2,%hi($LC5)
	ldc1	$f0,%lo($LC5)($2)
	sdc1	$f0,88($fp)
	li	$2,400			# 0x190
	sw	$2,64($fp)
	li	$2,225			# 0xe1
	sw	$2,68($fp)
	li	$2,100			# 0x64
	sw	$2,72($fp)
	li	$2,50			# 0x32
	sw	$2,76($fp)
	addiu	$2,$fp,128
	move	$4,$2
	lw	$2,%call16(_ZN13hittable_listC1Ev)($28)
	move	$25,$2
$LEHB6 = .
	.reloc	1f,R_MIPS_JALR,_ZN13hittable_listC1Ev
1:	jalr	$25
	nop

$LEHE6 = .
	lw	$28,40($fp)
	addiu	$3,$fp,416
	lui	$2,%hi($LC6)
	ldc1	$f0,%lo($LC6)($2)
	sdc1	$f0,24($sp)
	sw	$0,20($sp)
	sw	$0,16($sp)
	move	$7,$0
	move	$6,$0
	move	$4,$3
	lw	$2,%call16(_ZN4vec3C1Eddd)($28)
	move	$25,$2
$LEHB7 = .
	.reloc	1f,R_MIPS_JALR,_ZN4vec3C1Eddd
1:	jalr	$25
	nop

$LEHE7 = .
	lw	$28,40($fp)
	lui	$2,%hi($LC3)
	ldc1	$f0,%lo($LC3)($2)
	sdc1	$f0,80($fp)
	addiu	$2,$fp,112
	addiu	$4,$fp,80
	addiu	$3,$fp,416
	move	$6,$4
	move	$5,$3
	move	$4,$2
$LEHB8 = .
	.option	pic0
	jal	_ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_
	nop

	.option	pic2
$LEHE8 = .
	lw	$28,40($fp)
	addiu	$3,$fp,112
	addiu	$2,$fp,120
	move	$5,$3
	move	$4,$2
	.option	pic0
	jal	_ZNSt10shared_ptrI8hittableEC1I6spherevEEOS_IT_E
	nop

	.option	pic2
	lw	$28,40($fp)
	addiu	$3,$fp,120
	addiu	$2,$fp,128
	move	$5,$3
	move	$4,$2
	lw	$2,%call16(_ZN13hittable_list3addESt10shared_ptrI8hittableE)($28)
	move	$25,$2
$LEHB9 = .
	.reloc	1f,R_MIPS_JALR,_ZN13hittable_list3addESt10shared_ptrI8hittableE
1:	jalr	$25
	nop

$LEHE9 = .
	lw	$28,40($fp)
	addiu	$2,$fp,120
	move	$4,$2
	.option	pic0
	jal	_ZNSt10shared_ptrI8hittableED1Ev
	nop

	.option	pic2
	lw	$28,40($fp)
	addiu	$2,$fp,112
	move	$4,$2
	.option	pic0
	jal	_ZNSt10shared_ptrI6sphereED1Ev
	nop

	.option	pic2
	lw	$28,40($fp)
	addiu	$3,$fp,416
	lui	$2,%hi($LC6)
	ldc1	$f0,%lo($LC6)($2)
	sdc1	$f0,24($sp)
	lui	$2,%hi($LC7)
	ldc1	$f0,%lo($LC7)($2)
	sdc1	$f0,16($sp)
	move	$7,$0
	move	$6,$0
	move	$4,$3
	lw	$2,%call16(_ZN4vec3C1Eddd)($28)
	move	$25,$2
$LEHB10 = .
	.reloc	1f,R_MIPS_JALR,_ZN4vec3C1Eddd
1:	jalr	$25
	nop

$LEHE10 = .
	lw	$28,40($fp)
	li	$2,100			# 0x64
	sw	$2,80($fp)
	addiu	$2,$fp,112
	addiu	$4,$fp,80
	addiu	$3,$fp,416
	move	$6,$4
	move	$5,$3
	move	$4,$2
$LEHB11 = .
	.option	pic0
	jal	_ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_
	nop

	.option	pic2
$LEHE11 = .
	lw	$28,40($fp)
	addiu	$3,$fp,112
	addiu	$2,$fp,120
	move	$5,$3
	move	$4,$2
	.option	pic0
	jal	_ZNSt10shared_ptrI8hittableEC1I6spherevEEOS_IT_E
	nop

	.option	pic2
	lw	$28,40($fp)
	addiu	$3,$fp,120
	addiu	$2,$fp,128
	move	$5,$3
	move	$4,$2
	lw	$2,%call16(_ZN13hittable_list3addESt10shared_ptrI8hittableE)($28)
	move	$25,$2
$LEHB12 = .
	.reloc	1f,R_MIPS_JALR,_ZN13hittable_list3addESt10shared_ptrI8hittableE
1:	jalr	$25
	nop

$LEHE12 = .
	lw	$28,40($fp)
	addiu	$2,$fp,120
	move	$4,$2
	.option	pic0
	jal	_ZNSt10shared_ptrI8hittableED1Ev
	nop

	.option	pic2
	lw	$28,40($fp)
	addiu	$2,$fp,112
	move	$4,$2
	.option	pic0
	jal	_ZNSt10shared_ptrI6sphereED1Ev
	nop

	.option	pic2
	lw	$28,40($fp)
	addiu	$2,$fp,240
	move	$4,$2
	lw	$2,%call16(_ZN6cameraC1Ev)($28)
	move	$25,$2
$LEHB13 = .
	.reloc	1f,R_MIPS_JALR,_ZN6cameraC1Ev
1:	jalr	$25
	nop

	lw	$28,40($fp)
	addiu	$2,$fp,416
	move	$4,$2
	lw	$2,%call16(_ZNSt14basic_ofstreamIcSt11char_traitsIcEEC1Ev)($28)
	move	$25,$2
	.reloc	1f,R_MIPS_JALR,_ZNSt14basic_ofstreamIcSt11char_traitsIcEEC1Ev
1:	jalr	$25
	nop

$LEHE13 = .
	lw	$28,40($fp)
	addiu	$3,$fp,416
	li	$6,16			# 0x10
	lui	$2,%hi($LC8)
	addiu	$5,$2,%lo($LC8)
	move	$4,$3
	lw	$2,%call16(_ZNSt14basic_ofstreamIcSt11char_traitsIcEE4openEPKcSt13_Ios_Openmode)($28)
	move	$25,$2
$LEHB14 = .
	.reloc	1f,R_MIPS_JALR,_ZNSt14basic_ofstreamIcSt11char_traitsIcEE4openEPKcSt13_Ios_Openmode
1:	jalr	$25
	nop

	lw	$28,40($fp)
	addiu	$3,$fp,416
	lui	$2,%hi($LC9)
	addiu	$5,$2,%lo($LC9)
	move	$4,$3
	lw	$2,%call16(_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc)($28)
	move	$25,$2
	.reloc	1f,R_MIPS_JALR,_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
1:	jalr	$25
	nop

	lw	$28,40($fp)
	li	$5,400			# 0x190
	move	$4,$2
	lw	$2,%call16(_ZNSolsEi)($28)
	move	$25,$2
	.reloc	1f,R_MIPS_JALR,_ZNSolsEi
1:	jalr	$25
	nop

	lw	$28,40($fp)
	li	$5,32			# 0x20
	move	$4,$2
	lw	$2,%call16(_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_c)($28)
	move	$25,$2
	.reloc	1f,R_MIPS_JALR,_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_c
1:	jalr	$25
	nop

	lw	$28,40($fp)
	li	$5,225			# 0xe1
	move	$4,$2
	lw	$2,%call16(_ZNSolsEi)($28)
	move	$25,$2
	.reloc	1f,R_MIPS_JALR,_ZNSolsEi
1:	jalr	$25
	nop

	lw	$28,40($fp)
	move	$3,$2
	lui	$2,%hi($LC10)
	addiu	$5,$2,%lo($LC10)
	move	$4,$3
	lw	$2,%call16(_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc)($28)
	move	$25,$2
	.reloc	1f,R_MIPS_JALR,_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
1:	jalr	$25
	nop

	lw	$28,40($fp)
	li	$2,224			# 0xe0
	sw	$2,52($fp)
$L68:
	lw	$2,52($fp)
	bltz	$2,$L63
	nop

	lui	$2,%hi($LC11)
	addiu	$5,$2,%lo($LC11)
	lw	$4,%got(_ZSt4cerr)($28)
	lw	$2,%call16(_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc)($28)
	move	$25,$2
	.reloc	1f,R_MIPS_JALR,_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
1:	jalr	$25
	nop

	lw	$28,40($fp)
	lw	$5,52($fp)
	move	$4,$2
	lw	$2,%call16(_ZNSolsEi)($28)
	move	$25,$2
	.reloc	1f,R_MIPS_JALR,_ZNSolsEi
1:	jalr	$25
	nop

	lw	$28,40($fp)
	li	$5,32			# 0x20
	move	$4,$2
	lw	$2,%call16(_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_c)($28)
	move	$25,$2
	.reloc	1f,R_MIPS_JALR,_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_c
1:	jalr	$25
	nop

	lw	$28,40($fp)
	lw	$5,%got(_ZSt5flushIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)($28)
	move	$4,$2
	lw	$2,%call16(_ZNSolsEPFRSoS_E)($28)
	move	$25,$2
	.reloc	1f,R_MIPS_JALR,_ZNSolsEPFRSoS_E
1:	jalr	$25
	nop

	lw	$28,40($fp)
	sw	$0,56($fp)
$L67:
	lw	$2,56($fp)
	slt	$2,$2,400
	beq	$2,$0,$L64
	nop

	addiu	$2,$fp,144
	sw	$0,28($sp)
	sw	$0,24($sp)
	sw	$0,20($sp)
	sw	$0,16($sp)
	move	$7,$0
	move	$6,$0
	move	$4,$2
	lw	$2,%call16(_ZN4vec3C1Eddd)($28)
	move	$25,$2
	.reloc	1f,R_MIPS_JALR,_ZN4vec3C1Eddd
1:	jalr	$25
	nop

	lw	$28,40($fp)
	sw	$0,60($fp)
$L66:
	lw	$2,60($fp)
	slt	$2,$2,100
	beq	$2,$0,$L65
	nop

	lw	$2,56($fp)
	mtc1	$2,$f0
	cvt.d.w	$f20,$f0
	.option	pic0
	jal	_Z13random_doublev
	nop

	.option	pic2
	lw	$28,40($fp)
	add.d	$f2,$f20,$f0
	lui	$2,%hi($LC12)
	ldc1	$f0,%lo($LC12)($2)
	div.d	$f0,$f2,$f0
	sdc1	$f0,96($fp)
	lw	$2,52($fp)
	mtc1	$2,$f0
	cvt.d.w	$f20,$f0
	.option	pic0
	jal	_Z13random_doublev
	nop

	.option	pic2
	lw	$28,40($fp)
	add.d	$f2,$f20,$f0
	lui	$2,%hi($LC13)
	ldc1	$f0,%lo($LC13)($2)
	div.d	$f0,$f2,$f0
	sdc1	$f0,104($fp)
	addiu	$2,$fp,192
	addiu	$3,$fp,240
	ldc1	$f0,104($fp)
	sdc1	$f0,16($sp)
	lw	$7,100($fp)
	lw	$6,96($fp)
	move	$5,$3
	move	$4,$2
	lw	$2,%call16(_ZNK6camera7get_rayEdd)($28)
	move	$25,$2
	.reloc	1f,R_MIPS_JALR,_ZNK6camera7get_rayEdd
1:	jalr	$25
	nop

	lw	$28,40($fp)
	addiu	$2,$fp,168
	addiu	$4,$fp,128
	addiu	$3,$fp,192
	li	$7,50			# 0x32
	move	$6,$4
	move	$5,$3
	move	$4,$2
	.option	pic0
	jal	_ZL9ray_colorRK3rayRK8hittablei
	nop

	.option	pic2
	lw	$28,40($fp)
	addiu	$3,$fp,168
	addiu	$2,$fp,144
	move	$5,$3
	move	$4,$2
	lw	$2,%call16(_ZN4vec3pLERKS_)($28)
	move	$25,$2
	.reloc	1f,R_MIPS_JALR,_ZN4vec3pLERKS_
1:	jalr	$25
	nop

	lw	$28,40($fp)
	lw	$2,60($fp)
	addiu	$2,$2,1
	sw	$2,60($fp)
	.option	pic0
	b	$L66
	nop

	.option	pic2
$L65:
	addiu	$8,$fp,416
	li	$2,100			# 0x64
	sw	$2,32($sp)
	lw	$5,152($fp)
	lw	$4,156($fp)
	lw	$3,160($fp)
	lw	$2,164($fp)
	sw	$5,16($sp)
	sw	$4,20($sp)
	sw	$3,24($sp)
	sw	$2,28($sp)
	lw	$6,144($fp)
	lw	$7,148($fp)
	move	$4,$8
	lw	$2,%call16(_Z11write_colorRSt14basic_ofstreamIcSt11char_traitsIcEE4vec3i)($28)
	move	$25,$2
	.reloc	1f,R_MIPS_JALR,_Z11write_colorRSt14basic_ofstreamIcSt11char_traitsIcEE4vec3i
1:	jalr	$25
	nop

	lw	$28,40($fp)
	lw	$2,56($fp)
	addiu	$2,$2,1
	sw	$2,56($fp)
	.option	pic0
	b	$L67
	nop

	.option	pic2
$L64:
	lw	$2,52($fp)
	addiu	$2,$2,-1
	sw	$2,52($fp)
	.option	pic0
	b	$L68
	nop

	.option	pic2
$L63:
	lui	$2,%hi($LC14)
	addiu	$5,$2,%lo($LC14)
	lw	$4,%got(_ZSt4cerr)($28)
	lw	$2,%call16(_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc)($28)
	move	$25,$2
	.reloc	1f,R_MIPS_JALR,_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
1:	jalr	$25
	nop

$LEHE14 = .
	lw	$28,40($fp)
	move	$16,$0
	addiu	$2,$fp,416
	move	$4,$2
	lw	$2,%call16(_ZNSt14basic_ofstreamIcSt11char_traitsIcEED1Ev)($28)
	move	$25,$2
	.reloc	1f,R_MIPS_JALR,_ZNSt14basic_ofstreamIcSt11char_traitsIcEED1Ev
1:	jalr	$25
	nop

	lw	$28,40($fp)
	addiu	$2,$fp,128
	move	$4,$2
	.option	pic0
	jal	_ZN13hittable_listD1Ev
	nop

	.option	pic2
	lw	$28,40($fp)
	move	$4,$16
	lw	$2,%got(__stack_chk_guard)($28)
	lw	$3,692($fp)
	lw	$2,0($2)
	beq	$3,$2,$L76
	nop

	.option	pic0
	b	$L83
	nop

	.option	pic2
$L79:
	lw	$28,40($fp)
	move	$16,$4
	addiu	$2,$fp,120
	move	$4,$2
	.option	pic0
	jal	_ZNSt10shared_ptrI8hittableED1Ev
	nop

	.option	pic2
	lw	$28,40($fp)
	addiu	$2,$fp,112
	move	$4,$2
	.option	pic0
	jal	_ZNSt10shared_ptrI6sphereED1Ev
	nop

	.option	pic2
	lw	$28,40($fp)
	move	$2,$16
	.option	pic0
	b	$L71
	nop

	.option	pic2
$L78:
	lw	$28,40($fp)
	move	$2,$4
$L71:
	move	$16,$2
	.option	pic0
	b	$L72
	nop

	.option	pic2
$L81:
	lw	$28,40($fp)
	move	$16,$4
	addiu	$2,$fp,120
	move	$4,$2
	.option	pic0
	jal	_ZNSt10shared_ptrI8hittableED1Ev
	nop

	.option	pic2
	lw	$28,40($fp)
	addiu	$2,$fp,112
	move	$4,$2
	.option	pic0
	jal	_ZNSt10shared_ptrI6sphereED1Ev
	nop

	.option	pic2
	lw	$28,40($fp)
	move	$2,$16
	.option	pic0
	b	$L74
	nop

	.option	pic2
$L80:
	lw	$28,40($fp)
	move	$2,$4
$L74:
	move	$16,$2
	.option	pic0
	b	$L72
	nop

	.option	pic2
$L82:
	lw	$28,40($fp)
	move	$16,$4
	addiu	$2,$fp,416
	move	$4,$2
	lw	$2,%call16(_ZNSt14basic_ofstreamIcSt11char_traitsIcEED1Ev)($28)
	move	$25,$2
	.reloc	1f,R_MIPS_JALR,_ZNSt14basic_ofstreamIcSt11char_traitsIcEED1Ev
1:	jalr	$25
	nop

	lw	$28,40($fp)
	.option	pic0
	b	$L72
	nop

	.option	pic2
$L77:
	lw	$28,40($fp)
	move	$16,$4
$L72:
	addiu	$2,$fp,128
	move	$4,$2
	.option	pic0
	jal	_ZN13hittable_listD1Ev
	nop

	.option	pic2
	lw	$28,40($fp)
	move	$2,$16
	move	$4,$2
	lw	$2,%call16(_Unwind_Resume)($28)
	move	$25,$2
$LEHB15 = .
	.reloc	1f,R_MIPS_JALR,_Unwind_Resume
1:	jalr	$25
	nop

$LEHE15 = .
$L83:
	lw	$2,%call16(__stack_chk_fail)($28)
	move	$25,$2
	.reloc	1f,R_MIPS_JALR,__stack_chk_fail
1:	jalr	$25
	nop

$L76:
	move	$2,$4
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,708($sp)
	lw	$fp,704($sp)
	lw	$16,700($sp)
	ldc1	$f20,712($sp)
	addiu	$sp,$sp,720
	.cfi_restore 52
	.cfi_restore 53
	.cfi_restore 16
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	main
	.cfi_endproc
$LFE3947:
	.section	.gcc_except_table
$LLSDA3947:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 $LLSDACSE3947-$LLSDACSB3947
$LLSDACSB3947:
	.uleb128 $LEHB6-$LFB3947
	.uleb128 $LEHE6-$LEHB6
	.uleb128 0
	.uleb128 0
	.uleb128 $LEHB7-$LFB3947
	.uleb128 $LEHE7-$LEHB7
	.uleb128 $L77-$LFB3947
	.uleb128 0
	.uleb128 $LEHB8-$LFB3947
	.uleb128 $LEHE8-$LEHB8
	.uleb128 $L78-$LFB3947
	.uleb128 0
	.uleb128 $LEHB9-$LFB3947
	.uleb128 $LEHE9-$LEHB9
	.uleb128 $L79-$LFB3947
	.uleb128 0
	.uleb128 $LEHB10-$LFB3947
	.uleb128 $LEHE10-$LEHB10
	.uleb128 $L77-$LFB3947
	.uleb128 0
	.uleb128 $LEHB11-$LFB3947
	.uleb128 $LEHE11-$LEHB11
	.uleb128 $L80-$LFB3947
	.uleb128 0
	.uleb128 $LEHB12-$LFB3947
	.uleb128 $LEHE12-$LEHB12
	.uleb128 $L81-$LFB3947
	.uleb128 0
	.uleb128 $LEHB13-$LFB3947
	.uleb128 $LEHE13-$LEHB13
	.uleb128 $L77-$LFB3947
	.uleb128 0
	.uleb128 $LEHB14-$LFB3947
	.uleb128 $LEHE14-$LEHB14
	.uleb128 $L82-$LFB3947
	.uleb128 0
	.uleb128 $LEHB15-$LFB3947
	.uleb128 $LEHE15-$LEHB15
	.uleb128 0
	.uleb128 0
$LLSDACSE3947:
	.text
	.size	main, .-main
	.section	.text._ZNSt25uniform_real_distributionIdEC2Edd,"axG",@progbits,_ZNSt25uniform_real_distributionIdEC5Edd,comdat
	.align	2
	.weak	_ZNSt25uniform_real_distributionIdEC2Edd
$LFB4212 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZNSt25uniform_real_distributionIdEC2Edd
	.type	_ZNSt25uniform_real_distributionIdEC2Edd, @function
_ZNSt25uniform_real_distributionIdEC2Edd:
	.frame	$fp,40,$31		# vars= 0, regs= 2/0, args= 24, gp= 8
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-40
	.cfi_def_cfa_offset 40
	sw	$31,36($sp)
	sw	$fp,32($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,40($fp)
	sw	$7,52($fp)
	sw	$6,48($fp)
	lw	$2,40($fp)
	ldc1	$f0,56($fp)
	sdc1	$f0,16($sp)
	lw	$7,52($fp)
	lw	$6,48($fp)
	move	$4,$2
	.option	pic0
	jal	_ZNSt25uniform_real_distributionIdE10param_typeC1Edd
	nop

	.option	pic2
	nop
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,36($sp)
	lw	$fp,32($sp)
	addiu	$sp,$sp,40
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZNSt25uniform_real_distributionIdEC2Edd
	.cfi_endproc
$LFE4212:
	.size	_ZNSt25uniform_real_distributionIdEC2Edd, .-_ZNSt25uniform_real_distributionIdEC2Edd
	.weak	_ZNSt25uniform_real_distributionIdEC1Edd
	_ZNSt25uniform_real_distributionIdEC1Edd = _ZNSt25uniform_real_distributionIdEC2Edd
	.section	.text._ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC2Ev,"axG",@progbits,_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC5Ev,comdat
	.align	2
	.weak	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC2Ev
$LFB4215 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC2Ev
	.type	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC2Ev, @function
_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC2Ev:
	.frame	$fp,32,$31		# vars= 0, regs= 2/0, args= 16, gp= 8
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-32
	.cfi_def_cfa_offset 32
	sw	$31,28($sp)
	sw	$fp,24($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,32($fp)
	li	$5,5489			# 0x1571
	lw	$4,32($fp)
	.option	pic0
	jal	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC1Ej
	nop

	.option	pic2
	nop
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,28($sp)
	lw	$fp,24($sp)
	addiu	$sp,$sp,32
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC2Ev
	.cfi_endproc
$LFE4215:
	.size	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC2Ev, .-_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC2Ev
	.weak	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC1Ev
	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC1Ev = _ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC2Ev
	.section	.text._ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEEEdRT_,"axG",@progbits,_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEEEdRT_,comdat
	.align	2
	.weak	_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEEEdRT_
$LFB4217 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEEEdRT_
	.type	_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEEEdRT_, @function
_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEEEdRT_:
	.frame	$fp,32,$31		# vars= 0, regs= 2/0, args= 16, gp= 8
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-32
	.cfi_def_cfa_offset 32
	sw	$31,28($sp)
	sw	$fp,24($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,32($fp)
	sw	$5,36($fp)
	lw	$2,32($fp)
	move	$6,$2
	lw	$5,36($fp)
	lw	$4,32($fp)
	.option	pic0
	jal	_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEEEdRT_RKNS0_10param_typeE
	nop

	.option	pic2
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,28($sp)
	lw	$fp,24($sp)
	addiu	$sp,$sp,32
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEEEdRT_
	.cfi_endproc
$LFE4217:
	.size	_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEEEdRT_, .-_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEEEdRT_
	.section	.text._ZNSt10shared_ptrI8materialEC2Ev,"axG",@progbits,_ZNSt10shared_ptrI8materialEC5Ev,comdat
	.align	2
	.weak	_ZNSt10shared_ptrI8materialEC2Ev
$LFB4245 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZNSt10shared_ptrI8materialEC2Ev
	.type	_ZNSt10shared_ptrI8materialEC2Ev, @function
_ZNSt10shared_ptrI8materialEC2Ev:
	.frame	$fp,32,$31		# vars= 0, regs= 2/0, args= 16, gp= 8
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-32
	.cfi_def_cfa_offset 32
	sw	$31,28($sp)
	sw	$fp,24($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,32($fp)
	lw	$2,32($fp)
	move	$4,$2
	.option	pic0
	jal	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC2Ev
	nop

	.option	pic2
	nop
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,28($sp)
	lw	$fp,24($sp)
	addiu	$sp,$sp,32
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZNSt10shared_ptrI8materialEC2Ev
	.cfi_endproc
$LFE4245:
	.size	_ZNSt10shared_ptrI8materialEC2Ev, .-_ZNSt10shared_ptrI8materialEC2Ev
	.weak	_ZNSt10shared_ptrI8materialEC1Ev
	_ZNSt10shared_ptrI8materialEC1Ev = _ZNSt10shared_ptrI8materialEC2Ev
	.section	.text._ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align	2
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED2Ev
$LFB4248 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED2Ev
	.type	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED2Ev:
	.frame	$fp,32,$31		# vars= 0, regs= 2/0, args= 16, gp= 8
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-32
	.cfi_def_cfa_offset 32
	sw	$31,28($sp)
	sw	$fp,24($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,32($fp)
	lw	$2,32($fp)
	lw	$2,0($2)
	beq	$2,$0,$L91
	nop

	lw	$2,32($fp)
	lw	$2,0($2)
	move	$4,$2
	.option	pic0
	jal	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv
	nop

	.option	pic2
$L91:
	nop
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,28($sp)
	lw	$fp,24($sp)
	addiu	$sp,$sp,32
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED2Ev
	.cfi_endproc
$LFE4248:
	.size	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED1Ev
	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED1Ev = _ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED2Ev,"axG",@progbits,_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED5Ev,comdat
	.align	2
	.weak	_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED2Ev
$LFB4251 = .
	.cfi_startproc
	.cfi_personality 0x80,DW.ref.__gxx_personality_v0
	.cfi_lsda 0,$LLSDA4251
	.set	nomips16
	.set	nomicromips
	.ent	_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED2Ev
	.type	_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED2Ev, @function
_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED2Ev:
	.frame	$fp,40,$31		# vars= 0, regs= 4/0, args= 16, gp= 8
	.mask	0xc0030000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-40
	.cfi_def_cfa_offset 40
	sw	$31,36($sp)
	sw	$fp,32($sp)
	sw	$17,28($sp)
	sw	$16,24($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	.cfi_offset 17, -12
	.cfi_offset 16, -16
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,40($fp)
	lw	$2,40($fp)
	lw	$16,0($2)
	lw	$2,40($fp)
	lw	$17,4($2)
	lw	$2,40($fp)
	move	$4,$2
	.option	pic0
	jal	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE19_M_get_Tp_allocatorEv
	nop

	.option	pic2
	move	$6,$2
	move	$5,$17
	move	$4,$16
	.option	pic0
	jal	_ZSt8_DestroyIPSt10shared_ptrI8hittableES2_EvT_S4_RSaIT0_E
	nop

	.option	pic2
	lw	$2,40($fp)
	move	$4,$2
	.option	pic0
	jal	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED2Ev
	nop

	.option	pic2
	nop
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,36($sp)
	lw	$fp,32($sp)
	lw	$17,28($sp)
	lw	$16,24($sp)
	addiu	$sp,$sp,40
	.cfi_restore 16
	.cfi_restore 17
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED2Ev
	.cfi_endproc
$LFE4251:
	.section	.gcc_except_table
$LLSDA4251:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 $LLSDACSE4251-$LLSDACSB4251
$LLSDACSB4251:
$LLSDACSE4251:
	.section	.text._ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED2Ev,"axG",@progbits,_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED5Ev,comdat
	.size	_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED2Ev, .-_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED2Ev
	.weak	_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED1Ev
	_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED1Ev = _ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED2Ev
	.section	.text._ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_,"axG",@progbits,_ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_,comdat
	.align	2
	.weak	_ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_
$LFB4253 = .
	.cfi_startproc
	.cfi_personality 0x80,DW.ref.__gxx_personality_v0
	.cfi_lsda 0,$LLSDA4253
	.set	nomips16
	.set	nomicromips
	.ent	_ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_
	.type	_ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_, @function
_ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_:
	.frame	$fp,64,$31		# vars= 24, regs= 3/0, args= 16, gp= 8
	.mask	0xc0010000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-64
	.cfi_def_cfa_offset 64
	sw	$31,60($sp)
	sw	$fp,56($sp)
	sw	$16,52($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	.cfi_offset 16, -12
	move	$fp,$sp
	.cfi_def_cfa_register 30
	lui	$28,%hi(__gnu_local_gp)
	addiu	$28,$28,%lo(__gnu_local_gp)
	.cprestore	16
	sw	$4,36($fp)
	sw	$5,32($fp)
	sw	$6,28($fp)
	lw	$2,%got(__stack_chk_guard)($28)
	lw	$2,0($2)
	sw	$2,44($fp)
	addiu	$2,$fp,40
	move	$4,$2
	.option	pic0
	jal	_ZNSaI6sphereEC1Ev
	nop

	.option	pic2
	lw	$28,16($fp)
	lw	$4,32($fp)
	.option	pic0
	jal	_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
	nop

	.option	pic2
	lw	$28,16($fp)
	move	$16,$2
	lw	$4,28($fp)
	.option	pic0
	jal	_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE
	nop

	.option	pic2
	lw	$28,16($fp)
	move	$3,$2
	lw	$2,36($fp)
	addiu	$4,$fp,40
	move	$7,$3
	move	$6,$16
	move	$5,$4
	move	$4,$2
$LEHB16 = .
	.option	pic0
	jal	_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3dEESt10shared_ptrIT_ERKT0_DpOT1_
	nop

	.option	pic2
$LEHE16 = .
	lw	$28,16($fp)
	addiu	$2,$fp,40
	move	$4,$2
	.option	pic0
	jal	_ZNSaI6sphereED1Ev
	nop

	.option	pic2
	lw	$28,16($fp)
	lw	$2,%got(__stack_chk_guard)($28)
	lw	$3,44($fp)
	lw	$2,0($2)
	beq	$3,$2,$L96
	nop

	.option	pic0
	b	$L98
	nop

	.option	pic2
$L97:
	lw	$28,16($fp)
	move	$16,$4
	addiu	$2,$fp,40
	move	$4,$2
	.option	pic0
	jal	_ZNSaI6sphereED1Ev
	nop

	.option	pic2
	lw	$28,16($fp)
	move	$2,$16
	move	$4,$2
	lw	$2,%call16(_Unwind_Resume)($28)
	move	$25,$2
$LEHB17 = .
	.reloc	1f,R_MIPS_JALR,_Unwind_Resume
1:	jalr	$25
	nop

$LEHE17 = .
$L98:
	lw	$2,%call16(__stack_chk_fail)($28)
	move	$25,$2
	.reloc	1f,R_MIPS_JALR,__stack_chk_fail
1:	jalr	$25
	nop

$L96:
	lw	$2,36($fp)
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,60($sp)
	lw	$fp,56($sp)
	lw	$16,52($sp)
	addiu	$sp,$sp,64
	.cfi_restore 16
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_
	.cfi_endproc
$LFE4253:
	.section	.gcc_except_table
$LLSDA4253:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 $LLSDACSE4253-$LLSDACSB4253
$LLSDACSB4253:
	.uleb128 $LEHB16-$LFB4253
	.uleb128 $LEHE16-$LEHB16
	.uleb128 $L97-$LFB4253
	.uleb128 0
	.uleb128 $LEHB17-$LFB4253
	.uleb128 $LEHE17-$LEHB17
	.uleb128 0
	.uleb128 0
$LLSDACSE4253:
	.section	.text._ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_,"axG",@progbits,_ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_,comdat
	.size	_ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_, .-_ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_
	.section	.text._ZNSt10shared_ptrI8hittableEC2I6spherevEEOS_IT_E,"axG",@progbits,_ZNSt10shared_ptrI8hittableEC5I6spherevEEOS_IT_E,comdat
	.align	2
	.weak	_ZNSt10shared_ptrI8hittableEC2I6spherevEEOS_IT_E
$LFB4255 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZNSt10shared_ptrI8hittableEC2I6spherevEEOS_IT_E
	.type	_ZNSt10shared_ptrI8hittableEC2I6spherevEEOS_IT_E, @function
_ZNSt10shared_ptrI8hittableEC2I6spherevEEOS_IT_E:
	.frame	$fp,40,$31		# vars= 0, regs= 3/0, args= 16, gp= 8
	.mask	0xc0010000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-40
	.cfi_def_cfa_offset 40
	sw	$31,36($sp)
	sw	$fp,32($sp)
	sw	$16,28($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	.cfi_offset 16, -12
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,40($fp)
	sw	$5,44($fp)
	lw	$16,40($fp)
	lw	$4,44($fp)
	.option	pic0
	jal	_ZSt4moveIRSt10shared_ptrI6sphereEEONSt16remove_referenceIT_E4typeEOS5_
	nop

	.option	pic2
	move	$5,$2
	move	$4,$16
	.option	pic0
	jal	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC2I6spherevEEOS_IT_LS2_2EE
	nop

	.option	pic2
	nop
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,36($sp)
	lw	$fp,32($sp)
	lw	$16,28($sp)
	addiu	$sp,$sp,40
	.cfi_restore 16
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZNSt10shared_ptrI8hittableEC2I6spherevEEOS_IT_E
	.cfi_endproc
$LFE4255:
	.size	_ZNSt10shared_ptrI8hittableEC2I6spherevEEOS_IT_E, .-_ZNSt10shared_ptrI8hittableEC2I6spherevEEOS_IT_E
	.weak	_ZNSt10shared_ptrI8hittableEC1I6spherevEEOS_IT_E
	_ZNSt10shared_ptrI8hittableEC1I6spherevEEOS_IT_E = _ZNSt10shared_ptrI8hittableEC2I6spherevEEOS_IT_E
	.section	.text._ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_,"axG",@progbits,_ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_,comdat
	.align	2
	.weak	_ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_
$LFB4260 = .
	.cfi_startproc
	.cfi_personality 0x80,DW.ref.__gxx_personality_v0
	.cfi_lsda 0,$LLSDA4260
	.set	nomips16
	.set	nomicromips
	.ent	_ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_
	.type	_ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_, @function
_ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_:
	.frame	$fp,64,$31		# vars= 24, regs= 3/0, args= 16, gp= 8
	.mask	0xc0010000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-64
	.cfi_def_cfa_offset 64
	sw	$31,60($sp)
	sw	$fp,56($sp)
	sw	$16,52($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	.cfi_offset 16, -12
	move	$fp,$sp
	.cfi_def_cfa_register 30
	lui	$28,%hi(__gnu_local_gp)
	addiu	$28,$28,%lo(__gnu_local_gp)
	.cprestore	16
	sw	$4,36($fp)
	sw	$5,32($fp)
	sw	$6,28($fp)
	lw	$2,%got(__stack_chk_guard)($28)
	lw	$2,0($2)
	sw	$2,44($fp)
	addiu	$2,$fp,40
	move	$4,$2
	.option	pic0
	jal	_ZNSaI6sphereEC1Ev
	nop

	.option	pic2
	lw	$28,16($fp)
	lw	$4,32($fp)
	.option	pic0
	jal	_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
	nop

	.option	pic2
	lw	$28,16($fp)
	move	$16,$2
	lw	$4,28($fp)
	.option	pic0
	jal	_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE
	nop

	.option	pic2
	lw	$28,16($fp)
	move	$3,$2
	lw	$2,36($fp)
	addiu	$4,$fp,40
	move	$7,$3
	move	$6,$16
	move	$5,$4
	move	$4,$2
$LEHB18 = .
	.option	pic0
	jal	_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3iEESt10shared_ptrIT_ERKT0_DpOT1_
	nop

	.option	pic2
$LEHE18 = .
	lw	$28,16($fp)
	addiu	$2,$fp,40
	move	$4,$2
	.option	pic0
	jal	_ZNSaI6sphereED1Ev
	nop

	.option	pic2
	lw	$28,16($fp)
	lw	$2,%got(__stack_chk_guard)($28)
	lw	$3,44($fp)
	lw	$2,0($2)
	beq	$3,$2,$L103
	nop

	.option	pic0
	b	$L105
	nop

	.option	pic2
$L104:
	lw	$28,16($fp)
	move	$16,$4
	addiu	$2,$fp,40
	move	$4,$2
	.option	pic0
	jal	_ZNSaI6sphereED1Ev
	nop

	.option	pic2
	lw	$28,16($fp)
	move	$2,$16
	move	$4,$2
	lw	$2,%call16(_Unwind_Resume)($28)
	move	$25,$2
$LEHB19 = .
	.reloc	1f,R_MIPS_JALR,_Unwind_Resume
1:	jalr	$25
	nop

$LEHE19 = .
$L105:
	lw	$2,%call16(__stack_chk_fail)($28)
	move	$25,$2
	.reloc	1f,R_MIPS_JALR,__stack_chk_fail
1:	jalr	$25
	nop

$L103:
	lw	$2,36($fp)
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,60($sp)
	lw	$fp,56($sp)
	lw	$16,52($sp)
	addiu	$sp,$sp,64
	.cfi_restore 16
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_
	.cfi_endproc
$LFE4260:
	.section	.gcc_except_table
$LLSDA4260:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 $LLSDACSE4260-$LLSDACSB4260
$LLSDACSB4260:
	.uleb128 $LEHB18-$LFB4260
	.uleb128 $LEHE18-$LEHB18
	.uleb128 $L104-$LFB4260
	.uleb128 0
	.uleb128 $LEHB19-$LFB4260
	.uleb128 $LEHE19-$LEHB19
	.uleb128 0
	.uleb128 0
$LLSDACSE4260:
	.section	.text._ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_,"axG",@progbits,_ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_,comdat
	.size	_ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_, .-_ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_
	.section	.text._ZNSt25uniform_real_distributionIdE10param_typeC2Edd,"axG",@progbits,_ZNSt25uniform_real_distributionIdE10param_typeC5Edd,comdat
	.align	2
	.weak	_ZNSt25uniform_real_distributionIdE10param_typeC2Edd
$LFB4387 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZNSt25uniform_real_distributionIdE10param_typeC2Edd
	.type	_ZNSt25uniform_real_distributionIdE10param_typeC2Edd, @function
_ZNSt25uniform_real_distributionIdE10param_typeC2Edd:
	.frame	$fp,8,$31		# vars= 0, regs= 1/0, args= 0, gp= 0
	.mask	0x40000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-8
	.cfi_def_cfa_offset 8
	sw	$fp,4($sp)
	.cfi_offset 30, -4
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,8($fp)
	sw	$7,20($fp)
	sw	$6,16($fp)
	lw	$2,8($fp)
	ldc1	$f0,16($fp)
	sdc1	$f0,0($2)
	lw	$2,8($fp)
	ldc1	$f0,24($fp)
	sdc1	$f0,8($2)
	nop
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$fp,4($sp)
	addiu	$sp,$sp,8
	.cfi_restore 30
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZNSt25uniform_real_distributionIdE10param_typeC2Edd
	.cfi_endproc
$LFE4387:
	.size	_ZNSt25uniform_real_distributionIdE10param_typeC2Edd, .-_ZNSt25uniform_real_distributionIdE10param_typeC2Edd
	.weak	_ZNSt25uniform_real_distributionIdE10param_typeC1Edd
	_ZNSt25uniform_real_distributionIdE10param_typeC1Edd = _ZNSt25uniform_real_distributionIdE10param_typeC2Edd
	.section	.text._ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC2Ej,"axG",@progbits,_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC5Ej,comdat
	.align	2
	.weak	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC2Ej
$LFB4390 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC2Ej
	.type	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC2Ej, @function
_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC2Ej:
	.frame	$fp,32,$31		# vars= 0, regs= 2/0, args= 16, gp= 8
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-32
	.cfi_def_cfa_offset 32
	sw	$31,28($sp)
	sw	$fp,24($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,32($fp)
	sw	$5,36($fp)
	lw	$5,36($fp)
	lw	$4,32($fp)
	.option	pic0
	jal	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE4seedEj
	nop

	.option	pic2
	nop
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,28($sp)
	lw	$fp,24($sp)
	addiu	$sp,$sp,32
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC2Ej
	.cfi_endproc
$LFE4390:
	.size	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC2Ej, .-_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC2Ej
	.weak	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC1Ej
	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC1Ej = _ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC2Ej
	.section	.text._ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEEEdRT_RKNS0_10param_typeE,"axG",@progbits,_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEEEdRT_RKNS0_10param_typeE,comdat
	.align	2
	.weak	_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEEEdRT_RKNS0_10param_typeE
$LFB4392 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEEEdRT_RKNS0_10param_typeE
	.type	_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEEEdRT_RKNS0_10param_typeE, @function
_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEEEdRT_RKNS0_10param_typeE:
	.frame	$fp,72,$31		# vars= 24, regs= 2/4, args= 16, gp= 8
	.mask	0xc0000000,-20
	.fmask	0x00f00000,-8
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-72
	.cfi_def_cfa_offset 72
	sw	$31,52($sp)
	sw	$fp,48($sp)
	sdc1	$f22,64($sp)
	sdc1	$f20,56($sp)
	.cfi_offset 31, -20
	.cfi_offset 30, -24
	.cfi_offset 54, -4
	.cfi_offset 55, -8
	.cfi_offset 52, -12
	.cfi_offset 53, -16
	move	$fp,$sp
	.cfi_def_cfa_register 30
	lui	$28,%hi(__gnu_local_gp)
	addiu	$28,$28,%lo(__gnu_local_gp)
	.cprestore	16
	sw	$4,36($fp)
	sw	$5,32($fp)
	sw	$6,28($fp)
	lw	$2,%got(__stack_chk_guard)($28)
	lw	$2,0($2)
	sw	$2,44($fp)
	addiu	$2,$fp,40
	lw	$5,32($fp)
	move	$4,$2
	.option	pic0
	jal	_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEdEC1ERS2_
	nop

	.option	pic2
	lw	$28,16($fp)
	addiu	$2,$fp,40
	move	$4,$2
	.option	pic0
	jal	_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEdEclEv
	nop

	.option	pic2
	lw	$28,16($fp)
	mov.d	$f20,$f0
	lw	$4,28($fp)
	.option	pic0
	jal	_ZNKSt25uniform_real_distributionIdE10param_type1bEv
	nop

	.option	pic2
	lw	$28,16($fp)
	mov.d	$f22,$f0
	lw	$4,28($fp)
	.option	pic0
	jal	_ZNKSt25uniform_real_distributionIdE10param_type1aEv
	nop

	.option	pic2
	lw	$28,16($fp)
	sub.d	$f0,$f22,$f0
	mul.d	$f20,$f20,$f0
	lw	$4,28($fp)
	.option	pic0
	jal	_ZNKSt25uniform_real_distributionIdE10param_type1aEv
	nop

	.option	pic2
	lw	$28,16($fp)
	add.d	$f0,$f20,$f0
	lw	$2,%got(__stack_chk_guard)($28)
	lw	$3,44($fp)
	lw	$2,0($2)
	beq	$3,$2,$L110
	nop

	lw	$2,%call16(__stack_chk_fail)($28)
	move	$25,$2
	.reloc	1f,R_MIPS_JALR,__stack_chk_fail
1:	jalr	$25
	nop

$L110:
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,52($sp)
	lw	$fp,48($sp)
	ldc1	$f22,64($sp)
	ldc1	$f20,56($sp)
	addiu	$sp,$sp,72
	.cfi_restore 52
	.cfi_restore 53
	.cfi_restore 54
	.cfi_restore 55
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEEEdRT_RKNS0_10param_typeE
	.cfi_endproc
$LFE4392:
	.size	_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEEEdRT_RKNS0_10param_typeE, .-_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEEEdRT_RKNS0_10param_typeE
	.section	.text._ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC2Ev,"axG",@progbits,_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC5Ev,comdat
	.align	2
	.weak	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC2Ev
$LFB4404 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC2Ev
	.type	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC2Ev, @function
_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC2Ev:
	.frame	$fp,32,$31		# vars= 0, regs= 2/0, args= 16, gp= 8
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-32
	.cfi_def_cfa_offset 32
	sw	$31,28($sp)
	sw	$fp,24($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,32($fp)
	lw	$2,32($fp)
	sw	$0,0($2)
	lw	$2,32($fp)
	addiu	$2,$2,4
	move	$4,$2
	.option	pic0
	jal	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1Ev
	nop

	.option	pic2
	nop
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,28($sp)
	lw	$fp,24($sp)
	addiu	$sp,$sp,32
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC2Ev
	.cfi_endproc
$LFE4404:
	.size	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC2Ev, .-_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC2Ev
	.weak	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC1Ev
	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC1Ev = _ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC2Ev
	.section	.text._ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv,"axG",@progbits,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv,comdat
	.align	2
	.weak	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv
$LFB4406 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv
	.type	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv, @function
_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv:
	.frame	$fp,32,$31		# vars= 0, regs= 2/0, args= 16, gp= 8
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-32
	.cfi_def_cfa_offset 32
	sw	$31,28($sp)
	sw	$fp,24($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,32($fp)
	lw	$2,32($fp)
	addiu	$2,$2,4
	li	$5,-1			# 0xffffffffffffffff
	move	$4,$2
	.option	pic0
	jal	_ZN9__gnu_cxxL27__exchange_and_add_dispatchEPii
	nop

	.option	pic2
	xori	$2,$2,0x1
	sltu	$2,$2,1
	andi	$2,$2,0x00ff
	beq	$2,$0,$L114
	nop

	lw	$2,32($fp)
	lw	$2,0($2)
	addiu	$2,$2,8
	lw	$2,0($2)
	lw	$4,32($fp)
	move	$25,$2
	jalr	$25
	nop

	lw	$2,32($fp)
	addiu	$2,$2,8
	li	$5,-1			# 0xffffffffffffffff
	move	$4,$2
	.option	pic0
	jal	_ZN9__gnu_cxxL27__exchange_and_add_dispatchEPii
	nop

	.option	pic2
	xori	$2,$2,0x1
	sltu	$2,$2,1
	andi	$2,$2,0x00ff
	beq	$2,$0,$L114
	nop

	lw	$2,32($fp)
	lw	$2,0($2)
	addiu	$2,$2,12
	lw	$2,0($2)
	lw	$4,32($fp)
	move	$25,$2
	jalr	$25
	nop

$L114:
	nop
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,28($sp)
	lw	$fp,24($sp)
	addiu	$sp,$sp,32
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv
	.cfi_endproc
$LFE4406:
	.size	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv, .-_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv
	.section	.text._ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD2Ev,"axG",@progbits,_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD5Ev,comdat
	.align	2
	.weak	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD2Ev
$LFB4409 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD2Ev
	.type	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD2Ev, @function
_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD2Ev:
	.frame	$fp,32,$31		# vars= 0, regs= 2/0, args= 16, gp= 8
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-32
	.cfi_def_cfa_offset 32
	sw	$31,28($sp)
	sw	$fp,24($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,32($fp)
	lw	$4,32($fp)
	.option	pic0
	jal	_ZNSaISt10shared_ptrI8hittableEED2Ev
	nop

	.option	pic2
	nop
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,28($sp)
	lw	$fp,24($sp)
	addiu	$sp,$sp,32
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD2Ev
	.cfi_endproc
$LFE4409:
	.size	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD2Ev, .-_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD2Ev
	.weak	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD1Ev
	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD1Ev = _ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD2Ev
	.section	.text._ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED2Ev,"axG",@progbits,_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED5Ev,comdat
	.align	2
	.weak	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED2Ev
$LFB4411 = .
	.cfi_startproc
	.cfi_personality 0x80,DW.ref.__gxx_personality_v0
	.cfi_lsda 0,$LLSDA4411
	.set	nomips16
	.set	nomicromips
	.ent	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED2Ev
	.type	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED2Ev, @function
_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED2Ev:
	.frame	$fp,32,$31		# vars= 0, regs= 2/0, args= 16, gp= 8
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-32
	.cfi_def_cfa_offset 32
	sw	$31,28($sp)
	sw	$fp,24($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,32($fp)
	lw	$2,32($fp)
	lw	$4,0($2)
	lw	$2,32($fp)
	lw	$3,8($2)
	lw	$2,32($fp)
	lw	$2,0($2)
	subu	$2,$3,$2
	sra	$2,$2,3
	move	$6,$2
	move	$5,$4
	lw	$4,32($fp)
	.option	pic0
	jal	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE13_M_deallocateEPS2_j
	nop

	.option	pic2
	lw	$2,32($fp)
	move	$4,$2
	.option	pic0
	jal	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD1Ev
	nop

	.option	pic2
	nop
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,28($sp)
	lw	$fp,24($sp)
	addiu	$sp,$sp,32
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED2Ev
	.cfi_endproc
$LFE4411:
	.section	.gcc_except_table
$LLSDA4411:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 $LLSDACSE4411-$LLSDACSB4411
$LLSDACSB4411:
$LLSDACSE4411:
	.section	.text._ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED2Ev,"axG",@progbits,_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED5Ev,comdat
	.size	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED2Ev, .-_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED2Ev
	.weak	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED1Ev
	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED1Ev = _ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED2Ev
	.section	.text._ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE19_M_get_Tp_allocatorEv,"axG",@progbits,_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE19_M_get_Tp_allocatorEv,comdat
	.align	2
	.weak	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE19_M_get_Tp_allocatorEv
$LFB4413 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE19_M_get_Tp_allocatorEv
	.type	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE19_M_get_Tp_allocatorEv, @function
_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE19_M_get_Tp_allocatorEv:
	.frame	$fp,8,$31		# vars= 0, regs= 1/0, args= 0, gp= 0
	.mask	0x40000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-8
	.cfi_def_cfa_offset 8
	sw	$fp,4($sp)
	.cfi_offset 30, -4
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,8($fp)
	lw	$2,8($fp)
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$fp,4($sp)
	addiu	$sp,$sp,8
	.cfi_restore 30
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE19_M_get_Tp_allocatorEv
	.cfi_endproc
$LFE4413:
	.size	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE19_M_get_Tp_allocatorEv, .-_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE19_M_get_Tp_allocatorEv
	.section	.text._ZSt8_DestroyIPSt10shared_ptrI8hittableES2_EvT_S4_RSaIT0_E,"axG",@progbits,_ZSt8_DestroyIPSt10shared_ptrI8hittableES2_EvT_S4_RSaIT0_E,comdat
	.align	2
	.weak	_ZSt8_DestroyIPSt10shared_ptrI8hittableES2_EvT_S4_RSaIT0_E
$LFB4414 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZSt8_DestroyIPSt10shared_ptrI8hittableES2_EvT_S4_RSaIT0_E
	.type	_ZSt8_DestroyIPSt10shared_ptrI8hittableES2_EvT_S4_RSaIT0_E, @function
_ZSt8_DestroyIPSt10shared_ptrI8hittableES2_EvT_S4_RSaIT0_E:
	.frame	$fp,32,$31		# vars= 0, regs= 2/0, args= 16, gp= 8
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-32
	.cfi_def_cfa_offset 32
	sw	$31,28($sp)
	sw	$fp,24($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,32($fp)
	sw	$5,36($fp)
	sw	$6,40($fp)
	lw	$5,36($fp)
	lw	$4,32($fp)
	.option	pic0
	jal	_ZSt8_DestroyIPSt10shared_ptrI8hittableEEvT_S4_
	nop

	.option	pic2
	nop
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,28($sp)
	lw	$fp,24($sp)
	addiu	$sp,$sp,32
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZSt8_DestroyIPSt10shared_ptrI8hittableES2_EvT_S4_RSaIT0_E
	.cfi_endproc
$LFE4414:
	.size	_ZSt8_DestroyIPSt10shared_ptrI8hittableES2_EvT_S4_RSaIT0_E, .-_ZSt8_DestroyIPSt10shared_ptrI8hittableES2_EvT_S4_RSaIT0_E
	.section	.text._ZNSaI6sphereEC2Ev,"axG",@progbits,_ZNSaI6sphereEC5Ev,comdat
	.align	2
	.weak	_ZNSaI6sphereEC2Ev
$LFB4416 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZNSaI6sphereEC2Ev
	.type	_ZNSaI6sphereEC2Ev, @function
_ZNSaI6sphereEC2Ev:
	.frame	$fp,32,$31		# vars= 0, regs= 2/0, args= 16, gp= 8
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-32
	.cfi_def_cfa_offset 32
	sw	$31,28($sp)
	sw	$fp,24($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,32($fp)
	lw	$4,32($fp)
	.option	pic0
	jal	_ZN9__gnu_cxx13new_allocatorI6sphereEC2Ev
	nop

	.option	pic2
	nop
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,28($sp)
	lw	$fp,24($sp)
	addiu	$sp,$sp,32
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZNSaI6sphereEC2Ev
	.cfi_endproc
$LFE4416:
	.size	_ZNSaI6sphereEC2Ev, .-_ZNSaI6sphereEC2Ev
	.weak	_ZNSaI6sphereEC1Ev
	_ZNSaI6sphereEC1Ev = _ZNSaI6sphereEC2Ev
	.section	.text._ZNSaI6sphereED2Ev,"axG",@progbits,_ZNSaI6sphereED5Ev,comdat
	.align	2
	.weak	_ZNSaI6sphereED2Ev
$LFB4419 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZNSaI6sphereED2Ev
	.type	_ZNSaI6sphereED2Ev, @function
_ZNSaI6sphereED2Ev:
	.frame	$fp,32,$31		# vars= 0, regs= 2/0, args= 16, gp= 8
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-32
	.cfi_def_cfa_offset 32
	sw	$31,28($sp)
	sw	$fp,24($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,32($fp)
	lw	$4,32($fp)
	.option	pic0
	jal	_ZN9__gnu_cxx13new_allocatorI6sphereED2Ev
	nop

	.option	pic2
	nop
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,28($sp)
	lw	$fp,24($sp)
	addiu	$sp,$sp,32
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZNSaI6sphereED2Ev
	.cfi_endproc
$LFE4419:
	.size	_ZNSaI6sphereED2Ev, .-_ZNSaI6sphereED2Ev
	.weak	_ZNSaI6sphereED1Ev
	_ZNSaI6sphereED1Ev = _ZNSaI6sphereED2Ev
	.section	.text._ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE,"axG",@progbits,_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE,comdat
	.align	2
	.weak	_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
$LFB4421 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
	.type	_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE, @function
_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE:
	.frame	$fp,8,$31		# vars= 0, regs= 1/0, args= 0, gp= 0
	.mask	0x40000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-8
	.cfi_def_cfa_offset 8
	sw	$fp,4($sp)
	.cfi_offset 30, -4
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,8($fp)
	lw	$2,8($fp)
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$fp,4($sp)
	addiu	$sp,$sp,8
	.cfi_restore 30
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
	.cfi_endproc
$LFE4421:
	.size	_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE, .-_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
	.section	.text._ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE,"axG",@progbits,_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE,comdat
	.align	2
	.weak	_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE
$LFB4422 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE
	.type	_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE, @function
_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE:
	.frame	$fp,8,$31		# vars= 0, regs= 1/0, args= 0, gp= 0
	.mask	0x40000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-8
	.cfi_def_cfa_offset 8
	sw	$fp,4($sp)
	.cfi_offset 30, -4
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,8($fp)
	lw	$2,8($fp)
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$fp,4($sp)
	addiu	$sp,$sp,8
	.cfi_restore 30
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE
	.cfi_endproc
$LFE4422:
	.size	_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE, .-_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE
	.section	.text._ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3dEESt10shared_ptrIT_ERKT0_DpOT1_,"axG",@progbits,_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3dEESt10shared_ptrIT_ERKT0_DpOT1_,comdat
	.align	2
	.weak	_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3dEESt10shared_ptrIT_ERKT0_DpOT1_
$LFB4423 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3dEESt10shared_ptrIT_ERKT0_DpOT1_
	.type	_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3dEESt10shared_ptrIT_ERKT0_DpOT1_, @function
_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3dEESt10shared_ptrIT_ERKT0_DpOT1_:
	.frame	$fp,40,$31		# vars= 0, regs= 4/0, args= 16, gp= 8
	.mask	0xc0030000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-40
	.cfi_def_cfa_offset 40
	sw	$31,36($sp)
	sw	$fp,32($sp)
	sw	$17,28($sp)
	sw	$16,24($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	.cfi_offset 17, -12
	.cfi_offset 16, -16
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,40($fp)
	sw	$5,44($fp)
	sw	$6,48($fp)
	sw	$7,52($fp)
	lw	$16,44($fp)
	lw	$4,48($fp)
	.option	pic0
	jal	_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
	nop

	.option	pic2
	move	$17,$2
	lw	$4,52($fp)
	.option	pic0
	jal	_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE
	nop

	.option	pic2
	move	$7,$2
	move	$6,$17
	move	$5,$16
	lw	$4,40($fp)
	.option	pic0
	jal	_ZNSt10shared_ptrI6sphereEC1ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	nop

	.option	pic2
	lw	$2,40($fp)
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,36($sp)
	lw	$fp,32($sp)
	lw	$17,28($sp)
	lw	$16,24($sp)
	addiu	$sp,$sp,40
	.cfi_restore 16
	.cfi_restore 17
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3dEESt10shared_ptrIT_ERKT0_DpOT1_
	.cfi_endproc
$LFE4423:
	.size	_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3dEESt10shared_ptrIT_ERKT0_DpOT1_, .-_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3dEESt10shared_ptrIT_ERKT0_DpOT1_
	.section	.text._ZSt4moveIRSt10shared_ptrI6sphereEEONSt16remove_referenceIT_E4typeEOS5_,"axG",@progbits,_ZSt4moveIRSt10shared_ptrI6sphereEEONSt16remove_referenceIT_E4typeEOS5_,comdat
	.align	2
	.weak	_ZSt4moveIRSt10shared_ptrI6sphereEEONSt16remove_referenceIT_E4typeEOS5_
$LFB4427 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZSt4moveIRSt10shared_ptrI6sphereEEONSt16remove_referenceIT_E4typeEOS5_
	.type	_ZSt4moveIRSt10shared_ptrI6sphereEEONSt16remove_referenceIT_E4typeEOS5_, @function
_ZSt4moveIRSt10shared_ptrI6sphereEEONSt16remove_referenceIT_E4typeEOS5_:
	.frame	$fp,8,$31		# vars= 0, regs= 1/0, args= 0, gp= 0
	.mask	0x40000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-8
	.cfi_def_cfa_offset 8
	sw	$fp,4($sp)
	.cfi_offset 30, -4
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,8($fp)
	lw	$2,8($fp)
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$fp,4($sp)
	addiu	$sp,$sp,8
	.cfi_restore 30
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZSt4moveIRSt10shared_ptrI6sphereEEONSt16remove_referenceIT_E4typeEOS5_
	.cfi_endproc
$LFE4427:
	.size	_ZSt4moveIRSt10shared_ptrI6sphereEEONSt16remove_referenceIT_E4typeEOS5_, .-_ZSt4moveIRSt10shared_ptrI6sphereEEONSt16remove_referenceIT_E4typeEOS5_
	.section	.text._ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC2I6spherevEEOS_IT_LS2_2EE,"axG",@progbits,_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC5I6spherevEEOS_IT_LS2_2EE,comdat
	.align	2
	.weak	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC2I6spherevEEOS_IT_LS2_2EE
$LFB4429 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC2I6spherevEEOS_IT_LS2_2EE
	.type	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC2I6spherevEEOS_IT_LS2_2EE, @function
_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC2I6spherevEEOS_IT_LS2_2EE:
	.frame	$fp,32,$31		# vars= 0, regs= 2/0, args= 16, gp= 8
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-32
	.cfi_def_cfa_offset 32
	sw	$31,28($sp)
	sw	$fp,24($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,32($fp)
	sw	$5,36($fp)
	lw	$2,36($fp)
	lw	$3,0($2)
	lw	$2,32($fp)
	sw	$3,0($2)
	lw	$2,32($fp)
	addiu	$2,$2,4
	move	$4,$2
	.option	pic0
	jal	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1Ev
	nop

	.option	pic2
	lw	$2,32($fp)
	addiu	$3,$2,4
	lw	$2,36($fp)
	addiu	$2,$2,4
	move	$5,$2
	move	$4,$3
	.option	pic0
	jal	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EE7_M_swapERS2_
	nop

	.option	pic2
	lw	$2,36($fp)
	sw	$0,0($2)
	nop
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,28($sp)
	lw	$fp,24($sp)
	addiu	$sp,$sp,32
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC2I6spherevEEOS_IT_LS2_2EE
	.cfi_endproc
$LFE4429:
	.size	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC2I6spherevEEOS_IT_LS2_2EE, .-_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC2I6spherevEEOS_IT_LS2_2EE
	.weak	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC1I6spherevEEOS_IT_LS2_2EE
	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC1I6spherevEEOS_IT_LS2_2EE = _ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC2I6spherevEEOS_IT_LS2_2EE
	.section	.text._ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE,"axG",@progbits,_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE,comdat
	.align	2
	.weak	_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE
$LFB4435 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE
	.type	_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE, @function
_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE:
	.frame	$fp,8,$31		# vars= 0, regs= 1/0, args= 0, gp= 0
	.mask	0x40000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-8
	.cfi_def_cfa_offset 8
	sw	$fp,4($sp)
	.cfi_offset 30, -4
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,8($fp)
	lw	$2,8($fp)
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$fp,4($sp)
	addiu	$sp,$sp,8
	.cfi_restore 30
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE
	.cfi_endproc
$LFE4435:
	.size	_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE, .-_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE
	.section	.text._ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3iEESt10shared_ptrIT_ERKT0_DpOT1_,"axG",@progbits,_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3iEESt10shared_ptrIT_ERKT0_DpOT1_,comdat
	.align	2
	.weak	_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3iEESt10shared_ptrIT_ERKT0_DpOT1_
$LFB4436 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3iEESt10shared_ptrIT_ERKT0_DpOT1_
	.type	_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3iEESt10shared_ptrIT_ERKT0_DpOT1_, @function
_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3iEESt10shared_ptrIT_ERKT0_DpOT1_:
	.frame	$fp,40,$31		# vars= 0, regs= 4/0, args= 16, gp= 8
	.mask	0xc0030000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-40
	.cfi_def_cfa_offset 40
	sw	$31,36($sp)
	sw	$fp,32($sp)
	sw	$17,28($sp)
	sw	$16,24($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	.cfi_offset 17, -12
	.cfi_offset 16, -16
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,40($fp)
	sw	$5,44($fp)
	sw	$6,48($fp)
	sw	$7,52($fp)
	lw	$16,44($fp)
	lw	$4,48($fp)
	.option	pic0
	jal	_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
	nop

	.option	pic2
	move	$17,$2
	lw	$4,52($fp)
	.option	pic0
	jal	_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE
	nop

	.option	pic2
	move	$7,$2
	move	$6,$17
	move	$5,$16
	lw	$4,40($fp)
	.option	pic0
	jal	_ZNSt10shared_ptrI6sphereEC1ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	nop

	.option	pic2
	lw	$2,40($fp)
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,36($sp)
	lw	$fp,32($sp)
	lw	$17,28($sp)
	lw	$16,24($sp)
	addiu	$sp,$sp,40
	.cfi_restore 16
	.cfi_restore 17
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3iEESt10shared_ptrIT_ERKT0_DpOT1_
	.cfi_endproc
$LFE4436:
	.size	_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3iEESt10shared_ptrIT_ERKT0_DpOT1_, .-_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3iEESt10shared_ptrIT_ERKT0_DpOT1_
	.section	.text._ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE4seedEj,"axG",@progbits,_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE4seedEj,comdat
	.align	2
	.weak	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE4seedEj
$LFB4491 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE4seedEj
	.type	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE4seedEj, @function
_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE4seedEj:
	.frame	$fp,40,$31		# vars= 8, regs= 2/0, args= 16, gp= 8
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-40
	.cfi_def_cfa_offset 40
	sw	$31,36($sp)
	sw	$fp,32($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,40($fp)
	sw	$5,44($fp)
	lw	$4,44($fp)
	.option	pic0
	jal	_ZNSt8__detail5__modIjLj0ELj1ELj0EEET_S1_
	nop

	.option	pic2
	move	$3,$2
	lw	$2,40($fp)
	sw	$3,0($2)
	li	$2,1			# 0x1
	sw	$2,24($fp)
$L137:
	lw	$2,24($fp)
	sltu	$2,$2,624
	beq	$2,$0,$L136
	nop

	lw	$2,24($fp)
	addiu	$2,$2,-1
	lw	$3,40($fp)
	sll	$2,$2,2
	addu	$2,$3,$2
	lw	$2,0($2)
	sw	$2,28($fp)
	lw	$2,28($fp)
	srl	$2,$2,30
	lw	$3,28($fp)
	xor	$2,$3,$2
	sw	$2,28($fp)
	lw	$3,28($fp)
	li	$2,1812398080			# 0x6c070000
	ori	$2,$2,0x8965
	mul	$2,$3,$2
	sw	$2,28($fp)
	lw	$4,24($fp)
	.option	pic0
	jal	_ZNSt8__detail5__modIjLj624ELj1ELj0EEET_S1_
	nop

	.option	pic2
	move	$3,$2
	lw	$2,28($fp)
	addu	$2,$2,$3
	sw	$2,28($fp)
	lw	$4,28($fp)
	.option	pic0
	jal	_ZNSt8__detail5__modIjLj0ELj1ELj0EEET_S1_
	nop

	.option	pic2
	move	$4,$2
	lw	$3,40($fp)
	lw	$2,24($fp)
	sll	$2,$2,2
	addu	$2,$3,$2
	sw	$4,0($2)
	lw	$2,24($fp)
	addiu	$2,$2,1
	sw	$2,24($fp)
	.option	pic0
	b	$L137
	nop

	.option	pic2
$L136:
	lw	$2,40($fp)
	li	$3,624			# 0x270
	sw	$3,2496($2)
	nop
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,36($sp)
	lw	$fp,32($sp)
	addiu	$sp,$sp,40
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE4seedEj
	.cfi_endproc
$LFE4491:
	.size	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE4seedEj, .-_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE4seedEj
	.section	.text._ZNSt8__detail8_AdaptorISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEdEC2ERS2_,"axG",@progbits,_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEdEC5ERS2_,comdat
	.align	2
	.weak	_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEdEC2ERS2_
$LFB4493 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEdEC2ERS2_
	.type	_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEdEC2ERS2_, @function
_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEdEC2ERS2_:
	.frame	$fp,8,$31		# vars= 0, regs= 1/0, args= 0, gp= 0
	.mask	0x40000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-8
	.cfi_def_cfa_offset 8
	sw	$fp,4($sp)
	.cfi_offset 30, -4
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,8($fp)
	sw	$5,12($fp)
	lw	$2,8($fp)
	lw	$3,12($fp)
	sw	$3,0($2)
	nop
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$fp,4($sp)
	addiu	$sp,$sp,8
	.cfi_restore 30
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEdEC2ERS2_
	.cfi_endproc
$LFE4493:
	.size	_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEdEC2ERS2_, .-_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEdEC2ERS2_
	.weak	_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEdEC1ERS2_
	_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEdEC1ERS2_ = _ZNSt8__detail8_AdaptorISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEdEC2ERS2_
	.section	.text._ZNSt8__detail8_AdaptorISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEdEclEv,"axG",@progbits,_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEdEclEv,comdat
	.align	2
	.weak	_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEdEclEv
$LFB4495 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEdEclEv
	.type	_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEdEclEv, @function
_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEdEclEv:
	.frame	$fp,32,$31		# vars= 0, regs= 2/0, args= 16, gp= 8
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-32
	.cfi_def_cfa_offset 32
	sw	$31,28($sp)
	sw	$fp,24($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,32($fp)
	lw	$2,32($fp)
	lw	$2,0($2)
	move	$4,$2
	.option	pic0
	jal	_ZSt18generate_canonicalIdLj53ESt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEET_RT1_
	nop

	.option	pic2
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,28($sp)
	lw	$fp,24($sp)
	addiu	$sp,$sp,32
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEdEclEv
	.cfi_endproc
$LFE4495:
	.size	_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEdEclEv, .-_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEdEclEv
	.section	.text._ZNKSt25uniform_real_distributionIdE10param_type1bEv,"axG",@progbits,_ZNKSt25uniform_real_distributionIdE10param_type1bEv,comdat
	.align	2
	.weak	_ZNKSt25uniform_real_distributionIdE10param_type1bEv
$LFB4496 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZNKSt25uniform_real_distributionIdE10param_type1bEv
	.type	_ZNKSt25uniform_real_distributionIdE10param_type1bEv, @function
_ZNKSt25uniform_real_distributionIdE10param_type1bEv:
	.frame	$fp,8,$31		# vars= 0, regs= 1/0, args= 0, gp= 0
	.mask	0x40000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-8
	.cfi_def_cfa_offset 8
	sw	$fp,4($sp)
	.cfi_offset 30, -4
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,8($fp)
	lw	$2,8($fp)
	ldc1	$f0,8($2)
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$fp,4($sp)
	addiu	$sp,$sp,8
	.cfi_restore 30
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZNKSt25uniform_real_distributionIdE10param_type1bEv
	.cfi_endproc
$LFE4496:
	.size	_ZNKSt25uniform_real_distributionIdE10param_type1bEv, .-_ZNKSt25uniform_real_distributionIdE10param_type1bEv
	.section	.text._ZNKSt25uniform_real_distributionIdE10param_type1aEv,"axG",@progbits,_ZNKSt25uniform_real_distributionIdE10param_type1aEv,comdat
	.align	2
	.weak	_ZNKSt25uniform_real_distributionIdE10param_type1aEv
$LFB4497 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZNKSt25uniform_real_distributionIdE10param_type1aEv
	.type	_ZNKSt25uniform_real_distributionIdE10param_type1aEv, @function
_ZNKSt25uniform_real_distributionIdE10param_type1aEv:
	.frame	$fp,8,$31		# vars= 0, regs= 1/0, args= 0, gp= 0
	.mask	0x40000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-8
	.cfi_def_cfa_offset 8
	sw	$fp,4($sp)
	.cfi_offset 30, -4
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,8($fp)
	lw	$2,8($fp)
	ldc1	$f0,0($2)
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$fp,4($sp)
	addiu	$sp,$sp,8
	.cfi_restore 30
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZNKSt25uniform_real_distributionIdE10param_type1aEv
	.cfi_endproc
$LFE4497:
	.size	_ZNKSt25uniform_real_distributionIdE10param_type1aEv, .-_ZNKSt25uniform_real_distributionIdE10param_type1aEv
	.section	.text._ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2Ev,"axG",@progbits,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC5Ev,comdat
	.align	2
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2Ev
$LFB4503 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2Ev
	.type	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2Ev, @function
_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2Ev:
	.frame	$fp,8,$31		# vars= 0, regs= 1/0, args= 0, gp= 0
	.mask	0x40000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-8
	.cfi_def_cfa_offset 8
	sw	$fp,4($sp)
	.cfi_offset 30, -4
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,8($fp)
	lw	$2,8($fp)
	sw	$0,0($2)
	nop
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$fp,4($sp)
	addiu	$sp,$sp,8
	.cfi_restore 30
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2Ev
	.cfi_endproc
$LFE4503:
	.size	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2Ev, .-_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2Ev
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1Ev
	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1Ev = _ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2Ev
	.section	.text._ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,"axG",@progbits,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,comdat
	.align	2
	.weak	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
$LFB4505 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.type	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, @function
_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv:
	.frame	$fp,32,$31		# vars= 0, regs= 2/0, args= 16, gp= 8
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-32
	.cfi_def_cfa_offset 32
	sw	$31,28($sp)
	sw	$fp,24($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,32($fp)
	lw	$2,32($fp)
	beq	$2,$0,$L148
	nop

	lw	$2,32($fp)
	lw	$2,0($2)
	addiu	$2,$2,4
	lw	$2,0($2)
	lw	$4,32($fp)
	move	$25,$2
	jalr	$25
	nop

$L148:
	nop
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,28($sp)
	lw	$fp,24($sp)
	addiu	$sp,$sp,32
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.cfi_endproc
$LFE4505:
	.size	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, .-_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.section	.text._ZNSaISt10shared_ptrI8hittableEED2Ev,"axG",@progbits,_ZNSaISt10shared_ptrI8hittableEED5Ev,comdat
	.align	2
	.weak	_ZNSaISt10shared_ptrI8hittableEED2Ev
$LFB4507 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZNSaISt10shared_ptrI8hittableEED2Ev
	.type	_ZNSaISt10shared_ptrI8hittableEED2Ev, @function
_ZNSaISt10shared_ptrI8hittableEED2Ev:
	.frame	$fp,32,$31		# vars= 0, regs= 2/0, args= 16, gp= 8
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-32
	.cfi_def_cfa_offset 32
	sw	$31,28($sp)
	sw	$fp,24($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,32($fp)
	lw	$4,32($fp)
	.option	pic0
	jal	_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED2Ev
	nop

	.option	pic2
	nop
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,28($sp)
	lw	$fp,24($sp)
	addiu	$sp,$sp,32
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZNSaISt10shared_ptrI8hittableEED2Ev
	.cfi_endproc
$LFE4507:
	.size	_ZNSaISt10shared_ptrI8hittableEED2Ev, .-_ZNSaISt10shared_ptrI8hittableEED2Ev
	.weak	_ZNSaISt10shared_ptrI8hittableEED1Ev
	_ZNSaISt10shared_ptrI8hittableEED1Ev = _ZNSaISt10shared_ptrI8hittableEED2Ev
	.section	.text._ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE13_M_deallocateEPS2_j,"axG",@progbits,_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE13_M_deallocateEPS2_j,comdat
	.align	2
	.weak	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE13_M_deallocateEPS2_j
$LFB4509 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE13_M_deallocateEPS2_j
	.type	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE13_M_deallocateEPS2_j, @function
_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE13_M_deallocateEPS2_j:
	.frame	$fp,32,$31		# vars= 0, regs= 2/0, args= 16, gp= 8
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-32
	.cfi_def_cfa_offset 32
	sw	$31,28($sp)
	sw	$fp,24($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,32($fp)
	sw	$5,36($fp)
	sw	$6,40($fp)
	lw	$2,36($fp)
	beq	$2,$0,$L152
	nop

	lw	$2,32($fp)
	lw	$6,40($fp)
	lw	$5,36($fp)
	move	$4,$2
	.option	pic0
	jal	_ZNSt16allocator_traitsISaISt10shared_ptrI8hittableEEE10deallocateERS3_PS2_j
	nop

	.option	pic2
$L152:
	nop
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,28($sp)
	lw	$fp,24($sp)
	addiu	$sp,$sp,32
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE13_M_deallocateEPS2_j
	.cfi_endproc
$LFE4509:
	.size	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE13_M_deallocateEPS2_j, .-_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE13_M_deallocateEPS2_j
	.section	.text._ZSt8_DestroyIPSt10shared_ptrI8hittableEEvT_S4_,"axG",@progbits,_ZSt8_DestroyIPSt10shared_ptrI8hittableEEvT_S4_,comdat
	.align	2
	.weak	_ZSt8_DestroyIPSt10shared_ptrI8hittableEEvT_S4_
$LFB4510 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZSt8_DestroyIPSt10shared_ptrI8hittableEEvT_S4_
	.type	_ZSt8_DestroyIPSt10shared_ptrI8hittableEEvT_S4_, @function
_ZSt8_DestroyIPSt10shared_ptrI8hittableEEvT_S4_:
	.frame	$fp,32,$31		# vars= 0, regs= 2/0, args= 16, gp= 8
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-32
	.cfi_def_cfa_offset 32
	sw	$31,28($sp)
	sw	$fp,24($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,32($fp)
	sw	$5,36($fp)
	lw	$5,36($fp)
	lw	$4,32($fp)
	.option	pic0
	jal	_ZNSt12_Destroy_auxILb0EE9__destroyIPSt10shared_ptrI8hittableEEEvT_S6_
	nop

	.option	pic2
	nop
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,28($sp)
	lw	$fp,24($sp)
	addiu	$sp,$sp,32
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZSt8_DestroyIPSt10shared_ptrI8hittableEEvT_S4_
	.cfi_endproc
$LFE4510:
	.size	_ZSt8_DestroyIPSt10shared_ptrI8hittableEEvT_S4_, .-_ZSt8_DestroyIPSt10shared_ptrI8hittableEEvT_S4_
	.section	.text._ZN9__gnu_cxx13new_allocatorI6sphereEC2Ev,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorI6sphereEC5Ev,comdat
	.align	2
	.weak	_ZN9__gnu_cxx13new_allocatorI6sphereEC2Ev
$LFB4512 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZN9__gnu_cxx13new_allocatorI6sphereEC2Ev
	.type	_ZN9__gnu_cxx13new_allocatorI6sphereEC2Ev, @function
_ZN9__gnu_cxx13new_allocatorI6sphereEC2Ev:
	.frame	$fp,8,$31		# vars= 0, regs= 1/0, args= 0, gp= 0
	.mask	0x40000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-8
	.cfi_def_cfa_offset 8
	sw	$fp,4($sp)
	.cfi_offset 30, -4
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,8($fp)
	nop
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$fp,4($sp)
	addiu	$sp,$sp,8
	.cfi_restore 30
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZN9__gnu_cxx13new_allocatorI6sphereEC2Ev
	.cfi_endproc
$LFE4512:
	.size	_ZN9__gnu_cxx13new_allocatorI6sphereEC2Ev, .-_ZN9__gnu_cxx13new_allocatorI6sphereEC2Ev
	.weak	_ZN9__gnu_cxx13new_allocatorI6sphereEC1Ev
	_ZN9__gnu_cxx13new_allocatorI6sphereEC1Ev = _ZN9__gnu_cxx13new_allocatorI6sphereEC2Ev
	.section	.text._ZN9__gnu_cxx13new_allocatorI6sphereED2Ev,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorI6sphereED5Ev,comdat
	.align	2
	.weak	_ZN9__gnu_cxx13new_allocatorI6sphereED2Ev
$LFB4515 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZN9__gnu_cxx13new_allocatorI6sphereED2Ev
	.type	_ZN9__gnu_cxx13new_allocatorI6sphereED2Ev, @function
_ZN9__gnu_cxx13new_allocatorI6sphereED2Ev:
	.frame	$fp,8,$31		# vars= 0, regs= 1/0, args= 0, gp= 0
	.mask	0x40000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-8
	.cfi_def_cfa_offset 8
	sw	$fp,4($sp)
	.cfi_offset 30, -4
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,8($fp)
	nop
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$fp,4($sp)
	addiu	$sp,$sp,8
	.cfi_restore 30
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZN9__gnu_cxx13new_allocatorI6sphereED2Ev
	.cfi_endproc
$LFE4515:
	.size	_ZN9__gnu_cxx13new_allocatorI6sphereED2Ev, .-_ZN9__gnu_cxx13new_allocatorI6sphereED2Ev
	.weak	_ZN9__gnu_cxx13new_allocatorI6sphereED1Ev
	_ZN9__gnu_cxx13new_allocatorI6sphereED1Ev = _ZN9__gnu_cxx13new_allocatorI6sphereED2Ev
	.section	.text._ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,"axG",@progbits,_ZNSt10shared_ptrI6sphereEC5ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,comdat
	.align	2
	.weak	_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
$LFB4518 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.type	_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_, @function
_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_:
	.frame	$fp,40,$31		# vars= 0, regs= 4/0, args= 16, gp= 8
	.mask	0xc0030000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-40
	.cfi_def_cfa_offset 40
	sw	$31,36($sp)
	sw	$fp,32($sp)
	sw	$17,28($sp)
	sw	$16,24($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	.cfi_offset 17, -12
	.cfi_offset 16, -16
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,40($fp)
	sw	$5,44($fp)
	sw	$6,48($fp)
	sw	$7,52($fp)
	lw	$16,40($fp)
	lw	$4,48($fp)
	.option	pic0
	jal	_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
	nop

	.option	pic2
	move	$17,$2
	lw	$4,52($fp)
	.option	pic0
	jal	_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE
	nop

	.option	pic2
	move	$7,$2
	move	$6,$17
	lw	$5,44($fp)
	move	$4,$16
	.option	pic0
	jal	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	nop

	.option	pic2
	nop
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,36($sp)
	lw	$fp,32($sp)
	lw	$17,28($sp)
	lw	$16,24($sp)
	addiu	$sp,$sp,40
	.cfi_restore 16
	.cfi_restore 17
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.cfi_endproc
$LFE4518:
	.size	_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_, .-_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.weak	_ZNSt10shared_ptrI6sphereEC1ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	_ZNSt10shared_ptrI6sphereEC1ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_ = _ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.section	.text._ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EE7_M_swapERS2_,"axG",@progbits,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EE7_M_swapERS2_,comdat
	.align	2
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EE7_M_swapERS2_
$LFB4523 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EE7_M_swapERS2_
	.type	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EE7_M_swapERS2_, @function
_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EE7_M_swapERS2_:
	.frame	$fp,16,$31		# vars= 8, regs= 1/0, args= 0, gp= 0
	.mask	0x40000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-16
	.cfi_def_cfa_offset 16
	sw	$fp,12($sp)
	.cfi_offset 30, -4
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,16($fp)
	sw	$5,20($fp)
	lw	$2,20($fp)
	lw	$2,0($2)
	sw	$2,4($fp)
	lw	$2,16($fp)
	lw	$3,0($2)
	lw	$2,20($fp)
	sw	$3,0($2)
	lw	$2,16($fp)
	lw	$3,4($fp)
	sw	$3,0($2)
	nop
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$fp,12($sp)
	addiu	$sp,$sp,16
	.cfi_restore 30
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EE7_M_swapERS2_
	.cfi_endproc
$LFE4523:
	.size	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EE7_M_swapERS2_, .-_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EE7_M_swapERS2_
	.section	.text._ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,"axG",@progbits,_ZNSt10shared_ptrI6sphereEC5ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,comdat
	.align	2
	.weak	_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
$LFB4525 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.type	_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_, @function
_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_:
	.frame	$fp,40,$31		# vars= 0, regs= 4/0, args= 16, gp= 8
	.mask	0xc0030000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-40
	.cfi_def_cfa_offset 40
	sw	$31,36($sp)
	sw	$fp,32($sp)
	sw	$17,28($sp)
	sw	$16,24($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	.cfi_offset 17, -12
	.cfi_offset 16, -16
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,40($fp)
	sw	$5,44($fp)
	sw	$6,48($fp)
	sw	$7,52($fp)
	lw	$16,40($fp)
	lw	$4,48($fp)
	.option	pic0
	jal	_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
	nop

	.option	pic2
	move	$17,$2
	lw	$4,52($fp)
	.option	pic0
	jal	_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE
	nop

	.option	pic2
	move	$7,$2
	move	$6,$17
	lw	$5,44($fp)
	move	$4,$16
	.option	pic0
	jal	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	nop

	.option	pic2
	nop
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,36($sp)
	lw	$fp,32($sp)
	lw	$17,28($sp)
	lw	$16,24($sp)
	addiu	$sp,$sp,40
	.cfi_restore 16
	.cfi_restore 17
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.cfi_endproc
$LFE4525:
	.size	_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_, .-_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.weak	_ZNSt10shared_ptrI6sphereEC1ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	_ZNSt10shared_ptrI6sphereEC1ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_ = _ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.section	.text._ZNSt8__detail5__modIjLj0ELj1ELj0EEET_S1_,"axG",@progbits,_ZNSt8__detail5__modIjLj0ELj1ELj0EEET_S1_,comdat
	.align	2
	.weak	_ZNSt8__detail5__modIjLj0ELj1ELj0EEET_S1_
$LFB4564 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZNSt8__detail5__modIjLj0ELj1ELj0EEET_S1_
	.type	_ZNSt8__detail5__modIjLj0ELj1ELj0EEET_S1_, @function
_ZNSt8__detail5__modIjLj0ELj1ELj0EEET_S1_:
	.frame	$fp,32,$31		# vars= 0, regs= 2/0, args= 16, gp= 8
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-32
	.cfi_def_cfa_offset 32
	sw	$31,28($sp)
	sw	$fp,24($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,32($fp)
	lw	$4,32($fp)
	.option	pic0
	jal	_ZNSt8__detail4_ModIjLj0ELj1ELj0ELb1ELb0EE6__calcEj
	nop

	.option	pic2
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,28($sp)
	lw	$fp,24($sp)
	addiu	$sp,$sp,32
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZNSt8__detail5__modIjLj0ELj1ELj0EEET_S1_
	.cfi_endproc
$LFE4564:
	.size	_ZNSt8__detail5__modIjLj0ELj1ELj0EEET_S1_, .-_ZNSt8__detail5__modIjLj0ELj1ELj0EEET_S1_
	.section	.text._ZNSt8__detail5__modIjLj624ELj1ELj0EEET_S1_,"axG",@progbits,_ZNSt8__detail5__modIjLj624ELj1ELj0EEET_S1_,comdat
	.align	2
	.weak	_ZNSt8__detail5__modIjLj624ELj1ELj0EEET_S1_
$LFB4565 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZNSt8__detail5__modIjLj624ELj1ELj0EEET_S1_
	.type	_ZNSt8__detail5__modIjLj624ELj1ELj0EEET_S1_, @function
_ZNSt8__detail5__modIjLj624ELj1ELj0EEET_S1_:
	.frame	$fp,32,$31		# vars= 0, regs= 2/0, args= 16, gp= 8
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-32
	.cfi_def_cfa_offset 32
	sw	$31,28($sp)
	sw	$fp,24($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,32($fp)
	lw	$4,32($fp)
	.option	pic0
	jal	_ZNSt8__detail4_ModIjLj624ELj1ELj0ELb1ELb1EE6__calcEj
	nop

	.option	pic2
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,28($sp)
	lw	$fp,24($sp)
	addiu	$sp,$sp,32
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZNSt8__detail5__modIjLj624ELj1ELj0EEET_S1_
	.cfi_endproc
$LFE4565:
	.size	_ZNSt8__detail5__modIjLj624ELj1ELj0EEET_S1_, .-_ZNSt8__detail5__modIjLj624ELj1ELj0EEET_S1_
	.section	.text._ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE3minEv,"axG",@progbits,_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE3minEv,comdat
	.align	2
	.weak	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE3minEv
$LFB4569 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE3minEv
	.type	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE3minEv, @function
_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE3minEv:
	.frame	$fp,8,$31		# vars= 0, regs= 1/0, args= 0, gp= 0
	.mask	0x40000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-8
	.cfi_def_cfa_offset 8
	sw	$fp,4($sp)
	.cfi_offset 30, -4
	move	$fp,$sp
	.cfi_def_cfa_register 30
	move	$2,$0
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$fp,4($sp)
	addiu	$sp,$sp,8
	.cfi_restore 30
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE3minEv
	.cfi_endproc
$LFE4569:
	.size	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE3minEv, .-_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE3minEv
	.section	.text._ZSt18generate_canonicalIdLj53ESt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEET_RT1_,"axG",@progbits,_ZSt18generate_canonicalIdLj53ESt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEET_RT1_,comdat
	.align	2
	.weak	_ZSt18generate_canonicalIdLj53ESt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEET_RT1_
$LFB4566 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZSt18generate_canonicalIdLj53ESt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEET_RT1_
	.type	_ZSt18generate_canonicalIdLj53ESt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEET_RT1_, @function
_ZSt18generate_canonicalIdLj53ESt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEET_RT1_:
	.frame	$fp,88,$31		# vars= 48, regs= 3/0, args= 16, gp= 8
	.mask	0xc0010000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-88
	.cfi_def_cfa_offset 88
	sw	$31,84($sp)
	sw	$fp,80($sp)
	sw	$16,76($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	.cfi_offset 16, -12
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,88($fp)
	li	$2,53			# 0x35
	sw	$2,28($fp)
	lui	$2,%hi($LC15)
	ldc1	$f0,%lo($LC15)($2)
	sdc1	$f0,64($fp)
	li	$2,32			# 0x20
	sw	$2,32($fp)
	li	$2,2			# 0x2
	sw	$2,36($fp)
	sw	$0,52($fp)
	sw	$0,48($fp)
	lui	$2,%hi($LC0)
	ldc1	$f0,%lo($LC0)($2)
	sdc1	$f0,56($fp)
	li	$2,2			# 0x2
	sw	$2,24($fp)
$L168:
	lw	$2,24($fp)
	beq	$2,$0,$L166
	nop

	lw	$4,88($fp)
	.option	pic0
	jal	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEclEv
	nop

	.option	pic2
	move	$16,$2
	.option	pic0
	jal	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE3minEv
	nop

	.option	pic2
	subu	$2,$16,$2
	mtc1	$2,$f0
	cvt.d.w	$f0,$f0
	bgez	$2,$L167
	nop

	lui	$2,%hi($LC15)
	ldc1	$f2,%lo($LC15)($2)
	add.d	$f0,$f0,$f2
$L167:
	ldc1	$f2,56($fp)
	mul.d	$f0,$f0,$f2
	ldc1	$f2,48($fp)
	add.d	$f0,$f2,$f0
	sdc1	$f0,48($fp)
	ldc1	$f2,56($fp)
	lui	$2,%hi($LC15)
	ldc1	$f0,%lo($LC15)($2)
	mul.d	$f0,$f2,$f0
	sdc1	$f0,56($fp)
	lw	$2,24($fp)
	addiu	$2,$2,-1
	sw	$2,24($fp)
	.option	pic0
	b	$L168
	nop

	.option	pic2
$L166:
	ldc1	$f2,48($fp)
	ldc1	$f0,56($fp)
	div.d	$f0,$f2,$f0
	sdc1	$f0,40($fp)
	li	$3,1			# 0x1
	ldc1	$f2,40($fp)
	lui	$2,%hi($LC0)
	ldc1	$f0,%lo($LC0)($2)
	c.le.d	$fcc0,$f0,$f2
	bc1t	$fcc0,$L169
	nop

	move	$3,$0
$L169:
	andi	$2,$3,0x00ff
	beq	$2,$0,$L170
	nop

	lui	$2,%hi($LC16)
	ldc1	$f0,%lo($LC16)($2)
	sdc1	$f0,40($fp)
$L170:
	ldc1	$f0,40($fp)
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,84($sp)
	lw	$fp,80($sp)
	lw	$16,76($sp)
	addiu	$sp,$sp,88
	.cfi_restore 16
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZSt18generate_canonicalIdLj53ESt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEET_RT1_
	.cfi_endproc
$LFE4566:
	.size	_ZSt18generate_canonicalIdLj53ESt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEET_RT1_, .-_ZSt18generate_canonicalIdLj53ESt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEET_RT1_
	.section	.text._ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align	2
	.weak	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev
$LFB4576 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev
	.type	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev:
	.frame	$fp,8,$31		# vars= 0, regs= 1/0, args= 0, gp= 0
	.mask	0x40000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-8
	.cfi_def_cfa_offset 8
	sw	$fp,4($sp)
	.cfi_offset 30, -4
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,8($fp)
	lui	$2,%hi(_ZTVSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE+8)
	addiu	$3,$2,%lo(_ZTVSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE+8)
	lw	$2,8($fp)
	sw	$3,0($2)
	nop
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$fp,4($sp)
	addiu	$sp,$sp,8
	.cfi_restore 30
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev
	.cfi_endproc
$LFE4576:
	.size	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED1Ev
	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED1Ev = _ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED0Ev,"axG",@progbits,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align	2
	.weak	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED0Ev
$LFB4578 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED0Ev
	.type	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED0Ev, @function
_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED0Ev:
	.frame	$fp,32,$31		# vars= 0, regs= 2/0, args= 16, gp= 8
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-32
	.cfi_def_cfa_offset 32
	sw	$31,28($sp)
	sw	$fp,24($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	move	$fp,$sp
	.cfi_def_cfa_register 30
	lui	$28,%hi(__gnu_local_gp)
	addiu	$28,$28,%lo(__gnu_local_gp)
	.cprestore	16
	sw	$4,32($fp)
	lw	$4,32($fp)
	.option	pic0
	jal	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED1Ev
	nop

	.option	pic2
	lw	$28,16($fp)
	li	$5,12			# 0xc
	lw	$4,32($fp)
	lw	$2,%call16(_ZdlPvj)($28)
	move	$25,$2
	.reloc	1f,R_MIPS_JALR,_ZdlPvj
1:	jalr	$25
	nop

	lw	$28,16($fp)
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,28($sp)
	lw	$fp,24($sp)
	addiu	$sp,$sp,32
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED0Ev
	.cfi_endproc
$LFE4578:
	.size	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED0Ev, .-_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED0Ev
	.section	.text._ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED2Ev,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED5Ev,comdat
	.align	2
	.weak	_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED2Ev
$LFB4580 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED2Ev
	.type	_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED2Ev, @function
_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED2Ev:
	.frame	$fp,8,$31		# vars= 0, regs= 1/0, args= 0, gp= 0
	.mask	0x40000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-8
	.cfi_def_cfa_offset 8
	sw	$fp,4($sp)
	.cfi_offset 30, -4
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,8($fp)
	nop
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$fp,4($sp)
	addiu	$sp,$sp,8
	.cfi_restore 30
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED2Ev
	.cfi_endproc
$LFE4580:
	.size	_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED2Ev, .-_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED2Ev
	.weak	_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED1Ev
	_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED1Ev = _ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED2Ev
	.section	.text._ZNSt16allocator_traitsISaISt10shared_ptrI8hittableEEE10deallocateERS3_PS2_j,"axG",@progbits,_ZNSt16allocator_traitsISaISt10shared_ptrI8hittableEEE10deallocateERS3_PS2_j,comdat
	.align	2
	.weak	_ZNSt16allocator_traitsISaISt10shared_ptrI8hittableEEE10deallocateERS3_PS2_j
$LFB4582 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZNSt16allocator_traitsISaISt10shared_ptrI8hittableEEE10deallocateERS3_PS2_j
	.type	_ZNSt16allocator_traitsISaISt10shared_ptrI8hittableEEE10deallocateERS3_PS2_j, @function
_ZNSt16allocator_traitsISaISt10shared_ptrI8hittableEEE10deallocateERS3_PS2_j:
	.frame	$fp,32,$31		# vars= 0, regs= 2/0, args= 16, gp= 8
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-32
	.cfi_def_cfa_offset 32
	sw	$31,28($sp)
	sw	$fp,24($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,32($fp)
	sw	$5,36($fp)
	sw	$6,40($fp)
	lw	$6,40($fp)
	lw	$5,36($fp)
	lw	$4,32($fp)
	.option	pic0
	jal	_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEE10deallocateEPS3_j
	nop

	.option	pic2
	nop
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,28($sp)
	lw	$fp,24($sp)
	addiu	$sp,$sp,32
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZNSt16allocator_traitsISaISt10shared_ptrI8hittableEEE10deallocateERS3_PS2_j
	.cfi_endproc
$LFE4582:
	.size	_ZNSt16allocator_traitsISaISt10shared_ptrI8hittableEEE10deallocateERS3_PS2_j, .-_ZNSt16allocator_traitsISaISt10shared_ptrI8hittableEEE10deallocateERS3_PS2_j
	.section	.text._ZNSt12_Destroy_auxILb0EE9__destroyIPSt10shared_ptrI8hittableEEEvT_S6_,"axG",@progbits,_ZNSt12_Destroy_auxILb0EE9__destroyIPSt10shared_ptrI8hittableEEEvT_S6_,comdat
	.align	2
	.weak	_ZNSt12_Destroy_auxILb0EE9__destroyIPSt10shared_ptrI8hittableEEEvT_S6_
$LFB4583 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZNSt12_Destroy_auxILb0EE9__destroyIPSt10shared_ptrI8hittableEEEvT_S6_
	.type	_ZNSt12_Destroy_auxILb0EE9__destroyIPSt10shared_ptrI8hittableEEEvT_S6_, @function
_ZNSt12_Destroy_auxILb0EE9__destroyIPSt10shared_ptrI8hittableEEEvT_S6_:
	.frame	$fp,32,$31		# vars= 0, regs= 2/0, args= 16, gp= 8
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-32
	.cfi_def_cfa_offset 32
	sw	$31,28($sp)
	sw	$fp,24($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,32($fp)
	sw	$5,36($fp)
$L178:
	lw	$3,32($fp)
	lw	$2,36($fp)
	beq	$3,$2,$L179
	nop

	lw	$4,32($fp)
	.option	pic0
	jal	_ZSt11__addressofISt10shared_ptrI8hittableEEPT_RS3_
	nop

	.option	pic2
	move	$4,$2
	.option	pic0
	jal	_ZSt8_DestroyISt10shared_ptrI8hittableEEvPT_
	nop

	.option	pic2
	lw	$2,32($fp)
	addiu	$2,$2,8
	sw	$2,32($fp)
	.option	pic0
	b	$L178
	nop

	.option	pic2
$L179:
	nop
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,28($sp)
	lw	$fp,24($sp)
	addiu	$sp,$sp,32
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZNSt12_Destroy_auxILb0EE9__destroyIPSt10shared_ptrI8hittableEEEvT_S6_
	.cfi_endproc
$LFE4583:
	.size	_ZNSt12_Destroy_auxILb0EE9__destroyIPSt10shared_ptrI8hittableEEEvT_S6_, .-_ZNSt12_Destroy_auxILb0EE9__destroyIPSt10shared_ptrI8hittableEEEvT_S6_
	.section	.text._ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,"axG",@progbits,_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC5ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,comdat
	.align	2
	.weak	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
$LFB4585 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.type	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_, @function
_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_:
	.frame	$fp,56,$31		# vars= 0, regs= 5/0, args= 24, gp= 8
	.mask	0xc0070000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-56
	.cfi_def_cfa_offset 56
	sw	$31,52($sp)
	sw	$fp,48($sp)
	sw	$18,44($sp)
	sw	$17,40($sp)
	sw	$16,36($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	.cfi_offset 18, -12
	.cfi_offset 17, -16
	.cfi_offset 16, -20
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,56($fp)
	sw	$5,60($fp)
	sw	$6,64($fp)
	sw	$7,68($fp)
	lw	$2,56($fp)
	sw	$0,0($2)
	lw	$2,56($fp)
	addiu	$16,$2,4
	lw	$17,56($fp)
	lw	$4,64($fp)
	.option	pic0
	jal	_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
	nop

	.option	pic2
	move	$18,$2
	lw	$4,68($fp)
	.option	pic0
	jal	_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE
	nop

	.option	pic2
	sw	$2,16($sp)
	move	$7,$18
	lw	$6,60($fp)
	move	$5,$17
	move	$4,$16
	.option	pic0
	jal	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_
	nop

	.option	pic2
	lw	$2,56($fp)
	lw	$2,0($2)
	move	$5,$2
	lw	$4,56($fp)
	.option	pic0
	jal	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EE31_M_enable_shared_from_this_withIS0_S0_EENSt9enable_ifIXntsrNS3_15__has_esft_baseIT0_vEE5valueEvE4typeEPT_
	nop

	.option	pic2
	nop
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,52($sp)
	lw	$fp,48($sp)
	lw	$18,44($sp)
	lw	$17,40($sp)
	lw	$16,36($sp)
	addiu	$sp,$sp,56
	.cfi_restore 16
	.cfi_restore 17
	.cfi_restore 18
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.cfi_endproc
$LFE4585:
	.size	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_, .-_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.weak	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC1ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC1ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_ = _ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.section	.text._ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,"axG",@progbits,_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC5ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,comdat
	.align	2
	.weak	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
$LFB4588 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.type	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_, @function
_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_:
	.frame	$fp,56,$31		# vars= 0, regs= 5/0, args= 24, gp= 8
	.mask	0xc0070000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-56
	.cfi_def_cfa_offset 56
	sw	$31,52($sp)
	sw	$fp,48($sp)
	sw	$18,44($sp)
	sw	$17,40($sp)
	sw	$16,36($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	.cfi_offset 18, -12
	.cfi_offset 17, -16
	.cfi_offset 16, -20
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,56($fp)
	sw	$5,60($fp)
	sw	$6,64($fp)
	sw	$7,68($fp)
	lw	$2,56($fp)
	sw	$0,0($2)
	lw	$2,56($fp)
	addiu	$16,$2,4
	lw	$17,56($fp)
	lw	$4,64($fp)
	.option	pic0
	jal	_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
	nop

	.option	pic2
	move	$18,$2
	lw	$4,68($fp)
	.option	pic0
	jal	_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE
	nop

	.option	pic2
	sw	$2,16($sp)
	move	$7,$18
	lw	$6,60($fp)
	move	$5,$17
	move	$4,$16
	.option	pic0
	jal	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_
	nop

	.option	pic2
	lw	$2,56($fp)
	lw	$2,0($2)
	move	$5,$2
	lw	$4,56($fp)
	.option	pic0
	jal	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EE31_M_enable_shared_from_this_withIS0_S0_EENSt9enable_ifIXntsrNS3_15__has_esft_baseIT0_vEE5valueEvE4typeEPT_
	nop

	.option	pic2
	nop
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,52($sp)
	lw	$fp,48($sp)
	lw	$18,44($sp)
	lw	$17,40($sp)
	lw	$16,36($sp)
	addiu	$sp,$sp,56
	.cfi_restore 16
	.cfi_restore 17
	.cfi_restore 18
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.cfi_endproc
$LFE4588:
	.size	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_, .-_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.weak	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC1ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC1ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_ = _ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.section	.text._ZNSt8__detail4_ModIjLj0ELj1ELj0ELb1ELb0EE6__calcEj,"axG",@progbits,_ZNSt8__detail4_ModIjLj0ELj1ELj0ELb1ELb0EE6__calcEj,comdat
	.align	2
	.weak	_ZNSt8__detail4_ModIjLj0ELj1ELj0ELb1ELb0EE6__calcEj
$LFB4618 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZNSt8__detail4_ModIjLj0ELj1ELj0ELb1ELb0EE6__calcEj
	.type	_ZNSt8__detail4_ModIjLj0ELj1ELj0ELb1ELb0EE6__calcEj, @function
_ZNSt8__detail4_ModIjLj0ELj1ELj0ELb1ELb0EE6__calcEj:
	.frame	$fp,16,$31		# vars= 8, regs= 1/0, args= 0, gp= 0
	.mask	0x40000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-16
	.cfi_def_cfa_offset 16
	sw	$fp,12($sp)
	.cfi_offset 30, -4
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,16($fp)
	lw	$2,16($fp)
	sw	$2,4($fp)
	lw	$2,4($fp)
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$fp,12($sp)
	addiu	$sp,$sp,16
	.cfi_restore 30
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZNSt8__detail4_ModIjLj0ELj1ELj0ELb1ELb0EE6__calcEj
	.cfi_endproc
$LFE4618:
	.size	_ZNSt8__detail4_ModIjLj0ELj1ELj0ELb1ELb0EE6__calcEj, .-_ZNSt8__detail4_ModIjLj0ELj1ELj0ELb1ELb0EE6__calcEj
	.section	.text._ZNSt8__detail4_ModIjLj624ELj1ELj0ELb1ELb1EE6__calcEj,"axG",@progbits,_ZNSt8__detail4_ModIjLj624ELj1ELj0ELb1ELb1EE6__calcEj,comdat
	.align	2
	.weak	_ZNSt8__detail4_ModIjLj624ELj1ELj0ELb1ELb1EE6__calcEj
$LFB4619 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZNSt8__detail4_ModIjLj624ELj1ELj0ELb1ELb1EE6__calcEj
	.type	_ZNSt8__detail4_ModIjLj624ELj1ELj0ELb1ELb1EE6__calcEj, @function
_ZNSt8__detail4_ModIjLj624ELj1ELj0ELb1ELb1EE6__calcEj:
	.frame	$fp,16,$31		# vars= 8, regs= 1/0, args= 0, gp= 0
	.mask	0x40000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-16
	.cfi_def_cfa_offset 16
	sw	$fp,12($sp)
	.cfi_offset 30, -4
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,16($fp)
	lw	$2,16($fp)
	sw	$2,4($fp)
	lw	$2,4($fp)
	srl	$4,$2,4
	li	$3,440467456			# 0x1a410000
	ori	$3,$3,0xa41b
	multu	$4,$3
	mfhi	$3
	srl	$4,$3,2
	li	$3,624			# 0x270
	mul	$3,$4,$3
	subu	$2,$2,$3
	sw	$2,4($fp)
	lw	$2,4($fp)
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$fp,12($sp)
	addiu	$sp,$sp,16
	.cfi_restore 30
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZNSt8__detail4_ModIjLj624ELj1ELj0ELb1ELb1EE6__calcEj
	.cfi_endproc
$LFE4619:
	.size	_ZNSt8__detail4_ModIjLj624ELj1ELj0ELb1ELb1EE6__calcEj, .-_ZNSt8__detail4_ModIjLj624ELj1ELj0ELb1ELb1EE6__calcEj
	.section	.text._ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEclEv,"axG",@progbits,_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEclEv,comdat
	.align	2
	.weak	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEclEv
$LFB4620 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEclEv
	.type	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEclEv, @function
_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEclEv:
	.frame	$fp,40,$31		# vars= 8, regs= 2/0, args= 16, gp= 8
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-40
	.cfi_def_cfa_offset 40
	sw	$31,36($sp)
	sw	$fp,32($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,40($fp)
	lw	$2,40($fp)
	lw	$2,2496($2)
	sltu	$2,$2,624
	bne	$2,$0,$L187
	nop

	lw	$4,40($fp)
	.option	pic0
	jal	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE11_M_gen_randEv
	nop

	.option	pic2
$L187:
	lw	$2,40($fp)
	lw	$2,2496($2)
	addiu	$4,$2,1
	lw	$3,40($fp)
	sw	$4,2496($3)
	lw	$3,40($fp)
	sll	$2,$2,2
	addu	$2,$3,$2
	lw	$2,0($2)
	sw	$2,28($fp)
	lw	$2,28($fp)
	srl	$2,$2,11
	lw	$3,28($fp)
	xor	$2,$3,$2
	sw	$2,28($fp)
	lw	$2,28($fp)
	sll	$3,$2,7
	li	$2,-1658060800			# 0xffffffff9d2c0000
	ori	$2,$2,0x5680
	and	$2,$3,$2
	lw	$3,28($fp)
	xor	$2,$3,$2
	sw	$2,28($fp)
	lw	$2,28($fp)
	sll	$3,$2,15
	li	$2,-272236544			# 0xffffffffefc60000
	and	$2,$3,$2
	lw	$3,28($fp)
	xor	$2,$3,$2
	sw	$2,28($fp)
	lw	$2,28($fp)
	srl	$2,$2,18
	lw	$3,28($fp)
	xor	$2,$3,$2
	sw	$2,28($fp)
	lw	$2,28($fp)
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,36($sp)
	lw	$fp,32($sp)
	addiu	$sp,$sp,40
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEclEv
	.cfi_endproc
$LFE4620:
	.size	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEclEv, .-_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEclEv
	.section	.text._ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEE10deallocateEPS3_j,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEE10deallocateEPS3_j,comdat
	.align	2
	.weak	_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEE10deallocateEPS3_j
$LFB4622 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEE10deallocateEPS3_j
	.type	_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEE10deallocateEPS3_j, @function
_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEE10deallocateEPS3_j:
	.frame	$fp,32,$31		# vars= 0, regs= 2/0, args= 16, gp= 8
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-32
	.cfi_def_cfa_offset 32
	sw	$31,28($sp)
	sw	$fp,24($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	move	$fp,$sp
	.cfi_def_cfa_register 30
	lui	$28,%hi(__gnu_local_gp)
	addiu	$28,$28,%lo(__gnu_local_gp)
	.cprestore	16
	sw	$4,32($fp)
	sw	$5,36($fp)
	sw	$6,40($fp)
	lw	$4,36($fp)
	lw	$2,%call16(_ZdlPv)($28)
	move	$25,$2
	.reloc	1f,R_MIPS_JALR,_ZdlPv
1:	jalr	$25
	nop

	lw	$28,16($fp)
	nop
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,28($sp)
	lw	$fp,24($sp)
	addiu	$sp,$sp,32
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEE10deallocateEPS3_j
	.cfi_endproc
$LFE4622:
	.size	_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEE10deallocateEPS3_j, .-_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEE10deallocateEPS3_j
	.section	.text._ZSt11__addressofISt10shared_ptrI8hittableEEPT_RS3_,"axG",@progbits,_ZSt11__addressofISt10shared_ptrI8hittableEEPT_RS3_,comdat
	.align	2
	.weak	_ZSt11__addressofISt10shared_ptrI8hittableEEPT_RS3_
$LFB4623 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZSt11__addressofISt10shared_ptrI8hittableEEPT_RS3_
	.type	_ZSt11__addressofISt10shared_ptrI8hittableEEPT_RS3_, @function
_ZSt11__addressofISt10shared_ptrI8hittableEEPT_RS3_:
	.frame	$fp,8,$31		# vars= 0, regs= 1/0, args= 0, gp= 0
	.mask	0x40000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-8
	.cfi_def_cfa_offset 8
	sw	$fp,4($sp)
	.cfi_offset 30, -4
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,8($fp)
	lw	$2,8($fp)
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$fp,4($sp)
	addiu	$sp,$sp,8
	.cfi_restore 30
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZSt11__addressofISt10shared_ptrI8hittableEEPT_RS3_
	.cfi_endproc
$LFE4623:
	.size	_ZSt11__addressofISt10shared_ptrI8hittableEEPT_RS3_, .-_ZSt11__addressofISt10shared_ptrI8hittableEEPT_RS3_
	.section	.text._ZSt8_DestroyISt10shared_ptrI8hittableEEvPT_,"axG",@progbits,_ZSt8_DestroyISt10shared_ptrI8hittableEEvPT_,comdat
	.align	2
	.weak	_ZSt8_DestroyISt10shared_ptrI8hittableEEvPT_
$LFB4624 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZSt8_DestroyISt10shared_ptrI8hittableEEvPT_
	.type	_ZSt8_DestroyISt10shared_ptrI8hittableEEvPT_, @function
_ZSt8_DestroyISt10shared_ptrI8hittableEEvPT_:
	.frame	$fp,32,$31		# vars= 0, regs= 2/0, args= 16, gp= 8
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-32
	.cfi_def_cfa_offset 32
	sw	$31,28($sp)
	sw	$fp,24($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,32($fp)
	lw	$4,32($fp)
	.option	pic0
	jal	_ZNSt10shared_ptrI8hittableED1Ev
	nop

	.option	pic2
	nop
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,28($sp)
	lw	$fp,24($sp)
	addiu	$sp,$sp,32
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZSt8_DestroyISt10shared_ptrI8hittableEEvPT_
	.cfi_endproc
$LFE4624:
	.size	_ZSt8_DestroyISt10shared_ptrI8hittableEEvPT_, .-_ZSt8_DestroyISt10shared_ptrI8hittableEEvPT_
	.section	.text._ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_,"axG",@progbits,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC5I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_,comdat
	.align	2
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_
$LFB4626 = .
	.cfi_startproc
	.cfi_personality 0x80,DW.ref.__gxx_personality_v0
	.cfi_lsda 0,$LLSDA4626
	.set	nomips16
	.set	nomicromips
	.ent	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_
	.type	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_, @function
_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_:
	.frame	$fp,104,$31		# vars= 48, regs= 7/0, args= 16, gp= 8
	.mask	0xc01f0000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-104
	.cfi_def_cfa_offset 104
	sw	$31,100($sp)
	sw	$fp,96($sp)
	sw	$20,92($sp)
	sw	$19,88($sp)
	sw	$18,84($sp)
	sw	$17,80($sp)
	sw	$16,76($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	.cfi_offset 20, -12
	.cfi_offset 19, -16
	.cfi_offset 18, -20
	.cfi_offset 17, -24
	.cfi_offset 16, -28
	move	$fp,$sp
	.cfi_def_cfa_register 30
	lui	$28,%hi(__gnu_local_gp)
	addiu	$28,$28,%lo(__gnu_local_gp)
	.cprestore	16
	sw	$4,36($fp)
	sw	$5,32($fp)
	sw	$6,112($fp)
	sw	$7,28($fp)
	lw	$2,120($fp)
	sw	$2,24($fp)
	lw	$2,%got(__stack_chk_guard)($28)
	lw	$2,0($2)
	sw	$2,68($fp)
	lw	$2,112($fp)
	addiu	$3,$fp,44
	move	$5,$2
	move	$4,$3
	.option	pic0
	jal	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC1IS0_EERKSaIT_E
	nop

	.option	pic2
	lw	$28,16($fp)
	addiu	$2,$fp,60
	addiu	$3,$fp,44
	move	$5,$3
	move	$4,$2
$LEHB20 = .
	.option	pic0
	jal	_ZSt18__allocate_guardedISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEESt15__allocated_ptrIT_ERS8_
	nop

	.option	pic2
$LEHE20 = .
	lw	$28,16($fp)
	addiu	$2,$fp,60
	move	$4,$2
	.option	pic0
	jal	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE3getEv
	nop

	.option	pic2
	lw	$28,16($fp)
	sw	$2,52($fp)
	lw	$2,112($fp)
	addiu	$3,$fp,48
	move	$5,$2
	move	$4,$3
	.option	pic0
	jal	_ZNSaI6sphereEC1ERKS0_
	nop

	.option	pic2
	lw	$28,16($fp)
	addiu	$20,$fp,48
	lw	$4,28($fp)
	.option	pic0
	jal	_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
	nop

	.option	pic2
	lw	$28,16($fp)
	move	$18,$2
	lw	$4,24($fp)
	.option	pic0
	jal	_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE
	nop

	.option	pic2
	lw	$28,16($fp)
	move	$19,$2
	lw	$17,52($fp)
	move	$5,$17
	li	$4,64			# 0x40
	.option	pic0
	jal	_ZnwjPv
	nop

	.option	pic2
	lw	$28,16($fp)
	move	$16,$2
	move	$7,$19
	move	$6,$18
	move	$5,$20
	move	$4,$16
$LEHB21 = .
	.option	pic0
	jal	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC1IJ4vec3dEEES1_DpOT_
	nop

	.option	pic2
$LEHE21 = .
	lw	$28,16($fp)
	sw	$16,56($fp)
	addiu	$2,$fp,48
	move	$4,$2
	.option	pic0
	jal	_ZNSaI6sphereED1Ev
	nop

	.option	pic2
	lw	$28,16($fp)
	addiu	$2,$fp,60
	move	$5,$0
	move	$4,$2
	.option	pic0
	jal	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEaSEDn
	nop

	.option	pic2
	lw	$28,16($fp)
	lw	$2,36($fp)
	lw	$3,56($fp)
	sw	$3,0($2)
	lw	$4,56($fp)
	.option	pic0
	jal	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv
	nop

	.option	pic2
	lw	$28,16($fp)
	move	$3,$2
	lw	$2,32($fp)
	sw	$3,0($2)
	addiu	$2,$fp,60
	move	$4,$2
	.option	pic0
	jal	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED1Ev
	nop

	.option	pic2
	lw	$28,16($fp)
	addiu	$2,$fp,44
	move	$4,$2
	.option	pic0
	jal	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED1Ev
	nop

	.option	pic2
	lw	$28,16($fp)
	nop
	lw	$2,%got(__stack_chk_guard)($28)
	lw	$3,68($fp)
	lw	$2,0($2)
	beq	$3,$2,$L196
	nop

	.option	pic0
	b	$L199
	nop

	.option	pic2
$L198:
	lw	$28,16($fp)
	move	$18,$4
	move	$5,$17
	move	$4,$16
	.option	pic0
	jal	_ZdlPvS_
	nop

	.option	pic2
	lw	$28,16($fp)
	move	$16,$18
	addiu	$2,$fp,48
	move	$4,$2
	.option	pic0
	jal	_ZNSaI6sphereED1Ev
	nop

	.option	pic2
	lw	$28,16($fp)
	addiu	$2,$fp,60
	move	$4,$2
	.option	pic0
	jal	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED1Ev
	nop

	.option	pic2
	lw	$28,16($fp)
	.option	pic0
	b	$L195
	nop

	.option	pic2
$L197:
	lw	$28,16($fp)
	move	$16,$4
$L195:
	addiu	$2,$fp,44
	move	$4,$2
	.option	pic0
	jal	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED1Ev
	nop

	.option	pic2
	lw	$28,16($fp)
	move	$2,$16
	move	$4,$2
	lw	$2,%call16(_Unwind_Resume)($28)
	move	$25,$2
$LEHB22 = .
	.reloc	1f,R_MIPS_JALR,_Unwind_Resume
1:	jalr	$25
	nop

$LEHE22 = .
$L199:
	lw	$2,%call16(__stack_chk_fail)($28)
	move	$25,$2
	.reloc	1f,R_MIPS_JALR,__stack_chk_fail
1:	jalr	$25
	nop

$L196:
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,100($sp)
	lw	$fp,96($sp)
	lw	$20,92($sp)
	lw	$19,88($sp)
	lw	$18,84($sp)
	lw	$17,80($sp)
	lw	$16,76($sp)
	addiu	$sp,$sp,104
	.cfi_restore 16
	.cfi_restore 17
	.cfi_restore 18
	.cfi_restore 19
	.cfi_restore 20
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_
	.cfi_endproc
$LFE4626:
	.section	.gcc_except_table
$LLSDA4626:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 $LLSDACSE4626-$LLSDACSB4626
$LLSDACSB4626:
	.uleb128 $LEHB20-$LFB4626
	.uleb128 $LEHE20-$LEHB20
	.uleb128 $L197-$LFB4626
	.uleb128 0
	.uleb128 $LEHB21-$LFB4626
	.uleb128 $LEHE21-$LEHB21
	.uleb128 $L198-$LFB4626
	.uleb128 0
	.uleb128 $LEHB22-$LFB4626
	.uleb128 $LEHE22-$LEHB22
	.uleb128 0
	.uleb128 0
$LLSDACSE4626:
	.section	.text._ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_,"axG",@progbits,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC5I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_,comdat
	.size	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_, .-_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_
	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_ = _ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_
	.section	.text._ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EE31_M_enable_shared_from_this_withIS0_S0_EENSt9enable_ifIXntsrNS3_15__has_esft_baseIT0_vEE5valueEvE4typeEPT_,"axG",@progbits,_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EE31_M_enable_shared_from_this_withIS0_S0_EENSt9enable_ifIXntsrNS3_15__has_esft_baseIT0_vEE5valueEvE4typeEPT_,comdat
	.align	2
	.weak	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EE31_M_enable_shared_from_this_withIS0_S0_EENSt9enable_ifIXntsrNS3_15__has_esft_baseIT0_vEE5valueEvE4typeEPT_
$LFB4628 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EE31_M_enable_shared_from_this_withIS0_S0_EENSt9enable_ifIXntsrNS3_15__has_esft_baseIT0_vEE5valueEvE4typeEPT_
	.type	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EE31_M_enable_shared_from_this_withIS0_S0_EENSt9enable_ifIXntsrNS3_15__has_esft_baseIT0_vEE5valueEvE4typeEPT_, @function
_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EE31_M_enable_shared_from_this_withIS0_S0_EENSt9enable_ifIXntsrNS3_15__has_esft_baseIT0_vEE5valueEvE4typeEPT_:
	.frame	$fp,8,$31		# vars= 0, regs= 1/0, args= 0, gp= 0
	.mask	0x40000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-8
	.cfi_def_cfa_offset 8
	sw	$fp,4($sp)
	.cfi_offset 30, -4
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,8($fp)
	sw	$5,12($fp)
	nop
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$fp,4($sp)
	addiu	$sp,$sp,8
	.cfi_restore 30
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EE31_M_enable_shared_from_this_withIS0_S0_EENSt9enable_ifIXntsrNS3_15__has_esft_baseIT0_vEE5valueEvE4typeEPT_
	.cfi_endproc
$LFE4628:
	.size	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EE31_M_enable_shared_from_this_withIS0_S0_EENSt9enable_ifIXntsrNS3_15__has_esft_baseIT0_vEE5valueEvE4typeEPT_, .-_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EE31_M_enable_shared_from_this_withIS0_S0_EENSt9enable_ifIXntsrNS3_15__has_esft_baseIT0_vEE5valueEvE4typeEPT_
	.section	.text._ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_,"axG",@progbits,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC5I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_,comdat
	.align	2
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_
$LFB4630 = .
	.cfi_startproc
	.cfi_personality 0x80,DW.ref.__gxx_personality_v0
	.cfi_lsda 0,$LLSDA4630
	.set	nomips16
	.set	nomicromips
	.ent	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_
	.type	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_, @function
_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_:
	.frame	$fp,104,$31		# vars= 48, regs= 7/0, args= 16, gp= 8
	.mask	0xc01f0000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-104
	.cfi_def_cfa_offset 104
	sw	$31,100($sp)
	sw	$fp,96($sp)
	sw	$20,92($sp)
	sw	$19,88($sp)
	sw	$18,84($sp)
	sw	$17,80($sp)
	sw	$16,76($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	.cfi_offset 20, -12
	.cfi_offset 19, -16
	.cfi_offset 18, -20
	.cfi_offset 17, -24
	.cfi_offset 16, -28
	move	$fp,$sp
	.cfi_def_cfa_register 30
	lui	$28,%hi(__gnu_local_gp)
	addiu	$28,$28,%lo(__gnu_local_gp)
	.cprestore	16
	sw	$4,36($fp)
	sw	$5,32($fp)
	sw	$6,112($fp)
	sw	$7,28($fp)
	lw	$2,120($fp)
	sw	$2,24($fp)
	lw	$2,%got(__stack_chk_guard)($28)
	lw	$2,0($2)
	sw	$2,68($fp)
	lw	$2,112($fp)
	addiu	$3,$fp,44
	move	$5,$2
	move	$4,$3
	.option	pic0
	jal	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC1IS0_EERKSaIT_E
	nop

	.option	pic2
	lw	$28,16($fp)
	addiu	$2,$fp,60
	addiu	$3,$fp,44
	move	$5,$3
	move	$4,$2
$LEHB23 = .
	.option	pic0
	jal	_ZSt18__allocate_guardedISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEESt15__allocated_ptrIT_ERS8_
	nop

	.option	pic2
$LEHE23 = .
	lw	$28,16($fp)
	addiu	$2,$fp,60
	move	$4,$2
	.option	pic0
	jal	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE3getEv
	nop

	.option	pic2
	lw	$28,16($fp)
	sw	$2,52($fp)
	lw	$2,112($fp)
	addiu	$3,$fp,48
	move	$5,$2
	move	$4,$3
	.option	pic0
	jal	_ZNSaI6sphereEC1ERKS0_
	nop

	.option	pic2
	lw	$28,16($fp)
	addiu	$20,$fp,48
	lw	$4,28($fp)
	.option	pic0
	jal	_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
	nop

	.option	pic2
	lw	$28,16($fp)
	move	$18,$2
	lw	$4,24($fp)
	.option	pic0
	jal	_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE
	nop

	.option	pic2
	lw	$28,16($fp)
	move	$19,$2
	lw	$17,52($fp)
	move	$5,$17
	li	$4,64			# 0x40
	.option	pic0
	jal	_ZnwjPv
	nop

	.option	pic2
	lw	$28,16($fp)
	move	$16,$2
	move	$7,$19
	move	$6,$18
	move	$5,$20
	move	$4,$16
$LEHB24 = .
	.option	pic0
	jal	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC1IJ4vec3iEEES1_DpOT_
	nop

	.option	pic2
$LEHE24 = .
	lw	$28,16($fp)
	sw	$16,56($fp)
	addiu	$2,$fp,48
	move	$4,$2
	.option	pic0
	jal	_ZNSaI6sphereED1Ev
	nop

	.option	pic2
	lw	$28,16($fp)
	addiu	$2,$fp,60
	move	$5,$0
	move	$4,$2
	.option	pic0
	jal	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEaSEDn
	nop

	.option	pic2
	lw	$28,16($fp)
	lw	$2,36($fp)
	lw	$3,56($fp)
	sw	$3,0($2)
	lw	$4,56($fp)
	.option	pic0
	jal	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv
	nop

	.option	pic2
	lw	$28,16($fp)
	move	$3,$2
	lw	$2,32($fp)
	sw	$3,0($2)
	addiu	$2,$fp,60
	move	$4,$2
	.option	pic0
	jal	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED1Ev
	nop

	.option	pic2
	lw	$28,16($fp)
	addiu	$2,$fp,44
	move	$4,$2
	.option	pic0
	jal	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED1Ev
	nop

	.option	pic2
	lw	$28,16($fp)
	nop
	lw	$2,%got(__stack_chk_guard)($28)
	lw	$3,68($fp)
	lw	$2,0($2)
	beq	$3,$2,$L204
	nop

	.option	pic0
	b	$L207
	nop

	.option	pic2
$L206:
	lw	$28,16($fp)
	move	$18,$4
	move	$5,$17
	move	$4,$16
	.option	pic0
	jal	_ZdlPvS_
	nop

	.option	pic2
	lw	$28,16($fp)
	move	$16,$18
	addiu	$2,$fp,48
	move	$4,$2
	.option	pic0
	jal	_ZNSaI6sphereED1Ev
	nop

	.option	pic2
	lw	$28,16($fp)
	addiu	$2,$fp,60
	move	$4,$2
	.option	pic0
	jal	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED1Ev
	nop

	.option	pic2
	lw	$28,16($fp)
	.option	pic0
	b	$L203
	nop

	.option	pic2
$L205:
	lw	$28,16($fp)
	move	$16,$4
$L203:
	addiu	$2,$fp,44
	move	$4,$2
	.option	pic0
	jal	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED1Ev
	nop

	.option	pic2
	lw	$28,16($fp)
	move	$2,$16
	move	$4,$2
	lw	$2,%call16(_Unwind_Resume)($28)
	move	$25,$2
$LEHB25 = .
	.reloc	1f,R_MIPS_JALR,_Unwind_Resume
1:	jalr	$25
	nop

$LEHE25 = .
$L207:
	lw	$2,%call16(__stack_chk_fail)($28)
	move	$25,$2
	.reloc	1f,R_MIPS_JALR,__stack_chk_fail
1:	jalr	$25
	nop

$L204:
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,100($sp)
	lw	$fp,96($sp)
	lw	$20,92($sp)
	lw	$19,88($sp)
	lw	$18,84($sp)
	lw	$17,80($sp)
	lw	$16,76($sp)
	addiu	$sp,$sp,104
	.cfi_restore 16
	.cfi_restore 17
	.cfi_restore 18
	.cfi_restore 19
	.cfi_restore 20
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_
	.cfi_endproc
$LFE4630:
	.section	.gcc_except_table
$LLSDA4630:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 $LLSDACSE4630-$LLSDACSB4630
$LLSDACSB4630:
	.uleb128 $LEHB23-$LFB4630
	.uleb128 $LEHE23-$LEHB23
	.uleb128 $L205-$LFB4630
	.uleb128 0
	.uleb128 $LEHB24-$LFB4630
	.uleb128 $LEHE24-$LEHB24
	.uleb128 $L206-$LFB4630
	.uleb128 0
	.uleb128 $LEHB25-$LFB4630
	.uleb128 $LEHE25-$LEHB25
	.uleb128 0
	.uleb128 0
$LLSDACSE4630:
	.section	.text._ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_,"axG",@progbits,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC5I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_,comdat
	.size	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_, .-_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_
	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_ = _ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_
	.section	.text._ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE11_M_gen_randEv,"axG",@progbits,_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE11_M_gen_randEv,comdat
	.align	2
	.weak	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE11_M_gen_randEv
$LFB4658 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE11_M_gen_randEv
	.type	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE11_M_gen_randEv, @function
_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE11_M_gen_randEv:
	.frame	$fp,40,$31		# vars= 32, regs= 1/0, args= 0, gp= 0
	.mask	0x40000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-40
	.cfi_def_cfa_offset 40
	sw	$fp,36($sp)
	.cfi_offset 30, -4
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,40($fp)
	li	$2,-2147483648			# 0xffffffff80000000
	sw	$2,12($fp)
	li	$2,2147418112			# 0x7fff0000
	ori	$2,$2,0xffff
	sw	$2,16($fp)
	sw	$0,4($fp)
$L212:
	lw	$2,4($fp)
	sltu	$2,$2,227
	beq	$2,$0,$L209
	nop

	lw	$3,40($fp)
	lw	$2,4($fp)
	sll	$2,$2,2
	addu	$2,$3,$2
	lw	$3,0($2)
	li	$2,-2147483648			# 0xffffffff80000000
	and	$3,$3,$2
	lw	$2,4($fp)
	addiu	$2,$2,1
	lw	$4,40($fp)
	sll	$2,$2,2
	addu	$2,$4,$2
	lw	$2,0($2)
	ext	$2,$2,0,31
	or	$2,$3,$2
	sw	$2,20($fp)
	lw	$2,4($fp)
	addiu	$2,$2,397
	lw	$3,40($fp)
	sll	$2,$2,2
	addu	$2,$3,$2
	lw	$3,0($2)
	lw	$2,20($fp)
	srl	$2,$2,1
	xor	$3,$3,$2
	lw	$2,20($fp)
	andi	$2,$2,0x1
	beq	$2,$0,$L210
	nop

	li	$2,-1727528960			# 0xffffffff99080000
	ori	$2,$2,0xb0df
	.option	pic0
	b	$L211
	nop

	.option	pic2
$L210:
	move	$2,$0
$L211:
	xor	$3,$2,$3
	lw	$4,40($fp)
	lw	$2,4($fp)
	sll	$2,$2,2
	addu	$2,$4,$2
	sw	$3,0($2)
	lw	$2,4($fp)
	addiu	$2,$2,1
	sw	$2,4($fp)
	.option	pic0
	b	$L212
	nop

	.option	pic2
$L209:
	li	$2,227			# 0xe3
	sw	$2,8($fp)
$L216:
	lw	$2,8($fp)
	sltu	$2,$2,623
	beq	$2,$0,$L213
	nop

	lw	$3,40($fp)
	lw	$2,8($fp)
	sll	$2,$2,2
	addu	$2,$3,$2
	lw	$3,0($2)
	li	$2,-2147483648			# 0xffffffff80000000
	and	$3,$3,$2
	lw	$2,8($fp)
	addiu	$2,$2,1
	lw	$4,40($fp)
	sll	$2,$2,2
	addu	$2,$4,$2
	lw	$2,0($2)
	ext	$2,$2,0,31
	or	$2,$3,$2
	sw	$2,24($fp)
	lw	$2,8($fp)
	addiu	$2,$2,-227
	lw	$3,40($fp)
	sll	$2,$2,2
	addu	$2,$3,$2
	lw	$3,0($2)
	lw	$2,24($fp)
	srl	$2,$2,1
	xor	$3,$3,$2
	lw	$2,24($fp)
	andi	$2,$2,0x1
	beq	$2,$0,$L214
	nop

	li	$2,-1727528960			# 0xffffffff99080000
	ori	$2,$2,0xb0df
	.option	pic0
	b	$L215
	nop

	.option	pic2
$L214:
	move	$2,$0
$L215:
	xor	$3,$2,$3
	lw	$4,40($fp)
	lw	$2,8($fp)
	sll	$2,$2,2
	addu	$2,$4,$2
	sw	$3,0($2)
	lw	$2,8($fp)
	addiu	$2,$2,1
	sw	$2,8($fp)
	.option	pic0
	b	$L216
	nop

	.option	pic2
$L213:
	lw	$2,40($fp)
	lw	$3,2492($2)
	li	$2,-2147483648			# 0xffffffff80000000
	and	$3,$3,$2
	lw	$2,40($fp)
	lw	$2,0($2)
	ext	$2,$2,0,31
	or	$2,$3,$2
	sw	$2,28($fp)
	lw	$2,40($fp)
	lw	$3,1584($2)
	lw	$2,28($fp)
	srl	$2,$2,1
	xor	$3,$3,$2
	lw	$2,28($fp)
	andi	$2,$2,0x1
	beq	$2,$0,$L217
	nop

	li	$2,-1727528960			# 0xffffffff99080000
	ori	$2,$2,0xb0df
	.option	pic0
	b	$L218
	nop

	.option	pic2
$L217:
	move	$2,$0
$L218:
	xor	$3,$2,$3
	lw	$2,40($fp)
	sw	$3,2492($2)
	lw	$2,40($fp)
	sw	$0,2496($2)
	nop
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$fp,36($sp)
	addiu	$sp,$sp,40
	.cfi_restore 30
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE11_M_gen_randEv
	.cfi_endproc
$LFE4658:
	.size	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE11_M_gen_randEv, .-_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE11_M_gen_randEv
	.section	.text._ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC2IS0_EERKSaIT_E,"axG",@progbits,_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC5IS0_EERKSaIT_E,comdat
	.align	2
	.weak	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC2IS0_EERKSaIT_E
$LFB4661 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC2IS0_EERKSaIT_E
	.type	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC2IS0_EERKSaIT_E, @function
_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC2IS0_EERKSaIT_E:
	.frame	$fp,32,$31		# vars= 0, regs= 2/0, args= 16, gp= 8
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-32
	.cfi_def_cfa_offset 32
	sw	$31,28($sp)
	sw	$fp,24($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,32($fp)
	sw	$5,36($fp)
	lw	$4,32($fp)
	.option	pic0
	jal	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC2Ev
	nop

	.option	pic2
	nop
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,28($sp)
	lw	$fp,24($sp)
	addiu	$sp,$sp,32
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC2IS0_EERKSaIT_E
	.cfi_endproc
$LFE4661:
	.size	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC2IS0_EERKSaIT_E, .-_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC2IS0_EERKSaIT_E
	.weak	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC1IS0_EERKSaIT_E
	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC1IS0_EERKSaIT_E = _ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC2IS0_EERKSaIT_E
	.section	.text._ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED2Ev,"axG",@progbits,_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED5Ev,comdat
	.align	2
	.weak	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED2Ev
$LFB4664 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED2Ev
	.type	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED2Ev, @function
_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED2Ev:
	.frame	$fp,32,$31		# vars= 0, regs= 2/0, args= 16, gp= 8
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-32
	.cfi_def_cfa_offset 32
	sw	$31,28($sp)
	sw	$fp,24($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,32($fp)
	lw	$4,32($fp)
	.option	pic0
	jal	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED2Ev
	nop

	.option	pic2
	nop
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,28($sp)
	lw	$fp,24($sp)
	addiu	$sp,$sp,32
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED2Ev
	.cfi_endproc
$LFE4664:
	.size	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED2Ev, .-_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED2Ev
	.weak	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED1Ev
	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED1Ev = _ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED2Ev
	.section	.text._ZSt18__allocate_guardedISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEESt15__allocated_ptrIT_ERS8_,"axG",@progbits,_ZSt18__allocate_guardedISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEESt15__allocated_ptrIT_ERS8_,comdat
	.align	2
	.weak	_ZSt18__allocate_guardedISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEESt15__allocated_ptrIT_ERS8_
$LFB4666 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZSt18__allocate_guardedISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEESt15__allocated_ptrIT_ERS8_
	.type	_ZSt18__allocate_guardedISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEESt15__allocated_ptrIT_ERS8_, @function
_ZSt18__allocate_guardedISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEESt15__allocated_ptrIT_ERS8_:
	.frame	$fp,32,$31		# vars= 0, regs= 2/0, args= 16, gp= 8
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-32
	.cfi_def_cfa_offset 32
	sw	$31,28($sp)
	sw	$fp,24($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,32($fp)
	sw	$5,36($fp)
	li	$5,1			# 0x1
	lw	$4,36($fp)
	.option	pic0
	jal	_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE8allocateERS6_j
	nop

	.option	pic2
	move	$6,$2
	lw	$5,36($fp)
	lw	$4,32($fp)
	.option	pic0
	jal	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC1ERS6_PS5_
	nop

	.option	pic2
	lw	$2,32($fp)
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,28($sp)
	lw	$fp,24($sp)
	addiu	$sp,$sp,32
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZSt18__allocate_guardedISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEESt15__allocated_ptrIT_ERS8_
	.cfi_endproc
$LFE4666:
	.size	_ZSt18__allocate_guardedISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEESt15__allocated_ptrIT_ERS8_, .-_ZSt18__allocate_guardedISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEESt15__allocated_ptrIT_ERS8_
	.section	.text._ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED2Ev,"axG",@progbits,_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED5Ev,comdat
	.align	2
	.weak	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED2Ev
$LFB4668 = .
	.cfi_startproc
	.cfi_personality 0x80,DW.ref.__gxx_personality_v0
	.cfi_lsda 0,$LLSDA4668
	.set	nomips16
	.set	nomicromips
	.ent	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED2Ev
	.type	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED2Ev, @function
_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED2Ev:
	.frame	$fp,32,$31		# vars= 0, regs= 2/0, args= 16, gp= 8
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-32
	.cfi_def_cfa_offset 32
	sw	$31,28($sp)
	sw	$fp,24($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,32($fp)
	lw	$2,32($fp)
	lw	$2,4($2)
	beq	$2,$0,$L225
	nop

	lw	$2,32($fp)
	lw	$3,0($2)
	lw	$2,32($fp)
	lw	$2,4($2)
	li	$6,1			# 0x1
	move	$5,$2
	move	$4,$3
	.option	pic0
	jal	_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE10deallocateERS6_PS5_j
	nop

	.option	pic2
$L225:
	nop
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,28($sp)
	lw	$fp,24($sp)
	addiu	$sp,$sp,32
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED2Ev
	.cfi_endproc
$LFE4668:
	.section	.gcc_except_table
$LLSDA4668:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 $LLSDACSE4668-$LLSDACSB4668
$LLSDACSB4668:
$LLSDACSE4668:
	.section	.text._ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED2Ev,"axG",@progbits,_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED5Ev,comdat
	.size	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED2Ev, .-_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED2Ev
	.weak	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED1Ev
	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED1Ev = _ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED2Ev
	.section	.text._ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE3getEv,"axG",@progbits,_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE3getEv,comdat
	.align	2
	.weak	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE3getEv
$LFB4673 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE3getEv
	.type	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE3getEv, @function
_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE3getEv:
	.frame	$fp,32,$31		# vars= 0, regs= 2/0, args= 16, gp= 8
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-32
	.cfi_def_cfa_offset 32
	sw	$31,28($sp)
	sw	$fp,24($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,32($fp)
	lw	$2,32($fp)
	lw	$2,4($2)
	move	$4,$2
	.option	pic0
	jal	_ZSt12__to_addressISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEPT_S7_
	nop

	.option	pic2
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,28($sp)
	lw	$fp,24($sp)
	addiu	$sp,$sp,32
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE3getEv
	.cfi_endproc
$LFE4673:
	.size	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE3getEv, .-_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE3getEv
	.section	.text._ZNSaI6sphereEC2ERKS0_,"axG",@progbits,_ZNSaI6sphereEC5ERKS0_,comdat
	.align	2
	.weak	_ZNSaI6sphereEC2ERKS0_
$LFB4675 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZNSaI6sphereEC2ERKS0_
	.type	_ZNSaI6sphereEC2ERKS0_, @function
_ZNSaI6sphereEC2ERKS0_:
	.frame	$fp,32,$31		# vars= 0, regs= 2/0, args= 16, gp= 8
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-32
	.cfi_def_cfa_offset 32
	sw	$31,28($sp)
	sw	$fp,24($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,32($fp)
	sw	$5,36($fp)
	lw	$5,36($fp)
	lw	$4,32($fp)
	.option	pic0
	jal	_ZN9__gnu_cxx13new_allocatorI6sphereEC2ERKS2_
	nop

	.option	pic2
	nop
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,28($sp)
	lw	$fp,24($sp)
	addiu	$sp,$sp,32
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZNSaI6sphereEC2ERKS0_
	.cfi_endproc
$LFE4675:
	.size	_ZNSaI6sphereEC2ERKS0_, .-_ZNSaI6sphereEC2ERKS0_
	.weak	_ZNSaI6sphereEC1ERKS0_
	_ZNSaI6sphereEC1ERKS0_ = _ZNSaI6sphereEC2ERKS0_
	.section	.text._ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED2Ev,"axG",@progbits,_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED5Ev,comdat
	.align	2
	.weak	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED2Ev
$LFB4680 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED2Ev
	.type	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED2Ev, @function
_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED2Ev:
	.frame	$fp,32,$31		# vars= 0, regs= 2/0, args= 16, gp= 8
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-32
	.cfi_def_cfa_offset 32
	sw	$31,28($sp)
	sw	$fp,24($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,32($fp)
	lw	$4,32($fp)
	.option	pic0
	jal	_ZNSaI6sphereED2Ev
	nop

	.option	pic2
	nop
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,28($sp)
	lw	$fp,24($sp)
	addiu	$sp,$sp,32
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED2Ev
	.cfi_endproc
$LFE4680:
	.size	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED2Ev, .-_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED2Ev
	.weak	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED1Ev
	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED1Ev = _ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED2Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD2Ev,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD5Ev,comdat
	.align	2
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD2Ev
$LFB4682 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD2Ev
	.type	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD2Ev, @function
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD2Ev:
	.frame	$fp,32,$31		# vars= 0, regs= 2/0, args= 16, gp= 8
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-32
	.cfi_def_cfa_offset 32
	sw	$31,28($sp)
	sw	$fp,24($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,32($fp)
	lw	$4,32($fp)
	.option	pic0
	jal	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED2Ev
	nop

	.option	pic2
	nop
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,28($sp)
	lw	$fp,24($sp)
	addiu	$sp,$sp,32
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD2Ev
	.cfi_endproc
$LFE4682:
	.size	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD2Ev, .-_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD2Ev
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD1Ev
	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD1Ev = _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD2Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3dEEES1_DpOT_,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC5IJ4vec3dEEES1_DpOT_,comdat
	.align	2
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3dEEES1_DpOT_
$LFB4684 = .
	.cfi_startproc
	.cfi_personality 0x80,DW.ref.__gxx_personality_v0
	.cfi_lsda 0,$LLSDA4684
	.set	nomips16
	.set	nomicromips
	.ent	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3dEEES1_DpOT_
	.type	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3dEEES1_DpOT_, @function
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3dEEES1_DpOT_:
	.frame	$fp,64,$31		# vars= 24, regs= 4/0, args= 16, gp= 8
	.mask	0xc0030000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-64
	.cfi_def_cfa_offset 64
	sw	$31,60($sp)
	sw	$fp,56($sp)
	sw	$17,52($sp)
	sw	$16,48($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	.cfi_offset 17, -12
	.cfi_offset 16, -16
	move	$fp,$sp
	.cfi_def_cfa_register 30
	lui	$28,%hi(__gnu_local_gp)
	addiu	$28,$28,%lo(__gnu_local_gp)
	.cprestore	16
	sw	$4,36($fp)
	sw	$5,32($fp)
	sw	$6,28($fp)
	sw	$7,24($fp)
	lw	$2,%got(__stack_chk_guard)($28)
	lw	$2,0($2)
	sw	$2,44($fp)
	lw	$2,36($fp)
	move	$4,$2
	.option	pic0
	jal	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC2Ev
	nop

	.option	pic2
	lw	$28,16($fp)
	lui	$2,%hi(_ZTVSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE+8)
	addiu	$3,$2,%lo(_ZTVSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE+8)
	lw	$2,36($fp)
	sw	$3,0($2)
	lw	$2,36($fp)
	addiu	$16,$2,16
	addiu	$2,$fp,40
	lw	$5,32($fp)
	move	$4,$2
	.option	pic0
	jal	_ZNSaI6sphereEC1ERKS0_
	nop

	.option	pic2
	lw	$28,16($fp)
	addiu	$2,$fp,40
	move	$5,$2
	move	$4,$16
	.option	pic0
	jal	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC1ES1_
	nop

	.option	pic2
	lw	$28,16($fp)
	addiu	$2,$fp,40
	move	$4,$2
	.option	pic0
	jal	_ZNSaI6sphereED1Ev
	nop

	.option	pic2
	lw	$28,16($fp)
	lw	$4,36($fp)
	.option	pic0
	jal	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv
	nop

	.option	pic2
	lw	$28,16($fp)
	move	$16,$2
	lw	$4,28($fp)
	.option	pic0
	jal	_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
	nop

	.option	pic2
	lw	$28,16($fp)
	move	$17,$2
	lw	$4,24($fp)
	.option	pic0
	jal	_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE
	nop

	.option	pic2
	lw	$28,16($fp)
	move	$7,$2
	move	$6,$17
	move	$5,$16
	lw	$4,32($fp)
$LEHB26 = .
	.option	pic0
	jal	_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3dEEEvRS1_PT_DpOT0_
	nop

	.option	pic2
$LEHE26 = .
	lw	$28,16($fp)
	nop
	lw	$2,%got(__stack_chk_guard)($28)
	lw	$3,44($fp)
	lw	$2,0($2)
	beq	$3,$2,$L233
	nop

	.option	pic0
	b	$L235
	nop

	.option	pic2
$L234:
	lw	$28,16($fp)
	move	$16,$4
	lw	$2,36($fp)
	addiu	$2,$2,16
	move	$4,$2
	.option	pic0
	jal	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD1Ev
	nop

	.option	pic2
	lw	$28,16($fp)
	lw	$2,36($fp)
	move	$4,$2
	.option	pic0
	jal	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev
	nop

	.option	pic2
	lw	$28,16($fp)
	move	$2,$16
	move	$4,$2
	lw	$2,%call16(_Unwind_Resume)($28)
	move	$25,$2
$LEHB27 = .
	.reloc	1f,R_MIPS_JALR,_Unwind_Resume
1:	jalr	$25
	nop

$LEHE27 = .
$L235:
	lw	$2,%call16(__stack_chk_fail)($28)
	move	$25,$2
	.reloc	1f,R_MIPS_JALR,__stack_chk_fail
1:	jalr	$25
	nop

$L233:
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,60($sp)
	lw	$fp,56($sp)
	lw	$17,52($sp)
	lw	$16,48($sp)
	addiu	$sp,$sp,64
	.cfi_restore 16
	.cfi_restore 17
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3dEEES1_DpOT_
	.cfi_endproc
$LFE4684:
	.section	.gcc_except_table
$LLSDA4684:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 $LLSDACSE4684-$LLSDACSB4684
$LLSDACSB4684:
	.uleb128 $LEHB26-$LFB4684
	.uleb128 $LEHE26-$LEHB26
	.uleb128 $L234-$LFB4684
	.uleb128 0
	.uleb128 $LEHB27-$LFB4684
	.uleb128 $LEHE27-$LEHB27
	.uleb128 0
	.uleb128 0
$LLSDACSE4684:
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3dEEES1_DpOT_,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC5IJ4vec3dEEES1_DpOT_,comdat
	.size	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3dEEES1_DpOT_, .-_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3dEEES1_DpOT_
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC1IJ4vec3dEEES1_DpOT_
	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC1IJ4vec3dEEES1_DpOT_ = _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3dEEES1_DpOT_
	.section	.text._ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEaSEDn,"axG",@progbits,_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEaSEDn,comdat
	.align	2
	.weak	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEaSEDn
$LFB4686 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEaSEDn
	.type	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEaSEDn, @function
_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEaSEDn:
	.frame	$fp,8,$31		# vars= 0, regs= 1/0, args= 0, gp= 0
	.mask	0x40000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-8
	.cfi_def_cfa_offset 8
	sw	$fp,4($sp)
	.cfi_offset 30, -4
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,8($fp)
	sw	$5,12($fp)
	lw	$2,8($fp)
	sw	$0,4($2)
	lw	$2,8($fp)
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$fp,4($sp)
	addiu	$sp,$sp,8
	.cfi_restore 30
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEaSEDn
	.cfi_endproc
$LFE4686:
	.size	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEaSEDn, .-_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEaSEDn
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv,comdat
	.align	2
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv
$LFB4687 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv
	.type	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv, @function
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv:
	.frame	$fp,32,$31		# vars= 0, regs= 2/0, args= 16, gp= 8
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-32
	.cfi_def_cfa_offset 32
	sw	$31,28($sp)
	sw	$fp,24($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,32($fp)
	lw	$2,32($fp)
	addiu	$2,$2,16
	move	$4,$2
	.option	pic0
	jal	_ZN9__gnu_cxx16__aligned_bufferI6sphereE6_M_ptrEv
	nop

	.option	pic2
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,28($sp)
	lw	$fp,24($sp)
	addiu	$sp,$sp,32
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv
	.cfi_endproc
$LFE4687:
	.size	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv, .-_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3iEEES1_DpOT_,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC5IJ4vec3iEEES1_DpOT_,comdat
	.align	2
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3iEEES1_DpOT_
$LFB4689 = .
	.cfi_startproc
	.cfi_personality 0x80,DW.ref.__gxx_personality_v0
	.cfi_lsda 0,$LLSDA4689
	.set	nomips16
	.set	nomicromips
	.ent	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3iEEES1_DpOT_
	.type	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3iEEES1_DpOT_, @function
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3iEEES1_DpOT_:
	.frame	$fp,64,$31		# vars= 24, regs= 4/0, args= 16, gp= 8
	.mask	0xc0030000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-64
	.cfi_def_cfa_offset 64
	sw	$31,60($sp)
	sw	$fp,56($sp)
	sw	$17,52($sp)
	sw	$16,48($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	.cfi_offset 17, -12
	.cfi_offset 16, -16
	move	$fp,$sp
	.cfi_def_cfa_register 30
	lui	$28,%hi(__gnu_local_gp)
	addiu	$28,$28,%lo(__gnu_local_gp)
	.cprestore	16
	sw	$4,36($fp)
	sw	$5,32($fp)
	sw	$6,28($fp)
	sw	$7,24($fp)
	lw	$2,%got(__stack_chk_guard)($28)
	lw	$2,0($2)
	sw	$2,44($fp)
	lw	$2,36($fp)
	move	$4,$2
	.option	pic0
	jal	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC2Ev
	nop

	.option	pic2
	lw	$28,16($fp)
	lui	$2,%hi(_ZTVSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE+8)
	addiu	$3,$2,%lo(_ZTVSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE+8)
	lw	$2,36($fp)
	sw	$3,0($2)
	lw	$2,36($fp)
	addiu	$16,$2,16
	addiu	$2,$fp,40
	lw	$5,32($fp)
	move	$4,$2
	.option	pic0
	jal	_ZNSaI6sphereEC1ERKS0_
	nop

	.option	pic2
	lw	$28,16($fp)
	addiu	$2,$fp,40
	move	$5,$2
	move	$4,$16
	.option	pic0
	jal	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC1ES1_
	nop

	.option	pic2
	lw	$28,16($fp)
	addiu	$2,$fp,40
	move	$4,$2
	.option	pic0
	jal	_ZNSaI6sphereED1Ev
	nop

	.option	pic2
	lw	$28,16($fp)
	lw	$4,36($fp)
	.option	pic0
	jal	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv
	nop

	.option	pic2
	lw	$28,16($fp)
	move	$16,$2
	lw	$4,28($fp)
	.option	pic0
	jal	_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
	nop

	.option	pic2
	lw	$28,16($fp)
	move	$17,$2
	lw	$4,24($fp)
	.option	pic0
	jal	_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE
	nop

	.option	pic2
	lw	$28,16($fp)
	move	$7,$2
	move	$6,$17
	move	$5,$16
	lw	$4,32($fp)
$LEHB28 = .
	.option	pic0
	jal	_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3iEEEvRS1_PT_DpOT0_
	nop

	.option	pic2
$LEHE28 = .
	lw	$28,16($fp)
	nop
	lw	$2,%got(__stack_chk_guard)($28)
	lw	$3,44($fp)
	lw	$2,0($2)
	beq	$3,$2,$L242
	nop

	.option	pic0
	b	$L244
	nop

	.option	pic2
$L243:
	lw	$28,16($fp)
	move	$16,$4
	lw	$2,36($fp)
	addiu	$2,$2,16
	move	$4,$2
	.option	pic0
	jal	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD1Ev
	nop

	.option	pic2
	lw	$28,16($fp)
	lw	$2,36($fp)
	move	$4,$2
	.option	pic0
	jal	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev
	nop

	.option	pic2
	lw	$28,16($fp)
	move	$2,$16
	move	$4,$2
	lw	$2,%call16(_Unwind_Resume)($28)
	move	$25,$2
$LEHB29 = .
	.reloc	1f,R_MIPS_JALR,_Unwind_Resume
1:	jalr	$25
	nop

$LEHE29 = .
$L244:
	lw	$2,%call16(__stack_chk_fail)($28)
	move	$25,$2
	.reloc	1f,R_MIPS_JALR,__stack_chk_fail
1:	jalr	$25
	nop

$L242:
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,60($sp)
	lw	$fp,56($sp)
	lw	$17,52($sp)
	lw	$16,48($sp)
	addiu	$sp,$sp,64
	.cfi_restore 16
	.cfi_restore 17
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3iEEES1_DpOT_
	.cfi_endproc
$LFE4689:
	.section	.gcc_except_table
$LLSDA4689:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 $LLSDACSE4689-$LLSDACSB4689
$LLSDACSB4689:
	.uleb128 $LEHB28-$LFB4689
	.uleb128 $LEHE28-$LEHB28
	.uleb128 $L243-$LFB4689
	.uleb128 0
	.uleb128 $LEHB29-$LFB4689
	.uleb128 $LEHE29-$LEHB29
	.uleb128 0
	.uleb128 0
$LLSDACSE4689:
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3iEEES1_DpOT_,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC5IJ4vec3iEEES1_DpOT_,comdat
	.size	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3iEEES1_DpOT_, .-_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3iEEES1_DpOT_
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC1IJ4vec3iEEES1_DpOT_
	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC1IJ4vec3iEEES1_DpOT_ = _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3iEEES1_DpOT_
	.section	.text._ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC2Ev,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC5Ev,comdat
	.align	2
	.weak	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC2Ev
$LFB4702 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC2Ev
	.type	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC2Ev, @function
_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC2Ev:
	.frame	$fp,8,$31		# vars= 0, regs= 1/0, args= 0, gp= 0
	.mask	0x40000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-8
	.cfi_def_cfa_offset 8
	sw	$fp,4($sp)
	.cfi_offset 30, -4
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,8($fp)
	nop
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$fp,4($sp)
	addiu	$sp,$sp,8
	.cfi_restore 30
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC2Ev
	.cfi_endproc
$LFE4702:
	.size	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC2Ev, .-_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC2Ev
	.weak	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC1Ev
	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC1Ev = _ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC2Ev
	.section	.text._ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED2Ev,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED5Ev,comdat
	.align	2
	.weak	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED2Ev
$LFB4705 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED2Ev
	.type	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED2Ev, @function
_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED2Ev:
	.frame	$fp,8,$31		# vars= 0, regs= 1/0, args= 0, gp= 0
	.mask	0x40000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-8
	.cfi_def_cfa_offset 8
	sw	$fp,4($sp)
	.cfi_offset 30, -4
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,8($fp)
	nop
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$fp,4($sp)
	addiu	$sp,$sp,8
	.cfi_restore 30
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED2Ev
	.cfi_endproc
$LFE4705:
	.size	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED2Ev, .-_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED2Ev
	.weak	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED1Ev
	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED1Ev = _ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED2Ev
	.section	.text._ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE8allocateERS6_j,"axG",@progbits,_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE8allocateERS6_j,comdat
	.align	2
	.weak	_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE8allocateERS6_j
$LFB4707 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE8allocateERS6_j
	.type	_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE8allocateERS6_j, @function
_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE8allocateERS6_j:
	.frame	$fp,32,$31		# vars= 0, regs= 2/0, args= 16, gp= 8
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-32
	.cfi_def_cfa_offset 32
	sw	$31,28($sp)
	sw	$fp,24($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,32($fp)
	sw	$5,36($fp)
	move	$6,$0
	lw	$5,36($fp)
	lw	$4,32($fp)
	.option	pic0
	jal	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8allocateEjPKv
	nop

	.option	pic2
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,28($sp)
	lw	$fp,24($sp)
	addiu	$sp,$sp,32
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE8allocateERS6_j
	.cfi_endproc
$LFE4707:
	.size	_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE8allocateERS6_j, .-_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE8allocateERS6_j
	.section	.text._ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC2ERS6_PS5_,"axG",@progbits,_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC5ERS6_PS5_,comdat
	.align	2
	.weak	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC2ERS6_PS5_
$LFB4709 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC2ERS6_PS5_
	.type	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC2ERS6_PS5_, @function
_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC2ERS6_PS5_:
	.frame	$fp,32,$31		# vars= 0, regs= 2/0, args= 16, gp= 8
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-32
	.cfi_def_cfa_offset 32
	sw	$31,28($sp)
	sw	$fp,24($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,32($fp)
	sw	$5,36($fp)
	sw	$6,40($fp)
	lw	$4,36($fp)
	.option	pic0
	jal	_ZSt11__addressofISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEPT_RS7_
	nop

	.option	pic2
	move	$3,$2
	lw	$2,32($fp)
	sw	$3,0($2)
	lw	$2,32($fp)
	lw	$3,40($fp)
	sw	$3,4($2)
	nop
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,28($sp)
	lw	$fp,24($sp)
	addiu	$sp,$sp,32
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC2ERS6_PS5_
	.cfi_endproc
$LFE4709:
	.size	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC2ERS6_PS5_, .-_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC2ERS6_PS5_
	.weak	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC1ERS6_PS5_
	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC1ERS6_PS5_ = _ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC2ERS6_PS5_
	.section	.text._ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE10deallocateERS6_PS5_j,"axG",@progbits,_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE10deallocateERS6_PS5_j,comdat
	.align	2
	.weak	_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE10deallocateERS6_PS5_j
$LFB4711 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE10deallocateERS6_PS5_j
	.type	_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE10deallocateERS6_PS5_j, @function
_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE10deallocateERS6_PS5_j:
	.frame	$fp,32,$31		# vars= 0, regs= 2/0, args= 16, gp= 8
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-32
	.cfi_def_cfa_offset 32
	sw	$31,28($sp)
	sw	$fp,24($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,32($fp)
	sw	$5,36($fp)
	sw	$6,40($fp)
	lw	$6,40($fp)
	lw	$5,36($fp)
	lw	$4,32($fp)
	.option	pic0
	jal	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE10deallocateEPS5_j
	nop

	.option	pic2
	nop
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,28($sp)
	lw	$fp,24($sp)
	addiu	$sp,$sp,32
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE10deallocateERS6_PS5_j
	.cfi_endproc
$LFE4711:
	.size	_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE10deallocateERS6_PS5_j, .-_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE10deallocateERS6_PS5_j
	.section	.text._ZSt12__to_addressISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEPT_S7_,"axG",@progbits,_ZSt12__to_addressISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEPT_S7_,comdat
	.align	2
	.weak	_ZSt12__to_addressISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEPT_S7_
$LFB4712 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZSt12__to_addressISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEPT_S7_
	.type	_ZSt12__to_addressISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEPT_S7_, @function
_ZSt12__to_addressISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEPT_S7_:
	.frame	$fp,8,$31		# vars= 0, regs= 1/0, args= 0, gp= 0
	.mask	0x40000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-8
	.cfi_def_cfa_offset 8
	sw	$fp,4($sp)
	.cfi_offset 30, -4
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,8($fp)
	lw	$2,8($fp)
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$fp,4($sp)
	addiu	$sp,$sp,8
	.cfi_restore 30
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZSt12__to_addressISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEPT_S7_
	.cfi_endproc
$LFE4712:
	.size	_ZSt12__to_addressISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEPT_S7_, .-_ZSt12__to_addressISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEPT_S7_
	.section	.text._ZN9__gnu_cxx13new_allocatorI6sphereEC2ERKS2_,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorI6sphereEC5ERKS2_,comdat
	.align	2
	.weak	_ZN9__gnu_cxx13new_allocatorI6sphereEC2ERKS2_
$LFB4714 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZN9__gnu_cxx13new_allocatorI6sphereEC2ERKS2_
	.type	_ZN9__gnu_cxx13new_allocatorI6sphereEC2ERKS2_, @function
_ZN9__gnu_cxx13new_allocatorI6sphereEC2ERKS2_:
	.frame	$fp,8,$31		# vars= 0, regs= 1/0, args= 0, gp= 0
	.mask	0x40000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-8
	.cfi_def_cfa_offset 8
	sw	$fp,4($sp)
	.cfi_offset 30, -4
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,8($fp)
	sw	$5,12($fp)
	nop
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$fp,4($sp)
	addiu	$sp,$sp,8
	.cfi_restore 30
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZN9__gnu_cxx13new_allocatorI6sphereEC2ERKS2_
	.cfi_endproc
$LFE4714:
	.size	_ZN9__gnu_cxx13new_allocatorI6sphereEC2ERKS2_, .-_ZN9__gnu_cxx13new_allocatorI6sphereEC2ERKS2_
	.weak	_ZN9__gnu_cxx13new_allocatorI6sphereEC1ERKS2_
	_ZN9__gnu_cxx13new_allocatorI6sphereEC1ERKS2_ = _ZN9__gnu_cxx13new_allocatorI6sphereEC2ERKS2_
	.section	.text._ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC2Ev,"axG",@progbits,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC5Ev,comdat
	.align	2
	.weak	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC2Ev
$LFB4717 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC2Ev
	.type	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC2Ev, @function
_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC2Ev:
	.frame	$fp,8,$31		# vars= 0, regs= 1/0, args= 0, gp= 0
	.mask	0x40000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-8
	.cfi_def_cfa_offset 8
	sw	$fp,4($sp)
	.cfi_offset 30, -4
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,8($fp)
	lui	$2,%hi(_ZTVSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE+8)
	addiu	$3,$2,%lo(_ZTVSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE+8)
	lw	$2,8($fp)
	sw	$3,0($2)
	lw	$2,8($fp)
	li	$3,1			# 0x1
	sw	$3,4($2)
	lw	$2,8($fp)
	li	$3,1			# 0x1
	sw	$3,8($2)
	nop
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$fp,4($sp)
	addiu	$sp,$sp,8
	.cfi_restore 30
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC2Ev
	.cfi_endproc
$LFE4717:
	.size	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC2Ev, .-_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC2Ev
	.weak	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC1Ev
	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC1Ev = _ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC2Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC2ES1_,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC5ES1_,comdat
	.align	2
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC2ES1_
$LFB4720 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC2ES1_
	.type	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC2ES1_, @function
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC2ES1_:
	.frame	$fp,32,$31		# vars= 0, regs= 2/0, args= 16, gp= 8
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-32
	.cfi_def_cfa_offset 32
	sw	$31,28($sp)
	sw	$fp,24($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,32($fp)
	sw	$5,36($fp)
	lw	$5,36($fp)
	lw	$4,32($fp)
	.option	pic0
	jal	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC2ERKS1_
	nop

	.option	pic2
	nop
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,28($sp)
	lw	$fp,24($sp)
	addiu	$sp,$sp,32
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC2ES1_
	.cfi_endproc
$LFE4720:
	.size	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC2ES1_, .-_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC2ES1_
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC1ES1_
	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC1ES1_ = _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC2ES1_
	.section	.text._ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3dEEEvRS1_PT_DpOT0_,"axG",@progbits,_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3dEEEvRS1_PT_DpOT0_,comdat
	.align	2
	.weak	_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3dEEEvRS1_PT_DpOT0_
$LFB4722 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3dEEEvRS1_PT_DpOT0_
	.type	_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3dEEEvRS1_PT_DpOT0_, @function
_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3dEEEvRS1_PT_DpOT0_:
	.frame	$fp,40,$31		# vars= 0, regs= 3/0, args= 16, gp= 8
	.mask	0xc0010000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-40
	.cfi_def_cfa_offset 40
	sw	$31,36($sp)
	sw	$fp,32($sp)
	sw	$16,28($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	.cfi_offset 16, -12
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,40($fp)
	sw	$5,44($fp)
	sw	$6,48($fp)
	sw	$7,52($fp)
	lw	$4,48($fp)
	.option	pic0
	jal	_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
	nop

	.option	pic2
	move	$16,$2
	lw	$4,52($fp)
	.option	pic0
	jal	_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE
	nop

	.option	pic2
	move	$7,$2
	move	$6,$16
	lw	$5,44($fp)
	lw	$4,40($fp)
	.option	pic0
	jal	_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_
	nop

	.option	pic2
	nop
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,36($sp)
	lw	$fp,32($sp)
	lw	$16,28($sp)
	addiu	$sp,$sp,40
	.cfi_restore 16
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3dEEEvRS1_PT_DpOT0_
	.cfi_endproc
$LFE4722:
	.size	_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3dEEEvRS1_PT_DpOT0_, .-_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3dEEEvRS1_PT_DpOT0_
	.section	.text._ZN9__gnu_cxx16__aligned_bufferI6sphereE6_M_ptrEv,"axG",@progbits,_ZN9__gnu_cxx16__aligned_bufferI6sphereE6_M_ptrEv,comdat
	.align	2
	.weak	_ZN9__gnu_cxx16__aligned_bufferI6sphereE6_M_ptrEv
$LFB4723 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZN9__gnu_cxx16__aligned_bufferI6sphereE6_M_ptrEv
	.type	_ZN9__gnu_cxx16__aligned_bufferI6sphereE6_M_ptrEv, @function
_ZN9__gnu_cxx16__aligned_bufferI6sphereE6_M_ptrEv:
	.frame	$fp,32,$31		# vars= 0, regs= 2/0, args= 16, gp= 8
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-32
	.cfi_def_cfa_offset 32
	sw	$31,28($sp)
	sw	$fp,24($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,32($fp)
	lw	$4,32($fp)
	.option	pic0
	jal	_ZN9__gnu_cxx16__aligned_bufferI6sphereE7_M_addrEv
	nop

	.option	pic2
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,28($sp)
	lw	$fp,24($sp)
	addiu	$sp,$sp,32
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZN9__gnu_cxx16__aligned_bufferI6sphereE6_M_ptrEv
	.cfi_endproc
$LFE4723:
	.size	_ZN9__gnu_cxx16__aligned_bufferI6sphereE6_M_ptrEv, .-_ZN9__gnu_cxx16__aligned_bufferI6sphereE6_M_ptrEv
	.section	.text._ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3iEEEvRS1_PT_DpOT0_,"axG",@progbits,_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3iEEEvRS1_PT_DpOT0_,comdat
	.align	2
	.weak	_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3iEEEvRS1_PT_DpOT0_
$LFB4724 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3iEEEvRS1_PT_DpOT0_
	.type	_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3iEEEvRS1_PT_DpOT0_, @function
_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3iEEEvRS1_PT_DpOT0_:
	.frame	$fp,40,$31		# vars= 0, regs= 3/0, args= 16, gp= 8
	.mask	0xc0010000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-40
	.cfi_def_cfa_offset 40
	sw	$31,36($sp)
	sw	$fp,32($sp)
	sw	$16,28($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	.cfi_offset 16, -12
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,40($fp)
	sw	$5,44($fp)
	sw	$6,48($fp)
	sw	$7,52($fp)
	lw	$4,48($fp)
	.option	pic0
	jal	_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
	nop

	.option	pic2
	move	$16,$2
	lw	$4,52($fp)
	.option	pic0
	jal	_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE
	nop

	.option	pic2
	move	$7,$2
	move	$6,$16
	lw	$5,44($fp)
	lw	$4,40($fp)
	.option	pic0
	jal	_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_
	nop

	.option	pic2
	nop
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,36($sp)
	lw	$fp,32($sp)
	lw	$16,28($sp)
	addiu	$sp,$sp,40
	.cfi_restore 16
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3iEEEvRS1_PT_DpOT0_
	.cfi_endproc
$LFE4724:
	.size	_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3iEEEvRS1_PT_DpOT0_, .-_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3iEEEvRS1_PT_DpOT0_
	.section	.text._ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8allocateEjPKv,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8allocateEjPKv,comdat
	.align	2
	.weak	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8allocateEjPKv
$LFB4727 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8allocateEjPKv
	.type	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8allocateEjPKv, @function
_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8allocateEjPKv:
	.frame	$fp,32,$31		# vars= 0, regs= 2/0, args= 16, gp= 8
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-32
	.cfi_def_cfa_offset 32
	sw	$31,28($sp)
	sw	$fp,24($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	move	$fp,$sp
	.cfi_def_cfa_register 30
	lui	$28,%hi(__gnu_local_gp)
	addiu	$28,$28,%lo(__gnu_local_gp)
	.cprestore	16
	sw	$4,32($fp)
	sw	$5,36($fp)
	sw	$6,40($fp)
	lw	$4,32($fp)
	.option	pic0
	jal	_ZNK9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8max_sizeEv
	nop

	.option	pic2
	lw	$28,16($fp)
	move	$3,$2
	lw	$2,36($fp)
	sltu	$2,$3,$2
	andi	$2,$2,0x00ff
	beq	$2,$0,$L261
	nop

	lw	$2,%call16(_ZSt17__throw_bad_allocv)($28)
	move	$25,$2
	.reloc	1f,R_MIPS_JALR,_ZSt17__throw_bad_allocv
1:	jalr	$25
	nop

$L261:
	lw	$2,36($fp)
	sll	$2,$2,6
	move	$4,$2
	lw	$2,%call16(_Znwj)($28)
	move	$25,$2
	.reloc	1f,R_MIPS_JALR,_Znwj
1:	jalr	$25
	nop

	lw	$28,16($fp)
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,28($sp)
	lw	$fp,24($sp)
	addiu	$sp,$sp,32
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8allocateEjPKv
	.cfi_endproc
$LFE4727:
	.size	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8allocateEjPKv, .-_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8allocateEjPKv
	.section	.text._ZSt11__addressofISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEPT_RS7_,"axG",@progbits,_ZSt11__addressofISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEPT_RS7_,comdat
	.align	2
	.weak	_ZSt11__addressofISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEPT_RS7_
$LFB4728 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZSt11__addressofISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEPT_RS7_
	.type	_ZSt11__addressofISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEPT_RS7_, @function
_ZSt11__addressofISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEPT_RS7_:
	.frame	$fp,8,$31		# vars= 0, regs= 1/0, args= 0, gp= 0
	.mask	0x40000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-8
	.cfi_def_cfa_offset 8
	sw	$fp,4($sp)
	.cfi_offset 30, -4
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,8($fp)
	lw	$2,8($fp)
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$fp,4($sp)
	addiu	$sp,$sp,8
	.cfi_restore 30
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZSt11__addressofISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEPT_RS7_
	.cfi_endproc
$LFE4728:
	.size	_ZSt11__addressofISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEPT_RS7_, .-_ZSt11__addressofISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEPT_RS7_
	.section	.text._ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE10deallocateEPS5_j,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE10deallocateEPS5_j,comdat
	.align	2
	.weak	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE10deallocateEPS5_j
$LFB4729 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE10deallocateEPS5_j
	.type	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE10deallocateEPS5_j, @function
_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE10deallocateEPS5_j:
	.frame	$fp,32,$31		# vars= 0, regs= 2/0, args= 16, gp= 8
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-32
	.cfi_def_cfa_offset 32
	sw	$31,28($sp)
	sw	$fp,24($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	move	$fp,$sp
	.cfi_def_cfa_register 30
	lui	$28,%hi(__gnu_local_gp)
	addiu	$28,$28,%lo(__gnu_local_gp)
	.cprestore	16
	sw	$4,32($fp)
	sw	$5,36($fp)
	sw	$6,40($fp)
	lw	$4,36($fp)
	lw	$2,%call16(_ZdlPv)($28)
	move	$25,$2
	.reloc	1f,R_MIPS_JALR,_ZdlPv
1:	jalr	$25
	nop

	lw	$28,16($fp)
	nop
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,28($sp)
	lw	$fp,24($sp)
	addiu	$sp,$sp,32
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE10deallocateEPS5_j
	.cfi_endproc
$LFE4729:
	.size	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE10deallocateEPS5_j, .-_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE10deallocateEPS5_j
	.section	.text._ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC2ERKS1_,"axG",@progbits,_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC5ERKS1_,comdat
	.align	2
	.weak	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC2ERKS1_
$LFB4731 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC2ERKS1_
	.type	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC2ERKS1_, @function
_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC2ERKS1_:
	.frame	$fp,32,$31		# vars= 0, regs= 2/0, args= 16, gp= 8
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-32
	.cfi_def_cfa_offset 32
	sw	$31,28($sp)
	sw	$fp,24($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,32($fp)
	sw	$5,36($fp)
	lw	$5,36($fp)
	lw	$4,32($fp)
	.option	pic0
	jal	_ZNSaI6sphereEC2ERKS0_
	nop

	.option	pic2
	nop
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,28($sp)
	lw	$fp,24($sp)
	addiu	$sp,$sp,32
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC2ERKS1_
	.cfi_endproc
$LFE4731:
	.size	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC2ERKS1_, .-_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC2ERKS1_
	.weak	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC1ERKS1_
	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC1ERKS1_ = _ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC2ERKS1_
	.section	.text._ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_,comdat
	.align	2
	.weak	_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_
$LFB4733 = .
	.cfi_startproc
	.cfi_personality 0x80,DW.ref.__gxx_personality_v0
	.cfi_lsda 0,$LLSDA4733
	.set	nomips16
	.set	nomicromips
	.ent	_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_
	.type	_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_, @function
_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_:
	.frame	$fp,128,$31		# vars= 48, regs= 5/2, args= 40, gp= 8
	.mask	0xc0070000,-12
	.fmask	0x00300000,-8
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-128
	.cfi_def_cfa_offset 128
	sw	$31,116($sp)
	sw	$fp,112($sp)
	sw	$18,108($sp)
	sw	$17,104($sp)
	sw	$16,100($sp)
	sdc1	$f20,120($sp)
	.cfi_offset 31, -12
	.cfi_offset 30, -16
	.cfi_offset 18, -20
	.cfi_offset 17, -24
	.cfi_offset 16, -28
	.cfi_offset 52, -4
	.cfi_offset 53, -8
	move	$fp,$sp
	.cfi_def_cfa_register 30
	lui	$28,%hi(__gnu_local_gp)
	addiu	$28,$28,%lo(__gnu_local_gp)
	.cprestore	40
	sw	$4,60($fp)
	sw	$5,56($fp)
	sw	$6,52($fp)
	sw	$7,48($fp)
	lw	$2,%got(__stack_chk_guard)($28)
	lw	$2,0($2)
	sw	$2,92($fp)
	lw	$4,52($fp)
	.option	pic0
	jal	_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
	nop

	.option	pic2
	lw	$28,40($fp)
	lw	$7,0($2)
	lw	$6,4($2)
	lw	$5,8($2)
	lw	$4,12($2)
	lw	$3,16($2)
	lw	$2,20($2)
	sw	$7,64($fp)
	sw	$6,68($fp)
	sw	$5,72($fp)
	sw	$4,76($fp)
	sw	$3,80($fp)
	sw	$2,84($fp)
	lw	$4,48($fp)
	.option	pic0
	jal	_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE
	nop

	.option	pic2
	lw	$28,40($fp)
	ldc1	$f20,0($2)
	lw	$16,56($fp)
	move	$5,$16
	li	$4,48			# 0x30
	.option	pic0
	jal	_ZnwjPv
	nop

	.option	pic2
	lw	$28,40($fp)
	move	$17,$2
	sdc1	$f20,32($sp)
	lw	$5,72($fp)
	lw	$4,76($fp)
	lw	$3,80($fp)
	lw	$2,84($fp)
	sw	$5,16($sp)
	sw	$4,20($sp)
	sw	$3,24($sp)
	sw	$2,28($sp)
	lw	$6,64($fp)
	lw	$7,68($fp)
	move	$4,$17
	lw	$2,%call16(_ZN6sphereC1E4vec3d)($28)
	move	$25,$2
$LEHB30 = .
	.reloc	1f,R_MIPS_JALR,_ZN6sphereC1E4vec3d
1:	jalr	$25
	nop

$LEHE30 = .
	lw	$28,40($fp)
	nop
	lw	$2,%got(__stack_chk_guard)($28)
	lw	$3,92($fp)
	lw	$2,0($2)
	beq	$3,$2,$L269
	nop

	.option	pic0
	b	$L271
	nop

	.option	pic2
$L270:
	lw	$28,40($fp)
	move	$18,$4
	move	$5,$16
	move	$4,$17
	.option	pic0
	jal	_ZdlPvS_
	nop

	.option	pic2
	lw	$28,40($fp)
	move	$2,$18
	move	$4,$2
	lw	$2,%call16(_Unwind_Resume)($28)
	move	$25,$2
$LEHB31 = .
	.reloc	1f,R_MIPS_JALR,_Unwind_Resume
1:	jalr	$25
	nop

$LEHE31 = .
$L271:
	lw	$2,%call16(__stack_chk_fail)($28)
	move	$25,$2
	.reloc	1f,R_MIPS_JALR,__stack_chk_fail
1:	jalr	$25
	nop

$L269:
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,116($sp)
	lw	$fp,112($sp)
	lw	$18,108($sp)
	lw	$17,104($sp)
	lw	$16,100($sp)
	ldc1	$f20,120($sp)
	addiu	$sp,$sp,128
	.cfi_restore 52
	.cfi_restore 53
	.cfi_restore 16
	.cfi_restore 17
	.cfi_restore 18
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_
	.cfi_endproc
$LFE4733:
	.section	.gcc_except_table
$LLSDA4733:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 $LLSDACSE4733-$LLSDACSB4733
$LLSDACSB4733:
	.uleb128 $LEHB30-$LFB4733
	.uleb128 $LEHE30-$LEHB30
	.uleb128 $L270-$LFB4733
	.uleb128 0
	.uleb128 $LEHB31-$LFB4733
	.uleb128 $LEHE31-$LEHB31
	.uleb128 0
	.uleb128 0
$LLSDACSE4733:
	.section	.text._ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_,comdat
	.size	_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_, .-_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_
	.section	.text._ZN9__gnu_cxx16__aligned_bufferI6sphereE7_M_addrEv,"axG",@progbits,_ZN9__gnu_cxx16__aligned_bufferI6sphereE7_M_addrEv,comdat
	.align	2
	.weak	_ZN9__gnu_cxx16__aligned_bufferI6sphereE7_M_addrEv
$LFB4734 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZN9__gnu_cxx16__aligned_bufferI6sphereE7_M_addrEv
	.type	_ZN9__gnu_cxx16__aligned_bufferI6sphereE7_M_addrEv, @function
_ZN9__gnu_cxx16__aligned_bufferI6sphereE7_M_addrEv:
	.frame	$fp,8,$31		# vars= 0, regs= 1/0, args= 0, gp= 0
	.mask	0x40000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-8
	.cfi_def_cfa_offset 8
	sw	$fp,4($sp)
	.cfi_offset 30, -4
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,8($fp)
	lw	$2,8($fp)
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$fp,4($sp)
	addiu	$sp,$sp,8
	.cfi_restore 30
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZN9__gnu_cxx16__aligned_bufferI6sphereE7_M_addrEv
	.cfi_endproc
$LFE4734:
	.size	_ZN9__gnu_cxx16__aligned_bufferI6sphereE7_M_addrEv, .-_ZN9__gnu_cxx16__aligned_bufferI6sphereE7_M_addrEv
	.section	.text._ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_,comdat
	.align	2
	.weak	_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_
$LFB4735 = .
	.cfi_startproc
	.cfi_personality 0x80,DW.ref.__gxx_personality_v0
	.cfi_lsda 0,$LLSDA4735
	.set	nomips16
	.set	nomicromips
	.ent	_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_
	.type	_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_, @function
_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_:
	.frame	$fp,128,$31		# vars= 48, regs= 5/2, args= 40, gp= 8
	.mask	0xc0070000,-12
	.fmask	0x00300000,-8
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-128
	.cfi_def_cfa_offset 128
	sw	$31,116($sp)
	sw	$fp,112($sp)
	sw	$18,108($sp)
	sw	$17,104($sp)
	sw	$16,100($sp)
	sdc1	$f20,120($sp)
	.cfi_offset 31, -12
	.cfi_offset 30, -16
	.cfi_offset 18, -20
	.cfi_offset 17, -24
	.cfi_offset 16, -28
	.cfi_offset 52, -4
	.cfi_offset 53, -8
	move	$fp,$sp
	.cfi_def_cfa_register 30
	lui	$28,%hi(__gnu_local_gp)
	addiu	$28,$28,%lo(__gnu_local_gp)
	.cprestore	40
	sw	$4,60($fp)
	sw	$5,56($fp)
	sw	$6,52($fp)
	sw	$7,48($fp)
	lw	$2,%got(__stack_chk_guard)($28)
	lw	$2,0($2)
	sw	$2,92($fp)
	lw	$4,52($fp)
	.option	pic0
	jal	_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
	nop

	.option	pic2
	lw	$28,40($fp)
	lw	$7,0($2)
	lw	$6,4($2)
	lw	$5,8($2)
	lw	$4,12($2)
	lw	$3,16($2)
	lw	$2,20($2)
	sw	$7,64($fp)
	sw	$6,68($fp)
	sw	$5,72($fp)
	sw	$4,76($fp)
	sw	$3,80($fp)
	sw	$2,84($fp)
	lw	$4,48($fp)
	.option	pic0
	jal	_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE
	nop

	.option	pic2
	lw	$28,40($fp)
	lw	$2,0($2)
	mtc1	$2,$f0
	cvt.d.w	$f20,$f0
	lw	$16,56($fp)
	move	$5,$16
	li	$4,48			# 0x30
	.option	pic0
	jal	_ZnwjPv
	nop

	.option	pic2
	lw	$28,40($fp)
	move	$17,$2
	sdc1	$f20,32($sp)
	lw	$5,72($fp)
	lw	$4,76($fp)
	lw	$3,80($fp)
	lw	$2,84($fp)
	sw	$5,16($sp)
	sw	$4,20($sp)
	sw	$3,24($sp)
	sw	$2,28($sp)
	lw	$6,64($fp)
	lw	$7,68($fp)
	move	$4,$17
	lw	$2,%call16(_ZN6sphereC1E4vec3d)($28)
	move	$25,$2
$LEHB32 = .
	.reloc	1f,R_MIPS_JALR,_ZN6sphereC1E4vec3d
1:	jalr	$25
	nop

$LEHE32 = .
	lw	$28,40($fp)
	nop
	lw	$2,%got(__stack_chk_guard)($28)
	lw	$3,92($fp)
	lw	$2,0($2)
	beq	$3,$2,$L276
	nop

	.option	pic0
	b	$L278
	nop

	.option	pic2
$L277:
	lw	$28,40($fp)
	move	$18,$4
	move	$5,$16
	move	$4,$17
	.option	pic0
	jal	_ZdlPvS_
	nop

	.option	pic2
	lw	$28,40($fp)
	move	$2,$18
	move	$4,$2
	lw	$2,%call16(_Unwind_Resume)($28)
	move	$25,$2
$LEHB33 = .
	.reloc	1f,R_MIPS_JALR,_Unwind_Resume
1:	jalr	$25
	nop

$LEHE33 = .
$L278:
	lw	$2,%call16(__stack_chk_fail)($28)
	move	$25,$2
	.reloc	1f,R_MIPS_JALR,__stack_chk_fail
1:	jalr	$25
	nop

$L276:
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,116($sp)
	lw	$fp,112($sp)
	lw	$18,108($sp)
	lw	$17,104($sp)
	lw	$16,100($sp)
	ldc1	$f20,120($sp)
	addiu	$sp,$sp,128
	.cfi_restore 52
	.cfi_restore 53
	.cfi_restore 16
	.cfi_restore 17
	.cfi_restore 18
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_
	.cfi_endproc
$LFE4735:
	.section	.gcc_except_table
$LLSDA4735:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 $LLSDACSE4735-$LLSDACSB4735
$LLSDACSB4735:
	.uleb128 $LEHB32-$LFB4735
	.uleb128 $LEHE32-$LEHB32
	.uleb128 $L277-$LFB4735
	.uleb128 0
	.uleb128 $LEHB33-$LFB4735
	.uleb128 $LEHE33-$LEHB33
	.uleb128 0
	.uleb128 0
$LLSDACSE4735:
	.section	.text._ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_,comdat
	.size	_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_, .-_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_
	.section	.text._ZNK9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8max_sizeEv,"axG",@progbits,_ZNK9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8max_sizeEv,comdat
	.align	2
	.weak	_ZNK9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8max_sizeEv
$LFB4736 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZNK9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8max_sizeEv
	.type	_ZNK9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8max_sizeEv, @function
_ZNK9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8max_sizeEv:
	.frame	$fp,8,$31		# vars= 0, regs= 1/0, args= 0, gp= 0
	.mask	0x40000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-8
	.cfi_def_cfa_offset 8
	sw	$fp,4($sp)
	.cfi_offset 30, -4
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,8($fp)
	li	$2,33488896			# 0x1ff0000
	ori	$2,$2,0xffff
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$fp,4($sp)
	addiu	$sp,$sp,8
	.cfi_restore 30
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZNK9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8max_sizeEv
	.cfi_endproc
$LFE4736:
	.size	_ZNK9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8max_sizeEv, .-_ZNK9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8max_sizeEv
	.weak	_ZTVSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE
	.section	.data.rel.ro.local._ZTVSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE,"awG",@progbits,_ZTVSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align	2
	.type	_ZTVSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTVSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE, 28
_ZTVSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE:
	.word	0
	.word	_ZTISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE
	.word	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.word	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.word	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.word	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.word	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.weak	_ZTVSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE
	.section	.data.rel.ro._ZTVSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE,"awG",@progbits,_ZTVSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align	2
	.type	_ZTVSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTVSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE, 28
_ZTVSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE:
	.word	0
	.word	_ZTISt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE
	.word	0
	.word	0
	.word	__cxa_pure_virtual
	.word	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.word	__cxa_pure_virtual
	.weak	_ZTISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE
	.section	.data.rel.ro._ZTISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE,"awG",@progbits,_ZTISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align	2
	.type	_ZTISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE, 12
_ZTISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE:
	.word	_ZTVN10__cxxabiv120__si_class_type_infoE+8
	.word	_ZTSSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE
	.word	_ZTISt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE
	.weak	_ZTSSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE
	.section	.rodata._ZTSSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE,"aG",@progbits,_ZTSSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align	2
	.type	_ZTSSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTSSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE, 73
_ZTSSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE:
	.ascii	"St23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12"
	.ascii	"_Lock_policyE2EE\000"
	.weak	_ZTISt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE
	.section	.data.rel.ro._ZTISt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE,"awG",@progbits,_ZTISt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align	2
	.type	_ZTISt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTISt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE, 12
_ZTISt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE:
	.word	_ZTVN10__cxxabiv120__si_class_type_infoE+8
	.word	_ZTSSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE
	.word	_ZTISt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE
	.weak	_ZTSSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE
	.section	.rodata._ZTSSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE,"aG",@progbits,_ZTSSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align	2
	.type	_ZTSSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTSSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE, 52
_ZTSSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE:
	.ascii	"St16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE\000"
	.text
	.align	2
$LFB4753 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_Z41__static_initialization_and_destruction_0ii
	.type	_Z41__static_initialization_and_destruction_0ii, @function
_Z41__static_initialization_and_destruction_0ii:
	.frame	$fp,32,$31		# vars= 0, regs= 2/0, args= 16, gp= 8
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-32
	.cfi_def_cfa_offset 32
	sw	$31,28($sp)
	sw	$fp,24($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	move	$fp,$sp
	.cfi_def_cfa_register 30
	lui	$28,%hi(__gnu_local_gp)
	addiu	$28,$28,%lo(__gnu_local_gp)
	.cprestore	16
	sw	$4,32($fp)
	sw	$5,36($fp)
	lw	$3,32($fp)
	li	$2,1			# 0x1
	bne	$3,$2,$L283
	nop

	lw	$3,36($fp)
	li	$2,65535			# 0xffff
	bne	$3,$2,$L283
	nop

	lui	$2,%hi(_ZStL8__ioinit)
	addiu	$4,$2,%lo(_ZStL8__ioinit)
	lw	$2,%call16(_ZNSt8ios_base4InitC1Ev)($28)
	move	$25,$2
	.reloc	1f,R_MIPS_JALR,_ZNSt8ios_base4InitC1Ev
1:	jalr	$25
	nop

	lw	$28,16($fp)
	lui	$2,%hi(__dso_handle)
	addiu	$6,$2,%lo(__dso_handle)
	lui	$2,%hi(_ZStL8__ioinit)
	addiu	$5,$2,%lo(_ZStL8__ioinit)
	lw	$4,%got(_ZNSt8ios_base4InitD1Ev)($28)
	lw	$2,%call16(__cxa_atexit)($28)
	move	$25,$2
	.reloc	1f,R_MIPS_JALR,__cxa_atexit
1:	jalr	$25
	nop

	lw	$28,16($fp)
$L283:
	nop
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,28($sp)
	lw	$fp,24($sp)
	addiu	$sp,$sp,32
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_Z41__static_initialization_and_destruction_0ii
	.cfi_endproc
$LFE4753:
	.size	_Z41__static_initialization_and_destruction_0ii, .-_Z41__static_initialization_and_destruction_0ii
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align	2
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
$LFB4755 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.type	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED2Ev:
	.frame	$fp,32,$31		# vars= 0, regs= 2/0, args= 16, gp= 8
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-32
	.cfi_def_cfa_offset 32
	sw	$31,28($sp)
	sw	$fp,24($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,32($fp)
	lui	$2,%hi(_ZTVSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE+8)
	addiu	$3,$2,%lo(_ZTVSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE+8)
	lw	$2,32($fp)
	sw	$3,0($2)
	lw	$2,32($fp)
	addiu	$2,$2,16
	move	$4,$2
	.option	pic0
	jal	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD1Ev
	nop

	.option	pic2
	lw	$2,32($fp)
	move	$4,$2
	.option	pic0
	jal	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev
	nop

	.option	pic2
	nop
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,28($sp)
	lw	$fp,24($sp)
	addiu	$sp,$sp,32
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.cfi_endproc
$LFE4755:
	.size	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED1Ev
	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED1Ev = _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED0Ev,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align	2
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
$LFB4757 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.type	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED0Ev, @function
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED0Ev:
	.frame	$fp,32,$31		# vars= 0, regs= 2/0, args= 16, gp= 8
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-32
	.cfi_def_cfa_offset 32
	sw	$31,28($sp)
	sw	$fp,24($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	move	$fp,$sp
	.cfi_def_cfa_register 30
	lui	$28,%hi(__gnu_local_gp)
	addiu	$28,$28,%lo(__gnu_local_gp)
	.cprestore	16
	sw	$4,32($fp)
	lw	$4,32($fp)
	.option	pic0
	jal	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED1Ev
	nop

	.option	pic2
	lw	$28,16($fp)
	li	$5,64			# 0x40
	lw	$4,32($fp)
	lw	$2,%call16(_ZdlPvj)($28)
	move	$25,$2
	.reloc	1f,R_MIPS_JALR,_ZdlPvj
1:	jalr	$25
	nop

	lw	$28,16($fp)
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,28($sp)
	lw	$fp,24($sp)
	addiu	$sp,$sp,32
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.cfi_endproc
$LFE4757:
	.size	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED0Ev, .-_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,comdat
	.align	2
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
$LFB4758 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.type	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, @function
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv:
	.frame	$fp,40,$31		# vars= 0, regs= 3/0, args= 16, gp= 8
	.mask	0xc0010000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-40
	.cfi_def_cfa_offset 40
	sw	$31,36($sp)
	sw	$fp,32($sp)
	sw	$16,28($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	.cfi_offset 16, -12
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,40($fp)
	lw	$2,40($fp)
	addiu	$2,$2,16
	move	$4,$2
	.option	pic0
	jal	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_Impl8_M_allocEv
	nop

	.option	pic2
	move	$16,$2
	lw	$4,40($fp)
	.option	pic0
	jal	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv
	nop

	.option	pic2
	move	$5,$2
	move	$4,$16
	.option	pic0
	jal	_ZNSt16allocator_traitsISaI6sphereEE7destroyIS0_EEvRS1_PT_
	nop

	.option	pic2
	nop
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,36($sp)
	lw	$fp,32($sp)
	lw	$16,28($sp)
	addiu	$sp,$sp,40
	.cfi_restore 16
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.cfi_endproc
$LFE4758:
	.size	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, .-_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,comdat
	.align	2
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
$LFB4759 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.type	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, @function
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv:
	.frame	$fp,56,$31		# vars= 24, regs= 2/0, args= 16, gp= 8
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-56
	.cfi_def_cfa_offset 56
	sw	$31,52($sp)
	sw	$fp,48($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	move	$fp,$sp
	.cfi_def_cfa_register 30
	lui	$28,%hi(__gnu_local_gp)
	addiu	$28,$28,%lo(__gnu_local_gp)
	.cprestore	16
	sw	$4,28($fp)
	lw	$2,%got(__stack_chk_guard)($28)
	lw	$2,0($2)
	sw	$2,44($fp)
	lw	$2,28($fp)
	addiu	$2,$2,16
	move	$4,$2
	.option	pic0
	jal	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_Impl8_M_allocEv
	nop

	.option	pic2
	lw	$28,16($fp)
	addiu	$3,$fp,32
	move	$5,$2
	move	$4,$3
	.option	pic0
	jal	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC1IS0_EERKSaIT_E
	nop

	.option	pic2
	lw	$28,16($fp)
	addiu	$3,$fp,32
	addiu	$2,$fp,36
	lw	$6,28($fp)
	move	$5,$3
	move	$4,$2
	.option	pic0
	jal	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC1ERS6_PS5_
	nop

	.option	pic2
	lw	$28,16($fp)
	lw	$4,28($fp)
	.option	pic0
	jal	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED1Ev
	nop

	.option	pic2
	lw	$28,16($fp)
	addiu	$2,$fp,36
	move	$4,$2
	.option	pic0
	jal	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED1Ev
	nop

	.option	pic2
	lw	$28,16($fp)
	addiu	$2,$fp,32
	move	$4,$2
	.option	pic0
	jal	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED1Ev
	nop

	.option	pic2
	lw	$28,16($fp)
	nop
	lw	$2,%got(__stack_chk_guard)($28)
	lw	$3,44($fp)
	lw	$2,0($2)
	beq	$3,$2,$L288
	nop

	lw	$2,%call16(__stack_chk_fail)($28)
	move	$25,$2
	.reloc	1f,R_MIPS_JALR,__stack_chk_fail
1:	jalr	$25
	nop

$L288:
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,52($sp)
	lw	$fp,48($sp)
	addiu	$sp,$sp,56
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.cfi_endproc
$LFE4759:
	.size	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, .-_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,comdat
	.align	2
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
$LFB4760 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.type	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, @function
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info:
	.frame	$fp,40,$31		# vars= 8, regs= 2/0, args= 16, gp= 8
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-40
	.cfi_def_cfa_offset 40
	sw	$31,36($sp)
	sw	$fp,32($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,40($fp)
	sw	$5,44($fp)
	lw	$4,40($fp)
	.option	pic0
	jal	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv
	nop

	.option	pic2
	sw	$2,28($fp)
	.option	pic0
	jal	_ZNSt19_Sp_make_shared_tag5_S_tiEv
	nop

	.option	pic2
	move	$3,$2
	lw	$2,44($fp)
	beq	$2,$3,$L290
	nop

	lui	$2,%hi(_ZTISt19_Sp_make_shared_tag)
	addiu	$5,$2,%lo(_ZTISt19_Sp_make_shared_tag)
	lw	$4,44($fp)
	.option	pic0
	jal	_ZNKSt9type_infoeqERKS_
	nop

	.option	pic2
	beq	$2,$0,$L291
	nop

$L290:
	li	$2,1			# 0x1
	.option	pic0
	b	$L292
	nop

	.option	pic2
$L291:
	move	$2,$0
$L292:
	beq	$2,$0,$L293
	nop

	lw	$2,28($fp)
	.option	pic0
	b	$L294
	nop

	.option	pic2
$L293:
	move	$2,$0
$L294:
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,36($sp)
	lw	$fp,32($sp)
	addiu	$sp,$sp,40
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.cfi_endproc
$LFE4760:
	.size	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, .-_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_Impl8_M_allocEv,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_Impl8_M_allocEv,comdat
	.align	2
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_Impl8_M_allocEv
$LFB4761 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_Impl8_M_allocEv
	.type	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_Impl8_M_allocEv, @function
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_Impl8_M_allocEv:
	.frame	$fp,32,$31		# vars= 0, regs= 2/0, args= 16, gp= 8
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-32
	.cfi_def_cfa_offset 32
	sw	$31,28($sp)
	sw	$fp,24($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,32($fp)
	lw	$4,32($fp)
	.option	pic0
	jal	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EE6_S_getERS2_
	nop

	.option	pic2
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,28($sp)
	lw	$fp,24($sp)
	addiu	$sp,$sp,32
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_Impl8_M_allocEv
	.cfi_endproc
$LFE4761:
	.size	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_Impl8_M_allocEv, .-_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_Impl8_M_allocEv
	.section	.text._ZNSt16allocator_traitsISaI6sphereEE7destroyIS0_EEvRS1_PT_,"axG",@progbits,_ZNSt16allocator_traitsISaI6sphereEE7destroyIS0_EEvRS1_PT_,comdat
	.align	2
	.weak	_ZNSt16allocator_traitsISaI6sphereEE7destroyIS0_EEvRS1_PT_
$LFB4762 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZNSt16allocator_traitsISaI6sphereEE7destroyIS0_EEvRS1_PT_
	.type	_ZNSt16allocator_traitsISaI6sphereEE7destroyIS0_EEvRS1_PT_, @function
_ZNSt16allocator_traitsISaI6sphereEE7destroyIS0_EEvRS1_PT_:
	.frame	$fp,32,$31		# vars= 0, regs= 2/0, args= 16, gp= 8
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-32
	.cfi_def_cfa_offset 32
	sw	$31,28($sp)
	sw	$fp,24($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,32($fp)
	sw	$5,36($fp)
	lw	$5,36($fp)
	lw	$4,32($fp)
	.option	pic0
	jal	_ZN9__gnu_cxx13new_allocatorI6sphereE7destroyIS1_EEvPT_
	nop

	.option	pic2
	nop
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,28($sp)
	lw	$fp,24($sp)
	addiu	$sp,$sp,32
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZNSt16allocator_traitsISaI6sphereEE7destroyIS0_EEvRS1_PT_
	.cfi_endproc
$LFE4762:
	.size	_ZNSt16allocator_traitsISaI6sphereEE7destroyIS0_EEvRS1_PT_, .-_ZNSt16allocator_traitsISaI6sphereEE7destroyIS0_EEvRS1_PT_
	.section	.text._ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EE6_S_getERS2_,"axG",@progbits,_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EE6_S_getERS2_,comdat
	.align	2
	.weak	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EE6_S_getERS2_
$LFB4763 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EE6_S_getERS2_
	.type	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EE6_S_getERS2_, @function
_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EE6_S_getERS2_:
	.frame	$fp,8,$31		# vars= 0, regs= 1/0, args= 0, gp= 0
	.mask	0x40000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-8
	.cfi_def_cfa_offset 8
	sw	$fp,4($sp)
	.cfi_offset 30, -4
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,8($fp)
	lw	$2,8($fp)
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$fp,4($sp)
	addiu	$sp,$sp,8
	.cfi_restore 30
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EE6_S_getERS2_
	.cfi_endproc
$LFE4763:
	.size	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EE6_S_getERS2_, .-_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EE6_S_getERS2_
	.section	.text._ZN6sphereD2Ev,"axG",@progbits,_ZN6sphereD5Ev,comdat
	.align	2
	.weak	_ZN6sphereD2Ev
$LFB4766 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZN6sphereD2Ev
	.type	_ZN6sphereD2Ev, @function
_ZN6sphereD2Ev:
	.frame	$fp,32,$31		# vars= 0, regs= 2/0, args= 16, gp= 8
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-32
	.cfi_def_cfa_offset 32
	sw	$31,28($sp)
	sw	$fp,24($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	move	$fp,$sp
	.cfi_def_cfa_register 30
	lui	$28,%hi(__gnu_local_gp)
	addiu	$28,$28,%lo(__gnu_local_gp)
	.cprestore	16
	sw	$4,32($fp)
	lw	$2,%got(_ZTV6sphere)($28)
	addiu	$3,$2,8
	lw	$2,32($fp)
	sw	$3,0($2)
	lw	$2,32($fp)
	addiu	$2,$2,40
	move	$4,$2
	.option	pic0
	jal	_ZNSt10shared_ptrI8materialED1Ev
	nop

	.option	pic2
	lw	$28,16($fp)
	nop
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,28($sp)
	lw	$fp,24($sp)
	addiu	$sp,$sp,32
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZN6sphereD2Ev
	.cfi_endproc
$LFE4766:
	.size	_ZN6sphereD2Ev, .-_ZN6sphereD2Ev
	.weak	_ZN6sphereD1Ev
	_ZN6sphereD1Ev = _ZN6sphereD2Ev
	.section	.text._ZN9__gnu_cxx13new_allocatorI6sphereE7destroyIS1_EEvPT_,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorI6sphereE7destroyIS1_EEvPT_,comdat
	.align	2
	.weak	_ZN9__gnu_cxx13new_allocatorI6sphereE7destroyIS1_EEvPT_
$LFB4764 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZN9__gnu_cxx13new_allocatorI6sphereE7destroyIS1_EEvPT_
	.type	_ZN9__gnu_cxx13new_allocatorI6sphereE7destroyIS1_EEvPT_, @function
_ZN9__gnu_cxx13new_allocatorI6sphereE7destroyIS1_EEvPT_:
	.frame	$fp,32,$31		# vars= 0, regs= 2/0, args= 16, gp= 8
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-32
	.cfi_def_cfa_offset 32
	sw	$31,28($sp)
	sw	$fp,24($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	move	$fp,$sp
	.cfi_def_cfa_register 30
	sw	$4,32($fp)
	sw	$5,36($fp)
	lw	$4,36($fp)
	.option	pic0
	jal	_ZN6sphereD1Ev
	nop

	.option	pic2
	nop
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,28($sp)
	lw	$fp,24($sp)
	addiu	$sp,$sp,32
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZN9__gnu_cxx13new_allocatorI6sphereE7destroyIS1_EEvPT_
	.cfi_endproc
$LFE4764:
	.size	_ZN9__gnu_cxx13new_allocatorI6sphereE7destroyIS1_EEvPT_, .-_ZN9__gnu_cxx13new_allocatorI6sphereE7destroyIS1_EEvPT_
	.weak	_ZTISt19_Sp_make_shared_tag
	.section	.data.rel.ro._ZTISt19_Sp_make_shared_tag,"awG",@progbits,_ZTISt19_Sp_make_shared_tag,comdat
	.align	2
	.type	_ZTISt19_Sp_make_shared_tag, @object
	.size	_ZTISt19_Sp_make_shared_tag, 8
_ZTISt19_Sp_make_shared_tag:
	.word	_ZTVN10__cxxabiv117__class_type_infoE+8
	.word	_ZTSSt19_Sp_make_shared_tag
	.weak	_ZTSSt19_Sp_make_shared_tag
	.section	.rodata._ZTSSt19_Sp_make_shared_tag,"aG",@progbits,_ZTSSt19_Sp_make_shared_tag,comdat
	.align	2
	.type	_ZTSSt19_Sp_make_shared_tag, @object
	.size	_ZTSSt19_Sp_make_shared_tag, 24
_ZTSSt19_Sp_make_shared_tag:
	.ascii	"St19_Sp_make_shared_tag\000"
	.weak	_ZTISt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE
	.section	.data.rel.ro._ZTISt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE,"awG",@progbits,_ZTISt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align	2
	.type	_ZTISt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTISt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE, 8
_ZTISt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE:
	.word	_ZTVN10__cxxabiv117__class_type_infoE+8
	.word	_ZTSSt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE
	.weak	_ZTSSt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE
	.section	.rodata._ZTSSt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE,"aG",@progbits,_ZTSSt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align	2
	.type	_ZTSSt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTSSt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE, 47
_ZTSSt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE:
	.ascii	"St11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE\000"
	.text
	.align	2
$LFB4768 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_GLOBAL__sub_I_main
	.type	_GLOBAL__sub_I_main, @function
_GLOBAL__sub_I_main:
	.frame	$fp,32,$31		# vars= 0, regs= 2/0, args= 16, gp= 8
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-32
	.cfi_def_cfa_offset 32
	sw	$31,28($sp)
	sw	$fp,24($sp)
	.cfi_offset 31, -4
	.cfi_offset 30, -8
	move	$fp,$sp
	.cfi_def_cfa_register 30
	li	$5,65535			# 0xffff
	li	$4,1			# 0x1
	.option	pic0
	jal	_Z41__static_initialization_and_destruction_0ii
	nop

	.option	pic2
	move	$sp,$fp
	.cfi_def_cfa_register 29
	lw	$31,28($sp)
	lw	$fp,24($sp)
	addiu	$sp,$sp,32
	.cfi_restore 30
	.cfi_restore 31
	.cfi_def_cfa_offset 0
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_GLOBAL__sub_I_main
	.cfi_endproc
$LFE4768:
	.size	_GLOBAL__sub_I_main, .-_GLOBAL__sub_I_main
	.section	.ctors,"aw",@progbits
	.align	2
	.word	_GLOBAL__sub_I_main
	.weakref	_ZL28__gthrw___pthread_key_createPjPFvPvE,__pthread_key_create
	.rdata
	.align	3
$LC0:
	.word	1072693248
	.word	0
	.align	3
$LC1:
	.word	2146435072
	.word	0
	.align	3
$LC2:
	.word	1062232653
	.word	3539053052
	.align	3
$LC3:
	.word	1071644672
	.word	0
	.align	3
$LC4:
	.word	1072064102
	.word	1717986918
	.align	3
$LC5:
	.word	1073508807
	.word	477218588
	.align	3
$LC6:
	.word	-1074790400
	.word	0
	.align	3
$LC7:
	.word	-1067900928
	.word	0
	.align	3
$LC12:
	.word	1081667584
	.word	0
	.align	3
$LC13:
	.word	1080819712
	.word	0
	.align	3
$LC15:
	.word	1106247680
	.word	0
	.align	3
$LC16:
	.word	1072693247
	.word	4294967295
	.hidden	DW.ref.__gxx_personality_v0
	.weak	DW.ref.__gxx_personality_v0
	.section	.data.rel.local.DW.ref.__gxx_personality_v0,"awG",@progbits,DW.ref.__gxx_personality_v0,comdat
	.align	2
	.type	DW.ref.__gxx_personality_v0, @object
	.size	DW.ref.__gxx_personality_v0, 4
DW.ref.__gxx_personality_v0:
	.word	__gxx_personality_v0
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.4.0-1ubuntu1~20.04) 9.4.0"
