#NO_APP
	.file	"hello.cpp"
	.text
	.section	.rodata
	.type	_ZStL19piecewise_construct, @object
	.size	_ZStL19piecewise_construct, 1
_ZStL19piecewise_construct:
	.zero	1
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
.LC0:
	.string	"hello \n "
	.text
	.align	2
	.globl	main
	.type	main, @function
main:
.LFB1518:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	pea .LC0
	jsr printf
	addq.l #4,%sp
	clr.l %d0
	unlk %fp
	rts
	.cfi_endproc
.LFE1518:
	.size	main, .-main
	.align	2
	.type	_Z41__static_initialization_and_destruction_0ii, @function
_Z41__static_initialization_and_destruction_0ii:
.LFB1999:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	moveq #1,%d0
	cmp.l 8(%fp),%d0
	jne .L5
	cmp.l #65535,12(%fp)
	jne .L5
	pea _ZStL8__ioinit
	jsr _ZNSt8ios_base4InitC1Ev
	addq.l #4,%sp
	pea __dso_handle
	pea _ZStL8__ioinit
	pea _ZNSt8ios_base4InitD1Ev
	jsr __cxa_atexit
	lea (12,%sp),%sp
.L5:
	nop
	unlk %fp
	rts
	.cfi_endproc
.LFE1999:
	.size	_Z41__static_initialization_and_destruction_0ii, .-_Z41__static_initialization_and_destruction_0ii
	.align	2
	.type	_GLOBAL__sub_I_main, @function
_GLOBAL__sub_I_main:
.LFB2000:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l #65535,-(%sp)
	pea 1.w
	jsr _Z41__static_initialization_and_destruction_0ii
	addq.l #8,%sp
	unlk %fp
	rts
	.cfi_endproc
.LFE2000:
	.size	_GLOBAL__sub_I_main, .-_GLOBAL__sub_I_main
	.section	.ctors,"aw",@progbits
	.align	4
	.long	_GLOBAL__sub_I_main
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.4.0-1ubuntu1~20.04) 9.4.0"
	.section	.note.GNU-stack,"",@progbits
