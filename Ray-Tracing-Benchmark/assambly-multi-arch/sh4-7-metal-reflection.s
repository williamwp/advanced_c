	.file	"7-metal-reflection.cpp"
	.text
	.little
	.text
	.section	.rodata
	.type	_ZStL19piecewise_construct, @object
	.size	_ZStL19piecewise_construct, 1
_ZStL19piecewise_construct:
	.zero	1
	.section	.text._ZNKSt9type_infoeqERKS_,"axG",@progbits,_ZNKSt9type_infoeqERKS_,comdat
	.align 1
	.weak	_ZNKSt9type_infoeqERKS_
	.type	_ZNKSt9type_infoeqERKS_, @function
_ZNKSt9type_infoeqERKS_:
.LFB727:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 17, -8
	add	#-8,r15
	.cfi_def_cfa_offset 16
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-56,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-56,r1
	mov.l	r5,@(56,r1)
	mov	r14,r1
	add	#-56,r1
	mov.l	@(60,r1),r1
	mov.l	@(4,r1),r2
	mov	r14,r1
	add	#-56,r1
	mov.l	@(56,r1),r1
	mov.l	@(4,r1),r1
	cmp/eq	r1,r2
	bt	.L2
	mov	r14,r1
	add	#-56,r1
	mov.l	@(60,r1),r1
	mov.l	@(4,r1),r1
	mov.b	@r1,r2
	mov	#42,r1
	cmp/eq	r1,r2
	bt	.L3
	mov	r14,r1
	add	#-56,r1
	mov.l	@(60,r1),r1
	mov.l	@(4,r1),r2
	mov	r14,r1
	add	#-56,r1
	mov.l	@(56,r1),r1
	mov.l	@(4,r1),r1
	mov	r1,r5
	mov	r2,r4
	mov.l	.L6,r1
	jsr	@r1
	nop
	mov	r0,r1
	tst	r1,r1
	bf	.L3
.L2:
	mov	#1,r1
	bra	.L4
	nop
	.align 1
.L3:
	mov	#0,r1
.L4:
	mov	r1,r0
	add	#8,r14
	.cfi_def_cfa_offset 8
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
.L7:
	.align 2
.L6:
	.long	strcmp
	.cfi_endproc
.LFE727:
	.size	_ZNKSt9type_infoeqERKS_, .-_ZNKSt9type_infoeqERKS_
	.section	.text._ZnwjPv,"axG",@progbits,_ZnwjPv,comdat
	.align 1
	.weak	_ZnwjPv
	.type	_ZnwjPv, @function
_ZnwjPv:
.LFB769:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	add	#-8,r15
	.cfi_def_cfa_offset 12
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-56,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-56,r1
	mov.l	r5,@(56,r1)
	mov	r14,r1
	add	#-56,r1
	mov.l	@(56,r1),r1
	mov	r1,r0
	add	#8,r14
	.cfi_def_cfa_offset 4
	mov	r14,r15
	.cfi_def_cfa_register 15
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
	.cfi_endproc
.LFE769:
	.size	_ZnwjPv, .-_ZnwjPv
	.section	.text._ZdlPvS_,"axG",@progbits,_ZdlPvS_,comdat
	.align 1
	.weak	_ZdlPvS_
	.type	_ZdlPvS_, @function
_ZdlPvS_:
.LFB771:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	add	#-8,r15
	.cfi_def_cfa_offset 12
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-56,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-56,r1
	mov.l	r5,@(56,r1)
	nop
	add	#8,r14
	.cfi_def_cfa_offset 4
	mov	r14,r15
	.cfi_def_cfa_register 15
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
	.cfi_endproc
.LFE771:
	.size	_ZdlPvS_, .-_ZdlPvS_
	.section	.rodata
	.align 2
	.type	_ZZL18__gthread_active_pvE20__gthread_active_ptr, @object
	.size	_ZZL18__gthread_active_pvE20__gthread_active_ptr, 4
_ZZL18__gthread_active_pvE20__gthread_active_ptr:
	.long	_ZL28__gthrw___pthread_key_createPjPFvPvE
	.section	.text._ZL18__gthread_active_pv,"axG",@progbits,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv,comdat
	.align 1
	.type	_ZL18__gthread_active_pv, @function
_ZL18__gthread_active_pv:
.LFB945:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	#1,r1
	mov	r1,r2
	mov.l	.L14,r1
	tst	r1,r1
	bf	.L12
	mov	#0,r1
	mov	r1,r2
.L12:
	extu.b	r2,r1
	mov	r1,r0
	mov	r14,r15
	.cfi_def_cfa_register 15
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
.L15:
	.align 2
.L14:
	.long	_ZL28__gthrw___pthread_key_createPjPFvPvE
	.cfi_endproc
.LFE945:
	.size	_ZL18__gthread_active_pv, .-_ZL18__gthread_active_pv
	.section	.text._ZN9__gnu_cxxL18__exchange_and_addEPVii,"axG",@progbits,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv,comdat
	.align 1
	.type	_ZN9__gnu_cxxL18__exchange_and_addEPVii, @function
_ZN9__gnu_cxxL18__exchange_and_addEPVii:
.LFB974:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	add	#-8,r15
	.cfi_def_cfa_offset 12
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-56,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-56,r1
	mov.l	r5,@(56,r1)
	mov	r14,r1
	add	#-56,r1
	mov.l	@(56,r1),r3
	mov	r14,r1
	add	#-56,r1
	mov.l	@(60,r1),r2
		mova	1f,r0
	.align 2
	mov	r15,r1
	mov	#(0f-1f),r15
0:	mov.l	@r2,r7
	mov	r7,r6
	add	r3,r6
	mov.l	r6,@r2
1:	mov	r1,r15
	mov	r7,r1
	mov	r1,r0
	add	#8,r14
	.cfi_def_cfa_offset 4
	mov	r14,r15
	.cfi_def_cfa_register 15
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
	.cfi_endproc
.LFE974:
	.size	_ZN9__gnu_cxxL18__exchange_and_addEPVii, .-_ZN9__gnu_cxxL18__exchange_and_addEPVii
	.section	.text._ZN9__gnu_cxxL25__exchange_and_add_singleEPii,"axG",@progbits,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv,comdat
	.align 1
	.type	_ZN9__gnu_cxxL25__exchange_and_add_singleEPii, @function
_ZN9__gnu_cxxL25__exchange_and_add_singleEPii:
.LFB976:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	add	#-12,r15
	.cfi_def_cfa_offset 16
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-52,r1
	mov.l	r4,@(56,r1)
	mov	r14,r1
	add	#-52,r1
	mov.l	r5,@(52,r1)
	mov	r14,r1
	add	#-52,r1
	mov	r14,r2
	add	#-52,r2
	mov.l	@(56,r2),r2
	mov.l	@r2,r2
	mov.l	r2,@(60,r1)
	mov	r14,r1
	add	#-52,r1
	mov.l	@(56,r1),r1
	mov.l	@r1,r2
	mov	r14,r1
	add	#-52,r1
	mov.l	@(52,r1),r1
	add	r1,r2
	mov	r14,r1
	add	#-52,r1
	mov.l	@(56,r1),r1
	mov.l	r2,@r1
	mov	r14,r1
	add	#-52,r1
	mov.l	@(60,r1),r1
	mov	r1,r0
	add	#12,r14
	.cfi_def_cfa_offset 4
	mov	r14,r15
	.cfi_def_cfa_register 15
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
	.cfi_endproc
.LFE976:
	.size	_ZN9__gnu_cxxL25__exchange_and_add_singleEPii, .-_ZN9__gnu_cxxL25__exchange_and_add_singleEPii
	.section	.text._ZN9__gnu_cxxL27__exchange_and_add_dispatchEPii,"axG",@progbits,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv,comdat
	.align 1
	.type	_ZN9__gnu_cxxL27__exchange_and_add_dispatchEPii, @function
_ZN9__gnu_cxxL27__exchange_and_add_dispatchEPii:
.LFB978:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 17, -8
	add	#-8,r15
	.cfi_def_cfa_offset 16
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-56,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-56,r1
	mov.l	r5,@(56,r1)
	mov.l	.L23,r1
	jsr	@r1
	nop
	mov	r0,r1
	tst	r1,r1
	mov	#-1,r1
	negc	r1,r1
	extu.b	r1,r1
	tst	r1,r1
	bt	.L21
	mov	r14,r2
	add	#-56,r2
	mov	r14,r1
	add	#-56,r1
	mov.l	@(56,r2),r5
	mov.l	@(60,r1),r4
	mov.l	.L24,r1
	jsr	@r1
	nop
	mov	r0,r1
	bra	.L22
	nop
	.align 1
.L21:
	mov	r14,r2
	add	#-56,r2
	mov	r14,r1
	add	#-56,r1
	mov.l	@(56,r2),r5
	mov.l	@(60,r1),r4
	mov.l	.L25,r1
	jsr	@r1
	nop
	mov	r0,r1
	nop
.L22:
	mov	r1,r0
	add	#8,r14
	.cfi_def_cfa_offset 8
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
.L26:
	.align 2
.L23:
	.long	_ZL18__gthread_active_pv
.L24:
	.long	_ZN9__gnu_cxxL18__exchange_and_addEPVii
.L25:
	.long	_ZN9__gnu_cxxL25__exchange_and_add_singleEPii
	.cfi_endproc
.LFE978:
	.size	_ZN9__gnu_cxxL27__exchange_and_add_dispatchEPii, .-_ZN9__gnu_cxxL27__exchange_and_add_dispatchEPii
	.section	.rodata
	.align 2
	.type	_ZN9__gnu_cxxL21__default_lock_policyE, @object
	.size	_ZN9__gnu_cxxL21__default_lock_policyE, 4
_ZN9__gnu_cxxL21__default_lock_policyE:
	.long	2
	.type	_ZStL13allocator_arg, @object
	.size	_ZStL13allocator_arg, 1
_ZStL13allocator_arg:
	.zero	1
	.type	_ZStL6ignore, @object
	.size	_ZStL6ignore, 1
_ZStL6ignore:
	.zero	1
	.weak	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag
	.section	.rodata._ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag,"aG",@progbits,_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag,comdat
	.align 2
	.type	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag, @gnu_unique_object
	.size	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag, 8
_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag:
	.zero	8
	.section	.text._ZNSt19_Sp_make_shared_tag5_S_tiEv,"axG",@progbits,_ZNSt19_Sp_make_shared_tag5_S_tiEv,comdat
	.align 1
	.weak	_ZNSt19_Sp_make_shared_tag5_S_tiEv
	.type	_ZNSt19_Sp_make_shared_tag5_S_tiEv, @function
_ZNSt19_Sp_make_shared_tag5_S_tiEv:
.LFB1942:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov.l	.L29,r1
	mov	r1,r0
	mov	r14,r15
	.cfi_def_cfa_register 15
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
.L30:
	.align 2
.L29:
	.long	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag
	.cfi_endproc
.LFE1942:
	.size	_ZNSt19_Sp_make_shared_tag5_S_tiEv, .-_ZNSt19_Sp_make_shared_tag5_S_tiEv
	.section	.rodata
	.align 2
	.type	_ZL2pi, @object
	.size	_ZL2pi, 8
_ZL2pi:
	.long	1413754136
	.long	1074340347
	.weak	_ZZ13random_doublevE12distribution
	.section	.bss._ZZ13random_doublevE12distribution,"awG",@nobits,_ZZ13random_doublevE12distribution,comdat
	.align 2
	.type	_ZZ13random_doublevE12distribution, @gnu_unique_object
	.size	_ZZ13random_doublevE12distribution, 16
_ZZ13random_doublevE12distribution:
	.zero	16
	.weak	_ZGVZ13random_doublevE12distribution
	.section	.bss._ZGVZ13random_doublevE12distribution,"awG",@nobits,_ZGVZ13random_doublevE12distribution,comdat
	.align 2
	.type	_ZGVZ13random_doublevE12distribution, @gnu_unique_object
	.size	_ZGVZ13random_doublevE12distribution, 8
_ZGVZ13random_doublevE12distribution:
	.zero	8
	.weak	_ZZ13random_doublevE9generator
	.section	.bss._ZZ13random_doublevE9generator,"awG",@nobits,_ZZ13random_doublevE9generator,comdat
	.align 2
	.type	_ZZ13random_doublevE9generator, @gnu_unique_object
	.size	_ZZ13random_doublevE9generator, 2500
_ZZ13random_doublevE9generator:
	.zero	2500
	.weak	_ZGVZ13random_doublevE9generator
	.section	.bss._ZGVZ13random_doublevE9generator,"awG",@nobits,_ZGVZ13random_doublevE9generator,comdat
	.align 2
	.type	_ZGVZ13random_doublevE9generator, @gnu_unique_object
	.size	_ZGVZ13random_doublevE9generator, 8
_ZGVZ13random_doublevE9generator:
	.zero	8
	.section	.text._Z13random_doublev,"axG",@progbits,_Z13random_doublev,comdat
	.align 1
	.weak	_Z13random_doublev
	.type	_Z13random_doublev, @function
_Z13random_doublev:
.LFB3288:
	.cfi_startproc
	.cfi_personality 0,__gxx_personality_v0
	.cfi_lsda 0xb,.LLSDA3288
	mov.l	r8,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 8, -4
	mov.l	r9,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 9, -8
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 12
	.cfi_offset 14, -12
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 16
	.cfi_offset 17, -16
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov.l	.L63,r1
	mov.b	@r1,r1
	extu.b	r1,r1
	tst	r1,r1
	movt	r1
	extu.b	r1,r1
	tst	r1,r1
	bt	.L32
	mov.l	.L63,r1
	mov	r1,r4
	mov.l	.L55,r1
	jsr	@r1
	nop
	mov	r0,r1
	tst	r1,r1
	mov	#-1,r1
	negc	r1,r1
	extu.b	r1,r1
	tst	r1,r1
	bt	.L32
	mov	#0,r8
	mova	.L48,r0
	fmov.s	@r0+,fr5
	fmov.s	@r0+,fr4
	fmov.s	@r0+,fr3
	fmov.s	@r0+,fr2
	mov.l	.L61,r1
	fmov	fr4,fr6
	fmov	fr5,fr7
	fmov	fr2,fr4
	fmov	fr3,fr5
	mov	r1,r4
	mov.l	.L50,r1
.LEHB0:
	jsr	@r1
	nop
.LEHE0:
	mov.l	.L63,r1
	mov	r1,r4
	mov.l	.L59,r1
	jsr	@r1
	nop
.L32:
	mov.l	.L68,r1
	mov.b	@r1,r1
	extu.b	r1,r1
	tst	r1,r1
	movt	r1
	extu.b	r1,r1
	tst	r1,r1
	bt	.L33
	mov.l	.L68,r1
	mov	r1,r4
	mov.l	.L55,r1
	jsr	@r1
	nop
	mov	r0,r1
	tst	r1,r1
	mov	#-1,r1
	negc	r1,r1
	extu.b	r1,r1
	tst	r1,r1
	bt	.L33
	mov	#0,r8
	mov.l	.L60,r1
	mov	r1,r4
	mov.l	.L57,r1
.LEHB1:
	jsr	@r1
	nop
.LEHE1:
	mov.l	.L68,r1
	mov	r1,r4
	mov.l	.L59,r1
	jsr	@r1
	nop
.L33:
	mov.l	.L60,r2
	mov.l	.L61,r1
	mov	r2,r5
	mov	r1,r4
	mov.l	.L62,r1
.LEHB2:
	jsr	@r1
	nop
	fmov	fr0,fr2
	fmov	fr1,fr3
	bra	.L44
	nop
	.align 1
.L39:
	mov	r4,r9
	tst	r8,r8
	bf	.L42
	mov.l	.L63,r1
	mov	r1,r4
	mov.l	.L69,r2
	sts	fpscr,r1
	mov.l	.L71,r3
	or	r3,r1
	lds	r1,fpscr
	jsr	@r2
	nop
	bra	.L36
	nop
	.align 1
.L42:
	sts	fpscr,r1
	mov.l	.L71,r2
	or	r2,r1
	lds	r1,fpscr
.L36:
	mov	r9,r1
	mov	r1,r4
	mov.l	.L72,r1
	jsr	@r1
	nop
	.align 1
.L40:
	mov	r4,r9
	tst	r8,r8
	bf	.L43
	mov.l	.L68,r1
	mov	r1,r4
	mov.l	.L69,r2
	sts	fpscr,r1
	mov.l	.L71,r3
	or	r3,r1
	lds	r1,fpscr
	jsr	@r2
	nop
	bra	.L38
	nop
	.align 1
.L43:
	sts	fpscr,r1
	mov.l	.L71,r2
	or	r2,r1
	lds	r1,fpscr
.L38:
	mov	r9,r1
	mov	r1,r4
	mov.l	.L72,r1
	jsr	@r1
	nop
.LEHE2:
	.align 1
.L44:
	fmov	fr2,fr0
	fmov	fr3,fr1
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 12
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 8
	mov.l	@r15+,r9
	.cfi_restore 9
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r8
	.cfi_restore 8
	.cfi_def_cfa_offset 0
	rts	
	nop
.L73:
	.align 2
.L63:
	.long	_ZGVZ13random_doublevE12distribution
.L55:
	.long	__cxa_guard_acquire
.L48:
	.long	0
	.long	1072693248
	.long	0
	.long	0
.L61:
	.long	_ZZ13random_doublevE12distribution
.L50:
	.long	_ZNSt25uniform_real_distributionIdEC1Edd
.L59:
	.long	__cxa_guard_release
.L68:
	.long	_ZGVZ13random_doublevE9generator
.L60:
	.long	_ZZ13random_doublevE9generator
.L57:
	.long	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC1Ev
.L62:
	.long	_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEEEdRT_
.L69:
	.long	__cxa_guard_abort
.L71:
	.long	524288
.L72:
	.long	_Unwind_Resume
	.cfi_endproc
.LFE3288:
	.global	__gxx_personality_v0
	.section	.gcc_except_table._Z13random_doublev,"aG",@progbits,_Z13random_doublev,comdat
.LLSDA3288:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE3288-.LLSDACSB3288
.LLSDACSB3288:
	.uleb128 .LEHB0-.LFB3288
	.uleb128 .LEHE0-.LEHB0
	.uleb128 .L39-.LFB3288
	.uleb128 0
	.uleb128 .LEHB1-.LFB3288
	.uleb128 .LEHE1-.LEHB1
	.uleb128 .L40-.LFB3288
	.uleb128 0
	.uleb128 .LEHB2-.LFB3288
	.uleb128 .LEHE2-.LEHB2
	.uleb128 0
	.uleb128 0
.LLSDACSE3288:
	.section	.text._Z13random_doublev,"axG",@progbits,_Z13random_doublev,comdat
	.size	_Z13random_doublev, .-_Z13random_doublev
	.section	.text._ZplRK4vec3S1_,"axG",@progbits,_ZplRK4vec3S1_,comdat
	.align 1
	.weak	_ZplRK4vec3S1_
	.type	_ZplRK4vec3S1_, @function
_ZplRK4vec3S1_:
.LFB3924:
	.cfi_startproc
	mov.l	r8,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 8, -4
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 14, -8
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 12
	.cfi_offset 17, -12
	add	#-8,r15
	.cfi_def_cfa_offset 20
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r2,r8
	mov	r14,r1
	add	#-56,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-56,r1
	mov.l	r5,@(56,r1)
	mov	r14,r1
	add	#-56,r1
	mov.l	@(60,r1),r1
	fmov.s	@r1+,fr5
	fmov.s	@r1,fr4
	add	#-4,r1
	mov	r14,r1
	add	#-56,r1
	mov.l	@(56,r1),r1
	fmov.s	@r1+,fr3
	fmov.s	@r1,fr2
	add	#-4,r1
	fmov	fr4,fr0
	fmov	fr5,fr1
	fadd	dr2,dr0
	mov	r14,r1
	add	#-56,r1
	mov.l	@(60,r1),r1
	add	#8,r1
	fmov.s	@r1+,fr5
	fmov.s	@r1,fr4
	add	#-4,r1
	mov	r14,r1
	add	#-56,r1
	mov.l	@(56,r1),r1
	add	#8,r1
	fmov.s	@r1+,fr3
	fmov.s	@r1,fr2
	add	#-4,r1
	fmov	fr4,fr6
	fmov	fr5,fr7
	fadd	dr2,dr6
	mov	r14,r1
	add	#-56,r1
	mov.l	@(60,r1),r1
	add	#16,r1
	fmov.s	@r1+,fr5
	fmov.s	@r1,fr4
	add	#-4,r1
	mov	r14,r1
	add	#-56,r1
	mov.l	@(56,r1),r1
	add	#16,r1
	fmov.s	@r1+,fr3
	fmov.s	@r1,fr2
	add	#-4,r1
	fadd	dr4,dr2
	fmov	fr2,fr8
	fmov	fr3,fr9
	fmov	fr0,fr4
	fmov	fr1,fr5
	mov	r8,r4
	mov.l	.L76,r1
	jsr	@r1
	nop
	mov	r8,r0
	add	#8,r14
	.cfi_def_cfa_offset 12
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 8
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r8
	.cfi_restore 8
	.cfi_def_cfa_offset 0
	rts	
	nop
.L77:
	.align 2
.L76:
	.long	_ZN4vec3C1Eddd
	.cfi_endproc
.LFE3924:
	.size	_ZplRK4vec3S1_, .-_ZplRK4vec3S1_
	.section	.text._ZmiRK4vec3S1_,"axG",@progbits,_ZmiRK4vec3S1_,comdat
	.align 1
	.weak	_ZmiRK4vec3S1_
	.type	_ZmiRK4vec3S1_, @function
_ZmiRK4vec3S1_:
.LFB3925:
	.cfi_startproc
	mov.l	r8,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 8, -4
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 14, -8
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 12
	.cfi_offset 17, -12
	add	#-8,r15
	.cfi_def_cfa_offset 20
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r2,r8
	mov	r14,r1
	add	#-56,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-56,r1
	mov.l	r5,@(56,r1)
	mov	r14,r1
	add	#-56,r1
	mov.l	@(60,r1),r1
	fmov.s	@r1+,fr5
	fmov.s	@r1,fr4
	add	#-4,r1
	mov	r14,r1
	add	#-56,r1
	mov.l	@(56,r1),r1
	fmov.s	@r1+,fr3
	fmov.s	@r1,fr2
	add	#-4,r1
	fmov	fr4,fr0
	fmov	fr5,fr1
	fsub	dr2,dr0
	mov	r14,r1
	add	#-56,r1
	mov.l	@(60,r1),r1
	add	#8,r1
	fmov.s	@r1+,fr5
	fmov.s	@r1,fr4
	add	#-4,r1
	mov	r14,r1
	add	#-56,r1
	mov.l	@(56,r1),r1
	add	#8,r1
	fmov.s	@r1+,fr3
	fmov.s	@r1,fr2
	add	#-4,r1
	fmov	fr4,fr6
	fmov	fr5,fr7
	fsub	dr2,dr6
	mov	r14,r1
	add	#-56,r1
	mov.l	@(60,r1),r1
	add	#16,r1
	fmov.s	@r1+,fr5
	fmov.s	@r1,fr4
	add	#-4,r1
	mov	r14,r1
	add	#-56,r1
	mov.l	@(56,r1),r1
	add	#16,r1
	fmov.s	@r1+,fr3
	fmov.s	@r1,fr2
	add	#-4,r1
	fmov	fr4,fr8
	fmov	fr5,fr9
	fsub	dr2,dr8
	fmov	fr8,fr2
	fmov	fr9,fr3
	fmov	fr2,fr8
	fmov	fr3,fr9
	fmov	fr0,fr4
	fmov	fr1,fr5
	mov	r8,r4
	mov.l	.L80,r1
	jsr	@r1
	nop
	mov	r8,r0
	add	#8,r14
	.cfi_def_cfa_offset 12
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 8
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r8
	.cfi_restore 8
	.cfi_def_cfa_offset 0
	rts	
	nop
.L81:
	.align 2
.L80:
	.long	_ZN4vec3C1Eddd
	.cfi_endproc
.LFE3925:
	.size	_ZmiRK4vec3S1_, .-_ZmiRK4vec3S1_
	.section	.text._ZmldRK4vec3,"axG",@progbits,_ZmldRK4vec3,comdat
	.align 1
	.weak	_ZmldRK4vec3
	.type	_ZmldRK4vec3, @function
_ZmldRK4vec3:
.LFB3927:
	.cfi_startproc
	mov.l	r8,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 8, -4
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 14, -8
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 12
	.cfi_offset 17, -12
	add	#-12,r15
	.cfi_def_cfa_offset 24
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r2,r8
	mov	r14,r1
	add	#4,r1
	add	#4,r1
	fmov.s	fr4,@r1
	fmov.s	fr5,@-r1
	mov	r14,r1
	add	#-52,r1
	mov.l	r4,@(52,r1)
	mov	r14,r1
	add	#-52,r1
	mov.l	@(52,r1),r1
	fmov.s	@r1+,fr5
	fmov.s	@r1,fr4
	add	#-4,r1
	mov	r14,r1
	add	#4,r1
	fmov.s	@r1+,fr3
	fmov.s	@r1,fr2
	add	#-4,r1
	fmov	fr4,fr0
	fmov	fr5,fr1
	fmul	dr2,dr0
	mov	r14,r1
	add	#-52,r1
	mov.l	@(52,r1),r1
	add	#8,r1
	fmov.s	@r1+,fr5
	fmov.s	@r1,fr4
	add	#-4,r1
	mov	r14,r1
	add	#4,r1
	fmov.s	@r1+,fr3
	fmov.s	@r1,fr2
	add	#-4,r1
	fmov	fr4,fr6
	fmov	fr5,fr7
	fmul	dr2,dr6
	mov	r14,r1
	add	#-52,r1
	mov.l	@(52,r1),r1
	add	#16,r1
	fmov.s	@r1+,fr5
	fmov.s	@r1,fr4
	add	#-4,r1
	mov	r14,r1
	add	#4,r1
	fmov.s	@r1+,fr3
	fmov.s	@r1,fr2
	add	#-4,r1
	fmul	dr4,dr2
	fmov	fr2,fr8
	fmov	fr3,fr9
	fmov	fr0,fr4
	fmov	fr1,fr5
	mov	r8,r4
	mov.l	.L84,r1
	jsr	@r1
	nop
	mov	r8,r0
	add	#12,r14
	.cfi_def_cfa_offset 12
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 8
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r8
	.cfi_restore 8
	.cfi_def_cfa_offset 0
	rts	
	nop
.L85:
	.align 2
.L84:
	.long	_ZN4vec3C1Eddd
	.cfi_endproc
.LFE3927:
	.size	_ZmldRK4vec3, .-_ZmldRK4vec3
	.section	.text._Zdv4vec3d,"axG",@progbits,_Zdv4vec3d,comdat
	.align 1
	.weak	_Zdv4vec3d
	.type	_Zdv4vec3d, @function
_Zdv4vec3d:
.LFB3929:
	.cfi_startproc
	mov.l	r8,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 8, -4
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 14, -8
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 12
	.cfi_offset 17, -12
	add	#-12,r15
	.cfi_def_cfa_offset 24
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r2,r8
	mov	r14,r1
	add	#4,r1
	fmov.s	fr4,@r1
	fmov.s	fr5,@-r1
	mov	r14,r1
	add	#-52,r1
	mov.l	.L93,r2
	mov.l	@r2,r0
	mov.l	r0,@(60,r1)
	mov	#0,r0
	mov	r14,r1
	mova	.L91,r0
	fmov.s	@r0+,fr5
	fmov.s	@r0+,fr4
	fmov.s	@r1+,fr3
	fmov.s	@r1,fr2
	add	#-4,r1
	fmov	fr4,fr6
	fmov	fr5,fr7
	fdiv	dr2,dr6
	fmov	fr6,fr2
	fmov	fr7,fr3
	mov	r8,r2
	mov	r14,r4
	add	#24,r4
	fmov	fr2,fr4
	fmov	fr3,fr5
	mov.l	.L92,r1
	jsr	@r1
	nop
	mov	r14,r1
	add	#-52,r1
	mov.l	.L93,r2
	mov.l	@(60,r1),r0
	mov.l	@r2,r3
	cmp/eq	r0,r3
	mov	#0,r0
	mov	#0,r3
	bt	.L89
	mov.l	.L94,r1
	jsr	@r1
	nop
	.align 1
.L89:
	mov	r8,r0
	add	#12,r14
	.cfi_def_cfa_offset 12
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 8
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r8
	.cfi_restore 8
	.cfi_def_cfa_offset 0
	rts	
	nop
.L95:
	.align 2
.L93:
	.long	__stack_chk_guard
.L91:
	.long	0
	.long	1072693248
.L92:
	.long	_ZmldRK4vec3
.L94:
	.long	__stack_chk_fail
	.cfi_endproc
.LFE3929:
	.size	_Zdv4vec3d, .-_Zdv4vec3d
	.section	.text._Z11unit_vector4vec3,"axG",@progbits,_Z11unit_vector4vec3,comdat
	.align 1
	.weak	_Z11unit_vector4vec3
	.type	_Z11unit_vector4vec3, @function
_Z11unit_vector4vec3:
.LFB3932:
	.cfi_startproc
	mov.l	r8,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 8, -4
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 14, -8
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 12
	.cfi_offset 17, -12
	add	#-28,r15
	.cfi_def_cfa_offset 40
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r2,r8
	mov	r14,r1
	add	#-36,r1
	mov.l	.L103,r2
	mov.l	@r2,r3
	mov.l	r3,@(60,r1)
	mov	#0,r3
	mov	r14,r4
	add	#40,r4
	mov.l	.L101,r1
	jsr	@r1
	nop
	fmov	fr0,fr2
	fmov	fr1,fr3
	mov	r14,r1
	add	#40,r1
	mov.l	@r1,r2
	mov.l	@(4,r1),r3
	mov.l	r2,@r15
	mov.l	r3,@(4,r15)
	add	#8,r1
	mov.l	@r1,r2
	mov.l	@(4,r1),r3
	mov.l	r2,@(8,r15)
	mov.l	r3,@(12,r15)
	add	#8,r1
	mov.l	@r1,r2
	mov.l	@(4,r1),r3
	mov.l	r2,@(16,r15)
	mov.l	r3,@(20,r15)
	add	#8,r1
	mov	r8,r2
	fmov	fr2,fr4
	fmov	fr3,fr5
	mov.l	.L102,r1
	jsr	@r1
	nop
	mov	r14,r1
	add	#-36,r1
	mov.l	.L103,r2
	mov.l	@(60,r1),r7
	mov.l	@r2,r3
	cmp/eq	r7,r3
	mov	#0,r7
	mov	#0,r3
	bt	.L99
	mov.l	.L104,r1
	jsr	@r1
	nop
	.align 1
.L99:
	mov	r8,r0
	add	#28,r14
	.cfi_def_cfa_offset 12
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 8
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r8
	.cfi_restore 8
	.cfi_def_cfa_offset 0
	rts	
	nop
.L105:
	.align 2
.L103:
	.long	__stack_chk_guard
.L101:
	.long	_ZNK4vec36lengthEv
.L102:
	.long	_Zdv4vec3d
.L104:
	.long	__stack_chk_fail
	.cfi_endproc
.LFE3932:
	.size	_Z11unit_vector4vec3, .-_Z11unit_vector4vec3
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.section	.text._ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 1
	.weak	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED2Ev
	.type	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED2Ev:
.LFB3938:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 17, -8
	add	#-4,r15
	.cfi_def_cfa_offset 12
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-60,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-60,r1
	mov.l	@(60,r1),r1
	add	#4,r1
	mov	r1,r4
	mov.l	.L107,r1
	jsr	@r1
	nop
	nop
	add	#4,r14
	.cfi_def_cfa_offset 8
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
.L108:
	.align 2
.L107:
	.long	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED1Ev
	.cfi_endproc
.LFE3938:
	.size	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED1Ev
	.set	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED1Ev,_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt10shared_ptrI8materialED2Ev,"axG",@progbits,_ZNSt10shared_ptrI8materialED5Ev,comdat
	.align 1
	.weak	_ZNSt10shared_ptrI8materialED2Ev
	.type	_ZNSt10shared_ptrI8materialED2Ev, @function
_ZNSt10shared_ptrI8materialED2Ev:
.LFB3940:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 17, -8
	add	#-4,r15
	.cfi_def_cfa_offset 12
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-60,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-60,r1
	mov.l	@(60,r1),r1
	mov	r1,r4
	mov.l	.L110,r1
	jsr	@r1
	nop
	nop
	add	#4,r14
	.cfi_def_cfa_offset 8
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
.L111:
	.align 2
.L110:
	.long	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED2Ev
	.cfi_endproc
.LFE3940:
	.size	_ZNSt10shared_ptrI8materialED2Ev, .-_ZNSt10shared_ptrI8materialED2Ev
	.weak	_ZNSt10shared_ptrI8materialED1Ev
	.set	_ZNSt10shared_ptrI8materialED1Ev,_ZNSt10shared_ptrI8materialED2Ev
	.section	.text._ZN10hit_recordC2Ev,"axG",@progbits,_ZN10hit_recordC5Ev,comdat
	.align 1
	.weak	_ZN10hit_recordC2Ev
	.type	_ZN10hit_recordC2Ev, @function
_ZN10hit_recordC2Ev:
.LFB3942:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 17, -8
	add	#-4,r15
	.cfi_def_cfa_offset 12
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-60,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-60,r1
	mov.l	@(60,r1),r1
	mov	r1,r4
	mov.l	.L114,r1
	jsr	@r1
	nop
	mov	r14,r1
	add	#-60,r1
	mov.l	@(60,r1),r1
	add	#24,r1
	mov	r1,r4
	mov.l	.L114,r1
	jsr	@r1
	nop
	mov	r14,r1
	add	#-60,r1
	mov.l	@(60,r1),r1
	add	#48,r1
	mov	r1,r4
	mov.l	.L115,r1
	jsr	@r1
	nop
	nop
	add	#4,r14
	.cfi_def_cfa_offset 8
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
.L116:
	.align 2
.L114:
	.long	_ZN4vec3C1Ev
.L115:
	.long	_ZNSt10shared_ptrI8materialEC1Ev
	.cfi_endproc
.LFE3942:
	.size	_ZN10hit_recordC2Ev, .-_ZN10hit_recordC2Ev
	.weak	_ZN10hit_recordC1Ev
	.set	_ZN10hit_recordC1Ev,_ZN10hit_recordC2Ev
	.section	.text._ZN10hit_recordD2Ev,"axG",@progbits,_ZN10hit_recordD5Ev,comdat
	.align 1
	.weak	_ZN10hit_recordD2Ev
	.type	_ZN10hit_recordD2Ev, @function
_ZN10hit_recordD2Ev:
.LFB3945:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 17, -8
	add	#-4,r15
	.cfi_def_cfa_offset 12
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-60,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-60,r1
	mov.l	@(60,r1),r1
	add	#48,r1
	mov	r1,r4
	mov.l	.L118,r1
	jsr	@r1
	nop
	nop
	add	#4,r14
	.cfi_def_cfa_offset 8
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
.L119:
	.align 2
.L118:
	.long	_ZNSt10shared_ptrI8materialED1Ev
	.cfi_endproc
.LFE3945:
	.size	_ZN10hit_recordD2Ev, .-_ZN10hit_recordD2Ev
	.weak	_ZN10hit_recordD1Ev
	.set	_ZN10hit_recordD1Ev,_ZN10hit_recordD2Ev
	.text
	.align 1
	.type	_ZL9ray_colorRK3rayRK8hittablei, @function
_ZL9ray_colorRK3rayRK8hittablei:
.LFB3934:
	.cfi_startproc
	.cfi_personality 0,__gxx_personality_v0
	.cfi_lsda 0xb,.LLSDA3934
	mov.l	r8,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 8, -4
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 14, -8
	fmov.s	fr12,@-r15
	.cfi_def_cfa_offset 12
	.cfi_offset 37, -12
	fmov.s	fr13,@-r15
	.cfi_def_cfa_offset 16
	.cfi_offset 38, -16
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 20
	.cfi_offset 17, -20
	mov.w	.L130,r1
	sub	r1,r15
	.cfi_def_cfa_offset 312
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r2,r8
	mov	r14,r1
	add	#-28,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-28,r1
	mov.l	r5,@(56,r1)
	mov	r14,r1
	add	#-28,r1
	mov.l	r6,@(52,r1)
	mov.w	.L131,r1
	add	r14,r1
	mov.l	.L132,r2
	mov.l	@r2,r5
	mov.l	r5,@(60,r1)
	mov	#0,r5
	mov.w	.L152,r1
	add	r14,r1
	mov	r1,r4
	mov.l	.L134,r1
.LEHB3:
	jsr	@r1
	nop
.LEHE3:
	mov	r14,r1
	add	#-28,r1
	mov.l	@(52,r1),r1
	cmp/pl	r1
	bt	.L121
	mova	.L137,r0
	fmov.s	@r0+,fr7
	fmov.s	@r0+,fr6
	mova	.L137,r0
	fmov.s	@r0+,fr5
	fmov.s	@r0+,fr4
	mova	.L137,r0
	fmov.s	@r0+,fr3
	fmov.s	@r0+,fr2
	fmov	fr6,fr8
	fmov	fr7,fr9
	fmov	fr4,fr6
	fmov	fr5,fr7
	fmov	fr2,fr4
	fmov	fr3,fr5
	mov	r8,r4
	mov.l	.L138,r1
.LEHB4:
	jsr	@r1
	nop
	bra	.L122
	nop
	.align 1
.L121:
	mov	r14,r1
	add	#36,r1
	mova	.L139,r0
	fmov.s	@r0+,fr3
	fmov.s	@r0+,fr2
	add	#4,r1
	fmov.s	fr2,@r1
	fmov.s	fr3,@-r1
	mov	r14,r1
	add	#-28,r1
	mov.l	@(56,r1),r1
	mov.l	@r1,r1
	mov.l	@r1,r1
	mov.w	.L152,r6
	add	r14,r6
	mov	r14,r7
	add	#36,r7
	fmov.s	@r0+,fr3
	fmov.s	@r0+,fr2
	mov	r14,r3
	add	#-28,r3
	mov	r14,r2
	add	#-28,r2
	fmov.s	@r7+,fr7
	fmov.s	@r7,fr6
	add	#-4,r7
	fmov	fr2,fr4
	fmov	fr3,fr5
	mov.l	@(60,r3),r5
	mov.l	@(56,r2),r4
	jsr	@r1
	nop
	mov	r0,r1
	tst	r1,r1
	bf	.L194
	bra	.L123
	nop
.L194:
	mov.w	.L158,r2
	add	r14,r2
	mov.w	.L152,r1
	add	r14,r1
	mov	r1,r3
	add	#24,r3
	mov.w	.L152,r1
	add	r14,r1
	mov	r3,r5
	mov	r1,r4
	mov.l	.L149,r1
	jsr	@r1
	nop
	mov.w	.L156,r1
	add	r14,r1
	mov	r1,r2
	mov.l	.L146,r1
	jsr	@r1
	nop
	mov	r14,r2
	add	#100,r2
	mov.w	.L156,r3
	add	r14,r3
	mov.w	.L158,r1
	add	r14,r1
	mov	r3,r5
	mov	r1,r4
	mov.l	.L149,r1
	jsr	@r1
	nop
	mov	r14,r2
	add	#124,r2
	mov.w	.L152,r3
	add	r14,r3
	mov	r14,r1
	add	#100,r1
	mov	r3,r5
	mov	r1,r4
	mov.l	.L151,r1
	jsr	@r1
	nop
	mov	r14,r3
	add	#124,r3
	mov.w	.L152,r2
	add	r14,r2
	mov.w	.L156,r1
	add	r14,r1
	mov	r3,r6
	mov	r2,r5
	mov	r1,r4
	mov.l	.L154,r1
	jsr	@r1
	nop
	mov	r14,r1
	add	#-28,r1
	mov.l	@(52,r1),r1
	mov	r1,r7
	add	#-1,r7
	mov.w	.L158,r2
	add	r14,r2
	mov	r14,r1
	add	#-28,r1
	mov.w	.L156,r3
	add	r14,r3
	mov	r7,r6
	mov.l	@(56,r1),r5
	mov	r3,r4
	mov.l	.L157,r1
	jsr	@r1
	nop
	mov.w	.L158,r1
	add	r14,r1
	mova	.L159,r0
	fmov.s	@r0+,fr3
	fmov.s	@r0+,fr2
	mov	r8,r2
	mov	r1,r4
	fmov	fr2,fr4
	fmov	fr3,fr5
	mov.l	.L160,r1
	jsr	@r1
	nop
	bra	.L122
	nop
	.align 1
.L130:
	.short	292
.L131:
	.short	228
.L152:
	.short	220
.L158:
	.short	148
.L156:
	.short	172
.L161:
	.align 2
.L132:
	.long	__stack_chk_guard
.L134:
	.long	_ZN10hit_recordC1Ev
.L137:
	.long	0
	.long	0
.L138:
	.long	_ZN4vec3C1Eddd
.L139:
	.long	0
	.long	2146435072
	.long	3539053052
	.long	1062232653
.L149:
	.long	_ZplRK4vec3S1_
.L146:
	.long	_Z18random_unit_vectorv
.L151:
	.long	_ZmiRK4vec3S1_
.L154:
	.long	_ZN3rayC1ERK4vec3S2_
.L157:
	.long	_ZL9ray_colorRK3rayRK8hittablei
.L159:
	.long	0
	.long	1071644672
.L160:
	.long	_ZmldRK4vec3
	.align 1
.L123:
	mov.w	.L164,r2
	add	r14,r2
	mov	r14,r1
	add	#-28,r1
	mov.l	@(60,r1),r4
	mov.l	.L163,r1
	jsr	@r1
	nop
	mov	r14,r7
	add	#52,r7
	mov.w	.L164,r1
	add	r14,r1
	mov.l	@r1,r2
	mov.l	@(4,r1),r3
	mov.l	r2,@r15
	mov.l	r3,@(4,r15)
	add	#8,r1
	mov.l	@r1,r2
	mov.l	@(4,r1),r3
	mov.l	r2,@(8,r15)
	mov.l	r3,@(12,r15)
	add	#8,r1
	mov.l	@r1,r2
	mov.l	@(4,r1),r3
	mov.l	r2,@(16,r15)
	mov.l	r3,@(20,r15)
	add	#8,r1
	mov	r7,r2
	mov.l	.L165,r1
	jsr	@r1
	nop
	mov	r14,r1
	add	#52,r1
	mov	r1,r4
	mov.l	.L166,r1
	jsr	@r1
	nop
	fmov	fr0,fr4
	fmov	fr1,fr5
	mova	.L174,r0
	fmov.s	@r0+,fr3
	fmov.s	@r0+,fr2
	fadd	dr2,dr4
	mov	r14,r1
	add	#44,r1
	fmov.s	@r0+,fr3
	fmov.s	@r0+,fr2
	fmul	dr4,dr2
	add	#4,r1
	fmov.s	fr2,@r1
	fmov.s	fr3,@-r1
	mov	r14,r1
	add	#44,r1
	mova	.L174,r0
	fmov.s	@r0+,fr5
	fmov.s	@r0+,fr4
	fmov.s	@r1+,fr3
	fmov.s	@r1,fr2
	add	#-4,r1
	fmov	fr4,fr12
	fmov	fr5,fr13
	fsub	dr2,dr12
	mova	.L174,r0
	fmov.s	@r0+,fr7
	fmov.s	@r0+,fr6
	mova	.L174,r0
	fmov.s	@r0+,fr5
	fmov.s	@r0+,fr4
	mova	.L174,r0
	fmov.s	@r0+,fr3
	fmov.s	@r0+,fr2
	mov	r14,r1
	add	#76,r1
	fmov	fr6,fr8
	fmov	fr7,fr9
	fmov	fr4,fr6
	fmov	fr5,fr7
	fmov	fr2,fr4
	fmov	fr3,fr5
	mov	r1,r4
	mov.l	.L177,r1
	jsr	@r1
	nop
	mov	r14,r2
	add	#100,r2
	mov	r14,r1
	add	#76,r1
	mov	r1,r4
	fmov	fr12,fr4
	fmov	fr13,fr5
	mov.l	.L179,r1
	jsr	@r1
	nop
	mova	.L174,r0
	fmov.s	@r0+,fr7
	fmov.s	@r0+,fr6
	mova	.L175,r0
	fmov.s	@r0+,fr5
	fmov.s	@r0+,fr4
	mova	.L176,r0
	fmov.s	@r0+,fr3
	fmov.s	@r0+,fr2
	mov	r14,r1
	add	#124,r1
	fmov	fr6,fr8
	fmov	fr7,fr9
	fmov	fr4,fr6
	fmov	fr5,fr7
	fmov	fr2,fr4
	fmov	fr3,fr5
	mov	r1,r4
	mov.l	.L177,r1
	jsr	@r1
	nop
	mov.w	.L180,r2
	add	r14,r2
	mov	r14,r3
	add	#124,r3
	mov	r14,r1
	add	#44,r1
	mov	r3,r4
	fmov.s	@r1+,fr5
	fmov.s	@r1,fr4
	add	#-4,r1
	mov.l	.L179,r1
	jsr	@r1
	nop
	mov.w	.L180,r3
	add	r14,r3
	mov	r14,r1
	add	#100,r1
	mov	r8,r2
	mov	r3,r5
	mov	r1,r4
	mov.l	.L181,r1
	jsr	@r1
	nop
.LEHE4:
.L122:
	mov.w	.L186,r1
	add	r14,r1
	mov	r1,r4
	mov.l	.L187,r1
	jsr	@r1
	nop
	mov.w	.L184,r1
	add	r14,r1
	mov.l	.L185,r2
	mov.l	@(60,r1),r3
	mov.l	@r2,r5
	cmp/eq	r3,r5
	mov	#0,r3
	mov	#0,r5
	bt	.L129
	bra	.L128
	nop
	.align 1
.L127:
	mov	r4,r8
	mov.w	.L186,r1
	add	r14,r1
	mov	r1,r4
	mov.l	.L187,r2
	sts	fpscr,r1
	mov.l	.L188,r3
	or	r3,r1
	lds	r1,fpscr
	jsr	@r2
	nop
	mov	r8,r1
	mov	r1,r4
	mov.l	.L189,r1
.LEHB5:
	jsr	@r1
	nop
.LEHE5:
	.align 1
.L128:
	mov.l	.L190,r1
	jsr	@r1
	nop
	.align 1
.L129:
	mov	r8,r0
	mov.w	.L191,r7
	add	r7,r14
	.cfi_def_cfa_offset 20
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 16
	fmov.s	@r15+,fr13
	.cfi_restore 38
	.cfi_def_cfa_offset 12
	fmov.s	@r15+,fr12
	.cfi_restore 37
	.cfi_def_cfa_offset 8
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r8
	.cfi_restore 8
	.cfi_def_cfa_offset 0
	rts	
	nop
	.align 1
.L164:
	.short	172
.L180:
	.short	148
.L186:
	.short	220
.L184:
	.short	228
.L191:
	.short	292
.L192:
	.align 2
.L163:
	.long	_ZNK3ray9directionEv
.L165:
	.long	_Z11unit_vector4vec3
.L166:
	.long	_ZNK4vec31yEv
.L174:
	.long	0
	.long	1072693248
.L176:
	.long	0
	.long	1071644672
.L177:
	.long	_ZN4vec3C1Eddd
.L179:
	.long	_ZmldRK4vec3
.L175:
	.long	1717986918
	.long	1072064102
.L181:
	.long	_ZplRK4vec3S1_
.L187:
	.long	_ZN10hit_recordD1Ev
.L185:
	.long	__stack_chk_guard
.L188:
	.long	524288
.L189:
	.long	_Unwind_Resume
.L190:
	.long	__stack_chk_fail
	.cfi_endproc
.LFE3934:
	.section	.gcc_except_table,"a",@progbits
.LLSDA3934:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE3934-.LLSDACSB3934
.LLSDACSB3934:
	.uleb128 .LEHB3-.LFB3934
	.uleb128 .LEHE3-.LEHB3
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB4-.LFB3934
	.uleb128 .LEHE4-.LEHB4
	.uleb128 .L127-.LFB3934
	.uleb128 0
	.uleb128 .LEHB5-.LFB3934
	.uleb128 .LEHE5-.LEHB5
	.uleb128 0
	.uleb128 0
.LLSDACSE3934:
	.text
	.size	_ZL9ray_colorRK3rayRK8hittablei, .-_ZL9ray_colorRK3rayRK8hittablei
	.section	.text._ZN13hittable_listD2Ev,"axG",@progbits,_ZN13hittable_listD5Ev,comdat
	.align 1
	.weak	_ZN13hittable_listD2Ev
	.type	_ZN13hittable_listD2Ev, @function
_ZN13hittable_listD2Ev:
.LFB3949:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 17, -8
	add	#-4,r15
	.cfi_def_cfa_offset 12
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-60,r1
	mov.l	r4,@(60,r1)
	mov.l	.L196,r2
	mov	r14,r1
	add	#-60,r1
	mov.l	@(60,r1),r1
	mov.l	r2,@r1
	mov	r14,r1
	add	#-60,r1
	mov.l	@(60,r1),r1
	add	#4,r1
	mov	r1,r4
	mov.l	.L197,r1
	jsr	@r1
	nop
	nop
	add	#4,r14
	.cfi_def_cfa_offset 8
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
.L198:
	.align 2
.L196:
	.long	_ZTV13hittable_list+8
.L197:
	.long	_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED1Ev
	.cfi_endproc
.LFE3949:
	.size	_ZN13hittable_listD2Ev, .-_ZN13hittable_listD2Ev
	.weak	_ZN13hittable_listD1Ev
	.set	_ZN13hittable_listD1Ev,_ZN13hittable_listD2Ev
	.section	.text._ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 1
	.weak	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED2Ev
	.type	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED2Ev:
.LFB3953:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 17, -8
	add	#-4,r15
	.cfi_def_cfa_offset 12
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-60,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-60,r1
	mov.l	@(60,r1),r1
	add	#4,r1
	mov	r1,r4
	mov.l	.L200,r1
	jsr	@r1
	nop
	nop
	add	#4,r14
	.cfi_def_cfa_offset 8
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
.L201:
	.align 2
.L200:
	.long	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED1Ev
	.cfi_endproc
.LFE3953:
	.size	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED1Ev
	.set	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED1Ev,_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt10shared_ptrI6sphereED2Ev,"axG",@progbits,_ZNSt10shared_ptrI6sphereED5Ev,comdat
	.align 1
	.weak	_ZNSt10shared_ptrI6sphereED2Ev
	.type	_ZNSt10shared_ptrI6sphereED2Ev, @function
_ZNSt10shared_ptrI6sphereED2Ev:
.LFB3955:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 17, -8
	add	#-4,r15
	.cfi_def_cfa_offset 12
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-60,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-60,r1
	mov.l	@(60,r1),r1
	mov	r1,r4
	mov.l	.L203,r1
	jsr	@r1
	nop
	nop
	add	#4,r14
	.cfi_def_cfa_offset 8
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
.L204:
	.align 2
.L203:
	.long	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED2Ev
	.cfi_endproc
.LFE3955:
	.size	_ZNSt10shared_ptrI6sphereED2Ev, .-_ZNSt10shared_ptrI6sphereED2Ev
	.weak	_ZNSt10shared_ptrI6sphereED1Ev
	.set	_ZNSt10shared_ptrI6sphereED1Ev,_ZNSt10shared_ptrI6sphereED2Ev
	.section	.text._ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 1
	.weak	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED2Ev
	.type	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED2Ev:
.LFB3959:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 17, -8
	add	#-4,r15
	.cfi_def_cfa_offset 12
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-60,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-60,r1
	mov.l	@(60,r1),r1
	add	#4,r1
	mov	r1,r4
	mov.l	.L206,r1
	jsr	@r1
	nop
	nop
	add	#4,r14
	.cfi_def_cfa_offset 8
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
.L207:
	.align 2
.L206:
	.long	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED1Ev
	.cfi_endproc
.LFE3959:
	.size	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED1Ev
	.set	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED1Ev,_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt10shared_ptrI8hittableED2Ev,"axG",@progbits,_ZNSt10shared_ptrI8hittableED5Ev,comdat
	.align 1
	.weak	_ZNSt10shared_ptrI8hittableED2Ev
	.type	_ZNSt10shared_ptrI8hittableED2Ev, @function
_ZNSt10shared_ptrI8hittableED2Ev:
.LFB3961:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 17, -8
	add	#-4,r15
	.cfi_def_cfa_offset 12
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-60,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-60,r1
	mov.l	@(60,r1),r1
	mov	r1,r4
	mov.l	.L209,r1
	jsr	@r1
	nop
	nop
	add	#4,r14
	.cfi_def_cfa_offset 8
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
.L210:
	.align 2
.L209:
	.long	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED2Ev
	.cfi_endproc
.LFE3961:
	.size	_ZNSt10shared_ptrI8hittableED2Ev, .-_ZNSt10shared_ptrI8hittableED2Ev
	.weak	_ZNSt10shared_ptrI8hittableED1Ev
	.set	_ZNSt10shared_ptrI8hittableED1Ev,_ZNSt10shared_ptrI8hittableED2Ev
	.section	.rodata
	.align 2
.LC0:
	.string	"chapter8.ppm"
	.align 2
.LC1:
	.string	"P3\n"
	.align 2
.LC2:
	.string	"\n255\n"
	.align 2
.LC3:
	.string	"\rScanlines remaining: "
	.align 2
.LC4:
	.string	"\nDone.\n"
	.text
	.align 1
	.global	main
	.type	main, @function
main:
.LFB3947:
	.cfi_startproc
	.cfi_personality 0,__gxx_personality_v0
	.cfi_lsda 0xb,.LLSDA3947
	mov.l	r8,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 8, -4
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 14, -8
	fmov.s	fr12,@-r15
	.cfi_def_cfa_offset 12
	.cfi_offset 37, -12
	fmov.s	fr13,@-r15
	.cfi_def_cfa_offset 16
	.cfi_offset 38, -16
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 20
	.cfi_offset 17, -20
	mov.w	.L235,r1
	sub	r1,r15
	.cfi_def_cfa_offset 688
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov.w	.L236,r1
	add	r14,r1
	mov.l	.L237,r2
	mov.l	@r2,r0
	mov.l	r0,@(60,r1)
	mov	#0,r0
	mov	r14,r1
	add	#76,r1
	mova	.L238,r0
	fmov.s	@r0+,fr3
	fmov.s	@r0+,fr2
	add	#4,r1
	fmov.s	fr2,@r1
	fmov.s	fr3,@-r1
	mov	r14,r1
	add	#28,r1
	mov.w	.L274,r2
	mov.l	r2,@(8,r1)
	mov	r14,r1
	add	#28,r1
	mov.w	.L277,r2
	mov.l	r2,@(12,r1)
	mov	r14,r1
	add	#28,r1
	mov	#100,r2
	mov.l	r2,@(16,r1)
	mov	r14,r1
	add	#28,r1
	mov	#50,r2
	mov.l	r2,@(20,r1)
	mov	r14,r1
	add	#100,r1
	mov	r1,r4
	mov.l	.L241,r1
.LEHB6:
	jsr	@r1
	nop
.LEHE6:
	mova	.L253,r0
	fmov.s	@r0+,fr7
	fmov.s	@r0+,fr6
	fmov.s	@r0+,fr5
	fmov.s	@r0+,fr4
	mova	.L255,r0
	fmov.s	@r0+,fr3
	fmov.s	@r0+,fr2
	mov.w	.L272,r1
	add	r14,r1
	fmov	fr6,fr8
	fmov	fr7,fr9
	fmov	fr4,fr6
	fmov	fr5,fr7
	fmov	fr2,fr4
	fmov	fr3,fr5
	mov	r1,r4
	mov.l	.L257,r1
.LEHB7:
	jsr	@r1
	nop
.LEHE7:
	mov	r14,r1
	add	#52,r1
	mova	.L246,r0
	fmov.s	@r0+,fr3
	fmov.s	@r0+,fr2
	add	#4,r1
	fmov.s	fr2,@r1
	fmov.s	fr3,@-r1
	mov	r14,r2
	add	#60,r2
	mov	r14,r3
	add	#52,r3
	mov.w	.L272,r1
	add	r14,r1
	mov	r3,r5
	mov	r1,r4
	mov.l	.L248,r1
.LEHB8:
	jsr	@r1
	nop
.LEHE8:
	mov	r14,r2
	add	#60,r2
	mov	r14,r1
	add	#68,r1
	mov	r2,r5
	mov	r1,r4
	mov.l	.L260,r1
	jsr	@r1
	nop
	mov	r14,r2
	add	#68,r2
	mov	r14,r1
	add	#100,r1
	mov	r2,r5
	mov	r1,r4
	mov.l	.L261,r1
.LEHB9:
	jsr	@r1
	nop
.LEHE9:
	mov	r14,r1
	add	#68,r1
	mov	r1,r4
	mov.l	.L262,r1
	jsr	@r1
	nop
	mov	r14,r1
	add	#60,r1
	mov	r1,r4
	mov.l	.L263,r1
	jsr	@r1
	nop
	mova	.L253,r0
	fmov.s	@r0+,fr7
	fmov.s	@r0+,fr6
	mova	.L254,r0
	fmov.s	@r0+,fr5
	fmov.s	@r0+,fr4
	mova	.L255,r0
	fmov.s	@r0+,fr3
	fmov.s	@r0+,fr2
	mov.w	.L272,r1
	add	r14,r1
	fmov	fr6,fr8
	fmov	fr7,fr9
	fmov	fr4,fr6
	fmov	fr5,fr7
	fmov	fr2,fr4
	fmov	fr3,fr5
	mov	r1,r4
	mov.l	.L257,r1
.LEHB10:
	jsr	@r1
	nop
.LEHE10:
	mov	r14,r1
	add	#28,r1
	mov	#100,r2
	mov.l	r2,@(24,r1)
	mov	r14,r2
	add	#60,r2
	mov	r14,r3
	add	#52,r3
	mov.w	.L272,r1
	add	r14,r1
	mov	r3,r5
	mov	r1,r4
	mov.l	.L259,r1
.LEHB11:
	jsr	@r1
	nop
.LEHE11:
	mov	r14,r2
	add	#60,r2
	mov	r14,r1
	add	#68,r1
	mov	r2,r5
	mov	r1,r4
	mov.l	.L260,r1
	jsr	@r1
	nop
	mov	r14,r2
	add	#68,r2
	mov	r14,r1
	add	#100,r1
	mov	r2,r5
	mov	r1,r4
	mov.l	.L261,r1
.LEHB12:
	jsr	@r1
	nop
.LEHE12:
	mov	r14,r1
	add	#68,r1
	mov	r1,r4
	mov.l	.L262,r1
	jsr	@r1
	nop
	mov	r14,r1
	add	#60,r1
	mov	r1,r4
	mov.l	.L263,r1
	jsr	@r1
	nop
	mov.w	.L264,r1
	add	r14,r1
	mov	r1,r4
	mov.l	.L265,r1
.LEHB13:
	jsr	@r1
	nop
	mov.w	.L272,r1
	add	r14,r1
	mov	r1,r4
	mov.l	.L267,r1
	jsr	@r1
	nop
.LEHE13:
	mov.l	.L268,r2
	mov.w	.L272,r1
	add	r14,r1
	mov	#16,r6
	mov	r2,r5
	mov	r1,r4
	mov.l	.L270,r1
.LEHB14:
	jsr	@r1
	nop
	mov.l	.L271,r2
	mov.w	.L272,r1
	add	r14,r1
	mov	r2,r5
	mov	r1,r4
	mov.l	.L280,r1
	jsr	@r1
	nop
	mov	r0,r1
	mov.w	.L274,r2
	mov	r2,r5
	mov	r1,r4
	mov.l	.L278,r1
	jsr	@r1
	nop
	mov	r0,r1
	mov	#32,r5
	mov	r1,r4
	mov.l	.L276,r1
	jsr	@r1
	nop
	mov	r0,r1
	mov.w	.L277,r2
	mov	r2,r5
	mov	r1,r4
	mov.l	.L278,r1
	jsr	@r1
	nop
	mov	r0,r1
	mov.l	.L279,r2
	mov	r2,r5
	mov	r1,r4
	mov.l	.L280,r1
	jsr	@r1
	nop
	mov	r14,r1
	add	#-36,r1
	mov.w	.L281,r2
	mov.l	r2,@(60,r1)
.L217:
	mov	r14,r1
	add	#-36,r1
	mov.l	@(60,r1),r1
	cmp/pz	r1
	bra	.L234
	nop
	.align 1
.L235:
	.short	668
.L236:
	.short	604
.L274:
	.short	400
.L277:
	.short	225
.L272:
	.short	388
.L264:
	.short	212
.L281:
	.short	224
.L282:
	.align 2
.L237:
	.long	__stack_chk_guard
.L238:
	.long	477218588
	.long	1073508807
.L241:
	.long	_ZN13hittable_listC1Ev
.L253:
	.long	0
	.long	-1074790400
.L255:
	.long	0
	.long	0
.L257:
	.long	_ZN4vec3C1Eddd
.L246:
	.long	0
	.long	1071644672
.L248:
	.long	_ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_
.L260:
	.long	_ZNSt10shared_ptrI8hittableEC1I6spherevEEOS_IT_E
.L261:
	.long	_ZN13hittable_list3addESt10shared_ptrI8hittableE
.L262:
	.long	_ZNSt10shared_ptrI8hittableED1Ev
.L263:
	.long	_ZNSt10shared_ptrI6sphereED1Ev
.L254:
	.long	0
	.long	-1067900928
.L259:
	.long	_ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_
.L265:
	.long	_ZN6cameraC1Ev
.L267:
	.long	_ZNSt14basic_ofstreamIcSt11char_traitsIcEEC1Ev
.L268:
	.long	.LC0
.L270:
	.long	_ZNSt14basic_ofstreamIcSt11char_traitsIcEE4openEPKcSt13_Ios_Openmode
.L271:
	.long	.LC1
.L280:
	.long	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
.L278:
	.long	_ZNSolsEi
.L276:
	.long	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_c
.L279:
	.long	.LC2
	.align 1
.L234:
	bt	.L340
	bra	.L212
	nop
.L340:
	mov.l	.L283,r2
	mov.l	.L284,r1
	mov	r2,r5
	mov	r1,r4
	mov.l	.L285,r1
	jsr	@r1
	nop
	mov	r0,r2
	mov	r14,r1
	add	#-36,r1
	mov.l	@(60,r1),r5
	mov	r2,r4
	mov.l	.L286,r1
	jsr	@r1
	nop
	mov	r0,r1
	mov	#32,r5
	mov	r1,r4
	mov.l	.L287,r1
	jsr	@r1
	nop
	mov	r0,r1
	mov.l	.L288,r2
	mov	r2,r5
	mov	r1,r4
	mov.l	.L289,r1
	jsr	@r1
	nop
	mov	r14,r1
	add	#28,r1
	mov	#0,r2
	mov.l	r2,@(0,r1)
.L216:
	mov	r14,r1
	add	#28,r1
	mov.l	@(0,r1),r2
	mov.w	.L290,r1
	cmp/gt	r1,r2
	bf	.L339
	bra	.L213
	nop
.L339:
	mova	.L293,r0
	fmov.s	@r0+,fr7
	fmov.s	@r0+,fr6
	mova	.L293,r0
	fmov.s	@r0+,fr5
	fmov.s	@r0+,fr4
	mova	.L293,r0
	fmov.s	@r0+,fr3
	fmov.s	@r0+,fr2
	mov	r14,r1
	add	#116,r1
	fmov	fr6,fr8
	fmov	fr7,fr9
	fmov	fr4,fr6
	fmov	fr5,fr7
	fmov	fr2,fr4
	fmov	fr3,fr5
	mov	r1,r4
	mov.l	.L294,r1
	jsr	@r1
	nop
	mov	r14,r1
	add	#28,r1
	mov	#0,r2
	mov.l	r2,@(4,r1)
.L215:
	mov	r14,r1
	add	#28,r1
	mov.l	@(4,r1),r2
	mov	#99,r1
	cmp/gt	r1,r2
	bt	.L214
	mov	r14,r1
	add	#28,r1
	mov.l	@(0,r1),r0
	lds	r0,fpul
	fsts	fpul,fr1
	flds	fr1,fpul
	float	fpul,dr12
	mov.l	.L297,r1
	jsr	@r1
	nop
	fmov	fr0,fr2
	fmov	fr1,fr3
	fmov	fr12,fr4
	fmov	fr13,fr5
	fadd	dr2,dr4
	mov	r14,r1
	add	#84,r1
	mova	.L296,r0
	fmov.s	@r0+,fr3
	fmov.s	@r0+,fr2
	fmov	fr4,fr6
	fmov	fr5,fr7
	fdiv	dr2,dr6
	fmov	fr6,fr2
	fmov	fr7,fr3
	add	#4,r1
	fmov.s	fr2,@r1
	fmov.s	fr3,@-r1
	mov	r14,r1
	add	#-36,r1
	mov.l	@(60,r1),r0
	lds	r0,fpul
	fsts	fpul,fr1
	flds	fr1,fpul
	float	fpul,dr12
	mov.l	.L297,r1
	jsr	@r1
	nop
	fmov	fr0,fr2
	fmov	fr1,fr3
	fmov	fr12,fr4
	fmov	fr13,fr5
	fadd	dr2,dr4
	mov	r14,r1
	add	#92,r1
	mova	.L298,r0
	fmov.s	@r0+,fr3
	fmov.s	@r0+,fr2
	fmov	fr4,fr6
	fmov	fr5,fr7
	fdiv	dr2,dr6
	fmov	fr6,fr2
	fmov	fr7,fr3
	add	#4,r1
	fmov.s	fr2,@r1
	fmov.s	fr3,@-r1
	mov.w	.L303,r2
	add	r14,r2
	mov	r14,r3
	add	#92,r3
	mov	r14,r1
	add	#84,r1
	mov.w	.L300,r7
	add	r14,r7
	fmov.s	@r3+,fr7
	fmov.s	@r3,fr6
	add	#-4,r3
	fmov.s	@r1+,fr5
	fmov.s	@r1,fr4
	add	#-4,r1
	mov	r7,r4
	mov.l	.L301,r1
	jsr	@r1
	nop
	mov.w	.L305,r2
	add	r14,r2
	mov	r14,r3
	add	#100,r3
	mov.w	.L303,r1
	add	r14,r1
	mov	#50,r6
	mov	r3,r5
	mov	r1,r4
	mov.l	.L304,r1
	jsr	@r1
	nop
	mov.w	.L305,r2
	add	r14,r2
	mov	r14,r1
	add	#116,r1
	mov	r2,r5
	mov	r1,r4
	mov.l	.L306,r1
	jsr	@r1
	nop
	mov	r14,r1
	add	#28,r1
	mov	r14,r2
	add	#28,r2
	mov.l	@(4,r2),r2
	add	#1,r2
	mov.l	r2,@(4,r1)
	bra	.L215
	nop
	.align 1
.L214:
	mov.w	.L307,r7
	add	r14,r7
	mov	r14,r1
	add	#116,r1
	mov.l	@r1,r2
	mov.l	@(4,r1),r3
	mov.l	r2,@r15
	mov.l	r3,@(4,r15)
	add	#8,r1
	mov.l	@r1,r2
	mov.l	@(4,r1),r3
	mov.l	r2,@(8,r15)
	mov.l	r3,@(12,r15)
	add	#8,r1
	mov.l	@r1,r2
	mov.l	@(4,r1),r3
	mov.l	r2,@(16,r15)
	mov.l	r3,@(20,r15)
	add	#8,r1
	mov	#100,r5
	mov	r7,r4
	mov.l	.L308,r1
	jsr	@r1
	nop
	mov	r14,r1
	add	#28,r1
	mov	r14,r2
	add	#28,r2
	mov.l	@(0,r2),r2
	add	#1,r2
	mov.l	r2,@(0,r1)
	bra	.L216
	nop
	.align 1
.L213:
	mov	r14,r1
	add	#-36,r1
	mov	r14,r2
	add	#-36,r2
	mov.l	@(60,r2),r2
	add	#-1,r2
	mov.l	r2,@(60,r1)
	bra	.L217
	nop
	.align 1
.L290:
	.short	399
.L303:
	.short	164
.L300:
	.short	212
.L305:
	.short	140
.L307:
	.short	388
.L309:
	.align 2
.L283:
	.long	.LC3
.L284:
	.long	_ZSt4cerr
.L285:
	.long	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
.L286:
	.long	_ZNSolsEi
.L287:
	.long	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_c
.L288:
	.long	_ZSt5flushIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_
.L289:
	.long	_ZNSolsEPFRSoS_E
.L293:
	.long	0
	.long	0
.L294:
	.long	_ZN4vec3C1Eddd
.L297:
	.long	_Z13random_doublev
.L296:
	.long	0
	.long	1081667584
.L298:
	.long	0
	.long	1080819712
.L301:
	.long	_ZNK6camera7get_rayEdd
.L304:
	.long	_ZL9ray_colorRK3rayRK8hittablei
.L306:
	.long	_ZN4vec3pLERKS_
.L308:
	.long	_Z11write_colorRSt14basic_ofstreamIcSt11char_traitsIcEE4vec3i
	.align 1
.L212:
	mov.l	.L310,r2
	mov.l	.L311,r1
	mov	r2,r5
	mov	r1,r4
	mov.l	.L312,r1
	jsr	@r1
	nop
.LEHE14:
	mov	#0,r8
	mov.w	.L326,r1
	add	r14,r1
	mov	r1,r4
	mov.l	.L327,r1
	jsr	@r1
	nop
	mov	r14,r1
	add	#100,r1
	mov	r1,r4
	mov.l	.L330,r1
	jsr	@r1
	nop
	mov	r8,r1
	mov.w	.L316,r2
	add	r14,r2
	mov.l	.L317,r3
	mov.l	@(60,r2),r6
	mov.l	@r3,r7
	cmp/eq	r6,r7
	mov	#0,r6
	mov	#0,r7
	bf	.L338
	bra	.L233
	nop
.L338:
	bra	.L232
	nop
	.align 1
.L228:
	mov	r4,r8
	mov	r14,r1
	add	#68,r1
	mov	r1,r4
	mov.l	.L322,r2
	sts	fpscr,r1
	mov.l	.L329,r3
	or	r3,r1
	lds	r1,fpscr
	jsr	@r2
	nop
	mov	r14,r1
	add	#60,r1
	mov	r1,r4
	mov.l	.L324,r1
	jsr	@r1
	nop
	mov	r8,r2
	bra	.L220
	nop
	.align 1
.L227:
	mov	r4,r2
	sts	fpscr,r1
	mov.l	.L329,r3
	or	r3,r1
	lds	r1,fpscr
.L220:
	mov	r2,r8
	bra	.L221
	nop
	.align 1
.L230:
	mov	r4,r8
	mov	r14,r1
	add	#68,r1
	mov	r1,r4
	mov.l	.L322,r2
	sts	fpscr,r1
	mov.l	.L329,r3
	or	r3,r1
	lds	r1,fpscr
	jsr	@r2
	nop
	mov	r14,r1
	add	#60,r1
	mov	r1,r4
	mov.l	.L324,r1
	jsr	@r1
	nop
	mov	r8,r2
	bra	.L223
	nop
	.align 1
.L229:
	mov	r4,r2
	sts	fpscr,r1
	mov.l	.L329,r3
	or	r3,r1
	lds	r1,fpscr
.L223:
	mov	r2,r8
	bra	.L221
	nop
	.align 1
.L231:
	mov	r4,r8
	mov.w	.L326,r1
	add	r14,r1
	mov	r1,r4
	mov.l	.L327,r2
	sts	fpscr,r1
	mov.l	.L329,r3
	or	r3,r1
	lds	r1,fpscr
	jsr	@r2
	nop
	bra	.L221
	nop
	.align 1
.L226:
	mov	r4,r8
	sts	fpscr,r1
	mov.l	.L329,r2
	or	r2,r1
	lds	r1,fpscr
.L221:
	mov	r14,r1
	add	#100,r1
	mov	r1,r4
	mov.l	.L330,r1
	jsr	@r1
	nop
	mov	r8,r1
	mov	r1,r4
	mov.l	.L331,r1
.LEHB15:
	jsr	@r1
	nop
.LEHE15:
	.align 1
.L232:
	mov.l	.L332,r1
	jsr	@r1
	nop
	.align 1
.L233:
	mov	r1,r0
	mov.w	.L333,r7
	add	r7,r14
	.cfi_def_cfa_offset 20
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 16
	fmov.s	@r15+,fr13
	.cfi_restore 38
	.cfi_def_cfa_offset 12
	fmov.s	@r15+,fr12
	.cfi_restore 37
	.cfi_def_cfa_offset 8
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r8
	.cfi_restore 8
	.cfi_def_cfa_offset 0
	rts	
	nop
	.align 1
.L326:
	.short	388
.L316:
	.short	604
.L333:
	.short	668
.L334:
	.align 2
.L310:
	.long	.LC4
.L311:
	.long	_ZSt4cerr
.L312:
	.long	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
.L327:
	.long	_ZNSt14basic_ofstreamIcSt11char_traitsIcEED1Ev
.L330:
	.long	_ZN13hittable_listD1Ev
.L317:
	.long	__stack_chk_guard
.L322:
	.long	_ZNSt10shared_ptrI8hittableED1Ev
.L329:
	.long	524288
.L324:
	.long	_ZNSt10shared_ptrI6sphereED1Ev
.L331:
	.long	_Unwind_Resume
.L332:
	.long	__stack_chk_fail
	.cfi_endproc
.LFE3947:
	.section	.gcc_except_table
.LLSDA3947:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE3947-.LLSDACSB3947
.LLSDACSB3947:
	.uleb128 .LEHB6-.LFB3947
	.uleb128 .LEHE6-.LEHB6
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB7-.LFB3947
	.uleb128 .LEHE7-.LEHB7
	.uleb128 .L226-.LFB3947
	.uleb128 0
	.uleb128 .LEHB8-.LFB3947
	.uleb128 .LEHE8-.LEHB8
	.uleb128 .L227-.LFB3947
	.uleb128 0
	.uleb128 .LEHB9-.LFB3947
	.uleb128 .LEHE9-.LEHB9
	.uleb128 .L228-.LFB3947
	.uleb128 0
	.uleb128 .LEHB10-.LFB3947
	.uleb128 .LEHE10-.LEHB10
	.uleb128 .L226-.LFB3947
	.uleb128 0
	.uleb128 .LEHB11-.LFB3947
	.uleb128 .LEHE11-.LEHB11
	.uleb128 .L229-.LFB3947
	.uleb128 0
	.uleb128 .LEHB12-.LFB3947
	.uleb128 .LEHE12-.LEHB12
	.uleb128 .L230-.LFB3947
	.uleb128 0
	.uleb128 .LEHB13-.LFB3947
	.uleb128 .LEHE13-.LEHB13
	.uleb128 .L226-.LFB3947
	.uleb128 0
	.uleb128 .LEHB14-.LFB3947
	.uleb128 .LEHE14-.LEHB14
	.uleb128 .L231-.LFB3947
	.uleb128 0
	.uleb128 .LEHB15-.LFB3947
	.uleb128 .LEHE15-.LEHB15
	.uleb128 0
	.uleb128 0
.LLSDACSE3947:
	.text
	.size	main, .-main
	.section	.text._ZNSt25uniform_real_distributionIdEC2Edd,"axG",@progbits,_ZNSt25uniform_real_distributionIdEC5Edd,comdat
	.align 1
	.weak	_ZNSt25uniform_real_distributionIdEC2Edd
	.type	_ZNSt25uniform_real_distributionIdEC2Edd, @function
_ZNSt25uniform_real_distributionIdEC2Edd:
.LFB4212:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 17, -8
	add	#-20,r15
	.cfi_def_cfa_offset 28
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-44,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#8,r1
	add	#4,r1
	fmov.s	fr4,@r1
	fmov.s	fr5,@-r1
	mov	r14,r1
	add	#4,r1
	fmov.s	fr6,@r1
	fmov.s	fr7,@-r1
	mov	r14,r1
	add	#-44,r1
	mov.l	@(60,r1),r3
	mov	r14,r2
	mov	r14,r1
	add	#8,r1
	fmov.s	@r2+,fr7
	fmov.s	@r2,fr6
	add	#-4,r2
	fmov.s	@r1+,fr5
	fmov.s	@r1,fr4
	add	#-4,r1
	mov	r3,r4
	mov.l	.L342,r1
	jsr	@r1
	nop
	nop
	add	#20,r14
	.cfi_def_cfa_offset 8
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
.L343:
	.align 2
.L342:
	.long	_ZNSt25uniform_real_distributionIdE10param_typeC1Edd
	.cfi_endproc
.LFE4212:
	.size	_ZNSt25uniform_real_distributionIdEC2Edd, .-_ZNSt25uniform_real_distributionIdEC2Edd
	.weak	_ZNSt25uniform_real_distributionIdEC1Edd
	.set	_ZNSt25uniform_real_distributionIdEC1Edd,_ZNSt25uniform_real_distributionIdEC2Edd
	.section	.text._ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC2Ev,"axG",@progbits,_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC5Ev,comdat
	.align 1
	.weak	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC2Ev
	.type	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC2Ev, @function
_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC2Ev:
.LFB4215:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 17, -8
	add	#-4,r15
	.cfi_def_cfa_offset 12
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-60,r1
	mov.l	r4,@(60,r1)
	mov.w	.L345,r2
	mov	r14,r1
	add	#-60,r1
	mov	r2,r5
	mov.l	@(60,r1),r4
	mov.l	.L346,r1
	jsr	@r1
	nop
	nop
	add	#4,r14
	.cfi_def_cfa_offset 8
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
	.align 1
.L345:
	.short	5489
.L347:
	.align 2
.L346:
	.long	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC1Ej
	.cfi_endproc
.LFE4215:
	.size	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC2Ev, .-_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC2Ev
	.weak	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC1Ev
	.set	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC1Ev,_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC2Ev
	.section	.text._ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEEEdRT_,"axG",@progbits,_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEEEdRT_,comdat
	.align 1
	.weak	_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEEEdRT_
	.type	_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEEEdRT_, @function
_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEEEdRT_:
.LFB4217:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 17, -8
	add	#-8,r15
	.cfi_def_cfa_offset 16
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-56,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-56,r1
	mov.l	r5,@(56,r1)
	mov	r14,r1
	add	#-56,r1
	mov.l	@(60,r1),r3
	mov	r14,r2
	add	#-56,r2
	mov	r14,r1
	add	#-56,r1
	mov	r3,r6
	mov.l	@(56,r2),r5
	mov.l	@(60,r1),r4
	mov.l	.L350,r1
	jsr	@r1
	nop
	fmov	fr0,fr2
	fmov	fr1,fr3
	fmov	fr2,fr0
	fmov	fr3,fr1
	add	#8,r14
	.cfi_def_cfa_offset 8
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
.L351:
	.align 2
.L350:
	.long	_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEEEdRT_RKNS0_10param_typeE
	.cfi_endproc
.LFE4217:
	.size	_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEEEdRT_, .-_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEEEdRT_
	.section	.text._ZNSt10shared_ptrI8materialEC2Ev,"axG",@progbits,_ZNSt10shared_ptrI8materialEC5Ev,comdat
	.align 1
	.weak	_ZNSt10shared_ptrI8materialEC2Ev
	.type	_ZNSt10shared_ptrI8materialEC2Ev, @function
_ZNSt10shared_ptrI8materialEC2Ev:
.LFB4245:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 17, -8
	add	#-4,r15
	.cfi_def_cfa_offset 12
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-60,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-60,r1
	mov.l	@(60,r1),r1
	mov	r1,r4
	mov.l	.L353,r1
	jsr	@r1
	nop
	nop
	add	#4,r14
	.cfi_def_cfa_offset 8
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
.L354:
	.align 2
.L353:
	.long	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC2Ev
	.cfi_endproc
.LFE4245:
	.size	_ZNSt10shared_ptrI8materialEC2Ev, .-_ZNSt10shared_ptrI8materialEC2Ev
	.weak	_ZNSt10shared_ptrI8materialEC1Ev
	.set	_ZNSt10shared_ptrI8materialEC1Ev,_ZNSt10shared_ptrI8materialEC2Ev
	.section	.text._ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 1
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED2Ev
	.type	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED2Ev:
.LFB4248:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 17, -8
	add	#-4,r15
	.cfi_def_cfa_offset 12
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-60,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-60,r1
	mov.l	@(60,r1),r1
	mov.l	@r1,r1
	tst	r1,r1
	bt	.L357
	mov	r14,r1
	add	#-60,r1
	mov.l	@(60,r1),r1
	mov.l	@r1,r1
	mov	r1,r4
	mov.l	.L358,r1
	jsr	@r1
	nop
.L357:
	nop
	add	#4,r14
	.cfi_def_cfa_offset 8
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
.L359:
	.align 2
.L358:
	.long	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv
	.cfi_endproc
.LFE4248:
	.size	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED1Ev
	.set	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED1Ev,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED2Ev,"axG",@progbits,_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED5Ev,comdat
	.align 1
	.weak	_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED2Ev
	.type	_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED2Ev, @function
_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED2Ev:
.LFB4251:
	.cfi_startproc
	.cfi_personality 0,__gxx_personality_v0
	.cfi_lsda 0xb,.LLSDA4251
	mov.l	r8,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 8, -4
	mov.l	r9,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 9, -8
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 12
	.cfi_offset 14, -12
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 16
	.cfi_offset 17, -16
	add	#-4,r15
	.cfi_def_cfa_offset 20
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-60,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-60,r1
	mov.l	@(60,r1),r1
	mov.l	@r1,r8
	mov	r14,r1
	add	#-60,r1
	mov.l	@(60,r1),r1
	mov.l	@(4,r1),r9
	mov	r14,r1
	add	#-60,r1
	mov.l	@(60,r1),r1
	mov	r1,r4
	mov.l	.L361,r1
	jsr	@r1
	nop
	mov	r0,r1
	mov	r1,r6
	mov	r9,r5
	mov	r8,r4
	mov.l	.L362,r1
	jsr	@r1
	nop
	mov	r14,r1
	add	#-60,r1
	mov.l	@(60,r1),r1
	mov	r1,r4
	mov.l	.L363,r1
	jsr	@r1
	nop
	nop
	add	#4,r14
	.cfi_def_cfa_offset 16
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 12
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 8
	mov.l	@r15+,r9
	.cfi_restore 9
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r8
	.cfi_restore 8
	.cfi_def_cfa_offset 0
	rts	
	nop
.L364:
	.align 2
.L361:
	.long	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE19_M_get_Tp_allocatorEv
.L362:
	.long	_ZSt8_DestroyIPSt10shared_ptrI8hittableES2_EvT_S4_RSaIT0_E
.L363:
	.long	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED2Ev
	.cfi_endproc
.LFE4251:
	.section	.gcc_except_table
.LLSDA4251:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4251-.LLSDACSB4251
.LLSDACSB4251:
.LLSDACSE4251:
	.section	.text._ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED2Ev,"axG",@progbits,_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED5Ev,comdat
	.size	_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED2Ev, .-_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED2Ev
	.weak	_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED1Ev
	.set	_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED1Ev,_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED2Ev
	.section	.text._ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_,"axG",@progbits,_ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_,comdat
	.align 1
	.weak	_ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_
	.type	_ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_, @function
_ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_:
.LFB4253:
	.cfi_startproc
	.cfi_personality 0,__gxx_personality_v0
	.cfi_lsda 0xb,.LLSDA4253
	mov.l	r8,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 8, -4
	mov.l	r9,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 9, -8
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 12
	.cfi_offset 14, -12
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 16
	.cfi_offset 17, -16
	add	#-16,r15
	.cfi_def_cfa_offset 32
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r2,r8
	mov	r14,r1
	add	#-48,r1
	mov.l	r4,@(52,r1)
	mov	r14,r1
	add	#-48,r1
	mov.l	r5,@(48,r1)
	mov	r14,r1
	add	#-48,r1
	mov.l	.L377,r2
	mov.l	@r2,r3
	mov.l	r3,@(60,r1)
	mov	#0,r3
	mov	r14,r1
	add	#11,r1
	mov	r1,r4
	mov.l	.L372,r1
	jsr	@r1
	nop
	mov	r14,r1
	add	#-48,r1
	mov.l	@(52,r1),r4
	mov.l	.L373,r1
	jsr	@r1
	nop
	mov	r0,r9
	mov	r14,r1
	add	#-48,r1
	mov.l	@(48,r1),r4
	mov.l	.L374,r1
	jsr	@r1
	nop
	mov	r0,r1
	mov	r14,r3
	add	#11,r3
	mov	r8,r2
	mov	r1,r6
	mov	r9,r5
	mov	r3,r4
	mov.l	.L375,r1
.LEHB16:
	jsr	@r1
	nop
.LEHE16:
	mov	r14,r1
	add	#11,r1
	mov	r1,r4
	mov.l	.L378,r1
	jsr	@r1
	nop
	mov	r14,r1
	add	#-48,r1
	mov.l	.L377,r2
	mov.l	@(60,r1),r7
	mov.l	@r2,r3
	cmp/eq	r7,r3
	mov	#0,r7
	mov	#0,r3
	bt	.L368
	bra	.L370
	nop
	.align 1
.L369:
	mov	r4,r8
	mov	r14,r1
	add	#11,r1
	mov	r1,r4
	mov.l	.L378,r2
	sts	fpscr,r1
	mov.l	.L379,r3
	or	r3,r1
	lds	r1,fpscr
	jsr	@r2
	nop
	mov	r8,r1
	mov	r1,r4
	mov.l	.L380,r1
.LEHB17:
	jsr	@r1
	nop
.LEHE17:
	.align 1
.L370:
	mov.l	.L381,r1
	jsr	@r1
	nop
	.align 1
.L368:
	mov	r8,r0
	mov	r8,r0
	add	#16,r14
	.cfi_def_cfa_offset 16
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 12
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 8
	mov.l	@r15+,r9
	.cfi_restore 9
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r8
	.cfi_restore 8
	.cfi_def_cfa_offset 0
	rts	
	nop
.L382:
	.align 2
.L377:
	.long	__stack_chk_guard
.L372:
	.long	_ZNSaI6sphereEC1Ev
.L373:
	.long	_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
.L374:
	.long	_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE
.L375:
	.long	_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3dEESt10shared_ptrIT_ERKT0_DpOT1_
.L378:
	.long	_ZNSaI6sphereED1Ev
.L379:
	.long	524288
.L380:
	.long	_Unwind_Resume
.L381:
	.long	__stack_chk_fail
	.cfi_endproc
.LFE4253:
	.section	.gcc_except_table
.LLSDA4253:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4253-.LLSDACSB4253
.LLSDACSB4253:
	.uleb128 .LEHB16-.LFB4253
	.uleb128 .LEHE16-.LEHB16
	.uleb128 .L369-.LFB4253
	.uleb128 0
	.uleb128 .LEHB17-.LFB4253
	.uleb128 .LEHE17-.LEHB17
	.uleb128 0
	.uleb128 0
.LLSDACSE4253:
	.section	.text._ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_,"axG",@progbits,_ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_,comdat
	.size	_ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_, .-_ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_
	.section	.text._ZNSt10shared_ptrI8hittableEC2I6spherevEEOS_IT_E,"axG",@progbits,_ZNSt10shared_ptrI8hittableEC5I6spherevEEOS_IT_E,comdat
	.align 1
	.weak	_ZNSt10shared_ptrI8hittableEC2I6spherevEEOS_IT_E
	.type	_ZNSt10shared_ptrI8hittableEC2I6spherevEEOS_IT_E, @function
_ZNSt10shared_ptrI8hittableEC2I6spherevEEOS_IT_E:
.LFB4255:
	.cfi_startproc
	mov.l	r8,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 8, -4
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 14, -8
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 12
	.cfi_offset 17, -12
	add	#-8,r15
	.cfi_def_cfa_offset 20
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-56,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-56,r1
	mov.l	r5,@(56,r1)
	mov	r14,r1
	add	#-56,r1
	mov.l	@(60,r1),r8
	mov	r14,r1
	add	#-56,r1
	mov.l	@(56,r1),r4
	mov.l	.L384,r1
	jsr	@r1
	nop
	mov	r0,r1
	mov	r1,r5
	mov	r8,r4
	mov.l	.L385,r1
	jsr	@r1
	nop
	nop
	add	#8,r14
	.cfi_def_cfa_offset 12
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 8
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r8
	.cfi_restore 8
	.cfi_def_cfa_offset 0
	rts	
	nop
.L386:
	.align 2
.L384:
	.long	_ZSt4moveIRSt10shared_ptrI6sphereEEONSt16remove_referenceIT_E4typeEOS5_
.L385:
	.long	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC2I6spherevEEOS_IT_LS2_2EE
	.cfi_endproc
.LFE4255:
	.size	_ZNSt10shared_ptrI8hittableEC2I6spherevEEOS_IT_E, .-_ZNSt10shared_ptrI8hittableEC2I6spherevEEOS_IT_E
	.weak	_ZNSt10shared_ptrI8hittableEC1I6spherevEEOS_IT_E
	.set	_ZNSt10shared_ptrI8hittableEC1I6spherevEEOS_IT_E,_ZNSt10shared_ptrI8hittableEC2I6spherevEEOS_IT_E
	.section	.text._ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_,"axG",@progbits,_ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_,comdat
	.align 1
	.weak	_ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_
	.type	_ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_, @function
_ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_:
.LFB4260:
	.cfi_startproc
	.cfi_personality 0,__gxx_personality_v0
	.cfi_lsda 0xb,.LLSDA4260
	mov.l	r8,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 8, -4
	mov.l	r9,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 9, -8
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 12
	.cfi_offset 14, -12
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 16
	.cfi_offset 17, -16
	add	#-16,r15
	.cfi_def_cfa_offset 32
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r2,r8
	mov	r14,r1
	add	#-48,r1
	mov.l	r4,@(52,r1)
	mov	r14,r1
	add	#-48,r1
	mov.l	r5,@(48,r1)
	mov	r14,r1
	add	#-48,r1
	mov.l	.L399,r2
	mov.l	@r2,r3
	mov.l	r3,@(60,r1)
	mov	#0,r3
	mov	r14,r1
	add	#11,r1
	mov	r1,r4
	mov.l	.L394,r1
	jsr	@r1
	nop
	mov	r14,r1
	add	#-48,r1
	mov.l	@(52,r1),r4
	mov.l	.L395,r1
	jsr	@r1
	nop
	mov	r0,r9
	mov	r14,r1
	add	#-48,r1
	mov.l	@(48,r1),r4
	mov.l	.L396,r1
	jsr	@r1
	nop
	mov	r0,r1
	mov	r14,r3
	add	#11,r3
	mov	r8,r2
	mov	r1,r6
	mov	r9,r5
	mov	r3,r4
	mov.l	.L397,r1
.LEHB18:
	jsr	@r1
	nop
.LEHE18:
	mov	r14,r1
	add	#11,r1
	mov	r1,r4
	mov.l	.L400,r1
	jsr	@r1
	nop
	mov	r14,r1
	add	#-48,r1
	mov.l	.L399,r2
	mov.l	@(60,r1),r7
	mov.l	@r2,r3
	cmp/eq	r7,r3
	mov	#0,r7
	mov	#0,r3
	bt	.L390
	bra	.L392
	nop
	.align 1
.L391:
	mov	r4,r8
	mov	r14,r1
	add	#11,r1
	mov	r1,r4
	mov.l	.L400,r2
	sts	fpscr,r1
	mov.l	.L401,r3
	or	r3,r1
	lds	r1,fpscr
	jsr	@r2
	nop
	mov	r8,r1
	mov	r1,r4
	mov.l	.L402,r1
.LEHB19:
	jsr	@r1
	nop
.LEHE19:
	.align 1
.L392:
	mov.l	.L403,r1
	jsr	@r1
	nop
	.align 1
.L390:
	mov	r8,r0
	mov	r8,r0
	add	#16,r14
	.cfi_def_cfa_offset 16
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 12
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 8
	mov.l	@r15+,r9
	.cfi_restore 9
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r8
	.cfi_restore 8
	.cfi_def_cfa_offset 0
	rts	
	nop
.L404:
	.align 2
.L399:
	.long	__stack_chk_guard
.L394:
	.long	_ZNSaI6sphereEC1Ev
.L395:
	.long	_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
.L396:
	.long	_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE
.L397:
	.long	_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3iEESt10shared_ptrIT_ERKT0_DpOT1_
.L400:
	.long	_ZNSaI6sphereED1Ev
.L401:
	.long	524288
.L402:
	.long	_Unwind_Resume
.L403:
	.long	__stack_chk_fail
	.cfi_endproc
.LFE4260:
	.section	.gcc_except_table
.LLSDA4260:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4260-.LLSDACSB4260
.LLSDACSB4260:
	.uleb128 .LEHB18-.LFB4260
	.uleb128 .LEHE18-.LEHB18
	.uleb128 .L391-.LFB4260
	.uleb128 0
	.uleb128 .LEHB19-.LFB4260
	.uleb128 .LEHE19-.LEHB19
	.uleb128 0
	.uleb128 0
.LLSDACSE4260:
	.section	.text._ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_,"axG",@progbits,_ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_,comdat
	.size	_ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_, .-_ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_
	.section	.text._ZNSt25uniform_real_distributionIdE10param_typeC2Edd,"axG",@progbits,_ZNSt25uniform_real_distributionIdE10param_typeC5Edd,comdat
	.align 1
	.weak	_ZNSt25uniform_real_distributionIdE10param_typeC2Edd
	.type	_ZNSt25uniform_real_distributionIdE10param_typeC2Edd, @function
_ZNSt25uniform_real_distributionIdE10param_typeC2Edd:
.LFB4387:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	add	#-20,r15
	.cfi_def_cfa_offset 24
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-44,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#8,r1
	add	#4,r1
	fmov.s	fr4,@r1
	fmov.s	fr5,@-r1
	mov	r14,r1
	add	#4,r1
	fmov.s	fr6,@r1
	fmov.s	fr7,@-r1
	mov	r14,r1
	add	#-44,r1
	mov.l	@(60,r1),r1
	mov	r14,r2
	add	#8,r2
	fmov.s	@r2+,fr3
	fmov.s	@r2,fr2
	add	#-4,r2
	add	#4,r1
	fmov.s	fr2,@r1
	fmov.s	fr3,@-r1
	mov	r14,r1
	add	#-44,r1
	mov.l	@(60,r1),r1
	add	#8,r1
	mov	r14,r2
	fmov.s	@r2+,fr3
	fmov.s	@r2,fr2
	add	#-4,r2
	add	#4,r1
	fmov.s	fr2,@r1
	fmov.s	fr3,@-r1
	nop
	add	#20,r14
	.cfi_def_cfa_offset 4
	mov	r14,r15
	.cfi_def_cfa_register 15
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
	.cfi_endproc
.LFE4387:
	.size	_ZNSt25uniform_real_distributionIdE10param_typeC2Edd, .-_ZNSt25uniform_real_distributionIdE10param_typeC2Edd
	.weak	_ZNSt25uniform_real_distributionIdE10param_typeC1Edd
	.set	_ZNSt25uniform_real_distributionIdE10param_typeC1Edd,_ZNSt25uniform_real_distributionIdE10param_typeC2Edd
	.section	.text._ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC2Ej,"axG",@progbits,_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC5Ej,comdat
	.align 1
	.weak	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC2Ej
	.type	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC2Ej, @function
_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC2Ej:
.LFB4390:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 17, -8
	add	#-8,r15
	.cfi_def_cfa_offset 16
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-56,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-56,r1
	mov.l	r5,@(56,r1)
	mov	r14,r2
	add	#-56,r2
	mov	r14,r1
	add	#-56,r1
	mov.l	@(56,r2),r5
	mov.l	@(60,r1),r4
	mov.l	.L407,r1
	jsr	@r1
	nop
	nop
	add	#8,r14
	.cfi_def_cfa_offset 8
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
.L408:
	.align 2
.L407:
	.long	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE4seedEj
	.cfi_endproc
.LFE4390:
	.size	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC2Ej, .-_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC2Ej
	.weak	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC1Ej
	.set	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC1Ej,_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC2Ej
	.section	.text._ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEEEdRT_RKNS0_10param_typeE,"axG",@progbits,_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEEEdRT_RKNS0_10param_typeE,comdat
	.align 1
	.weak	_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEEEdRT_RKNS0_10param_typeE
	.type	_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEEEdRT_RKNS0_10param_typeE, @function
_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEEEdRT_RKNS0_10param_typeE:
.LFB4392:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	fmov.s	fr12,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 37, -8
	fmov.s	fr13,@-r15
	.cfi_def_cfa_offset 12
	.cfi_offset 38, -12
	fmov.s	fr14,@-r15
	.cfi_def_cfa_offset 16
	.cfi_offset 39, -16
	fmov.s	fr15,@-r15
	.cfi_def_cfa_offset 20
	.cfi_offset 40, -20
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 24
	.cfi_offset 17, -24
	add	#-20,r15
	.cfi_def_cfa_offset 44
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-44,r1
	mov.l	r4,@(52,r1)
	mov	r14,r1
	add	#-44,r1
	mov.l	r5,@(48,r1)
	mov	r14,r1
	add	#-44,r1
	mov.l	r6,@(44,r1)
	mov	r14,r1
	add	#-44,r1
	mov.l	.L419,r2
	mov.l	@r2,r3
	mov.l	r3,@(60,r1)
	mov	#0,r3
	mov	r14,r1
	add	#-44,r1
	mov	r14,r2
	add	#12,r2
	mov.l	@(48,r1),r5
	mov	r2,r4
	mov.l	.L414,r1
	jsr	@r1
	nop
	mov	r14,r1
	add	#12,r1
	mov	r1,r4
	mov.l	.L415,r1
	jsr	@r1
	nop
	fmov	fr0,fr12
	fmov	fr1,fr13
	mov	r14,r1
	add	#-44,r1
	mov.l	@(44,r1),r4
	mov.l	.L416,r1
	jsr	@r1
	nop
	fmov	fr0,fr14
	fmov	fr1,fr15
	mov	r14,r1
	add	#-44,r1
	mov.l	@(44,r1),r4
	mov.l	.L418,r1
	jsr	@r1
	nop
	fmov	fr0,fr2
	fmov	fr1,fr3
	fmov	fr14,fr4
	fmov	fr15,fr5
	fsub	dr2,dr4
	fmov	fr4,fr2
	fmov	fr5,fr3
	fmul	dr2,dr12
	mov	r14,r1
	add	#-44,r1
	mov.l	@(44,r1),r4
	mov.l	.L418,r1
	jsr	@r1
	nop
	fmov	fr0,fr2
	fmov	fr1,fr3
	fadd	dr12,dr2
	mov	r14,r1
	add	#-44,r1
	mov.l	.L419,r2
	mov.l	@(60,r1),r3
	mov.l	@r2,r7
	cmp/eq	r3,r7
	mov	#0,r3
	mov	#0,r7
	bt	.L412
	mov.l	.L420,r1
	jsr	@r1
	nop
	.align 1
.L412:
	fmov	fr2,fr0
	fmov	fr3,fr1
	add	#20,r14
	.cfi_def_cfa_offset 24
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 20
	fmov.s	@r15+,fr15
	.cfi_restore 40
	.cfi_def_cfa_offset 16
	fmov.s	@r15+,fr14
	.cfi_restore 39
	.cfi_def_cfa_offset 12
	fmov.s	@r15+,fr13
	.cfi_restore 38
	.cfi_def_cfa_offset 8
	fmov.s	@r15+,fr12
	.cfi_restore 37
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
.L421:
	.align 2
.L419:
	.long	__stack_chk_guard
.L414:
	.long	_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEdEC1ERS2_
.L415:
	.long	_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEdEclEv
.L416:
	.long	_ZNKSt25uniform_real_distributionIdE10param_type1bEv
.L418:
	.long	_ZNKSt25uniform_real_distributionIdE10param_type1aEv
.L420:
	.long	__stack_chk_fail
	.cfi_endproc
.LFE4392:
	.size	_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEEEdRT_RKNS0_10param_typeE, .-_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEEEdRT_RKNS0_10param_typeE
	.section	.text._ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC2Ev,"axG",@progbits,_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC5Ev,comdat
	.align 1
	.weak	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC2Ev
	.type	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC2Ev, @function
_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC2Ev:
.LFB4404:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 17, -8
	add	#-4,r15
	.cfi_def_cfa_offset 12
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-60,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-60,r1
	mov.l	@(60,r1),r1
	mov	#0,r2
	mov.l	r2,@r1
	mov	r14,r1
	add	#-60,r1
	mov.l	@(60,r1),r1
	add	#4,r1
	mov	r1,r4
	mov.l	.L423,r1
	jsr	@r1
	nop
	nop
	add	#4,r14
	.cfi_def_cfa_offset 8
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
.L424:
	.align 2
.L423:
	.long	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1Ev
	.cfi_endproc
.LFE4404:
	.size	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC2Ev, .-_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC2Ev
	.weak	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC1Ev
	.set	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC1Ev,_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC2Ev
	.section	.text._ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv,"axG",@progbits,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv,comdat
	.align 1
	.weak	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv
	.type	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv, @function
_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv:
.LFB4406:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 17, -8
	add	#-4,r15
	.cfi_def_cfa_offset 12
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-60,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-60,r1
	mov.l	@(60,r1),r1
	add	#4,r1
	mov	#-1,r5
	mov	r1,r4
	mov.l	.L429,r1
	jsr	@r1
	nop
	mov	r0,r1
	mov	r1,r0
	cmp/eq	#1,r0
	movt	r1
	extu.b	r1,r1
	tst	r1,r1
	bt	.L427
	mov	r14,r1
	add	#-60,r1
	mov.l	@(60,r1),r1
	mov.l	@r1,r1
	add	#8,r1
	mov.l	@r1,r1
	mov	r14,r2
	add	#-60,r2
	mov.l	@(60,r2),r4
	jsr	@r1
	nop
	mov	r14,r1
	add	#-60,r1
	mov.l	@(60,r1),r1
	add	#8,r1
	mov	#-1,r5
	mov	r1,r4
	mov.l	.L429,r1
	jsr	@r1
	nop
	mov	r0,r1
	mov	r1,r0
	cmp/eq	#1,r0
	movt	r1
	extu.b	r1,r1
	tst	r1,r1
	bt	.L427
	mov	r14,r1
	add	#-60,r1
	mov.l	@(60,r1),r1
	mov.l	@r1,r1
	add	#12,r1
	mov.l	@r1,r1
	mov	r14,r2
	add	#-60,r2
	mov.l	@(60,r2),r4
	jsr	@r1
	nop
.L427:
	nop
	add	#4,r14
	.cfi_def_cfa_offset 8
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
.L430:
	.align 2
.L429:
	.long	_ZN9__gnu_cxxL27__exchange_and_add_dispatchEPii
	.cfi_endproc
.LFE4406:
	.size	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv, .-_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv
	.section	.text._ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD2Ev,"axG",@progbits,_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD5Ev,comdat
	.align 1
	.weak	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD2Ev
	.type	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD2Ev, @function
_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD2Ev:
.LFB4409:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 17, -8
	add	#-4,r15
	.cfi_def_cfa_offset 12
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-60,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-60,r1
	mov.l	@(60,r1),r4
	mov.l	.L432,r1
	jsr	@r1
	nop
	nop
	add	#4,r14
	.cfi_def_cfa_offset 8
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
.L433:
	.align 2
.L432:
	.long	_ZNSaISt10shared_ptrI8hittableEED2Ev
	.cfi_endproc
.LFE4409:
	.size	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD2Ev, .-_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD2Ev
	.weak	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD1Ev
	.set	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD1Ev,_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD2Ev
	.section	.text._ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED2Ev,"axG",@progbits,_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED5Ev,comdat
	.align 1
	.weak	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED2Ev
	.type	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED2Ev, @function
_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED2Ev:
.LFB4411:
	.cfi_startproc
	.cfi_personality 0,__gxx_personality_v0
	.cfi_lsda 0xb,.LLSDA4411
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 17, -8
	add	#-4,r15
	.cfi_def_cfa_offset 12
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-60,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-60,r1
	mov.l	@(60,r1),r1
	mov.l	@r1,r3
	mov	r14,r1
	add	#-60,r1
	mov.l	@(60,r1),r1
	mov.l	@(8,r1),r2
	mov	r14,r1
	add	#-60,r1
	mov.l	@(60,r1),r1
	mov.l	@r1,r1
	sub	r1,r2
	mov	#-3,r1
	mov	r2,r7
	shad	r1,r7
	mov	r7,r1
	mov	r1,r2
	mov	r14,r1
	add	#-60,r1
	mov	r2,r6
	mov	r3,r5
	mov.l	@(60,r1),r4
	mov.l	.L435,r1
	jsr	@r1
	nop
	mov	r14,r1
	add	#-60,r1
	mov.l	@(60,r1),r1
	mov	r1,r4
	mov.l	.L436,r1
	jsr	@r1
	nop
	nop
	add	#4,r14
	.cfi_def_cfa_offset 8
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
.L437:
	.align 2
.L435:
	.long	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE13_M_deallocateEPS2_j
.L436:
	.long	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD1Ev
	.cfi_endproc
.LFE4411:
	.section	.gcc_except_table
.LLSDA4411:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4411-.LLSDACSB4411
.LLSDACSB4411:
.LLSDACSE4411:
	.section	.text._ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED2Ev,"axG",@progbits,_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED5Ev,comdat
	.size	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED2Ev, .-_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED2Ev
	.weak	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED1Ev
	.set	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED1Ev,_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED2Ev
	.section	.text._ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE19_M_get_Tp_allocatorEv,"axG",@progbits,_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE19_M_get_Tp_allocatorEv,comdat
	.align 1
	.weak	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE19_M_get_Tp_allocatorEv
	.type	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE19_M_get_Tp_allocatorEv, @function
_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE19_M_get_Tp_allocatorEv:
.LFB4413:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	add	#-4,r15
	.cfi_def_cfa_offset 8
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-60,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-60,r1
	mov.l	@(60,r1),r1
	mov	r1,r0
	add	#4,r14
	.cfi_def_cfa_offset 4
	mov	r14,r15
	.cfi_def_cfa_register 15
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
	.cfi_endproc
.LFE4413:
	.size	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE19_M_get_Tp_allocatorEv, .-_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE19_M_get_Tp_allocatorEv
	.section	.text._ZSt8_DestroyIPSt10shared_ptrI8hittableES2_EvT_S4_RSaIT0_E,"axG",@progbits,_ZSt8_DestroyIPSt10shared_ptrI8hittableES2_EvT_S4_RSaIT0_E,comdat
	.align 1
	.weak	_ZSt8_DestroyIPSt10shared_ptrI8hittableES2_EvT_S4_RSaIT0_E
	.type	_ZSt8_DestroyIPSt10shared_ptrI8hittableES2_EvT_S4_RSaIT0_E, @function
_ZSt8_DestroyIPSt10shared_ptrI8hittableES2_EvT_S4_RSaIT0_E:
.LFB4414:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 17, -8
	add	#-12,r15
	.cfi_def_cfa_offset 20
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-52,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-52,r1
	mov.l	r5,@(56,r1)
	mov	r14,r1
	add	#-52,r1
	mov.l	r6,@(52,r1)
	mov	r14,r2
	add	#-52,r2
	mov	r14,r1
	add	#-52,r1
	mov.l	@(56,r2),r5
	mov.l	@(60,r1),r4
	mov.l	.L441,r1
	jsr	@r1
	nop
	nop
	add	#12,r14
	.cfi_def_cfa_offset 8
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
.L442:
	.align 2
.L441:
	.long	_ZSt8_DestroyIPSt10shared_ptrI8hittableEEvT_S4_
	.cfi_endproc
.LFE4414:
	.size	_ZSt8_DestroyIPSt10shared_ptrI8hittableES2_EvT_S4_RSaIT0_E, .-_ZSt8_DestroyIPSt10shared_ptrI8hittableES2_EvT_S4_RSaIT0_E
	.section	.text._ZNSaI6sphereEC2Ev,"axG",@progbits,_ZNSaI6sphereEC5Ev,comdat
	.align 1
	.weak	_ZNSaI6sphereEC2Ev
	.type	_ZNSaI6sphereEC2Ev, @function
_ZNSaI6sphereEC2Ev:
.LFB4416:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 17, -8
	add	#-4,r15
	.cfi_def_cfa_offset 12
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-60,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-60,r1
	mov.l	@(60,r1),r4
	mov.l	.L444,r1
	jsr	@r1
	nop
	nop
	add	#4,r14
	.cfi_def_cfa_offset 8
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
.L445:
	.align 2
.L444:
	.long	_ZN9__gnu_cxx13new_allocatorI6sphereEC2Ev
	.cfi_endproc
.LFE4416:
	.size	_ZNSaI6sphereEC2Ev, .-_ZNSaI6sphereEC2Ev
	.weak	_ZNSaI6sphereEC1Ev
	.set	_ZNSaI6sphereEC1Ev,_ZNSaI6sphereEC2Ev
	.section	.text._ZNSaI6sphereED2Ev,"axG",@progbits,_ZNSaI6sphereED5Ev,comdat
	.align 1
	.weak	_ZNSaI6sphereED2Ev
	.type	_ZNSaI6sphereED2Ev, @function
_ZNSaI6sphereED2Ev:
.LFB4419:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 17, -8
	add	#-4,r15
	.cfi_def_cfa_offset 12
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-60,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-60,r1
	mov.l	@(60,r1),r4
	mov.l	.L447,r1
	jsr	@r1
	nop
	nop
	add	#4,r14
	.cfi_def_cfa_offset 8
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
.L448:
	.align 2
.L447:
	.long	_ZN9__gnu_cxx13new_allocatorI6sphereED2Ev
	.cfi_endproc
.LFE4419:
	.size	_ZNSaI6sphereED2Ev, .-_ZNSaI6sphereED2Ev
	.weak	_ZNSaI6sphereED1Ev
	.set	_ZNSaI6sphereED1Ev,_ZNSaI6sphereED2Ev
	.section	.text._ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE,"axG",@progbits,_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE,comdat
	.align 1
	.weak	_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
	.type	_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE, @function
_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE:
.LFB4421:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	add	#-4,r15
	.cfi_def_cfa_offset 8
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-60,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-60,r1
	mov.l	@(60,r1),r1
	mov	r1,r0
	add	#4,r14
	.cfi_def_cfa_offset 4
	mov	r14,r15
	.cfi_def_cfa_register 15
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
	.cfi_endproc
.LFE4421:
	.size	_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE, .-_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
	.section	.text._ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE,"axG",@progbits,_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE,comdat
	.align 1
	.weak	_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE
	.type	_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE, @function
_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE:
.LFB4422:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	add	#-4,r15
	.cfi_def_cfa_offset 8
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-60,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-60,r1
	mov.l	@(60,r1),r1
	mov	r1,r0
	add	#4,r14
	.cfi_def_cfa_offset 4
	mov	r14,r15
	.cfi_def_cfa_register 15
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
	.cfi_endproc
.LFE4422:
	.size	_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE, .-_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE
	.section	.text._ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3dEESt10shared_ptrIT_ERKT0_DpOT1_,"axG",@progbits,_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3dEESt10shared_ptrIT_ERKT0_DpOT1_,comdat
	.align 1
	.weak	_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3dEESt10shared_ptrIT_ERKT0_DpOT1_
	.type	_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3dEESt10shared_ptrIT_ERKT0_DpOT1_, @function
_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3dEESt10shared_ptrIT_ERKT0_DpOT1_:
.LFB4423:
	.cfi_startproc
	mov.l	r8,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 8, -4
	mov.l	r9,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 9, -8
	mov.l	r10,@-r15
	.cfi_def_cfa_offset 12
	.cfi_offset 10, -12
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 16
	.cfi_offset 14, -16
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 20
	.cfi_offset 17, -20
	add	#-12,r15
	.cfi_def_cfa_offset 32
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r2,r8
	mov	r14,r1
	add	#-52,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-52,r1
	mov.l	r5,@(56,r1)
	mov	r14,r1
	add	#-52,r1
	mov.l	r6,@(52,r1)
	mov	r14,r1
	add	#-52,r1
	mov.l	@(60,r1),r10
	mov	r14,r1
	add	#-52,r1
	mov.l	@(56,r1),r4
	mov.l	.L455,r1
	jsr	@r1
	nop
	mov	r0,r9
	mov	r14,r1
	add	#-52,r1
	mov.l	@(52,r1),r4
	mov.l	.L456,r1
	jsr	@r1
	nop
	mov	r0,r1
	mov	r1,r7
	mov	r9,r6
	mov	r10,r5
	mov	r8,r4
	mov.l	.L457,r1
	jsr	@r1
	nop
	mov	r8,r0
	mov	r8,r0
	add	#12,r14
	.cfi_def_cfa_offset 20
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 16
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 12
	mov.l	@r15+,r10
	.cfi_restore 10
	.cfi_def_cfa_offset 8
	mov.l	@r15+,r9
	.cfi_restore 9
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r8
	.cfi_restore 8
	.cfi_def_cfa_offset 0
	rts	
	nop
.L458:
	.align 2
.L455:
	.long	_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
.L456:
	.long	_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE
.L457:
	.long	_ZNSt10shared_ptrI6sphereEC1ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.cfi_endproc
.LFE4423:
	.size	_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3dEESt10shared_ptrIT_ERKT0_DpOT1_, .-_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3dEESt10shared_ptrIT_ERKT0_DpOT1_
	.section	.text._ZSt4moveIRSt10shared_ptrI6sphereEEONSt16remove_referenceIT_E4typeEOS5_,"axG",@progbits,_ZSt4moveIRSt10shared_ptrI6sphereEEONSt16remove_referenceIT_E4typeEOS5_,comdat
	.align 1
	.weak	_ZSt4moveIRSt10shared_ptrI6sphereEEONSt16remove_referenceIT_E4typeEOS5_
	.type	_ZSt4moveIRSt10shared_ptrI6sphereEEONSt16remove_referenceIT_E4typeEOS5_, @function
_ZSt4moveIRSt10shared_ptrI6sphereEEONSt16remove_referenceIT_E4typeEOS5_:
.LFB4427:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	add	#-4,r15
	.cfi_def_cfa_offset 8
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-60,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-60,r1
	mov.l	@(60,r1),r1
	mov	r1,r0
	add	#4,r14
	.cfi_def_cfa_offset 4
	mov	r14,r15
	.cfi_def_cfa_register 15
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
	.cfi_endproc
.LFE4427:
	.size	_ZSt4moveIRSt10shared_ptrI6sphereEEONSt16remove_referenceIT_E4typeEOS5_, .-_ZSt4moveIRSt10shared_ptrI6sphereEEONSt16remove_referenceIT_E4typeEOS5_
	.section	.text._ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC2I6spherevEEOS_IT_LS2_2EE,"axG",@progbits,_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC5I6spherevEEOS_IT_LS2_2EE,comdat
	.align 1
	.weak	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC2I6spherevEEOS_IT_LS2_2EE
	.type	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC2I6spherevEEOS_IT_LS2_2EE, @function
_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC2I6spherevEEOS_IT_LS2_2EE:
.LFB4429:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 17, -8
	add	#-8,r15
	.cfi_def_cfa_offset 16
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-56,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-56,r1
	mov.l	r5,@(56,r1)
	mov	r14,r1
	add	#-56,r1
	mov.l	@(56,r1),r1
	mov.l	@r1,r2
	mov	r14,r1
	add	#-56,r1
	mov.l	@(60,r1),r1
	mov.l	r2,@r1
	mov	r14,r1
	add	#-56,r1
	mov.l	@(60,r1),r1
	add	#4,r1
	mov	r1,r4
	mov.l	.L462,r1
	jsr	@r1
	nop
	mov	r14,r1
	add	#-56,r1
	mov.l	@(60,r1),r1
	mov	r1,r2
	add	#4,r2
	mov	r14,r1
	add	#-56,r1
	mov.l	@(56,r1),r1
	add	#4,r1
	mov	r1,r5
	mov	r2,r4
	mov.l	.L463,r1
	jsr	@r1
	nop
	mov	r14,r1
	add	#-56,r1
	mov.l	@(56,r1),r1
	mov	#0,r2
	mov.l	r2,@r1
	nop
	add	#8,r14
	.cfi_def_cfa_offset 8
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
.L464:
	.align 2
.L462:
	.long	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1Ev
.L463:
	.long	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EE7_M_swapERS2_
	.cfi_endproc
.LFE4429:
	.size	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC2I6spherevEEOS_IT_LS2_2EE, .-_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC2I6spherevEEOS_IT_LS2_2EE
	.weak	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC1I6spherevEEOS_IT_LS2_2EE
	.set	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC1I6spherevEEOS_IT_LS2_2EE,_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC2I6spherevEEOS_IT_LS2_2EE
	.section	.text._ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE,"axG",@progbits,_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE,comdat
	.align 1
	.weak	_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE
	.type	_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE, @function
_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE:
.LFB4435:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	add	#-4,r15
	.cfi_def_cfa_offset 8
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-60,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-60,r1
	mov.l	@(60,r1),r1
	mov	r1,r0
	add	#4,r14
	.cfi_def_cfa_offset 4
	mov	r14,r15
	.cfi_def_cfa_register 15
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
	.cfi_endproc
.LFE4435:
	.size	_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE, .-_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE
	.section	.text._ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3iEESt10shared_ptrIT_ERKT0_DpOT1_,"axG",@progbits,_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3iEESt10shared_ptrIT_ERKT0_DpOT1_,comdat
	.align 1
	.weak	_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3iEESt10shared_ptrIT_ERKT0_DpOT1_
	.type	_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3iEESt10shared_ptrIT_ERKT0_DpOT1_, @function
_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3iEESt10shared_ptrIT_ERKT0_DpOT1_:
.LFB4436:
	.cfi_startproc
	mov.l	r8,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 8, -4
	mov.l	r9,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 9, -8
	mov.l	r10,@-r15
	.cfi_def_cfa_offset 12
	.cfi_offset 10, -12
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 16
	.cfi_offset 14, -16
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 20
	.cfi_offset 17, -20
	add	#-12,r15
	.cfi_def_cfa_offset 32
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r2,r8
	mov	r14,r1
	add	#-52,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-52,r1
	mov.l	r5,@(56,r1)
	mov	r14,r1
	add	#-52,r1
	mov.l	r6,@(52,r1)
	mov	r14,r1
	add	#-52,r1
	mov.l	@(60,r1),r10
	mov	r14,r1
	add	#-52,r1
	mov.l	@(56,r1),r4
	mov.l	.L469,r1
	jsr	@r1
	nop
	mov	r0,r9
	mov	r14,r1
	add	#-52,r1
	mov.l	@(52,r1),r4
	mov.l	.L470,r1
	jsr	@r1
	nop
	mov	r0,r1
	mov	r1,r7
	mov	r9,r6
	mov	r10,r5
	mov	r8,r4
	mov.l	.L471,r1
	jsr	@r1
	nop
	mov	r8,r0
	mov	r8,r0
	add	#12,r14
	.cfi_def_cfa_offset 20
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 16
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 12
	mov.l	@r15+,r10
	.cfi_restore 10
	.cfi_def_cfa_offset 8
	mov.l	@r15+,r9
	.cfi_restore 9
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r8
	.cfi_restore 8
	.cfi_def_cfa_offset 0
	rts	
	nop
.L472:
	.align 2
.L469:
	.long	_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
.L470:
	.long	_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE
.L471:
	.long	_ZNSt10shared_ptrI6sphereEC1ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.cfi_endproc
.LFE4436:
	.size	_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3iEESt10shared_ptrIT_ERKT0_DpOT1_, .-_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3iEESt10shared_ptrIT_ERKT0_DpOT1_
	.section	.text._ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE4seedEj,"axG",@progbits,_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE4seedEj,comdat
	.align 1
	.weak	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE4seedEj
	.type	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE4seedEj, @function
_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE4seedEj:
.LFB4491:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 17, -8
	add	#-16,r15
	.cfi_def_cfa_offset 24
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-48,r1
	mov.l	r4,@(52,r1)
	mov	r14,r1
	add	#-48,r1
	mov.l	r5,@(48,r1)
	mov	r14,r1
	add	#-48,r1
	mov.l	@(48,r1),r4
	mov.l	.L480,r1
	jsr	@r1
	nop
	mov	r0,r1
	mov	r14,r2
	add	#-48,r2
	mov.l	@(52,r2),r2
	mov.l	r1,@r2
	mov	r14,r1
	add	#-48,r1
	mov	#1,r2
	mov.l	r2,@(56,r1)
.L475:
	mov	r14,r1
	add	#-48,r1
	mov.l	@(56,r1),r2
	mov.w	.L477,r1
	cmp/hi	r1,r2
	bt	.L474
	mov	r14,r1
	add	#-48,r1
	mov.l	@(56,r1),r1
	mov	r1,r2
	add	#-1,r2
	mov	r14,r1
	add	#-48,r1
	mov	r14,r3
	add	#-48,r3
	mov.l	@(52,r3),r3
	shll2	r2
	add	r3,r2
	mov.l	@r2,r2
	mov.l	r2,@(60,r1)
	mov	r14,r1
	add	#-48,r1
	mov.l	@(60,r1),r1
	mov	r1,r2
	mov	#-30,r3
	shld	r3,r2
	mov	r14,r1
	add	#-48,r1
	mov	r14,r3
	add	#-48,r3
	mov.l	@(60,r3),r3
	xor	r3,r2
	mov.l	r2,@(60,r1)
	mov	r14,r1
	add	#-48,r1
	mov	r14,r2
	add	#-48,r2
	mov.l	@(60,r2),r3
	mov.l	.L478,r2
	mul.l	r2,r3
	sts	macl,r2
	mov.l	r2,@(60,r1)
	mov	r14,r1
	add	#-48,r1
	mov.l	@(56,r1),r4
	mov.l	.L479,r1
	jsr	@r1
	nop
	mov	r0,r1
	mov	r14,r2
	add	#-48,r2
	mov	r14,r3
	add	#-48,r3
	mov.l	@(60,r3),r3
	add	r3,r1
	mov.l	r1,@(60,r2)
	mov	r14,r1
	add	#-48,r1
	mov.l	@(60,r1),r4
	mov.l	.L480,r1
	jsr	@r1
	nop
	mov	r0,r1
	mov	r14,r2
	add	#-48,r2
	mov.l	@(52,r2),r3
	mov	r14,r2
	add	#-48,r2
	mov.l	@(56,r2),r2
	shll2	r2
	add	r3,r2
	mov.l	r1,@r2
	mov	r14,r1
	add	#-48,r1
	mov	r14,r2
	add	#-48,r2
	mov.l	@(56,r2),r2
	add	#1,r2
	mov.l	r2,@(56,r1)
	bra	.L475
	nop
	.align 1
.L474:
	mov	r14,r1
	add	#-48,r1
	mov.l	@(52,r1),r1
	mov.w	.L481,r2
	add	r2,r1
	mov.w	.L482,r2
	mov.l	r2,@(4,r1)
	nop
	add	#16,r14
	.cfi_def_cfa_offset 8
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
	.align 1
.L477:
	.short	623
.L481:
	.short	2492
.L482:
	.short	624
.L483:
	.align 2
.L480:
	.long	_ZNSt8__detail5__modIjLj0ELj1ELj0EEET_S1_
.L478:
	.long	1812433253
.L479:
	.long	_ZNSt8__detail5__modIjLj624ELj1ELj0EEET_S1_
	.cfi_endproc
.LFE4491:
	.size	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE4seedEj, .-_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE4seedEj
	.section	.text._ZNSt8__detail8_AdaptorISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEdEC2ERS2_,"axG",@progbits,_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEdEC5ERS2_,comdat
	.align 1
	.weak	_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEdEC2ERS2_
	.type	_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEdEC2ERS2_, @function
_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEdEC2ERS2_:
.LFB4493:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	add	#-8,r15
	.cfi_def_cfa_offset 12
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-56,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-56,r1
	mov.l	r5,@(56,r1)
	mov	r14,r1
	add	#-56,r1
	mov.l	@(60,r1),r1
	mov	r14,r2
	add	#-56,r2
	mov.l	@(56,r2),r2
	mov.l	r2,@r1
	nop
	add	#8,r14
	.cfi_def_cfa_offset 4
	mov	r14,r15
	.cfi_def_cfa_register 15
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
	.cfi_endproc
.LFE4493:
	.size	_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEdEC2ERS2_, .-_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEdEC2ERS2_
	.weak	_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEdEC1ERS2_
	.set	_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEdEC1ERS2_,_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEdEC2ERS2_
	.section	.text._ZNSt8__detail8_AdaptorISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEdEclEv,"axG",@progbits,_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEdEclEv,comdat
	.align 1
	.weak	_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEdEclEv
	.type	_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEdEclEv, @function
_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEdEclEv:
.LFB4495:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 17, -8
	add	#-4,r15
	.cfi_def_cfa_offset 12
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-60,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-60,r1
	mov.l	@(60,r1),r1
	mov.l	@r1,r1
	mov	r1,r4
	mov.l	.L487,r1
	jsr	@r1
	nop
	fmov	fr0,fr2
	fmov	fr1,fr3
	fmov	fr2,fr0
	fmov	fr3,fr1
	add	#4,r14
	.cfi_def_cfa_offset 8
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
.L488:
	.align 2
.L487:
	.long	_ZSt18generate_canonicalIdLj53ESt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEET_RT1_
	.cfi_endproc
.LFE4495:
	.size	_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEdEclEv, .-_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEdEclEv
	.section	.text._ZNKSt25uniform_real_distributionIdE10param_type1bEv,"axG",@progbits,_ZNKSt25uniform_real_distributionIdE10param_type1bEv,comdat
	.align 1
	.weak	_ZNKSt25uniform_real_distributionIdE10param_type1bEv
	.type	_ZNKSt25uniform_real_distributionIdE10param_type1bEv, @function
_ZNKSt25uniform_real_distributionIdE10param_type1bEv:
.LFB4496:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	add	#-4,r15
	.cfi_def_cfa_offset 8
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-60,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-60,r1
	mov.l	@(60,r1),r1
	add	#8,r1
	fmov.s	@r1+,fr3
	fmov.s	@r1,fr2
	add	#-4,r1
	fmov	fr2,fr0
	fmov	fr3,fr1
	add	#4,r14
	.cfi_def_cfa_offset 4
	mov	r14,r15
	.cfi_def_cfa_register 15
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
	.cfi_endproc
.LFE4496:
	.size	_ZNKSt25uniform_real_distributionIdE10param_type1bEv, .-_ZNKSt25uniform_real_distributionIdE10param_type1bEv
	.section	.text._ZNKSt25uniform_real_distributionIdE10param_type1aEv,"axG",@progbits,_ZNKSt25uniform_real_distributionIdE10param_type1aEv,comdat
	.align 1
	.weak	_ZNKSt25uniform_real_distributionIdE10param_type1aEv
	.type	_ZNKSt25uniform_real_distributionIdE10param_type1aEv, @function
_ZNKSt25uniform_real_distributionIdE10param_type1aEv:
.LFB4497:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	add	#-4,r15
	.cfi_def_cfa_offset 8
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-60,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-60,r1
	mov.l	@(60,r1),r1
	fmov.s	@r1+,fr3
	fmov.s	@r1,fr2
	add	#-4,r1
	fmov	fr2,fr0
	fmov	fr3,fr1
	add	#4,r14
	.cfi_def_cfa_offset 4
	mov	r14,r15
	.cfi_def_cfa_register 15
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
	.cfi_endproc
.LFE4497:
	.size	_ZNKSt25uniform_real_distributionIdE10param_type1aEv, .-_ZNKSt25uniform_real_distributionIdE10param_type1aEv
	.section	.text._ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2Ev,"axG",@progbits,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC5Ev,comdat
	.align 1
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2Ev
	.type	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2Ev, @function
_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2Ev:
.LFB4503:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	add	#-4,r15
	.cfi_def_cfa_offset 8
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-60,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-60,r1
	mov.l	@(60,r1),r1
	mov	#0,r2
	mov.l	r2,@r1
	nop
	add	#4,r14
	.cfi_def_cfa_offset 4
	mov	r14,r15
	.cfi_def_cfa_register 15
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
	.cfi_endproc
.LFE4503:
	.size	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2Ev, .-_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2Ev
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1Ev
	.set	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1Ev,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2Ev
	.section	.text._ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,"axG",@progbits,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,comdat
	.align 1
	.weak	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.type	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, @function
_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv:
.LFB4505:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 17, -8
	add	#-4,r15
	.cfi_def_cfa_offset 12
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-60,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-60,r1
	mov.l	@(60,r1),r1
	tst	r1,r1
	bt	.L496
	mov	r14,r1
	add	#-60,r1
	mov.l	@(60,r1),r1
	mov.l	@r1,r1
	add	#4,r1
	mov.l	@r1,r1
	mov	r14,r2
	add	#-60,r2
	mov.l	@(60,r2),r4
	jsr	@r1
	nop
.L496:
	nop
	add	#4,r14
	.cfi_def_cfa_offset 8
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
	.cfi_endproc
.LFE4505:
	.size	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, .-_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.section	.text._ZNSaISt10shared_ptrI8hittableEED2Ev,"axG",@progbits,_ZNSaISt10shared_ptrI8hittableEED5Ev,comdat
	.align 1
	.weak	_ZNSaISt10shared_ptrI8hittableEED2Ev
	.type	_ZNSaISt10shared_ptrI8hittableEED2Ev, @function
_ZNSaISt10shared_ptrI8hittableEED2Ev:
.LFB4507:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 17, -8
	add	#-4,r15
	.cfi_def_cfa_offset 12
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-60,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-60,r1
	mov.l	@(60,r1),r4
	mov.l	.L498,r1
	jsr	@r1
	nop
	nop
	add	#4,r14
	.cfi_def_cfa_offset 8
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
.L499:
	.align 2
.L498:
	.long	_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED2Ev
	.cfi_endproc
.LFE4507:
	.size	_ZNSaISt10shared_ptrI8hittableEED2Ev, .-_ZNSaISt10shared_ptrI8hittableEED2Ev
	.weak	_ZNSaISt10shared_ptrI8hittableEED1Ev
	.set	_ZNSaISt10shared_ptrI8hittableEED1Ev,_ZNSaISt10shared_ptrI8hittableEED2Ev
	.section	.text._ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE13_M_deallocateEPS2_j,"axG",@progbits,_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE13_M_deallocateEPS2_j,comdat
	.align 1
	.weak	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE13_M_deallocateEPS2_j
	.type	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE13_M_deallocateEPS2_j, @function
_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE13_M_deallocateEPS2_j:
.LFB4509:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 17, -8
	add	#-12,r15
	.cfi_def_cfa_offset 20
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-52,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-52,r1
	mov.l	r5,@(56,r1)
	mov	r14,r1
	add	#-52,r1
	mov.l	r6,@(52,r1)
	mov	r14,r1
	add	#-52,r1
	mov.l	@(56,r1),r1
	tst	r1,r1
	bt	.L502
	mov	r14,r1
	add	#-52,r1
	mov.l	@(60,r1),r3
	mov	r14,r2
	add	#-52,r2
	mov	r14,r1
	add	#-52,r1
	mov.l	@(52,r2),r6
	mov.l	@(56,r1),r5
	mov	r3,r4
	mov.l	.L503,r1
	jsr	@r1
	nop
.L502:
	nop
	add	#12,r14
	.cfi_def_cfa_offset 8
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
.L504:
	.align 2
.L503:
	.long	_ZNSt16allocator_traitsISaISt10shared_ptrI8hittableEEE10deallocateERS3_PS2_j
	.cfi_endproc
.LFE4509:
	.size	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE13_M_deallocateEPS2_j, .-_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE13_M_deallocateEPS2_j
	.section	.text._ZSt8_DestroyIPSt10shared_ptrI8hittableEEvT_S4_,"axG",@progbits,_ZSt8_DestroyIPSt10shared_ptrI8hittableEEvT_S4_,comdat
	.align 1
	.weak	_ZSt8_DestroyIPSt10shared_ptrI8hittableEEvT_S4_
	.type	_ZSt8_DestroyIPSt10shared_ptrI8hittableEEvT_S4_, @function
_ZSt8_DestroyIPSt10shared_ptrI8hittableEEvT_S4_:
.LFB4510:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 17, -8
	add	#-8,r15
	.cfi_def_cfa_offset 16
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-56,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-56,r1
	mov.l	r5,@(56,r1)
	mov	r14,r2
	add	#-56,r2
	mov	r14,r1
	add	#-56,r1
	mov.l	@(56,r2),r5
	mov.l	@(60,r1),r4
	mov.l	.L506,r1
	jsr	@r1
	nop
	nop
	add	#8,r14
	.cfi_def_cfa_offset 8
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
.L507:
	.align 2
.L506:
	.long	_ZNSt12_Destroy_auxILb0EE9__destroyIPSt10shared_ptrI8hittableEEEvT_S6_
	.cfi_endproc
.LFE4510:
	.size	_ZSt8_DestroyIPSt10shared_ptrI8hittableEEvT_S4_, .-_ZSt8_DestroyIPSt10shared_ptrI8hittableEEvT_S4_
	.section	.text._ZN9__gnu_cxx13new_allocatorI6sphereEC2Ev,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorI6sphereEC5Ev,comdat
	.align 1
	.weak	_ZN9__gnu_cxx13new_allocatorI6sphereEC2Ev
	.type	_ZN9__gnu_cxx13new_allocatorI6sphereEC2Ev, @function
_ZN9__gnu_cxx13new_allocatorI6sphereEC2Ev:
.LFB4512:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	add	#-4,r15
	.cfi_def_cfa_offset 8
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-60,r1
	mov.l	r4,@(60,r1)
	nop
	add	#4,r14
	.cfi_def_cfa_offset 4
	mov	r14,r15
	.cfi_def_cfa_register 15
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
	.cfi_endproc
.LFE4512:
	.size	_ZN9__gnu_cxx13new_allocatorI6sphereEC2Ev, .-_ZN9__gnu_cxx13new_allocatorI6sphereEC2Ev
	.weak	_ZN9__gnu_cxx13new_allocatorI6sphereEC1Ev
	.set	_ZN9__gnu_cxx13new_allocatorI6sphereEC1Ev,_ZN9__gnu_cxx13new_allocatorI6sphereEC2Ev
	.section	.text._ZN9__gnu_cxx13new_allocatorI6sphereED2Ev,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorI6sphereED5Ev,comdat
	.align 1
	.weak	_ZN9__gnu_cxx13new_allocatorI6sphereED2Ev
	.type	_ZN9__gnu_cxx13new_allocatorI6sphereED2Ev, @function
_ZN9__gnu_cxx13new_allocatorI6sphereED2Ev:
.LFB4515:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	add	#-4,r15
	.cfi_def_cfa_offset 8
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-60,r1
	mov.l	r4,@(60,r1)
	nop
	add	#4,r14
	.cfi_def_cfa_offset 4
	mov	r14,r15
	.cfi_def_cfa_register 15
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
	.cfi_endproc
.LFE4515:
	.size	_ZN9__gnu_cxx13new_allocatorI6sphereED2Ev, .-_ZN9__gnu_cxx13new_allocatorI6sphereED2Ev
	.weak	_ZN9__gnu_cxx13new_allocatorI6sphereED1Ev
	.set	_ZN9__gnu_cxx13new_allocatorI6sphereED1Ev,_ZN9__gnu_cxx13new_allocatorI6sphereED2Ev
	.section	.text._ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,"axG",@progbits,_ZNSt10shared_ptrI6sphereEC5ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,comdat
	.align 1
	.weak	_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.type	_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_, @function
_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_:
.LFB4518:
	.cfi_startproc
	mov.l	r8,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 8, -4
	mov.l	r9,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 9, -8
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 12
	.cfi_offset 14, -12
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 16
	.cfi_offset 17, -16
	add	#-16,r15
	.cfi_def_cfa_offset 32
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-48,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-48,r1
	mov.l	r5,@(56,r1)
	mov	r14,r1
	add	#-48,r1
	mov.l	r6,@(52,r1)
	mov	r14,r1
	add	#-48,r1
	mov.l	r7,@(48,r1)
	mov	r14,r1
	add	#-48,r1
	mov.l	@(60,r1),r9
	mov	r14,r1
	add	#-48,r1
	mov.l	@(52,r1),r4
	mov.l	.L511,r1
	jsr	@r1
	nop
	mov	r0,r8
	mov	r14,r1
	add	#-48,r1
	mov.l	@(48,r1),r4
	mov.l	.L512,r1
	jsr	@r1
	nop
	mov	r0,r2
	mov	r14,r1
	add	#-48,r1
	mov	r2,r7
	mov	r8,r6
	mov.l	@(56,r1),r5
	mov	r9,r4
	mov.l	.L513,r1
	jsr	@r1
	nop
	nop
	add	#16,r14
	.cfi_def_cfa_offset 16
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 12
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 8
	mov.l	@r15+,r9
	.cfi_restore 9
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r8
	.cfi_restore 8
	.cfi_def_cfa_offset 0
	rts	
	nop
.L514:
	.align 2
.L511:
	.long	_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
.L512:
	.long	_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE
.L513:
	.long	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.cfi_endproc
.LFE4518:
	.size	_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_, .-_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.weak	_ZNSt10shared_ptrI6sphereEC1ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.set	_ZNSt10shared_ptrI6sphereEC1ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.section	.text._ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EE7_M_swapERS2_,"axG",@progbits,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EE7_M_swapERS2_,comdat
	.align 1
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EE7_M_swapERS2_
	.type	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EE7_M_swapERS2_, @function
_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EE7_M_swapERS2_:
.LFB4523:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	add	#-12,r15
	.cfi_def_cfa_offset 16
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-52,r1
	mov.l	r4,@(56,r1)
	mov	r14,r1
	add	#-52,r1
	mov.l	r5,@(52,r1)
	mov	r14,r1
	add	#-52,r1
	mov	r14,r2
	add	#-52,r2
	mov.l	@(52,r2),r2
	mov.l	@r2,r2
	mov.l	r2,@(60,r1)
	mov	r14,r1
	add	#-52,r1
	mov.l	@(56,r1),r1
	mov.l	@r1,r2
	mov	r14,r1
	add	#-52,r1
	mov.l	@(52,r1),r1
	mov.l	r2,@r1
	mov	r14,r1
	add	#-52,r1
	mov.l	@(56,r1),r1
	mov	r14,r2
	add	#-52,r2
	mov.l	@(60,r2),r2
	mov.l	r2,@r1
	nop
	add	#12,r14
	.cfi_def_cfa_offset 4
	mov	r14,r15
	.cfi_def_cfa_register 15
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
	.cfi_endproc
.LFE4523:
	.size	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EE7_M_swapERS2_, .-_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EE7_M_swapERS2_
	.section	.text._ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,"axG",@progbits,_ZNSt10shared_ptrI6sphereEC5ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,comdat
	.align 1
	.weak	_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.type	_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_, @function
_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_:
.LFB4525:
	.cfi_startproc
	mov.l	r8,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 8, -4
	mov.l	r9,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 9, -8
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 12
	.cfi_offset 14, -12
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 16
	.cfi_offset 17, -16
	add	#-16,r15
	.cfi_def_cfa_offset 32
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-48,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-48,r1
	mov.l	r5,@(56,r1)
	mov	r14,r1
	add	#-48,r1
	mov.l	r6,@(52,r1)
	mov	r14,r1
	add	#-48,r1
	mov.l	r7,@(48,r1)
	mov	r14,r1
	add	#-48,r1
	mov.l	@(60,r1),r9
	mov	r14,r1
	add	#-48,r1
	mov.l	@(52,r1),r4
	mov.l	.L517,r1
	jsr	@r1
	nop
	mov	r0,r8
	mov	r14,r1
	add	#-48,r1
	mov.l	@(48,r1),r4
	mov.l	.L518,r1
	jsr	@r1
	nop
	mov	r0,r2
	mov	r14,r1
	add	#-48,r1
	mov	r2,r7
	mov	r8,r6
	mov.l	@(56,r1),r5
	mov	r9,r4
	mov.l	.L519,r1
	jsr	@r1
	nop
	nop
	add	#16,r14
	.cfi_def_cfa_offset 16
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 12
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 8
	mov.l	@r15+,r9
	.cfi_restore 9
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r8
	.cfi_restore 8
	.cfi_def_cfa_offset 0
	rts	
	nop
.L520:
	.align 2
.L517:
	.long	_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
.L518:
	.long	_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE
.L519:
	.long	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.cfi_endproc
.LFE4525:
	.size	_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_, .-_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.weak	_ZNSt10shared_ptrI6sphereEC1ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.set	_ZNSt10shared_ptrI6sphereEC1ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.section	.text._ZNSt8__detail5__modIjLj0ELj1ELj0EEET_S1_,"axG",@progbits,_ZNSt8__detail5__modIjLj0ELj1ELj0EEET_S1_,comdat
	.align 1
	.weak	_ZNSt8__detail5__modIjLj0ELj1ELj0EEET_S1_
	.type	_ZNSt8__detail5__modIjLj0ELj1ELj0EEET_S1_, @function
_ZNSt8__detail5__modIjLj0ELj1ELj0EEET_S1_:
.LFB4564:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 17, -8
	add	#-4,r15
	.cfi_def_cfa_offset 12
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-60,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-60,r1
	mov.l	@(60,r1),r4
	mov.l	.L523,r1
	jsr	@r1
	nop
	mov	r0,r1
	mov	r1,r0
	add	#4,r14
	.cfi_def_cfa_offset 8
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
.L524:
	.align 2
.L523:
	.long	_ZNSt8__detail4_ModIjLj0ELj1ELj0ELb1ELb0EE6__calcEj
	.cfi_endproc
.LFE4564:
	.size	_ZNSt8__detail5__modIjLj0ELj1ELj0EEET_S1_, .-_ZNSt8__detail5__modIjLj0ELj1ELj0EEET_S1_
	.section	.text._ZNSt8__detail5__modIjLj624ELj1ELj0EEET_S1_,"axG",@progbits,_ZNSt8__detail5__modIjLj624ELj1ELj0EEET_S1_,comdat
	.align 1
	.weak	_ZNSt8__detail5__modIjLj624ELj1ELj0EEET_S1_
	.type	_ZNSt8__detail5__modIjLj624ELj1ELj0EEET_S1_, @function
_ZNSt8__detail5__modIjLj624ELj1ELj0EEET_S1_:
.LFB4565:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 17, -8
	add	#-4,r15
	.cfi_def_cfa_offset 12
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-60,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-60,r1
	mov.l	@(60,r1),r4
	mov.l	.L527,r1
	jsr	@r1
	nop
	mov	r0,r1
	mov	r1,r0
	add	#4,r14
	.cfi_def_cfa_offset 8
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
.L528:
	.align 2
.L527:
	.long	_ZNSt8__detail4_ModIjLj624ELj1ELj0ELb1ELb1EE6__calcEj
	.cfi_endproc
.LFE4565:
	.size	_ZNSt8__detail5__modIjLj624ELj1ELj0EEET_S1_, .-_ZNSt8__detail5__modIjLj624ELj1ELj0EEET_S1_
	.section	.text._ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE3minEv,"axG",@progbits,_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE3minEv,comdat
	.align 1
	.weak	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE3minEv
	.type	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE3minEv, @function
_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE3minEv:
.LFB4569:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	#0,r1
	mov	r1,r0
	mov	r14,r15
	.cfi_def_cfa_register 15
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
	.cfi_endproc
.LFE4569:
	.size	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE3minEv, .-_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE3minEv
	.section	.text._ZSt18generate_canonicalIdLj53ESt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEET_RT1_,"axG",@progbits,_ZSt18generate_canonicalIdLj53ESt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEET_RT1_,comdat
	.align 1
	.weak	_ZSt18generate_canonicalIdLj53ESt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEET_RT1_
	.type	_ZSt18generate_canonicalIdLj53ESt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEET_RT1_, @function
_ZSt18generate_canonicalIdLj53ESt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEET_RT1_:
.LFB4566:
	.cfi_startproc
	mov.l	r8,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 8, -4
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 14, -8
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 12
	.cfi_offset 17, -12
	add	#-52,r15
	.cfi_def_cfa_offset 64
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-12,r1
	mov.l	r4,@(12,r1)
	mov	r14,r1
	add	#-12,r1
	mov	#53,r2
	mov.l	r2,@(20,r1)
	mov	r14,r1
	add	#44,r1
	mova	.L542,r0
	fmov.s	@r0+,fr3
	fmov.s	@r0+,fr2
	add	#4,r1
	fmov.s	fr2,@r1
	fmov.s	fr3,@-r1
	mov	r14,r1
	add	#-12,r1
	mov	#32,r2
	mov.l	r2,@(24,r1)
	mov	r14,r1
	add	#-12,r1
	mov	#2,r2
	mov.l	r2,@(28,r1)
	mov	r14,r1
	add	#28,r1
	fmov.s	@r0+,fr3
	fmov.s	@r0+,fr2
	add	#4,r1
	fmov.s	fr2,@r1
	fmov.s	fr3,@-r1
	mov	r14,r1
	add	#36,r1
	fmov.s	@r0+,fr3
	fmov.s	@r0+,fr2
	add	#4,r1
	fmov.s	fr2,@r1
	fmov.s	fr3,@-r1
	mov	r14,r1
	add	#-12,r1
	mov	#2,r2
	mov.l	r2,@(16,r1)
.L534:
	mov	r14,r1
	add	#-12,r1
	mov.l	@(16,r1),r1
	tst	r1,r1
	bt	.L532
	mov	r14,r1
	add	#-12,r1
	mov.l	@(12,r1),r4
	mov.l	.L539,r1
	jsr	@r1
	nop
	mov	r0,r8
	mov.l	.L540,r1
	jsr	@r1
	nop
	mov	r0,r1
	mov	r8,r2
	sub	r1,r2
	mov	r2,r1
	lds	r1,fpul
	float	fpul,dr2
	cmp/pz	r1
	bt	.L533
	mova	.L542,r0
	fmov.s	@r0+,fr5
	fmov.s	@r0+,fr4
	fadd	dr4,dr2
.L533:
	mov	r14,r1
	add	#36,r1
	fmov.s	@r1+,fr5
	fmov.s	@r1,fr4
	add	#-4,r1
	fmul	dr4,dr2
	mov	r14,r1
	add	#28,r1
	mov	r14,r2
	add	#28,r2
	fmov.s	@r2+,fr5
	fmov.s	@r2,fr4
	add	#-4,r2
	fadd	dr4,dr2
	add	#4,r1
	fmov.s	fr2,@r1
	fmov.s	fr3,@-r1
	mov	r14,r1
	add	#36,r1
	mov	r14,r2
	add	#36,r2
	fmov.s	@r2+,fr5
	fmov.s	@r2,fr4
	add	#-4,r2
	mova	.L542,r0
	fmov.s	@r0+,fr3
	fmov.s	@r0+,fr2
	fmul	dr4,dr2
	add	#4,r1
	fmov.s	fr2,@r1
	fmov.s	fr3,@-r1
	mov	r14,r1
	add	#-12,r1
	mov	r14,r2
	add	#-12,r2
	mov.l	@(16,r2),r2
	add	#-1,r2
	mov.l	r2,@(16,r1)
	bra	.L534
	nop
	.align 1
.L532:
	mov	r14,r1
	add	#20,r1
	mov	r14,r3
	add	#28,r3
	mov	r14,r2
	add	#36,r2
	fmov.s	@r3+,fr5
	fmov.s	@r3,fr4
	add	#-4,r3
	fmov.s	@r2+,fr3
	fmov.s	@r2,fr2
	add	#-4,r2
	fmov	fr4,fr6
	fmov	fr5,fr7
	fdiv	dr2,dr6
	fmov	fr6,fr2
	fmov	fr7,fr3
	add	#4,r1
	fmov.s	fr2,@r1
	fmov.s	fr3,@-r1
	mov	r14,r1
	add	#20,r1
	fmov.s	@r1+,fr5
	fmov.s	@r1,fr4
	add	#-4,r1
	mova	.L543,r0
	fmov.s	@r0+,fr3
	fmov.s	@r0+,fr2
	fcmp/eq	dr2,dr4
	bt	.L535
	fcmp/gt	dr2,dr4
.L535:
	movt	r1
	extu.b	r1,r1
	tst	r1,r1
	bt	.L536
	mov	r14,r1
	add	#20,r1
	mova	.L544,r0
	fmov.s	@r0+,fr3
	fmov.s	@r0+,fr2
	add	#4,r1
	fmov.s	fr2,@r1
	fmov.s	fr3,@-r1
.L536:
	mov	r14,r1
	add	#20,r1
	fmov.s	@r1+,fr3
	fmov.s	@r1,fr2
	add	#-4,r1
	fmov	fr2,fr0
	fmov	fr3,fr1
	add	#52,r14
	.cfi_def_cfa_offset 12
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 8
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r8
	.cfi_restore 8
	.cfi_def_cfa_offset 0
	rts	
	nop
.L545:
	.align 2
.L542:
	.long	0
	.long	1106247680
	.long	0
	.long	0
.L543:
	.long	0
	.long	1072693248
.L539:
	.long	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEclEv
.L540:
	.long	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE3minEv
.L544:
	.long	4294967295
	.long	1072693247
	.cfi_endproc
.LFE4566:
	.size	_ZSt18generate_canonicalIdLj53ESt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEET_RT1_, .-_ZSt18generate_canonicalIdLj53ESt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEET_RT1_
	.section	.text._ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 1
	.weak	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev
	.type	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev:
.LFB4576:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	add	#-4,r15
	.cfi_def_cfa_offset 8
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-60,r1
	mov.l	r4,@(60,r1)
	mov.l	.L547,r2
	mov	r14,r1
	add	#-60,r1
	mov.l	@(60,r1),r1
	mov.l	r2,@r1
	nop
	add	#4,r14
	.cfi_def_cfa_offset 4
	mov	r14,r15
	.cfi_def_cfa_register 15
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
.L548:
	.align 2
.L547:
	.long	_ZTVSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE+8
	.cfi_endproc
.LFE4576:
	.size	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED1Ev
	.set	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED1Ev,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED0Ev,"axG",@progbits,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 1
	.weak	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED0Ev
	.type	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED0Ev, @function
_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED0Ev:
.LFB4578:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 17, -8
	add	#-4,r15
	.cfi_def_cfa_offset 12
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-60,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-60,r1
	mov.l	@(60,r1),r4
	mov.l	.L550,r1
	jsr	@r1
	nop
	mov	r14,r1
	add	#-60,r1
	mov	#12,r5
	mov.l	@(60,r1),r4
	mov.l	.L551,r1
	jsr	@r1
	nop
	add	#4,r14
	.cfi_def_cfa_offset 8
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
.L552:
	.align 2
.L550:
	.long	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED1Ev
.L551:
	.long	_ZdlPvj
	.cfi_endproc
.LFE4578:
	.size	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED0Ev, .-_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED0Ev
	.section	.text._ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED2Ev,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED5Ev,comdat
	.align 1
	.weak	_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED2Ev
	.type	_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED2Ev, @function
_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED2Ev:
.LFB4580:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	add	#-4,r15
	.cfi_def_cfa_offset 8
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-60,r1
	mov.l	r4,@(60,r1)
	nop
	add	#4,r14
	.cfi_def_cfa_offset 4
	mov	r14,r15
	.cfi_def_cfa_register 15
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
	.cfi_endproc
.LFE4580:
	.size	_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED2Ev, .-_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED2Ev
	.weak	_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED1Ev
	.set	_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED1Ev,_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED2Ev
	.section	.text._ZNSt16allocator_traitsISaISt10shared_ptrI8hittableEEE10deallocateERS3_PS2_j,"axG",@progbits,_ZNSt16allocator_traitsISaISt10shared_ptrI8hittableEEE10deallocateERS3_PS2_j,comdat
	.align 1
	.weak	_ZNSt16allocator_traitsISaISt10shared_ptrI8hittableEEE10deallocateERS3_PS2_j
	.type	_ZNSt16allocator_traitsISaISt10shared_ptrI8hittableEEE10deallocateERS3_PS2_j, @function
_ZNSt16allocator_traitsISaISt10shared_ptrI8hittableEEE10deallocateERS3_PS2_j:
.LFB4582:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 17, -8
	add	#-12,r15
	.cfi_def_cfa_offset 20
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-52,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-52,r1
	mov.l	r5,@(56,r1)
	mov	r14,r1
	add	#-52,r1
	mov.l	r6,@(52,r1)
	mov	r14,r3
	add	#-52,r3
	mov	r14,r2
	add	#-52,r2
	mov	r14,r1
	add	#-52,r1
	mov.l	@(52,r3),r6
	mov.l	@(56,r2),r5
	mov.l	@(60,r1),r4
	mov.l	.L555,r1
	jsr	@r1
	nop
	nop
	add	#12,r14
	.cfi_def_cfa_offset 8
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
.L556:
	.align 2
.L555:
	.long	_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEE10deallocateEPS3_j
	.cfi_endproc
.LFE4582:
	.size	_ZNSt16allocator_traitsISaISt10shared_ptrI8hittableEEE10deallocateERS3_PS2_j, .-_ZNSt16allocator_traitsISaISt10shared_ptrI8hittableEEE10deallocateERS3_PS2_j
	.section	.text._ZNSt12_Destroy_auxILb0EE9__destroyIPSt10shared_ptrI8hittableEEEvT_S6_,"axG",@progbits,_ZNSt12_Destroy_auxILb0EE9__destroyIPSt10shared_ptrI8hittableEEEvT_S6_,comdat
	.align 1
	.weak	_ZNSt12_Destroy_auxILb0EE9__destroyIPSt10shared_ptrI8hittableEEEvT_S6_
	.type	_ZNSt12_Destroy_auxILb0EE9__destroyIPSt10shared_ptrI8hittableEEEvT_S6_, @function
_ZNSt12_Destroy_auxILb0EE9__destroyIPSt10shared_ptrI8hittableEEEvT_S6_:
.LFB4583:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 17, -8
	add	#-8,r15
	.cfi_def_cfa_offset 16
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-56,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-56,r1
	mov.l	r5,@(56,r1)
.L559:
	mov	r14,r2
	add	#-56,r2
	mov	r14,r1
	add	#-56,r1
	mov.l	@(60,r2),r2
	mov.l	@(56,r1),r1
	cmp/eq	r1,r2
	bt	.L560
	mov	r14,r1
	add	#-56,r1
	mov.l	@(60,r1),r4
	mov.l	.L561,r1
	jsr	@r1
	nop
	mov	r0,r1
	mov	r1,r4
	mov.l	.L562,r1
	jsr	@r1
	nop
	mov	r14,r1
	add	#-56,r1
	mov	r14,r2
	add	#-56,r2
	mov.l	@(60,r2),r2
	add	#8,r2
	mov.l	r2,@(60,r1)
	bra	.L559
	nop
	.align 1
.L560:
	nop
	add	#8,r14
	.cfi_def_cfa_offset 8
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
.L563:
	.align 2
.L561:
	.long	_ZSt11__addressofISt10shared_ptrI8hittableEEPT_RS3_
.L562:
	.long	_ZSt8_DestroyISt10shared_ptrI8hittableEEvPT_
	.cfi_endproc
.LFE4583:
	.size	_ZNSt12_Destroy_auxILb0EE9__destroyIPSt10shared_ptrI8hittableEEEvT_S6_, .-_ZNSt12_Destroy_auxILb0EE9__destroyIPSt10shared_ptrI8hittableEEEvT_S6_
	.section	.text._ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,"axG",@progbits,_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC5ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,comdat
	.align 1
	.weak	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.type	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_, @function
_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_:
.LFB4585:
	.cfi_startproc
	mov.l	r8,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 8, -4
	mov.l	r9,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 9, -8
	mov.l	r10,@-r15
	.cfi_def_cfa_offset 12
	.cfi_offset 10, -12
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 16
	.cfi_offset 14, -16
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 20
	.cfi_offset 17, -20
	add	#-20,r15
	.cfi_def_cfa_offset 40
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-44,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-44,r1
	mov.l	r5,@(56,r1)
	mov	r14,r1
	add	#-44,r1
	mov.l	r6,@(52,r1)
	mov	r14,r1
	add	#-44,r1
	mov.l	r7,@(48,r1)
	mov	r14,r1
	add	#-44,r1
	mov.l	@(60,r1),r1
	mov	#0,r2
	mov.l	r2,@r1
	mov	r14,r1
	add	#-44,r1
	mov.l	@(60,r1),r1
	mov	r1,r9
	add	#4,r9
	mov	r14,r1
	add	#-44,r1
	mov.l	@(60,r1),r10
	mov	r14,r1
	add	#-44,r1
	mov.l	@(52,r1),r4
	mov.l	.L565,r1
	jsr	@r1
	nop
	mov	r0,r8
	mov	r14,r1
	add	#-44,r1
	mov.l	@(48,r1),r4
	mov.l	.L566,r1
	jsr	@r1
	nop
	mov	r0,r1
	mov	r14,r2
	add	#-44,r2
	mov.l	r1,@r15
	mov	r8,r7
	mov.l	@(56,r2),r6
	mov	r10,r5
	mov	r9,r4
	mov.l	.L567,r1
	jsr	@r1
	nop
	mov	r14,r1
	add	#-44,r1
	mov.l	@(60,r1),r1
	mov.l	@r1,r2
	mov	r14,r1
	add	#-44,r1
	mov	r2,r5
	mov.l	@(60,r1),r4
	mov.l	.L568,r1
	jsr	@r1
	nop
	nop
	add	#20,r14
	.cfi_def_cfa_offset 20
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 16
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 12
	mov.l	@r15+,r10
	.cfi_restore 10
	.cfi_def_cfa_offset 8
	mov.l	@r15+,r9
	.cfi_restore 9
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r8
	.cfi_restore 8
	.cfi_def_cfa_offset 0
	rts	
	nop
.L569:
	.align 2
.L565:
	.long	_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
.L566:
	.long	_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE
.L567:
	.long	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_
.L568:
	.long	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EE31_M_enable_shared_from_this_withIS0_S0_EENSt9enable_ifIXntsrNS3_15__has_esft_baseIT0_vEE5valueEvE4typeEPT_
	.cfi_endproc
.LFE4585:
	.size	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_, .-_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.weak	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC1ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.set	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC1ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.section	.text._ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,"axG",@progbits,_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC5ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,comdat
	.align 1
	.weak	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.type	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_, @function
_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_:
.LFB4588:
	.cfi_startproc
	mov.l	r8,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 8, -4
	mov.l	r9,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 9, -8
	mov.l	r10,@-r15
	.cfi_def_cfa_offset 12
	.cfi_offset 10, -12
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 16
	.cfi_offset 14, -16
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 20
	.cfi_offset 17, -20
	add	#-20,r15
	.cfi_def_cfa_offset 40
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-44,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-44,r1
	mov.l	r5,@(56,r1)
	mov	r14,r1
	add	#-44,r1
	mov.l	r6,@(52,r1)
	mov	r14,r1
	add	#-44,r1
	mov.l	r7,@(48,r1)
	mov	r14,r1
	add	#-44,r1
	mov.l	@(60,r1),r1
	mov	#0,r2
	mov.l	r2,@r1
	mov	r14,r1
	add	#-44,r1
	mov.l	@(60,r1),r1
	mov	r1,r9
	add	#4,r9
	mov	r14,r1
	add	#-44,r1
	mov.l	@(60,r1),r10
	mov	r14,r1
	add	#-44,r1
	mov.l	@(52,r1),r4
	mov.l	.L571,r1
	jsr	@r1
	nop
	mov	r0,r8
	mov	r14,r1
	add	#-44,r1
	mov.l	@(48,r1),r4
	mov.l	.L572,r1
	jsr	@r1
	nop
	mov	r0,r1
	mov	r14,r2
	add	#-44,r2
	mov.l	r1,@r15
	mov	r8,r7
	mov.l	@(56,r2),r6
	mov	r10,r5
	mov	r9,r4
	mov.l	.L573,r1
	jsr	@r1
	nop
	mov	r14,r1
	add	#-44,r1
	mov.l	@(60,r1),r1
	mov.l	@r1,r2
	mov	r14,r1
	add	#-44,r1
	mov	r2,r5
	mov.l	@(60,r1),r4
	mov.l	.L574,r1
	jsr	@r1
	nop
	nop
	add	#20,r14
	.cfi_def_cfa_offset 20
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 16
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 12
	mov.l	@r15+,r10
	.cfi_restore 10
	.cfi_def_cfa_offset 8
	mov.l	@r15+,r9
	.cfi_restore 9
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r8
	.cfi_restore 8
	.cfi_def_cfa_offset 0
	rts	
	nop
.L575:
	.align 2
.L571:
	.long	_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
.L572:
	.long	_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE
.L573:
	.long	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_
.L574:
	.long	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EE31_M_enable_shared_from_this_withIS0_S0_EENSt9enable_ifIXntsrNS3_15__has_esft_baseIT0_vEE5valueEvE4typeEPT_
	.cfi_endproc
.LFE4588:
	.size	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_, .-_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.weak	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC1ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.set	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC1ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.section	.text._ZNSt8__detail4_ModIjLj0ELj1ELj0ELb1ELb0EE6__calcEj,"axG",@progbits,_ZNSt8__detail4_ModIjLj0ELj1ELj0ELb1ELb0EE6__calcEj,comdat
	.align 1
	.weak	_ZNSt8__detail4_ModIjLj0ELj1ELj0ELb1ELb0EE6__calcEj
	.type	_ZNSt8__detail4_ModIjLj0ELj1ELj0ELb1ELb0EE6__calcEj, @function
_ZNSt8__detail4_ModIjLj0ELj1ELj0ELb1ELb0EE6__calcEj:
.LFB4618:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	add	#-8,r15
	.cfi_def_cfa_offset 12
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-56,r1
	mov.l	r4,@(56,r1)
	mov	r14,r1
	add	#-56,r1
	mov	r14,r2
	add	#-56,r2
	mov.l	@(56,r2),r2
	mov.l	r2,@(60,r1)
	mov	r14,r1
	add	#-56,r1
	mov.l	@(60,r1),r1
	mov	r1,r0
	add	#8,r14
	.cfi_def_cfa_offset 4
	mov	r14,r15
	.cfi_def_cfa_register 15
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
	.cfi_endproc
.LFE4618:
	.size	_ZNSt8__detail4_ModIjLj0ELj1ELj0ELb1ELb0EE6__calcEj, .-_ZNSt8__detail4_ModIjLj0ELj1ELj0ELb1ELb0EE6__calcEj
	.section	.text._ZNSt8__detail4_ModIjLj624ELj1ELj0ELb1ELb1EE6__calcEj,"axG",@progbits,_ZNSt8__detail4_ModIjLj624ELj1ELj0ELb1ELb1EE6__calcEj,comdat
	.align 1
	.weak	_ZNSt8__detail4_ModIjLj624ELj1ELj0ELb1ELb1EE6__calcEj
	.type	_ZNSt8__detail4_ModIjLj624ELj1ELj0ELb1ELb1EE6__calcEj, @function
_ZNSt8__detail4_ModIjLj624ELj1ELj0ELb1ELb1EE6__calcEj:
.LFB4619:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	add	#-8,r15
	.cfi_def_cfa_offset 12
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-56,r1
	mov.l	r4,@(56,r1)
	mov	r14,r1
	add	#-56,r1
	mov	r14,r2
	add	#-56,r2
	mov.l	@(56,r2),r2
	mov.l	r2,@(60,r1)
	mov	r14,r2
	add	#-56,r2
	mov	r14,r1
	add	#-56,r1
	mov.l	@(60,r1),r1
	mov	r1,r7
	shlr2	r7
	shlr2	r7
	mov.l	.L580,r3
	dmulu.l	r3,r7
	sts	mach,r3
	nop
	mov	r3,r7
	shlr2	r7
	mov.w	.L581,r3
	mul.l	r3,r7
	sts	macl,r3
	sub	r3,r1
	mov.l	r1,@(60,r2)
	mov	r14,r1
	add	#-56,r1
	mov.l	@(60,r1),r1
	mov	r1,r0
	add	#8,r14
	.cfi_def_cfa_offset 4
	mov	r14,r15
	.cfi_def_cfa_register 15
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
	.align 1
.L581:
	.short	624
.L582:
	.align 2
.L580:
	.long	440509467
	.cfi_endproc
.LFE4619:
	.size	_ZNSt8__detail4_ModIjLj624ELj1ELj0ELb1ELb1EE6__calcEj, .-_ZNSt8__detail4_ModIjLj624ELj1ELj0ELb1ELb1EE6__calcEj
	.section	.text._ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEclEv,"axG",@progbits,_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEclEv,comdat
	.align 1
	.weak	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEclEv
	.type	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEclEv, @function
_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEclEv:
.LFB4620:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 17, -8
	add	#-8,r15
	.cfi_def_cfa_offset 16
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-56,r1
	mov.l	r4,@(56,r1)
	mov	r14,r1
	add	#-56,r1
	mov.l	@(56,r1),r1
	mov.w	.L590,r2
	add	r2,r1
	mov.l	@(4,r1),r2
	mov.w	.L587,r1
	cmp/hi	r1,r2
	bf	.L584
	mov	r14,r1
	add	#-56,r1
	mov.l	@(56,r1),r4
	mov.l	.L588,r1
	jsr	@r1
	nop
.L584:
	mov	r14,r1
	add	#-56,r1
	mov.l	@(56,r1),r1
	mov.w	.L590,r3
	add	r3,r1
	mov.l	@(4,r1),r1
	mov	r1,r3
	add	#1,r3
	mov	r14,r2
	add	#-56,r2
	mov.l	@(56,r2),r2
	mov.w	.L590,r7
	add	r7,r2
	mov.l	r3,@(4,r2)
	mov	r14,r2
	add	#-56,r2
	mov	r14,r3
	add	#-56,r3
	mov.l	@(56,r3),r3
	shll2	r1
	add	r3,r1
	mov.l	@r1,r1
	mov.l	r1,@(60,r2)
	mov	r14,r1
	add	#-56,r1
	mov.l	@(60,r1),r1
	mov	r1,r2
	mov	#-11,r3
	shld	r3,r2
	mov	r14,r1
	add	#-56,r1
	mov	r14,r3
	add	#-56,r3
	mov.l	@(60,r3),r3
	xor	r3,r2
	mov.l	r2,@(60,r1)
	mov	r14,r1
	add	#-56,r1
	mov.l	@(60,r1),r2
	mov	#7,r1
	shld	r1,r2
	mov.l	.L591,r1
	and	r1,r2
	mov	r14,r1
	add	#-56,r1
	mov	r14,r3
	add	#-56,r3
	mov.l	@(60,r3),r3
	xor	r3,r2
	mov.l	r2,@(60,r1)
	mov	r14,r1
	add	#-56,r1
	mov.l	@(60,r1),r1
	mov	r1,r2
	mov	#15,r7
	shld	r7,r2
	mov.l	.L592,r1
	and	r1,r2
	mov	r14,r1
	add	#-56,r1
	mov	r14,r3
	add	#-56,r3
	mov.l	@(60,r3),r3
	xor	r3,r2
	mov.l	r2,@(60,r1)
	mov	r14,r1
	add	#-56,r1
	mov.l	@(60,r1),r1
	mov	r1,r2
	shlr16	r2
	shlr2	r2
	mov	r14,r1
	add	#-56,r1
	mov	r14,r3
	add	#-56,r3
	mov.l	@(60,r3),r3
	xor	r3,r2
	mov.l	r2,@(60,r1)
	mov	r14,r1
	add	#-56,r1
	mov.l	@(60,r1),r1
	mov	r1,r0
	add	#8,r14
	.cfi_def_cfa_offset 8
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
	.align 1
.L590:
	.short	2492
.L587:
	.short	623
.L593:
	.align 2
.L588:
	.long	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE11_M_gen_randEv
.L591:
	.long	-1658038656
.L592:
	.long	-272236544
	.cfi_endproc
.LFE4620:
	.size	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEclEv, .-_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEclEv
	.section	.text._ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEE10deallocateEPS3_j,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEE10deallocateEPS3_j,comdat
	.align 1
	.weak	_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEE10deallocateEPS3_j
	.type	_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEE10deallocateEPS3_j, @function
_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEE10deallocateEPS3_j:
.LFB4622:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 17, -8
	add	#-12,r15
	.cfi_def_cfa_offset 20
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-52,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-52,r1
	mov.l	r5,@(56,r1)
	mov	r14,r1
	add	#-52,r1
	mov.l	r6,@(52,r1)
	mov	r14,r1
	add	#-52,r1
	mov.l	@(56,r1),r4
	mov.l	.L595,r1
	jsr	@r1
	nop
	nop
	add	#12,r14
	.cfi_def_cfa_offset 8
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
.L596:
	.align 2
.L595:
	.long	_ZdlPv
	.cfi_endproc
.LFE4622:
	.size	_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEE10deallocateEPS3_j, .-_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEE10deallocateEPS3_j
	.section	.text._ZSt11__addressofISt10shared_ptrI8hittableEEPT_RS3_,"axG",@progbits,_ZSt11__addressofISt10shared_ptrI8hittableEEPT_RS3_,comdat
	.align 1
	.weak	_ZSt11__addressofISt10shared_ptrI8hittableEEPT_RS3_
	.type	_ZSt11__addressofISt10shared_ptrI8hittableEEPT_RS3_, @function
_ZSt11__addressofISt10shared_ptrI8hittableEEPT_RS3_:
.LFB4623:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	add	#-4,r15
	.cfi_def_cfa_offset 8
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-60,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-60,r1
	mov.l	@(60,r1),r1
	mov	r1,r0
	add	#4,r14
	.cfi_def_cfa_offset 4
	mov	r14,r15
	.cfi_def_cfa_register 15
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
	.cfi_endproc
.LFE4623:
	.size	_ZSt11__addressofISt10shared_ptrI8hittableEEPT_RS3_, .-_ZSt11__addressofISt10shared_ptrI8hittableEEPT_RS3_
	.section	.text._ZSt8_DestroyISt10shared_ptrI8hittableEEvPT_,"axG",@progbits,_ZSt8_DestroyISt10shared_ptrI8hittableEEvPT_,comdat
	.align 1
	.weak	_ZSt8_DestroyISt10shared_ptrI8hittableEEvPT_
	.type	_ZSt8_DestroyISt10shared_ptrI8hittableEEvPT_, @function
_ZSt8_DestroyISt10shared_ptrI8hittableEEvPT_:
.LFB4624:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 17, -8
	add	#-4,r15
	.cfi_def_cfa_offset 12
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-60,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-60,r1
	mov.l	@(60,r1),r4
	mov.l	.L600,r1
	jsr	@r1
	nop
	nop
	add	#4,r14
	.cfi_def_cfa_offset 8
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
.L601:
	.align 2
.L600:
	.long	_ZNSt10shared_ptrI8hittableED1Ev
	.cfi_endproc
.LFE4624:
	.size	_ZSt8_DestroyISt10shared_ptrI8hittableEEvPT_, .-_ZSt8_DestroyISt10shared_ptrI8hittableEEvPT_
	.section	.text._ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_,"axG",@progbits,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC5I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_,comdat
	.align 1
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_
	.type	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_, @function
_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_:
.LFB4626:
	.cfi_startproc
	.cfi_personality 0,__gxx_personality_v0
	.cfi_lsda 0xb,.LLSDA4626
	mov.l	r8,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 8, -4
	mov.l	r9,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 9, -8
	mov.l	r10,@-r15
	.cfi_def_cfa_offset 12
	.cfi_offset 10, -12
	mov.l	r11,@-r15
	.cfi_def_cfa_offset 16
	.cfi_offset 11, -16
	mov.l	r12,@-r15
	.cfi_def_cfa_offset 20
	.cfi_offset 12, -20
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 24
	.cfi_offset 14, -24
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 28
	.cfi_offset 17, -28
	add	#-44,r15
	.cfi_def_cfa_offset 72
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-20,r1
	mov.l	r4,@(36,r1)
	mov	r14,r1
	add	#-20,r1
	mov.l	r5,@(32,r1)
	mov	r14,r1
	add	#-20,r1
	mov.l	r6,@(28,r1)
	mov	r14,r1
	add	#-20,r1
	mov.l	r7,@(24,r1)
	mov	r14,r1
	add	#-20,r1
	add	#20,r1
	mov	r14,r3
	add	#64,r3
	mov.l	@(8,r3),r2
	mov.l	r2,@r1
	mov	r14,r1
	add	#-20,r1
	mov.l	.L624,r2
	mov.l	@r2,r7
	mov.l	r7,@(60,r1)
	mov	#0,r7
	mov	r14,r1
	add	#-20,r1
	mov.l	@(28,r1),r2
	mov	r14,r1
	add	#22,r1
	mov	r2,r5
	mov	r1,r4
	mov.l	.L611,r1
	jsr	@r1
	nop
	mov	r14,r2
	add	#32,r2
	mov	r14,r1
	add	#22,r1
	mov	r1,r4
	mov.l	.L612,r1
.LEHB20:
	jsr	@r1
	nop
.LEHE20:
	mov	r14,r8
	add	#-20,r8
	mov	r14,r1
	add	#32,r1
	mov	r1,r4
	mov.l	.L613,r1
	jsr	@r1
	nop
	mov	r0,r1
	mov.l	r1,@(44,r8)
	mov	r14,r1
	add	#-20,r1
	mov.l	@(28,r1),r2
	mov	r14,r1
	add	#23,r1
	mov	r2,r5
	mov	r1,r4
	mov.l	.L614,r1
	jsr	@r1
	nop
	mov	r14,r12
	add	#23,r12
	mov	r14,r1
	add	#-20,r1
	mov.l	@(24,r1),r4
	mov.l	.L615,r1
	jsr	@r1
	nop
	mov	r0,r10
	mov	r14,r1
	add	#-20,r1
	mov.l	@(20,r1),r4
	mov.l	.L616,r1
	jsr	@r1
	nop
	mov	r0,r11
	mov	r14,r1
	add	#-20,r1
	mov.l	@(44,r1),r9
	mov	r9,r5
	mov	#56,r4
	mov.l	.L617,r1
	jsr	@r1
	nop
	mov	r0,r8
	mov	r11,r7
	mov	r10,r6
	mov	r12,r5
	mov	r8,r4
	mov.l	.L618,r1
.LEHB21:
	jsr	@r1
	nop
.LEHE21:
	mov	r14,r1
	add	#-20,r1
	mov.l	r8,@(48,r1)
	mov	r14,r1
	add	#23,r1
	mov	r1,r4
	mov.l	.L627,r1
	jsr	@r1
	nop
	mov	r14,r1
	add	#32,r1
	mov	#0,r5
	mov	r1,r4
	mov.l	.L620,r1
	jsr	@r1
	nop
	mov	r14,r1
	add	#-20,r1
	mov.l	@(36,r1),r1
	mov	r14,r2
	add	#-20,r2
	mov.l	@(48,r2),r2
	mov.l	r2,@r1
	mov	r14,r1
	add	#-20,r1
	mov.l	@(48,r1),r4
	mov.l	.L621,r1
	jsr	@r1
	nop
	mov	r0,r1
	mov	r14,r2
	add	#-20,r2
	mov.l	@(32,r2),r2
	mov.l	r1,@r2
	mov	r14,r1
	add	#32,r1
	mov	r1,r4
	mov.l	.L628,r1
	jsr	@r1
	nop
	mov	r14,r1
	add	#22,r1
	mov	r1,r4
	mov.l	.L630,r1
	jsr	@r1
	nop
	nop
	mov	r14,r1
	add	#-20,r1
	mov.l	.L624,r2
	mov.l	@(60,r1),r3
	mov.l	@r2,r7
	cmp/eq	r3,r7
	mov	#0,r3
	mov	#0,r7
	bt	.L609
	bra	.L608
	nop
	.align 1
.L607:
	mov	r4,r10
	mov	r9,r5
	mov	r8,r4
	mov.l	.L625,r2
	sts	fpscr,r1
	mov.l	.L629,r3
	or	r3,r1
	lds	r1,fpscr
	jsr	@r2
	nop
	mov	r10,r8
	mov	r14,r1
	add	#23,r1
	mov	r1,r4
	mov.l	.L627,r1
	jsr	@r1
	nop
	mov	r14,r1
	add	#32,r1
	mov	r1,r4
	mov.l	.L628,r1
	jsr	@r1
	nop
	bra	.L604
	nop
	.align 1
.L606:
	mov	r4,r8
	sts	fpscr,r1
	mov.l	.L629,r2
	or	r2,r1
	lds	r1,fpscr
.L604:
	mov	r14,r1
	add	#22,r1
	mov	r1,r4
	mov.l	.L630,r1
	jsr	@r1
	nop
	mov	r8,r1
	mov	r1,r4
	mov.l	.L631,r1
.LEHB22:
	jsr	@r1
	nop
.LEHE22:
	.align 1
.L608:
	mov.l	.L632,r1
	jsr	@r1
	nop
	.align 1
.L609:
	add	#44,r14
	.cfi_def_cfa_offset 28
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 24
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 20
	mov.l	@r15+,r12
	.cfi_restore 12
	.cfi_def_cfa_offset 16
	mov.l	@r15+,r11
	.cfi_restore 11
	.cfi_def_cfa_offset 12
	mov.l	@r15+,r10
	.cfi_restore 10
	.cfi_def_cfa_offset 8
	mov.l	@r15+,r9
	.cfi_restore 9
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r8
	.cfi_restore 8
	.cfi_def_cfa_offset 0
	rts	
	nop
.L633:
	.align 2
.L624:
	.long	__stack_chk_guard
.L611:
	.long	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC1IS0_EERKSaIT_E
.L612:
	.long	_ZSt18__allocate_guardedISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEESt15__allocated_ptrIT_ERS8_
.L613:
	.long	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE3getEv
.L614:
	.long	_ZNSaI6sphereEC1ERKS0_
.L615:
	.long	_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
.L616:
	.long	_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE
.L617:
	.long	_ZnwjPv
.L618:
	.long	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC1IJ4vec3dEEES1_DpOT_
.L627:
	.long	_ZNSaI6sphereED1Ev
.L620:
	.long	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEaSEDn
.L621:
	.long	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv
.L628:
	.long	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED1Ev
.L630:
	.long	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED1Ev
.L625:
	.long	_ZdlPvS_
.L629:
	.long	524288
.L631:
	.long	_Unwind_Resume
.L632:
	.long	__stack_chk_fail
	.cfi_endproc
.LFE4626:
	.section	.gcc_except_table
.LLSDA4626:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4626-.LLSDACSB4626
.LLSDACSB4626:
	.uleb128 .LEHB20-.LFB4626
	.uleb128 .LEHE20-.LEHB20
	.uleb128 .L606-.LFB4626
	.uleb128 0
	.uleb128 .LEHB21-.LFB4626
	.uleb128 .LEHE21-.LEHB21
	.uleb128 .L607-.LFB4626
	.uleb128 0
	.uleb128 .LEHB22-.LFB4626
	.uleb128 .LEHE22-.LEHB22
	.uleb128 0
	.uleb128 0
.LLSDACSE4626:
	.section	.text._ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_,"axG",@progbits,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC5I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_,comdat
	.size	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_, .-_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_
	.set	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_
	.section	.text._ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EE31_M_enable_shared_from_this_withIS0_S0_EENSt9enable_ifIXntsrNS3_15__has_esft_baseIT0_vEE5valueEvE4typeEPT_,"axG",@progbits,_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EE31_M_enable_shared_from_this_withIS0_S0_EENSt9enable_ifIXntsrNS3_15__has_esft_baseIT0_vEE5valueEvE4typeEPT_,comdat
	.align 1
	.weak	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EE31_M_enable_shared_from_this_withIS0_S0_EENSt9enable_ifIXntsrNS3_15__has_esft_baseIT0_vEE5valueEvE4typeEPT_
	.type	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EE31_M_enable_shared_from_this_withIS0_S0_EENSt9enable_ifIXntsrNS3_15__has_esft_baseIT0_vEE5valueEvE4typeEPT_, @function
_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EE31_M_enable_shared_from_this_withIS0_S0_EENSt9enable_ifIXntsrNS3_15__has_esft_baseIT0_vEE5valueEvE4typeEPT_:
.LFB4628:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	add	#-8,r15
	.cfi_def_cfa_offset 12
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-56,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-56,r1
	mov.l	r5,@(56,r1)
	nop
	add	#8,r14
	.cfi_def_cfa_offset 4
	mov	r14,r15
	.cfi_def_cfa_register 15
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
	.cfi_endproc
.LFE4628:
	.size	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EE31_M_enable_shared_from_this_withIS0_S0_EENSt9enable_ifIXntsrNS3_15__has_esft_baseIT0_vEE5valueEvE4typeEPT_, .-_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EE31_M_enable_shared_from_this_withIS0_S0_EENSt9enable_ifIXntsrNS3_15__has_esft_baseIT0_vEE5valueEvE4typeEPT_
	.section	.text._ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_,"axG",@progbits,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC5I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_,comdat
	.align 1
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_
	.type	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_, @function
_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_:
.LFB4630:
	.cfi_startproc
	.cfi_personality 0,__gxx_personality_v0
	.cfi_lsda 0xb,.LLSDA4630
	mov.l	r8,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 8, -4
	mov.l	r9,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 9, -8
	mov.l	r10,@-r15
	.cfi_def_cfa_offset 12
	.cfi_offset 10, -12
	mov.l	r11,@-r15
	.cfi_def_cfa_offset 16
	.cfi_offset 11, -16
	mov.l	r12,@-r15
	.cfi_def_cfa_offset 20
	.cfi_offset 12, -20
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 24
	.cfi_offset 14, -24
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 28
	.cfi_offset 17, -28
	add	#-44,r15
	.cfi_def_cfa_offset 72
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-20,r1
	mov.l	r4,@(36,r1)
	mov	r14,r1
	add	#-20,r1
	mov.l	r5,@(32,r1)
	mov	r14,r1
	add	#-20,r1
	mov.l	r6,@(28,r1)
	mov	r14,r1
	add	#-20,r1
	mov.l	r7,@(24,r1)
	mov	r14,r1
	add	#-20,r1
	add	#20,r1
	mov	r14,r3
	add	#64,r3
	mov.l	@(8,r3),r2
	mov.l	r2,@r1
	mov	r14,r1
	add	#-20,r1
	mov.l	.L657,r2
	mov.l	@r2,r7
	mov.l	r7,@(60,r1)
	mov	#0,r7
	mov	r14,r1
	add	#-20,r1
	mov.l	@(28,r1),r2
	mov	r14,r1
	add	#22,r1
	mov	r2,r5
	mov	r1,r4
	mov.l	.L644,r1
	jsr	@r1
	nop
	mov	r14,r2
	add	#32,r2
	mov	r14,r1
	add	#22,r1
	mov	r1,r4
	mov.l	.L645,r1
.LEHB23:
	jsr	@r1
	nop
.LEHE23:
	mov	r14,r8
	add	#-20,r8
	mov	r14,r1
	add	#32,r1
	mov	r1,r4
	mov.l	.L646,r1
	jsr	@r1
	nop
	mov	r0,r1
	mov.l	r1,@(44,r8)
	mov	r14,r1
	add	#-20,r1
	mov.l	@(28,r1),r2
	mov	r14,r1
	add	#23,r1
	mov	r2,r5
	mov	r1,r4
	mov.l	.L647,r1
	jsr	@r1
	nop
	mov	r14,r12
	add	#23,r12
	mov	r14,r1
	add	#-20,r1
	mov.l	@(24,r1),r4
	mov.l	.L648,r1
	jsr	@r1
	nop
	mov	r0,r10
	mov	r14,r1
	add	#-20,r1
	mov.l	@(20,r1),r4
	mov.l	.L649,r1
	jsr	@r1
	nop
	mov	r0,r11
	mov	r14,r1
	add	#-20,r1
	mov.l	@(44,r1),r9
	mov	r9,r5
	mov	#56,r4
	mov.l	.L650,r1
	jsr	@r1
	nop
	mov	r0,r8
	mov	r11,r7
	mov	r10,r6
	mov	r12,r5
	mov	r8,r4
	mov.l	.L651,r1
.LEHB24:
	jsr	@r1
	nop
.LEHE24:
	mov	r14,r1
	add	#-20,r1
	mov.l	r8,@(48,r1)
	mov	r14,r1
	add	#23,r1
	mov	r1,r4
	mov.l	.L660,r1
	jsr	@r1
	nop
	mov	r14,r1
	add	#32,r1
	mov	#0,r5
	mov	r1,r4
	mov.l	.L653,r1
	jsr	@r1
	nop
	mov	r14,r1
	add	#-20,r1
	mov.l	@(36,r1),r1
	mov	r14,r2
	add	#-20,r2
	mov.l	@(48,r2),r2
	mov.l	r2,@r1
	mov	r14,r1
	add	#-20,r1
	mov.l	@(48,r1),r4
	mov.l	.L654,r1
	jsr	@r1
	nop
	mov	r0,r1
	mov	r14,r2
	add	#-20,r2
	mov.l	@(32,r2),r2
	mov.l	r1,@r2
	mov	r14,r1
	add	#32,r1
	mov	r1,r4
	mov.l	.L661,r1
	jsr	@r1
	nop
	mov	r14,r1
	add	#22,r1
	mov	r1,r4
	mov.l	.L663,r1
	jsr	@r1
	nop
	nop
	mov	r14,r1
	add	#-20,r1
	mov.l	.L657,r2
	mov.l	@(60,r1),r3
	mov.l	@r2,r7
	cmp/eq	r3,r7
	mov	#0,r3
	mov	#0,r7
	bt	.L642
	bra	.L641
	nop
	.align 1
.L640:
	mov	r4,r10
	mov	r9,r5
	mov	r8,r4
	mov.l	.L658,r2
	sts	fpscr,r1
	mov.l	.L662,r3
	or	r3,r1
	lds	r1,fpscr
	jsr	@r2
	nop
	mov	r10,r8
	mov	r14,r1
	add	#23,r1
	mov	r1,r4
	mov.l	.L660,r1
	jsr	@r1
	nop
	mov	r14,r1
	add	#32,r1
	mov	r1,r4
	mov.l	.L661,r1
	jsr	@r1
	nop
	bra	.L637
	nop
	.align 1
.L639:
	mov	r4,r8
	sts	fpscr,r1
	mov.l	.L662,r2
	or	r2,r1
	lds	r1,fpscr
.L637:
	mov	r14,r1
	add	#22,r1
	mov	r1,r4
	mov.l	.L663,r1
	jsr	@r1
	nop
	mov	r8,r1
	mov	r1,r4
	mov.l	.L664,r1
.LEHB25:
	jsr	@r1
	nop
.LEHE25:
	.align 1
.L641:
	mov.l	.L665,r1
	jsr	@r1
	nop
	.align 1
.L642:
	add	#44,r14
	.cfi_def_cfa_offset 28
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 24
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 20
	mov.l	@r15+,r12
	.cfi_restore 12
	.cfi_def_cfa_offset 16
	mov.l	@r15+,r11
	.cfi_restore 11
	.cfi_def_cfa_offset 12
	mov.l	@r15+,r10
	.cfi_restore 10
	.cfi_def_cfa_offset 8
	mov.l	@r15+,r9
	.cfi_restore 9
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r8
	.cfi_restore 8
	.cfi_def_cfa_offset 0
	rts	
	nop
.L666:
	.align 2
.L657:
	.long	__stack_chk_guard
.L644:
	.long	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC1IS0_EERKSaIT_E
.L645:
	.long	_ZSt18__allocate_guardedISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEESt15__allocated_ptrIT_ERS8_
.L646:
	.long	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE3getEv
.L647:
	.long	_ZNSaI6sphereEC1ERKS0_
.L648:
	.long	_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
.L649:
	.long	_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE
.L650:
	.long	_ZnwjPv
.L651:
	.long	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC1IJ4vec3iEEES1_DpOT_
.L660:
	.long	_ZNSaI6sphereED1Ev
.L653:
	.long	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEaSEDn
.L654:
	.long	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv
.L661:
	.long	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED1Ev
.L663:
	.long	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED1Ev
.L658:
	.long	_ZdlPvS_
.L662:
	.long	524288
.L664:
	.long	_Unwind_Resume
.L665:
	.long	__stack_chk_fail
	.cfi_endproc
.LFE4630:
	.section	.gcc_except_table
.LLSDA4630:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4630-.LLSDACSB4630
.LLSDACSB4630:
	.uleb128 .LEHB23-.LFB4630
	.uleb128 .LEHE23-.LEHB23
	.uleb128 .L639-.LFB4630
	.uleb128 0
	.uleb128 .LEHB24-.LFB4630
	.uleb128 .LEHE24-.LEHB24
	.uleb128 .L640-.LFB4630
	.uleb128 0
	.uleb128 .LEHB25-.LFB4630
	.uleb128 .LEHE25-.LEHB25
	.uleb128 0
	.uleb128 0
.LLSDACSE4630:
	.section	.text._ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_,"axG",@progbits,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC5I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_,comdat
	.size	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_, .-_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_
	.set	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_
	.section	.text._ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE11_M_gen_randEv,"axG",@progbits,_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE11_M_gen_randEv,comdat
	.align 1
	.weak	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE11_M_gen_randEv
	.type	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE11_M_gen_randEv, @function
_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE11_M_gen_randEv:
.LFB4658:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	add	#-32,r15
	.cfi_def_cfa_offset 36
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-32,r1
	mov.l	r4,@(32,r1)
	mov	r14,r1
	add	#-32,r1
	mov.l	.L687,r2
	mov.l	r2,@(44,r1)
	mov	r14,r1
	add	#-32,r1
	mov.l	.L688,r2
	mov.l	r2,@(48,r1)
	mov	r14,r1
	add	#-32,r1
	mov	#0,r2
	mov.l	r2,@(36,r1)
.L671:
	mov	r14,r1
	add	#-32,r1
	mov.l	@(36,r1),r2
	mov.w	.L680,r1
	cmp/hi	r1,r2
	bt	.L668
	mov	r14,r1
	add	#-32,r1
	mov.l	@(32,r1),r2
	mov	r14,r1
	add	#-32,r1
	mov.l	@(36,r1),r1
	shll2	r1
	add	r2,r1
	mov.l	@r1,r2
	mov.l	.L687,r1
	mov	r2,r3
	and	r1,r3
	mov	r14,r1
	add	#-32,r1
	mov.l	@(36,r1),r1
	add	#1,r1
	mov	r14,r2
	add	#-32,r2
	mov.l	@(32,r2),r2
	shll2	r1
	add	r2,r1
	mov.l	@r1,r2
	mov.l	.L688,r1
	and	r1,r2
	mov	r14,r1
	add	#-32,r1
	or	r3,r2
	mov.l	r2,@(52,r1)
	mov	r14,r1
	add	#-32,r1
	mov.l	@(36,r1),r1
	mov.w	.L683,r2
	add	r2,r1
	mov	r14,r2
	add	#-32,r2
	mov.l	@(32,r2),r2
	shll2	r1
	add	r2,r1
	mov.l	@r1,r2
	mov	r14,r1
	add	#-32,r1
	mov.l	@(52,r1),r1
	shlr	r1
	xor	r1,r2
	mov	r14,r1
	add	#-32,r1
	mov.l	@(52,r1),r1
	mov	#1,r3
	and	r3,r1
	tst	r1,r1
	bt	.L669
	mov.l	.L690,r1
	bra	.L670
	nop
	.align 1
.L669:
	mov	#0,r1
.L670:
	xor	r1,r2
	mov	r14,r1
	add	#-32,r1
	mov.l	@(32,r1),r3
	mov	r14,r1
	add	#-32,r1
	mov.l	@(36,r1),r1
	shll2	r1
	add	r3,r1
	mov.l	r2,@r1
	mov	r14,r1
	add	#-32,r1
	mov	r14,r2
	add	#-32,r2
	mov.l	@(36,r2),r2
	add	#1,r2
	mov.l	r2,@(36,r1)
	bra	.L671
	nop
	.align 1
.L668:
	mov	r14,r1
	add	#-32,r1
	mov.w	.L685,r2
	mov.l	r2,@(40,r1)
.L675:
	mov	r14,r1
	add	#-32,r1
	mov.l	@(40,r1),r2
	mov.w	.L686,r1
	cmp/hi	r1,r2
	bt	.L672
	mov	r14,r1
	add	#-32,r1
	mov.l	@(32,r1),r2
	mov	r14,r1
	add	#-32,r1
	mov.l	@(40,r1),r1
	shll2	r1
	add	r2,r1
	mov.l	@r1,r2
	mov.l	.L687,r1
	mov	r2,r3
	and	r1,r3
	mov	r14,r1
	add	#-32,r1
	mov.l	@(40,r1),r1
	add	#1,r1
	mov	r14,r2
	add	#-32,r2
	mov.l	@(32,r2),r2
	shll2	r1
	add	r2,r1
	mov.l	@r1,r2
	mov.l	.L688,r1
	and	r1,r2
	mov	r14,r1
	add	#-32,r1
	or	r3,r2
	mov.l	r2,@(56,r1)
	mov	r14,r1
	add	#-32,r1
	mov.l	@(40,r1),r1
	mov.w	.L689,r2
	add	r2,r1
	mov	r14,r2
	add	#-32,r2
	mov.l	@(32,r2),r2
	shll2	r1
	add	r2,r1
	mov.l	@r1,r2
	mov	r14,r1
	add	#-32,r1
	mov.l	@(56,r1),r1
	shlr	r1
	xor	r1,r2
	mov	r14,r1
	add	#-32,r1
	mov.l	@(56,r1),r1
	mov	#1,r3
	and	r3,r1
	tst	r1,r1
	bt	.L673
	mov.l	.L690,r1
	bra	.L674
	nop
	.align 1
.L673:
	mov	#0,r1
.L674:
	xor	r1,r2
	mov	r14,r1
	add	#-32,r1
	mov.l	@(32,r1),r3
	mov	r14,r1
	add	#-32,r1
	mov.l	@(40,r1),r1
	shll2	r1
	add	r3,r1
	mov.l	r2,@r1
	mov	r14,r1
	add	#-32,r1
	mov	r14,r2
	add	#-32,r2
	mov.l	@(40,r2),r2
	add	#1,r2
	mov.l	r2,@(40,r1)
	bra	.L675
	nop
	.align 1
.L680:
	.short	226
.L683:
	.short	397
.L685:
	.short	227
.L686:
	.short	622
.L689:
	.short	-227
.L691:
	.align 2
.L687:
	.long	-2147483648
.L688:
	.long	2147483647
.L690:
	.long	-1727483681
	.align 1
.L672:
	mov	r14,r1
	add	#-32,r1
	mov.l	@(32,r1),r1
	mov.w	.L698,r2
	add	r2,r1
	mov.l	@(0,r1),r2
	mov.l	.L693,r1
	mov	r2,r3
	and	r1,r3
	mov	r14,r1
	add	#-32,r1
	mov.l	@(32,r1),r1
	mov.l	@r1,r2
	mov.l	.L694,r1
	and	r1,r2
	mov	r14,r1
	add	#-32,r1
	or	r3,r2
	mov.l	r2,@(60,r1)
	mov	r14,r1
	add	#-32,r1
	mov.l	@(32,r1),r1
	mov.w	.L695,r3
	add	r3,r1
	mov.l	@(52,r1),r2
	mov	r14,r1
	add	#-32,r1
	mov.l	@(60,r1),r1
	shlr	r1
	xor	r1,r2
	mov	r14,r1
	add	#-32,r1
	mov.l	@(60,r1),r1
	mov	#1,r3
	and	r3,r1
	tst	r1,r1
	bt	.L676
	mov.l	.L696,r1
	bra	.L677
	nop
	.align 1
.L676:
	mov	#0,r1
.L677:
	xor	r1,r2
	mov	r14,r1
	add	#-32,r1
	mov.l	@(32,r1),r1
	mov.w	.L698,r3
	add	r3,r1
	mov.l	r2,@(0,r1)
	mov	r14,r1
	add	#-32,r1
	mov.l	@(32,r1),r1
	mov.w	.L698,r2
	add	r2,r1
	mov	#0,r2
	mov.l	r2,@(4,r1)
	nop
	add	#32,r14
	.cfi_def_cfa_offset 4
	mov	r14,r15
	.cfi_def_cfa_register 15
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
	.align 1
.L698:
	.short	2492
.L695:
	.short	1532
.L699:
	.align 2
.L693:
	.long	-2147483648
.L694:
	.long	2147483647
.L696:
	.long	-1727483681
	.cfi_endproc
.LFE4658:
	.size	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE11_M_gen_randEv, .-_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE11_M_gen_randEv
	.section	.text._ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC2IS0_EERKSaIT_E,"axG",@progbits,_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC5IS0_EERKSaIT_E,comdat
	.align 1
	.weak	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC2IS0_EERKSaIT_E
	.type	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC2IS0_EERKSaIT_E, @function
_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC2IS0_EERKSaIT_E:
.LFB4661:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 17, -8
	add	#-8,r15
	.cfi_def_cfa_offset 16
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-56,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-56,r1
	mov.l	r5,@(56,r1)
	mov	r14,r1
	add	#-56,r1
	mov.l	@(60,r1),r4
	mov.l	.L701,r1
	jsr	@r1
	nop
	nop
	add	#8,r14
	.cfi_def_cfa_offset 8
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
.L702:
	.align 2
.L701:
	.long	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC2Ev
	.cfi_endproc
.LFE4661:
	.size	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC2IS0_EERKSaIT_E, .-_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC2IS0_EERKSaIT_E
	.weak	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC1IS0_EERKSaIT_E
	.set	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC1IS0_EERKSaIT_E,_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC2IS0_EERKSaIT_E
	.section	.text._ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED2Ev,"axG",@progbits,_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED5Ev,comdat
	.align 1
	.weak	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED2Ev
	.type	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED2Ev, @function
_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED2Ev:
.LFB4664:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 17, -8
	add	#-4,r15
	.cfi_def_cfa_offset 12
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-60,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-60,r1
	mov.l	@(60,r1),r4
	mov.l	.L704,r1
	jsr	@r1
	nop
	nop
	add	#4,r14
	.cfi_def_cfa_offset 8
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
.L705:
	.align 2
.L704:
	.long	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED2Ev
	.cfi_endproc
.LFE4664:
	.size	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED2Ev, .-_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED2Ev
	.weak	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED1Ev
	.set	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED1Ev,_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED2Ev
	.section	.text._ZSt18__allocate_guardedISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEESt15__allocated_ptrIT_ERS8_,"axG",@progbits,_ZSt18__allocate_guardedISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEESt15__allocated_ptrIT_ERS8_,comdat
	.align 1
	.weak	_ZSt18__allocate_guardedISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEESt15__allocated_ptrIT_ERS8_
	.type	_ZSt18__allocate_guardedISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEESt15__allocated_ptrIT_ERS8_, @function
_ZSt18__allocate_guardedISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEESt15__allocated_ptrIT_ERS8_:
.LFB4666:
	.cfi_startproc
	mov.l	r8,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 8, -4
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 14, -8
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 12
	.cfi_offset 17, -12
	add	#-4,r15
	.cfi_def_cfa_offset 16
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r2,r8
	mov	r14,r1
	add	#-60,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-60,r1
	mov	#1,r5
	mov.l	@(60,r1),r4
	mov.l	.L708,r1
	jsr	@r1
	nop
	mov	r0,r2
	mov	r14,r1
	add	#-60,r1
	mov	r2,r6
	mov.l	@(60,r1),r5
	mov	r8,r4
	mov.l	.L709,r1
	jsr	@r1
	nop
	mov	r8,r0
	mov	r8,r0
	add	#4,r14
	.cfi_def_cfa_offset 12
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 8
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r8
	.cfi_restore 8
	.cfi_def_cfa_offset 0
	rts	
	nop
.L710:
	.align 2
.L708:
	.long	_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE8allocateERS6_j
.L709:
	.long	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC1ERS6_PS5_
	.cfi_endproc
.LFE4666:
	.size	_ZSt18__allocate_guardedISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEESt15__allocated_ptrIT_ERS8_, .-_ZSt18__allocate_guardedISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEESt15__allocated_ptrIT_ERS8_
	.section	.text._ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED2Ev,"axG",@progbits,_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED5Ev,comdat
	.align 1
	.weak	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED2Ev
	.type	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED2Ev, @function
_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED2Ev:
.LFB4668:
	.cfi_startproc
	.cfi_personality 0,__gxx_personality_v0
	.cfi_lsda 0xb,.LLSDA4668
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 17, -8
	add	#-4,r15
	.cfi_def_cfa_offset 12
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-60,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-60,r1
	mov.l	@(60,r1),r1
	mov.l	@(4,r1),r1
	tst	r1,r1
	bt	.L713
	mov	r14,r1
	add	#-60,r1
	mov.l	@(60,r1),r1
	mov.l	@r1,r2
	mov	r14,r1
	add	#-60,r1
	mov.l	@(60,r1),r1
	mov.l	@(4,r1),r1
	mov	#1,r6
	mov	r1,r5
	mov	r2,r4
	mov.l	.L714,r1
	jsr	@r1
	nop
.L713:
	nop
	add	#4,r14
	.cfi_def_cfa_offset 8
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
.L715:
	.align 2
.L714:
	.long	_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE10deallocateERS6_PS5_j
	.cfi_endproc
.LFE4668:
	.section	.gcc_except_table
.LLSDA4668:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4668-.LLSDACSB4668
.LLSDACSB4668:
.LLSDACSE4668:
	.section	.text._ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED2Ev,"axG",@progbits,_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED5Ev,comdat
	.size	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED2Ev, .-_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED2Ev
	.weak	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED1Ev
	.set	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED1Ev,_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED2Ev
	.section	.text._ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE3getEv,"axG",@progbits,_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE3getEv,comdat
	.align 1
	.weak	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE3getEv
	.type	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE3getEv, @function
_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE3getEv:
.LFB4673:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 17, -8
	add	#-4,r15
	.cfi_def_cfa_offset 12
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-60,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-60,r1
	mov.l	@(60,r1),r1
	mov.l	@(4,r1),r1
	mov	r1,r4
	mov.l	.L718,r1
	jsr	@r1
	nop
	mov	r0,r1
	mov	r1,r0
	add	#4,r14
	.cfi_def_cfa_offset 8
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
.L719:
	.align 2
.L718:
	.long	_ZSt12__to_addressISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEPT_S7_
	.cfi_endproc
.LFE4673:
	.size	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE3getEv, .-_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE3getEv
	.section	.text._ZNSaI6sphereEC2ERKS0_,"axG",@progbits,_ZNSaI6sphereEC5ERKS0_,comdat
	.align 1
	.weak	_ZNSaI6sphereEC2ERKS0_
	.type	_ZNSaI6sphereEC2ERKS0_, @function
_ZNSaI6sphereEC2ERKS0_:
.LFB4675:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 17, -8
	add	#-8,r15
	.cfi_def_cfa_offset 16
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-56,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-56,r1
	mov.l	r5,@(56,r1)
	mov	r14,r2
	add	#-56,r2
	mov	r14,r1
	add	#-56,r1
	mov.l	@(56,r2),r5
	mov.l	@(60,r1),r4
	mov.l	.L721,r1
	jsr	@r1
	nop
	nop
	add	#8,r14
	.cfi_def_cfa_offset 8
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
.L722:
	.align 2
.L721:
	.long	_ZN9__gnu_cxx13new_allocatorI6sphereEC2ERKS2_
	.cfi_endproc
.LFE4675:
	.size	_ZNSaI6sphereEC2ERKS0_, .-_ZNSaI6sphereEC2ERKS0_
	.weak	_ZNSaI6sphereEC1ERKS0_
	.set	_ZNSaI6sphereEC1ERKS0_,_ZNSaI6sphereEC2ERKS0_
	.section	.text._ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED2Ev,"axG",@progbits,_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED5Ev,comdat
	.align 1
	.weak	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED2Ev
	.type	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED2Ev, @function
_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED2Ev:
.LFB4680:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 17, -8
	add	#-4,r15
	.cfi_def_cfa_offset 12
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-60,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-60,r1
	mov.l	@(60,r1),r4
	mov.l	.L724,r1
	jsr	@r1
	nop
	nop
	add	#4,r14
	.cfi_def_cfa_offset 8
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
.L725:
	.align 2
.L724:
	.long	_ZNSaI6sphereED2Ev
	.cfi_endproc
.LFE4680:
	.size	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED2Ev, .-_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED2Ev
	.weak	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED1Ev
	.set	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED1Ev,_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED2Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD2Ev,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD5Ev,comdat
	.align 1
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD2Ev
	.type	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD2Ev, @function
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD2Ev:
.LFB4682:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 17, -8
	add	#-4,r15
	.cfi_def_cfa_offset 12
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-60,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-60,r1
	mov.l	@(60,r1),r4
	mov.l	.L727,r1
	jsr	@r1
	nop
	nop
	add	#4,r14
	.cfi_def_cfa_offset 8
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
.L728:
	.align 2
.L727:
	.long	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED2Ev
	.cfi_endproc
.LFE4682:
	.size	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD2Ev, .-_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD2Ev
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD1Ev
	.set	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD1Ev,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD2Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3dEEES1_DpOT_,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC5IJ4vec3dEEES1_DpOT_,comdat
	.align 1
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3dEEES1_DpOT_
	.type	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3dEEES1_DpOT_, @function
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3dEEES1_DpOT_:
.LFB4684:
	.cfi_startproc
	.cfi_personality 0,__gxx_personality_v0
	.cfi_lsda 0xb,.LLSDA4684
	mov.l	r8,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 8, -4
	mov.l	r9,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 9, -8
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 12
	.cfi_offset 14, -12
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 16
	.cfi_offset 17, -16
	add	#-24,r15
	.cfi_def_cfa_offset 40
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-40,r1
	mov.l	r4,@(52,r1)
	mov	r14,r1
	add	#-40,r1
	mov.l	r5,@(48,r1)
	mov	r14,r1
	add	#-40,r1
	mov.l	r6,@(44,r1)
	mov	r14,r1
	add	#-40,r1
	mov.l	r7,@(40,r1)
	mov	r14,r1
	add	#-40,r1
	mov.l	.L749,r2
	mov.l	@r2,r3
	mov.l	r3,@(60,r1)
	mov	#0,r3
	mov	r14,r1
	add	#-40,r1
	mov.l	@(52,r1),r1
	mov	r1,r4
	mov.l	.L736,r1
	jsr	@r1
	nop
	mov.l	.L737,r2
	mov	r14,r1
	add	#-40,r1
	mov.l	@(52,r1),r1
	mov.l	r2,@r1
	mov	r14,r1
	add	#-40,r1
	mov.l	@(52,r1),r1
	mov	r1,r8
	add	#12,r8
	mov	r14,r1
	add	#-40,r1
	mov	r14,r2
	add	#19,r2
	mov.l	@(48,r1),r5
	mov	r2,r4
	mov.l	.L738,r1
	jsr	@r1
	nop
	mov	r14,r1
	add	#19,r1
	mov	r1,r5
	mov	r8,r4
	mov.l	.L739,r1
	jsr	@r1
	nop
	mov	r14,r1
	add	#19,r1
	mov	r1,r4
	mov.l	.L740,r1
	jsr	@r1
	nop
	mov	r14,r1
	add	#-40,r1
	mov.l	@(52,r1),r4
	mov.l	.L741,r1
	jsr	@r1
	nop
	mov	r0,r8
	mov	r14,r1
	add	#-40,r1
	mov.l	@(44,r1),r4
	mov.l	.L742,r1
	jsr	@r1
	nop
	mov	r0,r9
	mov	r14,r1
	add	#-40,r1
	mov.l	@(40,r1),r4
	mov.l	.L743,r1
	jsr	@r1
	nop
	mov	r0,r2
	mov	r14,r1
	add	#-40,r1
	mov	r2,r7
	mov	r9,r6
	mov	r8,r5
	mov.l	@(48,r1),r4
	mov.l	.L744,r1
.LEHB26:
	jsr	@r1
	nop
.LEHE26:
	bra	.L733
	nop
	.align 1
.L732:
	mov	r4,r8
	mov	r14,r1
	add	#-40,r1
	mov.l	@(52,r1),r1
	add	#12,r1
	mov	r1,r4
	mov.l	.L745,r2
	sts	fpscr,r1
	mov.l	.L746,r3
	or	r3,r1
	lds	r1,fpscr
	jsr	@r2
	nop
	mov	r14,r1
	add	#-40,r1
	mov.l	@(52,r1),r1
	mov	r1,r4
	mov.l	.L747,r1
	jsr	@r1
	nop
	mov	r8,r1
	mov	r1,r4
	mov.l	.L748,r1
.LEHB27:
	jsr	@r1
	nop
.LEHE27:
	.align 1
.L733:
	mov	r14,r1
	add	#-40,r1
	mov.l	.L749,r2
	mov.l	@(60,r1),r7
	mov.l	@r2,r3
	cmp/eq	r7,r3
	mov	#0,r7
	mov	#0,r3
	bt	.L734
	mov.l	.L750,r1
	jsr	@r1
	nop
	.align 1
.L734:
	add	#24,r14
	.cfi_def_cfa_offset 16
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 12
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 8
	mov.l	@r15+,r9
	.cfi_restore 9
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r8
	.cfi_restore 8
	.cfi_def_cfa_offset 0
	rts	
	nop
.L751:
	.align 2
.L749:
	.long	__stack_chk_guard
.L736:
	.long	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC2Ev
.L737:
	.long	_ZTVSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE+8
.L738:
	.long	_ZNSaI6sphereEC1ERKS0_
.L739:
	.long	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC1ES1_
.L740:
	.long	_ZNSaI6sphereED1Ev
.L741:
	.long	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv
.L742:
	.long	_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
.L743:
	.long	_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE
.L744:
	.long	_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3dEEEvRS1_PT_DpOT0_
.L745:
	.long	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD1Ev
.L746:
	.long	524288
.L747:
	.long	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev
.L748:
	.long	_Unwind_Resume
.L750:
	.long	__stack_chk_fail
	.cfi_endproc
.LFE4684:
	.section	.gcc_except_table
.LLSDA4684:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4684-.LLSDACSB4684
.LLSDACSB4684:
	.uleb128 .LEHB26-.LFB4684
	.uleb128 .LEHE26-.LEHB26
	.uleb128 .L732-.LFB4684
	.uleb128 0
	.uleb128 .LEHB27-.LFB4684
	.uleb128 .LEHE27-.LEHB27
	.uleb128 0
	.uleb128 0
.LLSDACSE4684:
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3dEEES1_DpOT_,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC5IJ4vec3dEEES1_DpOT_,comdat
	.size	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3dEEES1_DpOT_, .-_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3dEEES1_DpOT_
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC1IJ4vec3dEEES1_DpOT_
	.set	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC1IJ4vec3dEEES1_DpOT_,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3dEEES1_DpOT_
	.section	.text._ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEaSEDn,"axG",@progbits,_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEaSEDn,comdat
	.align 1
	.weak	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEaSEDn
	.type	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEaSEDn, @function
_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEaSEDn:
.LFB4686:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	add	#-8,r15
	.cfi_def_cfa_offset 12
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-56,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-56,r1
	mov.l	r5,@(56,r1)
	mov	r14,r1
	add	#-56,r1
	mov.l	@(60,r1),r1
	mov	#0,r2
	mov.l	r2,@(4,r1)
	mov	r14,r1
	add	#-56,r1
	mov.l	@(60,r1),r1
	mov	r1,r0
	add	#8,r14
	.cfi_def_cfa_offset 4
	mov	r14,r15
	.cfi_def_cfa_register 15
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
	.cfi_endproc
.LFE4686:
	.size	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEaSEDn, .-_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEaSEDn
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv,comdat
	.align 1
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv
	.type	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv, @function
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv:
.LFB4687:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 17, -8
	add	#-4,r15
	.cfi_def_cfa_offset 12
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-60,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-60,r1
	mov.l	@(60,r1),r1
	add	#12,r1
	mov	r1,r4
	mov.l	.L756,r1
	jsr	@r1
	nop
	mov	r0,r1
	mov	r1,r0
	add	#4,r14
	.cfi_def_cfa_offset 8
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
.L757:
	.align 2
.L756:
	.long	_ZN9__gnu_cxx16__aligned_bufferI6sphereE6_M_ptrEv
	.cfi_endproc
.LFE4687:
	.size	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv, .-_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3iEEES1_DpOT_,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC5IJ4vec3iEEES1_DpOT_,comdat
	.align 1
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3iEEES1_DpOT_
	.type	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3iEEES1_DpOT_, @function
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3iEEES1_DpOT_:
.LFB4689:
	.cfi_startproc
	.cfi_personality 0,__gxx_personality_v0
	.cfi_lsda 0xb,.LLSDA4689
	mov.l	r8,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 8, -4
	mov.l	r9,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 9, -8
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 12
	.cfi_offset 14, -12
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 16
	.cfi_offset 17, -16
	add	#-24,r15
	.cfi_def_cfa_offset 40
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-40,r1
	mov.l	r4,@(52,r1)
	mov	r14,r1
	add	#-40,r1
	mov.l	r5,@(48,r1)
	mov	r14,r1
	add	#-40,r1
	mov.l	r6,@(44,r1)
	mov	r14,r1
	add	#-40,r1
	mov.l	r7,@(40,r1)
	mov	r14,r1
	add	#-40,r1
	mov.l	.L778,r2
	mov.l	@r2,r3
	mov.l	r3,@(60,r1)
	mov	#0,r3
	mov	r14,r1
	add	#-40,r1
	mov.l	@(52,r1),r1
	mov	r1,r4
	mov.l	.L765,r1
	jsr	@r1
	nop
	mov.l	.L766,r2
	mov	r14,r1
	add	#-40,r1
	mov.l	@(52,r1),r1
	mov.l	r2,@r1
	mov	r14,r1
	add	#-40,r1
	mov.l	@(52,r1),r1
	mov	r1,r8
	add	#12,r8
	mov	r14,r1
	add	#-40,r1
	mov	r14,r2
	add	#19,r2
	mov.l	@(48,r1),r5
	mov	r2,r4
	mov.l	.L767,r1
	jsr	@r1
	nop
	mov	r14,r1
	add	#19,r1
	mov	r1,r5
	mov	r8,r4
	mov.l	.L768,r1
	jsr	@r1
	nop
	mov	r14,r1
	add	#19,r1
	mov	r1,r4
	mov.l	.L769,r1
	jsr	@r1
	nop
	mov	r14,r1
	add	#-40,r1
	mov.l	@(52,r1),r4
	mov.l	.L770,r1
	jsr	@r1
	nop
	mov	r0,r8
	mov	r14,r1
	add	#-40,r1
	mov.l	@(44,r1),r4
	mov.l	.L771,r1
	jsr	@r1
	nop
	mov	r0,r9
	mov	r14,r1
	add	#-40,r1
	mov.l	@(40,r1),r4
	mov.l	.L772,r1
	jsr	@r1
	nop
	mov	r0,r2
	mov	r14,r1
	add	#-40,r1
	mov	r2,r7
	mov	r9,r6
	mov	r8,r5
	mov.l	@(48,r1),r4
	mov.l	.L773,r1
.LEHB28:
	jsr	@r1
	nop
.LEHE28:
	bra	.L762
	nop
	.align 1
.L761:
	mov	r4,r8
	mov	r14,r1
	add	#-40,r1
	mov.l	@(52,r1),r1
	add	#12,r1
	mov	r1,r4
	mov.l	.L774,r2
	sts	fpscr,r1
	mov.l	.L775,r3
	or	r3,r1
	lds	r1,fpscr
	jsr	@r2
	nop
	mov	r14,r1
	add	#-40,r1
	mov.l	@(52,r1),r1
	mov	r1,r4
	mov.l	.L776,r1
	jsr	@r1
	nop
	mov	r8,r1
	mov	r1,r4
	mov.l	.L777,r1
.LEHB29:
	jsr	@r1
	nop
.LEHE29:
	.align 1
.L762:
	mov	r14,r1
	add	#-40,r1
	mov.l	.L778,r2
	mov.l	@(60,r1),r7
	mov.l	@r2,r3
	cmp/eq	r7,r3
	mov	#0,r7
	mov	#0,r3
	bt	.L763
	mov.l	.L779,r1
	jsr	@r1
	nop
	.align 1
.L763:
	add	#24,r14
	.cfi_def_cfa_offset 16
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 12
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 8
	mov.l	@r15+,r9
	.cfi_restore 9
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r8
	.cfi_restore 8
	.cfi_def_cfa_offset 0
	rts	
	nop
.L780:
	.align 2
.L778:
	.long	__stack_chk_guard
.L765:
	.long	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC2Ev
.L766:
	.long	_ZTVSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE+8
.L767:
	.long	_ZNSaI6sphereEC1ERKS0_
.L768:
	.long	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC1ES1_
.L769:
	.long	_ZNSaI6sphereED1Ev
.L770:
	.long	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv
.L771:
	.long	_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
.L772:
	.long	_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE
.L773:
	.long	_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3iEEEvRS1_PT_DpOT0_
.L774:
	.long	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD1Ev
.L775:
	.long	524288
.L776:
	.long	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev
.L777:
	.long	_Unwind_Resume
.L779:
	.long	__stack_chk_fail
	.cfi_endproc
.LFE4689:
	.section	.gcc_except_table
.LLSDA4689:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4689-.LLSDACSB4689
.LLSDACSB4689:
	.uleb128 .LEHB28-.LFB4689
	.uleb128 .LEHE28-.LEHB28
	.uleb128 .L761-.LFB4689
	.uleb128 0
	.uleb128 .LEHB29-.LFB4689
	.uleb128 .LEHE29-.LEHB29
	.uleb128 0
	.uleb128 0
.LLSDACSE4689:
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3iEEES1_DpOT_,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC5IJ4vec3iEEES1_DpOT_,comdat
	.size	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3iEEES1_DpOT_, .-_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3iEEES1_DpOT_
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC1IJ4vec3iEEES1_DpOT_
	.set	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC1IJ4vec3iEEES1_DpOT_,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3iEEES1_DpOT_
	.section	.text._ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC2Ev,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC5Ev,comdat
	.align 1
	.weak	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC2Ev
	.type	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC2Ev, @function
_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC2Ev:
.LFB4702:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	add	#-4,r15
	.cfi_def_cfa_offset 8
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-60,r1
	mov.l	r4,@(60,r1)
	nop
	add	#4,r14
	.cfi_def_cfa_offset 4
	mov	r14,r15
	.cfi_def_cfa_register 15
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
	.cfi_endproc
.LFE4702:
	.size	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC2Ev, .-_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC2Ev
	.weak	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC1Ev
	.set	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC1Ev,_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC2Ev
	.section	.text._ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED2Ev,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED5Ev,comdat
	.align 1
	.weak	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED2Ev
	.type	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED2Ev, @function
_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED2Ev:
.LFB4705:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	add	#-4,r15
	.cfi_def_cfa_offset 8
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-60,r1
	mov.l	r4,@(60,r1)
	nop
	add	#4,r14
	.cfi_def_cfa_offset 4
	mov	r14,r15
	.cfi_def_cfa_register 15
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
	.cfi_endproc
.LFE4705:
	.size	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED2Ev, .-_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED2Ev
	.weak	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED1Ev
	.set	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED1Ev,_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED2Ev
	.section	.text._ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE8allocateERS6_j,"axG",@progbits,_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE8allocateERS6_j,comdat
	.align 1
	.weak	_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE8allocateERS6_j
	.type	_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE8allocateERS6_j, @function
_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE8allocateERS6_j:
.LFB4707:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 17, -8
	add	#-8,r15
	.cfi_def_cfa_offset 16
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-56,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-56,r1
	mov.l	r5,@(56,r1)
	mov	r14,r2
	add	#-56,r2
	mov	r14,r1
	add	#-56,r1
	mov	#0,r6
	mov.l	@(56,r2),r5
	mov.l	@(60,r1),r4
	mov.l	.L785,r1
	jsr	@r1
	nop
	mov	r0,r1
	mov	r1,r0
	add	#8,r14
	.cfi_def_cfa_offset 8
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
.L786:
	.align 2
.L785:
	.long	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8allocateEjPKv
	.cfi_endproc
.LFE4707:
	.size	_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE8allocateERS6_j, .-_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE8allocateERS6_j
	.section	.text._ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC2ERS6_PS5_,"axG",@progbits,_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC5ERS6_PS5_,comdat
	.align 1
	.weak	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC2ERS6_PS5_
	.type	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC2ERS6_PS5_, @function
_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC2ERS6_PS5_:
.LFB4709:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 17, -8
	add	#-12,r15
	.cfi_def_cfa_offset 20
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-52,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-52,r1
	mov.l	r5,@(56,r1)
	mov	r14,r1
	add	#-52,r1
	mov.l	r6,@(52,r1)
	mov	r14,r1
	add	#-52,r1
	mov.l	@(56,r1),r4
	mov.l	.L788,r1
	jsr	@r1
	nop
	mov	r0,r1
	mov	r14,r2
	add	#-52,r2
	mov.l	@(60,r2),r2
	mov.l	r1,@r2
	mov	r14,r1
	add	#-52,r1
	mov.l	@(60,r1),r1
	mov	r14,r2
	add	#-52,r2
	mov.l	@(52,r2),r2
	mov.l	r2,@(4,r1)
	nop
	add	#12,r14
	.cfi_def_cfa_offset 8
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
.L789:
	.align 2
.L788:
	.long	_ZSt11__addressofISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEPT_RS7_
	.cfi_endproc
.LFE4709:
	.size	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC2ERS6_PS5_, .-_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC2ERS6_PS5_
	.weak	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC1ERS6_PS5_
	.set	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC1ERS6_PS5_,_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC2ERS6_PS5_
	.section	.text._ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE10deallocateERS6_PS5_j,"axG",@progbits,_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE10deallocateERS6_PS5_j,comdat
	.align 1
	.weak	_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE10deallocateERS6_PS5_j
	.type	_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE10deallocateERS6_PS5_j, @function
_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE10deallocateERS6_PS5_j:
.LFB4711:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 17, -8
	add	#-12,r15
	.cfi_def_cfa_offset 20
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-52,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-52,r1
	mov.l	r5,@(56,r1)
	mov	r14,r1
	add	#-52,r1
	mov.l	r6,@(52,r1)
	mov	r14,r3
	add	#-52,r3
	mov	r14,r2
	add	#-52,r2
	mov	r14,r1
	add	#-52,r1
	mov.l	@(52,r3),r6
	mov.l	@(56,r2),r5
	mov.l	@(60,r1),r4
	mov.l	.L791,r1
	jsr	@r1
	nop
	nop
	add	#12,r14
	.cfi_def_cfa_offset 8
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
.L792:
	.align 2
.L791:
	.long	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE10deallocateEPS5_j
	.cfi_endproc
.LFE4711:
	.size	_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE10deallocateERS6_PS5_j, .-_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE10deallocateERS6_PS5_j
	.section	.text._ZSt12__to_addressISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEPT_S7_,"axG",@progbits,_ZSt12__to_addressISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEPT_S7_,comdat
	.align 1
	.weak	_ZSt12__to_addressISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEPT_S7_
	.type	_ZSt12__to_addressISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEPT_S7_, @function
_ZSt12__to_addressISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEPT_S7_:
.LFB4712:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	add	#-4,r15
	.cfi_def_cfa_offset 8
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-60,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-60,r1
	mov.l	@(60,r1),r1
	mov	r1,r0
	add	#4,r14
	.cfi_def_cfa_offset 4
	mov	r14,r15
	.cfi_def_cfa_register 15
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
	.cfi_endproc
.LFE4712:
	.size	_ZSt12__to_addressISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEPT_S7_, .-_ZSt12__to_addressISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEPT_S7_
	.section	.text._ZN9__gnu_cxx13new_allocatorI6sphereEC2ERKS2_,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorI6sphereEC5ERKS2_,comdat
	.align 1
	.weak	_ZN9__gnu_cxx13new_allocatorI6sphereEC2ERKS2_
	.type	_ZN9__gnu_cxx13new_allocatorI6sphereEC2ERKS2_, @function
_ZN9__gnu_cxx13new_allocatorI6sphereEC2ERKS2_:
.LFB4714:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	add	#-8,r15
	.cfi_def_cfa_offset 12
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-56,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-56,r1
	mov.l	r5,@(56,r1)
	nop
	add	#8,r14
	.cfi_def_cfa_offset 4
	mov	r14,r15
	.cfi_def_cfa_register 15
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
	.cfi_endproc
.LFE4714:
	.size	_ZN9__gnu_cxx13new_allocatorI6sphereEC2ERKS2_, .-_ZN9__gnu_cxx13new_allocatorI6sphereEC2ERKS2_
	.weak	_ZN9__gnu_cxx13new_allocatorI6sphereEC1ERKS2_
	.set	_ZN9__gnu_cxx13new_allocatorI6sphereEC1ERKS2_,_ZN9__gnu_cxx13new_allocatorI6sphereEC2ERKS2_
	.section	.text._ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC2Ev,"axG",@progbits,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC5Ev,comdat
	.align 1
	.weak	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC2Ev
	.type	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC2Ev, @function
_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC2Ev:
.LFB4717:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	add	#-4,r15
	.cfi_def_cfa_offset 8
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-60,r1
	mov.l	r4,@(60,r1)
	mov.l	.L797,r2
	mov	r14,r1
	add	#-60,r1
	mov.l	@(60,r1),r1
	mov.l	r2,@r1
	mov	r14,r1
	add	#-60,r1
	mov.l	@(60,r1),r1
	mov	#1,r2
	mov.l	r2,@(4,r1)
	mov	r14,r1
	add	#-60,r1
	mov.l	@(60,r1),r1
	mov	#1,r2
	mov.l	r2,@(8,r1)
	nop
	add	#4,r14
	.cfi_def_cfa_offset 4
	mov	r14,r15
	.cfi_def_cfa_register 15
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
.L798:
	.align 2
.L797:
	.long	_ZTVSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE+8
	.cfi_endproc
.LFE4717:
	.size	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC2Ev, .-_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC2Ev
	.weak	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC1Ev
	.set	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC1Ev,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC2Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC2ES1_,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC5ES1_,comdat
	.align 1
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC2ES1_
	.type	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC2ES1_, @function
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC2ES1_:
.LFB4720:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 17, -8
	add	#-8,r15
	.cfi_def_cfa_offset 16
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-56,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-56,r1
	mov.l	r5,@(56,r1)
	mov	r14,r2
	add	#-56,r2
	mov	r14,r1
	add	#-56,r1
	mov.l	@(56,r2),r5
	mov.l	@(60,r1),r4
	mov.l	.L800,r1
	jsr	@r1
	nop
	nop
	add	#8,r14
	.cfi_def_cfa_offset 8
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
.L801:
	.align 2
.L800:
	.long	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC2ERKS1_
	.cfi_endproc
.LFE4720:
	.size	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC2ES1_, .-_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC2ES1_
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC1ES1_
	.set	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC1ES1_,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC2ES1_
	.section	.text._ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3dEEEvRS1_PT_DpOT0_,"axG",@progbits,_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3dEEEvRS1_PT_DpOT0_,comdat
	.align 1
	.weak	_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3dEEEvRS1_PT_DpOT0_
	.type	_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3dEEEvRS1_PT_DpOT0_, @function
_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3dEEEvRS1_PT_DpOT0_:
.LFB4722:
	.cfi_startproc
	mov.l	r8,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 8, -4
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 14, -8
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 12
	.cfi_offset 17, -12
	add	#-16,r15
	.cfi_def_cfa_offset 28
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-48,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-48,r1
	mov.l	r5,@(56,r1)
	mov	r14,r1
	add	#-48,r1
	mov.l	r6,@(52,r1)
	mov	r14,r1
	add	#-48,r1
	mov.l	r7,@(48,r1)
	mov	r14,r1
	add	#-48,r1
	mov.l	@(52,r1),r4
	mov.l	.L803,r1
	jsr	@r1
	nop
	mov	r0,r8
	mov	r14,r1
	add	#-48,r1
	mov.l	@(48,r1),r4
	mov.l	.L804,r1
	jsr	@r1
	nop
	mov	r0,r3
	mov	r14,r2
	add	#-48,r2
	mov	r14,r1
	add	#-48,r1
	mov	r3,r7
	mov	r8,r6
	mov.l	@(56,r2),r5
	mov.l	@(60,r1),r4
	mov.l	.L805,r1
	jsr	@r1
	nop
	nop
	add	#16,r14
	.cfi_def_cfa_offset 12
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 8
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r8
	.cfi_restore 8
	.cfi_def_cfa_offset 0
	rts	
	nop
.L806:
	.align 2
.L803:
	.long	_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
.L804:
	.long	_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE
.L805:
	.long	_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_
	.cfi_endproc
.LFE4722:
	.size	_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3dEEEvRS1_PT_DpOT0_, .-_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3dEEEvRS1_PT_DpOT0_
	.section	.text._ZN9__gnu_cxx16__aligned_bufferI6sphereE6_M_ptrEv,"axG",@progbits,_ZN9__gnu_cxx16__aligned_bufferI6sphereE6_M_ptrEv,comdat
	.align 1
	.weak	_ZN9__gnu_cxx16__aligned_bufferI6sphereE6_M_ptrEv
	.type	_ZN9__gnu_cxx16__aligned_bufferI6sphereE6_M_ptrEv, @function
_ZN9__gnu_cxx16__aligned_bufferI6sphereE6_M_ptrEv:
.LFB4723:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 17, -8
	add	#-4,r15
	.cfi_def_cfa_offset 12
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-60,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-60,r1
	mov.l	@(60,r1),r4
	mov.l	.L809,r1
	jsr	@r1
	nop
	mov	r0,r1
	mov	r1,r0
	add	#4,r14
	.cfi_def_cfa_offset 8
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
.L810:
	.align 2
.L809:
	.long	_ZN9__gnu_cxx16__aligned_bufferI6sphereE7_M_addrEv
	.cfi_endproc
.LFE4723:
	.size	_ZN9__gnu_cxx16__aligned_bufferI6sphereE6_M_ptrEv, .-_ZN9__gnu_cxx16__aligned_bufferI6sphereE6_M_ptrEv
	.section	.text._ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3iEEEvRS1_PT_DpOT0_,"axG",@progbits,_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3iEEEvRS1_PT_DpOT0_,comdat
	.align 1
	.weak	_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3iEEEvRS1_PT_DpOT0_
	.type	_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3iEEEvRS1_PT_DpOT0_, @function
_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3iEEEvRS1_PT_DpOT0_:
.LFB4724:
	.cfi_startproc
	mov.l	r8,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 8, -4
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 14, -8
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 12
	.cfi_offset 17, -12
	add	#-16,r15
	.cfi_def_cfa_offset 28
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-48,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-48,r1
	mov.l	r5,@(56,r1)
	mov	r14,r1
	add	#-48,r1
	mov.l	r6,@(52,r1)
	mov	r14,r1
	add	#-48,r1
	mov.l	r7,@(48,r1)
	mov	r14,r1
	add	#-48,r1
	mov.l	@(52,r1),r4
	mov.l	.L812,r1
	jsr	@r1
	nop
	mov	r0,r8
	mov	r14,r1
	add	#-48,r1
	mov.l	@(48,r1),r4
	mov.l	.L813,r1
	jsr	@r1
	nop
	mov	r0,r3
	mov	r14,r2
	add	#-48,r2
	mov	r14,r1
	add	#-48,r1
	mov	r3,r7
	mov	r8,r6
	mov.l	@(56,r2),r5
	mov.l	@(60,r1),r4
	mov.l	.L814,r1
	jsr	@r1
	nop
	nop
	add	#16,r14
	.cfi_def_cfa_offset 12
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 8
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r8
	.cfi_restore 8
	.cfi_def_cfa_offset 0
	rts	
	nop
.L815:
	.align 2
.L812:
	.long	_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
.L813:
	.long	_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE
.L814:
	.long	_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_
	.cfi_endproc
.LFE4724:
	.size	_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3iEEEvRS1_PT_DpOT0_, .-_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3iEEEvRS1_PT_DpOT0_
	.section	.text._ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8allocateEjPKv,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8allocateEjPKv,comdat
	.align 1
	.weak	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8allocateEjPKv
	.type	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8allocateEjPKv, @function
_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8allocateEjPKv:
.LFB4727:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 17, -8
	add	#-12,r15
	.cfi_def_cfa_offset 20
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-52,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-52,r1
	mov.l	r5,@(56,r1)
	mov	r14,r1
	add	#-52,r1
	mov.l	r6,@(52,r1)
	mov	r14,r1
	add	#-52,r1
	mov.l	@(60,r1),r4
	mov.l	.L819,r1
	jsr	@r1
	nop
	mov	r0,r1
	mov	r14,r2
	add	#-52,r2
	mov.l	@(56,r2),r2
	cmp/hi	r1,r2
	movt	r1
	extu.b	r1,r1
	tst	r1,r1
	bt	.L817
	mov.l	.L820,r1
	jsr	@r1
	nop
	.align 1
.L817:
	mov	r14,r1
	add	#-52,r1
	mov.l	@(56,r1),r2
	mov	#56,r1
	mul.l	r1,r2
	sts	macl,r1
	mov	r1,r4
	mov.l	.L821,r1
	jsr	@r1
	nop
	mov	r0,r1
	mov	r1,r0
	add	#12,r14
	.cfi_def_cfa_offset 8
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
.L822:
	.align 2
.L819:
	.long	_ZNK9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8max_sizeEv
.L820:
	.long	_ZSt17__throw_bad_allocv
.L821:
	.long	_Znwj
	.cfi_endproc
.LFE4727:
	.size	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8allocateEjPKv, .-_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8allocateEjPKv
	.section	.text._ZSt11__addressofISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEPT_RS7_,"axG",@progbits,_ZSt11__addressofISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEPT_RS7_,comdat
	.align 1
	.weak	_ZSt11__addressofISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEPT_RS7_
	.type	_ZSt11__addressofISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEPT_RS7_, @function
_ZSt11__addressofISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEPT_RS7_:
.LFB4728:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	add	#-4,r15
	.cfi_def_cfa_offset 8
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-60,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-60,r1
	mov.l	@(60,r1),r1
	mov	r1,r0
	add	#4,r14
	.cfi_def_cfa_offset 4
	mov	r14,r15
	.cfi_def_cfa_register 15
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
	.cfi_endproc
.LFE4728:
	.size	_ZSt11__addressofISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEPT_RS7_, .-_ZSt11__addressofISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEPT_RS7_
	.section	.text._ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE10deallocateEPS5_j,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE10deallocateEPS5_j,comdat
	.align 1
	.weak	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE10deallocateEPS5_j
	.type	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE10deallocateEPS5_j, @function
_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE10deallocateEPS5_j:
.LFB4729:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 17, -8
	add	#-12,r15
	.cfi_def_cfa_offset 20
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-52,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-52,r1
	mov.l	r5,@(56,r1)
	mov	r14,r1
	add	#-52,r1
	mov.l	r6,@(52,r1)
	mov	r14,r1
	add	#-52,r1
	mov.l	@(56,r1),r4
	mov.l	.L826,r1
	jsr	@r1
	nop
	nop
	add	#12,r14
	.cfi_def_cfa_offset 8
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
.L827:
	.align 2
.L826:
	.long	_ZdlPv
	.cfi_endproc
.LFE4729:
	.size	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE10deallocateEPS5_j, .-_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE10deallocateEPS5_j
	.section	.text._ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC2ERKS1_,"axG",@progbits,_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC5ERKS1_,comdat
	.align 1
	.weak	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC2ERKS1_
	.type	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC2ERKS1_, @function
_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC2ERKS1_:
.LFB4731:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 17, -8
	add	#-8,r15
	.cfi_def_cfa_offset 16
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-56,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-56,r1
	mov.l	r5,@(56,r1)
	mov	r14,r2
	add	#-56,r2
	mov	r14,r1
	add	#-56,r1
	mov.l	@(56,r2),r5
	mov.l	@(60,r1),r4
	mov.l	.L829,r1
	jsr	@r1
	nop
	nop
	add	#8,r14
	.cfi_def_cfa_offset 8
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
.L830:
	.align 2
.L829:
	.long	_ZNSaI6sphereEC2ERKS0_
	.cfi_endproc
.LFE4731:
	.size	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC2ERKS1_, .-_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC2ERKS1_
	.weak	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC1ERKS1_
	.set	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC1ERKS1_,_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC2ERKS1_
	.section	.text._ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_,comdat
	.align 1
	.weak	_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_
	.type	_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_, @function
_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_:
.LFB4733:
	.cfi_startproc
	.cfi_personality 0,__gxx_personality_v0
	.cfi_lsda 0xb,.LLSDA4733
	mov.l	r8,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 8, -4
	mov.l	r9,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 9, -8
	mov.l	r10,@-r15
	.cfi_def_cfa_offset 12
	.cfi_offset 10, -12
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 16
	.cfi_offset 14, -16
	fmov.s	fr12,@-r15
	.cfi_def_cfa_offset 20
	.cfi_offset 37, -20
	fmov.s	fr13,@-r15
	.cfi_def_cfa_offset 24
	.cfi_offset 38, -24
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 28
	.cfi_offset 17, -28
	add	#-68,r15
	.cfi_def_cfa_offset 96
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#4,r1
	mov.l	r4,@(32,r1)
	mov	r14,r1
	add	#4,r1
	mov.l	r5,@(28,r1)
	mov	r14,r1
	add	#4,r1
	mov.l	r6,@(24,r1)
	mov	r14,r1
	add	#4,r1
	mov.l	r7,@(20,r1)
	mov	r14,r1
	add	#4,r1
	mov.l	.L845,r2
	mov.l	@r2,r3
	mov.l	r3,@(60,r1)
	mov	#0,r3
	mov	r14,r1
	add	#4,r1
	mov.l	@(24,r1),r4
	mov.l	.L838,r1
	jsr	@r1
	nop
	mov	r0,r1
	mov	r14,r2
	add	#40,r2
	mov.l	@r1,r6
	mov.l	@(4,r1),r7
	mov.l	r6,@r2
	mov.l	r7,@(4,r2)
	add	#8,r1
	mov.l	@r1,r6
	mov.l	@(4,r1),r7
	mov.l	r6,@(8,r2)
	mov.l	r7,@(12,r2)
	add	#8,r1
	mov.l	@r1,r6
	mov.l	@(4,r1),r7
	mov.l	r6,@(16,r2)
	mov.l	r7,@(20,r2)
	add	#8,r1
	mov	r14,r1
	add	#4,r1
	mov.l	@(20,r1),r4
	mov.l	.L839,r1
	jsr	@r1
	nop
	mov	r0,r1
	fmov.s	@r1+,fr13
	fmov.s	@r1,fr12
	add	#-4,r1
	mov	r14,r1
	add	#4,r1
	mov.l	@(28,r1),r9
	mov	r9,r5
	mov	#44,r4
	mov.l	.L840,r1
	jsr	@r1
	nop
	mov	r0,r8
	mov	r14,r1
	add	#40,r1
	mov.l	@r1,r2
	mov.l	@(4,r1),r3
	mov.l	r2,@r15
	mov.l	r3,@(4,r15)
	add	#8,r1
	mov.l	@r1,r2
	mov.l	@(4,r1),r3
	mov.l	r2,@(8,r15)
	mov.l	r3,@(12,r15)
	add	#8,r1
	mov.l	@r1,r2
	mov.l	@(4,r1),r3
	mov.l	r2,@(16,r15)
	mov.l	r3,@(20,r15)
	add	#8,r1
	fmov	fr12,fr4
	fmov	fr13,fr5
	mov	r8,r4
	mov.l	.L841,r1
.LEHB30:
	jsr	@r1
	nop
.LEHE30:
	bra	.L835
	nop
	.align 1
.L834:
	mov	r4,r10
	mov	r9,r5
	mov	r8,r4
	mov.l	.L842,r2
	sts	fpscr,r1
	mov.l	.L843,r3
	or	r3,r1
	lds	r1,fpscr
	jsr	@r2
	nop
	mov	r10,r1
	mov	r1,r4
	mov.l	.L844,r1
.LEHB31:
	jsr	@r1
	nop
.LEHE31:
	.align 1
.L835:
	mov	r14,r1
	add	#4,r1
	mov.l	.L845,r2
	mov.l	@(60,r1),r7
	mov.l	@r2,r3
	cmp/eq	r7,r3
	mov	#0,r7
	mov	#0,r3
	bt	.L836
	mov.l	.L846,r1
	jsr	@r1
	nop
	.align 1
.L836:
	add	#68,r14
	.cfi_def_cfa_offset 28
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 24
	fmov.s	@r15+,fr13
	.cfi_restore 38
	.cfi_def_cfa_offset 20
	fmov.s	@r15+,fr12
	.cfi_restore 37
	.cfi_def_cfa_offset 16
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 12
	mov.l	@r15+,r10
	.cfi_restore 10
	.cfi_def_cfa_offset 8
	mov.l	@r15+,r9
	.cfi_restore 9
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r8
	.cfi_restore 8
	.cfi_def_cfa_offset 0
	rts	
	nop
.L847:
	.align 2
.L845:
	.long	__stack_chk_guard
.L838:
	.long	_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
.L839:
	.long	_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE
.L840:
	.long	_ZnwjPv
.L841:
	.long	_ZN6sphereC1E4vec3d
.L842:
	.long	_ZdlPvS_
.L843:
	.long	524288
.L844:
	.long	_Unwind_Resume
.L846:
	.long	__stack_chk_fail
	.cfi_endproc
.LFE4733:
	.section	.gcc_except_table
.LLSDA4733:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4733-.LLSDACSB4733
.LLSDACSB4733:
	.uleb128 .LEHB30-.LFB4733
	.uleb128 .LEHE30-.LEHB30
	.uleb128 .L834-.LFB4733
	.uleb128 0
	.uleb128 .LEHB31-.LFB4733
	.uleb128 .LEHE31-.LEHB31
	.uleb128 0
	.uleb128 0
.LLSDACSE4733:
	.section	.text._ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_,comdat
	.size	_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_, .-_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_
	.section	.text._ZN9__gnu_cxx16__aligned_bufferI6sphereE7_M_addrEv,"axG",@progbits,_ZN9__gnu_cxx16__aligned_bufferI6sphereE7_M_addrEv,comdat
	.align 1
	.weak	_ZN9__gnu_cxx16__aligned_bufferI6sphereE7_M_addrEv
	.type	_ZN9__gnu_cxx16__aligned_bufferI6sphereE7_M_addrEv, @function
_ZN9__gnu_cxx16__aligned_bufferI6sphereE7_M_addrEv:
.LFB4734:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	add	#-4,r15
	.cfi_def_cfa_offset 8
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-60,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-60,r1
	mov.l	@(60,r1),r1
	mov	r1,r0
	add	#4,r14
	.cfi_def_cfa_offset 4
	mov	r14,r15
	.cfi_def_cfa_register 15
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
	.cfi_endproc
.LFE4734:
	.size	_ZN9__gnu_cxx16__aligned_bufferI6sphereE7_M_addrEv, .-_ZN9__gnu_cxx16__aligned_bufferI6sphereE7_M_addrEv
	.section	.text._ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_,comdat
	.align 1
	.weak	_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_
	.type	_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_, @function
_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_:
.LFB4735:
	.cfi_startproc
	.cfi_personality 0,__gxx_personality_v0
	.cfi_lsda 0xb,.LLSDA4735
	mov.l	r8,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 8, -4
	mov.l	r9,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 9, -8
	mov.l	r10,@-r15
	.cfi_def_cfa_offset 12
	.cfi_offset 10, -12
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 16
	.cfi_offset 14, -16
	fmov.s	fr12,@-r15
	.cfi_def_cfa_offset 20
	.cfi_offset 37, -20
	fmov.s	fr13,@-r15
	.cfi_def_cfa_offset 24
	.cfi_offset 38, -24
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 28
	.cfi_offset 17, -28
	add	#-68,r15
	.cfi_def_cfa_offset 96
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#4,r1
	mov.l	r4,@(32,r1)
	mov	r14,r1
	add	#4,r1
	mov.l	r5,@(28,r1)
	mov	r14,r1
	add	#4,r1
	mov.l	r6,@(24,r1)
	mov	r14,r1
	add	#4,r1
	mov.l	r7,@(20,r1)
	mov	r14,r1
	add	#4,r1
	mov.l	.L864,r2
	mov.l	@r2,r3
	mov.l	r3,@(60,r1)
	mov	#0,r3
	mov	r14,r1
	add	#4,r1
	mov.l	@(24,r1),r4
	mov.l	.L857,r1
	jsr	@r1
	nop
	mov	r0,r1
	mov	r14,r2
	add	#40,r2
	mov.l	@r1,r6
	mov.l	@(4,r1),r7
	mov.l	r6,@r2
	mov.l	r7,@(4,r2)
	add	#8,r1
	mov.l	@r1,r6
	mov.l	@(4,r1),r7
	mov.l	r6,@(8,r2)
	mov.l	r7,@(12,r2)
	add	#8,r1
	mov.l	@r1,r6
	mov.l	@(4,r1),r7
	mov.l	r6,@(16,r2)
	mov.l	r7,@(20,r2)
	add	#8,r1
	mov	r14,r1
	add	#4,r1
	mov.l	@(20,r1),r4
	mov.l	.L858,r1
	jsr	@r1
	nop
	mov	r0,r1
	mov.l	@r1,r2
	lds	r2,fpul
	fsts	fpul,fr1
	flds	fr1,fpul
	float	fpul,dr12
	mov	r14,r1
	add	#4,r1
	mov.l	@(28,r1),r9
	mov	r9,r5
	mov	#44,r4
	mov.l	.L859,r1
	jsr	@r1
	nop
	mov	r0,r8
	mov	r14,r1
	add	#40,r1
	mov.l	@r1,r2
	mov.l	@(4,r1),r3
	mov.l	r2,@r15
	mov.l	r3,@(4,r15)
	add	#8,r1
	mov.l	@r1,r2
	mov.l	@(4,r1),r3
	mov.l	r2,@(8,r15)
	mov.l	r3,@(12,r15)
	add	#8,r1
	mov.l	@r1,r2
	mov.l	@(4,r1),r3
	mov.l	r2,@(16,r15)
	mov.l	r3,@(20,r15)
	add	#8,r1
	fmov	fr12,fr4
	fmov	fr13,fr5
	mov	r8,r4
	mov.l	.L860,r1
.LEHB32:
	jsr	@r1
	nop
.LEHE32:
	bra	.L854
	nop
	.align 1
.L853:
	mov	r4,r10
	mov	r9,r5
	mov	r8,r4
	mov.l	.L861,r2
	sts	fpscr,r1
	mov.l	.L862,r3
	or	r3,r1
	lds	r1,fpscr
	jsr	@r2
	nop
	mov	r10,r1
	mov	r1,r4
	mov.l	.L863,r1
.LEHB33:
	jsr	@r1
	nop
.LEHE33:
	.align 1
.L854:
	mov	r14,r1
	add	#4,r1
	mov.l	.L864,r2
	mov.l	@(60,r1),r3
	mov.l	@r2,r7
	cmp/eq	r3,r7
	mov	#0,r3
	mov	#0,r7
	bt	.L855
	mov.l	.L865,r1
	jsr	@r1
	nop
	.align 1
.L855:
	add	#68,r14
	.cfi_def_cfa_offset 28
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 24
	fmov.s	@r15+,fr13
	.cfi_restore 38
	.cfi_def_cfa_offset 20
	fmov.s	@r15+,fr12
	.cfi_restore 37
	.cfi_def_cfa_offset 16
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 12
	mov.l	@r15+,r10
	.cfi_restore 10
	.cfi_def_cfa_offset 8
	mov.l	@r15+,r9
	.cfi_restore 9
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r8
	.cfi_restore 8
	.cfi_def_cfa_offset 0
	rts	
	nop
.L866:
	.align 2
.L864:
	.long	__stack_chk_guard
.L857:
	.long	_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
.L858:
	.long	_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE
.L859:
	.long	_ZnwjPv
.L860:
	.long	_ZN6sphereC1E4vec3d
.L861:
	.long	_ZdlPvS_
.L862:
	.long	524288
.L863:
	.long	_Unwind_Resume
.L865:
	.long	__stack_chk_fail
	.cfi_endproc
.LFE4735:
	.section	.gcc_except_table
.LLSDA4735:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4735-.LLSDACSB4735
.LLSDACSB4735:
	.uleb128 .LEHB32-.LFB4735
	.uleb128 .LEHE32-.LEHB32
	.uleb128 .L853-.LFB4735
	.uleb128 0
	.uleb128 .LEHB33-.LFB4735
	.uleb128 .LEHE33-.LEHB33
	.uleb128 0
	.uleb128 0
.LLSDACSE4735:
	.section	.text._ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_,comdat
	.size	_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_, .-_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_
	.section	.text._ZNK9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8max_sizeEv,"axG",@progbits,_ZNK9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8max_sizeEv,comdat
	.align 1
	.weak	_ZNK9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8max_sizeEv
	.type	_ZNK9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8max_sizeEv, @function
_ZNK9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8max_sizeEv:
.LFB4736:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	add	#-4,r15
	.cfi_def_cfa_offset 8
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-60,r1
	mov.l	r4,@(60,r1)
	mov.l	.L869,r1
	mov	r1,r0
	add	#4,r14
	.cfi_def_cfa_offset 4
	mov	r14,r15
	.cfi_def_cfa_register 15
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
.L870:
	.align 2
.L869:
	.long	38347922
	.cfi_endproc
.LFE4736:
	.size	_ZNK9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8max_sizeEv, .-_ZNK9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8max_sizeEv
	.weak	_ZTVSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE
	.section	.rodata._ZTVSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE,"aG",@progbits,_ZTVSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align 2
	.type	_ZTVSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTVSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE, 28
_ZTVSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE:
	.long	0
	.long	_ZTISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE
	.long	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.long	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.long	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.long	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.long	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.weak	_ZTVSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE
	.section	.rodata._ZTVSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE,"aG",@progbits,_ZTVSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align 2
	.type	_ZTVSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTVSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE, 28
_ZTVSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE:
	.long	0
	.long	_ZTISt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE
	.long	0
	.long	0
	.long	__cxa_pure_virtual
	.long	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.long	__cxa_pure_virtual
	.weak	_ZTISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE
	.section	.rodata._ZTISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE,"aG",@progbits,_ZTISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align 2
	.type	_ZTISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE, 12
_ZTISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE:
	.long	_ZTVN10__cxxabiv120__si_class_type_infoE+8
	.long	_ZTSSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE
	.long	_ZTISt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE
	.weak	_ZTSSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE
	.section	.rodata._ZTSSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE,"aG",@progbits,_ZTSSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align 2
	.type	_ZTSSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTSSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE, 73
_ZTSSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE:
	.string	"St23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE"
	.weak	_ZTISt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE
	.section	.rodata._ZTISt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE,"aG",@progbits,_ZTISt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align 2
	.type	_ZTISt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTISt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE, 12
_ZTISt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE:
	.long	_ZTVN10__cxxabiv120__si_class_type_infoE+8
	.long	_ZTSSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE
	.long	_ZTISt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE
	.weak	_ZTSSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE
	.section	.rodata._ZTSSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE,"aG",@progbits,_ZTSSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align 2
	.type	_ZTSSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTSSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE, 52
_ZTSSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE:
	.string	"St16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE"
	.text
	.align 1
	.type	_Z41__static_initialization_and_destruction_0ii, @function
_Z41__static_initialization_and_destruction_0ii:
.LFB4753:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 17, -8
	add	#-8,r15
	.cfi_def_cfa_offset 16
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-56,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-56,r1
	mov.l	r5,@(56,r1)
	mov	r14,r1
	add	#-56,r1
	mov.l	@(60,r1),r2
	mov	#1,r1
	cmp/eq	r1,r2
	bf	.L873
	mov	r14,r1
	add	#-56,r1
	mov.l	@(56,r1),r2
	mov.l	.L874,r1
	cmp/eq	r1,r2
	bf	.L873
	mov.l	.L878,r1
	mov	r1,r4
	mov.l	.L876,r1
	jsr	@r1
	nop
	mov.l	.L877,r3
	mov.l	.L878,r2
	mov.l	.L879,r1
	mov	r3,r6
	mov	r2,r5
	mov	r1,r4
	mov.l	.L880,r1
	jsr	@r1
	nop
.L873:
	nop
	add	#8,r14
	.cfi_def_cfa_offset 8
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
.L881:
	.align 2
.L874:
	.long	65535
.L878:
	.long	_ZStL8__ioinit
.L876:
	.long	_ZNSt8ios_base4InitC1Ev
.L877:
	.long	__dso_handle
.L879:
	.long	_ZNSt8ios_base4InitD1Ev
.L880:
	.long	__cxa_atexit
	.cfi_endproc
.LFE4753:
	.size	_Z41__static_initialization_and_destruction_0ii, .-_Z41__static_initialization_and_destruction_0ii
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 1
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.type	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED2Ev:
.LFB4755:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 17, -8
	add	#-4,r15
	.cfi_def_cfa_offset 12
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-60,r1
	mov.l	r4,@(60,r1)
	mov.l	.L883,r2
	mov	r14,r1
	add	#-60,r1
	mov.l	@(60,r1),r1
	mov.l	r2,@r1
	mov	r14,r1
	add	#-60,r1
	mov.l	@(60,r1),r1
	add	#12,r1
	mov	r1,r4
	mov.l	.L884,r1
	jsr	@r1
	nop
	mov	r14,r1
	add	#-60,r1
	mov.l	@(60,r1),r1
	mov	r1,r4
	mov.l	.L885,r1
	jsr	@r1
	nop
	nop
	add	#4,r14
	.cfi_def_cfa_offset 8
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
.L886:
	.align 2
.L883:
	.long	_ZTVSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE+8
.L884:
	.long	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD1Ev
.L885:
	.long	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev
	.cfi_endproc
.LFE4755:
	.size	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.set	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED1Ev,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED0Ev,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 1
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.type	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED0Ev, @function
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED0Ev:
.LFB4757:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 17, -8
	add	#-4,r15
	.cfi_def_cfa_offset 12
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-60,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-60,r1
	mov.l	@(60,r1),r4
	mov.l	.L888,r1
	jsr	@r1
	nop
	mov	r14,r1
	add	#-60,r1
	mov	#56,r5
	mov.l	@(60,r1),r4
	mov.l	.L889,r1
	jsr	@r1
	nop
	add	#4,r14
	.cfi_def_cfa_offset 8
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
.L890:
	.align 2
.L888:
	.long	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED1Ev
.L889:
	.long	_ZdlPvj
	.cfi_endproc
.LFE4757:
	.size	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED0Ev, .-_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,comdat
	.align 1
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.type	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, @function
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv:
.LFB4758:
	.cfi_startproc
	mov.l	r8,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 8, -4
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 14, -8
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 12
	.cfi_offset 17, -12
	add	#-4,r15
	.cfi_def_cfa_offset 16
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-60,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-60,r1
	mov.l	@(60,r1),r1
	add	#12,r1
	mov	r1,r4
	mov.l	.L892,r1
	jsr	@r1
	nop
	mov	r0,r8
	mov	r14,r1
	add	#-60,r1
	mov.l	@(60,r1),r4
	mov.l	.L893,r1
	jsr	@r1
	nop
	mov	r0,r1
	mov	r1,r5
	mov	r8,r4
	mov.l	.L894,r1
	jsr	@r1
	nop
	nop
	add	#4,r14
	.cfi_def_cfa_offset 12
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 8
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r8
	.cfi_restore 8
	.cfi_def_cfa_offset 0
	rts	
	nop
.L895:
	.align 2
.L892:
	.long	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_Impl8_M_allocEv
.L893:
	.long	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv
.L894:
	.long	_ZNSt16allocator_traitsISaI6sphereEE7destroyIS0_EEvRS1_PT_
	.cfi_endproc
.LFE4758:
	.size	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, .-_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,comdat
	.align 1
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.type	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, @function
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv:
.LFB4759:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 17, -8
	add	#-20,r15
	.cfi_def_cfa_offset 28
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-44,r1
	mov.l	r4,@(44,r1)
	mov	r14,r1
	add	#-44,r1
	mov.l	.L906,r2
	mov.l	@r2,r3
	mov.l	r3,@(60,r1)
	mov	#0,r3
	mov	r14,r1
	add	#-44,r1
	mov.l	@(44,r1),r1
	add	#12,r1
	mov	r1,r4
	mov.l	.L900,r1
	jsr	@r1
	nop
	mov	r0,r1
	mov	r14,r2
	add	#7,r2
	mov	r1,r5
	mov	r2,r4
	mov.l	.L901,r1
	jsr	@r1
	nop
	mov	r14,r1
	add	#-44,r1
	mov	r14,r3
	add	#7,r3
	mov	r14,r2
	add	#8,r2
	mov.l	@(44,r1),r6
	mov	r3,r5
	mov	r2,r4
	mov.l	.L902,r1
	jsr	@r1
	nop
	mov	r14,r1
	add	#-44,r1
	mov.l	@(44,r1),r4
	mov.l	.L903,r1
	jsr	@r1
	nop
	mov	r14,r1
	add	#8,r1
	mov	r1,r4
	mov.l	.L904,r1
	jsr	@r1
	nop
	mov	r14,r1
	add	#7,r1
	mov	r1,r4
	mov.l	.L905,r1
	jsr	@r1
	nop
	nop
	mov	r14,r1
	add	#-44,r1
	mov.l	.L906,r2
	mov.l	@(60,r1),r7
	mov.l	@r2,r3
	cmp/eq	r7,r3
	mov	#0,r7
	mov	#0,r3
	bt	.L898
	mov.l	.L907,r1
	jsr	@r1
	nop
	.align 1
.L898:
	add	#20,r14
	.cfi_def_cfa_offset 8
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
.L908:
	.align 2
.L906:
	.long	__stack_chk_guard
.L900:
	.long	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_Impl8_M_allocEv
.L901:
	.long	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC1IS0_EERKSaIT_E
.L902:
	.long	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC1ERS6_PS5_
.L903:
	.long	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED1Ev
.L904:
	.long	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED1Ev
.L905:
	.long	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED1Ev
.L907:
	.long	__stack_chk_fail
	.cfi_endproc
.LFE4759:
	.size	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, .-_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,comdat
	.align 1
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.type	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, @function
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info:
.LFB4760:
	.cfi_startproc
	mov.l	r8,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 8, -4
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 14, -8
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 12
	.cfi_offset 17, -12
	add	#-12,r15
	.cfi_def_cfa_offset 24
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-52,r1
	mov.l	r4,@(56,r1)
	mov	r14,r1
	add	#-52,r1
	mov.l	r5,@(52,r1)
	mov	r14,r8
	add	#-52,r8
	mov	r14,r1
	add	#-52,r1
	mov.l	@(56,r1),r4
	mov.l	.L915,r1
	jsr	@r1
	nop
	mov	r0,r1
	mov.l	r1,@(60,r8)
	mov.l	.L916,r1
	jsr	@r1
	nop
	mov	r0,r1
	mov	r14,r2
	add	#-52,r2
	mov.l	@(52,r2),r2
	cmp/eq	r1,r2
	bt	.L910
	mov.l	.L917,r2
	mov	r14,r1
	add	#-52,r1
	mov	r2,r5
	mov.l	@(52,r1),r4
	mov.l	.L918,r1
	jsr	@r1
	nop
	mov	r0,r1
	tst	r1,r1
	bt	.L911
.L910:
	mov	#1,r1
	bra	.L912
	nop
	.align 1
.L911:
	mov	#0,r1
.L912:
	tst	r1,r1
	bt	.L913
	mov	r14,r1
	add	#-52,r1
	mov.l	@(60,r1),r1
	bra	.L914
	nop
	.align 1
.L913:
	mov	#0,r1
.L914:
	mov	r1,r0
	add	#12,r14
	.cfi_def_cfa_offset 12
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 8
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r8
	.cfi_restore 8
	.cfi_def_cfa_offset 0
	rts	
	nop
.L919:
	.align 2
.L915:
	.long	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv
.L916:
	.long	_ZNSt19_Sp_make_shared_tag5_S_tiEv
.L917:
	.long	_ZTISt19_Sp_make_shared_tag
.L918:
	.long	_ZNKSt9type_infoeqERKS_
	.cfi_endproc
.LFE4760:
	.size	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, .-_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_Impl8_M_allocEv,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_Impl8_M_allocEv,comdat
	.align 1
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_Impl8_M_allocEv
	.type	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_Impl8_M_allocEv, @function
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_Impl8_M_allocEv:
.LFB4761:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 17, -8
	add	#-4,r15
	.cfi_def_cfa_offset 12
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-60,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-60,r1
	mov.l	@(60,r1),r4
	mov.l	.L922,r1
	jsr	@r1
	nop
	mov	r0,r1
	mov	r1,r0
	add	#4,r14
	.cfi_def_cfa_offset 8
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
.L923:
	.align 2
.L922:
	.long	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EE6_S_getERS2_
	.cfi_endproc
.LFE4761:
	.size	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_Impl8_M_allocEv, .-_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_Impl8_M_allocEv
	.section	.text._ZNSt16allocator_traitsISaI6sphereEE7destroyIS0_EEvRS1_PT_,"axG",@progbits,_ZNSt16allocator_traitsISaI6sphereEE7destroyIS0_EEvRS1_PT_,comdat
	.align 1
	.weak	_ZNSt16allocator_traitsISaI6sphereEE7destroyIS0_EEvRS1_PT_
	.type	_ZNSt16allocator_traitsISaI6sphereEE7destroyIS0_EEvRS1_PT_, @function
_ZNSt16allocator_traitsISaI6sphereEE7destroyIS0_EEvRS1_PT_:
.LFB4762:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 17, -8
	add	#-8,r15
	.cfi_def_cfa_offset 16
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-56,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-56,r1
	mov.l	r5,@(56,r1)
	mov	r14,r2
	add	#-56,r2
	mov	r14,r1
	add	#-56,r1
	mov.l	@(56,r2),r5
	mov.l	@(60,r1),r4
	mov.l	.L925,r1
	jsr	@r1
	nop
	nop
	add	#8,r14
	.cfi_def_cfa_offset 8
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
.L926:
	.align 2
.L925:
	.long	_ZN9__gnu_cxx13new_allocatorI6sphereE7destroyIS1_EEvPT_
	.cfi_endproc
.LFE4762:
	.size	_ZNSt16allocator_traitsISaI6sphereEE7destroyIS0_EEvRS1_PT_, .-_ZNSt16allocator_traitsISaI6sphereEE7destroyIS0_EEvRS1_PT_
	.section	.text._ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EE6_S_getERS2_,"axG",@progbits,_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EE6_S_getERS2_,comdat
	.align 1
	.weak	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EE6_S_getERS2_
	.type	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EE6_S_getERS2_, @function
_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EE6_S_getERS2_:
.LFB4763:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	add	#-4,r15
	.cfi_def_cfa_offset 8
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-60,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-60,r1
	mov.l	@(60,r1),r1
	mov	r1,r0
	add	#4,r14
	.cfi_def_cfa_offset 4
	mov	r14,r15
	.cfi_def_cfa_register 15
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
	.cfi_endproc
.LFE4763:
	.size	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EE6_S_getERS2_, .-_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EE6_S_getERS2_
	.section	.text._ZN6sphereD2Ev,"axG",@progbits,_ZN6sphereD5Ev,comdat
	.align 1
	.weak	_ZN6sphereD2Ev
	.type	_ZN6sphereD2Ev, @function
_ZN6sphereD2Ev:
.LFB4766:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 17, -8
	add	#-4,r15
	.cfi_def_cfa_offset 12
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-60,r1
	mov.l	r4,@(60,r1)
	mov.l	.L930,r2
	mov	r14,r1
	add	#-60,r1
	mov.l	@(60,r1),r1
	mov.l	r2,@r1
	mov	r14,r1
	add	#-60,r1
	mov.l	@(60,r1),r1
	add	#36,r1
	mov	r1,r4
	mov.l	.L931,r1
	jsr	@r1
	nop
	nop
	add	#4,r14
	.cfi_def_cfa_offset 8
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
.L932:
	.align 2
.L930:
	.long	_ZTV6sphere+8
.L931:
	.long	_ZNSt10shared_ptrI8materialED1Ev
	.cfi_endproc
.LFE4766:
	.size	_ZN6sphereD2Ev, .-_ZN6sphereD2Ev
	.weak	_ZN6sphereD1Ev
	.set	_ZN6sphereD1Ev,_ZN6sphereD2Ev
	.section	.text._ZN9__gnu_cxx13new_allocatorI6sphereE7destroyIS1_EEvPT_,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorI6sphereE7destroyIS1_EEvPT_,comdat
	.align 1
	.weak	_ZN9__gnu_cxx13new_allocatorI6sphereE7destroyIS1_EEvPT_
	.type	_ZN9__gnu_cxx13new_allocatorI6sphereE7destroyIS1_EEvPT_, @function
_ZN9__gnu_cxx13new_allocatorI6sphereE7destroyIS1_EEvPT_:
.LFB4764:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 17, -8
	add	#-8,r15
	.cfi_def_cfa_offset 16
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-56,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-56,r1
	mov.l	r5,@(56,r1)
	mov	r14,r1
	add	#-56,r1
	mov.l	@(56,r1),r4
	mov.l	.L934,r1
	jsr	@r1
	nop
	nop
	add	#8,r14
	.cfi_def_cfa_offset 8
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
.L935:
	.align 2
.L934:
	.long	_ZN6sphereD1Ev
	.cfi_endproc
.LFE4764:
	.size	_ZN9__gnu_cxx13new_allocatorI6sphereE7destroyIS1_EEvPT_, .-_ZN9__gnu_cxx13new_allocatorI6sphereE7destroyIS1_EEvPT_
	.weak	_ZTISt19_Sp_make_shared_tag
	.section	.rodata._ZTISt19_Sp_make_shared_tag,"aG",@progbits,_ZTISt19_Sp_make_shared_tag,comdat
	.align 2
	.type	_ZTISt19_Sp_make_shared_tag, @object
	.size	_ZTISt19_Sp_make_shared_tag, 8
_ZTISt19_Sp_make_shared_tag:
	.long	_ZTVN10__cxxabiv117__class_type_infoE+8
	.long	_ZTSSt19_Sp_make_shared_tag
	.weak	_ZTSSt19_Sp_make_shared_tag
	.section	.rodata._ZTSSt19_Sp_make_shared_tag,"aG",@progbits,_ZTSSt19_Sp_make_shared_tag,comdat
	.align 2
	.type	_ZTSSt19_Sp_make_shared_tag, @object
	.size	_ZTSSt19_Sp_make_shared_tag, 24
_ZTSSt19_Sp_make_shared_tag:
	.string	"St19_Sp_make_shared_tag"
	.weak	_ZTISt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE
	.section	.rodata._ZTISt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE,"aG",@progbits,_ZTISt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align 2
	.type	_ZTISt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTISt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE, 8
_ZTISt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE:
	.long	_ZTVN10__cxxabiv117__class_type_infoE+8
	.long	_ZTSSt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE
	.weak	_ZTSSt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE
	.section	.rodata._ZTSSt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE,"aG",@progbits,_ZTSSt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align 2
	.type	_ZTSSt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTSSt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE, 47
_ZTSSt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE:
	.string	"St11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE"
	.text
	.align 1
	.type	_GLOBAL__sub_I_main, @function
_GLOBAL__sub_I_main:
.LFB4768:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 17, -8
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov.l	.L937,r1
	mov	r1,r5
	mov	#1,r4
	mov.l	.L938,r1
	jsr	@r1
	nop
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
.L939:
	.align 2
.L937:
	.long	65535
.L938:
	.long	_Z41__static_initialization_and_destruction_0ii
	.cfi_endproc
.LFE4768:
	.size	_GLOBAL__sub_I_main, .-_GLOBAL__sub_I_main
	.section	.ctors,"aw",@progbits
	.align 2
	.long	_GLOBAL__sub_I_main
	.weakref	_ZL28__gthrw___pthread_key_createPjPFvPvE,__pthread_key_create
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.4.0-1ubuntu1~20.04) 9.4.0"
	.section	.note.GNU-stack,"",@progbits
