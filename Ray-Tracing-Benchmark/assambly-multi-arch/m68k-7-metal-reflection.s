#NO_APP
	.file	"7-metal-reflection.cpp"
	.text
	.section	.rodata
	.type	_ZStL19piecewise_construct, @object
	.size	_ZStL19piecewise_construct, 1
_ZStL19piecewise_construct:
	.zero	1
	.section	.text._ZNKSt9type_infoeqERKS_,"axG",@progbits,_ZNKSt9type_infoeqERKS_,comdat
	.align	2
	.weak	_ZNKSt9type_infoeqERKS_
	.type	_ZNKSt9type_infoeqERKS_, @function
_ZNKSt9type_infoeqERKS_:
.LFB730:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l 8(%fp),%a0
	move.l 4(%a0),%d1
	move.l 12(%fp),%a0
	move.l 4(%a0),%d0
	cmp.l %d1,%d0
	jeq .L2
	move.l 8(%fp),%a0
	move.l 4(%a0),%a0
	move.b (%a0),%d0
	cmp.b #42,%d0
	jeq .L3
	move.l 12(%fp),%a0
	move.l 4(%a0),%d1
	move.l 8(%fp),%a0
	move.l 4(%a0),%d0
	move.l %d1,-(%sp)
	move.l %d0,-(%sp)
	jsr strcmp
	addq.l #8,%sp
	tst.l %d0
	jne .L3
.L2:
	moveq #1,%d0
	jra .L4
.L3:
	clr.b %d0
.L4:
	unlk %fp
	rts
	.cfi_endproc
.LFE730:
	.size	_ZNKSt9type_infoeqERKS_, .-_ZNKSt9type_infoeqERKS_
	.section	.text._ZnwjPv,"axG",@progbits,_ZnwjPv,comdat
	.align	2
	.weak	_ZnwjPv
	.type	_ZnwjPv, @function
_ZnwjPv:
.LFB772:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l 12(%fp),%d0
	move.l %d0,%d1
	move.l %d1,%a0
	unlk %fp
	rts
	.cfi_endproc
.LFE772:
	.size	_ZnwjPv, .-_ZnwjPv
	.section	.text._ZdlPvS_,"axG",@progbits,_ZdlPvS_,comdat
	.align	2
	.weak	_ZdlPvS_
	.type	_ZdlPvS_, @function
_ZdlPvS_:
.LFB774:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	nop
	unlk %fp
	rts
	.cfi_endproc
.LFE774:
	.size	_ZdlPvS_, .-_ZdlPvS_
	.section	.rodata
	.align	2
	.type	_ZZL18__gthread_active_pvE20__gthread_active_ptr, @object
	.size	_ZZL18__gthread_active_pvE20__gthread_active_ptr, 4
_ZZL18__gthread_active_pvE20__gthread_active_ptr:
	.long	_ZL28__gthrw___pthread_key_createPjPFvPvE
	.section	.text._ZL18__gthread_active_pv,"axG",@progbits,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv,comdat
	.align	2
	.type	_ZL18__gthread_active_pv, @function
_ZL18__gthread_active_pv:
.LFB948:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	moveq #1,%d1
	move.l #_ZL28__gthrw___pthread_key_createPjPFvPvE,%d0
	tst.l %d0
	jne .L10
	clr.b %d1
.L10:
	clr.l %d0
	move.b %d1,%d0
	unlk %fp
	rts
	.cfi_endproc
.LFE948:
	.size	_ZL18__gthread_active_pv, .-_ZL18__gthread_active_pv
	.section	.text._ZN9__gnu_cxxL18__exchange_and_addEPVii,"axG",@progbits,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv,comdat
	.align	2
	.type	_ZN9__gnu_cxxL18__exchange_and_addEPVii, @function
_ZN9__gnu_cxxL18__exchange_and_addEPVii:
.LFB977:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l %d3,-(%sp)
	move.l %d2,-(%sp)
	.cfi_offset 3, -12
	.cfi_offset 2, -16
	move.l 12(%fp),%d2
	move.l 8(%fp),%a0
	move.l (%a0),%d1
.L13:
	move.l %d1,%d0
	move.l %d0,%a1
	move.l %d0,%d1
	add.l %d2,%d1
	move.l %d0,%d3
	cas.l %d3,%d1,(%a0)
	seq %d0
	move.l %d3,%d1
	neg.b %d0
	tst.b %d0
	jeq .L13
	move.l %a1,%d0
	move.l (%sp)+,%d2
	move.l (%sp)+,%d3
	unlk %fp
	rts
	.cfi_endproc
.LFE977:
	.size	_ZN9__gnu_cxxL18__exchange_and_addEPVii, .-_ZN9__gnu_cxxL18__exchange_and_addEPVii
	.section	.text._ZN9__gnu_cxxL25__exchange_and_add_singleEPii,"axG",@progbits,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv,comdat
	.align	2
	.type	_ZN9__gnu_cxxL25__exchange_and_add_singleEPii, @function
_ZN9__gnu_cxxL25__exchange_and_add_singleEPii:
.LFB979:
	.cfi_startproc
	link.w %fp,#-4
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l 8(%fp),%a0
	move.l (%a0),-4(%fp)
	move.l 8(%fp),%a0
	move.l (%a0),%d0
	add.l 12(%fp),%d0
	move.l 8(%fp),%a0
	move.l %d0,(%a0)
	move.l -4(%fp),%d0
	unlk %fp
	rts
	.cfi_endproc
.LFE979:
	.size	_ZN9__gnu_cxxL25__exchange_and_add_singleEPii, .-_ZN9__gnu_cxxL25__exchange_and_add_singleEPii
	.section	.text._ZN9__gnu_cxxL27__exchange_and_add_dispatchEPii,"axG",@progbits,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv,comdat
	.align	2
	.type	_ZN9__gnu_cxxL27__exchange_and_add_dispatchEPii, @function
_ZN9__gnu_cxxL27__exchange_and_add_dispatchEPii:
.LFB981:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	jsr _ZL18__gthread_active_pv
	tst.l %d0
	sne %d0
	neg.b %d0
	tst.b %d0
	jeq .L18
	move.l 12(%fp),-(%sp)
	move.l 8(%fp),-(%sp)
	jsr _ZN9__gnu_cxxL18__exchange_and_addEPVii
	addq.l #8,%sp
	jra .L19
.L18:
	move.l 12(%fp),-(%sp)
	move.l 8(%fp),-(%sp)
	jsr _ZN9__gnu_cxxL25__exchange_and_add_singleEPii
	addq.l #8,%sp
	nop
.L19:
	unlk %fp
	rts
	.cfi_endproc
.LFE981:
	.size	_ZN9__gnu_cxxL27__exchange_and_add_dispatchEPii, .-_ZN9__gnu_cxxL27__exchange_and_add_dispatchEPii
	.section	.rodata
	.align	2
	.type	_ZN9__gnu_cxxL21__default_lock_policyE, @object
	.size	_ZN9__gnu_cxxL21__default_lock_policyE, 4
_ZN9__gnu_cxxL21__default_lock_policyE:
	.long	2
	.type	_ZStL13allocator_arg, @object
	.size	_ZStL13allocator_arg, 1
_ZStL13allocator_arg:
	.zero	1
	.type	_ZStL6ignore, @object
	.size	_ZStL6ignore, 1
_ZStL6ignore:
	.zero	1
	.weak	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag
	.section	.rodata._ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag,"aG",@progbits,_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag,comdat
	.align	2
	.type	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag, @gnu_unique_object
	.size	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag, 8
_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag:
	.zero	8
	.section	.text._ZNSt19_Sp_make_shared_tag5_S_tiEv,"axG",@progbits,_ZNSt19_Sp_make_shared_tag5_S_tiEv,comdat
	.align	2
	.weak	_ZNSt19_Sp_make_shared_tag5_S_tiEv
	.type	_ZNSt19_Sp_make_shared_tag5_S_tiEv, @function
_ZNSt19_Sp_make_shared_tag5_S_tiEv:
.LFB1945:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l #_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag,%d0
	move.l %d0,%d1
	move.l %d1,%a0
	unlk %fp
	rts
	.cfi_endproc
.LFE1945:
	.size	_ZNSt19_Sp_make_shared_tag5_S_tiEv, .-_ZNSt19_Sp_make_shared_tag5_S_tiEv
	.section	.rodata
	.align	2
	.type	_ZL2pi, @object
	.size	_ZL2pi, 8
_ZL2pi:
	.long	1074340347
	.long	1413754136
	.weak	_ZZ13random_doublevE12distribution
	.section	.bss._ZZ13random_doublevE12distribution,"awG",@nobits,_ZZ13random_doublevE12distribution,comdat
	.align	2
	.type	_ZZ13random_doublevE12distribution, @gnu_unique_object
	.size	_ZZ13random_doublevE12distribution, 16
_ZZ13random_doublevE12distribution:
	.zero	16
	.weak	_ZGVZ13random_doublevE12distribution
	.section	.bss._ZGVZ13random_doublevE12distribution,"awG",@nobits,_ZGVZ13random_doublevE12distribution,comdat
	.align	2
	.type	_ZGVZ13random_doublevE12distribution, @gnu_unique_object
	.size	_ZGVZ13random_doublevE12distribution, 8
_ZGVZ13random_doublevE12distribution:
	.zero	8
	.weak	_ZZ13random_doublevE9generator
	.section	.bss._ZZ13random_doublevE9generator,"awG",@nobits,_ZZ13random_doublevE9generator,comdat
	.align	2
	.type	_ZZ13random_doublevE9generator, @gnu_unique_object
	.size	_ZZ13random_doublevE9generator, 2500
_ZZ13random_doublevE9generator:
	.zero	2500
	.weak	_ZGVZ13random_doublevE9generator
	.section	.bss._ZGVZ13random_doublevE9generator,"awG",@nobits,_ZGVZ13random_doublevE9generator,comdat
	.align	2
	.type	_ZGVZ13random_doublevE9generator, @gnu_unique_object
	.size	_ZGVZ13random_doublevE9generator, 8
_ZGVZ13random_doublevE9generator:
	.zero	8
	.section	.text._Z13random_doublev,"axG",@progbits,_Z13random_doublev,comdat
	.align	2
	.weak	_Z13random_doublev
	.type	_Z13random_doublev, @function
_Z13random_doublev:
.LFB3291:
	.cfi_startproc
	.cfi_personality 0,__gxx_personality_v0
	.cfi_lsda 0,.LLSDA3291
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l %d3,-(%sp)
	move.l %d2,-(%sp)
	.cfi_offset 3, -12
	.cfi_offset 2, -16
	move.b _ZGVZ13random_doublevE12distribution,%d0
	tst.b %d0
	seq %d0
	neg.b %d0
	tst.b %d0
	jeq .L23
	pea _ZGVZ13random_doublevE12distribution
	jsr __cxa_guard_acquire
	addq.l #4,%sp
	tst.l %d0
	sne %d0
	neg.b %d0
	tst.b %d0
	jeq .L23
	clr.b %d3
	clr.l -(%sp)
	move.l #1072693248,-(%sp)
	clr.l -(%sp)
	clr.l -(%sp)
	pea _ZZ13random_doublevE12distribution
.LEHB0:
	.cfi_escape 0x2e,0x14
	jsr _ZNSt25uniform_real_distributionIdEC1Edd
.LEHE0:
	lea (20,%sp),%sp
	pea _ZGVZ13random_doublevE12distribution
	jsr __cxa_guard_release
	addq.l #4,%sp
.L23:
	move.b _ZGVZ13random_doublevE9generator,%d0
	tst.b %d0
	seq %d0
	neg.b %d0
	tst.b %d0
	jeq .L24
	pea _ZGVZ13random_doublevE9generator
	jsr __cxa_guard_acquire
	addq.l #4,%sp
	tst.l %d0
	sne %d0
	neg.b %d0
	tst.b %d0
	jeq .L24
	clr.b %d3
	pea _ZZ13random_doublevE9generator
.LEHB1:
	.cfi_escape 0x2e,0x4
	jsr _ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC1Ev
.LEHE1:
	addq.l #4,%sp
	pea _ZGVZ13random_doublevE9generator
	jsr __cxa_guard_release
	addq.l #4,%sp
.L24:
	pea _ZZ13random_doublevE9generator
	pea _ZZ13random_doublevE12distribution
.LEHB2:
	jsr _ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEEEdRT_
	addq.l #8,%sp
	jra .L32
.L30:
	move.l %d0,%d2
	tst.b %d3
	jne .L27
	pea _ZGVZ13random_doublevE12distribution
	jsr __cxa_guard_abort
	addq.l #4,%sp
.L27:
	move.l %d2,%d0
	move.l %d0,-(%sp)
	jsr _Unwind_Resume
.L31:
	move.l %d0,%d2
	tst.b %d3
	jne .L29
	pea _ZGVZ13random_doublevE9generator
	jsr __cxa_guard_abort
	addq.l #4,%sp
.L29:
	move.l %d2,%d0
	move.l %d0,-(%sp)
	jsr _Unwind_Resume
.LEHE2:
.L32:
	move.l -8(%fp),%d2
	move.l -4(%fp),%d3
	unlk %fp
	rts
	.cfi_endproc
.LFE3291:
	.globl	__gxx_personality_v0
	.section	.gcc_except_table._Z13random_doublev,"aG",@progbits,_Z13random_doublev,comdat
.LLSDA3291:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE3291-.LLSDACSB3291
.LLSDACSB3291:
	.uleb128 .LEHB0-.LFB3291
	.uleb128 .LEHE0-.LEHB0
	.uleb128 .L30-.LFB3291
	.uleb128 0
	.uleb128 .LEHB1-.LFB3291
	.uleb128 .LEHE1-.LEHB1
	.uleb128 .L31-.LFB3291
	.uleb128 0
	.uleb128 .LEHB2-.LFB3291
	.uleb128 .LEHE2-.LEHB2
	.uleb128 0
	.uleb128 0
.LLSDACSE3291:
	.section	.text._Z13random_doublev,"axG",@progbits,_Z13random_doublev,comdat
	.size	_Z13random_doublev, .-_Z13random_doublev
	.section	.text._ZplRK4vec3S1_,"axG",@progbits,_ZplRK4vec3S1_,comdat
	.align	2
	.weak	_ZplRK4vec3S1_
	.type	_ZplRK4vec3S1_, @function
_ZplRK4vec3S1_:
.LFB3927:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	fmovem #12,-(%sp)
	move.l %d2,-(%sp)
	.cfi_offset 18, -32
	.cfi_offset 19, -20
	.cfi_offset 2, -36
	move.l %a1,%d2
	move.l 8(%fp),%a0
	fmove.d 16(%a0),%fp1
	move.l 12(%fp),%a0
	fmove.d 16(%a0),%fp0
	fmove.x %fp1,%fp2
	fadd.x %fp0,%fp2
	move.l 8(%fp),%a0
	fmove.d 8(%a0),%fp1
	move.l 12(%fp),%a0
	fmove.d 8(%a0),%fp0
	fadd.x %fp0,%fp1
	move.l 8(%fp),%a0
	fmove.d (%a0),%fp3
	move.l 12(%fp),%a0
	fmove.d (%a0),%fp0
	fadd.x %fp3,%fp0
	fmove.d %fp2,-(%sp)
	fmove.d %fp1,-(%sp)
	fmove.d %fp0,-(%sp)
	move.l %d2,-(%sp)
	jsr _ZN4vec3C1Eddd
	lea (28,%sp),%sp
	move.l %d2,%a0
	move.l -28(%fp),%d2
	fmovem -24(%fp),#48
	unlk %fp
	rts
	.cfi_endproc
.LFE3927:
	.size	_ZplRK4vec3S1_, .-_ZplRK4vec3S1_
	.section	.text._ZmiRK4vec3S1_,"axG",@progbits,_ZmiRK4vec3S1_,comdat
	.align	2
	.weak	_ZmiRK4vec3S1_
	.type	_ZmiRK4vec3S1_, @function
_ZmiRK4vec3S1_:
.LFB3928:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	fmovem #12,-(%sp)
	move.l %d2,-(%sp)
	.cfi_offset 18, -32
	.cfi_offset 19, -20
	.cfi_offset 2, -36
	move.l %a1,%d2
	move.l 8(%fp),%a0
	fmove.d 16(%a0),%fp0
	move.l 12(%fp),%a0
	fmove.d 16(%a0),%fp1
	fmove.x %fp0,%fp2
	fsub.x %fp1,%fp2
	move.l 8(%fp),%a0
	fmove.d 8(%a0),%fp0
	move.l 12(%fp),%a0
	fmove.d 8(%a0),%fp1
	fmove.x %fp0,%fp3
	fsub.x %fp1,%fp3
	fmove.x %fp3,%fp1
	move.l 8(%fp),%a0
	fmove.d (%a0),%fp0
	move.l 12(%fp),%a0
	fmove.d (%a0),%fp3
	fsub.x %fp3,%fp0
	fmove.d %fp2,-(%sp)
	fmove.d %fp1,-(%sp)
	fmove.d %fp0,-(%sp)
	move.l %d2,-(%sp)
	jsr _ZN4vec3C1Eddd
	lea (28,%sp),%sp
	move.l %d2,%a0
	move.l -28(%fp),%d2
	fmovem -24(%fp),#48
	unlk %fp
	rts
	.cfi_endproc
.LFE3928:
	.size	_ZmiRK4vec3S1_, .-_ZmiRK4vec3S1_
	.section	.text._ZmldRK4vec3,"axG",@progbits,_ZmldRK4vec3,comdat
	.align	2
	.weak	_ZmldRK4vec3
	.type	_ZmldRK4vec3, @function
_ZmldRK4vec3:
.LFB3930:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	fmovem #4,-(%sp)
	move.l %d2,-(%sp)
	.cfi_offset 18, -20
	.cfi_offset 2, -24
	move.l %a1,%d2
	move.l 16(%fp),%a0
	fmove.d 16(%a0),%fp0
	fmove.x %fp0,%fp2
	fmul.d 8(%fp),%fp2
	move.l 16(%fp),%a0
	fmove.d 8(%a0),%fp0
	fmove.x %fp0,%fp1
	fmul.d 8(%fp),%fp1
	move.l 16(%fp),%a0
	fmove.d (%a0),%fp0
	fmul.d 8(%fp),%fp0
	fmove.d %fp2,-(%sp)
	fmove.d %fp1,-(%sp)
	fmove.d %fp0,-(%sp)
	move.l %d2,-(%sp)
	jsr _ZN4vec3C1Eddd
	lea (28,%sp),%sp
	move.l %d2,%a0
	move.l -16(%fp),%d2
	fmovem -12(%fp),#32
	unlk %fp
	rts
	.cfi_endproc
.LFE3930:
	.size	_ZmldRK4vec3, .-_ZmldRK4vec3
	.section	.text._Zdv4vec3d,"axG",@progbits,_Zdv4vec3d,comdat
	.align	2
	.weak	_Zdv4vec3d
	.type	_Zdv4vec3d, @function
_Zdv4vec3d:
.LFB3932:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l %d2,-(%sp)
	.cfi_offset 2, -12
	move.l %a1,%d2
	fmovecr #0x32,%fp0
	fdiv.d 32(%fp),%fp0
	move.l %fp,%d0
	addq.l #8,%d0
	move.l %d0,-(%sp)
	fmove.d %fp0,-(%sp)
	move.l %d2,%a1
	jsr _ZmldRK4vec3
	lea (12,%sp),%sp
	move.l %d2,%a0
	move.l -4(%fp),%d2
	unlk %fp
	rts
	.cfi_endproc
.LFE3932:
	.size	_Zdv4vec3d, .-_Zdv4vec3d
	.section	.text._Z11unit_vector4vec3,"axG",@progbits,_Z11unit_vector4vec3,comdat
	.align	2
	.weak	_Z11unit_vector4vec3
	.type	_Z11unit_vector4vec3, @function
_Z11unit_vector4vec3:
.LFB3935:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l %d2,-(%sp)
	.cfi_offset 2, -12
	move.l %a1,%d2
	move.l %fp,%d0
	addq.l #8,%d0
	move.l %d0,-(%sp)
	jsr _ZNK4vec36lengthEv
	addq.l #4,%sp
	fmove.d %fp0,-(%sp)
	lea (32,%fp),%a0
	subq.l #4,%a0
	move.l (%a0),-(%sp)
	subq.l #4,%a0
	move.l (%a0),-(%sp)
	subq.l #4,%a0
	move.l (%a0),-(%sp)
	subq.l #4,%a0
	move.l (%a0),-(%sp)
	subq.l #4,%a0
	move.l (%a0),-(%sp)
	subq.l #4,%a0
	move.l (%a0),-(%sp)
	move.l %d2,%a1
	jsr _Zdv4vec3d
	lea (32,%sp),%sp
	move.l %d2,%a0
	move.l -4(%fp),%d2
	unlk %fp
	rts
	.cfi_endproc
.LFE3935:
	.size	_Z11unit_vector4vec3, .-_Z11unit_vector4vec3
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.section	.text._ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align	2
	.weak	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED2Ev
	.type	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED2Ev:
.LFB3941:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l 8(%fp),%d0
	addq.l #4,%d0
	move.l %d0,-(%sp)
	jsr _ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED1Ev
	addq.l #4,%sp
	nop
	unlk %fp
	rts
	.cfi_endproc
.LFE3941:
	.size	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED1Ev
	.set	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED1Ev,_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt10shared_ptrI8materialED2Ev,"axG",@progbits,_ZNSt10shared_ptrI8materialED5Ev,comdat
	.align	2
	.weak	_ZNSt10shared_ptrI8materialED2Ev
	.type	_ZNSt10shared_ptrI8materialED2Ev, @function
_ZNSt10shared_ptrI8materialED2Ev:
.LFB3943:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l 8(%fp),%d0
	move.l %d0,-(%sp)
	jsr _ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED2Ev
	addq.l #4,%sp
	nop
	unlk %fp
	rts
	.cfi_endproc
.LFE3943:
	.size	_ZNSt10shared_ptrI8materialED2Ev, .-_ZNSt10shared_ptrI8materialED2Ev
	.weak	_ZNSt10shared_ptrI8materialED1Ev
	.set	_ZNSt10shared_ptrI8materialED1Ev,_ZNSt10shared_ptrI8materialED2Ev
	.section	.text._ZN10hit_recordC2Ev,"axG",@progbits,_ZN10hit_recordC5Ev,comdat
	.align	2
	.weak	_ZN10hit_recordC2Ev
	.type	_ZN10hit_recordC2Ev, @function
_ZN10hit_recordC2Ev:
.LFB3945:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l 8(%fp),%d0
	move.l %d0,-(%sp)
	jsr _ZN4vec3C1Ev
	addq.l #4,%sp
	moveq #24,%d0
	add.l 8(%fp),%d0
	move.l %d0,-(%sp)
	jsr _ZN4vec3C1Ev
	addq.l #4,%sp
	moveq #48,%d0
	add.l 8(%fp),%d0
	move.l %d0,-(%sp)
	jsr _ZNSt10shared_ptrI8materialEC1Ev
	addq.l #4,%sp
	nop
	unlk %fp
	rts
	.cfi_endproc
.LFE3945:
	.size	_ZN10hit_recordC2Ev, .-_ZN10hit_recordC2Ev
	.weak	_ZN10hit_recordC1Ev
	.set	_ZN10hit_recordC1Ev,_ZN10hit_recordC2Ev
	.section	.text._ZN10hit_recordD2Ev,"axG",@progbits,_ZN10hit_recordD5Ev,comdat
	.align	2
	.weak	_ZN10hit_recordD2Ev
	.type	_ZN10hit_recordD2Ev, @function
_ZN10hit_recordD2Ev:
.LFB3948:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	moveq #48,%d0
	add.l 8(%fp),%d0
	move.l %d0,-(%sp)
	jsr _ZNSt10shared_ptrI8materialED1Ev
	addq.l #4,%sp
	nop
	unlk %fp
	rts
	.cfi_endproc
.LFE3948:
	.size	_ZN10hit_recordD2Ev, .-_ZN10hit_recordD2Ev
	.weak	_ZN10hit_recordD1Ev
	.set	_ZN10hit_recordD1Ev,_ZN10hit_recordD2Ev
	.text
	.align	2
	.type	_ZL9ray_colorRK3rayRK8hittablei, @function
_ZL9ray_colorRK3rayRK8hittablei:
.LFB3937:
	.cfi_startproc
	.cfi_personality 0,__gxx_personality_v0
	.cfi_lsda 0,.LLSDA3937
	link.w %fp,#-396
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l %d3,-(%sp)
	move.l %d2,-(%sp)
	.cfi_offset 3, -408
	.cfi_offset 2, -412
	move.l %a1,%d2
	move.l %fp,%d0
	add.l #-346,%d0
	move.l %d0,-(%sp)
.LEHB3:
	jsr _ZN10hit_recordC1Ev
.LEHE3:
	addq.l #4,%sp
	tst.l 16(%fp)
	jgt .L48
	clr.l -(%sp)
	clr.l -(%sp)
	clr.l -(%sp)
	clr.l -(%sp)
	clr.l -(%sp)
	clr.l -(%sp)
	move.l %d2,-(%sp)
.LEHB4:
	.cfi_escape 0x2e,0x1c
	jsr _ZN4vec3C1Eddd
	lea (28,%sp),%sp
	jra .L49
.L48:
	move.l #2146435072,-8(%fp)
	clr.l -4(%fp)
	move.l 12(%fp),%a0
	move.l (%a0),%a0
	move.l (%a0),%a0
	move.l %fp,%d0
	add.l #-346,%d0
	move.l %d0,-(%sp)
	move.l -4(%fp),-(%sp)
	move.l -8(%fp),-(%sp)
	move.l #-755914244,-(%sp)
	move.l #1062232653,-(%sp)
	move.l 8(%fp),-(%sp)
	move.l 12(%fp),-(%sp)
	jsr (%a0)
	lea (28,%sp),%sp
	tst.b %d0
	jeq .L50
	move.l %fp,%d0
	add.l #-280,%d0
	move.l %d0,%a1
	.cfi_escape 0x2e,0
	jsr _Z18random_unit_vectorv
	lea (-346,%fp),%a0
	lea (24,%a0),%a0
	move.l %a0,-(%sp)
	move.l %fp,%d0
	add.l #-346,%d0
	move.l %d0,-(%sp)
	move.l %fp,%d0
	add.l #-256,%d0
	move.l %d0,%a1
	.cfi_escape 0x2e,0x8
	jsr _ZplRK4vec3S1_
	addq.l #8,%sp
	move.l %fp,%d0
	add.l #-280,%d0
	move.l %d0,-(%sp)
	move.l %fp,%d0
	add.l #-256,%d0
	move.l %d0,-(%sp)
	move.l %fp,%d0
	add.l #-394,%d0
	move.l %d0,%a1
	jsr _ZplRK4vec3S1_
	addq.l #8,%sp
	move.l 16(%fp),%d3
	subq.l #1,%d3
	move.l %fp,%d0
	add.l #-346,%d0
	move.l %d0,-(%sp)
	move.l %fp,%d0
	add.l #-394,%d0
	move.l %d0,-(%sp)
	move.l %fp,%d0
	add.l #-160,%d0
	move.l %d0,%a1
	jsr _ZmiRK4vec3S1_
	addq.l #8,%sp
	move.l %fp,%d0
	add.l #-160,%d0
	move.l %d0,-(%sp)
	move.l %fp,%d0
	add.l #-346,%d0
	move.l %d0,-(%sp)
	move.l %fp,%d0
	add.l #-208,%d0
	move.l %d0,-(%sp)
	.cfi_escape 0x2e,0xc
	jsr _ZN3rayC1ERK4vec3S2_
	lea (12,%sp),%sp
	move.l %d3,-(%sp)
	move.l 12(%fp),-(%sp)
	move.l %fp,%d0
	add.l #-208,%d0
	move.l %d0,-(%sp)
	move.l %fp,%d0
	add.l #-232,%d0
	move.l %d0,%a1
	jsr _ZL9ray_colorRK3rayRK8hittablei
	lea (12,%sp),%sp
	move.l %fp,%d0
	add.l #-232,%d0
	move.l %d0,-(%sp)
	clr.l -(%sp)
	move.l #1071644672,-(%sp)
	move.l %d2,%a1
	jsr _ZmldRK4vec3
	lea (12,%sp),%sp
	jra .L49
.L50:
	move.l 8(%fp),-(%sp)
	move.l %fp,%d0
	add.l #-136,%d0
	move.l %d0,%a1
	.cfi_escape 0x2e,0x4
	jsr _ZNK3ray9directionEv
	addq.l #4,%sp
	lea (-24,%sp),%sp
	move.l %sp,%d0
	lea (-136,%fp),%a0
	move.l %d0,%a1
	move.l (%a0),(%a1)
	addq.l #4,%a1
	addq.l #4,%a0
	move.l (%a0),(%a1)
	addq.l #4,%a1
	addq.l #4,%a0
	move.l (%a0),(%a1)
	addq.l #4,%a1
	addq.l #4,%a0
	move.l (%a0),(%a1)
	addq.l #4,%a1
	addq.l #4,%a0
	move.l (%a0),(%a1)
	addq.l #4,%a1
	addq.l #4,%a0
	move.l (%a0),(%a1)
	addq.l #4,%a1
	addq.l #4,%a0
	move.l %fp,%d0
	add.l #-370,%d0
	move.l %d0,%a1
	.cfi_escape 0x2e,0x18
	jsr _Z11unit_vector4vec3
	lea (24,%sp),%sp
	move.l %fp,%d0
	add.l #-370,%d0
	move.l %d0,-(%sp)
	.cfi_escape 0x2e,0x4
	jsr _ZNK4vec31yEv
	addq.l #4,%sp
	fmovecr #0x32,%fp1
	fadd.x %fp1,%fp0
	fmove.x %fp0,%fp1
	fmul.d #0x3fe0000000000000,%fp1
	fmove.d %fp1,-16(%fp)
	clr.l -(%sp)
	move.l #1072693248,-(%sp)
	move.l #1717986918,-(%sp)
	move.l #1072064102,-(%sp)
	clr.l -(%sp)
	move.l #1071644672,-(%sp)
	lea (-88,%fp),%a0
	move.l %a0,-(%sp)
	.cfi_escape 0x2e,0x1c
	jsr _ZN4vec3C1Eddd
	lea (28,%sp),%sp
	lea (-88,%fp),%a0
	move.l %a0,-(%sp)
	move.l -12(%fp),-(%sp)
	move.l -16(%fp),-(%sp)
	lea (-112,%fp),%a0
	move.l %a0,%a1
	.cfi_escape 0x2e,0xc
	jsr _ZmldRK4vec3
	lea (12,%sp),%sp
	clr.l -(%sp)
	move.l #1072693248,-(%sp)
	clr.l -(%sp)
	move.l #1072693248,-(%sp)
	clr.l -(%sp)
	move.l #1072693248,-(%sp)
	lea (-40,%fp),%a0
	move.l %a0,-(%sp)
	.cfi_escape 0x2e,0x1c
	jsr _ZN4vec3C1Eddd
	lea (28,%sp),%sp
	fmovecr #0x32,%fp0
	fsub.d -16(%fp),%fp0
	lea (-40,%fp),%a0
	move.l %a0,-(%sp)
	fmove.d %fp0,-(%sp)
	lea (-64,%fp),%a0
	move.l %a0,%a1
	.cfi_escape 0x2e,0xc
	jsr _ZmldRK4vec3
	lea (12,%sp),%sp
	lea (-112,%fp),%a0
	move.l %a0,-(%sp)
	lea (-64,%fp),%a0
	move.l %a0,-(%sp)
	move.l %d2,%a1
	.cfi_escape 0x2e,0x8
	jsr _ZplRK4vec3S1_
.LEHE4:
	addq.l #8,%sp
.L49:
	move.l %fp,%d0
	add.l #-346,%d0
	move.l %d0,-(%sp)
	jsr _ZN10hit_recordD1Ev
	addq.l #4,%sp
	jra .L54
.L53:
	move.l %d0,%d2
	move.l %fp,%d0
	add.l #-346,%d0
	move.l %d0,-(%sp)
	jsr _ZN10hit_recordD1Ev
	addq.l #4,%sp
	move.l %d2,%d0
	move.l %d0,-(%sp)
.LEHB5:
	jsr _Unwind_Resume
.LEHE5:
.L54:
	move.l %d2,%a0
	move.l -404(%fp),%d2
	move.l -400(%fp),%d3
	unlk %fp
	rts
	.cfi_endproc
.LFE3937:
	.section	.gcc_except_table,"a",@progbits
.LLSDA3937:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE3937-.LLSDACSB3937
.LLSDACSB3937:
	.uleb128 .LEHB3-.LFB3937
	.uleb128 .LEHE3-.LEHB3
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB4-.LFB3937
	.uleb128 .LEHE4-.LEHB4
	.uleb128 .L53-.LFB3937
	.uleb128 0
	.uleb128 .LEHB5-.LFB3937
	.uleb128 .LEHE5-.LEHB5
	.uleb128 0
	.uleb128 0
.LLSDACSE3937:
	.text
	.size	_ZL9ray_colorRK3rayRK8hittablei, .-_ZL9ray_colorRK3rayRK8hittablei
	.section	.text._ZN13hittable_listD2Ev,"axG",@progbits,_ZN13hittable_listD5Ev,comdat
	.align	2
	.weak	_ZN13hittable_listD2Ev
	.type	_ZN13hittable_listD2Ev, @function
_ZN13hittable_listD2Ev:
.LFB3952:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l #_ZTV13hittable_list+8,%d0
	move.l 8(%fp),%a0
	move.l %d0,(%a0)
	move.l 8(%fp),%d0
	addq.l #4,%d0
	move.l %d0,-(%sp)
	jsr _ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED1Ev
	addq.l #4,%sp
	nop
	unlk %fp
	rts
	.cfi_endproc
.LFE3952:
	.size	_ZN13hittable_listD2Ev, .-_ZN13hittable_listD2Ev
	.weak	_ZN13hittable_listD1Ev
	.set	_ZN13hittable_listD1Ev,_ZN13hittable_listD2Ev
	.section	.text._ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align	2
	.weak	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED2Ev
	.type	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED2Ev:
.LFB3956:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l 8(%fp),%d0
	addq.l #4,%d0
	move.l %d0,-(%sp)
	jsr _ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED1Ev
	addq.l #4,%sp
	nop
	unlk %fp
	rts
	.cfi_endproc
.LFE3956:
	.size	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED1Ev
	.set	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED1Ev,_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt10shared_ptrI6sphereED2Ev,"axG",@progbits,_ZNSt10shared_ptrI6sphereED5Ev,comdat
	.align	2
	.weak	_ZNSt10shared_ptrI6sphereED2Ev
	.type	_ZNSt10shared_ptrI6sphereED2Ev, @function
_ZNSt10shared_ptrI6sphereED2Ev:
.LFB3958:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l 8(%fp),%d0
	move.l %d0,-(%sp)
	jsr _ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED2Ev
	addq.l #4,%sp
	nop
	unlk %fp
	rts
	.cfi_endproc
.LFE3958:
	.size	_ZNSt10shared_ptrI6sphereED2Ev, .-_ZNSt10shared_ptrI6sphereED2Ev
	.weak	_ZNSt10shared_ptrI6sphereED1Ev
	.set	_ZNSt10shared_ptrI6sphereED1Ev,_ZNSt10shared_ptrI6sphereED2Ev
	.section	.text._ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align	2
	.weak	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED2Ev
	.type	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED2Ev:
.LFB3962:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l 8(%fp),%d0
	addq.l #4,%d0
	move.l %d0,-(%sp)
	jsr _ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED1Ev
	addq.l #4,%sp
	nop
	unlk %fp
	rts
	.cfi_endproc
.LFE3962:
	.size	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED1Ev
	.set	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED1Ev,_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt10shared_ptrI8hittableED2Ev,"axG",@progbits,_ZNSt10shared_ptrI8hittableED5Ev,comdat
	.align	2
	.weak	_ZNSt10shared_ptrI8hittableED2Ev
	.type	_ZNSt10shared_ptrI8hittableED2Ev, @function
_ZNSt10shared_ptrI8hittableED2Ev:
.LFB3964:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l 8(%fp),%d0
	move.l %d0,-(%sp)
	jsr _ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED2Ev
	addq.l #4,%sp
	nop
	unlk %fp
	rts
	.cfi_endproc
.LFE3964:
	.size	_ZNSt10shared_ptrI8hittableED2Ev, .-_ZNSt10shared_ptrI8hittableED2Ev
	.weak	_ZNSt10shared_ptrI8hittableED1Ev
	.set	_ZNSt10shared_ptrI8hittableED1Ev,_ZNSt10shared_ptrI8hittableED2Ev
	.section	.rodata
.LC0:
	.string	"chapter8.ppm"
.LC1:
	.string	"P3\n"
.LC2:
	.string	"\n255\n"
.LC3:
	.string	"\rScanlines remaining: "
.LC4:
	.string	"\nDone.\n"
	.text
	.align	2
	.globl	main
	.type	main, @function
main:
.LFB3950:
	.cfi_startproc
	.cfi_personality 0,__gxx_personality_v0
	.cfi_lsda 0,.LLSDA3950
	link.w %fp,#-704
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	fmovem #4,-(%sp)
	move.l %d2,-(%sp)
	.cfi_offset 18, -724
	.cfi_offset 2, -728
	move.l #1073508807,-20(%fp)
	move.l #477218588,-16(%fp)
	move.l #400,-24(%fp)
	move.l #225,-28(%fp)
	moveq #100,%d0
	move.l %d0,-32(%fp)
	moveq #50,%d0
	move.l %d0,-36(%fp)
	move.l %fp,%d0
	add.l #-184,%d0
	move.l %d0,-(%sp)
.LEHB6:
	jsr _ZN13hittable_listC1Ev
.LEHE6:
	addq.l #4,%sp
	move.l #1071644672,-152(%fp)
	clr.l -148(%fp)
	clr.l -(%sp)
	move.l #-1074790400,-(%sp)
	clr.l -(%sp)
	clr.l -(%sp)
	clr.l -(%sp)
	clr.l -(%sp)
	move.l %fp,%d0
	add.l #-144,%d0
	move.l %d0,-(%sp)
.LEHB7:
	.cfi_escape 0x2e,0x1c
	jsr _ZN4vec3C1Eddd
	lea (28,%sp),%sp
	move.l %fp,%d0
	add.l #-152,%d0
	move.l %d0,-(%sp)
	move.l %fp,%d0
	add.l #-144,%d0
	move.l %d0,-(%sp)
	move.l %fp,%d0
	add.l #-160,%d0
	move.l %d0,%a1
	.cfi_escape 0x2e,0x8
	jsr _ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_
.LEHE7:
	addq.l #8,%sp
	move.l %fp,%d0
	add.l #-160,%d0
	move.l %d0,-(%sp)
	move.l %fp,%d0
	add.l #-168,%d0
	move.l %d0,-(%sp)
	jsr _ZNSt10shared_ptrI8hittableEC1I6spherevEEOS_IT_E
	addq.l #8,%sp
	move.l %fp,%d0
	add.l #-168,%d0
	move.l %d0,-(%sp)
	move.l %fp,%d0
	add.l #-184,%d0
	move.l %d0,-(%sp)
.LEHB8:
	jsr _ZN13hittable_list3addESt10shared_ptrI8hittableE
.LEHE8:
	addq.l #8,%sp
	move.l %fp,%d0
	add.l #-168,%d0
	move.l %d0,-(%sp)
	jsr _ZNSt10shared_ptrI8hittableED1Ev
	addq.l #4,%sp
	move.l %fp,%d0
	add.l #-160,%d0
	move.l %d0,-(%sp)
	jsr _ZNSt10shared_ptrI6sphereED1Ev
	addq.l #4,%sp
	moveq #100,%d0
	move.l %d0,-104(%fp)
	clr.l -(%sp)
	move.l #-1074790400,-(%sp)
	clr.l -(%sp)
	move.l #-1067900928,-(%sp)
	clr.l -(%sp)
	clr.l -(%sp)
	lea (-100,%fp),%a0
	move.l %a0,-(%sp)
.LEHB9:
	.cfi_escape 0x2e,0x1c
	jsr _ZN4vec3C1Eddd
	lea (28,%sp),%sp
	lea (-104,%fp),%a0
	move.l %a0,-(%sp)
	lea (-100,%fp),%a0
	move.l %a0,-(%sp)
	lea (-112,%fp),%a0
	move.l %a0,%a1
	.cfi_escape 0x2e,0x8
	jsr _ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_
.LEHE9:
	addq.l #8,%sp
	lea (-112,%fp),%a0
	move.l %a0,-(%sp)
	lea (-120,%fp),%a0
	move.l %a0,-(%sp)
	jsr _ZNSt10shared_ptrI8hittableEC1I6spherevEEOS_IT_E
	addq.l #8,%sp
	lea (-120,%fp),%a0
	move.l %a0,-(%sp)
	move.l %fp,%d0
	add.l #-184,%d0
	move.l %d0,-(%sp)
.LEHB10:
	jsr _ZN13hittable_list3addESt10shared_ptrI8hittableE
.LEHE10:
	addq.l #8,%sp
	lea (-120,%fp),%a0
	move.l %a0,-(%sp)
	jsr _ZNSt10shared_ptrI8hittableED1Ev
	addq.l #4,%sp
	lea (-112,%fp),%a0
	move.l %a0,-(%sp)
	jsr _ZNSt10shared_ptrI6sphereED1Ev
	addq.l #4,%sp
	move.l %fp,%d0
	add.l #-360,%d0
	move.l %d0,-(%sp)
.LEHB11:
	.cfi_escape 0x2e,0x4
	jsr _ZN6cameraC1Ev
	addq.l #4,%sp
	move.l %fp,%d0
	add.l #-632,%d0
	move.l %d0,-(%sp)
	jsr _ZNSt14basic_ofstreamIcSt11char_traitsIcEEC1Ev
.LEHE11:
	addq.l #4,%sp
	pea 16.w
	pea .LC0
	move.l %fp,%d0
	add.l #-632,%d0
	move.l %d0,-(%sp)
.LEHB12:
	.cfi_escape 0x2e,0xc
	jsr _ZNSt14basic_ofstreamIcSt11char_traitsIcEE4openEPKcSt13_Ios_Openmode
	lea (12,%sp),%sp
	pea .LC1
	move.l %fp,%d0
	add.l #-632,%d0
	move.l %d0,-(%sp)
	.cfi_escape 0x2e,0x8
	jsr _ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	addq.l #8,%sp
	move.l %a0,%d0
	pea 400.w
	move.l %d0,-(%sp)
	jsr _ZNSolsEi
	addq.l #8,%sp
	move.l %a0,%d0
	pea 32.w
	move.l %d0,-(%sp)
	jsr _ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_c
	addq.l #8,%sp
	move.l %a0,%d0
	pea 225.w
	move.l %d0,-(%sp)
	jsr _ZNSolsEi
	addq.l #8,%sp
	move.l %a0,%d0
	pea .LC2
	move.l %d0,-(%sp)
	jsr _ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	addq.l #8,%sp
	move.l #224,-4(%fp)
.L66:
	tst.l -4(%fp)
	jlt .L61
	pea .LC3
	pea _ZSt4cerr
	jsr _ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	addq.l #8,%sp
	move.l %a0,%d0
	move.l -4(%fp),-(%sp)
	move.l %d0,-(%sp)
	jsr _ZNSolsEi
	addq.l #8,%sp
	move.l %a0,%d0
	pea 32.w
	move.l %d0,-(%sp)
	jsr _ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_c
	addq.l #8,%sp
	move.l %a0,%d0
	pea _ZSt5flushIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_
	move.l %d0,-(%sp)
	jsr _ZNSolsEPFRSoS_E
	addq.l #8,%sp
	clr.l -8(%fp)
.L65:
	cmp.l #399,-8(%fp)
	jgt .L62
	clr.l -(%sp)
	clr.l -(%sp)
	clr.l -(%sp)
	clr.l -(%sp)
	clr.l -(%sp)
	clr.l -(%sp)
	move.l %fp,%d0
	add.l #-656,%d0
	move.l %d0,-(%sp)
	.cfi_escape 0x2e,0x1c
	jsr _ZN4vec3C1Eddd
	lea (28,%sp),%sp
	clr.l -12(%fp)
.L64:
	moveq #99,%d0
	cmp.l -12(%fp),%d0
	jlt .L63
	fmove.l -8(%fp),%fp2
	.cfi_escape 0x2e,0
	jsr _Z13random_doublev
	fadd.x %fp2,%fp0
	fmove.x %fp0,%fp1
	fdiv.d #0x4078f00000000000,%fp1
	fmove.d %fp1,-44(%fp)
	fmove.l -4(%fp),%fp2
	jsr _Z13random_doublev
	fadd.x %fp2,%fp0
	fmove.x %fp0,%fp1
	fdiv.d #0x406c000000000000,%fp1
	fmove.d %fp1,-52(%fp)
	move.l -48(%fp),-(%sp)
	move.l -52(%fp),-(%sp)
	move.l -40(%fp),-(%sp)
	move.l -44(%fp),-(%sp)
	move.l %fp,%d0
	add.l #-360,%d0
	move.l %d0,-(%sp)
	move.l %fp,%d0
	add.l #-704,%d0
	move.l %d0,%a1
	.cfi_escape 0x2e,0x14
	jsr _ZNK6camera7get_rayEdd
	lea (20,%sp),%sp
	pea 50.w
	move.l %fp,%d0
	add.l #-184,%d0
	move.l %d0,-(%sp)
	move.l %fp,%d0
	add.l #-704,%d0
	move.l %d0,-(%sp)
	lea (-76,%fp),%a0
	move.l %a0,%a1
	.cfi_escape 0x2e,0xc
	jsr _ZL9ray_colorRK3rayRK8hittablei
	lea (12,%sp),%sp
	lea (-76,%fp),%a0
	move.l %a0,-(%sp)
	move.l %fp,%d0
	add.l #-656,%d0
	move.l %d0,-(%sp)
	.cfi_escape 0x2e,0x8
	jsr _ZN4vec3pLERKS_
	addq.l #8,%sp
	addq.l #1,-12(%fp)
	jra .L64
.L63:
	pea 100.w
	lea (-632,%fp),%a0
	subq.l #4,%a0
	move.l (%a0),-(%sp)
	subq.l #4,%a0
	move.l (%a0),-(%sp)
	subq.l #4,%a0
	move.l (%a0),-(%sp)
	subq.l #4,%a0
	move.l (%a0),-(%sp)
	subq.l #4,%a0
	move.l (%a0),-(%sp)
	subq.l #4,%a0
	move.l (%a0),-(%sp)
	move.l %fp,%d0
	add.l #-632,%d0
	move.l %d0,-(%sp)
	.cfi_escape 0x2e,0x20
	jsr _Z11write_colorRSt14basic_ofstreamIcSt11char_traitsIcEE4vec3i
	lea (32,%sp),%sp
	addq.l #1,-8(%fp)
	jra .L65
.L62:
	subq.l #1,-4(%fp)
	jra .L66
.L61:
	pea .LC4
	pea _ZSt4cerr
	.cfi_escape 0x2e,0x8
	jsr _ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
.LEHE12:
	addq.l #8,%sp
	clr.l %d2
	move.l %fp,%d0
	add.l #-632,%d0
	move.l %d0,-(%sp)
	jsr _ZNSt14basic_ofstreamIcSt11char_traitsIcEED1Ev
	addq.l #4,%sp
	move.l %fp,%d0
	add.l #-184,%d0
	move.l %d0,-(%sp)
	jsr _ZN13hittable_listD1Ev
	addq.l #4,%sp
	move.l %d2,%d0
	jra .L76
.L72:
	move.l %d0,%d2
	move.l %fp,%d0
	add.l #-168,%d0
	move.l %d0,-(%sp)
	jsr _ZNSt10shared_ptrI8hittableED1Ev
	addq.l #4,%sp
	move.l %fp,%d0
	add.l #-160,%d0
	move.l %d0,-(%sp)
	jsr _ZNSt10shared_ptrI6sphereED1Ev
	addq.l #4,%sp
	jra .L69
.L74:
	move.l %d0,%d2
	lea (-120,%fp),%a0
	move.l %a0,-(%sp)
	jsr _ZNSt10shared_ptrI8hittableED1Ev
	addq.l #4,%sp
	lea (-112,%fp),%a0
	move.l %a0,-(%sp)
	jsr _ZNSt10shared_ptrI6sphereED1Ev
	addq.l #4,%sp
	jra .L69
.L75:
	move.l %d0,%d2
	move.l %fp,%d0
	add.l #-632,%d0
	move.l %d0,-(%sp)
	jsr _ZNSt14basic_ofstreamIcSt11char_traitsIcEED1Ev
	addq.l #4,%sp
	jra .L69
.L73:
	move.l %d0,%d2
.L69:
	move.l %fp,%d0
	add.l #-184,%d0
	move.l %d0,-(%sp)
	jsr _ZN13hittable_listD1Ev
	addq.l #4,%sp
	move.l %d2,%d0
	move.l %d0,-(%sp)
.LEHB13:
	jsr _Unwind_Resume
.LEHE13:
.L76:
	move.l -720(%fp),%d2
	fmovem -716(%fp),#32
	unlk %fp
	rts
	.cfi_endproc
.LFE3950:
	.section	.gcc_except_table
.LLSDA3950:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE3950-.LLSDACSB3950
.LLSDACSB3950:
	.uleb128 .LEHB6-.LFB3950
	.uleb128 .LEHE6-.LEHB6
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB7-.LFB3950
	.uleb128 .LEHE7-.LEHB7
	.uleb128 .L73-.LFB3950
	.uleb128 0
	.uleb128 .LEHB8-.LFB3950
	.uleb128 .LEHE8-.LEHB8
	.uleb128 .L72-.LFB3950
	.uleb128 0
	.uleb128 .LEHB9-.LFB3950
	.uleb128 .LEHE9-.LEHB9
	.uleb128 .L73-.LFB3950
	.uleb128 0
	.uleb128 .LEHB10-.LFB3950
	.uleb128 .LEHE10-.LEHB10
	.uleb128 .L74-.LFB3950
	.uleb128 0
	.uleb128 .LEHB11-.LFB3950
	.uleb128 .LEHE11-.LEHB11
	.uleb128 .L73-.LFB3950
	.uleb128 0
	.uleb128 .LEHB12-.LFB3950
	.uleb128 .LEHE12-.LEHB12
	.uleb128 .L75-.LFB3950
	.uleb128 0
	.uleb128 .LEHB13-.LFB3950
	.uleb128 .LEHE13-.LEHB13
	.uleb128 0
	.uleb128 0
.LLSDACSE3950:
	.text
	.size	main, .-main
	.section	.text._ZNSt25uniform_real_distributionIdEC2Edd,"axG",@progbits,_ZNSt25uniform_real_distributionIdEC5Edd,comdat
	.align	2
	.weak	_ZNSt25uniform_real_distributionIdEC2Edd
	.type	_ZNSt25uniform_real_distributionIdEC2Edd, @function
_ZNSt25uniform_real_distributionIdEC2Edd:
.LFB4215:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l 8(%fp),%d0
	move.l 24(%fp),-(%sp)
	move.l 20(%fp),-(%sp)
	move.l 16(%fp),-(%sp)
	move.l 12(%fp),-(%sp)
	move.l %d0,-(%sp)
	jsr _ZNSt25uniform_real_distributionIdE10param_typeC1Edd
	lea (20,%sp),%sp
	nop
	unlk %fp
	rts
	.cfi_endproc
.LFE4215:
	.size	_ZNSt25uniform_real_distributionIdEC2Edd, .-_ZNSt25uniform_real_distributionIdEC2Edd
	.weak	_ZNSt25uniform_real_distributionIdEC1Edd
	.set	_ZNSt25uniform_real_distributionIdEC1Edd,_ZNSt25uniform_real_distributionIdEC2Edd
	.section	.text._ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC2Ev,"axG",@progbits,_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC5Ev,comdat
	.align	2
	.weak	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC2Ev
	.type	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC2Ev, @function
_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC2Ev:
.LFB4218:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	pea 5489.w
	move.l 8(%fp),-(%sp)
	jsr _ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC1Ej
	addq.l #8,%sp
	nop
	unlk %fp
	rts
	.cfi_endproc
.LFE4218:
	.size	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC2Ev, .-_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC2Ev
	.weak	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC1Ev
	.set	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC1Ev,_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC2Ev
	.section	.text._ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEEEdRT_,"axG",@progbits,_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEEEdRT_,comdat
	.align	2
	.weak	_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEEEdRT_
	.type	_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEEEdRT_, @function
_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEEEdRT_:
.LFB4220:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l 8(%fp),%d0
	move.l %d0,-(%sp)
	move.l 12(%fp),-(%sp)
	move.l 8(%fp),-(%sp)
	jsr _ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEEEdRT_RKNS0_10param_typeE
	lea (12,%sp),%sp
	unlk %fp
	rts
	.cfi_endproc
.LFE4220:
	.size	_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEEEdRT_, .-_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEEEdRT_
	.section	.text._ZNSt10shared_ptrI8materialEC2Ev,"axG",@progbits,_ZNSt10shared_ptrI8materialEC5Ev,comdat
	.align	2
	.weak	_ZNSt10shared_ptrI8materialEC2Ev
	.type	_ZNSt10shared_ptrI8materialEC2Ev, @function
_ZNSt10shared_ptrI8materialEC2Ev:
.LFB4248:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l 8(%fp),%d0
	move.l %d0,-(%sp)
	jsr _ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC2Ev
	addq.l #4,%sp
	nop
	unlk %fp
	rts
	.cfi_endproc
.LFE4248:
	.size	_ZNSt10shared_ptrI8materialEC2Ev, .-_ZNSt10shared_ptrI8materialEC2Ev
	.weak	_ZNSt10shared_ptrI8materialEC1Ev
	.set	_ZNSt10shared_ptrI8materialEC1Ev,_ZNSt10shared_ptrI8materialEC2Ev
	.section	.text._ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align	2
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED2Ev
	.type	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED2Ev:
.LFB4251:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l 8(%fp),%a0
	move.l (%a0),%d0
	tst.l %d0
	jeq .L84
	move.l 8(%fp),%a0
	move.l (%a0),%d0
	move.l %d0,-(%sp)
	jsr _ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv
	addq.l #4,%sp
.L84:
	nop
	unlk %fp
	rts
	.cfi_endproc
.LFE4251:
	.size	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED1Ev
	.set	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED1Ev,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED2Ev,"axG",@progbits,_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED5Ev,comdat
	.align	2
	.weak	_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED2Ev
	.type	_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED2Ev, @function
_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED2Ev:
.LFB4254:
	.cfi_startproc
	.cfi_personality 0,__gxx_personality_v0
	.cfi_lsda 0,.LLSDA4254
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l 8(%fp),%d0
	move.l %d0,-(%sp)
	jsr _ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE19_M_get_Tp_allocatorEv
	addq.l #4,%sp
	move.l %a0,%a1
	move.l 8(%fp),%a0
	move.l 4(%a0),%d1
	move.l 8(%fp),%a0
	move.l (%a0),%d0
	move.l %a1,-(%sp)
	move.l %d1,-(%sp)
	move.l %d0,-(%sp)
	jsr _ZSt8_DestroyIPSt10shared_ptrI8hittableES2_EvT_S4_RSaIT0_E
	lea (12,%sp),%sp
	move.l 8(%fp),%d0
	move.l %d0,-(%sp)
	jsr _ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED2Ev
	addq.l #4,%sp
	nop
	unlk %fp
	rts
	.cfi_endproc
.LFE4254:
	.section	.gcc_except_table
.LLSDA4254:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4254-.LLSDACSB4254
.LLSDACSB4254:
.LLSDACSE4254:
	.section	.text._ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED2Ev,"axG",@progbits,_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED5Ev,comdat
	.size	_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED2Ev, .-_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED2Ev
	.weak	_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED1Ev
	.set	_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED1Ev,_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED2Ev
	.section	.text._ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_,"axG",@progbits,_ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_,comdat
	.align	2
	.weak	_ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_
	.type	_ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_, @function
_ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_:
.LFB4256:
	.cfi_startproc
	.cfi_personality 0,__gxx_personality_v0
	.cfi_lsda 0,.LLSDA4256
	link.w %fp,#-4
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	movem.l #14336,-(%sp)
	.cfi_offset 2, -24
	.cfi_offset 3, -20
	.cfi_offset 4, -16
	move.l %a1,%d2
	move.l 12(%fp),-(%sp)
	jsr _ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE
	addq.l #4,%sp
	move.l %a0,%d4
	move.l 8(%fp),-(%sp)
	jsr _ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
	addq.l #4,%sp
	move.l %a0,%d3
	move.l %fp,%d0
	subq.l #1,%d0
	move.l %d0,-(%sp)
	jsr _ZNSaI6sphereEC1Ev
	addq.l #4,%sp
	move.l %d4,-(%sp)
	move.l %d3,-(%sp)
	move.l %fp,%d0
	subq.l #1,%d0
	move.l %d0,-(%sp)
	move.l %d2,%a1
.LEHB14:
	.cfi_escape 0x2e,0xc
	jsr _ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3dEESt10shared_ptrIT_ERKT0_DpOT1_
.LEHE14:
	lea (12,%sp),%sp
	move.l %fp,%d0
	subq.l #1,%d0
	move.l %d0,-(%sp)
	jsr _ZNSaI6sphereED1Ev
	addq.l #4,%sp
	jra .L90
.L89:
	move.l %d0,%d2
	move.l %fp,%d0
	subq.l #1,%d0
	move.l %d0,-(%sp)
	jsr _ZNSaI6sphereED1Ev
	addq.l #4,%sp
	move.l %d2,%d0
	move.l %d0,-(%sp)
.LEHB15:
	jsr _Unwind_Resume
.LEHE15:
.L90:
	move.l %d2,%a0
	move.l %d2,%a0
	movem.l -16(%fp),#28
	unlk %fp
	rts
	.cfi_endproc
.LFE4256:
	.section	.gcc_except_table
.LLSDA4256:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4256-.LLSDACSB4256
.LLSDACSB4256:
	.uleb128 .LEHB14-.LFB4256
	.uleb128 .LEHE14-.LEHB14
	.uleb128 .L89-.LFB4256
	.uleb128 0
	.uleb128 .LEHB15-.LFB4256
	.uleb128 .LEHE15-.LEHB15
	.uleb128 0
	.uleb128 0
.LLSDACSE4256:
	.section	.text._ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_,"axG",@progbits,_ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_,comdat
	.size	_ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_, .-_ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_
	.section	.text._ZNSt10shared_ptrI8hittableEC2I6spherevEEOS_IT_E,"axG",@progbits,_ZNSt10shared_ptrI8hittableEC5I6spherevEEOS_IT_E,comdat
	.align	2
	.weak	_ZNSt10shared_ptrI8hittableEC2I6spherevEEOS_IT_E
	.type	_ZNSt10shared_ptrI8hittableEC2I6spherevEEOS_IT_E, @function
_ZNSt10shared_ptrI8hittableEC2I6spherevEEOS_IT_E:
.LFB4258:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l %d2,-(%sp)
	.cfi_offset 2, -12
	move.l 8(%fp),%d2
	move.l 12(%fp),-(%sp)
	jsr _ZSt4moveIRSt10shared_ptrI6sphereEEONSt16remove_referenceIT_E4typeEOS5_
	addq.l #4,%sp
	move.l %a0,%d0
	move.l %d0,-(%sp)
	move.l %d2,-(%sp)
	jsr _ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC2I6spherevEEOS_IT_LS2_2EE
	addq.l #8,%sp
	nop
	move.l -4(%fp),%d2
	unlk %fp
	rts
	.cfi_endproc
.LFE4258:
	.size	_ZNSt10shared_ptrI8hittableEC2I6spherevEEOS_IT_E, .-_ZNSt10shared_ptrI8hittableEC2I6spherevEEOS_IT_E
	.weak	_ZNSt10shared_ptrI8hittableEC1I6spherevEEOS_IT_E
	.set	_ZNSt10shared_ptrI8hittableEC1I6spherevEEOS_IT_E,_ZNSt10shared_ptrI8hittableEC2I6spherevEEOS_IT_E
	.section	.text._ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_,"axG",@progbits,_ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_,comdat
	.align	2
	.weak	_ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_
	.type	_ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_, @function
_ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_:
.LFB4263:
	.cfi_startproc
	.cfi_personality 0,__gxx_personality_v0
	.cfi_lsda 0,.LLSDA4263
	link.w %fp,#-4
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	movem.l #14336,-(%sp)
	.cfi_offset 2, -24
	.cfi_offset 3, -20
	.cfi_offset 4, -16
	move.l %a1,%d2
	move.l 12(%fp),-(%sp)
	jsr _ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE
	addq.l #4,%sp
	move.l %a0,%d4
	move.l 8(%fp),-(%sp)
	jsr _ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
	addq.l #4,%sp
	move.l %a0,%d3
	move.l %fp,%d0
	subq.l #1,%d0
	move.l %d0,-(%sp)
	jsr _ZNSaI6sphereEC1Ev
	addq.l #4,%sp
	move.l %d4,-(%sp)
	move.l %d3,-(%sp)
	move.l %fp,%d0
	subq.l #1,%d0
	move.l %d0,-(%sp)
	move.l %d2,%a1
.LEHB16:
	.cfi_escape 0x2e,0xc
	jsr _ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3iEESt10shared_ptrIT_ERKT0_DpOT1_
.LEHE16:
	lea (12,%sp),%sp
	move.l %fp,%d0
	subq.l #1,%d0
	move.l %d0,-(%sp)
	jsr _ZNSaI6sphereED1Ev
	addq.l #4,%sp
	jra .L96
.L95:
	move.l %d0,%d2
	move.l %fp,%d0
	subq.l #1,%d0
	move.l %d0,-(%sp)
	jsr _ZNSaI6sphereED1Ev
	addq.l #4,%sp
	move.l %d2,%d0
	move.l %d0,-(%sp)
.LEHB17:
	jsr _Unwind_Resume
.LEHE17:
.L96:
	move.l %d2,%a0
	move.l %d2,%a0
	movem.l -16(%fp),#28
	unlk %fp
	rts
	.cfi_endproc
.LFE4263:
	.section	.gcc_except_table
.LLSDA4263:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4263-.LLSDACSB4263
.LLSDACSB4263:
	.uleb128 .LEHB16-.LFB4263
	.uleb128 .LEHE16-.LEHB16
	.uleb128 .L95-.LFB4263
	.uleb128 0
	.uleb128 .LEHB17-.LFB4263
	.uleb128 .LEHE17-.LEHB17
	.uleb128 0
	.uleb128 0
.LLSDACSE4263:
	.section	.text._ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_,"axG",@progbits,_ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_,comdat
	.size	_ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_, .-_ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_
	.section	.text._ZNSt25uniform_real_distributionIdE10param_typeC2Edd,"axG",@progbits,_ZNSt25uniform_real_distributionIdE10param_typeC5Edd,comdat
	.align	2
	.weak	_ZNSt25uniform_real_distributionIdE10param_typeC2Edd
	.type	_ZNSt25uniform_real_distributionIdE10param_typeC2Edd, @function
_ZNSt25uniform_real_distributionIdE10param_typeC2Edd:
.LFB4390:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l 8(%fp),%a0
	move.l 12(%fp),%d0
	move.l 16(%fp),%d1
	move.l %d0,(%a0)
	move.l %d1,4(%a0)
	move.l 8(%fp),%a0
	move.l 20(%fp),%d0
	move.l 24(%fp),%d1
	move.l %d0,8(%a0)
	move.l %d1,12(%a0)
	nop
	unlk %fp
	rts
	.cfi_endproc
.LFE4390:
	.size	_ZNSt25uniform_real_distributionIdE10param_typeC2Edd, .-_ZNSt25uniform_real_distributionIdE10param_typeC2Edd
	.weak	_ZNSt25uniform_real_distributionIdE10param_typeC1Edd
	.set	_ZNSt25uniform_real_distributionIdE10param_typeC1Edd,_ZNSt25uniform_real_distributionIdE10param_typeC2Edd
	.section	.text._ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC2Ej,"axG",@progbits,_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC5Ej,comdat
	.align	2
	.weak	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC2Ej
	.type	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC2Ej, @function
_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC2Ej:
.LFB4393:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l 12(%fp),-(%sp)
	move.l 8(%fp),-(%sp)
	jsr _ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE4seedEj
	addq.l #8,%sp
	nop
	unlk %fp
	rts
	.cfi_endproc
.LFE4393:
	.size	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC2Ej, .-_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC2Ej
	.weak	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC1Ej
	.set	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC1Ej,_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC2Ej
	.section	.text._ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEEEdRT_RKNS0_10param_typeE,"axG",@progbits,_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEEEdRT_RKNS0_10param_typeE,comdat
	.align	2
	.weak	_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEEEdRT_RKNS0_10param_typeE
	.type	_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEEEdRT_RKNS0_10param_typeE, @function
_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEEEdRT_RKNS0_10param_typeE:
.LFB4395:
	.cfi_startproc
	link.w %fp,#-4
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	fmovem #12,-(%sp)
	.cfi_offset 18, -36
	.cfi_offset 19, -24
	move.l 12(%fp),-(%sp)
	move.l %fp,%d0
	subq.l #4,%d0
	move.l %d0,-(%sp)
	jsr _ZNSt8__detail8_AdaptorISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEdEC1ERS2_
	addq.l #8,%sp
	move.l %fp,%d0
	subq.l #4,%d0
	move.l %d0,-(%sp)
	jsr _ZNSt8__detail8_AdaptorISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEdEclEv
	addq.l #4,%sp
	fmove.x %fp0,%fp3
	move.l 16(%fp),-(%sp)
	jsr _ZNKSt25uniform_real_distributionIdE10param_type1bEv
	addq.l #4,%sp
	fmove.x %fp0,%fp2
	move.l 16(%fp),-(%sp)
	jsr _ZNKSt25uniform_real_distributionIdE10param_type1aEv
	addq.l #4,%sp
	fmove.x %fp2,%fp1
	fsub.x %fp0,%fp1
	fmove.x %fp1,%fp0
	fmove.x %fp3,%fp2
	fmul.x %fp0,%fp2
	move.l 16(%fp),-(%sp)
	jsr _ZNKSt25uniform_real_distributionIdE10param_type1aEv
	addq.l #4,%sp
	fadd.x %fp2,%fp0
	fmovem -28(%fp),#48
	unlk %fp
	rts
	.cfi_endproc
.LFE4395:
	.size	_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEEEdRT_RKNS0_10param_typeE, .-_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEEEdRT_RKNS0_10param_typeE
	.section	.text._ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC2Ev,"axG",@progbits,_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC5Ev,comdat
	.align	2
	.weak	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC2Ev
	.type	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC2Ev, @function
_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC2Ev:
.LFB4407:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l 8(%fp),%a0
	clr.l (%a0)
	move.l 8(%fp),%d0
	addq.l #4,%d0
	move.l %d0,-(%sp)
	jsr _ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1Ev
	addq.l #4,%sp
	nop
	unlk %fp
	rts
	.cfi_endproc
.LFE4407:
	.size	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC2Ev, .-_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC2Ev
	.weak	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC1Ev
	.set	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC1Ev,_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC2Ev
	.section	.text._ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv,"axG",@progbits,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv,comdat
	.align	2
	.weak	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv
	.type	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv, @function
_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv:
.LFB4409:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l 8(%fp),%d0
	addq.l #4,%d0
	pea -1.w
	move.l %d0,-(%sp)
	jsr _ZN9__gnu_cxxL27__exchange_and_add_dispatchEPii
	addq.l #8,%sp
	moveq #1,%d1
	cmp.l %d0,%d1
	seq %d0
	neg.b %d0
	tst.b %d0
	jeq .L104
	move.l 8(%fp),%a0
	move.l (%a0),%d0
	move.l %d0,%a0
	addq.l #8,%a0
	move.l (%a0),%a0
	move.l 8(%fp),-(%sp)
	jsr (%a0)
	addq.l #4,%sp
	move.l 8(%fp),%d0
	addq.l #8,%d0
	pea -1.w
	move.l %d0,-(%sp)
	jsr _ZN9__gnu_cxxL27__exchange_and_add_dispatchEPii
	addq.l #8,%sp
	moveq #1,%d1
	cmp.l %d0,%d1
	seq %d0
	neg.b %d0
	tst.b %d0
	jeq .L104
	move.l 8(%fp),%a0
	move.l (%a0),%a0
	lea (12,%a0),%a0
	move.l (%a0),%a0
	move.l 8(%fp),-(%sp)
	jsr (%a0)
	addq.l #4,%sp
.L104:
	nop
	unlk %fp
	rts
	.cfi_endproc
.LFE4409:
	.size	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv, .-_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv
	.section	.text._ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD2Ev,"axG",@progbits,_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD5Ev,comdat
	.align	2
	.weak	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD2Ev
	.type	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD2Ev, @function
_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD2Ev:
.LFB4412:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l 8(%fp),-(%sp)
	jsr _ZNSaISt10shared_ptrI8hittableEED2Ev
	addq.l #4,%sp
	nop
	unlk %fp
	rts
	.cfi_endproc
.LFE4412:
	.size	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD2Ev, .-_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD2Ev
	.weak	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD1Ev
	.set	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD1Ev,_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD2Ev
	.section	.text._ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED2Ev,"axG",@progbits,_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED5Ev,comdat
	.align	2
	.weak	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED2Ev
	.type	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED2Ev, @function
_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED2Ev:
.LFB4414:
	.cfi_startproc
	.cfi_personality 0,__gxx_personality_v0
	.cfi_lsda 0,.LLSDA4414
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l %d2,-(%sp)
	.cfi_offset 2, -12
	move.l 8(%fp),%a0
	move.l 8(%a0),%d1
	move.l 8(%fp),%a0
	move.l (%a0),%d0
	move.l %d1,%d2
	sub.l %d0,%d2
	move.l %d2,%d0
	asr.l #3,%d0
	move.l %d0,%d1
	move.l 8(%fp),%a0
	move.l (%a0),%d0
	move.l %d1,-(%sp)
	move.l %d0,-(%sp)
	move.l 8(%fp),-(%sp)
	jsr _ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE13_M_deallocateEPS2_j
	lea (12,%sp),%sp
	move.l 8(%fp),%d0
	move.l %d0,-(%sp)
	jsr _ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD1Ev
	addq.l #4,%sp
	nop
	move.l -4(%fp),%d2
	unlk %fp
	rts
	.cfi_endproc
.LFE4414:
	.section	.gcc_except_table
.LLSDA4414:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4414-.LLSDACSB4414
.LLSDACSB4414:
.LLSDACSE4414:
	.section	.text._ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED2Ev,"axG",@progbits,_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED5Ev,comdat
	.size	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED2Ev, .-_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED2Ev
	.weak	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED1Ev
	.set	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED1Ev,_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED2Ev
	.section	.text._ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE19_M_get_Tp_allocatorEv,"axG",@progbits,_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE19_M_get_Tp_allocatorEv,comdat
	.align	2
	.weak	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE19_M_get_Tp_allocatorEv
	.type	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE19_M_get_Tp_allocatorEv, @function
_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE19_M_get_Tp_allocatorEv:
.LFB4416:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l 8(%fp),%d0
	move.l %d0,%d1
	move.l %d1,%a0
	unlk %fp
	rts
	.cfi_endproc
.LFE4416:
	.size	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE19_M_get_Tp_allocatorEv, .-_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE19_M_get_Tp_allocatorEv
	.section	.text._ZSt8_DestroyIPSt10shared_ptrI8hittableES2_EvT_S4_RSaIT0_E,"axG",@progbits,_ZSt8_DestroyIPSt10shared_ptrI8hittableES2_EvT_S4_RSaIT0_E,comdat
	.align	2
	.weak	_ZSt8_DestroyIPSt10shared_ptrI8hittableES2_EvT_S4_RSaIT0_E
	.type	_ZSt8_DestroyIPSt10shared_ptrI8hittableES2_EvT_S4_RSaIT0_E, @function
_ZSt8_DestroyIPSt10shared_ptrI8hittableES2_EvT_S4_RSaIT0_E:
.LFB4417:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l 12(%fp),-(%sp)
	move.l 8(%fp),-(%sp)
	jsr _ZSt8_DestroyIPSt10shared_ptrI8hittableEEvT_S4_
	addq.l #8,%sp
	nop
	unlk %fp
	rts
	.cfi_endproc
.LFE4417:
	.size	_ZSt8_DestroyIPSt10shared_ptrI8hittableES2_EvT_S4_RSaIT0_E, .-_ZSt8_DestroyIPSt10shared_ptrI8hittableES2_EvT_S4_RSaIT0_E
	.section	.text._ZNSaI6sphereEC2Ev,"axG",@progbits,_ZNSaI6sphereEC5Ev,comdat
	.align	2
	.weak	_ZNSaI6sphereEC2Ev
	.type	_ZNSaI6sphereEC2Ev, @function
_ZNSaI6sphereEC2Ev:
.LFB4419:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l 8(%fp),-(%sp)
	jsr _ZN9__gnu_cxx13new_allocatorI6sphereEC2Ev
	addq.l #4,%sp
	nop
	unlk %fp
	rts
	.cfi_endproc
.LFE4419:
	.size	_ZNSaI6sphereEC2Ev, .-_ZNSaI6sphereEC2Ev
	.weak	_ZNSaI6sphereEC1Ev
	.set	_ZNSaI6sphereEC1Ev,_ZNSaI6sphereEC2Ev
	.section	.text._ZNSaI6sphereED2Ev,"axG",@progbits,_ZNSaI6sphereED5Ev,comdat
	.align	2
	.weak	_ZNSaI6sphereED2Ev
	.type	_ZNSaI6sphereED2Ev, @function
_ZNSaI6sphereED2Ev:
.LFB4422:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l 8(%fp),-(%sp)
	jsr _ZN9__gnu_cxx13new_allocatorI6sphereED2Ev
	addq.l #4,%sp
	nop
	unlk %fp
	rts
	.cfi_endproc
.LFE4422:
	.size	_ZNSaI6sphereED2Ev, .-_ZNSaI6sphereED2Ev
	.weak	_ZNSaI6sphereED1Ev
	.set	_ZNSaI6sphereED1Ev,_ZNSaI6sphereED2Ev
	.section	.text._ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE,"axG",@progbits,_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE,comdat
	.align	2
	.weak	_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
	.type	_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE, @function
_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE:
.LFB4424:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l 8(%fp),%d0
	move.l %d0,%d1
	move.l %d1,%a0
	unlk %fp
	rts
	.cfi_endproc
.LFE4424:
	.size	_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE, .-_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
	.section	.text._ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE,"axG",@progbits,_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE,comdat
	.align	2
	.weak	_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE
	.type	_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE, @function
_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE:
.LFB4425:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l 8(%fp),%d0
	move.l %d0,%d1
	move.l %d1,%a0
	unlk %fp
	rts
	.cfi_endproc
.LFE4425:
	.size	_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE, .-_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE
	.section	.text._ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3dEESt10shared_ptrIT_ERKT0_DpOT1_,"axG",@progbits,_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3dEESt10shared_ptrIT_ERKT0_DpOT1_,comdat
	.align	2
	.weak	_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3dEESt10shared_ptrIT_ERKT0_DpOT1_
	.type	_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3dEESt10shared_ptrIT_ERKT0_DpOT1_, @function
_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3dEESt10shared_ptrIT_ERKT0_DpOT1_:
.LFB4426:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l %d3,-(%sp)
	move.l %d2,-(%sp)
	.cfi_offset 3, -12
	.cfi_offset 2, -16
	move.l %a1,%d2
	move.l 16(%fp),-(%sp)
	jsr _ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE
	addq.l #4,%sp
	move.l %a0,%d3
	move.l 12(%fp),-(%sp)
	jsr _ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
	addq.l #4,%sp
	move.l %a0,%d1
	move.l 8(%fp),%d0
	move.l %d3,-(%sp)
	move.l %d1,-(%sp)
	move.l %d0,-(%sp)
	move.l %d2,-(%sp)
	jsr _ZNSt10shared_ptrI6sphereEC1ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	lea (16,%sp),%sp
	move.l %d2,%a0
	move.l %d2,%a0
	move.l -8(%fp),%d2
	move.l -4(%fp),%d3
	unlk %fp
	rts
	.cfi_endproc
.LFE4426:
	.size	_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3dEESt10shared_ptrIT_ERKT0_DpOT1_, .-_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3dEESt10shared_ptrIT_ERKT0_DpOT1_
	.section	.text._ZSt4moveIRSt10shared_ptrI6sphereEEONSt16remove_referenceIT_E4typeEOS5_,"axG",@progbits,_ZSt4moveIRSt10shared_ptrI6sphereEEONSt16remove_referenceIT_E4typeEOS5_,comdat
	.align	2
	.weak	_ZSt4moveIRSt10shared_ptrI6sphereEEONSt16remove_referenceIT_E4typeEOS5_
	.type	_ZSt4moveIRSt10shared_ptrI6sphereEEONSt16remove_referenceIT_E4typeEOS5_, @function
_ZSt4moveIRSt10shared_ptrI6sphereEEONSt16remove_referenceIT_E4typeEOS5_:
.LFB4430:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l 8(%fp),%d0
	move.l %d0,%d1
	move.l %d1,%a0
	unlk %fp
	rts
	.cfi_endproc
.LFE4430:
	.size	_ZSt4moveIRSt10shared_ptrI6sphereEEONSt16remove_referenceIT_E4typeEOS5_, .-_ZSt4moveIRSt10shared_ptrI6sphereEEONSt16remove_referenceIT_E4typeEOS5_
	.section	.text._ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC2I6spherevEEOS_IT_LS2_2EE,"axG",@progbits,_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC5I6spherevEEOS_IT_LS2_2EE,comdat
	.align	2
	.weak	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC2I6spherevEEOS_IT_LS2_2EE
	.type	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC2I6spherevEEOS_IT_LS2_2EE, @function
_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC2I6spherevEEOS_IT_LS2_2EE:
.LFB4432:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l 12(%fp),%a0
	move.l (%a0),%d0
	move.l 8(%fp),%a0
	move.l %d0,(%a0)
	move.l 8(%fp),%d0
	addq.l #4,%d0
	move.l %d0,-(%sp)
	jsr _ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1Ev
	addq.l #4,%sp
	move.l 8(%fp),%d0
	addq.l #4,%d0
	move.l 12(%fp),%d1
	addq.l #4,%d1
	move.l %d1,-(%sp)
	move.l %d0,-(%sp)
	jsr _ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EE7_M_swapERS2_
	addq.l #8,%sp
	move.l 12(%fp),%a0
	clr.l (%a0)
	nop
	unlk %fp
	rts
	.cfi_endproc
.LFE4432:
	.size	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC2I6spherevEEOS_IT_LS2_2EE, .-_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC2I6spherevEEOS_IT_LS2_2EE
	.weak	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC1I6spherevEEOS_IT_LS2_2EE
	.set	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC1I6spherevEEOS_IT_LS2_2EE,_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC2I6spherevEEOS_IT_LS2_2EE
	.section	.text._ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE,"axG",@progbits,_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE,comdat
	.align	2
	.weak	_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE
	.type	_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE, @function
_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE:
.LFB4438:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l 8(%fp),%d0
	move.l %d0,%d1
	move.l %d1,%a0
	unlk %fp
	rts
	.cfi_endproc
.LFE4438:
	.size	_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE, .-_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE
	.section	.text._ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3iEESt10shared_ptrIT_ERKT0_DpOT1_,"axG",@progbits,_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3iEESt10shared_ptrIT_ERKT0_DpOT1_,comdat
	.align	2
	.weak	_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3iEESt10shared_ptrIT_ERKT0_DpOT1_
	.type	_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3iEESt10shared_ptrIT_ERKT0_DpOT1_, @function
_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3iEESt10shared_ptrIT_ERKT0_DpOT1_:
.LFB4439:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l %d3,-(%sp)
	move.l %d2,-(%sp)
	.cfi_offset 3, -12
	.cfi_offset 2, -16
	move.l %a1,%d2
	move.l 16(%fp),-(%sp)
	jsr _ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE
	addq.l #4,%sp
	move.l %a0,%d3
	move.l 12(%fp),-(%sp)
	jsr _ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
	addq.l #4,%sp
	move.l %a0,%d1
	move.l 8(%fp),%d0
	move.l %d3,-(%sp)
	move.l %d1,-(%sp)
	move.l %d0,-(%sp)
	move.l %d2,-(%sp)
	jsr _ZNSt10shared_ptrI6sphereEC1ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	lea (16,%sp),%sp
	move.l %d2,%a0
	move.l %d2,%a0
	move.l -8(%fp),%d2
	move.l -4(%fp),%d3
	unlk %fp
	rts
	.cfi_endproc
.LFE4439:
	.size	_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3iEESt10shared_ptrIT_ERKT0_DpOT1_, .-_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3iEESt10shared_ptrIT_ERKT0_DpOT1_
	.section	.text._ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE4seedEj,"axG",@progbits,_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE4seedEj,comdat
	.align	2
	.weak	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE4seedEj
	.type	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE4seedEj, @function
_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE4seedEj:
.LFB4494:
	.cfi_startproc
	link.w %fp,#-8
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l 12(%fp),-(%sp)
	jsr _ZNSt8__detail5__modIjLj0ELj1ELj0EEET_S1_
	addq.l #4,%sp
	move.l 8(%fp),%a0
	move.l %d0,(%a0)
	moveq #1,%d0
	move.l %d0,-4(%fp)
.L127:
	cmp.l #623,-4(%fp)
	jhi .L126
	move.l -4(%fp),%d0
	subq.l #1,%d0
	move.l 8(%fp),%a0
	move.l (%a0,%d0.l*4),-8(%fp)
	move.l -8(%fp),%d0
	moveq #30,%d1
	lsr.l %d1,%d0
	eor.l %d0,-8(%fp)
	move.l -8(%fp),%d0
	muls.l #1812433253,%d0
	move.l %d0,-8(%fp)
	move.l -4(%fp),-(%sp)
	jsr _ZNSt8__detail5__modIjLj624ELj1ELj0EEET_S1_
	addq.l #4,%sp
	add.l %d0,-8(%fp)
	move.l -8(%fp),-(%sp)
	jsr _ZNSt8__detail5__modIjLj0ELj1ELj0EEET_S1_
	addq.l #4,%sp
	move.l %d0,%d1
	move.l 8(%fp),%a0
	move.l -4(%fp),%d0
	move.l %d1,(%a0,%d0.l*4)
	addq.l #1,-4(%fp)
	jra .L127
.L126:
	move.l 8(%fp),%a0
	move.l #624,2496(%a0)
	nop
	unlk %fp
	rts
	.cfi_endproc
.LFE4494:
	.size	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE4seedEj, .-_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE4seedEj
	.section	.text._ZNSt8__detail8_AdaptorISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEdEC2ERS2_,"axG",@progbits,_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEdEC5ERS2_,comdat
	.align	2
	.weak	_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEdEC2ERS2_
	.type	_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEdEC2ERS2_, @function
_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEdEC2ERS2_:
.LFB4496:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l 8(%fp),%a0
	move.l 12(%fp),(%a0)
	nop
	unlk %fp
	rts
	.cfi_endproc
.LFE4496:
	.size	_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEdEC2ERS2_, .-_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEdEC2ERS2_
	.weak	_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEdEC1ERS2_
	.set	_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEdEC1ERS2_,_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEdEC2ERS2_
	.section	.text._ZNSt8__detail8_AdaptorISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEdEclEv,"axG",@progbits,_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEdEclEv,comdat
	.align	2
	.weak	_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEdEclEv
	.type	_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEdEclEv, @function
_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEdEclEv:
.LFB4498:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l 8(%fp),%a0
	move.l (%a0),%d0
	move.l %d0,-(%sp)
	jsr _ZSt18generate_canonicalIdLj53ESt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEET_RT1_
	addq.l #4,%sp
	unlk %fp
	rts
	.cfi_endproc
.LFE4498:
	.size	_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEdEclEv, .-_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEdEclEv
	.section	.text._ZNKSt25uniform_real_distributionIdE10param_type1bEv,"axG",@progbits,_ZNKSt25uniform_real_distributionIdE10param_type1bEv,comdat
	.align	2
	.weak	_ZNKSt25uniform_real_distributionIdE10param_type1bEv
	.type	_ZNKSt25uniform_real_distributionIdE10param_type1bEv, @function
_ZNKSt25uniform_real_distributionIdE10param_type1bEv:
.LFB4499:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l 8(%fp),%a0
	move.l 8(%a0),%d0
	move.l 12(%a0),%d1
	move.l %d1,-(%sp)
	move.l %d0,-(%sp)
	fmove.d (%sp)+,%fp0
	unlk %fp
	rts
	.cfi_endproc
.LFE4499:
	.size	_ZNKSt25uniform_real_distributionIdE10param_type1bEv, .-_ZNKSt25uniform_real_distributionIdE10param_type1bEv
	.section	.text._ZNKSt25uniform_real_distributionIdE10param_type1aEv,"axG",@progbits,_ZNKSt25uniform_real_distributionIdE10param_type1aEv,comdat
	.align	2
	.weak	_ZNKSt25uniform_real_distributionIdE10param_type1aEv
	.type	_ZNKSt25uniform_real_distributionIdE10param_type1aEv, @function
_ZNKSt25uniform_real_distributionIdE10param_type1aEv:
.LFB4500:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l 8(%fp),%a0
	move.l (%a0),%d0
	move.l 4(%a0),%d1
	move.l %d1,-(%sp)
	move.l %d0,-(%sp)
	fmove.d (%sp)+,%fp0
	unlk %fp
	rts
	.cfi_endproc
.LFE4500:
	.size	_ZNKSt25uniform_real_distributionIdE10param_type1aEv, .-_ZNKSt25uniform_real_distributionIdE10param_type1aEv
	.section	.text._ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2Ev,"axG",@progbits,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC5Ev,comdat
	.align	2
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2Ev
	.type	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2Ev, @function
_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2Ev:
.LFB4506:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l 8(%fp),%a0
	clr.l (%a0)
	nop
	unlk %fp
	rts
	.cfi_endproc
.LFE4506:
	.size	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2Ev, .-_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2Ev
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1Ev
	.set	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1Ev,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2Ev
	.section	.text._ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,"axG",@progbits,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,comdat
	.align	2
	.weak	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.type	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, @function
_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv:
.LFB4508:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	tst.l 8(%fp)
	jeq .L138
	move.l 8(%fp),%a0
	move.l (%a0),%d0
	move.l %d0,%a0
	addq.l #4,%a0
	move.l (%a0),%a0
	move.l 8(%fp),-(%sp)
	jsr (%a0)
	addq.l #4,%sp
.L138:
	nop
	unlk %fp
	rts
	.cfi_endproc
.LFE4508:
	.size	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, .-_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.section	.text._ZNSaISt10shared_ptrI8hittableEED2Ev,"axG",@progbits,_ZNSaISt10shared_ptrI8hittableEED5Ev,comdat
	.align	2
	.weak	_ZNSaISt10shared_ptrI8hittableEED2Ev
	.type	_ZNSaISt10shared_ptrI8hittableEED2Ev, @function
_ZNSaISt10shared_ptrI8hittableEED2Ev:
.LFB4510:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l 8(%fp),-(%sp)
	jsr _ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED2Ev
	addq.l #4,%sp
	nop
	unlk %fp
	rts
	.cfi_endproc
.LFE4510:
	.size	_ZNSaISt10shared_ptrI8hittableEED2Ev, .-_ZNSaISt10shared_ptrI8hittableEED2Ev
	.weak	_ZNSaISt10shared_ptrI8hittableEED1Ev
	.set	_ZNSaISt10shared_ptrI8hittableEED1Ev,_ZNSaISt10shared_ptrI8hittableEED2Ev
	.section	.text._ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE13_M_deallocateEPS2_j,"axG",@progbits,_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE13_M_deallocateEPS2_j,comdat
	.align	2
	.weak	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE13_M_deallocateEPS2_j
	.type	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE13_M_deallocateEPS2_j, @function
_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE13_M_deallocateEPS2_j:
.LFB4512:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	tst.l 12(%fp)
	jeq .L142
	move.l 8(%fp),%d0
	move.l 16(%fp),-(%sp)
	move.l 12(%fp),-(%sp)
	move.l %d0,-(%sp)
	jsr _ZNSt16allocator_traitsISaISt10shared_ptrI8hittableEEE10deallocateERS3_PS2_j
	lea (12,%sp),%sp
.L142:
	nop
	unlk %fp
	rts
	.cfi_endproc
.LFE4512:
	.size	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE13_M_deallocateEPS2_j, .-_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE13_M_deallocateEPS2_j
	.section	.text._ZSt8_DestroyIPSt10shared_ptrI8hittableEEvT_S4_,"axG",@progbits,_ZSt8_DestroyIPSt10shared_ptrI8hittableEEvT_S4_,comdat
	.align	2
	.weak	_ZSt8_DestroyIPSt10shared_ptrI8hittableEEvT_S4_
	.type	_ZSt8_DestroyIPSt10shared_ptrI8hittableEEvT_S4_, @function
_ZSt8_DestroyIPSt10shared_ptrI8hittableEEvT_S4_:
.LFB4513:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l 12(%fp),-(%sp)
	move.l 8(%fp),-(%sp)
	jsr _ZNSt12_Destroy_auxILb0EE9__destroyIPSt10shared_ptrI8hittableEEEvT_S6_
	addq.l #8,%sp
	nop
	unlk %fp
	rts
	.cfi_endproc
.LFE4513:
	.size	_ZSt8_DestroyIPSt10shared_ptrI8hittableEEvT_S4_, .-_ZSt8_DestroyIPSt10shared_ptrI8hittableEEvT_S4_
	.section	.text._ZN9__gnu_cxx13new_allocatorI6sphereEC2Ev,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorI6sphereEC5Ev,comdat
	.align	2
	.weak	_ZN9__gnu_cxx13new_allocatorI6sphereEC2Ev
	.type	_ZN9__gnu_cxx13new_allocatorI6sphereEC2Ev, @function
_ZN9__gnu_cxx13new_allocatorI6sphereEC2Ev:
.LFB4515:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	nop
	unlk %fp
	rts
	.cfi_endproc
.LFE4515:
	.size	_ZN9__gnu_cxx13new_allocatorI6sphereEC2Ev, .-_ZN9__gnu_cxx13new_allocatorI6sphereEC2Ev
	.weak	_ZN9__gnu_cxx13new_allocatorI6sphereEC1Ev
	.set	_ZN9__gnu_cxx13new_allocatorI6sphereEC1Ev,_ZN9__gnu_cxx13new_allocatorI6sphereEC2Ev
	.section	.text._ZN9__gnu_cxx13new_allocatorI6sphereED2Ev,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorI6sphereED5Ev,comdat
	.align	2
	.weak	_ZN9__gnu_cxx13new_allocatorI6sphereED2Ev
	.type	_ZN9__gnu_cxx13new_allocatorI6sphereED2Ev, @function
_ZN9__gnu_cxx13new_allocatorI6sphereED2Ev:
.LFB4518:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	nop
	unlk %fp
	rts
	.cfi_endproc
.LFE4518:
	.size	_ZN9__gnu_cxx13new_allocatorI6sphereED2Ev, .-_ZN9__gnu_cxx13new_allocatorI6sphereED2Ev
	.weak	_ZN9__gnu_cxx13new_allocatorI6sphereED1Ev
	.set	_ZN9__gnu_cxx13new_allocatorI6sphereED1Ev,_ZN9__gnu_cxx13new_allocatorI6sphereED2Ev
	.section	.text._ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,"axG",@progbits,_ZNSt10shared_ptrI6sphereEC5ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,comdat
	.align	2
	.weak	_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.type	_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_, @function
_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_:
.LFB4521:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l %d3,-(%sp)
	move.l %d2,-(%sp)
	.cfi_offset 3, -12
	.cfi_offset 2, -16
	move.l 8(%fp),%d2
	move.l 20(%fp),-(%sp)
	jsr _ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE
	addq.l #4,%sp
	move.l %a0,%d3
	move.l 16(%fp),-(%sp)
	jsr _ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
	addq.l #4,%sp
	move.l %a0,%d0
	move.l %d3,-(%sp)
	move.l %d0,-(%sp)
	move.l 12(%fp),-(%sp)
	move.l %d2,-(%sp)
	jsr _ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	lea (16,%sp),%sp
	nop
	move.l -8(%fp),%d2
	move.l -4(%fp),%d3
	unlk %fp
	rts
	.cfi_endproc
.LFE4521:
	.size	_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_, .-_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.weak	_ZNSt10shared_ptrI6sphereEC1ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.set	_ZNSt10shared_ptrI6sphereEC1ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.section	.text._ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EE7_M_swapERS2_,"axG",@progbits,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EE7_M_swapERS2_,comdat
	.align	2
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EE7_M_swapERS2_
	.type	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EE7_M_swapERS2_, @function
_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EE7_M_swapERS2_:
.LFB4526:
	.cfi_startproc
	link.w %fp,#-4
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l 12(%fp),%a0
	move.l (%a0),-4(%fp)
	move.l 8(%fp),%a0
	move.l (%a0),%d0
	move.l 12(%fp),%a0
	move.l %d0,(%a0)
	move.l 8(%fp),%a0
	move.l -4(%fp),(%a0)
	nop
	unlk %fp
	rts
	.cfi_endproc
.LFE4526:
	.size	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EE7_M_swapERS2_, .-_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EE7_M_swapERS2_
	.section	.text._ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,"axG",@progbits,_ZNSt10shared_ptrI6sphereEC5ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,comdat
	.align	2
	.weak	_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.type	_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_, @function
_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_:
.LFB4528:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l %d3,-(%sp)
	move.l %d2,-(%sp)
	.cfi_offset 3, -12
	.cfi_offset 2, -16
	move.l 8(%fp),%d2
	move.l 20(%fp),-(%sp)
	jsr _ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE
	addq.l #4,%sp
	move.l %a0,%d3
	move.l 16(%fp),-(%sp)
	jsr _ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
	addq.l #4,%sp
	move.l %a0,%d0
	move.l %d3,-(%sp)
	move.l %d0,-(%sp)
	move.l 12(%fp),-(%sp)
	move.l %d2,-(%sp)
	jsr _ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	lea (16,%sp),%sp
	nop
	move.l -8(%fp),%d2
	move.l -4(%fp),%d3
	unlk %fp
	rts
	.cfi_endproc
.LFE4528:
	.size	_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_, .-_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.weak	_ZNSt10shared_ptrI6sphereEC1ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.set	_ZNSt10shared_ptrI6sphereEC1ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.section	.text._ZNSt8__detail5__modIjLj0ELj1ELj0EEET_S1_,"axG",@progbits,_ZNSt8__detail5__modIjLj0ELj1ELj0EEET_S1_,comdat
	.align	2
	.weak	_ZNSt8__detail5__modIjLj0ELj1ELj0EEET_S1_
	.type	_ZNSt8__detail5__modIjLj0ELj1ELj0EEET_S1_, @function
_ZNSt8__detail5__modIjLj0ELj1ELj0EEET_S1_:
.LFB4567:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l 8(%fp),-(%sp)
	jsr _ZNSt8__detail4_ModIjLj0ELj1ELj0ELb1ELb0EE6__calcEj
	addq.l #4,%sp
	unlk %fp
	rts
	.cfi_endproc
.LFE4567:
	.size	_ZNSt8__detail5__modIjLj0ELj1ELj0EEET_S1_, .-_ZNSt8__detail5__modIjLj0ELj1ELj0EEET_S1_
	.section	.text._ZNSt8__detail5__modIjLj624ELj1ELj0EEET_S1_,"axG",@progbits,_ZNSt8__detail5__modIjLj624ELj1ELj0EEET_S1_,comdat
	.align	2
	.weak	_ZNSt8__detail5__modIjLj624ELj1ELj0EEET_S1_
	.type	_ZNSt8__detail5__modIjLj624ELj1ELj0EEET_S1_, @function
_ZNSt8__detail5__modIjLj624ELj1ELj0EEET_S1_:
.LFB4568:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l 8(%fp),-(%sp)
	jsr _ZNSt8__detail4_ModIjLj624ELj1ELj0ELb1ELb1EE6__calcEj
	addq.l #4,%sp
	unlk %fp
	rts
	.cfi_endproc
.LFE4568:
	.size	_ZNSt8__detail5__modIjLj624ELj1ELj0EEET_S1_, .-_ZNSt8__detail5__modIjLj624ELj1ELj0EEET_S1_
	.section	.text._ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE3minEv,"axG",@progbits,_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE3minEv,comdat
	.align	2
	.weak	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE3minEv
	.type	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE3minEv, @function
_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE3minEv:
.LFB4572:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	clr.l %d0
	unlk %fp
	rts
	.cfi_endproc
.LFE4572:
	.size	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE3minEv, .-_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE3minEv
	.section	.rodata
	.align	2
.LC5:
	.long	1075773440
	.long	2147483648
	.long	0
	.section	.text._ZSt18generate_canonicalIdLj53ESt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEET_RT1_,"axG",@progbits,_ZSt18generate_canonicalIdLj53ESt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEET_RT1_,comdat
	.align	2
	.weak	_ZSt18generate_canonicalIdLj53ESt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEET_RT1_
	.type	_ZSt18generate_canonicalIdLj53ESt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEET_RT1_, @function
_ZSt18generate_canonicalIdLj53ESt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEET_RT1_:
.LFB4569:
	.cfi_startproc
	link.w %fp,#-52
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l %d2,-(%sp)
	.cfi_offset 2, -64
	moveq #53,%d0
	move.l %d0,-32(%fp)
	fmove.x .LC5,%fp0
	fmove.x %fp0,-44(%fp)
	moveq #32,%d0
	move.l %d0,-48(%fp)
	moveq #2,%d1
	move.l %d1,-52(%fp)
	clr.l -16(%fp)
	clr.l -12(%fp)
	move.l #1072693248,-24(%fp)
	clr.l -20(%fp)
	moveq #2,%d0
	move.l %d0,-28(%fp)
.L158:
	tst.l -28(%fp)
	jeq .L156
	move.l 8(%fp),-(%sp)
	jsr _ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEclEv
	addq.l #4,%sp
	move.l %d0,%d2
	jsr _ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE3minEv
	move.l %d2,%d1
	sub.l %d0,%d1
	move.l %d1,%d0
	fmove.l %d0,%fp0
	tst.l %d0
	jge .L157
	fadd.d #0x41f0000000000000,%fp0
.L157:
	fmul.d -24(%fp),%fp0
	fmove.d -16(%fp),%fp1
	fadd.x %fp0,%fp1
	fmove.d %fp1,-16(%fp)
	fmove.d -24(%fp),%fp1
	fmove.x .LC5,%fp0
	fmul.x %fp1,%fp0
	fmove.d %fp0,-24(%fp)
	subq.l #1,-28(%fp)
	jra .L158
.L156:
	fmove.d -16(%fp),%fp0
	fdiv.d -24(%fp),%fp0
	fmove.d %fp0,-8(%fp)
	fmove.d -8(%fp),%fp1
	fmovecr #0x32,%fp0
	fcmp.x %fp0,%fp1
	fsge %d0
	neg.b %d0
	move.b %d0,%d0
	and.l #255,%d0
	tst.l %d0
	jeq .L159
	move.l #1072693247,-8(%fp)
	move.l #-1,-4(%fp)
.L159:
	move.l -8(%fp),%d0
	move.l -4(%fp),%d1
	move.l %d1,-(%sp)
	move.l %d0,-(%sp)
	fmove.d (%sp)+,%fp0
	move.l -56(%fp),%d2
	unlk %fp
	rts
	.cfi_endproc
.LFE4569:
	.size	_ZSt18generate_canonicalIdLj53ESt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEET_RT1_, .-_ZSt18generate_canonicalIdLj53ESt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEET_RT1_
	.section	.text._ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align	2
	.weak	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev
	.type	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev:
.LFB4579:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l #_ZTVSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE+8,%d0
	move.l 8(%fp),%a0
	move.l %d0,(%a0)
	nop
	unlk %fp
	rts
	.cfi_endproc
.LFE4579:
	.size	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED1Ev
	.set	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED1Ev,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED0Ev,"axG",@progbits,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align	2
	.weak	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED0Ev
	.type	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED0Ev, @function
_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED0Ev:
.LFB4581:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l 8(%fp),-(%sp)
	jsr _ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED1Ev
	addq.l #4,%sp
	pea 12.w
	move.l 8(%fp),-(%sp)
	jsr _ZdlPvj
	addq.l #8,%sp
	unlk %fp
	rts
	.cfi_endproc
.LFE4581:
	.size	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED0Ev, .-_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED0Ev
	.section	.text._ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED2Ev,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED5Ev,comdat
	.align	2
	.weak	_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED2Ev
	.type	_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED2Ev, @function
_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED2Ev:
.LFB4583:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	nop
	unlk %fp
	rts
	.cfi_endproc
.LFE4583:
	.size	_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED2Ev, .-_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED2Ev
	.weak	_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED1Ev
	.set	_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED1Ev,_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED2Ev
	.section	.text._ZNSt16allocator_traitsISaISt10shared_ptrI8hittableEEE10deallocateERS3_PS2_j,"axG",@progbits,_ZNSt16allocator_traitsISaISt10shared_ptrI8hittableEEE10deallocateERS3_PS2_j,comdat
	.align	2
	.weak	_ZNSt16allocator_traitsISaISt10shared_ptrI8hittableEEE10deallocateERS3_PS2_j
	.type	_ZNSt16allocator_traitsISaISt10shared_ptrI8hittableEEE10deallocateERS3_PS2_j, @function
_ZNSt16allocator_traitsISaISt10shared_ptrI8hittableEEE10deallocateERS3_PS2_j:
.LFB4585:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l 16(%fp),-(%sp)
	move.l 12(%fp),-(%sp)
	move.l 8(%fp),-(%sp)
	jsr _ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEE10deallocateEPS3_j
	lea (12,%sp),%sp
	nop
	unlk %fp
	rts
	.cfi_endproc
.LFE4585:
	.size	_ZNSt16allocator_traitsISaISt10shared_ptrI8hittableEEE10deallocateERS3_PS2_j, .-_ZNSt16allocator_traitsISaISt10shared_ptrI8hittableEEE10deallocateERS3_PS2_j
	.section	.text._ZNSt12_Destroy_auxILb0EE9__destroyIPSt10shared_ptrI8hittableEEEvT_S6_,"axG",@progbits,_ZNSt12_Destroy_auxILb0EE9__destroyIPSt10shared_ptrI8hittableEEEvT_S6_,comdat
	.align	2
	.weak	_ZNSt12_Destroy_auxILb0EE9__destroyIPSt10shared_ptrI8hittableEEEvT_S6_
	.type	_ZNSt12_Destroy_auxILb0EE9__destroyIPSt10shared_ptrI8hittableEEEvT_S6_, @function
_ZNSt12_Destroy_auxILb0EE9__destroyIPSt10shared_ptrI8hittableEEEvT_S6_:
.LFB4586:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
.L167:
	move.l 8(%fp),%d0
	cmp.l 12(%fp),%d0
	jeq .L168
	move.l 8(%fp),-(%sp)
	jsr _ZSt11__addressofISt10shared_ptrI8hittableEEPT_RS3_
	addq.l #4,%sp
	move.l %a0,%d0
	move.l %d0,-(%sp)
	jsr _ZSt8_DestroyISt10shared_ptrI8hittableEEvPT_
	addq.l #4,%sp
	addq.l #8,8(%fp)
	jra .L167
.L168:
	nop
	unlk %fp
	rts
	.cfi_endproc
.LFE4586:
	.size	_ZNSt12_Destroy_auxILb0EE9__destroyIPSt10shared_ptrI8hittableEEEvT_S6_, .-_ZNSt12_Destroy_auxILb0EE9__destroyIPSt10shared_ptrI8hittableEEEvT_S6_
	.section	.text._ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,"axG",@progbits,_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC5ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,comdat
	.align	2
	.weak	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.type	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_, @function
_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_:
.LFB4588:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l %d3,-(%sp)
	move.l %d2,-(%sp)
	.cfi_offset 3, -12
	.cfi_offset 2, -16
	move.l 8(%fp),%a0
	clr.l (%a0)
	move.l 8(%fp),%d2
	addq.l #4,%d2
	move.l 20(%fp),-(%sp)
	jsr _ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE
	addq.l #4,%sp
	move.l %a0,%d3
	move.l 16(%fp),-(%sp)
	jsr _ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
	addq.l #4,%sp
	move.l %a0,%d1
	move.l 8(%fp),%d0
	move.l %d3,-(%sp)
	move.l %d1,-(%sp)
	move.l 12(%fp),-(%sp)
	move.l %d0,-(%sp)
	move.l %d2,-(%sp)
	jsr _ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_
	lea (20,%sp),%sp
	move.l 8(%fp),%a0
	move.l (%a0),%d0
	move.l %d0,-(%sp)
	move.l 8(%fp),-(%sp)
	jsr _ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EE31_M_enable_shared_from_this_withIS0_S0_EENSt9enable_ifIXntsrNS3_15__has_esft_baseIT0_vEE5valueEvE4typeEPT_
	addq.l #8,%sp
	nop
	move.l -8(%fp),%d2
	move.l -4(%fp),%d3
	unlk %fp
	rts
	.cfi_endproc
.LFE4588:
	.size	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_, .-_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.weak	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC1ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.set	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC1ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.section	.text._ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,"axG",@progbits,_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC5ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,comdat
	.align	2
	.weak	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.type	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_, @function
_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_:
.LFB4591:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l %d3,-(%sp)
	move.l %d2,-(%sp)
	.cfi_offset 3, -12
	.cfi_offset 2, -16
	move.l 8(%fp),%a0
	clr.l (%a0)
	move.l 8(%fp),%d2
	addq.l #4,%d2
	move.l 20(%fp),-(%sp)
	jsr _ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE
	addq.l #4,%sp
	move.l %a0,%d3
	move.l 16(%fp),-(%sp)
	jsr _ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
	addq.l #4,%sp
	move.l %a0,%d1
	move.l 8(%fp),%d0
	move.l %d3,-(%sp)
	move.l %d1,-(%sp)
	move.l 12(%fp),-(%sp)
	move.l %d0,-(%sp)
	move.l %d2,-(%sp)
	jsr _ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_
	lea (20,%sp),%sp
	move.l 8(%fp),%a0
	move.l (%a0),%d0
	move.l %d0,-(%sp)
	move.l 8(%fp),-(%sp)
	jsr _ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EE31_M_enable_shared_from_this_withIS0_S0_EENSt9enable_ifIXntsrNS3_15__has_esft_baseIT0_vEE5valueEvE4typeEPT_
	addq.l #8,%sp
	nop
	move.l -8(%fp),%d2
	move.l -4(%fp),%d3
	unlk %fp
	rts
	.cfi_endproc
.LFE4591:
	.size	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_, .-_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.weak	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC1ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.set	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC1ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.section	.text._ZNSt8__detail4_ModIjLj0ELj1ELj0ELb1ELb0EE6__calcEj,"axG",@progbits,_ZNSt8__detail4_ModIjLj0ELj1ELj0ELb1ELb0EE6__calcEj,comdat
	.align	2
	.weak	_ZNSt8__detail4_ModIjLj0ELj1ELj0ELb1ELb0EE6__calcEj
	.type	_ZNSt8__detail4_ModIjLj0ELj1ELj0ELb1ELb0EE6__calcEj, @function
_ZNSt8__detail4_ModIjLj0ELj1ELj0ELb1ELb0EE6__calcEj:
.LFB4621:
	.cfi_startproc
	link.w %fp,#-4
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l 8(%fp),-4(%fp)
	move.l -4(%fp),%d0
	unlk %fp
	rts
	.cfi_endproc
.LFE4621:
	.size	_ZNSt8__detail4_ModIjLj0ELj1ELj0ELb1ELb0EE6__calcEj, .-_ZNSt8__detail4_ModIjLj0ELj1ELj0ELb1ELb0EE6__calcEj
	.section	.text._ZNSt8__detail4_ModIjLj624ELj1ELj0ELb1ELb1EE6__calcEj,"axG",@progbits,_ZNSt8__detail4_ModIjLj624ELj1ELj0ELb1ELb1EE6__calcEj,comdat
	.align	2
	.weak	_ZNSt8__detail4_ModIjLj624ELj1ELj0ELb1ELb1EE6__calcEj
	.type	_ZNSt8__detail4_ModIjLj624ELj1ELj0ELb1ELb1EE6__calcEj, @function
_ZNSt8__detail4_ModIjLj624ELj1ELj0ELb1ELb1EE6__calcEj:
.LFB4622:
	.cfi_startproc
	link.w %fp,#-4
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l %d2,-(%sp)
	.cfi_offset 2, -16
	move.l 8(%fp),-4(%fp)
	move.l -4(%fp),%d2
	move.l %d2,%d0
	lsr.l #4,%d0
	move.l %d0,%d1
	mulu.l #440509467,%d0:%d1
	move.l %d0,%d1
	lsr.l #2,%d1
	move.l %d1,%d0
	lsl.l #2,%d0
	add.l %d1,%d0
	lsl.l #3,%d0
	sub.l %d1,%d0
	lsl.l #4,%d0
	move.l %d2,%d1
	sub.l %d0,%d1
	move.l %d1,%d0
	move.l %d0,-4(%fp)
	move.l -4(%fp),%d0
	move.l (%sp)+,%d2
	unlk %fp
	rts
	.cfi_endproc
.LFE4622:
	.size	_ZNSt8__detail4_ModIjLj624ELj1ELj0ELb1ELb1EE6__calcEj, .-_ZNSt8__detail4_ModIjLj624ELj1ELj0ELb1ELb1EE6__calcEj
	.section	.text._ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEclEv,"axG",@progbits,_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEclEv,comdat
	.align	2
	.weak	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEclEv
	.type	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEclEv, @function
_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEclEv:
.LFB4623:
	.cfi_startproc
	link.w %fp,#-4
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l 8(%fp),%a0
	move.l 2496(%a0),%d0
	cmp.l #623,%d0
	jls .L176
	move.l 8(%fp),-(%sp)
	jsr _ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE11_M_gen_randEv
	addq.l #4,%sp
.L176:
	move.l 8(%fp),%a0
	move.l 2496(%a0),%d0
	move.l %d0,%d1
	addq.l #1,%d1
	move.l 8(%fp),%a0
	move.l %d1,2496(%a0)
	move.l 8(%fp),%a0
	move.l (%a0,%d0.l*4),-4(%fp)
	move.l -4(%fp),%d0
	moveq #11,%d1
	lsr.l %d1,%d0
	eor.l %d0,-4(%fp)
	move.l -4(%fp),%d0
	lsl.l #7,%d0
	and.l #-1658038656,%d0
	eor.l %d0,-4(%fp)
	move.l -4(%fp),%d0
	moveq #15,%d1
	lsl.l %d1,%d0
	and.l #-272236544,%d0
	eor.l %d0,-4(%fp)
	move.l -4(%fp),%d0
	moveq #18,%d1
	lsr.l %d1,%d0
	eor.l %d0,-4(%fp)
	move.l -4(%fp),%d0
	unlk %fp
	rts
	.cfi_endproc
.LFE4623:
	.size	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEclEv, .-_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEclEv
	.section	.text._ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEE10deallocateEPS3_j,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEE10deallocateEPS3_j,comdat
	.align	2
	.weak	_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEE10deallocateEPS3_j
	.type	_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEE10deallocateEPS3_j, @function
_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEE10deallocateEPS3_j:
.LFB4625:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l 12(%fp),-(%sp)
	jsr _ZdlPv
	addq.l #4,%sp
	nop
	unlk %fp
	rts
	.cfi_endproc
.LFE4625:
	.size	_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEE10deallocateEPS3_j, .-_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEE10deallocateEPS3_j
	.section	.text._ZSt11__addressofISt10shared_ptrI8hittableEEPT_RS3_,"axG",@progbits,_ZSt11__addressofISt10shared_ptrI8hittableEEPT_RS3_,comdat
	.align	2
	.weak	_ZSt11__addressofISt10shared_ptrI8hittableEEPT_RS3_
	.type	_ZSt11__addressofISt10shared_ptrI8hittableEEPT_RS3_, @function
_ZSt11__addressofISt10shared_ptrI8hittableEEPT_RS3_:
.LFB4626:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l 8(%fp),%d0
	move.l %d0,%d1
	move.l %d1,%a0
	unlk %fp
	rts
	.cfi_endproc
.LFE4626:
	.size	_ZSt11__addressofISt10shared_ptrI8hittableEEPT_RS3_, .-_ZSt11__addressofISt10shared_ptrI8hittableEEPT_RS3_
	.section	.text._ZSt8_DestroyISt10shared_ptrI8hittableEEvPT_,"axG",@progbits,_ZSt8_DestroyISt10shared_ptrI8hittableEEvPT_,comdat
	.align	2
	.weak	_ZSt8_DestroyISt10shared_ptrI8hittableEEvPT_
	.type	_ZSt8_DestroyISt10shared_ptrI8hittableEEvPT_, @function
_ZSt8_DestroyISt10shared_ptrI8hittableEEvPT_:
.LFB4627:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l 8(%fp),-(%sp)
	jsr _ZNSt10shared_ptrI8hittableED1Ev
	addq.l #4,%sp
	nop
	unlk %fp
	rts
	.cfi_endproc
.LFE4627:
	.size	_ZSt8_DestroyISt10shared_ptrI8hittableEEvPT_, .-_ZSt8_DestroyISt10shared_ptrI8hittableEEvPT_
	.section	.text._ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_,"axG",@progbits,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC5I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_,comdat
	.align	2
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_
	.type	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_, @function
_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_:
.LFB4629:
	.cfi_startproc
	.cfi_personality 0,__gxx_personality_v0
	.cfi_lsda 0,.LLSDA4629
	link.w %fp,#-20
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	movem.l #15392,-(%sp)
	.cfi_offset 2, -48
	.cfi_offset 3, -44
	.cfi_offset 4, -40
	.cfi_offset 5, -36
	.cfi_offset 10, -32
	move.l 16(%fp),%d0
	move.l %d0,-(%sp)
	lea (-10,%fp),%a0
	move.l %a0,-(%sp)
	jsr _ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC1IS0_EERKSaIT_E
	addq.l #8,%sp
	lea (-10,%fp),%a0
	move.l %a0,-(%sp)
	lea (-18,%fp),%a0
	move.l %a0,%a1
.LEHB18:
	.cfi_escape 0x2e,0x4
	jsr _ZSt18__allocate_guardedISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEESt15__allocated_ptrIT_ERS8_
.LEHE18:
	addq.l #4,%sp
	lea (-18,%fp),%a0
	move.l %a0,-(%sp)
	jsr _ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE3getEv
	addq.l #4,%sp
	move.l %a0,-4(%fp)
	move.l 16(%fp),%d0
	move.l %d0,-(%sp)
	lea (-9,%fp),%a0
	move.l %a0,-(%sp)
	jsr _ZNSaI6sphereEC1ERKS0_
	addq.l #8,%sp
	lea (-9,%fp),%a2
	move.l 20(%fp),-(%sp)
	jsr _ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
	addq.l #4,%sp
	move.l %a0,%d4
	move.l 24(%fp),-(%sp)
	jsr _ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE
	addq.l #4,%sp
	move.l %a0,%d5
	move.l -4(%fp),%d3
	move.l %d3,-(%sp)
	pea 56.w
	jsr _ZnwjPv
	addq.l #8,%sp
	move.l %a0,%d2
	move.l %d5,-(%sp)
	move.l %d4,-(%sp)
	move.l %a2,-(%sp)
	move.l %d2,-(%sp)
.LEHB19:
	.cfi_escape 0x2e,0x10
	jsr _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC1IJ4vec3dEEES1_DpOT_
.LEHE19:
	lea (16,%sp),%sp
	move.l %d2,-8(%fp)
	lea (-9,%fp),%a0
	move.l %a0,-(%sp)
	jsr _ZNSaI6sphereED1Ev
	addq.l #4,%sp
	clr.l -(%sp)
	lea (-18,%fp),%a0
	move.l %a0,-(%sp)
	jsr _ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEaSEDn
	addq.l #8,%sp
	move.l 8(%fp),%a0
	move.l -8(%fp),(%a0)
	move.l -8(%fp),-(%sp)
	jsr _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv
	addq.l #4,%sp
	move.l %a0,%d0
	move.l 12(%fp),%a0
	move.l %d0,(%a0)
	lea (-18,%fp),%a0
	move.l %a0,-(%sp)
	jsr _ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED1Ev
	addq.l #4,%sp
	lea (-10,%fp),%a0
	move.l %a0,-(%sp)
	jsr _ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED1Ev
	addq.l #4,%sp
	jra .L187
.L186:
	move.l %d0,%d4
	move.l %d3,-(%sp)
	move.l %d2,-(%sp)
	jsr _ZdlPvS_
	addq.l #8,%sp
	move.l %d4,%d2
	lea (-9,%fp),%a0
	move.l %a0,-(%sp)
	jsr _ZNSaI6sphereED1Ev
	addq.l #4,%sp
	lea (-18,%fp),%a0
	move.l %a0,-(%sp)
	jsr _ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED1Ev
	addq.l #4,%sp
	jra .L184
.L185:
	move.l %d0,%d2
.L184:
	lea (-10,%fp),%a0
	move.l %a0,-(%sp)
	jsr _ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED1Ev
	addq.l #4,%sp
	move.l %d2,%d0
	move.l %d0,-(%sp)
.LEHB20:
	jsr _Unwind_Resume
.LEHE20:
.L187:
	movem.l -40(%fp),#1084
	unlk %fp
	rts
	.cfi_endproc
.LFE4629:
	.section	.gcc_except_table
.LLSDA4629:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4629-.LLSDACSB4629
.LLSDACSB4629:
	.uleb128 .LEHB18-.LFB4629
	.uleb128 .LEHE18-.LEHB18
	.uleb128 .L185-.LFB4629
	.uleb128 0
	.uleb128 .LEHB19-.LFB4629
	.uleb128 .LEHE19-.LEHB19
	.uleb128 .L186-.LFB4629
	.uleb128 0
	.uleb128 .LEHB20-.LFB4629
	.uleb128 .LEHE20-.LEHB20
	.uleb128 0
	.uleb128 0
.LLSDACSE4629:
	.section	.text._ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_,"axG",@progbits,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC5I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_,comdat
	.size	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_, .-_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_
	.set	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_
	.section	.text._ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EE31_M_enable_shared_from_this_withIS0_S0_EENSt9enable_ifIXntsrNS3_15__has_esft_baseIT0_vEE5valueEvE4typeEPT_,"axG",@progbits,_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EE31_M_enable_shared_from_this_withIS0_S0_EENSt9enable_ifIXntsrNS3_15__has_esft_baseIT0_vEE5valueEvE4typeEPT_,comdat
	.align	2
	.weak	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EE31_M_enable_shared_from_this_withIS0_S0_EENSt9enable_ifIXntsrNS3_15__has_esft_baseIT0_vEE5valueEvE4typeEPT_
	.type	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EE31_M_enable_shared_from_this_withIS0_S0_EENSt9enable_ifIXntsrNS3_15__has_esft_baseIT0_vEE5valueEvE4typeEPT_, @function
_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EE31_M_enable_shared_from_this_withIS0_S0_EENSt9enable_ifIXntsrNS3_15__has_esft_baseIT0_vEE5valueEvE4typeEPT_:
.LFB4631:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	nop
	unlk %fp
	rts
	.cfi_endproc
.LFE4631:
	.size	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EE31_M_enable_shared_from_this_withIS0_S0_EENSt9enable_ifIXntsrNS3_15__has_esft_baseIT0_vEE5valueEvE4typeEPT_, .-_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EE31_M_enable_shared_from_this_withIS0_S0_EENSt9enable_ifIXntsrNS3_15__has_esft_baseIT0_vEE5valueEvE4typeEPT_
	.section	.text._ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_,"axG",@progbits,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC5I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_,comdat
	.align	2
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_
	.type	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_, @function
_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_:
.LFB4633:
	.cfi_startproc
	.cfi_personality 0,__gxx_personality_v0
	.cfi_lsda 0,.LLSDA4633
	link.w %fp,#-20
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	movem.l #15392,-(%sp)
	.cfi_offset 2, -48
	.cfi_offset 3, -44
	.cfi_offset 4, -40
	.cfi_offset 5, -36
	.cfi_offset 10, -32
	move.l 16(%fp),%d0
	move.l %d0,-(%sp)
	lea (-10,%fp),%a0
	move.l %a0,-(%sp)
	jsr _ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC1IS0_EERKSaIT_E
	addq.l #8,%sp
	lea (-10,%fp),%a0
	move.l %a0,-(%sp)
	lea (-18,%fp),%a0
	move.l %a0,%a1
.LEHB21:
	.cfi_escape 0x2e,0x4
	jsr _ZSt18__allocate_guardedISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEESt15__allocated_ptrIT_ERS8_
.LEHE21:
	addq.l #4,%sp
	lea (-18,%fp),%a0
	move.l %a0,-(%sp)
	jsr _ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE3getEv
	addq.l #4,%sp
	move.l %a0,-4(%fp)
	move.l 16(%fp),%d0
	move.l %d0,-(%sp)
	lea (-9,%fp),%a0
	move.l %a0,-(%sp)
	jsr _ZNSaI6sphereEC1ERKS0_
	addq.l #8,%sp
	lea (-9,%fp),%a2
	move.l 20(%fp),-(%sp)
	jsr _ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
	addq.l #4,%sp
	move.l %a0,%d4
	move.l 24(%fp),-(%sp)
	jsr _ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE
	addq.l #4,%sp
	move.l %a0,%d5
	move.l -4(%fp),%d3
	move.l %d3,-(%sp)
	pea 56.w
	jsr _ZnwjPv
	addq.l #8,%sp
	move.l %a0,%d2
	move.l %d5,-(%sp)
	move.l %d4,-(%sp)
	move.l %a2,-(%sp)
	move.l %d2,-(%sp)
.LEHB22:
	.cfi_escape 0x2e,0x10
	jsr _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC1IJ4vec3iEEES1_DpOT_
.LEHE22:
	lea (16,%sp),%sp
	move.l %d2,-8(%fp)
	lea (-9,%fp),%a0
	move.l %a0,-(%sp)
	jsr _ZNSaI6sphereED1Ev
	addq.l #4,%sp
	clr.l -(%sp)
	lea (-18,%fp),%a0
	move.l %a0,-(%sp)
	jsr _ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEaSEDn
	addq.l #8,%sp
	move.l 8(%fp),%a0
	move.l -8(%fp),(%a0)
	move.l -8(%fp),-(%sp)
	jsr _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv
	addq.l #4,%sp
	move.l %a0,%d0
	move.l 12(%fp),%a0
	move.l %d0,(%a0)
	lea (-18,%fp),%a0
	move.l %a0,-(%sp)
	jsr _ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED1Ev
	addq.l #4,%sp
	lea (-10,%fp),%a0
	move.l %a0,-(%sp)
	jsr _ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED1Ev
	addq.l #4,%sp
	jra .L194
.L193:
	move.l %d0,%d4
	move.l %d3,-(%sp)
	move.l %d2,-(%sp)
	jsr _ZdlPvS_
	addq.l #8,%sp
	move.l %d4,%d2
	lea (-9,%fp),%a0
	move.l %a0,-(%sp)
	jsr _ZNSaI6sphereED1Ev
	addq.l #4,%sp
	lea (-18,%fp),%a0
	move.l %a0,-(%sp)
	jsr _ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED1Ev
	addq.l #4,%sp
	jra .L191
.L192:
	move.l %d0,%d2
.L191:
	lea (-10,%fp),%a0
	move.l %a0,-(%sp)
	jsr _ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED1Ev
	addq.l #4,%sp
	move.l %d2,%d0
	move.l %d0,-(%sp)
.LEHB23:
	jsr _Unwind_Resume
.LEHE23:
.L194:
	movem.l -40(%fp),#1084
	unlk %fp
	rts
	.cfi_endproc
.LFE4633:
	.section	.gcc_except_table
.LLSDA4633:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4633-.LLSDACSB4633
.LLSDACSB4633:
	.uleb128 .LEHB21-.LFB4633
	.uleb128 .LEHE21-.LEHB21
	.uleb128 .L192-.LFB4633
	.uleb128 0
	.uleb128 .LEHB22-.LFB4633
	.uleb128 .LEHE22-.LEHB22
	.uleb128 .L193-.LFB4633
	.uleb128 0
	.uleb128 .LEHB23-.LFB4633
	.uleb128 .LEHE23-.LEHB23
	.uleb128 0
	.uleb128 0
.LLSDACSE4633:
	.section	.text._ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_,"axG",@progbits,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC5I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_,comdat
	.size	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_, .-_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_
	.set	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_
	.section	.text._ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE11_M_gen_randEv,"axG",@progbits,_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE11_M_gen_randEv,comdat
	.align	2
	.weak	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE11_M_gen_randEv
	.type	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE11_M_gen_randEv, @function
_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE11_M_gen_randEv:
.LFB4661:
	.cfi_startproc
	link.w %fp,#-28
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l %d2,-(%sp)
	.cfi_offset 2, -40
	move.l #-2147483648,-12(%fp)
	move.l #2147483647,-16(%fp)
	clr.l -4(%fp)
.L199:
	cmp.l #226,-4(%fp)
	jhi .L196
	move.l 8(%fp),%a0
	move.l -4(%fp),%d0
	move.l (%a0,%d0.l*4),%d0
	move.l %d0,%d1
	and.l #-2147483648,%d1
	move.l -4(%fp),%d0
	addq.l #1,%d0
	move.l 8(%fp),%a0
	move.l (%a0,%d0.l*4),%d0
	bclr #31,%d0
	move.l %d1,%d2
	or.l %d0,%d2
	move.l %d2,-20(%fp)
	move.l -4(%fp),%d0
	add.l #397,%d0
	move.l 8(%fp),%a0
	move.l (%a0,%d0.l*4),%d1
	move.l -20(%fp),%d0
	lsr.l #1,%d0
	eor.l %d0,%d1
	moveq #1,%d0
	and.l -20(%fp),%d0
	tst.l %d0
	jeq .L197
	move.l #-1727483681,%d0
	jra .L198
.L197:
	clr.l %d0
.L198:
	eor.l %d0,%d1
	move.l 8(%fp),%a0
	move.l -4(%fp),%d0
	move.l %d1,(%a0,%d0.l*4)
	addq.l #1,-4(%fp)
	jra .L199
.L196:
	move.l #227,-8(%fp)
.L203:
	cmp.l #622,-8(%fp)
	jhi .L200
	move.l 8(%fp),%a0
	move.l -8(%fp),%d0
	move.l (%a0,%d0.l*4),%d0
	move.l %d0,%d1
	and.l #-2147483648,%d1
	move.l -8(%fp),%d0
	addq.l #1,%d0
	move.l 8(%fp),%a0
	move.l (%a0,%d0.l*4),%d0
	bclr #31,%d0
	move.l %d1,%d2
	or.l %d0,%d2
	move.l %d2,-24(%fp)
	move.l -8(%fp),%d0
	add.l #-227,%d0
	move.l 8(%fp),%a0
	move.l (%a0,%d0.l*4),%d1
	move.l -24(%fp),%d0
	lsr.l #1,%d0
	eor.l %d0,%d1
	moveq #1,%d0
	and.l -24(%fp),%d0
	tst.l %d0
	jeq .L201
	move.l #-1727483681,%d0
	jra .L202
.L201:
	clr.l %d0
.L202:
	eor.l %d0,%d1
	move.l 8(%fp),%a0
	move.l -8(%fp),%d0
	move.l %d1,(%a0,%d0.l*4)
	addq.l #1,-8(%fp)
	jra .L203
.L200:
	move.l 8(%fp),%a0
	move.l 2492(%a0),%d0
	move.l %d0,%d1
	and.l #-2147483648,%d1
	move.l 8(%fp),%a0
	move.l (%a0),%d0
	bclr #31,%d0
	move.l %d1,%d2
	or.l %d0,%d2
	move.l %d2,-28(%fp)
	move.l 8(%fp),%a0
	move.l 1584(%a0),%d1
	move.l -28(%fp),%d0
	lsr.l #1,%d0
	eor.l %d0,%d1
	moveq #1,%d0
	and.l -28(%fp),%d0
	tst.l %d0
	jeq .L204
	move.l #-1727483681,%d0
	jra .L205
.L204:
	clr.l %d0
.L205:
	eor.l %d1,%d0
	move.l 8(%fp),%a0
	move.l %d0,2492(%a0)
	move.l 8(%fp),%a0
	clr.l 2496(%a0)
	nop
	move.l (%sp)+,%d2
	unlk %fp
	rts
	.cfi_endproc
.LFE4661:
	.size	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE11_M_gen_randEv, .-_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE11_M_gen_randEv
	.section	.text._ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC2IS0_EERKSaIT_E,"axG",@progbits,_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC5IS0_EERKSaIT_E,comdat
	.align	2
	.weak	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC2IS0_EERKSaIT_E
	.type	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC2IS0_EERKSaIT_E, @function
_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC2IS0_EERKSaIT_E:
.LFB4664:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l 8(%fp),-(%sp)
	jsr _ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC2Ev
	addq.l #4,%sp
	nop
	unlk %fp
	rts
	.cfi_endproc
.LFE4664:
	.size	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC2IS0_EERKSaIT_E, .-_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC2IS0_EERKSaIT_E
	.weak	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC1IS0_EERKSaIT_E
	.set	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC1IS0_EERKSaIT_E,_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC2IS0_EERKSaIT_E
	.section	.text._ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED2Ev,"axG",@progbits,_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED5Ev,comdat
	.align	2
	.weak	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED2Ev
	.type	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED2Ev, @function
_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED2Ev:
.LFB4667:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l 8(%fp),-(%sp)
	jsr _ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED2Ev
	addq.l #4,%sp
	nop
	unlk %fp
	rts
	.cfi_endproc
.LFE4667:
	.size	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED2Ev, .-_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED2Ev
	.weak	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED1Ev
	.set	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED1Ev,_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED2Ev
	.section	.text._ZSt18__allocate_guardedISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEESt15__allocated_ptrIT_ERS8_,"axG",@progbits,_ZSt18__allocate_guardedISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEESt15__allocated_ptrIT_ERS8_,comdat
	.align	2
	.weak	_ZSt18__allocate_guardedISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEESt15__allocated_ptrIT_ERS8_
	.type	_ZSt18__allocate_guardedISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEESt15__allocated_ptrIT_ERS8_, @function
_ZSt18__allocate_guardedISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEESt15__allocated_ptrIT_ERS8_:
.LFB4669:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l %d2,-(%sp)
	.cfi_offset 2, -12
	move.l %a1,%d2
	pea 1.w
	move.l 8(%fp),-(%sp)
	jsr _ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE8allocateERS6_j
	addq.l #8,%sp
	move.l %a0,%d0
	move.l %d0,-(%sp)
	move.l 8(%fp),-(%sp)
	move.l %d2,-(%sp)
	jsr _ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC1ERS6_PS5_
	lea (12,%sp),%sp
	move.l %d2,%a0
	move.l %d2,%a0
	move.l -4(%fp),%d2
	unlk %fp
	rts
	.cfi_endproc
.LFE4669:
	.size	_ZSt18__allocate_guardedISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEESt15__allocated_ptrIT_ERS8_, .-_ZSt18__allocate_guardedISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEESt15__allocated_ptrIT_ERS8_
	.section	.text._ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED2Ev,"axG",@progbits,_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED5Ev,comdat
	.align	2
	.weak	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED2Ev
	.type	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED2Ev, @function
_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED2Ev:
.LFB4671:
	.cfi_startproc
	.cfi_personality 0,__gxx_personality_v0
	.cfi_lsda 0,.LLSDA4671
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l 8(%fp),%a0
	move.l 4(%a0),%d0
	tst.l %d0
	jeq .L212
	move.l 8(%fp),%a0
	move.l 4(%a0),%d1
	move.l 8(%fp),%a0
	move.l (%a0),%d0
	pea 1.w
	move.l %d1,-(%sp)
	move.l %d0,-(%sp)
	jsr _ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE10deallocateERS6_PS5_j
	lea (12,%sp),%sp
.L212:
	nop
	unlk %fp
	rts
	.cfi_endproc
.LFE4671:
	.section	.gcc_except_table
.LLSDA4671:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4671-.LLSDACSB4671
.LLSDACSB4671:
.LLSDACSE4671:
	.section	.text._ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED2Ev,"axG",@progbits,_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED5Ev,comdat
	.size	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED2Ev, .-_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED2Ev
	.weak	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED1Ev
	.set	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED1Ev,_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED2Ev
	.section	.text._ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE3getEv,"axG",@progbits,_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE3getEv,comdat
	.align	2
	.weak	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE3getEv
	.type	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE3getEv, @function
_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE3getEv:
.LFB4676:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l 8(%fp),%a0
	move.l 4(%a0),%d0
	move.l %d0,-(%sp)
	jsr _ZSt12__to_addressISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEPT_S7_
	addq.l #4,%sp
	move.l %a0,%d0
	move.l %d0,%d1
	move.l %d1,%a0
	unlk %fp
	rts
	.cfi_endproc
.LFE4676:
	.size	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE3getEv, .-_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE3getEv
	.section	.text._ZNSaI6sphereEC2ERKS0_,"axG",@progbits,_ZNSaI6sphereEC5ERKS0_,comdat
	.align	2
	.weak	_ZNSaI6sphereEC2ERKS0_
	.type	_ZNSaI6sphereEC2ERKS0_, @function
_ZNSaI6sphereEC2ERKS0_:
.LFB4678:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l 12(%fp),-(%sp)
	move.l 8(%fp),-(%sp)
	jsr _ZN9__gnu_cxx13new_allocatorI6sphereEC2ERKS2_
	addq.l #8,%sp
	nop
	unlk %fp
	rts
	.cfi_endproc
.LFE4678:
	.size	_ZNSaI6sphereEC2ERKS0_, .-_ZNSaI6sphereEC2ERKS0_
	.weak	_ZNSaI6sphereEC1ERKS0_
	.set	_ZNSaI6sphereEC1ERKS0_,_ZNSaI6sphereEC2ERKS0_
	.section	.text._ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED2Ev,"axG",@progbits,_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED5Ev,comdat
	.align	2
	.weak	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED2Ev
	.type	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED2Ev, @function
_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED2Ev:
.LFB4683:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l 8(%fp),-(%sp)
	jsr _ZNSaI6sphereED2Ev
	addq.l #4,%sp
	nop
	unlk %fp
	rts
	.cfi_endproc
.LFE4683:
	.size	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED2Ev, .-_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED2Ev
	.weak	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED1Ev
	.set	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED1Ev,_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED2Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD2Ev,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD5Ev,comdat
	.align	2
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD2Ev
	.type	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD2Ev, @function
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD2Ev:
.LFB4685:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l 8(%fp),-(%sp)
	jsr _ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED2Ev
	addq.l #4,%sp
	nop
	unlk %fp
	rts
	.cfi_endproc
.LFE4685:
	.size	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD2Ev, .-_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD2Ev
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD1Ev
	.set	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD1Ev,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD2Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3dEEES1_DpOT_,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC5IJ4vec3dEEES1_DpOT_,comdat
	.align	2
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3dEEES1_DpOT_
	.type	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3dEEES1_DpOT_, @function
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3dEEES1_DpOT_:
.LFB4687:
	.cfi_startproc
	.cfi_personality 0,__gxx_personality_v0
	.cfi_lsda 0,.LLSDA4687
	link.w %fp,#-4
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l %d3,-(%sp)
	move.l %d2,-(%sp)
	.cfi_offset 3, -16
	.cfi_offset 2, -20
	move.l 8(%fp),%d0
	move.l %d0,-(%sp)
	jsr _ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC2Ev
	addq.l #4,%sp
	move.l #_ZTVSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE+8,%d0
	move.l 8(%fp),%a0
	move.l %d0,(%a0)
	moveq #12,%d2
	add.l 8(%fp),%d2
	move.l 12(%fp),-(%sp)
	move.l %fp,%d0
	subq.l #1,%d0
	move.l %d0,-(%sp)
	jsr _ZNSaI6sphereEC1ERKS0_
	addq.l #8,%sp
	move.l %fp,%d0
	subq.l #1,%d0
	move.l %d0,-(%sp)
	move.l %d2,-(%sp)
	jsr _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC1ES1_
	addq.l #8,%sp
	move.l %fp,%d0
	subq.l #1,%d0
	move.l %d0,-(%sp)
	jsr _ZNSaI6sphereED1Ev
	addq.l #4,%sp
	move.l 20(%fp),-(%sp)
	jsr _ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE
	addq.l #4,%sp
	move.l %a0,%d3
	move.l 16(%fp),-(%sp)
	jsr _ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
	addq.l #4,%sp
	move.l %a0,%d2
	move.l 8(%fp),-(%sp)
	jsr _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv
	addq.l #4,%sp
	move.l %a0,%d0
	move.l %d3,-(%sp)
	move.l %d2,-(%sp)
	move.l %d0,-(%sp)
	move.l 12(%fp),-(%sp)
.LEHB24:
	.cfi_escape 0x2e,0x10
	jsr _ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3dEEEvRS1_PT_DpOT0_
.LEHE24:
	lea (16,%sp),%sp
	jra .L221
.L220:
	move.l %d0,%d2
	moveq #12,%d0
	add.l 8(%fp),%d0
	move.l %d0,-(%sp)
	jsr _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD1Ev
	addq.l #4,%sp
	move.l 8(%fp),%d0
	move.l %d0,-(%sp)
	jsr _ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev
	addq.l #4,%sp
	move.l %d2,%d0
	move.l %d0,-(%sp)
.LEHB25:
	jsr _Unwind_Resume
.LEHE25:
.L221:
	move.l -12(%fp),%d2
	move.l -8(%fp),%d3
	unlk %fp
	rts
	.cfi_endproc
.LFE4687:
	.section	.gcc_except_table
.LLSDA4687:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4687-.LLSDACSB4687
.LLSDACSB4687:
	.uleb128 .LEHB24-.LFB4687
	.uleb128 .LEHE24-.LEHB24
	.uleb128 .L220-.LFB4687
	.uleb128 0
	.uleb128 .LEHB25-.LFB4687
	.uleb128 .LEHE25-.LEHB25
	.uleb128 0
	.uleb128 0
.LLSDACSE4687:
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3dEEES1_DpOT_,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC5IJ4vec3dEEES1_DpOT_,comdat
	.size	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3dEEES1_DpOT_, .-_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3dEEES1_DpOT_
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC1IJ4vec3dEEES1_DpOT_
	.set	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC1IJ4vec3dEEES1_DpOT_,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3dEEES1_DpOT_
	.section	.text._ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEaSEDn,"axG",@progbits,_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEaSEDn,comdat
	.align	2
	.weak	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEaSEDn
	.type	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEaSEDn, @function
_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEaSEDn:
.LFB4689:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l 8(%fp),%a0
	clr.l 4(%a0)
	move.l 8(%fp),%d0
	move.l %d0,%d1
	move.l %d1,%a0
	unlk %fp
	rts
	.cfi_endproc
.LFE4689:
	.size	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEaSEDn, .-_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEaSEDn
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv,comdat
	.align	2
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv
	.type	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv, @function
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv:
.LFB4690:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	moveq #12,%d0
	add.l 8(%fp),%d0
	move.l %d0,-(%sp)
	jsr _ZN9__gnu_cxx16__aligned_bufferI6sphereE6_M_ptrEv
	addq.l #4,%sp
	move.l %a0,%d0
	move.l %d0,%d1
	move.l %d1,%a0
	unlk %fp
	rts
	.cfi_endproc
.LFE4690:
	.size	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv, .-_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3iEEES1_DpOT_,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC5IJ4vec3iEEES1_DpOT_,comdat
	.align	2
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3iEEES1_DpOT_
	.type	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3iEEES1_DpOT_, @function
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3iEEES1_DpOT_:
.LFB4692:
	.cfi_startproc
	.cfi_personality 0,__gxx_personality_v0
	.cfi_lsda 0,.LLSDA4692
	link.w %fp,#-4
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l %d3,-(%sp)
	move.l %d2,-(%sp)
	.cfi_offset 3, -16
	.cfi_offset 2, -20
	move.l 8(%fp),%d0
	move.l %d0,-(%sp)
	jsr _ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC2Ev
	addq.l #4,%sp
	move.l #_ZTVSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE+8,%d0
	move.l 8(%fp),%a0
	move.l %d0,(%a0)
	moveq #12,%d2
	add.l 8(%fp),%d2
	move.l 12(%fp),-(%sp)
	move.l %fp,%d0
	subq.l #1,%d0
	move.l %d0,-(%sp)
	jsr _ZNSaI6sphereEC1ERKS0_
	addq.l #8,%sp
	move.l %fp,%d0
	subq.l #1,%d0
	move.l %d0,-(%sp)
	move.l %d2,-(%sp)
	jsr _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC1ES1_
	addq.l #8,%sp
	move.l %fp,%d0
	subq.l #1,%d0
	move.l %d0,-(%sp)
	jsr _ZNSaI6sphereED1Ev
	addq.l #4,%sp
	move.l 20(%fp),-(%sp)
	jsr _ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE
	addq.l #4,%sp
	move.l %a0,%d3
	move.l 16(%fp),-(%sp)
	jsr _ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
	addq.l #4,%sp
	move.l %a0,%d2
	move.l 8(%fp),-(%sp)
	jsr _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv
	addq.l #4,%sp
	move.l %a0,%d0
	move.l %d3,-(%sp)
	move.l %d2,-(%sp)
	move.l %d0,-(%sp)
	move.l 12(%fp),-(%sp)
.LEHB26:
	.cfi_escape 0x2e,0x10
	jsr _ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3iEEEvRS1_PT_DpOT0_
.LEHE26:
	lea (16,%sp),%sp
	jra .L229
.L228:
	move.l %d0,%d2
	moveq #12,%d0
	add.l 8(%fp),%d0
	move.l %d0,-(%sp)
	jsr _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD1Ev
	addq.l #4,%sp
	move.l 8(%fp),%d0
	move.l %d0,-(%sp)
	jsr _ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev
	addq.l #4,%sp
	move.l %d2,%d0
	move.l %d0,-(%sp)
.LEHB27:
	jsr _Unwind_Resume
.LEHE27:
.L229:
	move.l -12(%fp),%d2
	move.l -8(%fp),%d3
	unlk %fp
	rts
	.cfi_endproc
.LFE4692:
	.section	.gcc_except_table
.LLSDA4692:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4692-.LLSDACSB4692
.LLSDACSB4692:
	.uleb128 .LEHB26-.LFB4692
	.uleb128 .LEHE26-.LEHB26
	.uleb128 .L228-.LFB4692
	.uleb128 0
	.uleb128 .LEHB27-.LFB4692
	.uleb128 .LEHE27-.LEHB27
	.uleb128 0
	.uleb128 0
.LLSDACSE4692:
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3iEEES1_DpOT_,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC5IJ4vec3iEEES1_DpOT_,comdat
	.size	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3iEEES1_DpOT_, .-_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3iEEES1_DpOT_
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC1IJ4vec3iEEES1_DpOT_
	.set	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC1IJ4vec3iEEES1_DpOT_,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3iEEES1_DpOT_
	.section	.text._ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC2Ev,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC5Ev,comdat
	.align	2
	.weak	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC2Ev
	.type	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC2Ev, @function
_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC2Ev:
.LFB4705:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	nop
	unlk %fp
	rts
	.cfi_endproc
.LFE4705:
	.size	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC2Ev, .-_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC2Ev
	.weak	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC1Ev
	.set	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC1Ev,_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC2Ev
	.section	.text._ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED2Ev,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED5Ev,comdat
	.align	2
	.weak	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED2Ev
	.type	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED2Ev, @function
_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED2Ev:
.LFB4708:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	nop
	unlk %fp
	rts
	.cfi_endproc
.LFE4708:
	.size	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED2Ev, .-_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED2Ev
	.weak	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED1Ev
	.set	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED1Ev,_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED2Ev
	.section	.text._ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE8allocateERS6_j,"axG",@progbits,_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE8allocateERS6_j,comdat
	.align	2
	.weak	_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE8allocateERS6_j
	.type	_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE8allocateERS6_j, @function
_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE8allocateERS6_j:
.LFB4710:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	clr.l -(%sp)
	move.l 12(%fp),-(%sp)
	move.l 8(%fp),-(%sp)
	jsr _ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8allocateEjPKv
	lea (12,%sp),%sp
	move.l %a0,%d0
	move.l %d0,%d1
	move.l %d1,%a0
	unlk %fp
	rts
	.cfi_endproc
.LFE4710:
	.size	_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE8allocateERS6_j, .-_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE8allocateERS6_j
	.section	.text._ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC2ERS6_PS5_,"axG",@progbits,_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC5ERS6_PS5_,comdat
	.align	2
	.weak	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC2ERS6_PS5_
	.type	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC2ERS6_PS5_, @function
_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC2ERS6_PS5_:
.LFB4712:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l 12(%fp),-(%sp)
	jsr _ZSt11__addressofISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEPT_RS7_
	addq.l #4,%sp
	move.l %a0,%d0
	move.l 8(%fp),%a0
	move.l %d0,(%a0)
	move.l 8(%fp),%a0
	move.l 16(%fp),4(%a0)
	nop
	unlk %fp
	rts
	.cfi_endproc
.LFE4712:
	.size	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC2ERS6_PS5_, .-_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC2ERS6_PS5_
	.weak	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC1ERS6_PS5_
	.set	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC1ERS6_PS5_,_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC2ERS6_PS5_
	.section	.text._ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE10deallocateERS6_PS5_j,"axG",@progbits,_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE10deallocateERS6_PS5_j,comdat
	.align	2
	.weak	_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE10deallocateERS6_PS5_j
	.type	_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE10deallocateERS6_PS5_j, @function
_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE10deallocateERS6_PS5_j:
.LFB4714:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l 16(%fp),-(%sp)
	move.l 12(%fp),-(%sp)
	move.l 8(%fp),-(%sp)
	jsr _ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE10deallocateEPS5_j
	lea (12,%sp),%sp
	nop
	unlk %fp
	rts
	.cfi_endproc
.LFE4714:
	.size	_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE10deallocateERS6_PS5_j, .-_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE10deallocateERS6_PS5_j
	.section	.text._ZSt12__to_addressISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEPT_S7_,"axG",@progbits,_ZSt12__to_addressISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEPT_S7_,comdat
	.align	2
	.weak	_ZSt12__to_addressISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEPT_S7_
	.type	_ZSt12__to_addressISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEPT_S7_, @function
_ZSt12__to_addressISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEPT_S7_:
.LFB4715:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l 8(%fp),%d0
	move.l %d0,%d1
	move.l %d1,%a0
	unlk %fp
	rts
	.cfi_endproc
.LFE4715:
	.size	_ZSt12__to_addressISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEPT_S7_, .-_ZSt12__to_addressISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEPT_S7_
	.section	.text._ZN9__gnu_cxx13new_allocatorI6sphereEC2ERKS2_,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorI6sphereEC5ERKS2_,comdat
	.align	2
	.weak	_ZN9__gnu_cxx13new_allocatorI6sphereEC2ERKS2_
	.type	_ZN9__gnu_cxx13new_allocatorI6sphereEC2ERKS2_, @function
_ZN9__gnu_cxx13new_allocatorI6sphereEC2ERKS2_:
.LFB4717:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	nop
	unlk %fp
	rts
	.cfi_endproc
.LFE4717:
	.size	_ZN9__gnu_cxx13new_allocatorI6sphereEC2ERKS2_, .-_ZN9__gnu_cxx13new_allocatorI6sphereEC2ERKS2_
	.weak	_ZN9__gnu_cxx13new_allocatorI6sphereEC1ERKS2_
	.set	_ZN9__gnu_cxx13new_allocatorI6sphereEC1ERKS2_,_ZN9__gnu_cxx13new_allocatorI6sphereEC2ERKS2_
	.section	.text._ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC2Ev,"axG",@progbits,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC5Ev,comdat
	.align	2
	.weak	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC2Ev
	.type	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC2Ev, @function
_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC2Ev:
.LFB4720:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l #_ZTVSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE+8,%d0
	move.l 8(%fp),%a0
	move.l %d0,(%a0)
	move.l 8(%fp),%a0
	moveq #1,%d0
	move.l %d0,4(%a0)
	move.l 8(%fp),%a0
	moveq #1,%d0
	move.l %d0,8(%a0)
	nop
	unlk %fp
	rts
	.cfi_endproc
.LFE4720:
	.size	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC2Ev, .-_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC2Ev
	.weak	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC1Ev
	.set	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC1Ev,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC2Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC2ES1_,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC5ES1_,comdat
	.align	2
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC2ES1_
	.type	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC2ES1_, @function
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC2ES1_:
.LFB4723:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l 12(%fp),-(%sp)
	move.l 8(%fp),-(%sp)
	jsr _ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC2ERKS1_
	addq.l #8,%sp
	nop
	unlk %fp
	rts
	.cfi_endproc
.LFE4723:
	.size	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC2ES1_, .-_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC2ES1_
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC1ES1_
	.set	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC1ES1_,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC2ES1_
	.section	.text._ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3dEEEvRS1_PT_DpOT0_,"axG",@progbits,_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3dEEEvRS1_PT_DpOT0_,comdat
	.align	2
	.weak	_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3dEEEvRS1_PT_DpOT0_
	.type	_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3dEEEvRS1_PT_DpOT0_, @function
_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3dEEEvRS1_PT_DpOT0_:
.LFB4725:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l %d2,-(%sp)
	.cfi_offset 2, -12
	move.l 20(%fp),-(%sp)
	jsr _ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE
	addq.l #4,%sp
	move.l %a0,%d2
	move.l 16(%fp),-(%sp)
	jsr _ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
	addq.l #4,%sp
	move.l %a0,%d0
	move.l %d2,-(%sp)
	move.l %d0,-(%sp)
	move.l 12(%fp),-(%sp)
	move.l 8(%fp),-(%sp)
	jsr _ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_
	lea (16,%sp),%sp
	nop
	move.l -4(%fp),%d2
	unlk %fp
	rts
	.cfi_endproc
.LFE4725:
	.size	_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3dEEEvRS1_PT_DpOT0_, .-_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3dEEEvRS1_PT_DpOT0_
	.section	.text._ZN9__gnu_cxx16__aligned_bufferI6sphereE6_M_ptrEv,"axG",@progbits,_ZN9__gnu_cxx16__aligned_bufferI6sphereE6_M_ptrEv,comdat
	.align	2
	.weak	_ZN9__gnu_cxx16__aligned_bufferI6sphereE6_M_ptrEv
	.type	_ZN9__gnu_cxx16__aligned_bufferI6sphereE6_M_ptrEv, @function
_ZN9__gnu_cxx16__aligned_bufferI6sphereE6_M_ptrEv:
.LFB4726:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l 8(%fp),-(%sp)
	jsr _ZN9__gnu_cxx16__aligned_bufferI6sphereE7_M_addrEv
	addq.l #4,%sp
	move.l %a0,%d0
	move.l %d0,%d1
	move.l %d1,%a0
	unlk %fp
	rts
	.cfi_endproc
.LFE4726:
	.size	_ZN9__gnu_cxx16__aligned_bufferI6sphereE6_M_ptrEv, .-_ZN9__gnu_cxx16__aligned_bufferI6sphereE6_M_ptrEv
	.section	.text._ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3iEEEvRS1_PT_DpOT0_,"axG",@progbits,_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3iEEEvRS1_PT_DpOT0_,comdat
	.align	2
	.weak	_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3iEEEvRS1_PT_DpOT0_
	.type	_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3iEEEvRS1_PT_DpOT0_, @function
_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3iEEEvRS1_PT_DpOT0_:
.LFB4727:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l %d2,-(%sp)
	.cfi_offset 2, -12
	move.l 20(%fp),-(%sp)
	jsr _ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE
	addq.l #4,%sp
	move.l %a0,%d2
	move.l 16(%fp),-(%sp)
	jsr _ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
	addq.l #4,%sp
	move.l %a0,%d0
	move.l %d2,-(%sp)
	move.l %d0,-(%sp)
	move.l 12(%fp),-(%sp)
	move.l 8(%fp),-(%sp)
	jsr _ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_
	lea (16,%sp),%sp
	nop
	move.l -4(%fp),%d2
	unlk %fp
	rts
	.cfi_endproc
.LFE4727:
	.size	_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3iEEEvRS1_PT_DpOT0_, .-_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3iEEEvRS1_PT_DpOT0_
	.section	.text._ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8allocateEjPKv,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8allocateEjPKv,comdat
	.align	2
	.weak	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8allocateEjPKv
	.type	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8allocateEjPKv, @function
_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8allocateEjPKv:
.LFB4730:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l 8(%fp),-(%sp)
	jsr _ZNK9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8max_sizeEv
	addq.l #4,%sp
	cmp.l 12(%fp),%d0
	scs %d0
	neg.b %d0
	tst.b %d0
	jeq .L246
	jsr _ZSt17__throw_bad_allocv
.L246:
	move.l 12(%fp),%d1
	move.l %d1,%d0
	lsl.l #3,%d0
	sub.l %d1,%d0
	lsl.l #3,%d0
	move.l %d0,-(%sp)
	jsr _Znwj
	addq.l #4,%sp
	move.l %a0,%d0
	move.l %d0,%d1
	move.l %d1,%a0
	unlk %fp
	rts
	.cfi_endproc
.LFE4730:
	.size	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8allocateEjPKv, .-_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8allocateEjPKv
	.section	.text._ZSt11__addressofISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEPT_RS7_,"axG",@progbits,_ZSt11__addressofISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEPT_RS7_,comdat
	.align	2
	.weak	_ZSt11__addressofISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEPT_RS7_
	.type	_ZSt11__addressofISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEPT_RS7_, @function
_ZSt11__addressofISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEPT_RS7_:
.LFB4731:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l 8(%fp),%d0
	move.l %d0,%d1
	move.l %d1,%a0
	unlk %fp
	rts
	.cfi_endproc
.LFE4731:
	.size	_ZSt11__addressofISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEPT_RS7_, .-_ZSt11__addressofISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEPT_RS7_
	.section	.text._ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE10deallocateEPS5_j,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE10deallocateEPS5_j,comdat
	.align	2
	.weak	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE10deallocateEPS5_j
	.type	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE10deallocateEPS5_j, @function
_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE10deallocateEPS5_j:
.LFB4732:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l 12(%fp),-(%sp)
	jsr _ZdlPv
	addq.l #4,%sp
	nop
	unlk %fp
	rts
	.cfi_endproc
.LFE4732:
	.size	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE10deallocateEPS5_j, .-_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE10deallocateEPS5_j
	.section	.text._ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC2ERKS1_,"axG",@progbits,_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC5ERKS1_,comdat
	.align	2
	.weak	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC2ERKS1_
	.type	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC2ERKS1_, @function
_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC2ERKS1_:
.LFB4734:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l 12(%fp),-(%sp)
	move.l 8(%fp),-(%sp)
	jsr _ZNSaI6sphereEC2ERKS0_
	addq.l #8,%sp
	nop
	unlk %fp
	rts
	.cfi_endproc
.LFE4734:
	.size	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC2ERKS1_, .-_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC2ERKS1_
	.weak	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC1ERKS1_
	.set	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC1ERKS1_,_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC2ERKS1_
	.section	.text._ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_,comdat
	.align	2
	.weak	_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_
	.type	_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_, @function
_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_:
.LFB4736:
	.cfi_startproc
	.cfi_personality 0,__gxx_personality_v0
	.cfi_lsda 0,.LLSDA4736
	link.w %fp,#-24
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	movem.l #15360,-(%sp)
	.cfi_offset 2, -48
	.cfi_offset 3, -44
	.cfi_offset 4, -40
	.cfi_offset 5, -36
	move.l 16(%fp),-(%sp)
	jsr _ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
	addq.l #4,%sp
	move.l %a0,%d0
	move.l %d0,%a1
	lea (-24,%fp),%a0
	move.l (%a1),(%a0)
	addq.l #4,%a0
	addq.l #4,%a1
	move.l (%a1),(%a0)
	addq.l #4,%a0
	addq.l #4,%a1
	move.l (%a1),(%a0)
	addq.l #4,%a0
	addq.l #4,%a1
	move.l (%a1),(%a0)
	addq.l #4,%a0
	addq.l #4,%a1
	move.l (%a1),(%a0)
	addq.l #4,%a0
	addq.l #4,%a1
	move.l (%a1),(%a0)
	addq.l #4,%a0
	addq.l #4,%a1
	move.l 20(%fp),-(%sp)
	jsr _ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE
	addq.l #4,%sp
	move.l (%a0),%d2
	move.l 4(%a0),%d3
	move.l 12(%fp),%d4
	move.l %d4,-(%sp)
	pea 44.w
	jsr _ZnwjPv
	addq.l #8,%sp
	move.l %a0,%d5
	move.l %d3,-(%sp)
	move.l %d2,-(%sp)
	lea (-24,%sp),%sp
	move.l %sp,%d0
	lea (-24,%fp),%a0
	move.l %d0,%a1
	move.l (%a0),(%a1)
	addq.l #4,%a1
	addq.l #4,%a0
	move.l (%a0),(%a1)
	addq.l #4,%a1
	addq.l #4,%a0
	move.l (%a0),(%a1)
	addq.l #4,%a1
	addq.l #4,%a0
	move.l (%a0),(%a1)
	addq.l #4,%a1
	addq.l #4,%a0
	move.l (%a0),(%a1)
	addq.l #4,%a1
	addq.l #4,%a0
	move.l (%a0),(%a1)
	addq.l #4,%a1
	addq.l #4,%a0
	move.l %d5,-(%sp)
.LEHB28:
	.cfi_escape 0x2e,0x24
	jsr _ZN6sphereC1E4vec3d
.LEHE28:
	lea (36,%sp),%sp
	jra .L255
.L254:
	move.l %d0,%d2
	move.l %d4,-(%sp)
	move.l %d5,-(%sp)
	jsr _ZdlPvS_
	addq.l #8,%sp
	move.l %d2,%d0
	move.l %d0,-(%sp)
.LEHB29:
	jsr _Unwind_Resume
.LEHE29:
.L255:
	movem.l -40(%fp),#60
	unlk %fp
	rts
	.cfi_endproc
.LFE4736:
	.section	.gcc_except_table
.LLSDA4736:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4736-.LLSDACSB4736
.LLSDACSB4736:
	.uleb128 .LEHB28-.LFB4736
	.uleb128 .LEHE28-.LEHB28
	.uleb128 .L254-.LFB4736
	.uleb128 0
	.uleb128 .LEHB29-.LFB4736
	.uleb128 .LEHE29-.LEHB29
	.uleb128 0
	.uleb128 0
.LLSDACSE4736:
	.section	.text._ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_,comdat
	.size	_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_, .-_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_
	.section	.text._ZN9__gnu_cxx16__aligned_bufferI6sphereE7_M_addrEv,"axG",@progbits,_ZN9__gnu_cxx16__aligned_bufferI6sphereE7_M_addrEv,comdat
	.align	2
	.weak	_ZN9__gnu_cxx16__aligned_bufferI6sphereE7_M_addrEv
	.type	_ZN9__gnu_cxx16__aligned_bufferI6sphereE7_M_addrEv, @function
_ZN9__gnu_cxx16__aligned_bufferI6sphereE7_M_addrEv:
.LFB4737:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l 8(%fp),%d0
	move.l %d0,%d1
	move.l %d1,%a0
	unlk %fp
	rts
	.cfi_endproc
.LFE4737:
	.size	_ZN9__gnu_cxx16__aligned_bufferI6sphereE7_M_addrEv, .-_ZN9__gnu_cxx16__aligned_bufferI6sphereE7_M_addrEv
	.section	.text._ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_,comdat
	.align	2
	.weak	_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_
	.type	_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_, @function
_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_:
.LFB4738:
	.cfi_startproc
	.cfi_personality 0,__gxx_personality_v0
	.cfi_lsda 0,.LLSDA4738
	link.w %fp,#-24
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	fmovem #4,-(%sp)
	movem.l #14336,-(%sp)
	.cfi_offset 18, -44
	.cfi_offset 2, -56
	.cfi_offset 3, -52
	.cfi_offset 4, -48
	move.l 16(%fp),-(%sp)
	jsr _ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
	addq.l #4,%sp
	move.l %a0,%d0
	move.l %d0,%a1
	lea (-24,%fp),%a0
	move.l (%a1),(%a0)
	addq.l #4,%a0
	addq.l #4,%a1
	move.l (%a1),(%a0)
	addq.l #4,%a0
	addq.l #4,%a1
	move.l (%a1),(%a0)
	addq.l #4,%a0
	addq.l #4,%a1
	move.l (%a1),(%a0)
	addq.l #4,%a0
	addq.l #4,%a1
	move.l (%a1),(%a0)
	addq.l #4,%a0
	addq.l #4,%a1
	move.l (%a1),(%a0)
	addq.l #4,%a0
	addq.l #4,%a1
	move.l 20(%fp),-(%sp)
	jsr _ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE
	addq.l #4,%sp
	move.l (%a0),%d0
	fmove.l %d0,%fp2
	move.l 12(%fp),%d2
	move.l %d2,-(%sp)
	pea 44.w
	jsr _ZnwjPv
	addq.l #8,%sp
	move.l %a0,%d3
	fmove.d %fp2,-(%sp)
	lea (-24,%sp),%sp
	move.l %sp,%d0
	lea (-24,%fp),%a0
	move.l %d0,%a1
	move.l (%a0),(%a1)
	addq.l #4,%a1
	addq.l #4,%a0
	move.l (%a0),(%a1)
	addq.l #4,%a1
	addq.l #4,%a0
	move.l (%a0),(%a1)
	addq.l #4,%a1
	addq.l #4,%a0
	move.l (%a0),(%a1)
	addq.l #4,%a1
	addq.l #4,%a0
	move.l (%a0),(%a1)
	addq.l #4,%a1
	addq.l #4,%a0
	move.l (%a0),(%a1)
	addq.l #4,%a1
	addq.l #4,%a0
	move.l %d3,-(%sp)
.LEHB30:
	.cfi_escape 0x2e,0x24
	jsr _ZN6sphereC1E4vec3d
.LEHE30:
	lea (36,%sp),%sp
	jra .L261
.L260:
	move.l %d0,%d4
	move.l %d2,-(%sp)
	move.l %d3,-(%sp)
	jsr _ZdlPvS_
	addq.l #8,%sp
	move.l %d4,%d0
	move.l %d0,-(%sp)
.LEHB31:
	jsr _Unwind_Resume
.LEHE31:
.L261:
	movem.l -48(%fp),#28
	fmovem -36(%fp),#32
	unlk %fp
	rts
	.cfi_endproc
.LFE4738:
	.section	.gcc_except_table
.LLSDA4738:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4738-.LLSDACSB4738
.LLSDACSB4738:
	.uleb128 .LEHB30-.LFB4738
	.uleb128 .LEHE30-.LEHB30
	.uleb128 .L260-.LFB4738
	.uleb128 0
	.uleb128 .LEHB31-.LFB4738
	.uleb128 .LEHE31-.LEHB31
	.uleb128 0
	.uleb128 0
.LLSDACSE4738:
	.section	.text._ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_,comdat
	.size	_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_, .-_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_
	.section	.text._ZNK9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8max_sizeEv,"axG",@progbits,_ZNK9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8max_sizeEv,comdat
	.align	2
	.weak	_ZNK9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8max_sizeEv
	.type	_ZNK9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8max_sizeEv, @function
_ZNK9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8max_sizeEv:
.LFB4739:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l #38347922,%d0
	unlk %fp
	rts
	.cfi_endproc
.LFE4739:
	.size	_ZNK9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8max_sizeEv, .-_ZNK9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8max_sizeEv
	.weak	_ZTVSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE
	.section	.rodata._ZTVSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE,"aG",@progbits,_ZTVSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align	4
	.type	_ZTVSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTVSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE, 28
_ZTVSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE:
	.long	0
	.long	_ZTISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE
	.long	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.long	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.long	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.long	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.long	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.weak	_ZTVSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE
	.section	.rodata._ZTVSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE,"aG",@progbits,_ZTVSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align	4
	.type	_ZTVSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTVSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE, 28
_ZTVSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE:
	.long	0
	.long	_ZTISt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE
	.long	0
	.long	0
	.long	__cxa_pure_virtual
	.long	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.long	__cxa_pure_virtual
	.weak	_ZTISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE
	.section	.rodata._ZTISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE,"aG",@progbits,_ZTISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align	2
	.type	_ZTISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE, 12
_ZTISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE:
	.long	_ZTVN10__cxxabiv120__si_class_type_infoE+8
	.long	_ZTSSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE
	.long	_ZTISt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE
	.weak	_ZTSSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE
	.section	.rodata._ZTSSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE,"aG",@progbits,_ZTSSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE,comdat
	.type	_ZTSSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTSSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE, 73
_ZTSSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE:
	.string	"St23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE"
	.weak	_ZTISt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE
	.section	.rodata._ZTISt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE,"aG",@progbits,_ZTISt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align	2
	.type	_ZTISt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTISt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE, 12
_ZTISt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE:
	.long	_ZTVN10__cxxabiv120__si_class_type_infoE+8
	.long	_ZTSSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE
	.long	_ZTISt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE
	.weak	_ZTSSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE
	.section	.rodata._ZTSSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE,"aG",@progbits,_ZTSSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE,comdat
	.type	_ZTSSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTSSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE, 52
_ZTSSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE:
	.string	"St16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE"
	.text
	.align	2
	.type	_Z41__static_initialization_and_destruction_0ii, @function
_Z41__static_initialization_and_destruction_0ii:
.LFB4756:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	moveq #1,%d0
	cmp.l 8(%fp),%d0
	jne .L266
	cmp.l #65535,12(%fp)
	jne .L266
	pea _ZStL8__ioinit
	jsr _ZNSt8ios_base4InitC1Ev
	addq.l #4,%sp
	pea __dso_handle
	pea _ZStL8__ioinit
	pea _ZNSt8ios_base4InitD1Ev
	jsr __cxa_atexit
	lea (12,%sp),%sp
.L266:
	nop
	unlk %fp
	rts
	.cfi_endproc
.LFE4756:
	.size	_Z41__static_initialization_and_destruction_0ii, .-_Z41__static_initialization_and_destruction_0ii
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align	2
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.type	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED2Ev:
.LFB4758:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l #_ZTVSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE+8,%d0
	move.l 8(%fp),%a0
	move.l %d0,(%a0)
	moveq #12,%d0
	add.l 8(%fp),%d0
	move.l %d0,-(%sp)
	jsr _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD1Ev
	addq.l #4,%sp
	move.l 8(%fp),%d0
	move.l %d0,-(%sp)
	jsr _ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev
	addq.l #4,%sp
	nop
	unlk %fp
	rts
	.cfi_endproc
.LFE4758:
	.size	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.set	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED1Ev,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED0Ev,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align	2
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.type	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED0Ev, @function
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED0Ev:
.LFB4760:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l 8(%fp),-(%sp)
	jsr _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED1Ev
	addq.l #4,%sp
	pea 56.w
	move.l 8(%fp),-(%sp)
	jsr _ZdlPvj
	addq.l #8,%sp
	unlk %fp
	rts
	.cfi_endproc
.LFE4760:
	.size	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED0Ev, .-_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,comdat
	.align	2
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.type	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, @function
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv:
.LFB4761:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l %d2,-(%sp)
	.cfi_offset 2, -12
	move.l 8(%fp),-(%sp)
	jsr _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv
	addq.l #4,%sp
	move.l %a0,%d2
	moveq #12,%d0
	add.l 8(%fp),%d0
	move.l %d0,-(%sp)
	jsr _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_Impl8_M_allocEv
	addq.l #4,%sp
	move.l %a0,%d0
	move.l %d2,-(%sp)
	move.l %d0,-(%sp)
	jsr _ZNSt16allocator_traitsISaI6sphereEE7destroyIS0_EEvRS1_PT_
	addq.l #8,%sp
	nop
	move.l -4(%fp),%d2
	unlk %fp
	rts
	.cfi_endproc
.LFE4761:
	.size	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, .-_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,comdat
	.align	2
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.type	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, @function
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv:
.LFB4762:
	.cfi_startproc
	link.w %fp,#-12
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	moveq #12,%d0
	add.l 8(%fp),%d0
	move.l %d0,-(%sp)
	jsr _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_Impl8_M_allocEv
	addq.l #4,%sp
	move.l %a0,%d0
	move.l %d0,-(%sp)
	move.l %fp,%d0
	subq.l #1,%d0
	move.l %d0,-(%sp)
	jsr _ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC1IS0_EERKSaIT_E
	addq.l #8,%sp
	move.l 8(%fp),-(%sp)
	move.l %fp,%d0
	subq.l #1,%d0
	move.l %d0,-(%sp)
	lea (-10,%fp),%a0
	move.l %a0,-(%sp)
	jsr _ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC1ERS6_PS5_
	lea (12,%sp),%sp
	move.l 8(%fp),-(%sp)
	jsr _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED1Ev
	addq.l #4,%sp
	lea (-10,%fp),%a0
	move.l %a0,-(%sp)
	jsr _ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED1Ev
	addq.l #4,%sp
	move.l %fp,%d0
	subq.l #1,%d0
	move.l %d0,-(%sp)
	jsr _ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED1Ev
	addq.l #4,%sp
	nop
	unlk %fp
	rts
	.cfi_endproc
.LFE4762:
	.size	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, .-_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,comdat
	.align	2
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.type	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, @function
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info:
.LFB4763:
	.cfi_startproc
	link.w %fp,#-4
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l 8(%fp),-(%sp)
	jsr _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv
	addq.l #4,%sp
	move.l %a0,-4(%fp)
	jsr _ZNSt19_Sp_make_shared_tag5_S_tiEv
	move.l %a0,%d0
	cmp.l 12(%fp),%d0
	jeq .L272
	pea _ZTISt19_Sp_make_shared_tag
	move.l 12(%fp),-(%sp)
	jsr _ZNKSt9type_infoeqERKS_
	addq.l #8,%sp
	tst.b %d0
	jeq .L273
.L272:
	moveq #1,%d0
	jra .L274
.L273:
	clr.b %d0
.L274:
	tst.b %d0
	jeq .L275
	move.l -4(%fp),%d0
	jra .L276
.L275:
	clr.l %d0
.L276:
	move.l %d0,%d1
	move.l %d1,%a0
	unlk %fp
	rts
	.cfi_endproc
.LFE4763:
	.size	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, .-_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_Impl8_M_allocEv,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_Impl8_M_allocEv,comdat
	.align	2
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_Impl8_M_allocEv
	.type	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_Impl8_M_allocEv, @function
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_Impl8_M_allocEv:
.LFB4764:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l 8(%fp),-(%sp)
	jsr _ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EE6_S_getERS2_
	addq.l #4,%sp
	move.l %a0,%d0
	move.l %d0,%d1
	move.l %d1,%a0
	unlk %fp
	rts
	.cfi_endproc
.LFE4764:
	.size	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_Impl8_M_allocEv, .-_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_Impl8_M_allocEv
	.section	.text._ZNSt16allocator_traitsISaI6sphereEE7destroyIS0_EEvRS1_PT_,"axG",@progbits,_ZNSt16allocator_traitsISaI6sphereEE7destroyIS0_EEvRS1_PT_,comdat
	.align	2
	.weak	_ZNSt16allocator_traitsISaI6sphereEE7destroyIS0_EEvRS1_PT_
	.type	_ZNSt16allocator_traitsISaI6sphereEE7destroyIS0_EEvRS1_PT_, @function
_ZNSt16allocator_traitsISaI6sphereEE7destroyIS0_EEvRS1_PT_:
.LFB4765:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l 12(%fp),-(%sp)
	move.l 8(%fp),-(%sp)
	jsr _ZN9__gnu_cxx13new_allocatorI6sphereE7destroyIS1_EEvPT_
	addq.l #8,%sp
	nop
	unlk %fp
	rts
	.cfi_endproc
.LFE4765:
	.size	_ZNSt16allocator_traitsISaI6sphereEE7destroyIS0_EEvRS1_PT_, .-_ZNSt16allocator_traitsISaI6sphereEE7destroyIS0_EEvRS1_PT_
	.section	.text._ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EE6_S_getERS2_,"axG",@progbits,_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EE6_S_getERS2_,comdat
	.align	2
	.weak	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EE6_S_getERS2_
	.type	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EE6_S_getERS2_, @function
_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EE6_S_getERS2_:
.LFB4766:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l 8(%fp),%d0
	move.l %d0,%d1
	move.l %d1,%a0
	unlk %fp
	rts
	.cfi_endproc
.LFE4766:
	.size	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EE6_S_getERS2_, .-_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EE6_S_getERS2_
	.section	.text._ZN6sphereD2Ev,"axG",@progbits,_ZN6sphereD5Ev,comdat
	.align	2
	.weak	_ZN6sphereD2Ev
	.type	_ZN6sphereD2Ev, @function
_ZN6sphereD2Ev:
.LFB4769:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l #_ZTV6sphere+8,%d0
	move.l 8(%fp),%a0
	move.l %d0,(%a0)
	moveq #36,%d0
	add.l 8(%fp),%d0
	move.l %d0,-(%sp)
	jsr _ZNSt10shared_ptrI8materialED1Ev
	addq.l #4,%sp
	nop
	unlk %fp
	rts
	.cfi_endproc
.LFE4769:
	.size	_ZN6sphereD2Ev, .-_ZN6sphereD2Ev
	.weak	_ZN6sphereD1Ev
	.set	_ZN6sphereD1Ev,_ZN6sphereD2Ev
	.section	.text._ZN9__gnu_cxx13new_allocatorI6sphereE7destroyIS1_EEvPT_,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorI6sphereE7destroyIS1_EEvPT_,comdat
	.align	2
	.weak	_ZN9__gnu_cxx13new_allocatorI6sphereE7destroyIS1_EEvPT_
	.type	_ZN9__gnu_cxx13new_allocatorI6sphereE7destroyIS1_EEvPT_, @function
_ZN9__gnu_cxx13new_allocatorI6sphereE7destroyIS1_EEvPT_:
.LFB4767:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l 12(%fp),-(%sp)
	jsr _ZN6sphereD1Ev
	addq.l #4,%sp
	nop
	unlk %fp
	rts
	.cfi_endproc
.LFE4767:
	.size	_ZN9__gnu_cxx13new_allocatorI6sphereE7destroyIS1_EEvPT_, .-_ZN9__gnu_cxx13new_allocatorI6sphereE7destroyIS1_EEvPT_
	.weak	_ZTISt19_Sp_make_shared_tag
	.section	.rodata._ZTISt19_Sp_make_shared_tag,"aG",@progbits,_ZTISt19_Sp_make_shared_tag,comdat
	.align	2
	.type	_ZTISt19_Sp_make_shared_tag, @object
	.size	_ZTISt19_Sp_make_shared_tag, 8
_ZTISt19_Sp_make_shared_tag:
	.long	_ZTVN10__cxxabiv117__class_type_infoE+8
	.long	_ZTSSt19_Sp_make_shared_tag
	.weak	_ZTSSt19_Sp_make_shared_tag
	.section	.rodata._ZTSSt19_Sp_make_shared_tag,"aG",@progbits,_ZTSSt19_Sp_make_shared_tag,comdat
	.type	_ZTSSt19_Sp_make_shared_tag, @object
	.size	_ZTSSt19_Sp_make_shared_tag, 24
_ZTSSt19_Sp_make_shared_tag:
	.string	"St19_Sp_make_shared_tag"
	.weak	_ZTISt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE
	.section	.rodata._ZTISt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE,"aG",@progbits,_ZTISt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align	2
	.type	_ZTISt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTISt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE, 8
_ZTISt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE:
	.long	_ZTVN10__cxxabiv117__class_type_infoE+8
	.long	_ZTSSt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE
	.weak	_ZTSSt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE
	.section	.rodata._ZTSSt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE,"aG",@progbits,_ZTSSt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE,comdat
	.type	_ZTSSt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTSSt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE, 47
_ZTSSt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE:
	.string	"St11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE"
	.text
	.align	2
	.type	_GLOBAL__sub_I_main, @function
_GLOBAL__sub_I_main:
.LFB4771:
	.cfi_startproc
	link.w %fp,#0
	.cfi_offset 14, -8
	.cfi_def_cfa 14, 8
	move.l #65535,-(%sp)
	pea 1.w
	jsr _Z41__static_initialization_and_destruction_0ii
	addq.l #8,%sp
	unlk %fp
	rts
	.cfi_endproc
.LFE4771:
	.size	_GLOBAL__sub_I_main, .-_GLOBAL__sub_I_main
	.section	.ctors,"aw",@progbits
	.align	4
	.long	_GLOBAL__sub_I_main
	.weakref	_ZL28__gthrw___pthread_key_createPjPFvPvE,__pthread_key_create
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.4.0-1ubuntu1~20.04) 9.4.0"
	.section	.note.GNU-stack,"",@progbits
