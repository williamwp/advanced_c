	.file	"7-metal-reflection.cpp"
	.section	".text"
	.section	".rodata"
	.type	_ZStL19piecewise_construct, #object
	.size	_ZStL19piecewise_construct, 1
_ZStL19piecewise_construct:
	.skip	1
	.section	.text._ZNKSt9type_infoeqERKS_,"axG",@progbits,_ZNKSt9type_infoeqERKS_,comdat
	.align 4
	.weak	_ZNKSt9type_infoeqERKS_
	.type	_ZNKSt9type_infoeqERKS_, #function
	.proc	00
_ZNKSt9type_infoeqERKS_:
.LFB746:
	.cfi_startproc
	.register	%g2, #scratch
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	stx	%i1, [%fp+2183]
	ldx	[%fp+2175], %g1
	ldx	[%g1+8], %g2
	ldx	[%fp+2183], %g1
	ldx	[%g1+8], %g1
	cmp	%g2, %g1
	be	%xcc, .L2
	 nop
	ldx	[%fp+2175], %g1
	ldx	[%g1+8], %g1
	ldub	[%g1], %g1
	sll	%g1, 24, %g1
	sra	%g1, 24, %g1
	cmp	%g1, 42
	be	%icc, .L3
	 nop
	ldx	[%fp+2175], %g1
	ldx	[%g1+8], %g2
	ldx	[%fp+2183], %g1
	ldx	[%g1+8], %g1
	mov	%g1, %o1
	mov	%g2, %o0
	call	strcmp, 0
	 nop
	mov	%o0, %g1
	cmp	%g1, 0
	bne	%icc, .L3
	 nop
.L2:
	mov	1, %g1
	ba,pt	%xcc, .L4
	 nop
.L3:
	mov	0, %g1
.L4:
	and	%g1, 0xff, %g1
	mov	%g1, %i0
	return	%i7+8
	 nop
	.cfi_endproc
.LFE746:
	.size	_ZNKSt9type_infoeqERKS_, .-_ZNKSt9type_infoeqERKS_
	.section	.text._ZnwmPv,"axG",@progbits,_ZnwmPv,comdat
	.align 4
	.weak	_ZnwmPv
	.type	_ZnwmPv, #function
	.proc	0120
_ZnwmPv:
.LFB788:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	stx	%i1, [%fp+2183]
	ldx	[%fp+2183], %g1
	mov	%g1, %i0
	return	%i7+8
	 nop
	.cfi_endproc
.LFE788:
	.size	_ZnwmPv, .-_ZnwmPv
	.section	.text._ZdlPvS_,"axG",@progbits,_ZdlPvS_,comdat
	.align 4
	.weak	_ZdlPvS_
	.type	_ZdlPvS_, #function
	.proc	020
_ZdlPvS_:
.LFB790:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	stx	%i1, [%fp+2183]
	nop
	return	%i7+8
	 nop
	.cfi_endproc
.LFE790:
	.size	_ZdlPvS_, .-_ZdlPvS_
	.section	".rodata"
	.align 8
	.type	_ZZL18__gthread_active_pvE20__gthread_active_ptr, #object
	.size	_ZZL18__gthread_active_pvE20__gthread_active_ptr, 8
_ZZL18__gthread_active_pvE20__gthread_active_ptr:
	.xword	_ZL28__gthrw___pthread_key_createPjPFvPvE
	.section	.text._ZL18__gthread_active_pv,"axG",@progbits,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv,comdat
	.align 4
	.type	_ZL18__gthread_active_pv, #function
	.proc	04
_ZL18__gthread_active_pv:
.LFB964:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	mov	1, %g2
	sethi	%hi(_ZL28__gthrw___pthread_key_createPjPFvPvE), %g1
	or	%g1, %lo(_ZL28__gthrw___pthread_key_createPjPFvPvE), %g1
	brnz	%g1, .L10
	 nop
	mov	0, %g2
.L10:
	and	%g2, 0xff, %g1
	sra	%g1, 0, %g1
	mov	%g1, %i0
	return	%i7+8
	 nop
	.cfi_endproc
.LFE964:
	.size	_ZL18__gthread_active_pv, .-_ZL18__gthread_active_pv
	.section	.text._ZN9__gnu_cxxL18__exchange_and_addEPVli,"axG",@progbits,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv,comdat
	.align 4
	.type	_ZN9__gnu_cxxL18__exchange_and_addEPVli, #function
	.proc	05
_ZN9__gnu_cxxL18__exchange_and_addEPVli:
.LFB993:
	.cfi_startproc
	.register	%g3, #scratch
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	mov	%i1, %g1
	st	%g1, [%fp+2183]
	ld	[%fp+2183], %g1
	sra	%g1, 0, %g5
	ldx	[%fp+2175], %g3
	ldx	[%g3], %g2
.L13:
	mov	%g2, %g1
	mov	%g1, %i5
	add	%g1, %g5, %g2
	casx	[%g3], %g1, %g2
	xor	%g2, %g1, %g4
	mov	0, %g1
	movre	%g4, 1, %g1
	cmp	%g1, 0
	be	%icc, .L13
	 nop
	mov	%i5, %g1
	mov	%g1, %i0
	return	%i7+8
	 nop
	.cfi_endproc
.LFE993:
	.size	_ZN9__gnu_cxxL18__exchange_and_addEPVli, .-_ZN9__gnu_cxxL18__exchange_and_addEPVli
	.section	.text._ZN9__gnu_cxxL25__exchange_and_add_singleEPli,"axG",@progbits,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv,comdat
	.align 4
	.type	_ZN9__gnu_cxxL25__exchange_and_add_singleEPli, #function
	.proc	05
_ZN9__gnu_cxxL25__exchange_and_add_singleEPli:
.LFB995:
	.cfi_startproc
	save	%sp, -192, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	mov	%i1, %g1
	st	%g1, [%fp+2183]
	ldx	[%fp+2175], %g1
	ldx	[%g1], %g1
	stx	%g1, [%fp+2039]
	ldx	[%fp+2175], %g1
	ldx	[%g1], %g2
	ld	[%fp+2183], %g1
	sra	%g1, 0, %g1
	add	%g2, %g1, %g2
	ldx	[%fp+2175], %g1
	stx	%g2, [%g1]
	ldx	[%fp+2039], %g1
	mov	%g1, %i0
	return	%i7+8
	 nop
	.cfi_endproc
.LFE995:
	.size	_ZN9__gnu_cxxL25__exchange_and_add_singleEPli, .-_ZN9__gnu_cxxL25__exchange_and_add_singleEPli
	.section	.text._ZN9__gnu_cxxL27__exchange_and_add_dispatchEPli,"axG",@progbits,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv,comdat
	.align 4
	.type	_ZN9__gnu_cxxL27__exchange_and_add_dispatchEPli, #function
	.proc	05
_ZN9__gnu_cxxL27__exchange_and_add_dispatchEPli:
.LFB997:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	mov	%i1, %g1
	st	%g1, [%fp+2183]
	call	_ZL18__gthread_active_pv, 0
	 nop
	mov	%o0, %g1
	cmp	%g0, %g1
	addx	%g0, 0, %g1
	and	%g1, 0xff, %g1
	cmp	%g1, 0
	be	%icc, .L18
	 nop
	ld	[%fp+2183], %g1
	sra	%g1, 0, %g1
	mov	%g1, %o1
	ldx	[%fp+2175], %o0
	call	_ZN9__gnu_cxxL18__exchange_and_addEPVli, 0
	 nop
	mov	%o0, %g1
	ba,pt	%xcc, .L19
	 nop
.L18:
	ld	[%fp+2183], %g1
	sra	%g1, 0, %g1
	mov	%g1, %o1
	ldx	[%fp+2175], %o0
	call	_ZN9__gnu_cxxL25__exchange_and_add_singleEPli, 0
	 nop
	mov	%o0, %g1
	nop
.L19:
	mov	%g1, %i0
	return	%i7+8
	 nop
	.cfi_endproc
.LFE997:
	.size	_ZN9__gnu_cxxL27__exchange_and_add_dispatchEPli, .-_ZN9__gnu_cxxL27__exchange_and_add_dispatchEPli
	.section	".rodata"
	.align 4
	.type	_ZN9__gnu_cxxL21__default_lock_policyE, #object
	.size	_ZN9__gnu_cxxL21__default_lock_policyE, 4
_ZN9__gnu_cxxL21__default_lock_policyE:
	.long	2
	.type	_ZStL13allocator_arg, #object
	.size	_ZStL13allocator_arg, 1
_ZStL13allocator_arg:
	.skip	1
	.type	_ZStL6ignore, #object
	.size	_ZStL6ignore, 1
_ZStL6ignore:
	.skip	1
	.weak	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag
	.section	.rodata._ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag,"aG",@progbits,_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag,comdat
	.align 8
	.type	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag, #gnu_unique_object
	.size	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag, 16
_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag:
	.skip	16
	.section	.text._ZNSt19_Sp_make_shared_tag5_S_tiEv,"axG",@progbits,_ZNSt19_Sp_make_shared_tag5_S_tiEv,comdat
	.align 4
	.weak	_ZNSt19_Sp_make_shared_tag5_S_tiEv
	.type	_ZNSt19_Sp_make_shared_tag5_S_tiEv, #function
	.proc	0110
_ZNSt19_Sp_make_shared_tag5_S_tiEv:
.LFB1963:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	sethi	%hi(_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag), %g1
	or	%g1, %lo(_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag), %g1
	mov	%g1, %i0
	return	%i7+8
	 nop
	.cfi_endproc
.LFE1963:
	.size	_ZNSt19_Sp_make_shared_tag5_S_tiEv, .-_ZNSt19_Sp_make_shared_tag5_S_tiEv
	.section	".rodata"
	.align 8
	.type	_ZL2pi, #object
	.size	_ZL2pi, 8
_ZL2pi:
	.long	1074340347
	.long	1413754136
	.weak	_ZZ13random_doublevE12distribution
	.section	.bss._ZZ13random_doublevE12distribution,"awG",@nobits,_ZZ13random_doublevE12distribution,comdat
	.align 8
	.type	_ZZ13random_doublevE12distribution, #gnu_unique_object
	.size	_ZZ13random_doublevE12distribution, 16
_ZZ13random_doublevE12distribution:
	.skip	16
	.weak	_ZGVZ13random_doublevE12distribution
	.section	.bss._ZGVZ13random_doublevE12distribution,"awG",@nobits,_ZGVZ13random_doublevE12distribution,comdat
	.align 8
	.type	_ZGVZ13random_doublevE12distribution, #gnu_unique_object
	.size	_ZGVZ13random_doublevE12distribution, 8
_ZGVZ13random_doublevE12distribution:
	.skip	8
	.weak	_ZZ13random_doublevE9generator
	.section	.bss._ZZ13random_doublevE9generator,"awG",@nobits,_ZZ13random_doublevE9generator,comdat
	.align 8
	.type	_ZZ13random_doublevE9generator, #gnu_unique_object
	.size	_ZZ13random_doublevE9generator, 5000
_ZZ13random_doublevE9generator:
	.skip	5000
	.weak	_ZGVZ13random_doublevE9generator
	.section	.bss._ZGVZ13random_doublevE9generator,"awG",@nobits,_ZGVZ13random_doublevE9generator,comdat
	.align 8
	.type	_ZGVZ13random_doublevE9generator, #gnu_unique_object
	.size	_ZGVZ13random_doublevE9generator, 8
_ZGVZ13random_doublevE9generator:
	.skip	8
	.section	".rodata"
	.align 8
.LC0:
	.long	1072693248
	.long	0
	.align 8
.LC1:
	.long	0
	.long	0
	.section	.text._Z13random_doublev,"axG",@progbits,_Z13random_doublev,comdat
	.align 4
	.weak	_Z13random_doublev
	.type	_Z13random_doublev, #function
	.proc	07
_Z13random_doublev:
.LFB3309:
	.cfi_startproc
	.cfi_personality 0,__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA3309
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	sethi	%hi(_ZGVZ13random_doublevE12distribution), %g1
	or	%g1, %lo(_ZGVZ13random_doublevE12distribution), %g1
	ldub	[%g1], %g1
	and	%g1, 0xff, %g1
	cmp	%g0, %g1
	subx	%g0, -1, %g1
	and	%g1, 0xff, %g1
	cmp	%g1, 0
	be	%icc, .L23
	 nop
	sethi	%hi(_ZGVZ13random_doublevE12distribution), %g1
	or	%g1, %lo(_ZGVZ13random_doublevE12distribution), %o0
	call	__cxa_guard_acquire, 0
	 nop
	mov	%o0, %g1
	cmp	%g0, %g1
	addx	%g0, 0, %g1
	and	%g1, 0xff, %g1
	cmp	%g1, 0
	be	%icc, .L23
	 nop
	mov	0, %i4
	sethi	%hi(.LC0), %g1
	or	%g1, %lo(.LC0), %g1
	ldd	[%g1], %f10
	sethi	%hi(.LC1), %g1
	or	%g1, %lo(.LC1), %g1
	ldd	[%g1], %f8
	fmovd	%f10, %f4
	fmovd	%f8, %f2
	sethi	%hi(_ZZ13random_doublevE12distribution), %g1
	or	%g1, %lo(_ZZ13random_doublevE12distribution), %o0
.LEHB0:
	call	_ZNSt25uniform_real_distributionIdEC1Edd, 0
	 nop
.LEHE0:
	sethi	%hi(_ZGVZ13random_doublevE12distribution), %g1
	or	%g1, %lo(_ZGVZ13random_doublevE12distribution), %o0
	call	__cxa_guard_release, 0
	 nop
.L23:
	sethi	%hi(_ZGVZ13random_doublevE9generator), %g1
	or	%g1, %lo(_ZGVZ13random_doublevE9generator), %g1
	ldub	[%g1], %g1
	and	%g1, 0xff, %g1
	cmp	%g0, %g1
	subx	%g0, -1, %g1
	and	%g1, 0xff, %g1
	cmp	%g1, 0
	be	%icc, .L24
	 nop
	sethi	%hi(_ZGVZ13random_doublevE9generator), %g1
	or	%g1, %lo(_ZGVZ13random_doublevE9generator), %o0
	call	__cxa_guard_acquire, 0
	 nop
	mov	%o0, %g1
	cmp	%g0, %g1
	addx	%g0, 0, %g1
	and	%g1, 0xff, %g1
	cmp	%g1, 0
	be	%icc, .L24
	 nop
	mov	0, %i4
	sethi	%hi(_ZZ13random_doublevE9generator), %g1
	or	%g1, %lo(_ZZ13random_doublevE9generator), %o0
.LEHB1:
	call	_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC1Ev, 0
	 nop
.LEHE1:
	sethi	%hi(_ZGVZ13random_doublevE9generator), %g1
	or	%g1, %lo(_ZGVZ13random_doublevE9generator), %o0
	call	__cxa_guard_release, 0
	 nop
.L24:
	sethi	%hi(_ZZ13random_doublevE9generator), %g1
	or	%g1, %lo(_ZZ13random_doublevE9generator), %o1
	sethi	%hi(_ZZ13random_doublevE12distribution), %g1
	or	%g1, %lo(_ZZ13random_doublevE12distribution), %o0
.LEHB2:
	call	_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEEEdRT_, 0
	 nop
	fmovd	%f0, %f8
	ba,pt	%xcc, .L32
	 nop
.L30:
	mov	%i0, %i5
	and	%i4, 0xff, %g1
	cmp	%g1, 0
	bne	%icc, .L27
	 nop
	sethi	%hi(_ZGVZ13random_doublevE12distribution), %g1
	or	%g1, %lo(_ZGVZ13random_doublevE12distribution), %o0
	call	__cxa_guard_abort, 0
	 nop
.L27:
	mov	%i5, %g1
	mov	%g1, %o0
	call	_Unwind_Resume, 0
	 nop
.L31:
	mov	%i0, %i5
	and	%i4, 0xff, %g1
	cmp	%g1, 0
	bne	%icc, .L29
	 nop
	sethi	%hi(_ZGVZ13random_doublevE9generator), %g1
	or	%g1, %lo(_ZGVZ13random_doublevE9generator), %o0
	call	__cxa_guard_abort, 0
	 nop
.L29:
	mov	%i5, %g1
	mov	%g1, %o0
	call	_Unwind_Resume, 0
	 nop
.LEHE2:
.L32:
	fmovd	%f8, %f0
	return	%i7+8
	 nop
	.cfi_endproc
.LFE3309:
	.global __gxx_personality_v0
	.section	.gcc_except_table._Z13random_doublev,"aG",@progbits,_Z13random_doublev,comdat
.LLSDA3309:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE3309-.LLSDACSB3309
.LLSDACSB3309:
	.uleb128 .LEHB0-.LFB3309
	.uleb128 .LEHE0-.LEHB0
	.uleb128 .L30-.LFB3309
	.uleb128 0
	.uleb128 .LEHB1-.LFB3309
	.uleb128 .LEHE1-.LEHB1
	.uleb128 .L31-.LFB3309
	.uleb128 0
	.uleb128 .LEHB2-.LFB3309
	.uleb128 .LEHE2-.LEHB2
	.uleb128 0
	.uleb128 0
.LLSDACSE3309:
	.section	.text._Z13random_doublev,"axG",@progbits,_Z13random_doublev,comdat
	.size	_Z13random_doublev, .-_Z13random_doublev
	.section	.text._ZplRK4vec3S1_,"axG",@progbits,_ZplRK4vec3S1_,comdat
	.align 4
	.weak	_ZplRK4vec3S1_
	.type	_ZplRK4vec3S1_, #function
	.proc	010
_ZplRK4vec3S1_:
.LFB3945:
	.cfi_startproc
	.register	%g7, #ignore
	save	%sp, -288, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+1975]
	stx	%i1, [%fp+1967]
	ldx	[%g7+40], %g1
	stx	%g1, [%fp+2039]
	mov	0, %g1
	ldx	[%fp+1975], %g1
	ldd	[%g1], %f10
	ldx	[%fp+1967], %g1
	ldd	[%g1], %f8
	faddd	%f10, %f8, %f12
	ldx	[%fp+1975], %g1
	ldd	[%g1+8], %f10
	ldx	[%fp+1967], %g1
	ldd	[%g1+8], %f8
	faddd	%f10, %f8, %f14
	ldx	[%fp+1975], %g1
	ldd	[%g1+16], %f10
	ldx	[%fp+1967], %g1
	ldd	[%g1+16], %f8
	faddd	%f10, %f8, %f8
	add	%fp, 1991, %g1
	fmovd	%f8, %f6
	fmovd	%f14, %f4
	fmovd	%f12, %f2
	mov	%g1, %o0
	call	_ZN4vec3C1Eddd, 0
	 nop
	ldx	[%fp+1991], %g1
	stx	%g1, [%fp+2015]
	ldx	[%fp+1999], %g1
	stx	%g1, [%fp+2023]
	ldx	[%fp+2007], %g1
	stx	%g1, [%fp+2031]
	ldx	[%fp+2015], %g3
	ldx	[%fp+2023], %g2
	ldx	[%fp+2031], %g1
	mov	%g3, %g4
	mov	%g2, %g3
	mov	%g1, %g2
	ldx	[%fp+2039], %g1
	ldx	[%g7+40], %g5
	xor	%g1, %g5, %g1
	mov	0, %g5
	brz	%g1, .L35
	 nop
	call	__stack_chk_fail, 0
	 nop
.L35:
	mov	%g4, %i0
	mov	%g3, %i1
	mov	%g2, %i2
	return	%i7+8
	 nop
	.cfi_endproc
.LFE3945:
	.size	_ZplRK4vec3S1_, .-_ZplRK4vec3S1_
	.section	.text._ZmiRK4vec3S1_,"axG",@progbits,_ZmiRK4vec3S1_,comdat
	.align 4
	.weak	_ZmiRK4vec3S1_
	.type	_ZmiRK4vec3S1_, #function
	.proc	010
_ZmiRK4vec3S1_:
.LFB3946:
	.cfi_startproc
	save	%sp, -288, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+1975]
	stx	%i1, [%fp+1967]
	ldx	[%g7+40], %g1
	stx	%g1, [%fp+2039]
	mov	0, %g1
	ldx	[%fp+1975], %g1
	ldd	[%g1], %f10
	ldx	[%fp+1967], %g1
	ldd	[%g1], %f8
	fsubd	%f10, %f8, %f12
	ldx	[%fp+1975], %g1
	ldd	[%g1+8], %f10
	ldx	[%fp+1967], %g1
	ldd	[%g1+8], %f8
	fsubd	%f10, %f8, %f14
	ldx	[%fp+1975], %g1
	ldd	[%g1+16], %f10
	ldx	[%fp+1967], %g1
	ldd	[%g1+16], %f8
	fsubd	%f10, %f8, %f8
	add	%fp, 1991, %g1
	fmovd	%f8, %f6
	fmovd	%f14, %f4
	fmovd	%f12, %f2
	mov	%g1, %o0
	call	_ZN4vec3C1Eddd, 0
	 nop
	ldx	[%fp+1991], %g1
	stx	%g1, [%fp+2015]
	ldx	[%fp+1999], %g1
	stx	%g1, [%fp+2023]
	ldx	[%fp+2007], %g1
	stx	%g1, [%fp+2031]
	ldx	[%fp+2015], %g3
	ldx	[%fp+2023], %g2
	ldx	[%fp+2031], %g1
	mov	%g3, %g4
	mov	%g2, %g3
	mov	%g1, %g2
	ldx	[%fp+2039], %g1
	ldx	[%g7+40], %g5
	xor	%g1, %g5, %g1
	mov	0, %g5
	brz	%g1, .L38
	 nop
	call	__stack_chk_fail, 0
	 nop
.L38:
	mov	%g4, %i0
	mov	%g3, %i1
	mov	%g2, %i2
	return	%i7+8
	 nop
	.cfi_endproc
.LFE3946:
	.size	_ZmiRK4vec3S1_, .-_ZmiRK4vec3S1_
	.section	.text._ZmldRK4vec3,"axG",@progbits,_ZmldRK4vec3,comdat
	.align 4
	.weak	_ZmldRK4vec3
	.type	_ZmldRK4vec3, #function
	.proc	010
_ZmldRK4vec3:
.LFB3948:
	.cfi_startproc
	save	%sp, -288, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	std	%f0, [%fp+2175]
	stx	%i1, [%fp+1975]
	ldx	[%g7+40], %g1
	stx	%g1, [%fp+2039]
	mov	0, %g1
	ldx	[%fp+1975], %g1
	ldd	[%g1], %f10
	ldd	[%fp+2175], %f8
	fmuld	%f10, %f8, %f12
	ldx	[%fp+1975], %g1
	ldd	[%g1+8], %f10
	ldd	[%fp+2175], %f8
	fmuld	%f10, %f8, %f14
	ldx	[%fp+1975], %g1
	ldd	[%g1+16], %f10
	ldd	[%fp+2175], %f8
	fmuld	%f10, %f8, %f8
	add	%fp, 1991, %g1
	fmovd	%f8, %f6
	fmovd	%f14, %f4
	fmovd	%f12, %f2
	mov	%g1, %o0
	call	_ZN4vec3C1Eddd, 0
	 nop
	ldx	[%fp+1991], %g1
	stx	%g1, [%fp+2015]
	ldx	[%fp+1999], %g1
	stx	%g1, [%fp+2023]
	ldx	[%fp+2007], %g1
	stx	%g1, [%fp+2031]
	ldx	[%fp+2015], %g3
	ldx	[%fp+2023], %g2
	ldx	[%fp+2031], %g1
	mov	%g3, %g4
	mov	%g2, %g3
	mov	%g1, %g2
	ldx	[%fp+2039], %g1
	ldx	[%g7+40], %g5
	xor	%g1, %g5, %g1
	mov	0, %g5
	brz	%g1, .L41
	 nop
	call	__stack_chk_fail, 0
	 nop
.L41:
	mov	%g4, %i0
	mov	%g3, %i1
	mov	%g2, %i2
	return	%i7+8
	 nop
	.cfi_endproc
.LFE3948:
	.size	_ZmldRK4vec3, .-_ZmldRK4vec3
	.section	".rodata"
	.align 8
.LC2:
	.long	1072693248
	.long	0
	.section	.text._Zdv4vec3d,"axG",@progbits,_Zdv4vec3d,comdat
	.align 4
	.weak	_Zdv4vec3d
	.type	_Zdv4vec3d, #function
	.proc	010
_Zdv4vec3d:
.LFB3950:
	.cfi_startproc
	save	%sp, -240, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	mov	%i0, %i5
	std	%f2, [%fp+2183]
	ldx	[%g7+40], %g1
	stx	%g1, [%fp+2039]
	mov	0, %g1
	sethi	%hi(.LC2), %g1
	or	%g1, %lo(.LC2), %g1
	ldd	[%g1], %f10
	ldd	[%fp+2183], %f8
	fdivd	%f10, %f8, %f8
	mov	%i5, %o1
	fmovd	%f8, %f0
	call	_ZmldRK4vec3, 0
	 nop
	mov	%o0, %g3
	mov	%o1, %g2
	mov	%o2, %g1
	stx	%g3, [%fp+2015]
	stx	%g2, [%fp+2023]
	stx	%g1, [%fp+2031]
	ldx	[%fp+2015], %g3
	ldx	[%fp+2023], %g2
	ldx	[%fp+2031], %g1
	mov	%g3, %g4
	mov	%g2, %g3
	mov	%g1, %g2
	ldx	[%fp+2039], %g1
	ldx	[%g7+40], %g5
	xor	%g1, %g5, %g1
	mov	0, %g5
	brz	%g1, .L44
	 nop
	call	__stack_chk_fail, 0
	 nop
.L44:
	mov	%g4, %i0
	mov	%g3, %i1
	mov	%g2, %i2
	return	%i7+8
	 nop
	.cfi_endproc
.LFE3950:
	.size	_Zdv4vec3d, .-_Zdv4vec3d
	.section	.text._Z11unit_vector4vec3,"axG",@progbits,_Z11unit_vector4vec3,comdat
	.align 4
	.weak	_Z11unit_vector4vec3
	.type	_Z11unit_vector4vec3, #function
	.proc	010
_Z11unit_vector4vec3:
.LFB3953:
	.cfi_startproc
	save	%sp, -240, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	mov	%i0, %i5
	ldx	[%g7+40], %g1
	stx	%g1, [%fp+2039]
	mov	0, %g1
	mov	%i5, %o0
	call	_ZNK4vec36lengthEv, 0
	 nop
	fmovd	%f0, %f8
	ldx	[%i5], %g1
	stx	%g1, [%fp+1983]
	ldx	[%i5+8], %g1
	stx	%g1, [%fp+1991]
	ldx	[%i5+16], %g1
	stx	%g1, [%fp+1999]
	add	%fp, 1983, %g1
	fmovd	%f8, %f2
	mov	%g1, %o0
	call	_Zdv4vec3d, 0
	 nop
	mov	%o0, %g3
	mov	%o1, %g2
	mov	%o2, %g1
	stx	%g3, [%fp+2015]
	stx	%g2, [%fp+2023]
	stx	%g1, [%fp+2031]
	ldx	[%fp+2015], %g3
	ldx	[%fp+2023], %g2
	ldx	[%fp+2031], %g1
	mov	%g3, %g4
	mov	%g2, %g3
	mov	%g1, %g2
	ldx	[%fp+2039], %g1
	ldx	[%g7+40], %g5
	xor	%g1, %g5, %g1
	mov	0, %g5
	brz	%g1, .L47
	 nop
	call	__stack_chk_fail, 0
	 nop
.L47:
	mov	%g4, %i0
	mov	%g3, %i1
	mov	%g2, %i2
	return	%i7+8
	 nop
	.cfi_endproc
.LFE3953:
	.size	_Z11unit_vector4vec3, .-_Z11unit_vector4vec3
	.local	_ZStL8__ioinit
	.common	_ZStL8__ioinit,1,1
	.section	.text._ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 4
	.weak	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED2Ev
	.type	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED2Ev, #function
	.proc	020
_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED2Ev:
.LFB3959:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	ldx	[%fp+2175], %g1
	add	%g1, 8, %g1
	mov	%g1, %o0
	call	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED1Ev, 0
	 nop
	nop
	return	%i7+8
	 nop
	.cfi_endproc
.LFE3959:
	.size	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED1Ev
	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED1Ev = _ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt10shared_ptrI8materialED2Ev,"axG",@progbits,_ZNSt10shared_ptrI8materialED5Ev,comdat
	.align 4
	.weak	_ZNSt10shared_ptrI8materialED2Ev
	.type	_ZNSt10shared_ptrI8materialED2Ev, #function
	.proc	020
_ZNSt10shared_ptrI8materialED2Ev:
.LFB3961:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	ldx	[%fp+2175], %g1
	mov	%g1, %o0
	call	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED2Ev, 0
	 nop
	nop
	return	%i7+8
	 nop
	.cfi_endproc
.LFE3961:
	.size	_ZNSt10shared_ptrI8materialED2Ev, .-_ZNSt10shared_ptrI8materialED2Ev
	.weak	_ZNSt10shared_ptrI8materialED1Ev
	_ZNSt10shared_ptrI8materialED1Ev = _ZNSt10shared_ptrI8materialED2Ev
	.section	.text._ZN10hit_recordC2Ev,"axG",@progbits,_ZN10hit_recordC5Ev,comdat
	.align 4
	.weak	_ZN10hit_recordC2Ev
	.type	_ZN10hit_recordC2Ev, #function
	.proc	020
_ZN10hit_recordC2Ev:
.LFB3963:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	ldx	[%fp+2175], %g1
	mov	%g1, %o0
	call	_ZN4vec3C1Ev, 0
	 nop
	ldx	[%fp+2175], %g1
	add	%g1, 24, %g1
	mov	%g1, %o0
	call	_ZN4vec3C1Ev, 0
	 nop
	ldx	[%fp+2175], %g1
	add	%g1, 48, %g1
	mov	%g1, %o0
	call	_ZNSt10shared_ptrI8materialEC1Ev, 0
	 nop
	nop
	return	%i7+8
	 nop
	.cfi_endproc
.LFE3963:
	.size	_ZN10hit_recordC2Ev, .-_ZN10hit_recordC2Ev
	.weak	_ZN10hit_recordC1Ev
	_ZN10hit_recordC1Ev = _ZN10hit_recordC2Ev
	.section	.text._ZN10hit_recordD2Ev,"axG",@progbits,_ZN10hit_recordD5Ev,comdat
	.align 4
	.weak	_ZN10hit_recordD2Ev
	.type	_ZN10hit_recordD2Ev, #function
	.proc	020
_ZN10hit_recordD2Ev:
.LFB3966:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	ldx	[%fp+2175], %g1
	add	%g1, 48, %g1
	mov	%g1, %o0
	call	_ZNSt10shared_ptrI8materialED1Ev, 0
	 nop
	nop
	return	%i7+8
	 nop
	.cfi_endproc
.LFE3966:
	.size	_ZN10hit_recordD2Ev, .-_ZN10hit_recordD2Ev
	.weak	_ZN10hit_recordD1Ev
	_ZN10hit_recordD1Ev = _ZN10hit_recordD2Ev
	.section	".rodata"
	.align 8
.LC3:
	.long	0
	.long	0
	.align 8
.LC4:
	.long	2146435072
	.long	0
	.align 8
.LC5:
	.long	1062232653
	.long	3539053052
	.align 8
.LC6:
	.long	1071644672
	.long	0
	.align 8
.LC7:
	.long	1072693248
	.long	0
	.align 8
.LC8:
	.long	1072064102
	.long	1717986918
	.section	".text"
	.align 4
	.type	_ZL9ray_colorRK3rayRK8hittablei, #function
	.proc	010
_ZL9ray_colorRK3rayRK8hittablei:
.LFB3955:
	.cfi_startproc
	.cfi_personality 0,__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA3955
	save	%sp, -544, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+1735]
	stx	%i1, [%fp+1727]
	mov	%i2, %g1
	st	%g1, [%fp+2191]
	ldx	[%g7+40], %g1
	stx	%g1, [%fp+2039]
	mov	0, %g1
	add	%fp, 1959, %g1
	mov	%g1, %o0
.LEHB3:
	call	_ZN10hit_recordC1Ev, 0
	 nop
.LEHE3:
	ld	[%fp+2191], %g1
	cmp	%g1, 0
	bg	%icc, .L53
	 nop
	sethi	%hi(.LC3), %g1
	or	%g1, %lo(.LC3), %g1
	ldd	[%g1], %f12
	sethi	%hi(.LC3), %g1
	or	%g1, %lo(.LC3), %g1
	ldd	[%g1], %f10
	sethi	%hi(.LC3), %g1
	or	%g1, %lo(.LC3), %g1
	ldd	[%g1], %f8
	add	%fp, 1863, %g1
	fmovd	%f12, %f6
	fmovd	%f10, %f4
	fmovd	%f8, %f2
	mov	%g1, %o0
.LEHB4:
	call	_ZN4vec3C1Eddd, 0
	 nop
	ldx	[%fp+1863], %g1
	stx	%g1, [%fp+1887]
	ldx	[%fp+1871], %g1
	stx	%g1, [%fp+1895]
	ldx	[%fp+1879], %g1
	stx	%g1, [%fp+1903]
	ba,pt	%xcc, .L54
	 nop
.L53:
	sethi	%hi(.LC4), %g1
	or	%g1, %lo(.LC4), %g1
	ldd	[%g1], %f8
	std	%f8, [%fp+1751]
	ldx	[%fp+1727], %g1
	ldx	[%g1], %g1
	ldx	[%g1], %g1
	add	%fp, 1959, %g3
	sethi	%hi(.LC5), %g2
	or	%g2, %lo(.LC5), %g2
	ldd	[%g2], %f8
	mov	%g3, %o4
	ldd	[%fp+1751], %f6
	fmovd	%f8, %f4
	ldx	[%fp+1735], %o1
	ldx	[%fp+1727], %o0
	call	%g1, 0
	 nop
	mov	%o0, %g1
	and	%g1, 0xff, %g1
	cmp	%g1, 0
	be	%icc, .L55
	 nop
	add	%fp, 1959, %g1
	add	%g1, 24, %g2
	add	%fp, 1959, %g1
	mov	%g2, %o1
	mov	%g1, %o0
	call	_ZplRK4vec3S1_, 0
	 nop
	mov	%o0, %g3
	mov	%o1, %g2
	mov	%o2, %g1
	stx	%g3, [%fp+1887]
	stx	%g2, [%fp+1895]
	stx	%g1, [%fp+1903]
	call	_Z18random_unit_vectorv, 0
	 nop
	mov	%o0, %g3
	mov	%o1, %g2
	mov	%o2, %g1
	stx	%g3, [%fp+1911]
	stx	%g2, [%fp+1919]
	stx	%g1, [%fp+1927]
	add	%fp, 1911, %g2
	add	%fp, 1887, %g1
	mov	%g2, %o1
	mov	%g1, %o0
	call	_ZplRK4vec3S1_, 0
	 nop
	mov	%o0, %g3
	mov	%o1, %g2
	mov	%o2, %g1
	stx	%g3, [%fp+1815]
	stx	%g2, [%fp+1823]
	stx	%g1, [%fp+1831]
	add	%fp, 1959, %g2
	add	%fp, 1815, %g1
	mov	%g2, %o1
	mov	%g1, %o0
	call	_ZmiRK4vec3S1_, 0
	 nop
	mov	%o0, %g3
	mov	%o1, %g2
	mov	%o2, %g1
	stx	%g3, [%fp+1839]
	stx	%g2, [%fp+1847]
	stx	%g1, [%fp+1855]
	add	%fp, 1839, %g3
	add	%fp, 1959, %g2
	add	%fp, 1911, %g1
	mov	%g3, %o2
	mov	%g2, %o1
	mov	%g1, %o0
	call	_ZN3rayC1ERK4vec3S2_, 0
	 nop
	ld	[%fp+2191], %g1
	add	%g1, -1, %g1
	sra	%g1, 0, %g2
	add	%fp, 1911, %g1
	mov	%g2, %o2
	ldx	[%fp+1727], %o1
	mov	%g1, %o0
	call	_ZL9ray_colorRK3rayRK8hittablei, 0
	 nop
	mov	%o0, %g3
	mov	%o1, %g2
	mov	%o2, %g1
	stx	%g3, [%fp+1863]
	stx	%g2, [%fp+1871]
	stx	%g1, [%fp+1879]
	add	%fp, 1863, %g2
	sethi	%hi(.LC6), %g1
	or	%g1, %lo(.LC6), %g1
	ldd	[%g1], %f8
	mov	%g2, %o1
	fmovd	%f8, %f0
	call	_ZmldRK4vec3, 0
	 nop
	mov	%o0, %g3
	mov	%o1, %g2
	mov	%o2, %g1
	stx	%g3, [%fp+1887]
	stx	%g2, [%fp+1895]
	stx	%g1, [%fp+1903]
	ba,pt	%xcc, .L54
	 nop
.L55:
	ldx	[%fp+1735], %o0
	call	_ZNK3ray9directionEv, 0
	 nop
	mov	%o0, %g3
	mov	%o1, %g2
	mov	%o2, %g1
	stx	%g3, [%fp+1911]
	stx	%g2, [%fp+1919]
	stx	%g1, [%fp+1927]
	ldx	[%fp+1911], %g1
	stx	%g1, [%fp+1695]
	ldx	[%fp+1919], %g1
	stx	%g1, [%fp+1703]
	ldx	[%fp+1927], %g1
	stx	%g1, [%fp+1711]
	add	%fp, 1695, %g1
	mov	%g1, %o0
	call	_Z11unit_vector4vec3, 0
	 nop
	mov	%o0, %g3
	mov	%o1, %g2
	mov	%o2, %g1
	stx	%g3, [%fp+1767]
	stx	%g2, [%fp+1775]
	stx	%g1, [%fp+1783]
	add	%fp, 1767, %g1
	mov	%g1, %o0
	call	_ZNK4vec31yEv, 0
	 nop
	fmovd	%f0, %f10
	sethi	%hi(.LC7), %g1
	or	%g1, %lo(.LC7), %g1
	ldd	[%g1], %f8
	faddd	%f10, %f8, %f10
	sethi	%hi(.LC6), %g1
	or	%g1, %lo(.LC6), %g1
	ldd	[%g1], %f8
	fmuld	%f10, %f8, %f8
	std	%f8, [%fp+1759]
	sethi	%hi(.LC7), %g1
	or	%g1, %lo(.LC7), %g1
	ldd	[%g1], %f10
	ldd	[%fp+1759], %f8
	fsubd	%f10, %f8, %f8
	std	%f8, [%fp+1687]
	sethi	%hi(.LC7), %g1
	or	%g1, %lo(.LC7), %g1
	ldd	[%g1], %f12
	sethi	%hi(.LC7), %g1
	or	%g1, %lo(.LC7), %g1
	ldd	[%g1], %f10
	sethi	%hi(.LC7), %g1
	or	%g1, %lo(.LC7), %g1
	ldd	[%g1], %f8
	add	%fp, 1791, %g1
	fmovd	%f12, %f6
	fmovd	%f10, %f4
	fmovd	%f8, %f2
	mov	%g1, %o0
	call	_ZN4vec3C1Eddd, 0
	 nop
	add	%fp, 1791, %g1
	mov	%g1, %o1
	ldd	[%fp+1687], %f0
	call	_ZmldRK4vec3, 0
	 nop
	mov	%o0, %g3
	mov	%o1, %g2
	mov	%o2, %g1
	stx	%g3, [%fp+1815]
	stx	%g2, [%fp+1823]
	stx	%g1, [%fp+1831]
	sethi	%hi(.LC7), %g1
	or	%g1, %lo(.LC7), %g1
	ldd	[%g1], %f12
	sethi	%hi(.LC8), %g1
	or	%g1, %lo(.LC8), %g1
	ldd	[%g1], %f10
	sethi	%hi(.LC6), %g1
	or	%g1, %lo(.LC6), %g1
	ldd	[%g1], %f8
	add	%fp, 1839, %g1
	fmovd	%f12, %f6
	fmovd	%f10, %f4
	fmovd	%f8, %f2
	mov	%g1, %o0
	call	_ZN4vec3C1Eddd, 0
	 nop
	add	%fp, 1839, %g1
	mov	%g1, %o1
	ldd	[%fp+1759], %f0
	call	_ZmldRK4vec3, 0
	 nop
	mov	%o0, %g3
	mov	%o1, %g2
	mov	%o2, %g1
	stx	%g3, [%fp+1863]
	stx	%g2, [%fp+1871]
	stx	%g1, [%fp+1879]
	add	%fp, 1863, %g2
	add	%fp, 1815, %g1
	mov	%g2, %o1
	mov	%g1, %o0
	call	_ZplRK4vec3S1_, 0
	 nop
.LEHE4:
	mov	%o0, %g3
	mov	%o1, %g2
	mov	%o2, %g1
	stx	%g3, [%fp+1887]
	stx	%g2, [%fp+1895]
	stx	%g1, [%fp+1903]
.L54:
	add	%fp, 1959, %g1
	mov	%g1, %o0
	call	_ZN10hit_recordD1Ev, 0
	 nop
	ldx	[%fp+1887], %g3
	ldx	[%fp+1895], %g2
	ldx	[%fp+1903], %g1
	mov	%g3, %g4
	mov	%g2, %g3
	mov	%g1, %g2
	ldx	[%fp+2039], %g1
	ldx	[%g7+40], %g5
	xor	%g1, %g5, %g1
	mov	0, %g5
	brz	%g1, .L58
	 nop
	ba,pt	%xcc, .L60
	 nop
.L59:
	mov	%i0, %i5
	add	%fp, 1959, %g1
	mov	%g1, %o0
	call	_ZN10hit_recordD1Ev, 0
	 nop
	mov	%i5, %g1
	mov	%g1, %o0
.LEHB5:
	call	_Unwind_Resume, 0
	 nop
.LEHE5:
.L60:
	call	__stack_chk_fail, 0
	 nop
.L58:
	mov	%g4, %i0
	mov	%g3, %i1
	mov	%g2, %i2
	return	%i7+8
	 nop
	.cfi_endproc
.LFE3955:
	.section	.gcc_except_table,"a",@progbits
.LLSDA3955:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE3955-.LLSDACSB3955
.LLSDACSB3955:
	.uleb128 .LEHB3-.LFB3955
	.uleb128 .LEHE3-.LEHB3
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB4-.LFB3955
	.uleb128 .LEHE4-.LEHB4
	.uleb128 .L59-.LFB3955
	.uleb128 0
	.uleb128 .LEHB5-.LFB3955
	.uleb128 .LEHE5-.LEHB5
	.uleb128 0
	.uleb128 0
.LLSDACSE3955:
	.section	".text"
	.size	_ZL9ray_colorRK3rayRK8hittablei, .-_ZL9ray_colorRK3rayRK8hittablei
	.section	.text._ZN13hittable_listD2Ev,"axG",@progbits,_ZN13hittable_listD5Ev,comdat
	.align 4
	.weak	_ZN13hittable_listD2Ev
	.type	_ZN13hittable_listD2Ev, #function
	.proc	020
_ZN13hittable_listD2Ev:
.LFB3970:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	sethi	%hi(_ZTV13hittable_list+16), %g1
	or	%g1, %lo(_ZTV13hittable_list+16), %g2
	ldx	[%fp+2175], %g1
	stx	%g2, [%g1]
	ldx	[%fp+2175], %g1
	add	%g1, 8, %g1
	mov	%g1, %o0
	call	_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED1Ev, 0
	 nop
	nop
	return	%i7+8
	 nop
	.cfi_endproc
.LFE3970:
	.size	_ZN13hittable_listD2Ev, .-_ZN13hittable_listD2Ev
	.weak	_ZN13hittable_listD1Ev
	_ZN13hittable_listD1Ev = _ZN13hittable_listD2Ev
	.section	.text._ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 4
	.weak	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED2Ev
	.type	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED2Ev, #function
	.proc	020
_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED2Ev:
.LFB3974:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	ldx	[%fp+2175], %g1
	add	%g1, 8, %g1
	mov	%g1, %o0
	call	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED1Ev, 0
	 nop
	nop
	return	%i7+8
	 nop
	.cfi_endproc
.LFE3974:
	.size	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED1Ev
	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED1Ev = _ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt10shared_ptrI6sphereED2Ev,"axG",@progbits,_ZNSt10shared_ptrI6sphereED5Ev,comdat
	.align 4
	.weak	_ZNSt10shared_ptrI6sphereED2Ev
	.type	_ZNSt10shared_ptrI6sphereED2Ev, #function
	.proc	020
_ZNSt10shared_ptrI6sphereED2Ev:
.LFB3976:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	ldx	[%fp+2175], %g1
	mov	%g1, %o0
	call	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED2Ev, 0
	 nop
	nop
	return	%i7+8
	 nop
	.cfi_endproc
.LFE3976:
	.size	_ZNSt10shared_ptrI6sphereED2Ev, .-_ZNSt10shared_ptrI6sphereED2Ev
	.weak	_ZNSt10shared_ptrI6sphereED1Ev
	_ZNSt10shared_ptrI6sphereED1Ev = _ZNSt10shared_ptrI6sphereED2Ev
	.section	.text._ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 4
	.weak	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED2Ev
	.type	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED2Ev, #function
	.proc	020
_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED2Ev:
.LFB3980:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	ldx	[%fp+2175], %g1
	add	%g1, 8, %g1
	mov	%g1, %o0
	call	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED1Ev, 0
	 nop
	nop
	return	%i7+8
	 nop
	.cfi_endproc
.LFE3980:
	.size	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED1Ev
	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED1Ev = _ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt10shared_ptrI8hittableED2Ev,"axG",@progbits,_ZNSt10shared_ptrI8hittableED5Ev,comdat
	.align 4
	.weak	_ZNSt10shared_ptrI8hittableED2Ev
	.type	_ZNSt10shared_ptrI8hittableED2Ev, #function
	.proc	020
_ZNSt10shared_ptrI8hittableED2Ev:
.LFB3982:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	ldx	[%fp+2175], %g1
	mov	%g1, %o0
	call	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED2Ev, 0
	 nop
	nop
	return	%i7+8
	 nop
	.cfi_endproc
.LFE3982:
	.size	_ZNSt10shared_ptrI8hittableED2Ev, .-_ZNSt10shared_ptrI8hittableED2Ev
	.weak	_ZNSt10shared_ptrI8hittableED1Ev
	_ZNSt10shared_ptrI8hittableED1Ev = _ZNSt10shared_ptrI8hittableED2Ev
	.section	".rodata"
	.align 8
.LC14:
	.asciz	"chapter8.ppm"
	.align 8
.LC15:
	.asciz	"P3\n"
	.align 8
.LC16:
	.asciz	"\n255\n"
	.align 8
.LC17:
	.asciz	"\rScanlines remaining: "
	.align 8
.LC20:
	.asciz	"\nDone.\n"
	.align 8
.LC9:
	.long	1073508807
	.long	477218588
	.align 8
.LC10:
	.long	-1074790400
	.long	0
	.align 8
.LC11:
	.long	0
	.long	0
	.align 8
.LC12:
	.long	1071644672
	.long	0
	.align 8
.LC13:
	.long	-1067900928
	.long	0
	.align 8
.LC18:
	.long	1081667584
	.long	0
	.align 8
.LC19:
	.long	1080819712
	.long	0
	.section	".text"
	.align 4
	.global main
	.type	main, #function
	.proc	04
main:
.LFB3968:
	.cfi_startproc
	.cfi_personality 0,__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA3968
	save	%sp, -1152, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	ldx	[%g7+40], %g1
	stx	%g1, [%fp+2039]
	mov	0, %g1
	sethi	%hi(.LC9), %g1
	or	%g1, %lo(.LC9), %g1
	ldd	[%g1], %f8
	std	%f8, [%fp+1167]
	mov	400, %g1
	st	%g1, [%fp+1143]
	mov	225, %g1
	st	%g1, [%fp+1147]
	mov	100, %g1
	st	%g1, [%fp+1151]
	mov	50, %g1
	st	%g1, [%fp+1155]
	add	%fp, 1223, %g1
	mov	%g1, %o0
.LEHB6:
	call	_ZN13hittable_listC1Ev, 0
	 nop
.LEHE6:
	sethi	%hi(.LC10), %g1
	or	%g1, %lo(.LC10), %g1
	ldd	[%g1], %f12
	sethi	%hi(.LC11), %g1
	or	%g1, %lo(.LC11), %g1
	ldd	[%g1], %f10
	sethi	%hi(.LC11), %g1
	or	%g1, %lo(.LC11), %g1
	ldd	[%g1], %f8
	add	%fp, 1527, %g1
	fmovd	%f12, %f6
	fmovd	%f10, %f4
	fmovd	%f8, %f2
	mov	%g1, %o0
.LEHB7:
	call	_ZN4vec3C1Eddd, 0
	 nop
.LEHE7:
	sethi	%hi(.LC12), %g1
	or	%g1, %lo(.LC12), %g1
	ldd	[%g1], %f8
	std	%f8, [%fp+1159]
	add	%fp, 1191, %g1
	add	%fp, 1159, %g3
	add	%fp, 1527, %g2
	mov	%g3, %o2
	mov	%g2, %o1
	mov	%g1, %o0
.LEHB8:
	call	_ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_, 0
	 nop
.LEHE8:
	add	%fp, 1191, %g2
	add	%fp, 1207, %g1
	mov	%g2, %o1
	mov	%g1, %o0
	call	_ZNSt10shared_ptrI8hittableEC1I6spherevEEOS_IT_E, 0
	 nop
	add	%fp, 1207, %g2
	add	%fp, 1223, %g1
	mov	%g2, %o1
	mov	%g1, %o0
.LEHB9:
	call	_ZN13hittable_list3addESt10shared_ptrI8hittableE, 0
	 nop
.LEHE9:
	add	%fp, 1207, %g1
	mov	%g1, %o0
	call	_ZNSt10shared_ptrI8hittableED1Ev, 0
	 nop
	add	%fp, 1191, %g1
	mov	%g1, %o0
	call	_ZNSt10shared_ptrI6sphereED1Ev, 0
	 nop
	sethi	%hi(.LC10), %g1
	or	%g1, %lo(.LC10), %g1
	ldd	[%g1], %f12
	sethi	%hi(.LC13), %g1
	or	%g1, %lo(.LC13), %g1
	ldd	[%g1], %f10
	sethi	%hi(.LC11), %g1
	or	%g1, %lo(.LC11), %g1
	ldd	[%g1], %f8
	add	%fp, 1527, %g1
	fmovd	%f12, %f6
	fmovd	%f10, %f4
	fmovd	%f8, %f2
	mov	%g1, %o0
.LEHB10:
	call	_ZN4vec3C1Eddd, 0
	 nop
.LEHE10:
	mov	100, %g1
	st	%g1, [%fp+1159]
	add	%fp, 1191, %g1
	add	%fp, 1159, %g3
	add	%fp, 1527, %g2
	mov	%g3, %o2
	mov	%g2, %o1
	mov	%g1, %o0
.LEHB11:
	call	_ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_, 0
	 nop
.LEHE11:
	add	%fp, 1191, %g2
	add	%fp, 1207, %g1
	mov	%g2, %o1
	mov	%g1, %o0
	call	_ZNSt10shared_ptrI8hittableEC1I6spherevEEOS_IT_E, 0
	 nop
	add	%fp, 1207, %g2
	add	%fp, 1223, %g1
	mov	%g2, %o1
	mov	%g1, %o0
.LEHB12:
	call	_ZN13hittable_list3addESt10shared_ptrI8hittableE, 0
	 nop
.LEHE12:
	add	%fp, 1207, %g1
	mov	%g1, %o0
	call	_ZNSt10shared_ptrI8hittableED1Ev, 0
	 nop
	add	%fp, 1191, %g1
	mov	%g1, %o0
	call	_ZNSt10shared_ptrI6sphereED1Ev, 0
	 nop
	add	%fp, 1351, %g1
	mov	%g1, %o0
.LEHB13:
	call	_ZN6cameraC1Ev, 0
	 nop
	add	%fp, 1527, %g1
	mov	%g1, %o0
	call	_ZNSt14basic_ofstreamIcSt11char_traitsIcEEC1Ev, 0
	 nop
.LEHE13:
	add	%fp, 1527, %g2
	mov	16, %o2
	sethi	%hi(.LC14), %g1
	or	%g1, %lo(.LC14), %o1
	mov	%g2, %o0
.LEHB14:
	call	_ZNSt14basic_ofstreamIcSt11char_traitsIcEE4openEPKcSt13_Ios_Openmode, 0
	 nop
	add	%fp, 1527, %g2
	sethi	%hi(.LC15), %g1
	or	%g1, %lo(.LC15), %o1
	mov	%g2, %o0
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc, 0
	 nop
	mov	%o0, %g1
	mov	400, %o1
	mov	%g1, %o0
	call	_ZNSolsEi, 0
	 nop
	mov	%o0, %g1
	mov	32, %o1
	mov	%g1, %o0
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_c, 0
	 nop
	mov	%o0, %g1
	mov	225, %o1
	mov	%g1, %o0
	call	_ZNSolsEi, 0
	 nop
	mov	%o0, %g2
	sethi	%hi(.LC16), %g1
	or	%g1, %lo(.LC16), %o1
	mov	%g2, %o0
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc, 0
	 nop
	mov	224, %g1
	st	%g1, [%fp+1131]
.L72:
	ld	[%fp+1131], %g1
	cmp	%g1, 0
	bl	%icc, .L67
	 nop
	sethi	%hi(.LC17), %g1
	or	%g1, %lo(.LC17), %o1
	sethi	%hi(_ZSt4cerr), %g1
	or	%g1, %lo(_ZSt4cerr), %o0
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc, 0
	 nop
	mov	%o0, %g2
	ld	[%fp+1131], %g1
	sra	%g1, 0, %g1
	mov	%g1, %o1
	mov	%g2, %o0
	call	_ZNSolsEi, 0
	 nop
	mov	%o0, %g1
	mov	32, %o1
	mov	%g1, %o0
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_c, 0
	 nop
	mov	%o0, %g2
	sethi	%hi(_ZSt5flushIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_), %g1
	or	%g1, %lo(_ZSt5flushIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_), %o1
	mov	%g2, %o0
	call	_ZNSolsEPFRSoS_E, 0
	 nop
	st	%g0, [%fp+1135]
.L71:
	ld	[%fp+1135], %g1
	cmp	%g1, 399
	bg	%icc, .L68
	 nop
	sethi	%hi(.LC11), %g1
	or	%g1, %lo(.LC11), %g1
	ldd	[%g1], %f12
	sethi	%hi(.LC11), %g1
	or	%g1, %lo(.LC11), %g1
	ldd	[%g1], %f10
	sethi	%hi(.LC11), %g1
	or	%g1, %lo(.LC11), %g1
	ldd	[%g1], %f8
	add	%fp, 1255, %g1
	fmovd	%f12, %f6
	fmovd	%f10, %f4
	fmovd	%f8, %f2
	mov	%g1, %o0
	call	_ZN4vec3C1Eddd, 0
	 nop
	st	%g0, [%fp+1139]
.L70:
	ld	[%fp+1139], %g1
	cmp	%g1, 99
	bg	%icc, .L69
	 nop
	ld	[%fp+1135], %g1
	st	%g1, [%fp+1079]
	ld	[%fp+1079], %f8
	fitod	%f8, %f8
	std	%f8, [%fp+1079]
	call	_Z13random_doublev, 0
	 nop
	fmovd	%f0, %f8
	ldd	[%fp+1079], %f10
	faddd	%f10, %f8, %f10
	sethi	%hi(.LC18), %g1
	or	%g1, %lo(.LC18), %g1
	ldd	[%g1], %f8
	fdivd	%f10, %f8, %f8
	std	%f8, [%fp+1175]
	ld	[%fp+1131], %g1
	st	%g1, [%fp+1079]
	ld	[%fp+1079], %f8
	fitod	%f8, %f8
	std	%f8, [%fp+1079]
	call	_Z13random_doublev, 0
	 nop
	fmovd	%f0, %f8
	ldd	[%fp+1079], %f10
	faddd	%f10, %f8, %f10
	sethi	%hi(.LC19), %g1
	or	%g1, %lo(.LC19), %g1
	ldd	[%g1], %f8
	fdivd	%f10, %f8, %f8
	std	%f8, [%fp+1183]
	add	%fp, 1303, %g1
	add	%fp, 1351, %g2
	ldd	[%fp+1183], %f6
	ldd	[%fp+1175], %f4
	mov	%g2, %o1
	mov	%g1, %o0
	call	_ZNK6camera7get_rayEdd, 0
	 nop
	add	%fp, 1223, %g2
	add	%fp, 1303, %g1
	mov	50, %o2
	mov	%g2, %o1
	mov	%g1, %o0
	call	_ZL9ray_colorRK3rayRK8hittablei, 0
	 nop
	mov	%o0, %g3
	mov	%o1, %g2
	mov	%o2, %g1
	stx	%g3, [%fp+1279]
	stx	%g2, [%fp+1287]
	stx	%g1, [%fp+1295]
	add	%fp, 1279, %g2
	add	%fp, 1255, %g1
	mov	%g2, %o1
	mov	%g1, %o0
	call	_ZN4vec3pLERKS_, 0
	 nop
	ld	[%fp+1139], %g1
	add	%g1, 1, %g1
	st	%g1, [%fp+1139]
	ba,pt	%xcc, .L70
	 nop
.L69:
	ldx	[%fp+1255], %g1
	stx	%g1, [%fp+1087]
	ldx	[%fp+1263], %g1
	stx	%g1, [%fp+1095]
	ldx	[%fp+1271], %g1
	stx	%g1, [%fp+1103]
	add	%fp, 1087, %g2
	add	%fp, 1527, %g1
	mov	100, %o2
	mov	%g2, %o1
	mov	%g1, %o0
	call	_Z11write_colorRSt14basic_ofstreamIcSt11char_traitsIcEE4vec3i, 0
	 nop
	ld	[%fp+1135], %g1
	add	%g1, 1, %g1
	st	%g1, [%fp+1135]
	ba,pt	%xcc, .L71
	 nop
.L68:
	ld	[%fp+1131], %g1
	add	%g1, -1, %g1
	st	%g1, [%fp+1131]
	ba,pt	%xcc, .L72
	 nop
.L67:
	sethi	%hi(.LC20), %g1
	or	%g1, %lo(.LC20), %o1
	sethi	%hi(_ZSt4cerr), %g1
	or	%g1, %lo(_ZSt4cerr), %o0
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc, 0
	 nop
.LEHE14:
	mov	0, %i5
	add	%fp, 1527, %g1
	mov	%g1, %o0
	call	_ZNSt14basic_ofstreamIcSt11char_traitsIcEED1Ev, 0
	 nop
	add	%fp, 1223, %g1
	mov	%g1, %o0
	call	_ZN13hittable_listD1Ev, 0
	 nop
	sra	%i5, 0, %g1
	mov	%g1, %g2
	ldx	[%fp+2039], %g1
	ldx	[%g7+40], %g3
	xor	%g1, %g3, %g1
	mov	0, %g3
	brz	%g1, .L80
	 nop
	ba,pt	%xcc, .L87
	 nop
.L83:
	mov	%i0, %i5
	add	%fp, 1207, %g1
	mov	%g1, %o0
	call	_ZNSt10shared_ptrI8hittableED1Ev, 0
	 nop
	add	%fp, 1191, %g1
	mov	%g1, %o0
	call	_ZNSt10shared_ptrI6sphereED1Ev, 0
	 nop
	mov	%i5, %g1
	ba,pt	%xcc, .L75
	 nop
.L82:
	mov	%i0, %g1
.L75:
	mov	%g1, %i5
	ba,pt	%xcc, .L76
	 nop
.L85:
	mov	%i0, %i5
	add	%fp, 1207, %g1
	mov	%g1, %o0
	call	_ZNSt10shared_ptrI8hittableED1Ev, 0
	 nop
	add	%fp, 1191, %g1
	mov	%g1, %o0
	call	_ZNSt10shared_ptrI6sphereED1Ev, 0
	 nop
	mov	%i5, %g1
	ba,pt	%xcc, .L78
	 nop
.L84:
	mov	%i0, %g1
.L78:
	mov	%g1, %i5
	ba,pt	%xcc, .L76
	 nop
.L86:
	mov	%i0, %i5
	add	%fp, 1527, %g1
	mov	%g1, %o0
	call	_ZNSt14basic_ofstreamIcSt11char_traitsIcEED1Ev, 0
	 nop
	ba,pt	%xcc, .L76
	 nop
.L81:
	mov	%i0, %i5
.L76:
	add	%fp, 1223, %g1
	mov	%g1, %o0
	call	_ZN13hittable_listD1Ev, 0
	 nop
	mov	%i5, %g1
	mov	%g1, %o0
.LEHB15:
	call	_Unwind_Resume, 0
	 nop
.LEHE15:
.L87:
	call	__stack_chk_fail, 0
	 nop
.L80:
	mov	%g2, %i0
	return	%i7+8
	 nop
	.cfi_endproc
.LFE3968:
	.section	.gcc_except_table
.LLSDA3968:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE3968-.LLSDACSB3968
.LLSDACSB3968:
	.uleb128 .LEHB6-.LFB3968
	.uleb128 .LEHE6-.LEHB6
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB7-.LFB3968
	.uleb128 .LEHE7-.LEHB7
	.uleb128 .L81-.LFB3968
	.uleb128 0
	.uleb128 .LEHB8-.LFB3968
	.uleb128 .LEHE8-.LEHB8
	.uleb128 .L82-.LFB3968
	.uleb128 0
	.uleb128 .LEHB9-.LFB3968
	.uleb128 .LEHE9-.LEHB9
	.uleb128 .L83-.LFB3968
	.uleb128 0
	.uleb128 .LEHB10-.LFB3968
	.uleb128 .LEHE10-.LEHB10
	.uleb128 .L81-.LFB3968
	.uleb128 0
	.uleb128 .LEHB11-.LFB3968
	.uleb128 .LEHE11-.LEHB11
	.uleb128 .L84-.LFB3968
	.uleb128 0
	.uleb128 .LEHB12-.LFB3968
	.uleb128 .LEHE12-.LEHB12
	.uleb128 .L85-.LFB3968
	.uleb128 0
	.uleb128 .LEHB13-.LFB3968
	.uleb128 .LEHE13-.LEHB13
	.uleb128 .L81-.LFB3968
	.uleb128 0
	.uleb128 .LEHB14-.LFB3968
	.uleb128 .LEHE14-.LEHB14
	.uleb128 .L86-.LFB3968
	.uleb128 0
	.uleb128 .LEHB15-.LFB3968
	.uleb128 .LEHE15-.LEHB15
	.uleb128 0
	.uleb128 0
.LLSDACSE3968:
	.section	".text"
	.size	main, .-main
	.section	.text._ZNSt25uniform_real_distributionIdEC2Edd,"axG",@progbits,_ZNSt25uniform_real_distributionIdEC5Edd,comdat
	.align 4
	.weak	_ZNSt25uniform_real_distributionIdEC2Edd
	.type	_ZNSt25uniform_real_distributionIdEC2Edd, #function
	.proc	020
_ZNSt25uniform_real_distributionIdEC2Edd:
.LFB4233:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	std	%f2, [%fp+2183]
	std	%f4, [%fp+2191]
	ldx	[%fp+2175], %g1
	ldd	[%fp+2191], %f4
	ldd	[%fp+2183], %f2
	mov	%g1, %o0
	call	_ZNSt25uniform_real_distributionIdE10param_typeC1Edd, 0
	 nop
	nop
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4233:
	.size	_ZNSt25uniform_real_distributionIdEC2Edd, .-_ZNSt25uniform_real_distributionIdEC2Edd
	.weak	_ZNSt25uniform_real_distributionIdEC1Edd
	_ZNSt25uniform_real_distributionIdEC1Edd = _ZNSt25uniform_real_distributionIdEC2Edd
	.section	.text._ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC2Ev,"axG",@progbits,_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC5Ev,comdat
	.align 4
	.weak	_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC2Ev
	.type	_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC2Ev, #function
	.proc	020
_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC2Ev:
.LFB4236:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	sethi	%hi(5620736), %g1
	srlx	%g1, 10, %o1
	ldx	[%fp+2175], %o0
	call	_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC1Em, 0
	 nop
	nop
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4236:
	.size	_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC2Ev, .-_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC2Ev
	.weak	_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC1Ev
	_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC1Ev = _ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC2Ev
	.section	.text._ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEEEdRT_,"axG",@progbits,_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEEEdRT_,comdat
	.align 4
	.weak	_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEEEdRT_
	.type	_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEEEdRT_, #function
	.proc	07
_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEEEdRT_:
.LFB4238:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	stx	%i1, [%fp+2183]
	ldx	[%fp+2175], %g1
	mov	%g1, %o2
	ldx	[%fp+2183], %o1
	ldx	[%fp+2175], %o0
	call	_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEEEdRT_RKNS0_10param_typeE, 0
	 nop
	fmovd	%f0, %f8
	fmovd	%f8, %f0
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4238:
	.size	_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEEEdRT_, .-_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEEEdRT_
	.section	.text._ZNSt10shared_ptrI8materialEC2Ev,"axG",@progbits,_ZNSt10shared_ptrI8materialEC5Ev,comdat
	.align 4
	.weak	_ZNSt10shared_ptrI8materialEC2Ev
	.type	_ZNSt10shared_ptrI8materialEC2Ev, #function
	.proc	020
_ZNSt10shared_ptrI8materialEC2Ev:
.LFB4266:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	ldx	[%fp+2175], %g1
	mov	%g1, %o0
	call	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC2Ev, 0
	 nop
	nop
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4266:
	.size	_ZNSt10shared_ptrI8materialEC2Ev, .-_ZNSt10shared_ptrI8materialEC2Ev
	.weak	_ZNSt10shared_ptrI8materialEC1Ev
	_ZNSt10shared_ptrI8materialEC1Ev = _ZNSt10shared_ptrI8materialEC2Ev
	.section	.text._ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 4
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED2Ev
	.type	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED2Ev, #function
	.proc	020
_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED2Ev:
.LFB4269:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	ldx	[%fp+2175], %g1
	ldx	[%g1], %g1
	brz	%g1, .L95
	 nop
	ldx	[%fp+2175], %g1
	ldx	[%g1], %g1
	mov	%g1, %o0
	call	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv, 0
	 nop
.L95:
	nop
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4269:
	.size	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED1Ev
	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED1Ev = _ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED2Ev,"axG",@progbits,_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED5Ev,comdat
	.align 4
	.weak	_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED2Ev
	.type	_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED2Ev, #function
	.proc	020
_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED2Ev:
.LFB4272:
	.cfi_startproc
	.cfi_personality 0,__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA4272
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	ldx	[%fp+2175], %g1
	ldx	[%g1], %i5
	ldx	[%fp+2175], %g1
	ldx	[%g1+8], %i4
	ldx	[%fp+2175], %g1
	mov	%g1, %o0
	call	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE19_M_get_Tp_allocatorEv, 0
	 nop
	mov	%o0, %g1
	mov	%g1, %o2
	mov	%i4, %o1
	mov	%i5, %o0
	call	_ZSt8_DestroyIPSt10shared_ptrI8hittableES2_EvT_S4_RSaIT0_E, 0
	 nop
	ldx	[%fp+2175], %g1
	mov	%g1, %o0
	call	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED2Ev, 0
	 nop
	nop
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4272:
	.section	.gcc_except_table
.LLSDA4272:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4272-.LLSDACSB4272
.LLSDACSB4272:
.LLSDACSE4272:
	.section	.text._ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED2Ev,"axG",@progbits,_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED5Ev,comdat
	.size	_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED2Ev, .-_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED2Ev
	.weak	_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED1Ev
	_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED1Ev = _ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED2Ev
	.section	.text._ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_,"axG",@progbits,_ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_,comdat
	.align 4
	.weak	_ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_
	.type	_ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_, #function
	.proc	0110
_ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_:
.LFB4274:
	.cfi_startproc
	.cfi_personality 0,__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA4274
	save	%sp, -224, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2023]
	stx	%i1, [%fp+2015]
	stx	%i2, [%fp+2007]
	ldx	[%g7+40], %g1
	stx	%g1, [%fp+2039]
	mov	0, %g1
	add	%fp, 2038, %g1
	mov	%g1, %o0
	call	_ZNSaI6sphereEC1Ev, 0
	 nop
	ldx	[%fp+2015], %o0
	call	_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE, 0
	 nop
	mov	%o0, %i5
	ldx	[%fp+2007], %o0
	call	_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE, 0
	 nop
	mov	%o0, %g3
	ldx	[%fp+2023], %g1
	add	%fp, 2038, %g2
	mov	%g3, %o3
	mov	%i5, %o2
	mov	%g2, %o1
	mov	%g1, %o0
.LEHB16:
	call	_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3dEESt10shared_ptrIT_ERKT0_DpOT1_, 0
	 nop
.LEHE16:
	add	%fp, 2038, %g1
	mov	%g1, %o0
	call	_ZNSaI6sphereED1Ev, 0
	 nop
	ldx	[%fp+2039], %g1
	ldx	[%g7+40], %g2
	xor	%g1, %g2, %g1
	mov	0, %g2
	brz	%g1, .L100
	 nop
	ba,pt	%xcc, .L102
	 nop
.L101:
	mov	%i0, %i5
	add	%fp, 2038, %g1
	mov	%g1, %o0
	call	_ZNSaI6sphereED1Ev, 0
	 nop
	mov	%i5, %g1
	mov	%g1, %o0
.LEHB17:
	call	_Unwind_Resume, 0
	 nop
.LEHE17:
.L102:
	call	__stack_chk_fail, 0
	 nop
.L100:
	ldx	[%fp+2023], %i0
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4274:
	.section	.gcc_except_table
.LLSDA4274:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4274-.LLSDACSB4274
.LLSDACSB4274:
	.uleb128 .LEHB16-.LFB4274
	.uleb128 .LEHE16-.LEHB16
	.uleb128 .L101-.LFB4274
	.uleb128 0
	.uleb128 .LEHB17-.LFB4274
	.uleb128 .LEHE17-.LEHB17
	.uleb128 0
	.uleb128 0
.LLSDACSE4274:
	.section	.text._ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_,"axG",@progbits,_ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_,comdat
	.size	_ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_, .-_ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_
	.section	.text._ZNSt10shared_ptrI8hittableEC2I6spherevEEOS_IT_E,"axG",@progbits,_ZNSt10shared_ptrI8hittableEC5I6spherevEEOS_IT_E,comdat
	.align 4
	.weak	_ZNSt10shared_ptrI8hittableEC2I6spherevEEOS_IT_E
	.type	_ZNSt10shared_ptrI8hittableEC2I6spherevEEOS_IT_E, #function
	.proc	020
_ZNSt10shared_ptrI8hittableEC2I6spherevEEOS_IT_E:
.LFB4276:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	stx	%i1, [%fp+2183]
	ldx	[%fp+2175], %i5
	ldx	[%fp+2183], %o0
	call	_ZSt4moveIRSt10shared_ptrI6sphereEEONSt16remove_referenceIT_E4typeEOS5_, 0
	 nop
	mov	%o0, %g1
	mov	%g1, %o1
	mov	%i5, %o0
	call	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC2I6spherevEEOS_IT_LS2_2EE, 0
	 nop
	nop
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4276:
	.size	_ZNSt10shared_ptrI8hittableEC2I6spherevEEOS_IT_E, .-_ZNSt10shared_ptrI8hittableEC2I6spherevEEOS_IT_E
	.weak	_ZNSt10shared_ptrI8hittableEC1I6spherevEEOS_IT_E
	_ZNSt10shared_ptrI8hittableEC1I6spherevEEOS_IT_E = _ZNSt10shared_ptrI8hittableEC2I6spherevEEOS_IT_E
	.section	.text._ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_,"axG",@progbits,_ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_,comdat
	.align 4
	.weak	_ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_
	.type	_ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_, #function
	.proc	0110
_ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_:
.LFB4281:
	.cfi_startproc
	.cfi_personality 0,__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA4281
	save	%sp, -224, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2023]
	stx	%i1, [%fp+2015]
	stx	%i2, [%fp+2007]
	ldx	[%g7+40], %g1
	stx	%g1, [%fp+2039]
	mov	0, %g1
	add	%fp, 2038, %g1
	mov	%g1, %o0
	call	_ZNSaI6sphereEC1Ev, 0
	 nop
	ldx	[%fp+2015], %o0
	call	_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE, 0
	 nop
	mov	%o0, %i5
	ldx	[%fp+2007], %o0
	call	_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE, 0
	 nop
	mov	%o0, %g3
	ldx	[%fp+2023], %g1
	add	%fp, 2038, %g2
	mov	%g3, %o3
	mov	%i5, %o2
	mov	%g2, %o1
	mov	%g1, %o0
.LEHB18:
	call	_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3iEESt10shared_ptrIT_ERKT0_DpOT1_, 0
	 nop
.LEHE18:
	add	%fp, 2038, %g1
	mov	%g1, %o0
	call	_ZNSaI6sphereED1Ev, 0
	 nop
	ldx	[%fp+2039], %g1
	ldx	[%g7+40], %g2
	xor	%g1, %g2, %g1
	mov	0, %g2
	brz	%g1, .L107
	 nop
	ba,pt	%xcc, .L109
	 nop
.L108:
	mov	%i0, %i5
	add	%fp, 2038, %g1
	mov	%g1, %o0
	call	_ZNSaI6sphereED1Ev, 0
	 nop
	mov	%i5, %g1
	mov	%g1, %o0
.LEHB19:
	call	_Unwind_Resume, 0
	 nop
.LEHE19:
.L109:
	call	__stack_chk_fail, 0
	 nop
.L107:
	ldx	[%fp+2023], %i0
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4281:
	.section	.gcc_except_table
.LLSDA4281:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4281-.LLSDACSB4281
.LLSDACSB4281:
	.uleb128 .LEHB18-.LFB4281
	.uleb128 .LEHE18-.LEHB18
	.uleb128 .L108-.LFB4281
	.uleb128 0
	.uleb128 .LEHB19-.LFB4281
	.uleb128 .LEHE19-.LEHB19
	.uleb128 0
	.uleb128 0
.LLSDACSE4281:
	.section	.text._ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_,"axG",@progbits,_ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_,comdat
	.size	_ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_, .-_ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_
	.section	.text._ZNSt25uniform_real_distributionIdE10param_typeC2Edd,"axG",@progbits,_ZNSt25uniform_real_distributionIdE10param_typeC5Edd,comdat
	.align 4
	.weak	_ZNSt25uniform_real_distributionIdE10param_typeC2Edd
	.type	_ZNSt25uniform_real_distributionIdE10param_typeC2Edd, #function
	.proc	020
_ZNSt25uniform_real_distributionIdE10param_typeC2Edd:
.LFB4408:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	std	%f2, [%fp+2183]
	std	%f4, [%fp+2191]
	ldx	[%fp+2175], %g1
	ldd	[%fp+2183], %f8
	std	%f8, [%g1]
	ldx	[%fp+2175], %g1
	ldd	[%fp+2191], %f8
	std	%f8, [%g1+8]
	nop
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4408:
	.size	_ZNSt25uniform_real_distributionIdE10param_typeC2Edd, .-_ZNSt25uniform_real_distributionIdE10param_typeC2Edd
	.weak	_ZNSt25uniform_real_distributionIdE10param_typeC1Edd
	_ZNSt25uniform_real_distributionIdE10param_typeC1Edd = _ZNSt25uniform_real_distributionIdE10param_typeC2Edd
	.section	.text._ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC2Em,"axG",@progbits,_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC5Em,comdat
	.align 4
	.weak	_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC2Em
	.type	_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC2Em, #function
	.proc	020
_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC2Em:
.LFB4411:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	stx	%i1, [%fp+2183]
	ldx	[%fp+2183], %o1
	ldx	[%fp+2175], %o0
	call	_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE4seedEm, 0
	 nop
	nop
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4411:
	.size	_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC2Em, .-_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC2Em
	.weak	_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC1Em
	_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC1Em = _ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC2Em
	.section	.text._ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEEEdRT_RKNS0_10param_typeE,"axG",@progbits,_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEEEdRT_RKNS0_10param_typeE,comdat
	.align 4
	.weak	_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEEEdRT_RKNS0_10param_typeE
	.type	_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEEEdRT_RKNS0_10param_typeE, #function
	.proc	07
_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEEEdRT_RKNS0_10param_typeE:
.LFB4413:
	.cfi_startproc
	save	%sp, -240, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2023]
	stx	%i1, [%fp+2015]
	stx	%i2, [%fp+2007]
	ldx	[%g7+40], %g1
	stx	%g1, [%fp+2039]
	mov	0, %g1
	add	%fp, 2031, %g1
	ldx	[%fp+2015], %o1
	mov	%g1, %o0
	call	_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEdEC1ERS2_, 0
	 nop
	add	%fp, 2031, %g1
	mov	%g1, %o0
	call	_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEdEclEv, 0
	 nop
	std	%f0, [%fp+1999]
	ldx	[%fp+2007], %o0
	call	_ZNKSt25uniform_real_distributionIdE10param_type1bEv, 0
	 nop
	std	%f0, [%fp+1991]
	ldx	[%fp+2007], %o0
	call	_ZNKSt25uniform_real_distributionIdE10param_type1aEv, 0
	 nop
	fmovd	%f0, %f8
	ldd	[%fp+1991], %f10
	fsubd	%f10, %f8, %f8
	ldd	[%fp+1999], %f10
	fmuld	%f10, %f8, %f8
	std	%f8, [%fp+1999]
	ldx	[%fp+2007], %o0
	call	_ZNKSt25uniform_real_distributionIdE10param_type1aEv, 0
	 nop
	fmovd	%f0, %f8
	ldd	[%fp+1999], %f10
	faddd	%f10, %f8, %f8
	ldx	[%fp+2039], %g1
	ldx	[%g7+40], %g2
	xor	%g1, %g2, %g1
	mov	0, %g2
	brz	%g1, .L114
	 nop
	call	__stack_chk_fail, 0
	 nop
.L114:
	fmovd	%f8, %f0
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4413:
	.size	_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEEEdRT_RKNS0_10param_typeE, .-_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEEEdRT_RKNS0_10param_typeE
	.section	.text._ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC2Ev,"axG",@progbits,_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC5Ev,comdat
	.align 4
	.weak	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC2Ev
	.type	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC2Ev, #function
	.proc	020
_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC2Ev:
.LFB4425:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	ldx	[%fp+2175], %g1
	stx	%g0, [%g1]
	ldx	[%fp+2175], %g1
	add	%g1, 8, %g1
	mov	%g1, %o0
	call	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1Ev, 0
	 nop
	nop
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4425:
	.size	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC2Ev, .-_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC2Ev
	.weak	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC1Ev
	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC1Ev = _ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC2Ev
	.section	.text._ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv,"axG",@progbits,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv,comdat
	.align 4
	.weak	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv
	.type	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv, #function
	.proc	020
_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv:
.LFB4427:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	ldx	[%fp+2175], %g1
	add	%g1, 8, %g1
	mov	-1, %o1
	mov	%g1, %o0
	call	_ZN9__gnu_cxxL27__exchange_and_add_dispatchEPli, 0
	 nop
	mov	%o0, %g1
	xor	%g1, 1, %g2
	mov	0, %g1
	movre	%g2, 1, %g1
	and	%g1, 0xff, %g1
	cmp	%g1, 0
	be	%icc, .L118
	 nop
	ldx	[%fp+2175], %g1
	ldx	[%g1], %g1
	add	%g1, 16, %g1
	ldx	[%g1], %g1
	ldx	[%fp+2175], %o0
	call	%g1, 0
	 nop
	ldx	[%fp+2175], %g1
	add	%g1, 16, %g1
	mov	-1, %o1
	mov	%g1, %o0
	call	_ZN9__gnu_cxxL27__exchange_and_add_dispatchEPli, 0
	 nop
	mov	%o0, %g1
	xor	%g1, 1, %g2
	mov	0, %g1
	movre	%g2, 1, %g1
	and	%g1, 0xff, %g1
	cmp	%g1, 0
	be	%icc, .L118
	 nop
	ldx	[%fp+2175], %g1
	ldx	[%g1], %g1
	add	%g1, 24, %g1
	ldx	[%g1], %g1
	ldx	[%fp+2175], %o0
	call	%g1, 0
	 nop
.L118:
	nop
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4427:
	.size	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv, .-_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv
	.section	.text._ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD2Ev,"axG",@progbits,_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD5Ev,comdat
	.align 4
	.weak	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD2Ev
	.type	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD2Ev, #function
	.proc	020
_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD2Ev:
.LFB4430:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	ldx	[%fp+2175], %o0
	call	_ZNSaISt10shared_ptrI8hittableEED2Ev, 0
	 nop
	nop
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4430:
	.size	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD2Ev, .-_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD2Ev
	.weak	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD1Ev
	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD1Ev = _ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD2Ev
	.section	.text._ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED2Ev,"axG",@progbits,_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED5Ev,comdat
	.align 4
	.weak	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED2Ev
	.type	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED2Ev, #function
	.proc	020
_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED2Ev:
.LFB4432:
	.cfi_startproc
	.cfi_personality 0,__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA4432
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	ldx	[%fp+2175], %g1
	ldx	[%g1], %g3
	ldx	[%fp+2175], %g1
	ldx	[%g1+16], %g2
	ldx	[%fp+2175], %g1
	ldx	[%g1], %g1
	sub	%g2, %g1, %g1
	srax	%g1, 4, %g1
	mov	%g1, %o2
	mov	%g3, %o1
	ldx	[%fp+2175], %o0
	call	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE13_M_deallocateEPS2_m, 0
	 nop
	ldx	[%fp+2175], %g1
	mov	%g1, %o0
	call	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD1Ev, 0
	 nop
	nop
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4432:
	.section	.gcc_except_table
.LLSDA4432:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4432-.LLSDACSB4432
.LLSDACSB4432:
.LLSDACSE4432:
	.section	.text._ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED2Ev,"axG",@progbits,_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED5Ev,comdat
	.size	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED2Ev, .-_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED2Ev
	.weak	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED1Ev
	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED1Ev = _ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED2Ev
	.section	.text._ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE19_M_get_Tp_allocatorEv,"axG",@progbits,_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE19_M_get_Tp_allocatorEv,comdat
	.align 4
	.weak	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE19_M_get_Tp_allocatorEv
	.type	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE19_M_get_Tp_allocatorEv, #function
	.proc	0110
_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE19_M_get_Tp_allocatorEv:
.LFB4434:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	ldx	[%fp+2175], %g1
	mov	%g1, %i0
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4434:
	.size	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE19_M_get_Tp_allocatorEv, .-_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE19_M_get_Tp_allocatorEv
	.section	.text._ZSt8_DestroyIPSt10shared_ptrI8hittableES2_EvT_S4_RSaIT0_E,"axG",@progbits,_ZSt8_DestroyIPSt10shared_ptrI8hittableES2_EvT_S4_RSaIT0_E,comdat
	.align 4
	.weak	_ZSt8_DestroyIPSt10shared_ptrI8hittableES2_EvT_S4_RSaIT0_E
	.type	_ZSt8_DestroyIPSt10shared_ptrI8hittableES2_EvT_S4_RSaIT0_E, #function
	.proc	020
_ZSt8_DestroyIPSt10shared_ptrI8hittableES2_EvT_S4_RSaIT0_E:
.LFB4435:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	stx	%i1, [%fp+2183]
	stx	%i2, [%fp+2191]
	ldx	[%fp+2183], %o1
	ldx	[%fp+2175], %o0
	call	_ZSt8_DestroyIPSt10shared_ptrI8hittableEEvT_S4_, 0
	 nop
	nop
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4435:
	.size	_ZSt8_DestroyIPSt10shared_ptrI8hittableES2_EvT_S4_RSaIT0_E, .-_ZSt8_DestroyIPSt10shared_ptrI8hittableES2_EvT_S4_RSaIT0_E
	.section	.text._ZNSaI6sphereEC2Ev,"axG",@progbits,_ZNSaI6sphereEC5Ev,comdat
	.align 4
	.weak	_ZNSaI6sphereEC2Ev
	.type	_ZNSaI6sphereEC2Ev, #function
	.proc	020
_ZNSaI6sphereEC2Ev:
.LFB4437:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	ldx	[%fp+2175], %o0
	call	_ZN9__gnu_cxx13new_allocatorI6sphereEC2Ev, 0
	 nop
	nop
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4437:
	.size	_ZNSaI6sphereEC2Ev, .-_ZNSaI6sphereEC2Ev
	.weak	_ZNSaI6sphereEC1Ev
	_ZNSaI6sphereEC1Ev = _ZNSaI6sphereEC2Ev
	.section	.text._ZNSaI6sphereED2Ev,"axG",@progbits,_ZNSaI6sphereED5Ev,comdat
	.align 4
	.weak	_ZNSaI6sphereED2Ev
	.type	_ZNSaI6sphereED2Ev, #function
	.proc	020
_ZNSaI6sphereED2Ev:
.LFB4440:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	ldx	[%fp+2175], %o0
	call	_ZN9__gnu_cxx13new_allocatorI6sphereED2Ev, 0
	 nop
	nop
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4440:
	.size	_ZNSaI6sphereED2Ev, .-_ZNSaI6sphereED2Ev
	.weak	_ZNSaI6sphereED1Ev
	_ZNSaI6sphereED1Ev = _ZNSaI6sphereED2Ev
	.section	.text._ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE,"axG",@progbits,_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE,comdat
	.align 4
	.weak	_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
	.type	_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE, #function
	.proc	0110
_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE:
.LFB4442:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	ldx	[%fp+2175], %g1
	mov	%g1, %i0
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4442:
	.size	_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE, .-_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
	.section	.text._ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE,"axG",@progbits,_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE,comdat
	.align 4
	.weak	_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE
	.type	_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE, #function
	.proc	0107
_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE:
.LFB4443:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	ldx	[%fp+2175], %g1
	mov	%g1, %i0
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4443:
	.size	_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE, .-_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE
	.section	.text._ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3dEESt10shared_ptrIT_ERKT0_DpOT1_,"axG",@progbits,_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3dEESt10shared_ptrIT_ERKT0_DpOT1_,comdat
	.align 4
	.weak	_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3dEESt10shared_ptrIT_ERKT0_DpOT1_
	.type	_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3dEESt10shared_ptrIT_ERKT0_DpOT1_, #function
	.proc	0110
_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3dEESt10shared_ptrIT_ERKT0_DpOT1_:
.LFB4444:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	stx	%i1, [%fp+2183]
	stx	%i2, [%fp+2191]
	stx	%i3, [%fp+2199]
	ldx	[%fp+2183], %i5
	ldx	[%fp+2191], %o0
	call	_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE, 0
	 nop
	mov	%o0, %i4
	ldx	[%fp+2199], %o0
	call	_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE, 0
	 nop
	mov	%o0, %g1
	mov	%i5, %g2
	mov	%g1, %o3
	mov	%i4, %o2
	mov	%g2, %o1
	ldx	[%fp+2175], %o0
	call	_ZNSt10shared_ptrI6sphereEC1ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_, 0
	 nop
	ldx	[%fp+2175], %i0
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4444:
	.size	_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3dEESt10shared_ptrIT_ERKT0_DpOT1_, .-_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3dEESt10shared_ptrIT_ERKT0_DpOT1_
	.section	.text._ZSt4moveIRSt10shared_ptrI6sphereEEONSt16remove_referenceIT_E4typeEOS5_,"axG",@progbits,_ZSt4moveIRSt10shared_ptrI6sphereEEONSt16remove_referenceIT_E4typeEOS5_,comdat
	.align 4
	.weak	_ZSt4moveIRSt10shared_ptrI6sphereEEONSt16remove_referenceIT_E4typeEOS5_
	.type	_ZSt4moveIRSt10shared_ptrI6sphereEEONSt16remove_referenceIT_E4typeEOS5_, #function
	.proc	0110
_ZSt4moveIRSt10shared_ptrI6sphereEEONSt16remove_referenceIT_E4typeEOS5_:
.LFB4448:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	ldx	[%fp+2175], %g1
	mov	%g1, %i0
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4448:
	.size	_ZSt4moveIRSt10shared_ptrI6sphereEEONSt16remove_referenceIT_E4typeEOS5_, .-_ZSt4moveIRSt10shared_ptrI6sphereEEONSt16remove_referenceIT_E4typeEOS5_
	.section	.text._ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC2I6spherevEEOS_IT_LS2_2EE,"axG",@progbits,_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC5I6spherevEEOS_IT_LS2_2EE,comdat
	.align 4
	.weak	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC2I6spherevEEOS_IT_LS2_2EE
	.type	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC2I6spherevEEOS_IT_LS2_2EE, #function
	.proc	020
_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC2I6spherevEEOS_IT_LS2_2EE:
.LFB4450:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	stx	%i1, [%fp+2183]
	ldx	[%fp+2183], %g1
	ldx	[%g1], %g2
	ldx	[%fp+2175], %g1
	stx	%g2, [%g1]
	ldx	[%fp+2175], %g1
	add	%g1, 8, %g1
	mov	%g1, %o0
	call	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1Ev, 0
	 nop
	ldx	[%fp+2175], %g1
	add	%g1, 8, %g2
	ldx	[%fp+2183], %g1
	add	%g1, 8, %g1
	mov	%g1, %o1
	mov	%g2, %o0
	call	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EE7_M_swapERS2_, 0
	 nop
	ldx	[%fp+2183], %g1
	stx	%g0, [%g1]
	nop
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4450:
	.size	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC2I6spherevEEOS_IT_LS2_2EE, .-_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC2I6spherevEEOS_IT_LS2_2EE
	.weak	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC1I6spherevEEOS_IT_LS2_2EE
	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC1I6spherevEEOS_IT_LS2_2EE = _ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC2I6spherevEEOS_IT_LS2_2EE
	.section	.text._ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE,"axG",@progbits,_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE,comdat
	.align 4
	.weak	_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE
	.type	_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE, #function
	.proc	0104
_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE:
.LFB4456:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	ldx	[%fp+2175], %g1
	mov	%g1, %i0
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4456:
	.size	_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE, .-_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE
	.section	.text._ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3iEESt10shared_ptrIT_ERKT0_DpOT1_,"axG",@progbits,_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3iEESt10shared_ptrIT_ERKT0_DpOT1_,comdat
	.align 4
	.weak	_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3iEESt10shared_ptrIT_ERKT0_DpOT1_
	.type	_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3iEESt10shared_ptrIT_ERKT0_DpOT1_, #function
	.proc	0110
_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3iEESt10shared_ptrIT_ERKT0_DpOT1_:
.LFB4457:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	stx	%i1, [%fp+2183]
	stx	%i2, [%fp+2191]
	stx	%i3, [%fp+2199]
	ldx	[%fp+2183], %i5
	ldx	[%fp+2191], %o0
	call	_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE, 0
	 nop
	mov	%o0, %i4
	ldx	[%fp+2199], %o0
	call	_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE, 0
	 nop
	mov	%o0, %g1
	mov	%i5, %g2
	mov	%g1, %o3
	mov	%i4, %o2
	mov	%g2, %o1
	ldx	[%fp+2175], %o0
	call	_ZNSt10shared_ptrI6sphereEC1ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_, 0
	 nop
	ldx	[%fp+2175], %i0
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4457:
	.size	_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3iEESt10shared_ptrIT_ERKT0_DpOT1_, .-_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3iEESt10shared_ptrIT_ERKT0_DpOT1_
	.section	.text._ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE4seedEm,"axG",@progbits,_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE4seedEm,comdat
	.align 4
	.weak	_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE4seedEm
	.type	_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE4seedEm, #function
	.proc	020
_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE4seedEm:
.LFB4512:
	.cfi_startproc
	save	%sp, -192, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	stx	%i1, [%fp+2183]
	ldx	[%fp+2183], %o0
	call	_ZNSt8__detail5__modImLm4294967296ELm1ELm0EEET_S1_, 0
	 nop
	mov	%o0, %g2
	ldx	[%fp+2175], %g1
	stx	%g2, [%g1]
	mov	1, %g1
	stx	%g1, [%fp+2031]
.L141:
	ldx	[%fp+2031], %g1
	cmp	%g1, 623
	bgu	%xcc, .L140
	 nop
	ldx	[%fp+2031], %g1
	add	%g1, -1, %g1
	ldx	[%fp+2175], %g2
	sllx	%g1, 3, %g1
	ldx	[%g2+%g1], %g1
	stx	%g1, [%fp+2039]
	ldx	[%fp+2039], %g1
	srlx	%g1, 30, %g1
	ldx	[%fp+2039], %g2
	xor	%g2, %g1, %g1
	stx	%g1, [%fp+2039]
	ldx	[%fp+2039], %g2
	sethi	%hi(1812432896), %g1
	or	%g1, 357, %g1
	mulx	%g2, %g1, %g1
	stx	%g1, [%fp+2039]
	ldx	[%fp+2031], %o0
	call	_ZNSt8__detail5__modImLm624ELm1ELm0EEET_S1_, 0
	 nop
	mov	%o0, %g2
	ldx	[%fp+2039], %g1
	add	%g1, %g2, %g1
	stx	%g1, [%fp+2039]
	ldx	[%fp+2039], %o0
	call	_ZNSt8__detail5__modImLm4294967296ELm1ELm0EEET_S1_, 0
	 nop
	mov	%o0, %g3
	ldx	[%fp+2175], %g2
	ldx	[%fp+2031], %g1
	sllx	%g1, 3, %g1
	stx	%g3, [%g2+%g1]
	ldx	[%fp+2031], %g1
	add	%g1, 1, %g1
	stx	%g1, [%fp+2031]
	ba,pt	%xcc, .L141
	 nop
.L140:
	ldx	[%fp+2175], %g2
	mov	39, %g1
	sllx	%g1, 7, %g1
	mov	624, %g3
	stx	%g3, [%g2+%g1]
	nop
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4512:
	.size	_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE4seedEm, .-_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE4seedEm
	.section	.text._ZNSt8__detail8_AdaptorISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEdEC2ERS2_,"axG",@progbits,_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEdEC5ERS2_,comdat
	.align 4
	.weak	_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEdEC2ERS2_
	.type	_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEdEC2ERS2_, #function
	.proc	020
_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEdEC2ERS2_:
.LFB4514:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	stx	%i1, [%fp+2183]
	ldx	[%fp+2175], %g1
	ldx	[%fp+2183], %g2
	stx	%g2, [%g1]
	nop
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4514:
	.size	_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEdEC2ERS2_, .-_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEdEC2ERS2_
	.weak	_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEdEC1ERS2_
	_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEdEC1ERS2_ = _ZNSt8__detail8_AdaptorISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEdEC2ERS2_
	.section	.text._ZNSt8__detail8_AdaptorISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEdEclEv,"axG",@progbits,_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEdEclEv,comdat
	.align 4
	.weak	_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEdEclEv
	.type	_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEdEclEv, #function
	.proc	07
_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEdEclEv:
.LFB4516:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	ldx	[%fp+2175], %g1
	ldx	[%g1], %g1
	mov	%g1, %o0
	call	_ZSt18generate_canonicalIdLm53ESt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEET_RT1_, 0
	 nop
	fmovd	%f0, %f8
	fmovd	%f8, %f0
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4516:
	.size	_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEdEclEv, .-_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEdEclEv
	.section	.text._ZNKSt25uniform_real_distributionIdE10param_type1bEv,"axG",@progbits,_ZNKSt25uniform_real_distributionIdE10param_type1bEv,comdat
	.align 4
	.weak	_ZNKSt25uniform_real_distributionIdE10param_type1bEv
	.type	_ZNKSt25uniform_real_distributionIdE10param_type1bEv, #function
	.proc	07
_ZNKSt25uniform_real_distributionIdE10param_type1bEv:
.LFB4517:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	ldx	[%fp+2175], %g1
	ldd	[%g1+8], %f8
	fmovd	%f8, %f0
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4517:
	.size	_ZNKSt25uniform_real_distributionIdE10param_type1bEv, .-_ZNKSt25uniform_real_distributionIdE10param_type1bEv
	.section	.text._ZNKSt25uniform_real_distributionIdE10param_type1aEv,"axG",@progbits,_ZNKSt25uniform_real_distributionIdE10param_type1aEv,comdat
	.align 4
	.weak	_ZNKSt25uniform_real_distributionIdE10param_type1aEv
	.type	_ZNKSt25uniform_real_distributionIdE10param_type1aEv, #function
	.proc	07
_ZNKSt25uniform_real_distributionIdE10param_type1aEv:
.LFB4518:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	ldx	[%fp+2175], %g1
	ldd	[%g1], %f8
	fmovd	%f8, %f0
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4518:
	.size	_ZNKSt25uniform_real_distributionIdE10param_type1aEv, .-_ZNKSt25uniform_real_distributionIdE10param_type1aEv
	.section	.text._ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2Ev,"axG",@progbits,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC5Ev,comdat
	.align 4
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2Ev
	.type	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2Ev, #function
	.proc	020
_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2Ev:
.LFB4524:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	ldx	[%fp+2175], %g1
	stx	%g0, [%g1]
	nop
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4524:
	.size	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2Ev, .-_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2Ev
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1Ev
	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1Ev = _ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2Ev
	.section	.text._ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,"axG",@progbits,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,comdat
	.align 4
	.weak	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.type	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, #function
	.proc	020
_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv:
.LFB4526:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	ldx	[%fp+2175], %g1
	brz	%g1, .L152
	 nop
	ldx	[%fp+2175], %g1
	ldx	[%g1], %g1
	add	%g1, 8, %g1
	ldx	[%g1], %g1
	ldx	[%fp+2175], %o0
	call	%g1, 0
	 nop
.L152:
	nop
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4526:
	.size	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, .-_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.section	.text._ZNSaISt10shared_ptrI8hittableEED2Ev,"axG",@progbits,_ZNSaISt10shared_ptrI8hittableEED5Ev,comdat
	.align 4
	.weak	_ZNSaISt10shared_ptrI8hittableEED2Ev
	.type	_ZNSaISt10shared_ptrI8hittableEED2Ev, #function
	.proc	020
_ZNSaISt10shared_ptrI8hittableEED2Ev:
.LFB4528:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	ldx	[%fp+2175], %o0
	call	_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED2Ev, 0
	 nop
	nop
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4528:
	.size	_ZNSaISt10shared_ptrI8hittableEED2Ev, .-_ZNSaISt10shared_ptrI8hittableEED2Ev
	.weak	_ZNSaISt10shared_ptrI8hittableEED1Ev
	_ZNSaISt10shared_ptrI8hittableEED1Ev = _ZNSaISt10shared_ptrI8hittableEED2Ev
	.section	.text._ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE13_M_deallocateEPS2_m,"axG",@progbits,_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE13_M_deallocateEPS2_m,comdat
	.align 4
	.weak	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE13_M_deallocateEPS2_m
	.type	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE13_M_deallocateEPS2_m, #function
	.proc	020
_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE13_M_deallocateEPS2_m:
.LFB4530:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	stx	%i1, [%fp+2183]
	stx	%i2, [%fp+2191]
	ldx	[%fp+2183], %g1
	brz	%g1, .L156
	 nop
	ldx	[%fp+2175], %g1
	ldx	[%fp+2191], %o2
	ldx	[%fp+2183], %o1
	mov	%g1, %o0
	call	_ZNSt16allocator_traitsISaISt10shared_ptrI8hittableEEE10deallocateERS3_PS2_m, 0
	 nop
.L156:
	nop
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4530:
	.size	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE13_M_deallocateEPS2_m, .-_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE13_M_deallocateEPS2_m
	.section	.text._ZSt8_DestroyIPSt10shared_ptrI8hittableEEvT_S4_,"axG",@progbits,_ZSt8_DestroyIPSt10shared_ptrI8hittableEEvT_S4_,comdat
	.align 4
	.weak	_ZSt8_DestroyIPSt10shared_ptrI8hittableEEvT_S4_
	.type	_ZSt8_DestroyIPSt10shared_ptrI8hittableEEvT_S4_, #function
	.proc	020
_ZSt8_DestroyIPSt10shared_ptrI8hittableEEvT_S4_:
.LFB4531:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	stx	%i1, [%fp+2183]
	ldx	[%fp+2183], %o1
	ldx	[%fp+2175], %o0
	call	_ZNSt12_Destroy_auxILb0EE9__destroyIPSt10shared_ptrI8hittableEEEvT_S6_, 0
	 nop
	nop
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4531:
	.size	_ZSt8_DestroyIPSt10shared_ptrI8hittableEEvT_S4_, .-_ZSt8_DestroyIPSt10shared_ptrI8hittableEEvT_S4_
	.section	.text._ZN9__gnu_cxx13new_allocatorI6sphereEC2Ev,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorI6sphereEC5Ev,comdat
	.align 4
	.weak	_ZN9__gnu_cxx13new_allocatorI6sphereEC2Ev
	.type	_ZN9__gnu_cxx13new_allocatorI6sphereEC2Ev, #function
	.proc	020
_ZN9__gnu_cxx13new_allocatorI6sphereEC2Ev:
.LFB4533:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	nop
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4533:
	.size	_ZN9__gnu_cxx13new_allocatorI6sphereEC2Ev, .-_ZN9__gnu_cxx13new_allocatorI6sphereEC2Ev
	.weak	_ZN9__gnu_cxx13new_allocatorI6sphereEC1Ev
	_ZN9__gnu_cxx13new_allocatorI6sphereEC1Ev = _ZN9__gnu_cxx13new_allocatorI6sphereEC2Ev
	.section	.text._ZN9__gnu_cxx13new_allocatorI6sphereED2Ev,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorI6sphereED5Ev,comdat
	.align 4
	.weak	_ZN9__gnu_cxx13new_allocatorI6sphereED2Ev
	.type	_ZN9__gnu_cxx13new_allocatorI6sphereED2Ev, #function
	.proc	020
_ZN9__gnu_cxx13new_allocatorI6sphereED2Ev:
.LFB4536:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	nop
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4536:
	.size	_ZN9__gnu_cxx13new_allocatorI6sphereED2Ev, .-_ZN9__gnu_cxx13new_allocatorI6sphereED2Ev
	.weak	_ZN9__gnu_cxx13new_allocatorI6sphereED1Ev
	_ZN9__gnu_cxx13new_allocatorI6sphereED1Ev = _ZN9__gnu_cxx13new_allocatorI6sphereED2Ev
	.section	.text._ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,"axG",@progbits,_ZNSt10shared_ptrI6sphereEC5ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,comdat
	.align 4
	.weak	_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.type	_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_, #function
	.proc	020
_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_:
.LFB4539:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	stx	%i1, [%fp+2183]
	stx	%i2, [%fp+2191]
	stx	%i3, [%fp+2199]
	ldx	[%fp+2175], %i5
	ldx	[%fp+2191], %o0
	call	_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE, 0
	 nop
	mov	%o0, %i4
	ldx	[%fp+2199], %o0
	call	_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE, 0
	 nop
	mov	%o0, %g2
	ldx	[%fp+2183], %g1
	mov	%g2, %o3
	mov	%i4, %o2
	mov	%g1, %o1
	mov	%i5, %o0
	call	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_, 0
	 nop
	nop
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4539:
	.size	_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_, .-_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.weak	_ZNSt10shared_ptrI6sphereEC1ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	_ZNSt10shared_ptrI6sphereEC1ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_ = _ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.section	.text._ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EE7_M_swapERS2_,"axG",@progbits,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EE7_M_swapERS2_,comdat
	.align 4
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EE7_M_swapERS2_
	.type	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EE7_M_swapERS2_, #function
	.proc	020
_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EE7_M_swapERS2_:
.LFB4544:
	.cfi_startproc
	save	%sp, -192, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	stx	%i1, [%fp+2183]
	ldx	[%fp+2183], %g1
	ldx	[%g1], %g1
	stx	%g1, [%fp+2039]
	ldx	[%fp+2175], %g1
	ldx	[%g1], %g2
	ldx	[%fp+2183], %g1
	stx	%g2, [%g1]
	ldx	[%fp+2175], %g1
	ldx	[%fp+2039], %g2
	stx	%g2, [%g1]
	nop
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4544:
	.size	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EE7_M_swapERS2_, .-_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EE7_M_swapERS2_
	.section	.text._ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,"axG",@progbits,_ZNSt10shared_ptrI6sphereEC5ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,comdat
	.align 4
	.weak	_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.type	_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_, #function
	.proc	020
_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_:
.LFB4546:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	stx	%i1, [%fp+2183]
	stx	%i2, [%fp+2191]
	stx	%i3, [%fp+2199]
	ldx	[%fp+2175], %i5
	ldx	[%fp+2191], %o0
	call	_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE, 0
	 nop
	mov	%o0, %i4
	ldx	[%fp+2199], %o0
	call	_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE, 0
	 nop
	mov	%o0, %g2
	ldx	[%fp+2183], %g1
	mov	%g2, %o3
	mov	%i4, %o2
	mov	%g1, %o1
	mov	%i5, %o0
	call	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_, 0
	 nop
	nop
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4546:
	.size	_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_, .-_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.weak	_ZNSt10shared_ptrI6sphereEC1ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	_ZNSt10shared_ptrI6sphereEC1ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_ = _ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.section	.text._ZNSt8__detail5__modImLm4294967296ELm1ELm0EEET_S1_,"axG",@progbits,_ZNSt8__detail5__modImLm4294967296ELm1ELm0EEET_S1_,comdat
	.align 4
	.weak	_ZNSt8__detail5__modImLm4294967296ELm1ELm0EEET_S1_
	.type	_ZNSt8__detail5__modImLm4294967296ELm1ELm0EEET_S1_, #function
	.proc	017
_ZNSt8__detail5__modImLm4294967296ELm1ELm0EEET_S1_:
.LFB4585:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	ldx	[%fp+2175], %o0
	call	_ZNSt8__detail4_ModImLm4294967296ELm1ELm0ELb1ELb1EE6__calcEm, 0
	 nop
	mov	%o0, %g1
	mov	%g1, %i0
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4585:
	.size	_ZNSt8__detail5__modImLm4294967296ELm1ELm0EEET_S1_, .-_ZNSt8__detail5__modImLm4294967296ELm1ELm0EEET_S1_
	.section	.text._ZNSt8__detail5__modImLm624ELm1ELm0EEET_S1_,"axG",@progbits,_ZNSt8__detail5__modImLm624ELm1ELm0EEET_S1_,comdat
	.align 4
	.weak	_ZNSt8__detail5__modImLm624ELm1ELm0EEET_S1_
	.type	_ZNSt8__detail5__modImLm624ELm1ELm0EEET_S1_, #function
	.proc	017
_ZNSt8__detail5__modImLm624ELm1ELm0EEET_S1_:
.LFB4586:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	ldx	[%fp+2175], %o0
	call	_ZNSt8__detail4_ModImLm624ELm1ELm0ELb1ELb1EE6__calcEm, 0
	 nop
	mov	%o0, %g1
	mov	%g1, %i0
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4586:
	.size	_ZNSt8__detail5__modImLm624ELm1ELm0EEET_S1_, .-_ZNSt8__detail5__modImLm624ELm1ELm0EEET_S1_
	.section	.text._ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE3minEv,"axG",@progbits,_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE3minEv,comdat
	.align 4
	.weak	_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE3minEv
	.type	_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE3minEv, #function
	.proc	017
_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE3minEv:
.LFB4590:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	mov	0, %g1
	mov	%g1, %i0
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4590:
	.size	_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE3minEv, .-_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE3minEv
	.global _Qp_dtoq
	.global _Qp_mul
	.global _Qp_qtod
	.section	".rodata"
	.align 16
.LC21:
	.long	1075773440
	.long	0
	.long	0
	.long	0
	.align 8
.LC22:
	.long	0
	.long	0
	.align 8
.LC23:
	.long	1072693248
	.long	0
	.align 8
.LC24:
	.long	1072693247
	.long	4294967295
	.section	.text._ZSt18generate_canonicalIdLm53ESt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEET_RT1_,"axG",@progbits,_ZSt18generate_canonicalIdLm53ESt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEET_RT1_,comdat
	.align 4
	.weak	_ZSt18generate_canonicalIdLm53ESt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEET_RT1_
	.type	_ZSt18generate_canonicalIdLm53ESt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEET_RT1_, #function
	.proc	07
_ZSt18generate_canonicalIdLm53ESt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEET_RT1_:
.LFB4587:
	.cfi_startproc
	save	%sp, -352, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	mov	53, %g1
	stx	%g1, [%fp+2007]
	sethi	%hi(.LC21), %g1
	or	%g1, %lo(.LC21), %g1
	ldx	[%g1], %g2
	ldx	[%g1+8], %g3
	stx	%g2, [%fp+2031]
	stx	%g3, [%fp+2039]
	mov	32, %g1
	stx	%g1, [%fp+2015]
	mov	2, %g1
	stx	%g1, [%fp+2023]
	sethi	%hi(.LC22), %g1
	or	%g1, %lo(.LC22), %g1
	ldd	[%g1], %f8
	std	%f8, [%fp+1983]
	sethi	%hi(.LC23), %g1
	or	%g1, %lo(.LC23), %g1
	ldd	[%g1], %f8
	std	%f8, [%fp+1991]
	mov	2, %g1
	stx	%g1, [%fp+1999]
.L173:
	ldx	[%fp+1999], %g1
	brz	%g1, .L170
	 nop
	ldx	[%fp+2175], %o0
	call	_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEclEv, 0
	 nop
	mov	%o0, %i5
	call	_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE3minEv, 0
	 nop
	mov	%o0, %g1
	sub	%i5, %g1, %g1
	brlz	%g1, .L171
	 nop
	stx	%g1, [%fp+1879]
	ldd	[%fp+1879], %f8
	fxtod	%f8, %f8
	ba,pt	%xcc, .L172
	 nop
.L171:
	srlx	%g1, 1, %g2
	and	%g1, 1, %g1
	or	%g2, %g1, %g2
	stx	%g2, [%fp+1879]
	ldd	[%fp+1879], %f8
	fxtod	%f8, %f8
	faddd	%f8, %f8, %f8
.L172:
	ldd	[%fp+1991], %f10
	fmuld	%f8, %f10, %f8
	ldd	[%fp+1983], %f10
	faddd	%f10, %f8, %f8
	std	%f8, [%fp+1983]
	ldd	[%fp+1991], %f8
	add	%fp, 1951, %g1
	fmovd	%f8, %f2
	mov	%g1, %o0
	call	_Qp_dtoq, 0
	 nop
	ldx	[%fp+1951], %g4
	ldx	[%fp+1959], %g5
	sethi	%hi(.LC21), %g1
	or	%g1, %lo(.LC21), %g1
	ldx	[%g1], %g2
	ldx	[%g1+8], %g3
	stx	%g4, [%fp+1919]
	stx	%g5, [%fp+1927]
	stx	%g2, [%fp+1903]
	stx	%g3, [%fp+1911]
	add	%fp, 1935, %g1
	add	%fp, 1919, %g2
	add	%fp, 1903, %g3
	mov	%g3, %o2
	mov	%g2, %o1
	mov	%g1, %o0
	call	_Qp_mul, 0
	 nop
	ldx	[%fp+1935], %g2
	ldx	[%fp+1943], %g3
	stx	%g2, [%fp+1887]
	stx	%g3, [%fp+1895]
	add	%fp, 1887, %g1
	mov	%g1, %o0
	call	_Qp_qtod, 0
	 nop
	fmovd	%f0, %f8
	std	%f8, [%fp+1991]
	ldx	[%fp+1999], %g1
	add	%g1, -1, %g1
	stx	%g1, [%fp+1999]
	ba,pt	%xcc, .L173
	 nop
.L170:
	ldd	[%fp+1983], %f10
	ldd	[%fp+1991], %f8
	fdivd	%f10, %f8, %f8
	std	%f8, [%fp+1975]
	ldd	[%fp+1975], %f10
	sethi	%hi(.LC23), %g1
	or	%g1, %lo(.LC23), %g1
	ldd	[%g1], %f8
	fcmped	%fcc0, %f10, %f8
	mov	0, %g1
	movge	%fcc0, 1, %g1
	and	%g1, 0xff, %g1
	brz	%g1, .L174
	 nop
	sethi	%hi(.LC24), %g1
	or	%g1, %lo(.LC24), %g1
	ldd	[%g1], %f8
	std	%f8, [%fp+1975]
.L174:
	ldd	[%fp+1975], %f8
	fmovd	%f8, %f0
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4587:
	.size	_ZSt18generate_canonicalIdLm53ESt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEET_RT1_, .-_ZSt18generate_canonicalIdLm53ESt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEET_RT1_
	.section	.text._ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 4
	.weak	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev
	.type	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev, #function
	.proc	020
_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev:
.LFB4597:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	sethi	%hi(_ZTVSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE+16), %g1
	or	%g1, %lo(_ZTVSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE+16), %g2
	ldx	[%fp+2175], %g1
	stx	%g2, [%g1]
	nop
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4597:
	.size	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED1Ev
	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED1Ev = _ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED0Ev,"axG",@progbits,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 4
	.weak	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED0Ev
	.type	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED0Ev, #function
	.proc	020
_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED0Ev:
.LFB4599:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	ldx	[%fp+2175], %o0
	call	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED1Ev, 0
	 nop
	mov	24, %o1
	ldx	[%fp+2175], %o0
	call	_ZdlPvm, 0
	 nop
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4599:
	.size	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED0Ev, .-_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED0Ev
	.section	.text._ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED2Ev,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED5Ev,comdat
	.align 4
	.weak	_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED2Ev
	.type	_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED2Ev, #function
	.proc	020
_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED2Ev:
.LFB4601:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	nop
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4601:
	.size	_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED2Ev, .-_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED2Ev
	.weak	_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED1Ev
	_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED1Ev = _ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED2Ev
	.section	.text._ZNSt16allocator_traitsISaISt10shared_ptrI8hittableEEE10deallocateERS3_PS2_m,"axG",@progbits,_ZNSt16allocator_traitsISaISt10shared_ptrI8hittableEEE10deallocateERS3_PS2_m,comdat
	.align 4
	.weak	_ZNSt16allocator_traitsISaISt10shared_ptrI8hittableEEE10deallocateERS3_PS2_m
	.type	_ZNSt16allocator_traitsISaISt10shared_ptrI8hittableEEE10deallocateERS3_PS2_m, #function
	.proc	020
_ZNSt16allocator_traitsISaISt10shared_ptrI8hittableEEE10deallocateERS3_PS2_m:
.LFB4603:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	stx	%i1, [%fp+2183]
	stx	%i2, [%fp+2191]
	ldx	[%fp+2191], %o2
	ldx	[%fp+2183], %o1
	ldx	[%fp+2175], %o0
	call	_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEE10deallocateEPS3_m, 0
	 nop
	nop
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4603:
	.size	_ZNSt16allocator_traitsISaISt10shared_ptrI8hittableEEE10deallocateERS3_PS2_m, .-_ZNSt16allocator_traitsISaISt10shared_ptrI8hittableEEE10deallocateERS3_PS2_m
	.section	.text._ZNSt12_Destroy_auxILb0EE9__destroyIPSt10shared_ptrI8hittableEEEvT_S6_,"axG",@progbits,_ZNSt12_Destroy_auxILb0EE9__destroyIPSt10shared_ptrI8hittableEEEvT_S6_,comdat
	.align 4
	.weak	_ZNSt12_Destroy_auxILb0EE9__destroyIPSt10shared_ptrI8hittableEEEvT_S6_
	.type	_ZNSt12_Destroy_auxILb0EE9__destroyIPSt10shared_ptrI8hittableEEEvT_S6_, #function
	.proc	020
_ZNSt12_Destroy_auxILb0EE9__destroyIPSt10shared_ptrI8hittableEEEvT_S6_:
.LFB4604:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	stx	%i1, [%fp+2183]
.L182:
	ldx	[%fp+2175], %g2
	ldx	[%fp+2183], %g1
	cmp	%g2, %g1
	be	%xcc, .L183
	 nop
	ldx	[%fp+2175], %o0
	call	_ZSt11__addressofISt10shared_ptrI8hittableEEPT_RS3_, 0
	 nop
	mov	%o0, %g1
	mov	%g1, %o0
	call	_ZSt8_DestroyISt10shared_ptrI8hittableEEvPT_, 0
	 nop
	ldx	[%fp+2175], %g1
	add	%g1, 16, %g1
	stx	%g1, [%fp+2175]
	ba,pt	%xcc, .L182
	 nop
.L183:
	nop
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4604:
	.size	_ZNSt12_Destroy_auxILb0EE9__destroyIPSt10shared_ptrI8hittableEEEvT_S6_, .-_ZNSt12_Destroy_auxILb0EE9__destroyIPSt10shared_ptrI8hittableEEEvT_S6_
	.section	.text._ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,"axG",@progbits,_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC5ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,comdat
	.align 4
	.weak	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.type	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_, #function
	.proc	020
_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_:
.LFB4606:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	stx	%i1, [%fp+2183]
	stx	%i2, [%fp+2191]
	stx	%i3, [%fp+2199]
	ldx	[%fp+2175], %g1
	stx	%g0, [%g1]
	ldx	[%fp+2175], %g1
	add	%g1, 8, %i5
	ldx	[%fp+2175], %i4
	ldx	[%fp+2191], %o0
	call	_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE, 0
	 nop
	mov	%o0, %i3
	ldx	[%fp+2199], %o0
	call	_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE, 0
	 nop
	mov	%o0, %g2
	ldx	[%fp+2183], %g1
	mov	%g2, %o4
	mov	%i3, %o3
	mov	%g1, %o2
	mov	%i4, %o1
	mov	%i5, %o0
	call	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_, 0
	 nop
	ldx	[%fp+2175], %g1
	ldx	[%g1], %g1
	mov	%g1, %o1
	ldx	[%fp+2175], %o0
	call	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EE31_M_enable_shared_from_this_withIS0_S0_EENSt9enable_ifIXntsrNS3_15__has_esft_baseIT0_vEE5valueEvE4typeEPT_, 0
	 nop
	nop
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4606:
	.size	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_, .-_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.weak	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC1ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC1ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_ = _ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.section	.text._ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,"axG",@progbits,_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC5ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,comdat
	.align 4
	.weak	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.type	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_, #function
	.proc	020
_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_:
.LFB4609:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	stx	%i1, [%fp+2183]
	stx	%i2, [%fp+2191]
	stx	%i3, [%fp+2199]
	ldx	[%fp+2175], %g1
	stx	%g0, [%g1]
	ldx	[%fp+2175], %g1
	add	%g1, 8, %i5
	ldx	[%fp+2175], %i4
	ldx	[%fp+2191], %o0
	call	_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE, 0
	 nop
	mov	%o0, %i3
	ldx	[%fp+2199], %o0
	call	_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE, 0
	 nop
	mov	%o0, %g2
	ldx	[%fp+2183], %g1
	mov	%g2, %o4
	mov	%i3, %o3
	mov	%g1, %o2
	mov	%i4, %o1
	mov	%i5, %o0
	call	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_, 0
	 nop
	ldx	[%fp+2175], %g1
	ldx	[%g1], %g1
	mov	%g1, %o1
	ldx	[%fp+2175], %o0
	call	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EE31_M_enable_shared_from_this_withIS0_S0_EENSt9enable_ifIXntsrNS3_15__has_esft_baseIT0_vEE5valueEvE4typeEPT_, 0
	 nop
	nop
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4609:
	.size	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_, .-_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.weak	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC1ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC1ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_ = _ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.section	.text._ZNSt8__detail4_ModImLm4294967296ELm1ELm0ELb1ELb1EE6__calcEm,"axG",@progbits,_ZNSt8__detail4_ModImLm4294967296ELm1ELm0ELb1ELb1EE6__calcEm,comdat
	.align 4
	.weak	_ZNSt8__detail4_ModImLm4294967296ELm1ELm0ELb1ELb1EE6__calcEm
	.type	_ZNSt8__detail4_ModImLm4294967296ELm1ELm0ELb1ELb1EE6__calcEm, #function
	.proc	017
_ZNSt8__detail4_ModImLm4294967296ELm1ELm0ELb1ELb1EE6__calcEm:
.LFB4639:
	.cfi_startproc
	save	%sp, -192, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	ldx	[%fp+2175], %g1
	stx	%g1, [%fp+2039]
	ldx	[%fp+2039], %g2
	mov	-1, %g1
	srlx	%g1, 32, %g1
	and	%g2, %g1, %g1
	stx	%g1, [%fp+2039]
	ldx	[%fp+2039], %g1
	mov	%g1, %i0
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4639:
	.size	_ZNSt8__detail4_ModImLm4294967296ELm1ELm0ELb1ELb1EE6__calcEm, .-_ZNSt8__detail4_ModImLm4294967296ELm1ELm0ELb1ELb1EE6__calcEm
	.section	.text._ZNSt8__detail4_ModImLm624ELm1ELm0ELb1ELb1EE6__calcEm,"axG",@progbits,_ZNSt8__detail4_ModImLm624ELm1ELm0ELb1ELb1EE6__calcEm,comdat
	.align 4
	.weak	_ZNSt8__detail4_ModImLm624ELm1ELm0ELb1ELb1EE6__calcEm
	.type	_ZNSt8__detail4_ModImLm624ELm1ELm0ELb1ELb1EE6__calcEm, #function
	.proc	017
_ZNSt8__detail4_ModImLm624ELm1ELm0ELb1ELb1EE6__calcEm:
.LFB4640:
	.cfi_startproc
	save	%sp, -192, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	ldx	[%fp+2175], %g1
	stx	%g1, [%fp+2039]
	ldx	[%fp+2039], %g1
	udivx	%g1, 624, %g2
	mulx	%g2, 624, %g2
	sub	%g1, %g2, %g1
	stx	%g1, [%fp+2039]
	ldx	[%fp+2039], %g1
	mov	%g1, %i0
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4640:
	.size	_ZNSt8__detail4_ModImLm624ELm1ELm0ELb1ELb1EE6__calcEm, .-_ZNSt8__detail4_ModImLm624ELm1ELm0ELb1ELb1EE6__calcEm
	.section	.text._ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEclEv,"axG",@progbits,_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEclEv,comdat
	.align 4
	.weak	_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEclEv
	.type	_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEclEv, #function
	.proc	017
_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEclEv:
.LFB4641:
	.cfi_startproc
	save	%sp, -192, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	ldx	[%fp+2175], %g2
	mov	39, %g1
	sllx	%g1, 7, %g1
	ldx	[%g2+%g1], %g1
	cmp	%g1, 623
	bleu	%xcc, .L191
	 nop
	ldx	[%fp+2175], %o0
	call	_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE11_M_gen_randEv, 0
	 nop
.L191:
	ldx	[%fp+2175], %g2
	mov	39, %g1
	sllx	%g1, 7, %g1
	ldx	[%g2+%g1], %g1
	add	%g1, 1, %g4
	ldx	[%fp+2175], %g3
	mov	39, %g2
	sllx	%g2, 7, %g2
	stx	%g4, [%g3+%g2]
	ldx	[%fp+2175], %g2
	sllx	%g1, 3, %g1
	ldx	[%g2+%g1], %g1
	stx	%g1, [%fp+2039]
	ldx	[%fp+2039], %g1
	srlx	%g1, 11, %g2
	mov	-1, %g1
	srlx	%g1, 32, %g1
	and	%g2, %g1, %g1
	ldx	[%fp+2039], %g2
	xor	%g2, %g1, %g1
	stx	%g1, [%fp+2039]
	ldx	[%fp+2039], %g1
	sllx	%g1, 7, %g2
	sethi	%hi(2636928000), %g1
	or	%g1, 640, %g1
	and	%g2, %g1, %g1
	ldx	[%fp+2039], %g2
	xor	%g2, %g1, %g1
	stx	%g1, [%fp+2039]
	ldx	[%fp+2039], %g1
	sllx	%g1, 15, %g2
	sethi	%hi(4022730752), %g1
	and	%g2, %g1, %g1
	ldx	[%fp+2039], %g2
	xor	%g2, %g1, %g1
	stx	%g1, [%fp+2039]
	ldx	[%fp+2039], %g1
	srlx	%g1, 18, %g1
	ldx	[%fp+2039], %g2
	xor	%g2, %g1, %g1
	stx	%g1, [%fp+2039]
	ldx	[%fp+2039], %g1
	mov	%g1, %i0
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4641:
	.size	_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEclEv, .-_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEclEv
	.section	.text._ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEE10deallocateEPS3_m,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEE10deallocateEPS3_m,comdat
	.align 4
	.weak	_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEE10deallocateEPS3_m
	.type	_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEE10deallocateEPS3_m, #function
	.proc	020
_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEE10deallocateEPS3_m:
.LFB4643:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	stx	%i1, [%fp+2183]
	stx	%i2, [%fp+2191]
	ldx	[%fp+2183], %o0
	call	_ZdlPv, 0
	 nop
	nop
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4643:
	.size	_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEE10deallocateEPS3_m, .-_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEE10deallocateEPS3_m
	.section	.text._ZSt11__addressofISt10shared_ptrI8hittableEEPT_RS3_,"axG",@progbits,_ZSt11__addressofISt10shared_ptrI8hittableEEPT_RS3_,comdat
	.align 4
	.weak	_ZSt11__addressofISt10shared_ptrI8hittableEEPT_RS3_
	.type	_ZSt11__addressofISt10shared_ptrI8hittableEEPT_RS3_, #function
	.proc	0110
_ZSt11__addressofISt10shared_ptrI8hittableEEPT_RS3_:
.LFB4644:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	ldx	[%fp+2175], %g1
	mov	%g1, %i0
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4644:
	.size	_ZSt11__addressofISt10shared_ptrI8hittableEEPT_RS3_, .-_ZSt11__addressofISt10shared_ptrI8hittableEEPT_RS3_
	.section	.text._ZSt8_DestroyISt10shared_ptrI8hittableEEvPT_,"axG",@progbits,_ZSt8_DestroyISt10shared_ptrI8hittableEEvPT_,comdat
	.align 4
	.weak	_ZSt8_DestroyISt10shared_ptrI8hittableEEvPT_
	.type	_ZSt8_DestroyISt10shared_ptrI8hittableEEvPT_, #function
	.proc	020
_ZSt8_DestroyISt10shared_ptrI8hittableEEvPT_:
.LFB4645:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	ldx	[%fp+2175], %o0
	call	_ZNSt10shared_ptrI8hittableED1Ev, 0
	 nop
	nop
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4645:
	.size	_ZSt8_DestroyISt10shared_ptrI8hittableEEvPT_, .-_ZSt8_DestroyISt10shared_ptrI8hittableEEvPT_
	.section	.text._ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_,"axG",@progbits,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC5I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_,comdat
	.align 4
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_
	.type	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_, #function
	.proc	020
_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_:
.LFB4647:
	.cfi_startproc
	.cfi_personality 0,__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA4647
	save	%sp, -256, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+1991]
	stx	%i1, [%fp+1983]
	stx	%i2, [%fp+2191]
	stx	%i3, [%fp+1975]
	stx	%i4, [%fp+1967]
	ldx	[%g7+40], %g1
	stx	%g1, [%fp+2039]
	mov	0, %g1
	ldx	[%fp+2191], %g2
	add	%fp, 2005, %g1
	mov	%g2, %o1
	mov	%g1, %o0
	call	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC1IS0_EERKSaIT_E, 0
	 nop
	add	%fp, 2023, %g1
	add	%fp, 2005, %g2
	mov	%g2, %o1
	mov	%g1, %o0
.LEHB20:
	call	_ZSt18__allocate_guardedISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEESt15__allocated_ptrIT_ERS8_, 0
	 nop
.LEHE20:
	add	%fp, 2023, %g1
	mov	%g1, %o0
	call	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE3getEv, 0
	 nop
	stx	%o0, [%fp+2007]
	ldx	[%fp+2191], %g2
	add	%fp, 2006, %g1
	mov	%g2, %o1
	mov	%g1, %o0
	call	_ZNSaI6sphereEC1ERKS0_, 0
	 nop
	add	%fp, 2006, %i3
	ldx	[%fp+1975], %o0
	call	_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE, 0
	 nop
	mov	%o0, %i2
	ldx	[%fp+1967], %o0
	call	_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE, 0
	 nop
	mov	%o0, %i1
	ldx	[%fp+2007], %i4
	mov	%i4, %o1
	mov	80, %o0
	call	_ZnwmPv, 0
	 nop
	mov	%o0, %i5
	mov	%i1, %o3
	mov	%i2, %o2
	mov	%i3, %o1
	mov	%i5, %o0
.LEHB21:
	call	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC1IJ4vec3dEEES1_DpOT_, 0
	 nop
.LEHE21:
	stx	%i5, [%fp+2015]
	add	%fp, 2006, %g1
	mov	%g1, %o0
	call	_ZNSaI6sphereED1Ev, 0
	 nop
	add	%fp, 2023, %g1
	mov	0, %o1
	mov	%g1, %o0
	call	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEaSEDn, 0
	 nop
	ldx	[%fp+1991], %g1
	ldx	[%fp+2015], %g2
	stx	%g2, [%g1]
	ldx	[%fp+2015], %o0
	call	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv, 0
	 nop
	mov	%o0, %g2
	ldx	[%fp+1983], %g1
	stx	%g2, [%g1]
	add	%fp, 2023, %g1
	mov	%g1, %o0
	call	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED1Ev, 0
	 nop
	add	%fp, 2005, %g1
	mov	%g1, %o0
	call	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED1Ev, 0
	 nop
	nop
	ldx	[%fp+2039], %g1
	ldx	[%g7+40], %g2
	xor	%g1, %g2, %g1
	mov	0, %g2
	brz	%g1, .L200
	 nop
	ba,pt	%xcc, .L203
	 nop
.L202:
	mov	%i0, %i3
	mov	%i4, %o1
	mov	%i5, %o0
	call	_ZdlPvS_, 0
	 nop
	mov	%i3, %i5
	add	%fp, 2006, %g1
	mov	%g1, %o0
	call	_ZNSaI6sphereED1Ev, 0
	 nop
	add	%fp, 2023, %g1
	mov	%g1, %o0
	call	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED1Ev, 0
	 nop
	ba,pt	%xcc, .L199
	 nop
.L201:
	mov	%i0, %i5
.L199:
	add	%fp, 2005, %g1
	mov	%g1, %o0
	call	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED1Ev, 0
	 nop
	mov	%i5, %g1
	mov	%g1, %o0
.LEHB22:
	call	_Unwind_Resume, 0
	 nop
.LEHE22:
.L203:
	call	__stack_chk_fail, 0
	 nop
.L200:
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4647:
	.section	.gcc_except_table
.LLSDA4647:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4647-.LLSDACSB4647
.LLSDACSB4647:
	.uleb128 .LEHB20-.LFB4647
	.uleb128 .LEHE20-.LEHB20
	.uleb128 .L201-.LFB4647
	.uleb128 0
	.uleb128 .LEHB21-.LFB4647
	.uleb128 .LEHE21-.LEHB21
	.uleb128 .L202-.LFB4647
	.uleb128 0
	.uleb128 .LEHB22-.LFB4647
	.uleb128 .LEHE22-.LEHB22
	.uleb128 0
	.uleb128 0
.LLSDACSE4647:
	.section	.text._ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_,"axG",@progbits,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC5I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_,comdat
	.size	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_, .-_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_
	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_ = _ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_
	.section	.text._ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EE31_M_enable_shared_from_this_withIS0_S0_EENSt9enable_ifIXntsrNS3_15__has_esft_baseIT0_vEE5valueEvE4typeEPT_,"axG",@progbits,_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EE31_M_enable_shared_from_this_withIS0_S0_EENSt9enable_ifIXntsrNS3_15__has_esft_baseIT0_vEE5valueEvE4typeEPT_,comdat
	.align 4
	.weak	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EE31_M_enable_shared_from_this_withIS0_S0_EENSt9enable_ifIXntsrNS3_15__has_esft_baseIT0_vEE5valueEvE4typeEPT_
	.type	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EE31_M_enable_shared_from_this_withIS0_S0_EENSt9enable_ifIXntsrNS3_15__has_esft_baseIT0_vEE5valueEvE4typeEPT_, #function
	.proc	020
_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EE31_M_enable_shared_from_this_withIS0_S0_EENSt9enable_ifIXntsrNS3_15__has_esft_baseIT0_vEE5valueEvE4typeEPT_:
.LFB4649:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	stx	%i1, [%fp+2183]
	nop
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4649:
	.size	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EE31_M_enable_shared_from_this_withIS0_S0_EENSt9enable_ifIXntsrNS3_15__has_esft_baseIT0_vEE5valueEvE4typeEPT_, .-_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EE31_M_enable_shared_from_this_withIS0_S0_EENSt9enable_ifIXntsrNS3_15__has_esft_baseIT0_vEE5valueEvE4typeEPT_
	.section	.text._ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_,"axG",@progbits,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC5I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_,comdat
	.align 4
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_
	.type	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_, #function
	.proc	020
_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_:
.LFB4651:
	.cfi_startproc
	.cfi_personality 0,__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA4651
	save	%sp, -256, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+1991]
	stx	%i1, [%fp+1983]
	stx	%i2, [%fp+2191]
	stx	%i3, [%fp+1975]
	stx	%i4, [%fp+1967]
	ldx	[%g7+40], %g1
	stx	%g1, [%fp+2039]
	mov	0, %g1
	ldx	[%fp+2191], %g2
	add	%fp, 2005, %g1
	mov	%g2, %o1
	mov	%g1, %o0
	call	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC1IS0_EERKSaIT_E, 0
	 nop
	add	%fp, 2023, %g1
	add	%fp, 2005, %g2
	mov	%g2, %o1
	mov	%g1, %o0
.LEHB23:
	call	_ZSt18__allocate_guardedISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEESt15__allocated_ptrIT_ERS8_, 0
	 nop
.LEHE23:
	add	%fp, 2023, %g1
	mov	%g1, %o0
	call	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE3getEv, 0
	 nop
	stx	%o0, [%fp+2007]
	ldx	[%fp+2191], %g2
	add	%fp, 2006, %g1
	mov	%g2, %o1
	mov	%g1, %o0
	call	_ZNSaI6sphereEC1ERKS0_, 0
	 nop
	add	%fp, 2006, %i3
	ldx	[%fp+1975], %o0
	call	_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE, 0
	 nop
	mov	%o0, %i2
	ldx	[%fp+1967], %o0
	call	_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE, 0
	 nop
	mov	%o0, %i1
	ldx	[%fp+2007], %i4
	mov	%i4, %o1
	mov	80, %o0
	call	_ZnwmPv, 0
	 nop
	mov	%o0, %i5
	mov	%i1, %o3
	mov	%i2, %o2
	mov	%i3, %o1
	mov	%i5, %o0
.LEHB24:
	call	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC1IJ4vec3iEEES1_DpOT_, 0
	 nop
.LEHE24:
	stx	%i5, [%fp+2015]
	add	%fp, 2006, %g1
	mov	%g1, %o0
	call	_ZNSaI6sphereED1Ev, 0
	 nop
	add	%fp, 2023, %g1
	mov	0, %o1
	mov	%g1, %o0
	call	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEaSEDn, 0
	 nop
	ldx	[%fp+1991], %g1
	ldx	[%fp+2015], %g2
	stx	%g2, [%g1]
	ldx	[%fp+2015], %o0
	call	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv, 0
	 nop
	mov	%o0, %g2
	ldx	[%fp+1983], %g1
	stx	%g2, [%g1]
	add	%fp, 2023, %g1
	mov	%g1, %o0
	call	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED1Ev, 0
	 nop
	add	%fp, 2005, %g1
	mov	%g1, %o0
	call	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED1Ev, 0
	 nop
	nop
	ldx	[%fp+2039], %g1
	ldx	[%g7+40], %g2
	xor	%g1, %g2, %g1
	mov	0, %g2
	brz	%g1, .L208
	 nop
	ba,pt	%xcc, .L211
	 nop
.L210:
	mov	%i0, %i3
	mov	%i4, %o1
	mov	%i5, %o0
	call	_ZdlPvS_, 0
	 nop
	mov	%i3, %i5
	add	%fp, 2006, %g1
	mov	%g1, %o0
	call	_ZNSaI6sphereED1Ev, 0
	 nop
	add	%fp, 2023, %g1
	mov	%g1, %o0
	call	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED1Ev, 0
	 nop
	ba,pt	%xcc, .L207
	 nop
.L209:
	mov	%i0, %i5
.L207:
	add	%fp, 2005, %g1
	mov	%g1, %o0
	call	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED1Ev, 0
	 nop
	mov	%i5, %g1
	mov	%g1, %o0
.LEHB25:
	call	_Unwind_Resume, 0
	 nop
.LEHE25:
.L211:
	call	__stack_chk_fail, 0
	 nop
.L208:
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4651:
	.section	.gcc_except_table
.LLSDA4651:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4651-.LLSDACSB4651
.LLSDACSB4651:
	.uleb128 .LEHB23-.LFB4651
	.uleb128 .LEHE23-.LEHB23
	.uleb128 .L209-.LFB4651
	.uleb128 0
	.uleb128 .LEHB24-.LFB4651
	.uleb128 .LEHE24-.LEHB24
	.uleb128 .L210-.LFB4651
	.uleb128 0
	.uleb128 .LEHB25-.LFB4651
	.uleb128 .LEHE25-.LEHB25
	.uleb128 0
	.uleb128 0
.LLSDACSE4651:
	.section	.text._ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_,"axG",@progbits,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC5I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_,comdat
	.size	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_, .-_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_
	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_ = _ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_
	.section	.text._ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE11_M_gen_randEv,"axG",@progbits,_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE11_M_gen_randEv,comdat
	.align 4
	.weak	_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE11_M_gen_randEv
	.type	_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE11_M_gen_randEv, #function
	.proc	020
_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE11_M_gen_randEv:
.LFB4679:
	.cfi_startproc
	save	%sp, -240, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	mov	-1, %g1
	sllx	%g1, 31, %g1
	stx	%g1, [%fp+2007]
	mov	-1, %g1
	srlx	%g1, 33, %g1
	stx	%g1, [%fp+2015]
	stx	%g0, [%fp+1991]
.L216:
	ldx	[%fp+1991], %g1
	cmp	%g1, 226
	bgu	%xcc, .L213
	 nop
	ldx	[%fp+2175], %g2
	ldx	[%fp+1991], %g1
	sllx	%g1, 3, %g1
	ldx	[%g2+%g1], %g2
	mov	-1, %g1
	sllx	%g1, 31, %g1
	and	%g2, %g1, %g2
	ldx	[%fp+1991], %g1
	add	%g1, 1, %g1
	ldx	[%fp+2175], %g3
	sllx	%g1, 3, %g1
	ldx	[%g3+%g1], %g3
	mov	-1, %g1
	srlx	%g1, 33, %g1
	and	%g3, %g1, %g1
	or	%g2, %g1, %g1
	stx	%g1, [%fp+2023]
	ldx	[%fp+1991], %g1
	add	%g1, 397, %g1
	ldx	[%fp+2175], %g2
	sllx	%g1, 3, %g1
	ldx	[%g2+%g1], %g2
	ldx	[%fp+2023], %g1
	srlx	%g1, 1, %g1
	xor	%g2, %g1, %g2
	ldx	[%fp+2023], %g1
	and	%g1, 1, %g1
	brz	%g1, .L214
	 nop
	sethi	%hi(2567483392), %g1
	or	%g1, 223, %g1
	ba,pt	%xcc, .L215
	 nop
.L214:
	mov	0, %g1
.L215:
	xor	%g1, %g2, %g3
	ldx	[%fp+2175], %g2
	ldx	[%fp+1991], %g1
	sllx	%g1, 3, %g1
	stx	%g3, [%g2+%g1]
	ldx	[%fp+1991], %g1
	add	%g1, 1, %g1
	stx	%g1, [%fp+1991]
	ba,pt	%xcc, .L216
	 nop
.L213:
	mov	227, %g1
	stx	%g1, [%fp+1999]
.L220:
	ldx	[%fp+1999], %g1
	cmp	%g1, 622
	bgu	%xcc, .L217
	 nop
	ldx	[%fp+2175], %g2
	ldx	[%fp+1999], %g1
	sllx	%g1, 3, %g1
	ldx	[%g2+%g1], %g2
	mov	-1, %g1
	sllx	%g1, 31, %g1
	and	%g2, %g1, %g2
	ldx	[%fp+1999], %g1
	add	%g1, 1, %g1
	ldx	[%fp+2175], %g3
	sllx	%g1, 3, %g1
	ldx	[%g3+%g1], %g3
	mov	-1, %g1
	srlx	%g1, 33, %g1
	and	%g3, %g1, %g1
	or	%g2, %g1, %g1
	stx	%g1, [%fp+2031]
	ldx	[%fp+1999], %g1
	add	%g1, -227, %g1
	ldx	[%fp+2175], %g2
	sllx	%g1, 3, %g1
	ldx	[%g2+%g1], %g2
	ldx	[%fp+2031], %g1
	srlx	%g1, 1, %g1
	xor	%g2, %g1, %g2
	ldx	[%fp+2031], %g1
	and	%g1, 1, %g1
	brz	%g1, .L218
	 nop
	sethi	%hi(2567483392), %g1
	or	%g1, 223, %g1
	ba,pt	%xcc, .L219
	 nop
.L218:
	mov	0, %g1
.L219:
	xor	%g1, %g2, %g3
	ldx	[%fp+2175], %g2
	ldx	[%fp+1999], %g1
	sllx	%g1, 3, %g1
	stx	%g3, [%g2+%g1]
	ldx	[%fp+1999], %g1
	add	%g1, 1, %g1
	stx	%g1, [%fp+1999]
	ba,pt	%xcc, .L220
	 nop
.L217:
	ldx	[%fp+2175], %g2
	mov	623, %g1
	sllx	%g1, 3, %g1
	ldx	[%g2+%g1], %g2
	mov	-1, %g1
	sllx	%g1, 31, %g1
	and	%g2, %g1, %g2
	ldx	[%fp+2175], %g1
	ldx	[%g1], %g3
	mov	-1, %g1
	srlx	%g1, 33, %g1
	and	%g3, %g1, %g1
	or	%g2, %g1, %g1
	stx	%g1, [%fp+2039]
	ldx	[%fp+2175], %g1
	ldx	[%g1+3168], %g2
	ldx	[%fp+2039], %g1
	srlx	%g1, 1, %g1
	xor	%g2, %g1, %g2
	ldx	[%fp+2039], %g1
	and	%g1, 1, %g1
	brz	%g1, .L221
	 nop
	sethi	%hi(2567483392), %g1
	or	%g1, 223, %g1
	ba,pt	%xcc, .L222
	 nop
.L221:
	mov	0, %g1
.L222:
	xor	%g1, %g2, %g3
	ldx	[%fp+2175], %g2
	mov	623, %g1
	sllx	%g1, 3, %g1
	stx	%g3, [%g2+%g1]
	ldx	[%fp+2175], %g2
	mov	39, %g1
	sllx	%g1, 7, %g1
	stx	%g0, [%g2+%g1]
	nop
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4679:
	.size	_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE11_M_gen_randEv, .-_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE11_M_gen_randEv
	.section	.text._ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC2IS0_EERKSaIT_E,"axG",@progbits,_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC5IS0_EERKSaIT_E,comdat
	.align 4
	.weak	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC2IS0_EERKSaIT_E
	.type	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC2IS0_EERKSaIT_E, #function
	.proc	020
_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC2IS0_EERKSaIT_E:
.LFB4682:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	stx	%i1, [%fp+2183]
	ldx	[%fp+2175], %o0
	call	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC2Ev, 0
	 nop
	nop
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4682:
	.size	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC2IS0_EERKSaIT_E, .-_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC2IS0_EERKSaIT_E
	.weak	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC1IS0_EERKSaIT_E
	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC1IS0_EERKSaIT_E = _ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC2IS0_EERKSaIT_E
	.section	.text._ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED2Ev,"axG",@progbits,_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED5Ev,comdat
	.align 4
	.weak	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED2Ev
	.type	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED2Ev, #function
	.proc	020
_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED2Ev:
.LFB4685:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	ldx	[%fp+2175], %o0
	call	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED2Ev, 0
	 nop
	nop
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4685:
	.size	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED2Ev, .-_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED2Ev
	.weak	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED1Ev
	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED1Ev = _ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED2Ev
	.section	.text._ZSt18__allocate_guardedISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEESt15__allocated_ptrIT_ERS8_,"axG",@progbits,_ZSt18__allocate_guardedISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEESt15__allocated_ptrIT_ERS8_,comdat
	.align 4
	.weak	_ZSt18__allocate_guardedISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEESt15__allocated_ptrIT_ERS8_
	.type	_ZSt18__allocate_guardedISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEESt15__allocated_ptrIT_ERS8_, #function
	.proc	0110
_ZSt18__allocate_guardedISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEESt15__allocated_ptrIT_ERS8_:
.LFB4687:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	stx	%i1, [%fp+2183]
	mov	1, %o1
	ldx	[%fp+2183], %o0
	call	_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE8allocateERS6_m, 0
	 nop
	mov	%o0, %g1
	mov	%g1, %o2
	ldx	[%fp+2183], %o1
	ldx	[%fp+2175], %o0
	call	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC1ERS6_PS5_, 0
	 nop
	ldx	[%fp+2175], %i0
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4687:
	.size	_ZSt18__allocate_guardedISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEESt15__allocated_ptrIT_ERS8_, .-_ZSt18__allocate_guardedISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEESt15__allocated_ptrIT_ERS8_
	.section	.text._ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED2Ev,"axG",@progbits,_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED5Ev,comdat
	.align 4
	.weak	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED2Ev
	.type	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED2Ev, #function
	.proc	020
_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED2Ev:
.LFB4689:
	.cfi_startproc
	.cfi_personality 0,__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA4689
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	ldx	[%fp+2175], %g1
	ldx	[%g1+8], %g1
	brz	%g1, .L229
	 nop
	ldx	[%fp+2175], %g1
	ldx	[%g1], %g2
	ldx	[%fp+2175], %g1
	ldx	[%g1+8], %g1
	mov	1, %o2
	mov	%g1, %o1
	mov	%g2, %o0
	call	_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE10deallocateERS6_PS5_m, 0
	 nop
.L229:
	nop
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4689:
	.section	.gcc_except_table
.LLSDA4689:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4689-.LLSDACSB4689
.LLSDACSB4689:
.LLSDACSE4689:
	.section	.text._ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED2Ev,"axG",@progbits,_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED5Ev,comdat
	.size	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED2Ev, .-_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED2Ev
	.weak	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED1Ev
	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED1Ev = _ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED2Ev
	.section	.text._ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE3getEv,"axG",@progbits,_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE3getEv,comdat
	.align 4
	.weak	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE3getEv
	.type	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE3getEv, #function
	.proc	0110
_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE3getEv:
.LFB4694:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	ldx	[%fp+2175], %g1
	ldx	[%g1+8], %g1
	mov	%g1, %o0
	call	_ZSt12__to_addressISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEPT_S7_, 0
	 nop
	mov	%o0, %g1
	mov	%g1, %i0
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4694:
	.size	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE3getEv, .-_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE3getEv
	.section	.text._ZNSaI6sphereEC2ERKS0_,"axG",@progbits,_ZNSaI6sphereEC5ERKS0_,comdat
	.align 4
	.weak	_ZNSaI6sphereEC2ERKS0_
	.type	_ZNSaI6sphereEC2ERKS0_, #function
	.proc	020
_ZNSaI6sphereEC2ERKS0_:
.LFB4696:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	stx	%i1, [%fp+2183]
	ldx	[%fp+2183], %o1
	ldx	[%fp+2175], %o0
	call	_ZN9__gnu_cxx13new_allocatorI6sphereEC2ERKS2_, 0
	 nop
	nop
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4696:
	.size	_ZNSaI6sphereEC2ERKS0_, .-_ZNSaI6sphereEC2ERKS0_
	.weak	_ZNSaI6sphereEC1ERKS0_
	_ZNSaI6sphereEC1ERKS0_ = _ZNSaI6sphereEC2ERKS0_
	.section	.text._ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED2Ev,"axG",@progbits,_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED5Ev,comdat
	.align 4
	.weak	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED2Ev
	.type	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED2Ev, #function
	.proc	020
_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED2Ev:
.LFB4701:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	ldx	[%fp+2175], %o0
	call	_ZNSaI6sphereED2Ev, 0
	 nop
	nop
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4701:
	.size	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED2Ev, .-_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED2Ev
	.weak	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED1Ev
	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED1Ev = _ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED2Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD2Ev,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD5Ev,comdat
	.align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD2Ev
	.type	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD2Ev, #function
	.proc	020
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD2Ev:
.LFB4703:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	ldx	[%fp+2175], %o0
	call	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED2Ev, 0
	 nop
	nop
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4703:
	.size	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD2Ev, .-_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD2Ev
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD1Ev
	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD1Ev = _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD2Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3dEEES1_DpOT_,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC5IJ4vec3dEEES1_DpOT_,comdat
	.align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3dEEES1_DpOT_
	.type	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3dEEES1_DpOT_, #function
	.proc	020
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3dEEES1_DpOT_:
.LFB4705:
	.cfi_startproc
	.cfi_personality 0,__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA4705
	save	%sp, -224, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2023]
	stx	%i1, [%fp+2015]
	stx	%i2, [%fp+2007]
	stx	%i3, [%fp+1999]
	ldx	[%g7+40], %g1
	stx	%g1, [%fp+2039]
	mov	0, %g1
	ldx	[%fp+2023], %g1
	mov	%g1, %o0
	call	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC2Ev, 0
	 nop
	sethi	%hi(_ZTVSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE+16), %g1
	or	%g1, %lo(_ZTVSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE+16), %g2
	ldx	[%fp+2023], %g1
	stx	%g2, [%g1]
	ldx	[%fp+2023], %g1
	add	%g1, 24, %i5
	add	%fp, 2038, %g1
	ldx	[%fp+2015], %o1
	mov	%g1, %o0
	call	_ZNSaI6sphereEC1ERKS0_, 0
	 nop
	add	%fp, 2038, %g1
	mov	%g1, %o1
	mov	%i5, %o0
	call	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC1ES1_, 0
	 nop
	add	%fp, 2038, %g1
	mov	%g1, %o0
	call	_ZNSaI6sphereED1Ev, 0
	 nop
	ldx	[%fp+2023], %o0
	call	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv, 0
	 nop
	mov	%o0, %i5
	ldx	[%fp+2007], %o0
	call	_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE, 0
	 nop
	mov	%o0, %i4
	ldx	[%fp+1999], %o0
	call	_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE, 0
	 nop
	mov	%o0, %g1
	mov	%g1, %o3
	mov	%i4, %o2
	mov	%i5, %o1
	ldx	[%fp+2015], %o0
.LEHB26:
	call	_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3dEEEvRS1_PT_DpOT0_, 0
	 nop
.LEHE26:
	ba,pt	%xcc, .L239
	 nop
.L238:
	mov	%i0, %i5
	ldx	[%fp+2023], %g1
	add	%g1, 24, %g1
	mov	%g1, %o0
	call	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD1Ev, 0
	 nop
	ldx	[%fp+2023], %g1
	mov	%g1, %o0
	call	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev, 0
	 nop
	mov	%i5, %g1
	mov	%g1, %o0
.LEHB27:
	call	_Unwind_Resume, 0
	 nop
.LEHE27:
.L239:
	ldx	[%fp+2039], %g1
	ldx	[%g7+40], %g2
	xor	%g1, %g2, %g1
	mov	0, %g2
	brz	%g1, .L237
	 nop
	call	__stack_chk_fail, 0
	 nop
.L237:
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4705:
	.section	.gcc_except_table
.LLSDA4705:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4705-.LLSDACSB4705
.LLSDACSB4705:
	.uleb128 .LEHB26-.LFB4705
	.uleb128 .LEHE26-.LEHB26
	.uleb128 .L238-.LFB4705
	.uleb128 0
	.uleb128 .LEHB27-.LFB4705
	.uleb128 .LEHE27-.LEHB27
	.uleb128 0
	.uleb128 0
.LLSDACSE4705:
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3dEEES1_DpOT_,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC5IJ4vec3dEEES1_DpOT_,comdat
	.size	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3dEEES1_DpOT_, .-_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3dEEES1_DpOT_
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC1IJ4vec3dEEES1_DpOT_
	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC1IJ4vec3dEEES1_DpOT_ = _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3dEEES1_DpOT_
	.section	.text._ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEaSEDn,"axG",@progbits,_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEaSEDn,comdat
	.align 4
	.weak	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEaSEDn
	.type	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEaSEDn, #function
	.proc	0110
_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEaSEDn:
.LFB4707:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	stx	%i1, [%fp+2183]
	ldx	[%fp+2175], %g1
	stx	%g0, [%g1+8]
	ldx	[%fp+2175], %g1
	mov	%g1, %i0
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4707:
	.size	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEaSEDn, .-_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEaSEDn
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv,comdat
	.align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv
	.type	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv, #function
	.proc	0110
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv:
.LFB4708:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	ldx	[%fp+2175], %g1
	add	%g1, 24, %g1
	mov	%g1, %o0
	call	_ZN9__gnu_cxx16__aligned_bufferI6sphereE6_M_ptrEv, 0
	 nop
	mov	%o0, %g1
	mov	%g1, %i0
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4708:
	.size	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv, .-_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3iEEES1_DpOT_,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC5IJ4vec3iEEES1_DpOT_,comdat
	.align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3iEEES1_DpOT_
	.type	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3iEEES1_DpOT_, #function
	.proc	020
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3iEEES1_DpOT_:
.LFB4710:
	.cfi_startproc
	.cfi_personality 0,__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA4710
	save	%sp, -224, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2023]
	stx	%i1, [%fp+2015]
	stx	%i2, [%fp+2007]
	stx	%i3, [%fp+1999]
	ldx	[%g7+40], %g1
	stx	%g1, [%fp+2039]
	mov	0, %g1
	ldx	[%fp+2023], %g1
	mov	%g1, %o0
	call	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC2Ev, 0
	 nop
	sethi	%hi(_ZTVSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE+16), %g1
	or	%g1, %lo(_ZTVSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE+16), %g2
	ldx	[%fp+2023], %g1
	stx	%g2, [%g1]
	ldx	[%fp+2023], %g1
	add	%g1, 24, %i5
	add	%fp, 2038, %g1
	ldx	[%fp+2015], %o1
	mov	%g1, %o0
	call	_ZNSaI6sphereEC1ERKS0_, 0
	 nop
	add	%fp, 2038, %g1
	mov	%g1, %o1
	mov	%i5, %o0
	call	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC1ES1_, 0
	 nop
	add	%fp, 2038, %g1
	mov	%g1, %o0
	call	_ZNSaI6sphereED1Ev, 0
	 nop
	ldx	[%fp+2023], %o0
	call	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv, 0
	 nop
	mov	%o0, %i5
	ldx	[%fp+2007], %o0
	call	_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE, 0
	 nop
	mov	%o0, %i4
	ldx	[%fp+1999], %o0
	call	_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE, 0
	 nop
	mov	%o0, %g1
	mov	%g1, %o3
	mov	%i4, %o2
	mov	%i5, %o1
	ldx	[%fp+2015], %o0
.LEHB28:
	call	_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3iEEEvRS1_PT_DpOT0_, 0
	 nop
.LEHE28:
	ba,pt	%xcc, .L248
	 nop
.L247:
	mov	%i0, %i5
	ldx	[%fp+2023], %g1
	add	%g1, 24, %g1
	mov	%g1, %o0
	call	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD1Ev, 0
	 nop
	ldx	[%fp+2023], %g1
	mov	%g1, %o0
	call	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev, 0
	 nop
	mov	%i5, %g1
	mov	%g1, %o0
.LEHB29:
	call	_Unwind_Resume, 0
	 nop
.LEHE29:
.L248:
	ldx	[%fp+2039], %g1
	ldx	[%g7+40], %g2
	xor	%g1, %g2, %g1
	mov	0, %g2
	brz	%g1, .L246
	 nop
	call	__stack_chk_fail, 0
	 nop
.L246:
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4710:
	.section	.gcc_except_table
.LLSDA4710:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4710-.LLSDACSB4710
.LLSDACSB4710:
	.uleb128 .LEHB28-.LFB4710
	.uleb128 .LEHE28-.LEHB28
	.uleb128 .L247-.LFB4710
	.uleb128 0
	.uleb128 .LEHB29-.LFB4710
	.uleb128 .LEHE29-.LEHB29
	.uleb128 0
	.uleb128 0
.LLSDACSE4710:
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3iEEES1_DpOT_,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC5IJ4vec3iEEES1_DpOT_,comdat
	.size	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3iEEES1_DpOT_, .-_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3iEEES1_DpOT_
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC1IJ4vec3iEEES1_DpOT_
	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC1IJ4vec3iEEES1_DpOT_ = _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3iEEES1_DpOT_
	.section	.text._ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC2Ev,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC5Ev,comdat
	.align 4
	.weak	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC2Ev
	.type	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC2Ev, #function
	.proc	020
_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC2Ev:
.LFB4723:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	nop
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4723:
	.size	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC2Ev, .-_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC2Ev
	.weak	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC1Ev
	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC1Ev = _ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC2Ev
	.section	.text._ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED2Ev,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED5Ev,comdat
	.align 4
	.weak	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED2Ev
	.type	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED2Ev, #function
	.proc	020
_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED2Ev:
.LFB4726:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	nop
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4726:
	.size	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED2Ev, .-_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED2Ev
	.weak	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED1Ev
	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED1Ev = _ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED2Ev
	.section	.text._ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE8allocateERS6_m,"axG",@progbits,_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE8allocateERS6_m,comdat
	.align 4
	.weak	_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE8allocateERS6_m
	.type	_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE8allocateERS6_m, #function
	.proc	0110
_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE8allocateERS6_m:
.LFB4728:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	stx	%i1, [%fp+2183]
	mov	0, %o2
	ldx	[%fp+2183], %o1
	ldx	[%fp+2175], %o0
	call	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8allocateEmPKv, 0
	 nop
	mov	%o0, %g1
	mov	%g1, %i0
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4728:
	.size	_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE8allocateERS6_m, .-_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE8allocateERS6_m
	.section	.text._ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC2ERS6_PS5_,"axG",@progbits,_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC5ERS6_PS5_,comdat
	.align 4
	.weak	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC2ERS6_PS5_
	.type	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC2ERS6_PS5_, #function
	.proc	020
_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC2ERS6_PS5_:
.LFB4730:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	stx	%i1, [%fp+2183]
	stx	%i2, [%fp+2191]
	ldx	[%fp+2183], %o0
	call	_ZSt11__addressofISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEPT_RS7_, 0
	 nop
	mov	%o0, %g2
	ldx	[%fp+2175], %g1
	stx	%g2, [%g1]
	ldx	[%fp+2175], %g1
	ldx	[%fp+2191], %g2
	stx	%g2, [%g1+8]
	nop
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4730:
	.size	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC2ERS6_PS5_, .-_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC2ERS6_PS5_
	.weak	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC1ERS6_PS5_
	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC1ERS6_PS5_ = _ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC2ERS6_PS5_
	.section	.text._ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE10deallocateERS6_PS5_m,"axG",@progbits,_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE10deallocateERS6_PS5_m,comdat
	.align 4
	.weak	_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE10deallocateERS6_PS5_m
	.type	_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE10deallocateERS6_PS5_m, #function
	.proc	020
_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE10deallocateERS6_PS5_m:
.LFB4732:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	stx	%i1, [%fp+2183]
	stx	%i2, [%fp+2191]
	ldx	[%fp+2191], %o2
	ldx	[%fp+2183], %o1
	ldx	[%fp+2175], %o0
	call	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE10deallocateEPS5_m, 0
	 nop
	nop
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4732:
	.size	_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE10deallocateERS6_PS5_m, .-_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE10deallocateERS6_PS5_m
	.section	.text._ZSt12__to_addressISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEPT_S7_,"axG",@progbits,_ZSt12__to_addressISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEPT_S7_,comdat
	.align 4
	.weak	_ZSt12__to_addressISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEPT_S7_
	.type	_ZSt12__to_addressISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEPT_S7_, #function
	.proc	0110
_ZSt12__to_addressISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEPT_S7_:
.LFB4733:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	ldx	[%fp+2175], %g1
	mov	%g1, %i0
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4733:
	.size	_ZSt12__to_addressISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEPT_S7_, .-_ZSt12__to_addressISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEPT_S7_
	.section	.text._ZN9__gnu_cxx13new_allocatorI6sphereEC2ERKS2_,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorI6sphereEC5ERKS2_,comdat
	.align 4
	.weak	_ZN9__gnu_cxx13new_allocatorI6sphereEC2ERKS2_
	.type	_ZN9__gnu_cxx13new_allocatorI6sphereEC2ERKS2_, #function
	.proc	020
_ZN9__gnu_cxx13new_allocatorI6sphereEC2ERKS2_:
.LFB4735:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	stx	%i1, [%fp+2183]
	nop
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4735:
	.size	_ZN9__gnu_cxx13new_allocatorI6sphereEC2ERKS2_, .-_ZN9__gnu_cxx13new_allocatorI6sphereEC2ERKS2_
	.weak	_ZN9__gnu_cxx13new_allocatorI6sphereEC1ERKS2_
	_ZN9__gnu_cxx13new_allocatorI6sphereEC1ERKS2_ = _ZN9__gnu_cxx13new_allocatorI6sphereEC2ERKS2_
	.section	.text._ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC2Ev,"axG",@progbits,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC5Ev,comdat
	.align 4
	.weak	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC2Ev
	.type	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC2Ev, #function
	.proc	020
_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC2Ev:
.LFB4738:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	sethi	%hi(_ZTVSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE+16), %g1
	or	%g1, %lo(_ZTVSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE+16), %g2
	ldx	[%fp+2175], %g1
	stx	%g2, [%g1]
	ldx	[%fp+2175], %g1
	mov	1, %g2
	stx	%g2, [%g1+8]
	ldx	[%fp+2175], %g1
	mov	1, %g2
	stx	%g2, [%g1+16]
	nop
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4738:
	.size	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC2Ev, .-_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC2Ev
	.weak	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC1Ev
	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC1Ev = _ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC2Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC2ES1_,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC5ES1_,comdat
	.align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC2ES1_
	.type	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC2ES1_, #function
	.proc	020
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC2ES1_:
.LFB4741:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	stx	%i1, [%fp+2183]
	ldx	[%fp+2183], %o1
	ldx	[%fp+2175], %o0
	call	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC2ERKS1_, 0
	 nop
	nop
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4741:
	.size	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC2ES1_, .-_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC2ES1_
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC1ES1_
	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC1ES1_ = _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC2ES1_
	.section	.text._ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3dEEEvRS1_PT_DpOT0_,"axG",@progbits,_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3dEEEvRS1_PT_DpOT0_,comdat
	.align 4
	.weak	_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3dEEEvRS1_PT_DpOT0_
	.type	_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3dEEEvRS1_PT_DpOT0_, #function
	.proc	020
_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3dEEEvRS1_PT_DpOT0_:
.LFB4743:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	stx	%i1, [%fp+2183]
	stx	%i2, [%fp+2191]
	stx	%i3, [%fp+2199]
	ldx	[%fp+2191], %o0
	call	_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE, 0
	 nop
	mov	%o0, %i5
	ldx	[%fp+2199], %o0
	call	_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE, 0
	 nop
	mov	%o0, %g1
	mov	%g1, %o3
	mov	%i5, %o2
	ldx	[%fp+2183], %o1
	ldx	[%fp+2175], %o0
	call	_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_, 0
	 nop
	nop
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4743:
	.size	_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3dEEEvRS1_PT_DpOT0_, .-_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3dEEEvRS1_PT_DpOT0_
	.section	.text._ZN9__gnu_cxx16__aligned_bufferI6sphereE6_M_ptrEv,"axG",@progbits,_ZN9__gnu_cxx16__aligned_bufferI6sphereE6_M_ptrEv,comdat
	.align 4
	.weak	_ZN9__gnu_cxx16__aligned_bufferI6sphereE6_M_ptrEv
	.type	_ZN9__gnu_cxx16__aligned_bufferI6sphereE6_M_ptrEv, #function
	.proc	0110
_ZN9__gnu_cxx16__aligned_bufferI6sphereE6_M_ptrEv:
.LFB4744:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	ldx	[%fp+2175], %o0
	call	_ZN9__gnu_cxx16__aligned_bufferI6sphereE7_M_addrEv, 0
	 nop
	mov	%o0, %g1
	mov	%g1, %i0
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4744:
	.size	_ZN9__gnu_cxx16__aligned_bufferI6sphereE6_M_ptrEv, .-_ZN9__gnu_cxx16__aligned_bufferI6sphereE6_M_ptrEv
	.section	.text._ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3iEEEvRS1_PT_DpOT0_,"axG",@progbits,_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3iEEEvRS1_PT_DpOT0_,comdat
	.align 4
	.weak	_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3iEEEvRS1_PT_DpOT0_
	.type	_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3iEEEvRS1_PT_DpOT0_, #function
	.proc	020
_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3iEEEvRS1_PT_DpOT0_:
.LFB4745:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	stx	%i1, [%fp+2183]
	stx	%i2, [%fp+2191]
	stx	%i3, [%fp+2199]
	ldx	[%fp+2191], %o0
	call	_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE, 0
	 nop
	mov	%o0, %i5
	ldx	[%fp+2199], %o0
	call	_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE, 0
	 nop
	mov	%o0, %g1
	mov	%g1, %o3
	mov	%i5, %o2
	ldx	[%fp+2183], %o1
	ldx	[%fp+2175], %o0
	call	_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_, 0
	 nop
	nop
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4745:
	.size	_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3iEEEvRS1_PT_DpOT0_, .-_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3iEEEvRS1_PT_DpOT0_
	.section	.text._ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8allocateEmPKv,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8allocateEmPKv,comdat
	.align 4
	.weak	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8allocateEmPKv
	.type	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8allocateEmPKv, #function
	.proc	0110
_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8allocateEmPKv:
.LFB4748:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	stx	%i1, [%fp+2183]
	stx	%i2, [%fp+2191]
	ldx	[%fp+2175], %o0
	call	_ZNK9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8max_sizeEv, 0
	 nop
	mov	%o0, %g2
	ldx	[%fp+2183], %g1
	cmp	%g1, %g2
	mov	0, %g1
	movgu	%xcc, 1, %g1
	and	%g1, 0xff, %g1
	cmp	%g1, 0
	be	%icc, .L265
	 nop
	call	_ZSt17__throw_bad_allocv, 0
	 nop
.L265:
	ldx	[%fp+2183], %g1
	mulx	%g1, 80, %g1
	mov	%g1, %o0
	call	_Znwm, 0
	 nop
	mov	%o0, %g1
	mov	%g1, %i0
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4748:
	.size	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8allocateEmPKv, .-_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8allocateEmPKv
	.section	.text._ZSt11__addressofISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEPT_RS7_,"axG",@progbits,_ZSt11__addressofISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEPT_RS7_,comdat
	.align 4
	.weak	_ZSt11__addressofISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEPT_RS7_
	.type	_ZSt11__addressofISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEPT_RS7_, #function
	.proc	0110
_ZSt11__addressofISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEPT_RS7_:
.LFB4749:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	ldx	[%fp+2175], %g1
	mov	%g1, %i0
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4749:
	.size	_ZSt11__addressofISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEPT_RS7_, .-_ZSt11__addressofISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEPT_RS7_
	.section	.text._ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE10deallocateEPS5_m,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE10deallocateEPS5_m,comdat
	.align 4
	.weak	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE10deallocateEPS5_m
	.type	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE10deallocateEPS5_m, #function
	.proc	020
_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE10deallocateEPS5_m:
.LFB4750:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	stx	%i1, [%fp+2183]
	stx	%i2, [%fp+2191]
	ldx	[%fp+2183], %o0
	call	_ZdlPv, 0
	 nop
	nop
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4750:
	.size	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE10deallocateEPS5_m, .-_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE10deallocateEPS5_m
	.section	.text._ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC2ERKS1_,"axG",@progbits,_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC5ERKS1_,comdat
	.align 4
	.weak	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC2ERKS1_
	.type	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC2ERKS1_, #function
	.proc	020
_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC2ERKS1_:
.LFB4752:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	stx	%i1, [%fp+2183]
	ldx	[%fp+2183], %o1
	ldx	[%fp+2175], %o0
	call	_ZNSaI6sphereEC2ERKS0_, 0
	 nop
	nop
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4752:
	.size	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC2ERKS1_, .-_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC2ERKS1_
	.weak	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC1ERKS1_
	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC1ERKS1_ = _ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC2ERKS1_
	.section	.text._ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_,comdat
	.align 4
	.weak	_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_
	.type	_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_, #function
	.proc	020
_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_:
.LFB4754:
	.cfi_startproc
	.cfi_personality 0,__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA4754
	save	%sp, -288, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2007]
	stx	%i1, [%fp+1999]
	stx	%i2, [%fp+1991]
	stx	%i3, [%fp+1983]
	ldx	[%g7+40], %g1
	stx	%g1, [%fp+2039]
	mov	0, %g1
	ldx	[%fp+1991], %o0
	call	_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE, 0
	 nop
	mov	%o0, %g1
	ldx	[%g1], %g2
	stx	%g2, [%fp+2015]
	ldx	[%g1+8], %g2
	stx	%g2, [%fp+2023]
	ldx	[%g1+16], %g1
	stx	%g1, [%fp+2031]
	ldx	[%fp+1983], %o0
	call	_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE, 0
	 nop
	mov	%o0, %g1
	ldd	[%g1], %f8
	std	%f8, [%fp+1943]
	ldx	[%fp+1999], %i5
	mov	%i5, %o1
	mov	56, %o0
	call	_ZnwmPv, 0
	 nop
	mov	%o0, %i4
	ldx	[%fp+2015], %g1
	stx	%g1, [%fp+1951]
	ldx	[%fp+2023], %g1
	stx	%g1, [%fp+1959]
	ldx	[%fp+2031], %g1
	stx	%g1, [%fp+1967]
	add	%fp, 1951, %g1
	ldd	[%fp+1943], %f4
	mov	%g1, %o1
	mov	%i4, %o0
.LEHB30:
	call	_ZN6sphereC1E4vec3d, 0
	 nop
.LEHE30:
	ba,pt	%xcc, .L275
	 nop
.L274:
	mov	%i0, %i3
	mov	%i5, %o1
	mov	%i4, %o0
	call	_ZdlPvS_, 0
	 nop
	mov	%i3, %g1
	mov	%g1, %o0
.LEHB31:
	call	_Unwind_Resume, 0
	 nop
.LEHE31:
.L275:
	ldx	[%fp+2039], %g1
	ldx	[%g7+40], %g2
	xor	%g1, %g2, %g1
	mov	0, %g2
	brz	%g1, .L273
	 nop
	call	__stack_chk_fail, 0
	 nop
.L273:
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4754:
	.section	.gcc_except_table
.LLSDA4754:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4754-.LLSDACSB4754
.LLSDACSB4754:
	.uleb128 .LEHB30-.LFB4754
	.uleb128 .LEHE30-.LEHB30
	.uleb128 .L274-.LFB4754
	.uleb128 0
	.uleb128 .LEHB31-.LFB4754
	.uleb128 .LEHE31-.LEHB31
	.uleb128 0
	.uleb128 0
.LLSDACSE4754:
	.section	.text._ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_,comdat
	.size	_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_, .-_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_
	.section	.text._ZN9__gnu_cxx16__aligned_bufferI6sphereE7_M_addrEv,"axG",@progbits,_ZN9__gnu_cxx16__aligned_bufferI6sphereE7_M_addrEv,comdat
	.align 4
	.weak	_ZN9__gnu_cxx16__aligned_bufferI6sphereE7_M_addrEv
	.type	_ZN9__gnu_cxx16__aligned_bufferI6sphereE7_M_addrEv, #function
	.proc	0120
_ZN9__gnu_cxx16__aligned_bufferI6sphereE7_M_addrEv:
.LFB4755:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	ldx	[%fp+2175], %g1
	mov	%g1, %i0
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4755:
	.size	_ZN9__gnu_cxx16__aligned_bufferI6sphereE7_M_addrEv, .-_ZN9__gnu_cxx16__aligned_bufferI6sphereE7_M_addrEv
	.section	.text._ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_,comdat
	.align 4
	.weak	_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_
	.type	_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_, #function
	.proc	020
_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_:
.LFB4756:
	.cfi_startproc
	.cfi_personality 0,__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA4756
	save	%sp, -288, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2007]
	stx	%i1, [%fp+1999]
	stx	%i2, [%fp+1991]
	stx	%i3, [%fp+1983]
	ldx	[%g7+40], %g1
	stx	%g1, [%fp+2039]
	mov	0, %g1
	ldx	[%fp+1991], %o0
	call	_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE, 0
	 nop
	mov	%o0, %g1
	ldx	[%g1], %g2
	stx	%g2, [%fp+2015]
	ldx	[%g1+8], %g2
	stx	%g2, [%fp+2023]
	ldx	[%g1+16], %g1
	stx	%g1, [%fp+2031]
	ldx	[%fp+1983], %o0
	call	_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE, 0
	 nop
	mov	%o0, %g1
	ld	[%g1], %g1
	st	%g1, [%fp+1943]
	ld	[%fp+1943], %f8
	fitod	%f8, %f8
	std	%f8, [%fp+1943]
	ldx	[%fp+1999], %i5
	mov	%i5, %o1
	mov	56, %o0
	call	_ZnwmPv, 0
	 nop
	mov	%o0, %i4
	ldx	[%fp+2015], %g1
	stx	%g1, [%fp+1951]
	ldx	[%fp+2023], %g1
	stx	%g1, [%fp+1959]
	ldx	[%fp+2031], %g1
	stx	%g1, [%fp+1967]
	add	%fp, 1951, %g1
	ldd	[%fp+1943], %f4
	mov	%g1, %o1
	mov	%i4, %o0
.LEHB32:
	call	_ZN6sphereC1E4vec3d, 0
	 nop
.LEHE32:
	ba,pt	%xcc, .L282
	 nop
.L281:
	mov	%i0, %i3
	mov	%i5, %o1
	mov	%i4, %o0
	call	_ZdlPvS_, 0
	 nop
	mov	%i3, %g1
	mov	%g1, %o0
.LEHB33:
	call	_Unwind_Resume, 0
	 nop
.LEHE33:
.L282:
	ldx	[%fp+2039], %g1
	ldx	[%g7+40], %g2
	xor	%g1, %g2, %g1
	mov	0, %g2
	brz	%g1, .L280
	 nop
	call	__stack_chk_fail, 0
	 nop
.L280:
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4756:
	.section	.gcc_except_table
.LLSDA4756:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4756-.LLSDACSB4756
.LLSDACSB4756:
	.uleb128 .LEHB32-.LFB4756
	.uleb128 .LEHE32-.LEHB32
	.uleb128 .L281-.LFB4756
	.uleb128 0
	.uleb128 .LEHB33-.LFB4756
	.uleb128 .LEHE33-.LEHB33
	.uleb128 0
	.uleb128 0
.LLSDACSE4756:
	.section	.text._ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_,comdat
	.size	_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_, .-_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_
	.section	.text._ZNK9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8max_sizeEv,"axG",@progbits,_ZNK9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8max_sizeEv,comdat
	.align 4
	.weak	_ZNK9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8max_sizeEv
	.type	_ZNK9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8max_sizeEv, #function
	.proc	017
_ZNK9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8max_sizeEv:
.LFB4757:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	sethi	%hi(26843136), %g1
	or	%g1, 409, %g1
	sllx	%g1, 32, %g2
	sethi	%hi(2576979968), %g1
	or	%g1, 409, %g1
	add	%g2, %g1, %g1
	mov	%g1, %i0
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4757:
	.size	_ZNK9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8max_sizeEv, .-_ZNK9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8max_sizeEv
	.weak	_ZTVSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE
	.section	.rodata._ZTVSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE,"aG",@progbits,_ZTVSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align 8
	.type	_ZTVSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE, #object
	.size	_ZTVSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE, 56
_ZTVSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.xword	_ZTISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE
	.xword	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.xword	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.xword	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.xword	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.xword	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.weak	_ZTVSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE
	.section	.rodata._ZTVSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE,"aG",@progbits,_ZTVSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align 8
	.type	_ZTVSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE, #object
	.size	_ZTVSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE, 56
_ZTVSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.xword	_ZTISt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.xword	__cxa_pure_virtual
	.xword	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.xword	__cxa_pure_virtual
	.weak	_ZTISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE
	.section	.rodata._ZTISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE,"aG",@progbits,_ZTISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align 8
	.type	_ZTISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE, #object
	.size	_ZTISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE, 24
_ZTISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE:
	.xword	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.xword	_ZTSSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE
	.xword	_ZTISt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE
	.weak	_ZTSSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE
	.section	.rodata._ZTSSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE,"aG",@progbits,_ZTSSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align 8
	.type	_ZTSSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE, #object
	.size	_ZTSSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE, 73
_ZTSSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE:
	.asciz	"St23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE"
	.weak	_ZTISt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE
	.section	.rodata._ZTISt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE,"aG",@progbits,_ZTISt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align 8
	.type	_ZTISt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE, #object
	.size	_ZTISt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE, 24
_ZTISt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE:
	.xword	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.xword	_ZTSSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE
	.xword	_ZTISt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE
	.weak	_ZTSSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE
	.section	.rodata._ZTSSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE,"aG",@progbits,_ZTSSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align 8
	.type	_ZTSSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE, #object
	.size	_ZTSSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE, 52
_ZTSSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE:
	.asciz	"St16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE"
	.section	".text"
	.align 4
	.type	_Z41__static_initialization_and_destruction_0ii, #function
	.proc	020
_Z41__static_initialization_and_destruction_0ii:
.LFB4774:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	mov	%i0, %g1
	mov	%i1, %g2
	st	%g1, [%fp+2175]
	mov	%g2, %g1
	st	%g1, [%fp+2183]
	ld	[%fp+2175], %g1
	cmp	%g1, 1
	bne	%icc, .L287
	 nop
	ld	[%fp+2183], %g2
	sethi	%hi(64512), %g1
	or	%g1, 1023, %g1
	cmp	%g2, %g1
	bne	%icc, .L287
	 nop
	sethi	%hi(_ZStL8__ioinit), %g1
	or	%g1, %lo(_ZStL8__ioinit), %o0
	call	_ZNSt8ios_base4InitC1Ev, 0
	 nop
	sethi	%hi(__dso_handle), %g1
	or	%g1, %lo(__dso_handle), %o2
	sethi	%hi(_ZStL8__ioinit), %g1
	or	%g1, %lo(_ZStL8__ioinit), %o1
	sethi	%hi(_ZNSt8ios_base4InitD1Ev), %g1
	or	%g1, %lo(_ZNSt8ios_base4InitD1Ev), %o0
	call	__cxa_atexit, 0
	 nop
.L287:
	nop
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4774:
	.size	_Z41__static_initialization_and_destruction_0ii, .-_Z41__static_initialization_and_destruction_0ii
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.type	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED2Ev, #function
	.proc	020
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED2Ev:
.LFB4776:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	sethi	%hi(_ZTVSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE+16), %g1
	or	%g1, %lo(_ZTVSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE+16), %g2
	ldx	[%fp+2175], %g1
	stx	%g2, [%g1]
	ldx	[%fp+2175], %g1
	add	%g1, 24, %g1
	mov	%g1, %o0
	call	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD1Ev, 0
	 nop
	ldx	[%fp+2175], %g1
	mov	%g1, %o0
	call	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev, 0
	 nop
	nop
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4776:
	.size	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED1Ev
	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED1Ev = _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED0Ev,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.type	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED0Ev, #function
	.proc	020
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED0Ev:
.LFB4778:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	ldx	[%fp+2175], %o0
	call	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED1Ev, 0
	 nop
	mov	80, %o1
	ldx	[%fp+2175], %o0
	call	_ZdlPvm, 0
	 nop
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4778:
	.size	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED0Ev, .-_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,comdat
	.align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.type	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, #function
	.proc	020
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv:
.LFB4779:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	ldx	[%fp+2175], %g1
	add	%g1, 24, %g1
	mov	%g1, %o0
	call	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_Impl8_M_allocEv, 0
	 nop
	mov	%o0, %i5
	ldx	[%fp+2175], %o0
	call	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv, 0
	 nop
	mov	%o0, %g1
	mov	%g1, %o1
	mov	%i5, %o0
	call	_ZNSt16allocator_traitsISaI6sphereEE7destroyIS0_EEvRS1_PT_, 0
	 nop
	nop
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4779:
	.size	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, .-_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,comdat
	.align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.type	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, #function
	.proc	020
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv:
.LFB4780:
	.cfi_startproc
	save	%sp, -224, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2007]
	ldx	[%g7+40], %g1
	stx	%g1, [%fp+2039]
	mov	0, %g1
	ldx	[%fp+2007], %g1
	add	%g1, 24, %g1
	mov	%g1, %o0
	call	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_Impl8_M_allocEv, 0
	 nop
	mov	%o0, %g2
	add	%fp, 2022, %g1
	mov	%g2, %o1
	mov	%g1, %o0
	call	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC1IS0_EERKSaIT_E, 0
	 nop
	add	%fp, 2022, %g2
	add	%fp, 2023, %g1
	ldx	[%fp+2007], %o2
	mov	%g2, %o1
	mov	%g1, %o0
	call	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC1ERS6_PS5_, 0
	 nop
	ldx	[%fp+2007], %o0
	call	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED1Ev, 0
	 nop
	add	%fp, 2023, %g1
	mov	%g1, %o0
	call	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED1Ev, 0
	 nop
	add	%fp, 2022, %g1
	mov	%g1, %o0
	call	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED1Ev, 0
	 nop
	nop
	ldx	[%fp+2039], %g1
	ldx	[%g7+40], %g2
	xor	%g1, %g2, %g1
	mov	0, %g2
	brz	%g1, .L292
	 nop
	call	__stack_chk_fail, 0
	 nop
.L292:
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4780:
	.size	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, .-_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,comdat
	.align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.type	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, #function
	.proc	0120
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info:
.LFB4781:
	.cfi_startproc
	save	%sp, -192, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	stx	%i1, [%fp+2183]
	ldx	[%fp+2175], %o0
	call	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv, 0
	 nop
	stx	%o0, [%fp+2039]
	call	_ZNSt19_Sp_make_shared_tag5_S_tiEv, 0
	 nop
	mov	%o0, %g2
	ldx	[%fp+2183], %g1
	cmp	%g1, %g2
	be	%xcc, .L294
	 nop
	sethi	%hi(_ZTISt19_Sp_make_shared_tag), %g1
	or	%g1, %lo(_ZTISt19_Sp_make_shared_tag), %o1
	ldx	[%fp+2183], %o0
	call	_ZNKSt9type_infoeqERKS_, 0
	 nop
	mov	%o0, %g1
	and	%g1, 0xff, %g1
	cmp	%g1, 0
	be	%icc, .L295
	 nop
.L294:
	mov	1, %g1
	ba,pt	%xcc, .L296
	 nop
.L295:
	mov	0, %g1
.L296:
	and	%g1, 0xff, %g1
	cmp	%g1, 0
	be	%icc, .L297
	 nop
	ldx	[%fp+2039], %g1
	ba,pt	%xcc, .L298
	 nop
.L297:
	mov	0, %g1
.L298:
	mov	%g1, %i0
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4781:
	.size	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, .-_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_Impl8_M_allocEv,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_Impl8_M_allocEv,comdat
	.align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_Impl8_M_allocEv
	.type	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_Impl8_M_allocEv, #function
	.proc	0110
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_Impl8_M_allocEv:
.LFB4782:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	ldx	[%fp+2175], %o0
	call	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EE6_S_getERS2_, 0
	 nop
	mov	%o0, %g1
	mov	%g1, %i0
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4782:
	.size	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_Impl8_M_allocEv, .-_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_Impl8_M_allocEv
	.section	.text._ZNSt16allocator_traitsISaI6sphereEE7destroyIS0_EEvRS1_PT_,"axG",@progbits,_ZNSt16allocator_traitsISaI6sphereEE7destroyIS0_EEvRS1_PT_,comdat
	.align 4
	.weak	_ZNSt16allocator_traitsISaI6sphereEE7destroyIS0_EEvRS1_PT_
	.type	_ZNSt16allocator_traitsISaI6sphereEE7destroyIS0_EEvRS1_PT_, #function
	.proc	020
_ZNSt16allocator_traitsISaI6sphereEE7destroyIS0_EEvRS1_PT_:
.LFB4783:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	stx	%i1, [%fp+2183]
	ldx	[%fp+2183], %o1
	ldx	[%fp+2175], %o0
	call	_ZN9__gnu_cxx13new_allocatorI6sphereE7destroyIS1_EEvPT_, 0
	 nop
	nop
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4783:
	.size	_ZNSt16allocator_traitsISaI6sphereEE7destroyIS0_EEvRS1_PT_, .-_ZNSt16allocator_traitsISaI6sphereEE7destroyIS0_EEvRS1_PT_
	.section	.text._ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EE6_S_getERS2_,"axG",@progbits,_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EE6_S_getERS2_,comdat
	.align 4
	.weak	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EE6_S_getERS2_
	.type	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EE6_S_getERS2_, #function
	.proc	0110
_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EE6_S_getERS2_:
.LFB4784:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	ldx	[%fp+2175], %g1
	mov	%g1, %i0
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4784:
	.size	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EE6_S_getERS2_, .-_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EE6_S_getERS2_
	.section	.text._ZN6sphereD2Ev,"axG",@progbits,_ZN6sphereD5Ev,comdat
	.align 4
	.weak	_ZN6sphereD2Ev
	.type	_ZN6sphereD2Ev, #function
	.proc	020
_ZN6sphereD2Ev:
.LFB4787:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	sethi	%hi(_ZTV6sphere+16), %g1
	or	%g1, %lo(_ZTV6sphere+16), %g2
	ldx	[%fp+2175], %g1
	stx	%g2, [%g1]
	ldx	[%fp+2175], %g1
	add	%g1, 40, %g1
	mov	%g1, %o0
	call	_ZNSt10shared_ptrI8materialED1Ev, 0
	 nop
	nop
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4787:
	.size	_ZN6sphereD2Ev, .-_ZN6sphereD2Ev
	.weak	_ZN6sphereD1Ev
	_ZN6sphereD1Ev = _ZN6sphereD2Ev
	.section	.text._ZN9__gnu_cxx13new_allocatorI6sphereE7destroyIS1_EEvPT_,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorI6sphereE7destroyIS1_EEvPT_,comdat
	.align 4
	.weak	_ZN9__gnu_cxx13new_allocatorI6sphereE7destroyIS1_EEvPT_
	.type	_ZN9__gnu_cxx13new_allocatorI6sphereE7destroyIS1_EEvPT_, #function
	.proc	020
_ZN9__gnu_cxx13new_allocatorI6sphereE7destroyIS1_EEvPT_:
.LFB4785:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	stx	%i0, [%fp+2175]
	stx	%i1, [%fp+2183]
	ldx	[%fp+2183], %o0
	call	_ZN6sphereD1Ev, 0
	 nop
	nop
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4785:
	.size	_ZN9__gnu_cxx13new_allocatorI6sphereE7destroyIS1_EEvPT_, .-_ZN9__gnu_cxx13new_allocatorI6sphereE7destroyIS1_EEvPT_
	.weak	_ZTISt19_Sp_make_shared_tag
	.section	.rodata._ZTISt19_Sp_make_shared_tag,"aG",@progbits,_ZTISt19_Sp_make_shared_tag,comdat
	.align 8
	.type	_ZTISt19_Sp_make_shared_tag, #object
	.size	_ZTISt19_Sp_make_shared_tag, 16
_ZTISt19_Sp_make_shared_tag:
	.xword	_ZTVN10__cxxabiv117__class_type_infoE+16
	.xword	_ZTSSt19_Sp_make_shared_tag
	.weak	_ZTSSt19_Sp_make_shared_tag
	.section	.rodata._ZTSSt19_Sp_make_shared_tag,"aG",@progbits,_ZTSSt19_Sp_make_shared_tag,comdat
	.align 8
	.type	_ZTSSt19_Sp_make_shared_tag, #object
	.size	_ZTSSt19_Sp_make_shared_tag, 24
_ZTSSt19_Sp_make_shared_tag:
	.asciz	"St19_Sp_make_shared_tag"
	.weak	_ZTISt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE
	.section	.rodata._ZTISt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE,"aG",@progbits,_ZTISt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align 8
	.type	_ZTISt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE, #object
	.size	_ZTISt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE, 16
_ZTISt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE:
	.xword	_ZTVN10__cxxabiv117__class_type_infoE+16
	.xword	_ZTSSt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE
	.weak	_ZTSSt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE
	.section	.rodata._ZTSSt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE,"aG",@progbits,_ZTSSt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align 8
	.type	_ZTSSt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE, #object
	.size	_ZTSSt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE, 47
_ZTSSt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE:
	.asciz	"St11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE"
	.section	".text"
	.align 4
	.type	_GLOBAL__sub_I_main, #function
	.proc	020
_GLOBAL__sub_I_main:
.LFB4789:
	.cfi_startproc
	save	%sp, -176, %sp
	.cfi_window_save
	.cfi_register 15, 31
	.cfi_def_cfa_register 30
	mov	-1, %g1
	srlx	%g1, 48, %o1
	mov	1, %o0
	call	_Z41__static_initialization_and_destruction_0ii, 0
	 nop
	return	%i7+8
	 nop
	.cfi_endproc
.LFE4789:
	.size	_GLOBAL__sub_I_main, .-_GLOBAL__sub_I_main
	.section	.ctors,"aw",@progbits
	.align 8
	.xword	_GLOBAL__sub_I_main
	.weakref	_ZL28__gthrw___pthread_key_createPjPFvPvE,__pthread_key_create
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.4.0-1ubuntu1~20.04) 9.4.0"
	.section	.note.GNU-stack,"",@progbits
