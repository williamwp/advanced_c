	.file	"7-metal-reflection.cpp"
	.machinemode zarch
	.machine "zEC12"
.text
	.section	.rodata
	.align	2
	.type	_ZStL19piecewise_construct, @object
	.size	_ZStL19piecewise_construct, 1
_ZStL19piecewise_construct:
	.zero	1
	.section	.text._ZNKSt9type_infoeqERKS_,"axG",@progbits,_ZNKSt9type_infoeqERKS_,comdat
	.align	8
	.weak	_ZNKSt9type_infoeqERKS_
	.type	_ZNKSt9type_infoeqERKS_, @function
_ZNKSt9type_infoeqERKS_:
.LFB746:
	.cfi_startproc
	ldgr	%f2,%r11
	.cfi_register 11, 17
	ldgr	%f0,%r15
	.cfi_register 15, 16
	lay	%r15,-176(%r15)
	.cfi_def_cfa_offset 336
	cg	%r0,168(%r15)
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,168(%r11)
	stg	%r3,160(%r11)
	lg	%r1,168(%r11)
	lg	%r2,8(%r1)
	lg	%r1,160(%r11)
	lg	%r1,8(%r1)
	cgr	%r2,%r1
	je	.L2
	lg	%r1,168(%r11)
	lg	%r1,8(%r1)
	ic	%r1,0(%r1)
	llcr	%r1,%r1
	chi	%r1,42
	je	.L3
	lg	%r1,168(%r11)
	lg	%r2,8(%r1)
	lg	%r1,160(%r11)
	lg	%r1,8(%r1)
	lhi	%r0,0
	clst	%r1,%r2
	jo	.-4
	ipm	%r1
	sll	%r1,2
	sra	%r1,30
	ltr	%r1,%r1
	jne	.L3
.L2:
	lhi	%r1,1
	j	.L4
.L3:
	lhi	%r1,0
.L4:
	llgcr	%r1,%r1
	lgr	%r2,%r1
	lgdr	%r15,%f0
	.cfi_restore 15
	.cfi_def_cfa 15, 160
	lgdr	%r11,%f2
	.cfi_restore 11
	br	%r14
	.cfi_endproc
.LFE746:
	.size	_ZNKSt9type_infoeqERKS_, .-_ZNKSt9type_infoeqERKS_
	.section	.text._ZnwmPv,"axG",@progbits,_ZnwmPv,comdat
	.align	8
	.weak	_ZnwmPv
	.type	_ZnwmPv, @function
_ZnwmPv:
.LFB788:
	.cfi_startproc
	ldgr	%f2,%r11
	.cfi_register 11, 17
	ldgr	%f0,%r15
	.cfi_register 15, 16
	lay	%r15,-176(%r15)
	.cfi_def_cfa_offset 336
	cg	%r0,168(%r15)
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,168(%r11)
	stg	%r3,160(%r11)
	lg	%r1,160(%r11)
	lgr	%r2,%r1
	lgdr	%r15,%f0
	.cfi_restore 15
	.cfi_def_cfa 15, 160
	lgdr	%r11,%f2
	.cfi_restore 11
	br	%r14
	.cfi_endproc
.LFE788:
	.size	_ZnwmPv, .-_ZnwmPv
	.section	.text._ZdlPvS_,"axG",@progbits,_ZdlPvS_,comdat
	.align	8
	.weak	_ZdlPvS_
	.type	_ZdlPvS_, @function
_ZdlPvS_:
.LFB790:
	.cfi_startproc
	ldgr	%f2,%r11
	.cfi_register 11, 17
	ldgr	%f0,%r15
	.cfi_register 15, 16
	lay	%r15,-176(%r15)
	.cfi_def_cfa_offset 336
	cg	%r0,168(%r15)
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,168(%r11)
	stg	%r3,160(%r11)
	nopr	%r0
	lgdr	%r15,%f0
	.cfi_restore 15
	.cfi_def_cfa 15, 160
	lgdr	%r11,%f2
	.cfi_restore 11
	br	%r14
	.cfi_endproc
.LFE790:
	.size	_ZdlPvS_, .-_ZdlPvS_
	.section	.data.rel.ro,"aw"
	.align	8
	.type	_ZZL18__gthread_active_pvE20__gthread_active_ptr, @object
	.size	_ZZL18__gthread_active_pvE20__gthread_active_ptr, 8
_ZZL18__gthread_active_pvE20__gthread_active_ptr:
	.quad	_ZL28__gthrw___pthread_key_createPjPFvPvE
	.section	.text._ZL18__gthread_active_pv,"axG",@progbits,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv,comdat
	.align	8
	.type	_ZL18__gthread_active_pv, @function
_ZL18__gthread_active_pv:
.LFB964:
	.cfi_startproc
	ldgr	%f0,%r11
	.cfi_register 11, 16
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	lhi	%r1,1
	lgrl	%r2,_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTENT
	ltgr	%r2,%r2
	jne	.L13
	lhi	%r1,0
.L13:
	llcr	%r1,%r1
	lgfr	%r1,%r1
	lgr	%r2,%r1
	lgdr	%r11,%f0
	.cfi_restore 11
	.cfi_def_cfa_register 15
	br	%r14
	.cfi_endproc
.LFE964:
	.size	_ZL18__gthread_active_pv, .-_ZL18__gthread_active_pv
	.section	.text._ZN9__gnu_cxxL18__exchange_and_addEPVii,"axG",@progbits,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv,comdat
	.align	8
	.type	_ZN9__gnu_cxxL18__exchange_and_addEPVii, @function
_ZN9__gnu_cxxL18__exchange_and_addEPVii:
.LFB993:
	.cfi_startproc
	ldgr	%f2,%r11
	.cfi_register 11, 17
	ldgr	%f0,%r15
	.cfi_register 15, 16
	lay	%r15,-176(%r15)
	.cfi_def_cfa_offset 336
	cg	%r0,168(%r15)
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,168(%r11)
	lgr	%r1,%r3
	st	%r1,164(%r11)
	l	%r2,164(%r11)
	lg	%r1,168(%r11)
	laa	%r2,%r2,0(%r1)
	lr	%r1,%r2
	lgfr	%r1,%r1
	lgr	%r2,%r1
	lgdr	%r15,%f0
	.cfi_restore 15
	.cfi_def_cfa 15, 160
	lgdr	%r11,%f2
	.cfi_restore 11
	br	%r14
	.cfi_endproc
.LFE993:
	.size	_ZN9__gnu_cxxL18__exchange_and_addEPVii, .-_ZN9__gnu_cxxL18__exchange_and_addEPVii
	.section	.text._ZN9__gnu_cxxL25__exchange_and_add_singleEPii,"axG",@progbits,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv,comdat
	.align	8
	.type	_ZN9__gnu_cxxL25__exchange_and_add_singleEPii, @function
_ZN9__gnu_cxxL25__exchange_and_add_singleEPii:
.LFB995:
	.cfi_startproc
	ldgr	%f2,%r11
	.cfi_register 11, 17
	ldgr	%f0,%r15
	.cfi_register 15, 16
	lay	%r15,-184(%r15)
	.cfi_def_cfa_offset 344
	cg	%r0,176(%r15)
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,168(%r11)
	lgr	%r1,%r3
	st	%r1,164(%r11)
	lg	%r1,168(%r11)
	l	%r1,0(%r1)
	st	%r1,180(%r11)
	lg	%r1,168(%r11)
	l	%r1,0(%r1)
	a	%r1,164(%r11)
	lr	%r2,%r1
	lg	%r1,168(%r11)
	st	%r2,0(%r1)
	l	%r1,180(%r11)
	lgfr	%r1,%r1
	lgr	%r2,%r1
	lgdr	%r15,%f0
	.cfi_restore 15
	.cfi_def_cfa 15, 160
	lgdr	%r11,%f2
	.cfi_restore 11
	br	%r14
	.cfi_endproc
.LFE995:
	.size	_ZN9__gnu_cxxL25__exchange_and_add_singleEPii, .-_ZN9__gnu_cxxL25__exchange_and_add_singleEPii
	.section	.text._ZN9__gnu_cxxL27__exchange_and_add_dispatchEPii,"axG",@progbits,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv,comdat
	.align	8
	.type	_ZN9__gnu_cxxL27__exchange_and_add_dispatchEPii, @function
_ZN9__gnu_cxxL27__exchange_and_add_dispatchEPii:
.LFB997:
	.cfi_startproc
	stmg	%r11,%r15,88(%r15)
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-176(%r15)
	.cfi_def_cfa_offset 336
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,168(%r11)
	lgr	%r1,%r3
	st	%r1,164(%r11)
	brasl	%r14,_ZL18__gthread_active_pv
	lgr	%r1,%r2
	lpr	%r1,%r1
	lcr	%r1,%r1
	srl	%r1,31
	llcr	%r1,%r1
	ltr	%r1,%r1
	je	.L23
	lgf	%r1,164(%r11)
	lgr	%r3,%r1
	lg	%r2,168(%r11)
	brasl	%r14,_ZN9__gnu_cxxL18__exchange_and_addEPVii
	lgr	%r1,%r2
	j	.L24
.L23:
	lgf	%r1,164(%r11)
	lgr	%r3,%r1
	lg	%r2,168(%r11)
	brasl	%r14,_ZN9__gnu_cxxL25__exchange_and_add_singleEPii
	lgr	%r1,%r2
	nopr	%r0
.L24:
	lgfr	%r1,%r1
	lgr	%r2,%r1
	lmg	%r11,%r15,264(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE997:
	.size	_ZN9__gnu_cxxL27__exchange_and_add_dispatchEPii, .-_ZN9__gnu_cxxL27__exchange_and_add_dispatchEPii
	.section	.rodata
	.align	4
	.type	_ZN9__gnu_cxxL21__default_lock_policyE, @object
	.size	_ZN9__gnu_cxxL21__default_lock_policyE, 4
_ZN9__gnu_cxxL21__default_lock_policyE:
	.long	2
	.align	2
	.type	_ZStL13allocator_arg, @object
	.size	_ZStL13allocator_arg, 1
_ZStL13allocator_arg:
	.zero	1
	.align	2
	.type	_ZStL6ignore, @object
	.size	_ZStL6ignore, 1
_ZStL6ignore:
	.zero	1
	.weak	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag
	.section	.rodata._ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag,"aG",@progbits,_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag,comdat
	.align	8
	.type	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag, @gnu_unique_object
	.size	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag, 16
_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag:
	.zero	16
	.section	.text._ZNSt19_Sp_make_shared_tag5_S_tiEv,"axG",@progbits,_ZNSt19_Sp_make_shared_tag5_S_tiEv,comdat
	.align	8
	.weak	_ZNSt19_Sp_make_shared_tag5_S_tiEv
	.type	_ZNSt19_Sp_make_shared_tag5_S_tiEv, @function
_ZNSt19_Sp_make_shared_tag5_S_tiEv:
.LFB1963:
	.cfi_startproc
	ldgr	%f0,%r11
	.cfi_register 11, 16
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	larl	%r1,_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag
	lgr	%r2,%r1
	lgdr	%r11,%f0
	.cfi_restore 11
	.cfi_def_cfa_register 15
	br	%r14
	.cfi_endproc
.LFE1963:
	.size	_ZNSt19_Sp_make_shared_tag5_S_tiEv, .-_ZNSt19_Sp_make_shared_tag5_S_tiEv
	.section	.rodata
	.align	8
	.type	_ZL2pi, @object
	.size	_ZL2pi, 8
_ZL2pi:
	.long	1074340347
	.long	1413754136
	.weak	_ZZ13random_doublevE12distribution
	.section	.bss._ZZ13random_doublevE12distribution,"awG",@nobits,_ZZ13random_doublevE12distribution,comdat
	.align	8
	.type	_ZZ13random_doublevE12distribution, @gnu_unique_object
	.size	_ZZ13random_doublevE12distribution, 16
_ZZ13random_doublevE12distribution:
	.zero	16
	.weak	_ZGVZ13random_doublevE12distribution
	.section	.bss._ZGVZ13random_doublevE12distribution,"awG",@nobits,_ZGVZ13random_doublevE12distribution,comdat
	.align	8
	.type	_ZGVZ13random_doublevE12distribution, @gnu_unique_object
	.size	_ZGVZ13random_doublevE12distribution, 8
_ZGVZ13random_doublevE12distribution:
	.zero	8
	.weak	_ZZ13random_doublevE9generator
	.section	.bss._ZZ13random_doublevE9generator,"awG",@nobits,_ZZ13random_doublevE9generator,comdat
	.align	8
	.type	_ZZ13random_doublevE9generator, @gnu_unique_object
	.size	_ZZ13random_doublevE9generator, 5000
_ZZ13random_doublevE9generator:
	.zero	5000
	.weak	_ZGVZ13random_doublevE9generator
	.section	.bss._ZGVZ13random_doublevE9generator,"awG",@nobits,_ZGVZ13random_doublevE9generator,comdat
	.align	8
	.type	_ZGVZ13random_doublevE9generator, @gnu_unique_object
	.size	_ZGVZ13random_doublevE9generator, 8
_ZGVZ13random_doublevE9generator:
	.zero	8
	.section	.text._Z13random_doublev,"axG",@progbits,_Z13random_doublev,comdat
	.align	8
	.weak	_Z13random_doublev
	.type	_Z13random_doublev, @function
_Z13random_doublev:
.LFB3309:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA3309
	stmg	%r6,%r15,48(%r15)
	.cfi_offset 6, -112
	.cfi_offset 7, -104
	.cfi_offset 8, -96
	.cfi_offset 9, -88
	.cfi_offset 10, -80
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	larl	%r13,.L40
	lay	%r15,-168(%r15)
	.cfi_def_cfa_offset 328
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	larl	%r1,_ZGVZ13random_doublevE12distribution
	ic	%r1,0(%r1)
	llgcr	%r1,%r1
	aghi	%r1,-1
	srlg	%r1,%r1,63
	llcr	%r1,%r1
	ltr	%r1,%r1
	je	.L30
	larl	%r2,_ZGVZ13random_doublevE12distribution
	brasl	%r14,__cxa_guard_acquire@PLT
	lgr	%r1,%r2
	lpr	%r1,%r1
	lcr	%r1,%r1
	srl	%r1,31
	llcr	%r1,%r1
	ltr	%r1,%r1
	je	.L30
	mvi	167(%r11),0
	ld	%f2,.L41-.L40(%r13)
	lzdr	%f0
	larl	%r2,_ZZ13random_doublevE12distribution
.LEHB0:
	brasl	%r14,_ZNSt25uniform_real_distributionIdEC1Edd
.LEHE0:
	larl	%r2,_ZGVZ13random_doublevE12distribution
	brasl	%r14,__cxa_guard_release@PLT
.L30:
	larl	%r1,_ZGVZ13random_doublevE9generator
	ic	%r1,0(%r1)
	llgcr	%r1,%r1
	aghi	%r1,-1
	srlg	%r1,%r1,63
	llcr	%r1,%r1
	ltr	%r1,%r1
	je	.L31
	larl	%r2,_ZGVZ13random_doublevE9generator
	brasl	%r14,__cxa_guard_acquire@PLT
	lgr	%r1,%r2
	lpr	%r1,%r1
	lcr	%r1,%r1
	srl	%r1,31
	llcr	%r1,%r1
	ltr	%r1,%r1
	je	.L31
	mvi	167(%r11),0
	larl	%r2,_ZZ13random_doublevE9generator
.LEHB1:
	brasl	%r14,_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC1Ev
.LEHE1:
	larl	%r2,_ZGVZ13random_doublevE9generator
	brasl	%r14,__cxa_guard_release@PLT
.L31:
	larl	%r3,_ZZ13random_doublevE9generator
	larl	%r2,_ZZ13random_doublevE12distribution
.LEHB2:
	brasl	%r14,_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEEEdRT_
	lgdr	%r1,%f0
	j	.L39
.L37:
	lgr	%r10,%r6
	llc	%r1,167(%r11)
	ltr	%r1,%r1
	jne	.L34
	larl	%r2,_ZGVZ13random_doublevE12distribution
	brasl	%r14,__cxa_guard_abort@PLT
.L34:
	lgr	%r1,%r10
	lgr	%r2,%r1
	brasl	%r14,_Unwind_Resume@PLT
.L38:
	lgr	%r10,%r6
	llc	%r1,167(%r11)
	ltr	%r1,%r1
	jne	.L36
	larl	%r2,_ZGVZ13random_doublevE9generator
	brasl	%r14,__cxa_guard_abort@PLT
.L36:
	lgr	%r1,%r10
	lgr	%r2,%r1
	brasl	%r14,_Unwind_Resume@PLT
.LEHE2:
.L39:
	ldgr	%f0,%r1
	lmg	%r6,%r15,216(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_restore 10
	.cfi_restore 9
	.cfi_restore 8
	.cfi_restore 7
	.cfi_restore 6
	.cfi_def_cfa 15, 160
	br	%r14
	.section	.rodata._Z13random_doublev,"aG",@progbits,_Z13random_doublev,comdat
	.align	8
.L40:
.L41:
	.long	1072693248
	.long	0
	.align	2
	.section	.text._Z13random_doublev,"axG",@progbits,_Z13random_doublev,comdat
	.cfi_endproc
.LFE3309:
.globl __gxx_personality_v0
	.section	.gcc_except_table._Z13random_doublev,"aG",@progbits,_Z13random_doublev,comdat
.LLSDA3309:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE3309-.LLSDACSB3309
.LLSDACSB3309:
	.uleb128 .LEHB0-.LFB3309
	.uleb128 .LEHE0-.LEHB0
	.uleb128 .L37-.LFB3309
	.uleb128 0
	.uleb128 .LEHB1-.LFB3309
	.uleb128 .LEHE1-.LEHB1
	.uleb128 .L38-.LFB3309
	.uleb128 0
	.uleb128 .LEHB2-.LFB3309
	.uleb128 .LEHE2-.LEHB2
	.uleb128 0
	.uleb128 0
.LLSDACSE3309:
	.section	.text._Z13random_doublev,"axG",@progbits,_Z13random_doublev,comdat
	.size	_Z13random_doublev, .-_Z13random_doublev
	.section	.text._ZplRK4vec3S1_,"axG",@progbits,_ZplRK4vec3S1_,comdat
	.align	8
	.weak	_ZplRK4vec3S1_
	.type	_ZplRK4vec3S1_, @function
_ZplRK4vec3S1_:
.LFB3947:
	.cfi_startproc
	stmg	%r11,%r15,88(%r15)
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-184(%r15)
	.cfi_def_cfa_offset 344
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,176(%r11)
	stg	%r3,168(%r11)
	stg	%r4,160(%r11)
	lg	%r1,168(%r11)
	ld	%f2,0(%r1)
	lg	%r1,160(%r11)
	ld	%f0,0(%r1)
	adbr	%f0,%f2
	lg	%r1,168(%r11)
	ld	%f4,8(%r1)
	lg	%r1,160(%r11)
	ld	%f2,8(%r1)
	adbr	%f2,%f4
	lg	%r1,168(%r11)
	ld	%f6,16(%r1)
	lg	%r1,160(%r11)
	ld	%f4,16(%r1)
	adbr	%f4,%f6
	lg	%r2,176(%r11)
	brasl	%r14,_ZN4vec3C1Eddd@PLT
	lg	%r2,176(%r11)
	lmg	%r11,%r15,272(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE3947:
	.size	_ZplRK4vec3S1_, .-_ZplRK4vec3S1_
	.section	.text._ZmiRK4vec3S1_,"axG",@progbits,_ZmiRK4vec3S1_,comdat
	.align	8
	.weak	_ZmiRK4vec3S1_
	.type	_ZmiRK4vec3S1_, @function
_ZmiRK4vec3S1_:
.LFB3948:
	.cfi_startproc
	stmg	%r11,%r15,88(%r15)
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-184(%r15)
	.cfi_def_cfa_offset 344
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,176(%r11)
	stg	%r3,168(%r11)
	stg	%r4,160(%r11)
	lg	%r1,168(%r11)
	ld	%f0,0(%r1)
	lg	%r1,160(%r11)
	ld	%f2,0(%r1)
	sdbr	%f0,%f2
	ldr	%f6,%f0
	lg	%r1,168(%r11)
	ld	%f0,8(%r1)
	lg	%r1,160(%r11)
	ld	%f2,8(%r1)
	sdbr	%f0,%f2
	ldr	%f2,%f0
	lg	%r1,168(%r11)
	ld	%f0,16(%r1)
	lg	%r1,160(%r11)
	ld	%f4,16(%r1)
	sdbr	%f0,%f4
	ldr	%f4,%f0
	ldr	%f0,%f6
	lg	%r2,176(%r11)
	brasl	%r14,_ZN4vec3C1Eddd@PLT
	lg	%r2,176(%r11)
	lmg	%r11,%r15,272(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE3948:
	.size	_ZmiRK4vec3S1_, .-_ZmiRK4vec3S1_
	.section	.text._ZmldRK4vec3,"axG",@progbits,_ZmldRK4vec3,comdat
	.align	8
	.weak	_ZmldRK4vec3
	.type	_ZmldRK4vec3, @function
_ZmldRK4vec3:
.LFB3950:
	.cfi_startproc
	stmg	%r11,%r15,88(%r15)
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-184(%r15)
	.cfi_def_cfa_offset 344
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,176(%r11)
	std	%f0,168(%r11)
	stg	%r3,160(%r11)
	lg	%r1,160(%r11)
	ld	%f0,0(%r1)
	mdb	%f0,168(%r11)
	ldr	%f6,%f0
	lg	%r1,160(%r11)
	ld	%f0,8(%r1)
	mdb	%f0,168(%r11)
	ldr	%f2,%f0
	lg	%r1,160(%r11)
	ld	%f0,16(%r1)
	mdb	%f0,168(%r11)
	ldr	%f4,%f0
	ldr	%f0,%f6
	lg	%r2,176(%r11)
	brasl	%r14,_ZN4vec3C1Eddd@PLT
	lg	%r2,176(%r11)
	lmg	%r11,%r15,272(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE3950:
	.size	_ZmldRK4vec3, .-_ZmldRK4vec3
	.section	.text._Zdv4vec3d,"axG",@progbits,_Zdv4vec3d,comdat
	.align	8
	.weak	_Zdv4vec3d
	.type	_Zdv4vec3d, @function
_Zdv4vec3d:
.LFB3952:
	.cfi_startproc
	stmg	%r10,%r15,80(%r15)
	.cfi_offset 10, -80
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	larl	%r13,.L54
	lay	%r15,-184(%r15)
	.cfi_def_cfa_offset 344
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,168(%r11)
	lgr	%r10,%r3
	std	%f0,160(%r11)
	ear	%r1,%a0
	sllg	%r1,%r1,32
	ear	%r1,%a1
	mvc	176(8,%r11),40(%r1)
	ld	%f0,.L55-.L54(%r13)
	ddb	%f0,160(%r11)
	lg	%r1,168(%r11)
	lgr	%r3,%r10
	lgr	%r2,%r1
	brasl	%r14,_ZmldRK4vec3
	ear	%r1,%a0
	sllg	%r1,%r1,32
	ear	%r1,%a1
	clc	176(8,%r11),40(%r1)
	je	.L53
	brasl	%r14,__stack_chk_fail@PLT
.L53:
	lg	%r2,168(%r11)
	lmg	%r10,%r15,264(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_restore 10
	.cfi_def_cfa 15, 160
	br	%r14
	.section	.rodata._Zdv4vec3d,"aG",@progbits,_Zdv4vec3d,comdat
	.align	8
.L54:
.L55:
	.long	1072693248
	.long	0
	.align	2
	.section	.text._Zdv4vec3d,"axG",@progbits,_Zdv4vec3d,comdat
	.cfi_endproc
.LFE3952:
	.size	_Zdv4vec3d, .-_Zdv4vec3d
	.section	.text._Z11unit_vector4vec3,"axG",@progbits,_Z11unit_vector4vec3,comdat
	.align	8
	.weak	_Z11unit_vector4vec3
	.type	_Z11unit_vector4vec3, @function
_Z11unit_vector4vec3:
.LFB3955:
	.cfi_startproc
	stmg	%r10,%r15,80(%r15)
	.cfi_offset 10, -80
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-200(%r15)
	.cfi_def_cfa_offset 360
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,184(%r11)
	lgr	%r10,%r3
	ear	%r1,%a0
	sllg	%r1,%r1,32
	ear	%r1,%a1
	mvc	192(8,%r11),40(%r1)
	lgr	%r2,%r10
	brasl	%r14,_ZNK4vec36lengthEv@PLT
	lg	%r1,184(%r11)
	mvc	160(24,%r11),0(%r10)
	aghik	%r2,%r11,160
	lgr	%r3,%r2
	lgr	%r2,%r1
	brasl	%r14,_Zdv4vec3d
	ear	%r1,%a0
	sllg	%r1,%r1,32
	ear	%r1,%a1
	clc	192(8,%r11),40(%r1)
	je	.L58
	brasl	%r14,__stack_chk_fail@PLT
.L58:
	lg	%r2,184(%r11)
	lmg	%r10,%r15,280(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_restore 10
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE3955:
	.size	_Z11unit_vector4vec3, .-_Z11unit_vector4vec3
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,2
	.section	.text._ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align	8
	.weak	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED2Ev
	.type	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED2Ev:
.LFB3961:
	.cfi_startproc
	stmg	%r11,%r15,88(%r15)
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-168(%r15)
	.cfi_def_cfa_offset 328
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,160(%r11)
	lg	%r1,160(%r11)
	aghi	%r1,8
	lgr	%r2,%r1
	brasl	%r14,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED1Ev
	nopr	%r0
	lmg	%r11,%r15,256(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE3961:
	.size	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED1Ev
	.set	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED1Ev,_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt10shared_ptrI8materialED2Ev,"axG",@progbits,_ZNSt10shared_ptrI8materialED5Ev,comdat
	.align	8
	.weak	_ZNSt10shared_ptrI8materialED2Ev
	.type	_ZNSt10shared_ptrI8materialED2Ev, @function
_ZNSt10shared_ptrI8materialED2Ev:
.LFB3963:
	.cfi_startproc
	stmg	%r11,%r15,88(%r15)
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-168(%r15)
	.cfi_def_cfa_offset 328
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,160(%r11)
	lg	%r1,160(%r11)
	lgr	%r2,%r1
	brasl	%r14,_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED2Ev
	nopr	%r0
	lmg	%r11,%r15,256(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE3963:
	.size	_ZNSt10shared_ptrI8materialED2Ev, .-_ZNSt10shared_ptrI8materialED2Ev
	.weak	_ZNSt10shared_ptrI8materialED1Ev
	.set	_ZNSt10shared_ptrI8materialED1Ev,_ZNSt10shared_ptrI8materialED2Ev
	.section	.text._ZN10hit_recordC2Ev,"axG",@progbits,_ZN10hit_recordC5Ev,comdat
	.align	8
	.weak	_ZN10hit_recordC2Ev
	.type	_ZN10hit_recordC2Ev, @function
_ZN10hit_recordC2Ev:
.LFB3965:
	.cfi_startproc
	stmg	%r11,%r15,88(%r15)
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-168(%r15)
	.cfi_def_cfa_offset 328
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,160(%r11)
	lg	%r1,160(%r11)
	lgr	%r2,%r1
	brasl	%r14,_ZN4vec3C1Ev@PLT
	lg	%r1,160(%r11)
	aghi	%r1,24
	lgr	%r2,%r1
	brasl	%r14,_ZN4vec3C1Ev@PLT
	lg	%r1,160(%r11)
	aghi	%r1,48
	lgr	%r2,%r1
	brasl	%r14,_ZNSt10shared_ptrI8materialEC1Ev
	nopr	%r0
	lmg	%r11,%r15,256(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE3965:
	.size	_ZN10hit_recordC2Ev, .-_ZN10hit_recordC2Ev
	.weak	_ZN10hit_recordC1Ev
	.set	_ZN10hit_recordC1Ev,_ZN10hit_recordC2Ev
	.section	.text._ZN10hit_recordD2Ev,"axG",@progbits,_ZN10hit_recordD5Ev,comdat
	.align	8
	.weak	_ZN10hit_recordD2Ev
	.type	_ZN10hit_recordD2Ev, @function
_ZN10hit_recordD2Ev:
.LFB3968:
	.cfi_startproc
	stmg	%r11,%r15,88(%r15)
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-168(%r15)
	.cfi_def_cfa_offset 328
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,160(%r11)
	lg	%r1,160(%r11)
	aghi	%r1,48
	lgr	%r2,%r1
	brasl	%r14,_ZNSt10shared_ptrI8materialED1Ev
	nopr	%r0
	lmg	%r11,%r15,256(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE3968:
	.size	_ZN10hit_recordD2Ev, .-_ZN10hit_recordD2Ev
	.weak	_ZN10hit_recordD1Ev
	.set	_ZN10hit_recordD1Ev,_ZN10hit_recordD2Ev
.text
	.align	8
	.type	_ZL9ray_colorRK3rayRK8hittablei, @function
_ZL9ray_colorRK3rayRK8hittablei:
.LFB3957:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA3957
	stmg	%r6,%r15,48(%r15)
	.cfi_offset 6, -112
	.cfi_offset 7, -104
	.cfi_offset 8, -96
	.cfi_offset 9, -88
	.cfi_offset 10, -80
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	larl	%r13,.L77
	lgr	%r14,%r15
	lay	%r15,-496(%r15)
	.cfi_def_cfa_offset 656
	aghi	%r14,-8
	std	%f8,0(%r14)
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	.cfi_offset 24, -168
	stg	%r2,208(%r11)
	stg	%r3,200(%r11)
	stg	%r4,192(%r11)
	lgr	%r1,%r5
	st	%r1,188(%r11)
	ear	%r1,%a0
	sllg	%r1,%r1,32
	ear	%r1,%a1
	mvc	480(8,%r11),40(%r1)
	aghik	%r1,%r11,400
	lgr	%r2,%r1
.LEHB3:
	brasl	%r14,_ZN10hit_recordC1Ev
.LEHE3:
	l	%r1,188(%r11)
	ltr	%r1,%r1
	jh	.L69
	lzdr	%f4
	lzdr	%f2
	lzdr	%f0
	lg	%r2,208(%r11)
.LEHB4:
	brasl	%r14,_ZN4vec3C1Eddd@PLT
	j	.L70
.L69:
	ld	%f0,.L78-.L77(%r13)
	std	%f0,216(%r11)
	lg	%r1,192(%r11)
	lg	%r1,0(%r1)
	lg	%r1,0(%r1)
	aghik	%r2,%r11,400
	lgr	%r4,%r2
	ld	%f2,216(%r11)
	ld	%f0,.L79-.L77(%r13)
	lg	%r3,200(%r11)
	lg	%r2,192(%r11)
	basr	%r14,%r1
	lgr	%r1,%r2
	llcr	%r1,%r1
	ltr	%r1,%r1
	je	.L71
	aghik	%r1,%r11,328
	aghik	%r2,%r11,400
	aghik	%r3,%r2,24
	aghik	%r2,%r11,400
	lgr	%r4,%r3
	lgr	%r3,%r2
	lgr	%r2,%r1
	brasl	%r14,_ZplRK4vec3S1_
	aghik	%r1,%r11,352
	lgr	%r2,%r1
	brasl	%r14,_Z18random_unit_vectorv@PLT
	aghik	%r1,%r11,280
	aghik	%r3,%r11,352
	aghik	%r2,%r11,328
	lgr	%r4,%r3
	lgr	%r3,%r2
	lgr	%r2,%r1
	brasl	%r14,_ZplRK4vec3S1_
	aghik	%r1,%r11,304
	aghik	%r3,%r11,400
	aghik	%r2,%r11,280
	lgr	%r4,%r3
	lgr	%r3,%r2
	lgr	%r2,%r1
	brasl	%r14,_ZmiRK4vec3S1_
	aghik	%r3,%r11,304
	aghik	%r2,%r11,400
	aghik	%r1,%r11,352
	lgr	%r4,%r3
	lgr	%r3,%r2
	lgr	%r2,%r1
	brasl	%r14,_ZN3rayC1ERK4vec3S2_@PLT
	l	%r1,188(%r11)
	ahik	%r2,%r1,-1
	aghik	%r1,%r11,328
	lgfr	%r3,%r2
	aghik	%r2,%r11,352
	lgr	%r5,%r3
	lg	%r4,192(%r11)
	lgr	%r3,%r2
	lgr	%r2,%r1
	brasl	%r14,_ZL9ray_colorRK3rayRK8hittablei
	lg	%r1,208(%r11)
	aghik	%r2,%r11,328
	lgr	%r3,%r2
	ld	%f0,.L80-.L77(%r13)
	lgr	%r2,%r1
	brasl	%r14,_ZmldRK4vec3
	j	.L70
.L71:
	aghik	%r1,%r11,352
	lg	%r3,200(%r11)
	lgr	%r2,%r1
	brasl	%r14,_ZNK3ray9directionEv@PLT
	aghik	%r1,%r11,232
	mvc	160(24,%r11),352(%r11)
	aghik	%r2,%r11,160
	lgr	%r3,%r2
	lgr	%r2,%r1
	brasl	%r14,_Z11unit_vector4vec3
	aghik	%r1,%r11,232
	lgr	%r2,%r1
	brasl	%r14,_ZNK4vec31yEv@PLT
	adb	%f0,.L81-.L77(%r13)
	mdb	%f0,.L80-.L77(%r13)
	std	%f0,224(%r11)
	ld	%f0,.L81-.L77(%r13)
	sdb	%f0,224(%r11)
	ldr	%f8,%f0
	aghik	%r1,%r11,256
	ld	%f4,.L81-.L77(%r13)
	ld	%f2,.L81-.L77(%r13)
	ld	%f0,.L81-.L77(%r13)
	lgr	%r2,%r1
	brasl	%r14,_ZN4vec3C1Eddd@PLT
	aghik	%r1,%r11,280
	aghik	%r2,%r11,256
	lgr	%r3,%r2
	ldr	%f0,%f8
	lgr	%r2,%r1
	brasl	%r14,_ZmldRK4vec3
	aghik	%r1,%r11,304
	ld	%f4,.L81-.L77(%r13)
	ld	%f2,.L82-.L77(%r13)
	ld	%f0,.L80-.L77(%r13)
	lgr	%r2,%r1
	brasl	%r14,_ZN4vec3C1Eddd@PLT
	aghik	%r1,%r11,328
	aghik	%r2,%r11,304
	lgr	%r3,%r2
	ld	%f0,224(%r11)
	lgr	%r2,%r1
	brasl	%r14,_ZmldRK4vec3
	lg	%r1,208(%r11)
	aghik	%r3,%r11,328
	aghik	%r2,%r11,280
	lgr	%r4,%r3
	lgr	%r3,%r2
	lgr	%r2,%r1
	brasl	%r14,_ZplRK4vec3S1_
.LEHE4:
.L70:
	aghik	%r1,%r11,400
	lgr	%r2,%r1
	brasl	%r14,_ZN10hit_recordD1Ev
	ear	%r1,%a0
	sllg	%r1,%r1,32
	ear	%r1,%a1
	clc	480(8,%r11),40(%r1)
	je	.L74
	j	.L76
.L75:
	lgr	%r10,%r6
	aghik	%r1,%r11,400
	lgr	%r2,%r1
	brasl	%r14,_ZN10hit_recordD1Ev
	lgr	%r1,%r10
	lgr	%r2,%r1
.LEHB5:
	brasl	%r14,_Unwind_Resume@PLT
.LEHE5:
.L76:
	brasl	%r14,__stack_chk_fail@PLT
.L74:
	lg	%r2,208(%r11)
	ld	%f8,488(%r11)
	lmg	%r6,%r15,544(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_restore 10
	.cfi_restore 9
	.cfi_restore 8
	.cfi_restore 7
	.cfi_restore 6
	.cfi_restore 24
	.cfi_def_cfa 15, 160
	br	%r14
	.section	.rodata
	.align	8
.L77:
.L82:
	.long	1072064102
	.long	1717986918
.L81:
	.long	1072693248
	.long	0
.L80:
	.long	1071644672
	.long	0
.L79:
	.long	1062232653
	.long	3539053052
.L78:
	.long	2146435072
	.long	0
	.align	2
.text
	.cfi_endproc
.LFE3957:
	.section	.gcc_except_table,"a",@progbits
.LLSDA3957:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE3957-.LLSDACSB3957
.LLSDACSB3957:
	.uleb128 .LEHB3-.LFB3957
	.uleb128 .LEHE3-.LEHB3
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB4-.LFB3957
	.uleb128 .LEHE4-.LEHB4
	.uleb128 .L75-.LFB3957
	.uleb128 0
	.uleb128 .LEHB5-.LFB3957
	.uleb128 .LEHE5-.LEHB5
	.uleb128 0
	.uleb128 0
.LLSDACSE3957:
.text
	.size	_ZL9ray_colorRK3rayRK8hittablei, .-_ZL9ray_colorRK3rayRK8hittablei
	.section	.text._ZN13hittable_listD2Ev,"axG",@progbits,_ZN13hittable_listD5Ev,comdat
	.align	8
	.weak	_ZN13hittable_listD2Ev
	.type	_ZN13hittable_listD2Ev, @function
_ZN13hittable_listD2Ev:
.LFB3972:
	.cfi_startproc
	stmg	%r11,%r15,88(%r15)
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-168(%r15)
	.cfi_def_cfa_offset 328
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,160(%r11)
	lgrl	%r1,_ZTV13hittable_list@GOTENT
	aghi	%r1,16
	lgr	%r2,%r1
	lg	%r1,160(%r11)
	stg	%r2,0(%r1)
	lg	%r1,160(%r11)
	aghi	%r1,8
	lgr	%r2,%r1
	brasl	%r14,_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED1Ev
	nopr	%r0
	lmg	%r11,%r15,256(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE3972:
	.size	_ZN13hittable_listD2Ev, .-_ZN13hittable_listD2Ev
	.weak	_ZN13hittable_listD1Ev
	.set	_ZN13hittable_listD1Ev,_ZN13hittable_listD2Ev
	.section	.text._ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align	8
	.weak	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED2Ev
	.type	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED2Ev:
.LFB3976:
	.cfi_startproc
	stmg	%r11,%r15,88(%r15)
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-168(%r15)
	.cfi_def_cfa_offset 328
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,160(%r11)
	lg	%r1,160(%r11)
	aghi	%r1,8
	lgr	%r2,%r1
	brasl	%r14,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED1Ev
	nopr	%r0
	lmg	%r11,%r15,256(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE3976:
	.size	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED1Ev
	.set	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED1Ev,_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt10shared_ptrI6sphereED2Ev,"axG",@progbits,_ZNSt10shared_ptrI6sphereED5Ev,comdat
	.align	8
	.weak	_ZNSt10shared_ptrI6sphereED2Ev
	.type	_ZNSt10shared_ptrI6sphereED2Ev, @function
_ZNSt10shared_ptrI6sphereED2Ev:
.LFB3978:
	.cfi_startproc
	stmg	%r11,%r15,88(%r15)
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-168(%r15)
	.cfi_def_cfa_offset 328
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,160(%r11)
	lg	%r1,160(%r11)
	lgr	%r2,%r1
	brasl	%r14,_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED2Ev
	nopr	%r0
	lmg	%r11,%r15,256(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE3978:
	.size	_ZNSt10shared_ptrI6sphereED2Ev, .-_ZNSt10shared_ptrI6sphereED2Ev
	.weak	_ZNSt10shared_ptrI6sphereED1Ev
	.set	_ZNSt10shared_ptrI6sphereED1Ev,_ZNSt10shared_ptrI6sphereED2Ev
	.section	.text._ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align	8
	.weak	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED2Ev
	.type	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED2Ev:
.LFB3982:
	.cfi_startproc
	stmg	%r11,%r15,88(%r15)
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-168(%r15)
	.cfi_def_cfa_offset 328
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,160(%r11)
	lg	%r1,160(%r11)
	aghi	%r1,8
	lgr	%r2,%r1
	brasl	%r14,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED1Ev
	nopr	%r0
	lmg	%r11,%r15,256(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE3982:
	.size	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED1Ev
	.set	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED1Ev,_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt10shared_ptrI8hittableED2Ev,"axG",@progbits,_ZNSt10shared_ptrI8hittableED5Ev,comdat
	.align	8
	.weak	_ZNSt10shared_ptrI8hittableED2Ev
	.type	_ZNSt10shared_ptrI8hittableED2Ev, @function
_ZNSt10shared_ptrI8hittableED2Ev:
.LFB3984:
	.cfi_startproc
	stmg	%r11,%r15,88(%r15)
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-168(%r15)
	.cfi_def_cfa_offset 328
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,160(%r11)
	lg	%r1,160(%r11)
	lgr	%r2,%r1
	brasl	%r14,_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED2Ev
	nopr	%r0
	lmg	%r11,%r15,256(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE3984:
	.size	_ZNSt10shared_ptrI8hittableED2Ev, .-_ZNSt10shared_ptrI8hittableED2Ev
	.weak	_ZNSt10shared_ptrI8hittableED1Ev
	.set	_ZNSt10shared_ptrI8hittableED1Ev,_ZNSt10shared_ptrI8hittableED2Ev
	.section	.rodata
	.align	2
.LC7:
	.string	"chapter8.ppm"
	.align	2
.LC8:
	.string	"P3\n"
	.align	2
.LC9:
	.string	"\n255\n"
	.align	2
.LC10:
	.string	"\rScanlines remaining: "
	.align	2
.LC11:
	.string	"\nDone.\n"
.text
	.align	8
.globl main
	.type	main, @function
main:
.LFB3970:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA3970
	stmg	%r6,%r15,48(%r15)
	.cfi_offset 6, -112
	.cfi_offset 7, -104
	.cfi_offset 8, -96
	.cfi_offset 9, -88
	.cfi_offset 10, -80
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	larl	%r13,.L115
	lgr	%r14,%r15
	lay	%r15,-1112(%r15)
	.cfi_def_cfa_offset 1272
	aghi	%r14,-8
	std	%f8,0(%r14)
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	.cfi_offset 24, -168
	ear	%r1,%a0
	sllg	%r1,%r1,32
	ear	%r1,%a1
	mvc	1096(8,%r11),40(%r1)
	ld	%f0,.L116-.L115(%r13)
	std	%f0,224(%r11)
	mvhi	200(%r11),400
	mvhi	204(%r11),225
	mvhi	208(%r11),100
	mvhi	212(%r11),50
	aghik	%r1,%r11,280
	lgr	%r2,%r1
.LEHB6:
	brasl	%r14,_ZN13hittable_listC1Ev@PLT
.LEHE6:
	aghik	%r1,%r11,584
	ld	%f4,.L117-.L115(%r13)
	lzdr	%f2
	lzdr	%f0
	lgr	%r2,%r1
.LEHB7:
	brasl	%r14,_ZN4vec3C1Eddd@PLT
.LEHE7:
	ld	%f0,.L118-.L115(%r13)
	std	%f0,216(%r11)
	aghik	%r1,%r11,248
	aghik	%r3,%r11,216
	aghik	%r2,%r11,584
	lgr	%r4,%r3
	lgr	%r3,%r2
	lgr	%r2,%r1
.LEHB8:
	brasl	%r14,_ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_
.LEHE8:
	aghik	%r2,%r11,248
	aghik	%r1,%r11,264
	lgr	%r3,%r2
	lgr	%r2,%r1
	brasl	%r14,_ZNSt10shared_ptrI8hittableEC1I6spherevEEOS_IT_E
	aghik	%r2,%r11,264
	aghik	%r1,%r11,280
	lgr	%r3,%r2
	lgr	%r2,%r1
.LEHB9:
	brasl	%r14,_ZN13hittable_list3addESt10shared_ptrI8hittableE@PLT
.LEHE9:
	aghik	%r1,%r11,264
	lgr	%r2,%r1
	brasl	%r14,_ZNSt10shared_ptrI8hittableED1Ev
	aghik	%r1,%r11,248
	lgr	%r2,%r1
	brasl	%r14,_ZNSt10shared_ptrI6sphereED1Ev
	aghik	%r1,%r11,584
	ld	%f4,.L117-.L115(%r13)
	ld	%f2,.L119-.L115(%r13)
	lzdr	%f0
	lgr	%r2,%r1
.LEHB10:
	brasl	%r14,_ZN4vec3C1Eddd@PLT
.LEHE10:
	mvhi	216(%r11),100
	aghik	%r1,%r11,248
	aghik	%r3,%r11,216
	aghik	%r2,%r11,584
	lgr	%r4,%r3
	lgr	%r3,%r2
	lgr	%r2,%r1
.LEHB11:
	brasl	%r14,_ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_
.LEHE11:
	aghik	%r2,%r11,248
	aghik	%r1,%r11,264
	lgr	%r3,%r2
	lgr	%r2,%r1
	brasl	%r14,_ZNSt10shared_ptrI8hittableEC1I6spherevEEOS_IT_E
	aghik	%r2,%r11,264
	aghik	%r1,%r11,280
	lgr	%r3,%r2
	lgr	%r2,%r1
.LEHB12:
	brasl	%r14,_ZN13hittable_list3addESt10shared_ptrI8hittableE@PLT
.LEHE12:
	aghik	%r1,%r11,264
	lgr	%r2,%r1
	brasl	%r14,_ZNSt10shared_ptrI8hittableED1Ev
	aghik	%r1,%r11,248
	lgr	%r2,%r1
	brasl	%r14,_ZNSt10shared_ptrI6sphereED1Ev
	aghik	%r1,%r11,408
	lgr	%r2,%r1
.LEHB13:
	brasl	%r14,_ZN6cameraC1Ev@PLT
	aghik	%r1,%r11,584
	lgr	%r2,%r1
	brasl	%r14,_ZNSt14basic_ofstreamIcSt11char_traitsIcEEC1Ev@PLT
.LEHE13:
	aghik	%r1,%r11,584
	lghi	%r4,16
	larl	%r3,.LC7
	lgr	%r2,%r1
.LEHB14:
	brasl	%r14,_ZNSt14basic_ofstreamIcSt11char_traitsIcEE4openEPKcSt13_Ios_Openmode@PLT
	aghik	%r1,%r11,584
	larl	%r3,.LC8
	lgr	%r2,%r1
	brasl	%r14,_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	lgr	%r1,%r2
	lghi	%r3,400
	lgr	%r2,%r1
	brasl	%r14,_ZNSolsEi@PLT
	lgr	%r1,%r2
	lghi	%r3,32
	lgr	%r2,%r1
	brasl	%r14,_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_c@PLT
	lgr	%r1,%r2
	lghi	%r3,225
	lgr	%r2,%r1
	brasl	%r14,_ZNSolsEi@PLT
	lgr	%r1,%r2
	larl	%r3,.LC9
	lgr	%r2,%r1
	brasl	%r14,_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	mvhi	188(%r11),224
.L99:
	l	%r1,188(%r11)
	ltr	%r1,%r1
	jl	.L94
	larl	%r3,.LC10
	lgrl	%r1,_ZSt4cerr@GOTENT
	lgr	%r2,%r1
	brasl	%r14,_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	lgf	%r1,188(%r11)
	lgr	%r3,%r1
	brasl	%r14,_ZNSolsEi@PLT
	lgr	%r1,%r2
	lghi	%r3,32
	lgr	%r2,%r1
	brasl	%r14,_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_c@PLT
	lgrl	%r1,_ZSt5flushIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_@GOTENT
	lgr	%r3,%r1
	brasl	%r14,_ZNSolsEPFRSoS_E@PLT
	mvhi	192(%r11),0
.L98:
	l	%r1,192(%r11)
	chi	%r1,399
	jh	.L95
	aghik	%r1,%r11,312
	lzdr	%f4
	lzdr	%f2
	lzdr	%f0
	lgr	%r2,%r1
	brasl	%r14,_ZN4vec3C1Eddd@PLT
	mvhi	196(%r11),0
.L97:
	l	%r1,196(%r11)
	chi	%r1,99
	jh	.L96
	l	%r1,192(%r11)
	cdfbr	%f8,%r1
	brasl	%r14,_Z13random_doublev
	adbr	%f0,%f8
	ddb	%f0,.L120-.L115(%r13)
	std	%f0,232(%r11)
	l	%r1,188(%r11)
	cdfbr	%f8,%r1
	brasl	%r14,_Z13random_doublev
	adbr	%f0,%f8
	ddb	%f0,.L121-.L115(%r13)
	std	%f0,240(%r11)
	aghik	%r1,%r11,360
	aghik	%r2,%r11,408
	ld	%f2,240(%r11)
	ld	%f0,232(%r11)
	lgr	%r3,%r2
	lgr	%r2,%r1
	brasl	%r14,_ZNK6camera7get_rayEdd@PLT
	aghik	%r1,%r11,336
	aghik	%r3,%r11,280
	aghik	%r2,%r11,360
	lghi	%r5,50
	lgr	%r4,%r3
	lgr	%r3,%r2
	lgr	%r2,%r1
	brasl	%r14,_ZL9ray_colorRK3rayRK8hittablei
	aghik	%r2,%r11,336
	aghik	%r1,%r11,312
	lgr	%r3,%r2
	lgr	%r2,%r1
	brasl	%r14,_ZN4vec3pLERKS_@PLT
	asi	196(%r11),1
	j	.L97
.L96:
	mvc	160(24,%r11),312(%r11)
	aghik	%r2,%r11,160
	aghik	%r1,%r11,584
	lghi	%r4,100
	lgr	%r3,%r2
	lgr	%r2,%r1
	brasl	%r14,_Z11write_colorRSt14basic_ofstreamIcSt11char_traitsIcEE4vec3i@PLT
	asi	192(%r11),1
	j	.L98
.L95:
	asi	188(%r11),-1
	j	.L99
.L94:
	larl	%r3,.LC11
	lgrl	%r1,_ZSt4cerr@GOTENT
	lgr	%r2,%r1
	brasl	%r14,_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
.LEHE14:
	lhi	%r10,0
	aghik	%r1,%r11,584
	lgr	%r2,%r1
	brasl	%r14,_ZNSt14basic_ofstreamIcSt11char_traitsIcEED1Ev@PLT
	aghik	%r1,%r11,280
	lgr	%r2,%r1
	brasl	%r14,_ZN13hittable_listD1Ev
	lgfr	%r1,%r10
	lgr	%r2,%r1
	ear	%r1,%a0
	sllg	%r1,%r1,32
	ear	%r1,%a1
	clc	1096(8,%r11),40(%r1)
	je	.L107
	j	.L114
.L110:
	lgr	%r10,%r6
	aghik	%r1,%r11,264
	lgr	%r2,%r1
	brasl	%r14,_ZNSt10shared_ptrI8hittableED1Ev
	aghik	%r1,%r11,248
	lgr	%r2,%r1
	brasl	%r14,_ZNSt10shared_ptrI6sphereED1Ev
	lgr	%r1,%r10
	j	.L102
.L109:
	lgr	%r1,%r6
.L102:
	lgr	%r10,%r1
	j	.L103
.L112:
	lgr	%r10,%r6
	aghik	%r1,%r11,264
	lgr	%r2,%r1
	brasl	%r14,_ZNSt10shared_ptrI8hittableED1Ev
	aghik	%r1,%r11,248
	lgr	%r2,%r1
	brasl	%r14,_ZNSt10shared_ptrI6sphereED1Ev
	lgr	%r1,%r10
	j	.L105
.L111:
	lgr	%r1,%r6
.L105:
	lgr	%r10,%r1
	j	.L103
.L113:
	lgr	%r10,%r6
	aghik	%r1,%r11,584
	lgr	%r2,%r1
	brasl	%r14,_ZNSt14basic_ofstreamIcSt11char_traitsIcEED1Ev@PLT
	j	.L103
.L108:
	lgr	%r10,%r6
.L103:
	aghik	%r1,%r11,280
	lgr	%r2,%r1
	brasl	%r14,_ZN13hittable_listD1Ev
	lgr	%r1,%r10
	lgr	%r2,%r1
.LEHB15:
	brasl	%r14,_Unwind_Resume@PLT
.LEHE15:
.L114:
	brasl	%r14,__stack_chk_fail@PLT
.L107:
	ld	%f8,1104(%r11)
	lmg	%r6,%r15,1160(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_restore 10
	.cfi_restore 9
	.cfi_restore 8
	.cfi_restore 7
	.cfi_restore 6
	.cfi_restore 24
	.cfi_def_cfa 15, 160
	br	%r14
	.section	.rodata
	.align	8
.L115:
.L121:
	.long	1080819712
	.long	0
.L120:
	.long	1081667584
	.long	0
.L119:
	.long	-1067900928
	.long	0
.L118:
	.long	1071644672
	.long	0
.L117:
	.long	-1074790400
	.long	0
.L116:
	.long	1073508807
	.long	477218588
	.align	2
.text
	.cfi_endproc
.LFE3970:
	.section	.gcc_except_table
.LLSDA3970:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE3970-.LLSDACSB3970
.LLSDACSB3970:
	.uleb128 .LEHB6-.LFB3970
	.uleb128 .LEHE6-.LEHB6
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB7-.LFB3970
	.uleb128 .LEHE7-.LEHB7
	.uleb128 .L108-.LFB3970
	.uleb128 0
	.uleb128 .LEHB8-.LFB3970
	.uleb128 .LEHE8-.LEHB8
	.uleb128 .L109-.LFB3970
	.uleb128 0
	.uleb128 .LEHB9-.LFB3970
	.uleb128 .LEHE9-.LEHB9
	.uleb128 .L110-.LFB3970
	.uleb128 0
	.uleb128 .LEHB10-.LFB3970
	.uleb128 .LEHE10-.LEHB10
	.uleb128 .L108-.LFB3970
	.uleb128 0
	.uleb128 .LEHB11-.LFB3970
	.uleb128 .LEHE11-.LEHB11
	.uleb128 .L111-.LFB3970
	.uleb128 0
	.uleb128 .LEHB12-.LFB3970
	.uleb128 .LEHE12-.LEHB12
	.uleb128 .L112-.LFB3970
	.uleb128 0
	.uleb128 .LEHB13-.LFB3970
	.uleb128 .LEHE13-.LEHB13
	.uleb128 .L108-.LFB3970
	.uleb128 0
	.uleb128 .LEHB14-.LFB3970
	.uleb128 .LEHE14-.LEHB14
	.uleb128 .L113-.LFB3970
	.uleb128 0
	.uleb128 .LEHB15-.LFB3970
	.uleb128 .LEHE15-.LEHB15
	.uleb128 0
	.uleb128 0
.LLSDACSE3970:
.text
	.size	main, .-main
	.section	.text._ZNSt25uniform_real_distributionIdEC2Edd,"axG",@progbits,_ZNSt25uniform_real_distributionIdEC5Edd,comdat
	.align	8
	.weak	_ZNSt25uniform_real_distributionIdEC2Edd
	.type	_ZNSt25uniform_real_distributionIdEC2Edd, @function
_ZNSt25uniform_real_distributionIdEC2Edd:
.LFB4235:
	.cfi_startproc
	stmg	%r11,%r15,88(%r15)
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-184(%r15)
	.cfi_def_cfa_offset 344
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,176(%r11)
	std	%f0,168(%r11)
	std	%f2,160(%r11)
	lg	%r1,176(%r11)
	ld	%f2,160(%r11)
	ld	%f0,168(%r11)
	lgr	%r2,%r1
	brasl	%r14,_ZNSt25uniform_real_distributionIdE10param_typeC1Edd
	nopr	%r0
	lmg	%r11,%r15,272(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE4235:
	.size	_ZNSt25uniform_real_distributionIdEC2Edd, .-_ZNSt25uniform_real_distributionIdEC2Edd
	.weak	_ZNSt25uniform_real_distributionIdEC1Edd
	.set	_ZNSt25uniform_real_distributionIdEC1Edd,_ZNSt25uniform_real_distributionIdEC2Edd
	.section	.text._ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC2Ev,"axG",@progbits,_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC5Ev,comdat
	.align	8
	.weak	_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC2Ev
	.type	_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC2Ev, @function
_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC2Ev:
.LFB4238:
	.cfi_startproc
	stmg	%r11,%r15,88(%r15)
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-168(%r15)
	.cfi_def_cfa_offset 328
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,160(%r11)
	lghi	%r3,5489
	lg	%r2,160(%r11)
	brasl	%r14,_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC1Em
	nopr	%r0
	lmg	%r11,%r15,256(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE4238:
	.size	_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC2Ev, .-_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC2Ev
	.weak	_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC1Ev
	.set	_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC1Ev,_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC2Ev
	.section	.text._ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEEEdRT_,"axG",@progbits,_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEEEdRT_,comdat
	.align	8
	.weak	_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEEEdRT_
	.type	_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEEEdRT_, @function
_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEEEdRT_:
.LFB4240:
	.cfi_startproc
	stmg	%r11,%r15,88(%r15)
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-176(%r15)
	.cfi_def_cfa_offset 336
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,168(%r11)
	stg	%r3,160(%r11)
	lg	%r1,168(%r11)
	lgr	%r4,%r1
	lg	%r3,160(%r11)
	lg	%r2,168(%r11)
	brasl	%r14,_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEEEdRT_RKNS0_10param_typeE
	lgdr	%r1,%f0
	ldgr	%f0,%r1
	lmg	%r11,%r15,264(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE4240:
	.size	_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEEEdRT_, .-_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEEEdRT_
	.section	.text._ZNSt10shared_ptrI8materialEC2Ev,"axG",@progbits,_ZNSt10shared_ptrI8materialEC5Ev,comdat
	.align	8
	.weak	_ZNSt10shared_ptrI8materialEC2Ev
	.type	_ZNSt10shared_ptrI8materialEC2Ev, @function
_ZNSt10shared_ptrI8materialEC2Ev:
.LFB4268:
	.cfi_startproc
	stmg	%r11,%r15,88(%r15)
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-168(%r15)
	.cfi_def_cfa_offset 328
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,160(%r11)
	lg	%r1,160(%r11)
	lgr	%r2,%r1
	brasl	%r14,_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC2Ev
	nopr	%r0
	lmg	%r11,%r15,256(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE4268:
	.size	_ZNSt10shared_ptrI8materialEC2Ev, .-_ZNSt10shared_ptrI8materialEC2Ev
	.weak	_ZNSt10shared_ptrI8materialEC1Ev
	.set	_ZNSt10shared_ptrI8materialEC1Ev,_ZNSt10shared_ptrI8materialEC2Ev
	.section	.text._ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align	8
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED2Ev
	.type	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED2Ev:
.LFB4271:
	.cfi_startproc
	stmg	%r11,%r15,88(%r15)
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-168(%r15)
	.cfi_def_cfa_offset 328
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,160(%r11)
	lg	%r1,160(%r11)
	lg	%r1,0(%r1)
	ltgr	%r1,%r1
	je	.L133
	lg	%r1,160(%r11)
	lg	%r1,0(%r1)
	lgr	%r2,%r1
	brasl	%r14,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv
.L133:
	nopr	%r0
	lmg	%r11,%r15,256(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE4271:
	.size	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED1Ev
	.set	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED1Ev,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED2Ev,"axG",@progbits,_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED5Ev,comdat
	.align	8
	.weak	_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED2Ev
	.type	_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED2Ev, @function
_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED2Ev:
.LFB4274:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA4274
	stmg	%r9,%r15,72(%r15)
	.cfi_offset 9, -88
	.cfi_offset 10, -80
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-168(%r15)
	.cfi_def_cfa_offset 328
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,160(%r11)
	lg	%r1,160(%r11)
	lg	%r10,0(%r1)
	lg	%r1,160(%r11)
	lg	%r9,8(%r1)
	lg	%r1,160(%r11)
	lgr	%r2,%r1
	brasl	%r14,_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE19_M_get_Tp_allocatorEv
	lgr	%r1,%r2
	lgr	%r4,%r1
	lgr	%r3,%r9
	lgr	%r2,%r10
	brasl	%r14,_ZSt8_DestroyIPSt10shared_ptrI8hittableES2_EvT_S4_RSaIT0_E
	lg	%r1,160(%r11)
	lgr	%r2,%r1
	brasl	%r14,_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED2Ev
	nopr	%r0
	lmg	%r9,%r15,240(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_restore 10
	.cfi_restore 9
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE4274:
	.section	.gcc_except_table
.LLSDA4274:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4274-.LLSDACSB4274
.LLSDACSB4274:
.LLSDACSE4274:
	.section	.text._ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED2Ev,"axG",@progbits,_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED5Ev,comdat
	.size	_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED2Ev, .-_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED2Ev
	.weak	_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED1Ev
	.set	_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED1Ev,_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED2Ev
	.section	.text._ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_,"axG",@progbits,_ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_,comdat
	.align	8
	.weak	_ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_
	.type	_ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_, @function
_ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_:
.LFB4276:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA4276
	stmg	%r6,%r15,48(%r15)
	.cfi_offset 6, -112
	.cfi_offset 7, -104
	.cfi_offset 8, -96
	.cfi_offset 9, -88
	.cfi_offset 10, -80
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-200(%r15)
	.cfi_def_cfa_offset 360
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,176(%r11)
	stg	%r3,168(%r11)
	stg	%r4,160(%r11)
	ear	%r1,%a0
	sllg	%r1,%r1,32
	ear	%r1,%a1
	mvc	192(8,%r11),40(%r1)
	aghik	%r1,%r11,191
	lgr	%r2,%r1
	brasl	%r14,_ZNSaI6sphereEC1Ev
	lg	%r2,168(%r11)
	brasl	%r14,_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
	lgr	%r10,%r2
	lg	%r2,160(%r11)
	brasl	%r14,_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE
	lgr	%r3,%r2
	lg	%r1,176(%r11)
	aghik	%r2,%r11,191
	lgr	%r5,%r3
	lgr	%r4,%r10
	lgr	%r3,%r2
	lgr	%r2,%r1
.LEHB16:
	brasl	%r14,_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3dEESt10shared_ptrIT_ERKT0_DpOT1_
.LEHE16:
	aghik	%r1,%r11,191
	lgr	%r2,%r1
	brasl	%r14,_ZNSaI6sphereED1Ev
	ear	%r1,%a0
	sllg	%r1,%r1,32
	ear	%r1,%a1
	clc	192(8,%r11),40(%r1)
	je	.L140
	j	.L142
.L141:
	lgr	%r10,%r6
	aghik	%r1,%r11,191
	lgr	%r2,%r1
	brasl	%r14,_ZNSaI6sphereED1Ev
	lgr	%r1,%r10
	lgr	%r2,%r1
.LEHB17:
	brasl	%r14,_Unwind_Resume@PLT
.LEHE17:
.L142:
	brasl	%r14,__stack_chk_fail@PLT
.L140:
	lg	%r2,176(%r11)
	lmg	%r6,%r15,248(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_restore 10
	.cfi_restore 9
	.cfi_restore 8
	.cfi_restore 7
	.cfi_restore 6
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE4276:
	.section	.gcc_except_table
.LLSDA4276:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4276-.LLSDACSB4276
.LLSDACSB4276:
	.uleb128 .LEHB16-.LFB4276
	.uleb128 .LEHE16-.LEHB16
	.uleb128 .L141-.LFB4276
	.uleb128 0
	.uleb128 .LEHB17-.LFB4276
	.uleb128 .LEHE17-.LEHB17
	.uleb128 0
	.uleb128 0
.LLSDACSE4276:
	.section	.text._ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_,"axG",@progbits,_ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_,comdat
	.size	_ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_, .-_ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_
	.section	.text._ZNSt10shared_ptrI8hittableEC2I6spherevEEOS_IT_E,"axG",@progbits,_ZNSt10shared_ptrI8hittableEC5I6spherevEEOS_IT_E,comdat
	.align	8
	.weak	_ZNSt10shared_ptrI8hittableEC2I6spherevEEOS_IT_E
	.type	_ZNSt10shared_ptrI8hittableEC2I6spherevEEOS_IT_E, @function
_ZNSt10shared_ptrI8hittableEC2I6spherevEEOS_IT_E:
.LFB4278:
	.cfi_startproc
	stmg	%r10,%r15,80(%r15)
	.cfi_offset 10, -80
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-176(%r15)
	.cfi_def_cfa_offset 336
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,168(%r11)
	stg	%r3,160(%r11)
	lg	%r10,168(%r11)
	lg	%r2,160(%r11)
	brasl	%r14,_ZSt4moveIRSt10shared_ptrI6sphereEEONSt16remove_referenceIT_E4typeEOS5_
	lgr	%r1,%r2
	lgr	%r3,%r1
	lgr	%r2,%r10
	brasl	%r14,_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC2I6spherevEEOS_IT_LS2_2EE
	nopr	%r0
	lmg	%r10,%r15,256(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_restore 10
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE4278:
	.size	_ZNSt10shared_ptrI8hittableEC2I6spherevEEOS_IT_E, .-_ZNSt10shared_ptrI8hittableEC2I6spherevEEOS_IT_E
	.weak	_ZNSt10shared_ptrI8hittableEC1I6spherevEEOS_IT_E
	.set	_ZNSt10shared_ptrI8hittableEC1I6spherevEEOS_IT_E,_ZNSt10shared_ptrI8hittableEC2I6spherevEEOS_IT_E
	.section	.text._ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_,"axG",@progbits,_ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_,comdat
	.align	8
	.weak	_ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_
	.type	_ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_, @function
_ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_:
.LFB4283:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA4283
	stmg	%r6,%r15,48(%r15)
	.cfi_offset 6, -112
	.cfi_offset 7, -104
	.cfi_offset 8, -96
	.cfi_offset 9, -88
	.cfi_offset 10, -80
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-200(%r15)
	.cfi_def_cfa_offset 360
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,176(%r11)
	stg	%r3,168(%r11)
	stg	%r4,160(%r11)
	ear	%r1,%a0
	sllg	%r1,%r1,32
	ear	%r1,%a1
	mvc	192(8,%r11),40(%r1)
	aghik	%r1,%r11,191
	lgr	%r2,%r1
	brasl	%r14,_ZNSaI6sphereEC1Ev
	lg	%r2,168(%r11)
	brasl	%r14,_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
	lgr	%r10,%r2
	lg	%r2,160(%r11)
	brasl	%r14,_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE
	lgr	%r3,%r2
	lg	%r1,176(%r11)
	aghik	%r2,%r11,191
	lgr	%r5,%r3
	lgr	%r4,%r10
	lgr	%r3,%r2
	lgr	%r2,%r1
.LEHB18:
	brasl	%r14,_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3iEESt10shared_ptrIT_ERKT0_DpOT1_
.LEHE18:
	aghik	%r1,%r11,191
	lgr	%r2,%r1
	brasl	%r14,_ZNSaI6sphereED1Ev
	ear	%r1,%a0
	sllg	%r1,%r1,32
	ear	%r1,%a1
	clc	192(8,%r11),40(%r1)
	je	.L149
	j	.L151
.L150:
	lgr	%r10,%r6
	aghik	%r1,%r11,191
	lgr	%r2,%r1
	brasl	%r14,_ZNSaI6sphereED1Ev
	lgr	%r1,%r10
	lgr	%r2,%r1
.LEHB19:
	brasl	%r14,_Unwind_Resume@PLT
.LEHE19:
.L151:
	brasl	%r14,__stack_chk_fail@PLT
.L149:
	lg	%r2,176(%r11)
	lmg	%r6,%r15,248(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_restore 10
	.cfi_restore 9
	.cfi_restore 8
	.cfi_restore 7
	.cfi_restore 6
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE4283:
	.section	.gcc_except_table
.LLSDA4283:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4283-.LLSDACSB4283
.LLSDACSB4283:
	.uleb128 .LEHB18-.LFB4283
	.uleb128 .LEHE18-.LEHB18
	.uleb128 .L150-.LFB4283
	.uleb128 0
	.uleb128 .LEHB19-.LFB4283
	.uleb128 .LEHE19-.LEHB19
	.uleb128 0
	.uleb128 0
.LLSDACSE4283:
	.section	.text._ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_,"axG",@progbits,_ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_,comdat
	.size	_ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_, .-_ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_
	.section	.text._ZNSt25uniform_real_distributionIdE10param_typeC2Edd,"axG",@progbits,_ZNSt25uniform_real_distributionIdE10param_typeC5Edd,comdat
	.align	8
	.weak	_ZNSt25uniform_real_distributionIdE10param_typeC2Edd
	.type	_ZNSt25uniform_real_distributionIdE10param_typeC2Edd, @function
_ZNSt25uniform_real_distributionIdE10param_typeC2Edd:
.LFB4410:
	.cfi_startproc
	ldgr	%f6,%r11
	.cfi_register 11, 19
	ldgr	%f4,%r15
	.cfi_register 15, 18
	lay	%r15,-184(%r15)
	.cfi_def_cfa_offset 344
	cg	%r0,176(%r15)
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,176(%r11)
	std	%f0,168(%r11)
	std	%f2,160(%r11)
	lg	%r1,176(%r11)
	ld	%f0,168(%r11)
	std	%f0,0(%r1)
	lg	%r1,176(%r11)
	ld	%f0,160(%r11)
	std	%f0,8(%r1)
	nopr	%r0
	lgdr	%r15,%f4
	.cfi_restore 15
	.cfi_def_cfa 15, 160
	lgdr	%r11,%f6
	.cfi_restore 11
	br	%r14
	.cfi_endproc
.LFE4410:
	.size	_ZNSt25uniform_real_distributionIdE10param_typeC2Edd, .-_ZNSt25uniform_real_distributionIdE10param_typeC2Edd
	.weak	_ZNSt25uniform_real_distributionIdE10param_typeC1Edd
	.set	_ZNSt25uniform_real_distributionIdE10param_typeC1Edd,_ZNSt25uniform_real_distributionIdE10param_typeC2Edd
	.section	.text._ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC2Em,"axG",@progbits,_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC5Em,comdat
	.align	8
	.weak	_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC2Em
	.type	_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC2Em, @function
_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC2Em:
.LFB4413:
	.cfi_startproc
	stmg	%r11,%r15,88(%r15)
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-176(%r15)
	.cfi_def_cfa_offset 336
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,168(%r11)
	stg	%r3,160(%r11)
	lg	%r3,160(%r11)
	lg	%r2,168(%r11)
	brasl	%r14,_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE4seedEm
	nopr	%r0
	lmg	%r11,%r15,264(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE4413:
	.size	_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC2Em, .-_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC2Em
	.weak	_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC1Em
	.set	_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC1Em,_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC2Em
	.section	.text._ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEEEdRT_RKNS0_10param_typeE,"axG",@progbits,_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEEEdRT_RKNS0_10param_typeE,comdat
	.align	8
	.weak	_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEEEdRT_RKNS0_10param_typeE
	.type	_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEEEdRT_RKNS0_10param_typeE, @function
_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEEEdRT_RKNS0_10param_typeE:
.LFB4415:
	.cfi_startproc
	stmg	%r11,%r15,88(%r15)
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lgr	%r14,%r15
	lay	%r15,-216(%r15)
	.cfi_def_cfa_offset 376
	aghi	%r14,-16
	std	%f8,0(%r14)
	std	%f10,8(%r14)
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	.cfi_offset 24, -176
	.cfi_offset 25, -168
	stg	%r2,176(%r11)
	stg	%r3,168(%r11)
	stg	%r4,160(%r11)
	ear	%r1,%a0
	sllg	%r1,%r1,32
	ear	%r1,%a1
	mvc	192(8,%r11),40(%r1)
	aghik	%r1,%r11,184
	lg	%r3,168(%r11)
	lgr	%r2,%r1
	brasl	%r14,_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEdEC1ERS2_
	aghik	%r1,%r11,184
	lgr	%r2,%r1
	brasl	%r14,_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEdEclEv
	ldr	%f10,%f0
	lg	%r2,160(%r11)
	brasl	%r14,_ZNKSt25uniform_real_distributionIdE10param_type1bEv
	ldr	%f8,%f0
	lg	%r2,160(%r11)
	brasl	%r14,_ZNKSt25uniform_real_distributionIdE10param_type1aEv
	sdbr	%f8,%f0
	ldr	%f0,%f8
	ldr	%f8,%f10
	mdbr	%f8,%f0
	lg	%r2,160(%r11)
	brasl	%r14,_ZNKSt25uniform_real_distributionIdE10param_type1aEv
	adbr	%f0,%f8
	lgdr	%r2,%f0
	ear	%r1,%a0
	sllg	%r1,%r1,32
	ear	%r1,%a1
	clc	192(8,%r11),40(%r1)
	je	.L159
	brasl	%r14,__stack_chk_fail@PLT
.L159:
	ldgr	%f0,%r2
	ld	%f8,200(%r11)
	ld	%f10,208(%r11)
	lmg	%r11,%r15,304(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_restore 25
	.cfi_restore 24
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE4415:
	.size	_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEEEdRT_RKNS0_10param_typeE, .-_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEEEdRT_RKNS0_10param_typeE
	.section	.text._ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC2Ev,"axG",@progbits,_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC5Ev,comdat
	.align	8
	.weak	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC2Ev
	.type	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC2Ev, @function
_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC2Ev:
.LFB4427:
	.cfi_startproc
	stmg	%r11,%r15,88(%r15)
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-168(%r15)
	.cfi_def_cfa_offset 328
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,160(%r11)
	lg	%r1,160(%r11)
	mvghi	0(%r1),0
	lg	%r1,160(%r11)
	aghi	%r1,8
	lgr	%r2,%r1
	brasl	%r14,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1Ev
	nopr	%r0
	lmg	%r11,%r15,256(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE4427:
	.size	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC2Ev, .-_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC2Ev
	.weak	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC1Ev
	.set	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC1Ev,_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC2Ev
	.section	.text._ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv,"axG",@progbits,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv,comdat
	.align	8
	.weak	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv
	.type	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv, @function
_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv:
.LFB4429:
	.cfi_startproc
	stmg	%r11,%r15,88(%r15)
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-168(%r15)
	.cfi_def_cfa_offset 328
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,160(%r11)
	lg	%r1,160(%r11)
	aghi	%r1,8
	lghi	%r3,-1
	lgr	%r2,%r1
	brasl	%r14,_ZN9__gnu_cxxL27__exchange_and_add_dispatchEPii
	lgr	%r1,%r2
	xilf	%r1,1
	lpr	%r1,%r1
	ahi	%r1,-1
	srl	%r1,31
	llcr	%r1,%r1
	ltr	%r1,%r1
	je	.L165
	lg	%r1,160(%r11)
	lg	%r1,0(%r1)
	aghi	%r1,16
	lg	%r1,0(%r1)
	lg	%r2,160(%r11)
	basr	%r14,%r1
	lg	%r1,160(%r11)
	aghi	%r1,12
	lghi	%r3,-1
	lgr	%r2,%r1
	brasl	%r14,_ZN9__gnu_cxxL27__exchange_and_add_dispatchEPii
	lgr	%r1,%r2
	xilf	%r1,1
	lpr	%r1,%r1
	ahi	%r1,-1
	srl	%r1,31
	llcr	%r1,%r1
	ltr	%r1,%r1
	je	.L165
	lg	%r1,160(%r11)
	lg	%r1,0(%r1)
	aghi	%r1,24
	lg	%r1,0(%r1)
	lg	%r2,160(%r11)
	basr	%r14,%r1
.L165:
	nopr	%r0
	lmg	%r11,%r15,256(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE4429:
	.size	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv, .-_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv
	.section	.text._ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD2Ev,"axG",@progbits,_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD5Ev,comdat
	.align	8
	.weak	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD2Ev
	.type	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD2Ev, @function
_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD2Ev:
.LFB4432:
	.cfi_startproc
	stmg	%r11,%r15,88(%r15)
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-168(%r15)
	.cfi_def_cfa_offset 328
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,160(%r11)
	lg	%r2,160(%r11)
	brasl	%r14,_ZNSaISt10shared_ptrI8hittableEED2Ev
	nopr	%r0
	lmg	%r11,%r15,256(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE4432:
	.size	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD2Ev, .-_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD2Ev
	.weak	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD1Ev
	.set	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD1Ev,_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD2Ev
	.section	.text._ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED2Ev,"axG",@progbits,_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED5Ev,comdat
	.align	8
	.weak	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED2Ev
	.type	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED2Ev, @function
_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED2Ev:
.LFB4434:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA4434
	stmg	%r11,%r15,88(%r15)
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-168(%r15)
	.cfi_def_cfa_offset 328
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,160(%r11)
	lg	%r1,160(%r11)
	lg	%r3,0(%r1)
	lg	%r1,160(%r11)
	lg	%r1,16(%r1)
	lg	%r2,160(%r11)
	lg	%r2,0(%r2)
	sgr	%r1,%r2
	srag	%r1,%r1,4
	lgr	%r4,%r1
	lg	%r2,160(%r11)
	brasl	%r14,_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE13_M_deallocateEPS2_m
	lg	%r1,160(%r11)
	lgr	%r2,%r1
	brasl	%r14,_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD1Ev
	nopr	%r0
	lmg	%r11,%r15,256(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE4434:
	.section	.gcc_except_table
.LLSDA4434:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4434-.LLSDACSB4434
.LLSDACSB4434:
.LLSDACSE4434:
	.section	.text._ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED2Ev,"axG",@progbits,_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED5Ev,comdat
	.size	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED2Ev, .-_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED2Ev
	.weak	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED1Ev
	.set	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED1Ev,_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED2Ev
	.section	.text._ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE19_M_get_Tp_allocatorEv,"axG",@progbits,_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE19_M_get_Tp_allocatorEv,comdat
	.align	8
	.weak	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE19_M_get_Tp_allocatorEv
	.type	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE19_M_get_Tp_allocatorEv, @function
_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE19_M_get_Tp_allocatorEv:
.LFB4436:
	.cfi_startproc
	ldgr	%f2,%r11
	.cfi_register 11, 17
	ldgr	%f0,%r15
	.cfi_register 15, 16
	lay	%r15,-168(%r15)
	.cfi_def_cfa_offset 328
	cg	%r0,160(%r15)
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,160(%r11)
	lg	%r1,160(%r11)
	lgr	%r2,%r1
	lgdr	%r15,%f0
	.cfi_restore 15
	.cfi_def_cfa 15, 160
	lgdr	%r11,%f2
	.cfi_restore 11
	br	%r14
	.cfi_endproc
.LFE4436:
	.size	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE19_M_get_Tp_allocatorEv, .-_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE19_M_get_Tp_allocatorEv
	.section	.text._ZSt8_DestroyIPSt10shared_ptrI8hittableES2_EvT_S4_RSaIT0_E,"axG",@progbits,_ZSt8_DestroyIPSt10shared_ptrI8hittableES2_EvT_S4_RSaIT0_E,comdat
	.align	8
	.weak	_ZSt8_DestroyIPSt10shared_ptrI8hittableES2_EvT_S4_RSaIT0_E
	.type	_ZSt8_DestroyIPSt10shared_ptrI8hittableES2_EvT_S4_RSaIT0_E, @function
_ZSt8_DestroyIPSt10shared_ptrI8hittableES2_EvT_S4_RSaIT0_E:
.LFB4437:
	.cfi_startproc
	stmg	%r11,%r15,88(%r15)
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-184(%r15)
	.cfi_def_cfa_offset 344
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,176(%r11)
	stg	%r3,168(%r11)
	stg	%r4,160(%r11)
	lg	%r3,168(%r11)
	lg	%r2,176(%r11)
	brasl	%r14,_ZSt8_DestroyIPSt10shared_ptrI8hittableEEvT_S4_
	nopr	%r0
	lmg	%r11,%r15,272(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE4437:
	.size	_ZSt8_DestroyIPSt10shared_ptrI8hittableES2_EvT_S4_RSaIT0_E, .-_ZSt8_DestroyIPSt10shared_ptrI8hittableES2_EvT_S4_RSaIT0_E
	.section	.text._ZNSaI6sphereEC2Ev,"axG",@progbits,_ZNSaI6sphereEC5Ev,comdat
	.align	8
	.weak	_ZNSaI6sphereEC2Ev
	.type	_ZNSaI6sphereEC2Ev, @function
_ZNSaI6sphereEC2Ev:
.LFB4439:
	.cfi_startproc
	stmg	%r11,%r15,88(%r15)
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-168(%r15)
	.cfi_def_cfa_offset 328
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,160(%r11)
	lg	%r2,160(%r11)
	brasl	%r14,_ZN9__gnu_cxx13new_allocatorI6sphereEC2Ev
	nopr	%r0
	lmg	%r11,%r15,256(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE4439:
	.size	_ZNSaI6sphereEC2Ev, .-_ZNSaI6sphereEC2Ev
	.weak	_ZNSaI6sphereEC1Ev
	.set	_ZNSaI6sphereEC1Ev,_ZNSaI6sphereEC2Ev
	.section	.text._ZNSaI6sphereED2Ev,"axG",@progbits,_ZNSaI6sphereED5Ev,comdat
	.align	8
	.weak	_ZNSaI6sphereED2Ev
	.type	_ZNSaI6sphereED2Ev, @function
_ZNSaI6sphereED2Ev:
.LFB4442:
	.cfi_startproc
	stmg	%r11,%r15,88(%r15)
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-168(%r15)
	.cfi_def_cfa_offset 328
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,160(%r11)
	lg	%r2,160(%r11)
	brasl	%r14,_ZN9__gnu_cxx13new_allocatorI6sphereED2Ev
	nopr	%r0
	lmg	%r11,%r15,256(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE4442:
	.size	_ZNSaI6sphereED2Ev, .-_ZNSaI6sphereED2Ev
	.weak	_ZNSaI6sphereED1Ev
	.set	_ZNSaI6sphereED1Ev,_ZNSaI6sphereED2Ev
	.section	.text._ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE,"axG",@progbits,_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE,comdat
	.align	8
	.weak	_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
	.type	_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE, @function
_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE:
.LFB4444:
	.cfi_startproc
	ldgr	%f2,%r11
	.cfi_register 11, 17
	ldgr	%f0,%r15
	.cfi_register 15, 16
	lay	%r15,-168(%r15)
	.cfi_def_cfa_offset 328
	cg	%r0,160(%r15)
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,160(%r11)
	lg	%r1,160(%r11)
	lgr	%r2,%r1
	lgdr	%r15,%f0
	.cfi_restore 15
	.cfi_def_cfa 15, 160
	lgdr	%r11,%f2
	.cfi_restore 11
	br	%r14
	.cfi_endproc
.LFE4444:
	.size	_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE, .-_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
	.section	.text._ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE,"axG",@progbits,_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE,comdat
	.align	8
	.weak	_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE
	.type	_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE, @function
_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE:
.LFB4445:
	.cfi_startproc
	ldgr	%f2,%r11
	.cfi_register 11, 17
	ldgr	%f0,%r15
	.cfi_register 15, 16
	lay	%r15,-168(%r15)
	.cfi_def_cfa_offset 328
	cg	%r0,160(%r15)
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,160(%r11)
	lg	%r1,160(%r11)
	lgr	%r2,%r1
	lgdr	%r15,%f0
	.cfi_restore 15
	.cfi_def_cfa 15, 160
	lgdr	%r11,%f2
	.cfi_restore 11
	br	%r14
	.cfi_endproc
.LFE4445:
	.size	_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE, .-_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE
	.section	.text._ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3dEESt10shared_ptrIT_ERKT0_DpOT1_,"axG",@progbits,_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3dEESt10shared_ptrIT_ERKT0_DpOT1_,comdat
	.align	8
	.weak	_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3dEESt10shared_ptrIT_ERKT0_DpOT1_
	.type	_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3dEESt10shared_ptrIT_ERKT0_DpOT1_, @function
_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3dEESt10shared_ptrIT_ERKT0_DpOT1_:
.LFB4446:
	.cfi_startproc
	stmg	%r9,%r15,72(%r15)
	.cfi_offset 9, -88
	.cfi_offset 10, -80
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-192(%r15)
	.cfi_def_cfa_offset 352
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,184(%r11)
	stg	%r3,176(%r11)
	stg	%r4,168(%r11)
	stg	%r5,160(%r11)
	lg	%r10,176(%r11)
	lg	%r2,168(%r11)
	brasl	%r14,_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
	lgr	%r9,%r2
	lg	%r2,160(%r11)
	brasl	%r14,_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE
	lgr	%r1,%r2
	lgr	%r5,%r1
	lgr	%r4,%r9
	lgr	%r3,%r10
	lg	%r2,184(%r11)
	brasl	%r14,_ZNSt10shared_ptrI6sphereEC1ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	lg	%r2,184(%r11)
	lmg	%r9,%r15,264(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_restore 10
	.cfi_restore 9
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE4446:
	.size	_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3dEESt10shared_ptrIT_ERKT0_DpOT1_, .-_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3dEESt10shared_ptrIT_ERKT0_DpOT1_
	.section	.text._ZSt4moveIRSt10shared_ptrI6sphereEEONSt16remove_referenceIT_E4typeEOS5_,"axG",@progbits,_ZSt4moveIRSt10shared_ptrI6sphereEEONSt16remove_referenceIT_E4typeEOS5_,comdat
	.align	8
	.weak	_ZSt4moveIRSt10shared_ptrI6sphereEEONSt16remove_referenceIT_E4typeEOS5_
	.type	_ZSt4moveIRSt10shared_ptrI6sphereEEONSt16remove_referenceIT_E4typeEOS5_, @function
_ZSt4moveIRSt10shared_ptrI6sphereEEONSt16remove_referenceIT_E4typeEOS5_:
.LFB4450:
	.cfi_startproc
	ldgr	%f2,%r11
	.cfi_register 11, 17
	ldgr	%f0,%r15
	.cfi_register 15, 16
	lay	%r15,-168(%r15)
	.cfi_def_cfa_offset 328
	cg	%r0,160(%r15)
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,160(%r11)
	lg	%r1,160(%r11)
	lgr	%r2,%r1
	lgdr	%r15,%f0
	.cfi_restore 15
	.cfi_def_cfa 15, 160
	lgdr	%r11,%f2
	.cfi_restore 11
	br	%r14
	.cfi_endproc
.LFE4450:
	.size	_ZSt4moveIRSt10shared_ptrI6sphereEEONSt16remove_referenceIT_E4typeEOS5_, .-_ZSt4moveIRSt10shared_ptrI6sphereEEONSt16remove_referenceIT_E4typeEOS5_
	.section	.text._ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC2I6spherevEEOS_IT_LS2_2EE,"axG",@progbits,_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC5I6spherevEEOS_IT_LS2_2EE,comdat
	.align	8
	.weak	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC2I6spherevEEOS_IT_LS2_2EE
	.type	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC2I6spherevEEOS_IT_LS2_2EE, @function
_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC2I6spherevEEOS_IT_LS2_2EE:
.LFB4452:
	.cfi_startproc
	stmg	%r11,%r15,88(%r15)
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-176(%r15)
	.cfi_def_cfa_offset 336
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,168(%r11)
	stg	%r3,160(%r11)
	lg	%r1,160(%r11)
	lg	%r2,0(%r1)
	lg	%r1,168(%r11)
	stg	%r2,0(%r1)
	lg	%r1,168(%r11)
	aghi	%r1,8
	lgr	%r2,%r1
	brasl	%r14,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1Ev
	lg	%r1,168(%r11)
	aghi	%r1,8
	lg	%r2,160(%r11)
	aghi	%r2,8
	lgr	%r3,%r2
	lgr	%r2,%r1
	brasl	%r14,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EE7_M_swapERS2_
	lg	%r1,160(%r11)
	mvghi	0(%r1),0
	nopr	%r0
	lmg	%r11,%r15,264(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE4452:
	.size	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC2I6spherevEEOS_IT_LS2_2EE, .-_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC2I6spherevEEOS_IT_LS2_2EE
	.weak	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC1I6spherevEEOS_IT_LS2_2EE
	.set	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC1I6spherevEEOS_IT_LS2_2EE,_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC2I6spherevEEOS_IT_LS2_2EE
	.section	.text._ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE,"axG",@progbits,_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE,comdat
	.align	8
	.weak	_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE
	.type	_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE, @function
_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE:
.LFB4458:
	.cfi_startproc
	ldgr	%f2,%r11
	.cfi_register 11, 17
	ldgr	%f0,%r15
	.cfi_register 15, 16
	lay	%r15,-168(%r15)
	.cfi_def_cfa_offset 328
	cg	%r0,160(%r15)
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,160(%r11)
	lg	%r1,160(%r11)
	lgr	%r2,%r1
	lgdr	%r15,%f0
	.cfi_restore 15
	.cfi_def_cfa 15, 160
	lgdr	%r11,%f2
	.cfi_restore 11
	br	%r14
	.cfi_endproc
.LFE4458:
	.size	_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE, .-_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE
	.section	.text._ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3iEESt10shared_ptrIT_ERKT0_DpOT1_,"axG",@progbits,_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3iEESt10shared_ptrIT_ERKT0_DpOT1_,comdat
	.align	8
	.weak	_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3iEESt10shared_ptrIT_ERKT0_DpOT1_
	.type	_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3iEESt10shared_ptrIT_ERKT0_DpOT1_, @function
_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3iEESt10shared_ptrIT_ERKT0_DpOT1_:
.LFB4459:
	.cfi_startproc
	stmg	%r9,%r15,72(%r15)
	.cfi_offset 9, -88
	.cfi_offset 10, -80
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-192(%r15)
	.cfi_def_cfa_offset 352
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,184(%r11)
	stg	%r3,176(%r11)
	stg	%r4,168(%r11)
	stg	%r5,160(%r11)
	lg	%r10,176(%r11)
	lg	%r2,168(%r11)
	brasl	%r14,_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
	lgr	%r9,%r2
	lg	%r2,160(%r11)
	brasl	%r14,_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE
	lgr	%r1,%r2
	lgr	%r5,%r1
	lgr	%r4,%r9
	lgr	%r3,%r10
	lg	%r2,184(%r11)
	brasl	%r14,_ZNSt10shared_ptrI6sphereEC1ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	lg	%r2,184(%r11)
	lmg	%r9,%r15,264(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_restore 10
	.cfi_restore 9
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE4459:
	.size	_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3iEESt10shared_ptrIT_ERKT0_DpOT1_, .-_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3iEESt10shared_ptrIT_ERKT0_DpOT1_
	.section	.text._ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE4seedEm,"axG",@progbits,_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE4seedEm,comdat
	.align	8
	.weak	_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE4seedEm
	.type	_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE4seedEm, @function
_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE4seedEm:
.LFB4514:
	.cfi_startproc
	stmg	%r11,%r15,88(%r15)
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-192(%r15)
	.cfi_def_cfa_offset 352
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,168(%r11)
	stg	%r3,160(%r11)
	lg	%r2,160(%r11)
	brasl	%r14,_ZNSt8__detail5__modImLm4294967296ELm1ELm0EEET_S1_
	lg	%r1,168(%r11)
	stg	%r2,0(%r1)
	mvghi	176(%r11),1
.L202:
	lg	%r1,176(%r11)
	clgfi	%r1,623
	jh	.L201
	lg	%r1,176(%r11)
	aghik	%r2,%r1,-1
	lg	%r1,168(%r11)
	sllg	%r2,%r2,3
	lg	%r1,0(%r2,%r1)
	stg	%r1,184(%r11)
	lg	%r1,184(%r11)
	srlg	%r1,%r1,30
	xg	%r1,184(%r11)
	stg	%r1,184(%r11)
	lgfi	%r1,1812433253
	msg	%r1,184(%r11)
	stg	%r1,184(%r11)
	lg	%r2,176(%r11)
	brasl	%r14,_ZNSt8__detail5__modImLm624ELm1ELm0EEET_S1_
	lgr	%r1,%r2
	ag	%r1,184(%r11)
	stg	%r1,184(%r11)
	lg	%r2,184(%r11)
	brasl	%r14,_ZNSt8__detail5__modImLm4294967296ELm1ELm0EEET_S1_
	lgr	%r3,%r2
	lg	%r1,168(%r11)
	lg	%r2,176(%r11)
	sllg	%r2,%r2,3
	stg	%r3,0(%r2,%r1)
	agsi	176(%r11),1
	j	.L202
.L201:
	lg	%r1,168(%r11)
	lay	%r1,4992(%r1)
	mvghi	0(%r1),624
	nopr	%r0
	lmg	%r11,%r15,280(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE4514:
	.size	_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE4seedEm, .-_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE4seedEm
	.section	.text._ZNSt8__detail8_AdaptorISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEdEC2ERS2_,"axG",@progbits,_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEdEC5ERS2_,comdat
	.align	8
	.weak	_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEdEC2ERS2_
	.type	_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEdEC2ERS2_, @function
_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEdEC2ERS2_:
.LFB4516:
	.cfi_startproc
	ldgr	%f2,%r11
	.cfi_register 11, 17
	ldgr	%f0,%r15
	.cfi_register 15, 16
	lay	%r15,-176(%r15)
	.cfi_def_cfa_offset 336
	cg	%r0,168(%r15)
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,168(%r11)
	stg	%r3,160(%r11)
	lg	%r1,168(%r11)
	lg	%r2,160(%r11)
	stg	%r2,0(%r1)
	nopr	%r0
	lgdr	%r15,%f0
	.cfi_restore 15
	.cfi_def_cfa 15, 160
	lgdr	%r11,%f2
	.cfi_restore 11
	br	%r14
	.cfi_endproc
.LFE4516:
	.size	_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEdEC2ERS2_, .-_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEdEC2ERS2_
	.weak	_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEdEC1ERS2_
	.set	_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEdEC1ERS2_,_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEdEC2ERS2_
	.section	.text._ZNSt8__detail8_AdaptorISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEdEclEv,"axG",@progbits,_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEdEclEv,comdat
	.align	8
	.weak	_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEdEclEv
	.type	_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEdEclEv, @function
_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEdEclEv:
.LFB4518:
	.cfi_startproc
	stmg	%r11,%r15,88(%r15)
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-168(%r15)
	.cfi_def_cfa_offset 328
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,160(%r11)
	lg	%r1,160(%r11)
	lg	%r1,0(%r1)
	lgr	%r2,%r1
	brasl	%r14,_ZSt18generate_canonicalIdLm53ESt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEET_RT1_
	lgdr	%r1,%f0
	ldgr	%f0,%r1
	lmg	%r11,%r15,256(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE4518:
	.size	_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEdEclEv, .-_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEdEclEv
	.section	.text._ZNKSt25uniform_real_distributionIdE10param_type1bEv,"axG",@progbits,_ZNKSt25uniform_real_distributionIdE10param_type1bEv,comdat
	.align	8
	.weak	_ZNKSt25uniform_real_distributionIdE10param_type1bEv
	.type	_ZNKSt25uniform_real_distributionIdE10param_type1bEv, @function
_ZNKSt25uniform_real_distributionIdE10param_type1bEv:
.LFB4519:
	.cfi_startproc
	ldgr	%f4,%r11
	.cfi_register 11, 18
	ldgr	%f2,%r15
	.cfi_register 15, 17
	lay	%r15,-168(%r15)
	.cfi_def_cfa_offset 328
	cg	%r0,160(%r15)
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,160(%r11)
	lg	%r1,160(%r11)
	lg	%r1,8(%r1)
	ldgr	%f0,%r1
	lgdr	%r15,%f2
	.cfi_restore 15
	.cfi_def_cfa 15, 160
	lgdr	%r11,%f4
	.cfi_restore 11
	br	%r14
	.cfi_endproc
.LFE4519:
	.size	_ZNKSt25uniform_real_distributionIdE10param_type1bEv, .-_ZNKSt25uniform_real_distributionIdE10param_type1bEv
	.section	.text._ZNKSt25uniform_real_distributionIdE10param_type1aEv,"axG",@progbits,_ZNKSt25uniform_real_distributionIdE10param_type1aEv,comdat
	.align	8
	.weak	_ZNKSt25uniform_real_distributionIdE10param_type1aEv
	.type	_ZNKSt25uniform_real_distributionIdE10param_type1aEv, @function
_ZNKSt25uniform_real_distributionIdE10param_type1aEv:
.LFB4520:
	.cfi_startproc
	ldgr	%f4,%r11
	.cfi_register 11, 18
	ldgr	%f2,%r15
	.cfi_register 15, 17
	lay	%r15,-168(%r15)
	.cfi_def_cfa_offset 328
	cg	%r0,160(%r15)
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,160(%r11)
	lg	%r1,160(%r11)
	lg	%r1,0(%r1)
	ldgr	%f0,%r1
	lgdr	%r15,%f2
	.cfi_restore 15
	.cfi_def_cfa 15, 160
	lgdr	%r11,%f4
	.cfi_restore 11
	br	%r14
	.cfi_endproc
.LFE4520:
	.size	_ZNKSt25uniform_real_distributionIdE10param_type1aEv, .-_ZNKSt25uniform_real_distributionIdE10param_type1aEv
	.section	.text._ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2Ev,"axG",@progbits,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC5Ev,comdat
	.align	8
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2Ev
	.type	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2Ev, @function
_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2Ev:
.LFB4526:
	.cfi_startproc
	ldgr	%f2,%r11
	.cfi_register 11, 17
	ldgr	%f0,%r15
	.cfi_register 15, 16
	lay	%r15,-168(%r15)
	.cfi_def_cfa_offset 328
	cg	%r0,160(%r15)
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,160(%r11)
	lg	%r1,160(%r11)
	mvghi	0(%r1),0
	nopr	%r0
	lgdr	%r15,%f0
	.cfi_restore 15
	.cfi_def_cfa 15, 160
	lgdr	%r11,%f2
	.cfi_restore 11
	br	%r14
	.cfi_endproc
.LFE4526:
	.size	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2Ev, .-_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2Ev
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1Ev
	.set	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1Ev,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2Ev
	.section	.text._ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,"axG",@progbits,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,comdat
	.align	8
	.weak	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.type	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, @function
_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv:
.LFB4528:
	.cfi_startproc
	stmg	%r11,%r15,88(%r15)
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-168(%r15)
	.cfi_def_cfa_offset 328
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,160(%r11)
	lg	%r1,160(%r11)
	ltgr	%r1,%r1
	je	.L219
	lg	%r1,160(%r11)
	lg	%r1,0(%r1)
	aghi	%r1,8
	lg	%r1,0(%r1)
	lg	%r2,160(%r11)
	basr	%r14,%r1
.L219:
	nopr	%r0
	lmg	%r11,%r15,256(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE4528:
	.size	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, .-_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.section	.text._ZNSaISt10shared_ptrI8hittableEED2Ev,"axG",@progbits,_ZNSaISt10shared_ptrI8hittableEED5Ev,comdat
	.align	8
	.weak	_ZNSaISt10shared_ptrI8hittableEED2Ev
	.type	_ZNSaISt10shared_ptrI8hittableEED2Ev, @function
_ZNSaISt10shared_ptrI8hittableEED2Ev:
.LFB4530:
	.cfi_startproc
	stmg	%r11,%r15,88(%r15)
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-168(%r15)
	.cfi_def_cfa_offset 328
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,160(%r11)
	lg	%r2,160(%r11)
	brasl	%r14,_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED2Ev
	nopr	%r0
	lmg	%r11,%r15,256(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE4530:
	.size	_ZNSaISt10shared_ptrI8hittableEED2Ev, .-_ZNSaISt10shared_ptrI8hittableEED2Ev
	.weak	_ZNSaISt10shared_ptrI8hittableEED1Ev
	.set	_ZNSaISt10shared_ptrI8hittableEED1Ev,_ZNSaISt10shared_ptrI8hittableEED2Ev
	.section	.text._ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE13_M_deallocateEPS2_m,"axG",@progbits,_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE13_M_deallocateEPS2_m,comdat
	.align	8
	.weak	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE13_M_deallocateEPS2_m
	.type	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE13_M_deallocateEPS2_m, @function
_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE13_M_deallocateEPS2_m:
.LFB4532:
	.cfi_startproc
	stmg	%r11,%r15,88(%r15)
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-184(%r15)
	.cfi_def_cfa_offset 344
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,176(%r11)
	stg	%r3,168(%r11)
	stg	%r4,160(%r11)
	lg	%r1,168(%r11)
	ltgr	%r1,%r1
	je	.L225
	lg	%r1,176(%r11)
	lg	%r4,160(%r11)
	lg	%r3,168(%r11)
	lgr	%r2,%r1
	brasl	%r14,_ZNSt16allocator_traitsISaISt10shared_ptrI8hittableEEE10deallocateERS3_PS2_m
.L225:
	nopr	%r0
	lmg	%r11,%r15,272(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE4532:
	.size	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE13_M_deallocateEPS2_m, .-_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE13_M_deallocateEPS2_m
	.section	.text._ZSt8_DestroyIPSt10shared_ptrI8hittableEEvT_S4_,"axG",@progbits,_ZSt8_DestroyIPSt10shared_ptrI8hittableEEvT_S4_,comdat
	.align	8
	.weak	_ZSt8_DestroyIPSt10shared_ptrI8hittableEEvT_S4_
	.type	_ZSt8_DestroyIPSt10shared_ptrI8hittableEEvT_S4_, @function
_ZSt8_DestroyIPSt10shared_ptrI8hittableEEvT_S4_:
.LFB4533:
	.cfi_startproc
	stmg	%r11,%r15,88(%r15)
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-176(%r15)
	.cfi_def_cfa_offset 336
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,168(%r11)
	stg	%r3,160(%r11)
	lg	%r3,160(%r11)
	lg	%r2,168(%r11)
	brasl	%r14,_ZNSt12_Destroy_auxILb0EE9__destroyIPSt10shared_ptrI8hittableEEEvT_S6_
	nopr	%r0
	lmg	%r11,%r15,264(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE4533:
	.size	_ZSt8_DestroyIPSt10shared_ptrI8hittableEEvT_S4_, .-_ZSt8_DestroyIPSt10shared_ptrI8hittableEEvT_S4_
	.section	.text._ZN9__gnu_cxx13new_allocatorI6sphereEC2Ev,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorI6sphereEC5Ev,comdat
	.align	8
	.weak	_ZN9__gnu_cxx13new_allocatorI6sphereEC2Ev
	.type	_ZN9__gnu_cxx13new_allocatorI6sphereEC2Ev, @function
_ZN9__gnu_cxx13new_allocatorI6sphereEC2Ev:
.LFB4535:
	.cfi_startproc
	ldgr	%f2,%r11
	.cfi_register 11, 17
	ldgr	%f0,%r15
	.cfi_register 15, 16
	lay	%r15,-168(%r15)
	.cfi_def_cfa_offset 328
	cg	%r0,160(%r15)
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,160(%r11)
	nopr	%r0
	lgdr	%r15,%f0
	.cfi_restore 15
	.cfi_def_cfa 15, 160
	lgdr	%r11,%f2
	.cfi_restore 11
	br	%r14
	.cfi_endproc
.LFE4535:
	.size	_ZN9__gnu_cxx13new_allocatorI6sphereEC2Ev, .-_ZN9__gnu_cxx13new_allocatorI6sphereEC2Ev
	.weak	_ZN9__gnu_cxx13new_allocatorI6sphereEC1Ev
	.set	_ZN9__gnu_cxx13new_allocatorI6sphereEC1Ev,_ZN9__gnu_cxx13new_allocatorI6sphereEC2Ev
	.section	.text._ZN9__gnu_cxx13new_allocatorI6sphereED2Ev,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorI6sphereED5Ev,comdat
	.align	8
	.weak	_ZN9__gnu_cxx13new_allocatorI6sphereED2Ev
	.type	_ZN9__gnu_cxx13new_allocatorI6sphereED2Ev, @function
_ZN9__gnu_cxx13new_allocatorI6sphereED2Ev:
.LFB4538:
	.cfi_startproc
	ldgr	%f2,%r11
	.cfi_register 11, 17
	ldgr	%f0,%r15
	.cfi_register 15, 16
	lay	%r15,-168(%r15)
	.cfi_def_cfa_offset 328
	cg	%r0,160(%r15)
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,160(%r11)
	nopr	%r0
	lgdr	%r15,%f0
	.cfi_restore 15
	.cfi_def_cfa 15, 160
	lgdr	%r11,%f2
	.cfi_restore 11
	br	%r14
	.cfi_endproc
.LFE4538:
	.size	_ZN9__gnu_cxx13new_allocatorI6sphereED2Ev, .-_ZN9__gnu_cxx13new_allocatorI6sphereED2Ev
	.weak	_ZN9__gnu_cxx13new_allocatorI6sphereED1Ev
	.set	_ZN9__gnu_cxx13new_allocatorI6sphereED1Ev,_ZN9__gnu_cxx13new_allocatorI6sphereED2Ev
	.section	.text._ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,"axG",@progbits,_ZNSt10shared_ptrI6sphereEC5ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,comdat
	.align	8
	.weak	_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.type	_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_, @function
_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_:
.LFB4541:
	.cfi_startproc
	stmg	%r9,%r15,72(%r15)
	.cfi_offset 9, -88
	.cfi_offset 10, -80
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-192(%r15)
	.cfi_def_cfa_offset 352
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,184(%r11)
	stg	%r3,176(%r11)
	stg	%r4,168(%r11)
	stg	%r5,160(%r11)
	lg	%r10,184(%r11)
	lg	%r2,168(%r11)
	brasl	%r14,_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
	lgr	%r9,%r2
	lg	%r2,160(%r11)
	brasl	%r14,_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE
	lgr	%r1,%r2
	lgr	%r5,%r1
	lgr	%r4,%r9
	lg	%r3,176(%r11)
	lgr	%r2,%r10
	brasl	%r14,_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	nopr	%r0
	lmg	%r9,%r15,264(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_restore 10
	.cfi_restore 9
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE4541:
	.size	_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_, .-_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.weak	_ZNSt10shared_ptrI6sphereEC1ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.set	_ZNSt10shared_ptrI6sphereEC1ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.section	.text._ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EE7_M_swapERS2_,"axG",@progbits,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EE7_M_swapERS2_,comdat
	.align	8
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EE7_M_swapERS2_
	.type	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EE7_M_swapERS2_, @function
_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EE7_M_swapERS2_:
.LFB4546:
	.cfi_startproc
	ldgr	%f2,%r11
	.cfi_register 11, 17
	ldgr	%f0,%r15
	.cfi_register 15, 16
	lay	%r15,-184(%r15)
	.cfi_def_cfa_offset 344
	cg	%r0,176(%r15)
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,168(%r11)
	stg	%r3,160(%r11)
	lg	%r1,160(%r11)
	lg	%r1,0(%r1)
	stg	%r1,176(%r11)
	lg	%r1,168(%r11)
	lg	%r2,0(%r1)
	lg	%r1,160(%r11)
	stg	%r2,0(%r1)
	lg	%r1,168(%r11)
	lg	%r2,176(%r11)
	stg	%r2,0(%r1)
	nopr	%r0
	lgdr	%r15,%f0
	.cfi_restore 15
	.cfi_def_cfa 15, 160
	lgdr	%r11,%f2
	.cfi_restore 11
	br	%r14
	.cfi_endproc
.LFE4546:
	.size	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EE7_M_swapERS2_, .-_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EE7_M_swapERS2_
	.section	.text._ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,"axG",@progbits,_ZNSt10shared_ptrI6sphereEC5ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,comdat
	.align	8
	.weak	_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.type	_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_, @function
_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_:
.LFB4548:
	.cfi_startproc
	stmg	%r9,%r15,72(%r15)
	.cfi_offset 9, -88
	.cfi_offset 10, -80
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-192(%r15)
	.cfi_def_cfa_offset 352
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,184(%r11)
	stg	%r3,176(%r11)
	stg	%r4,168(%r11)
	stg	%r5,160(%r11)
	lg	%r10,184(%r11)
	lg	%r2,168(%r11)
	brasl	%r14,_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
	lgr	%r9,%r2
	lg	%r2,160(%r11)
	brasl	%r14,_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE
	lgr	%r1,%r2
	lgr	%r5,%r1
	lgr	%r4,%r9
	lg	%r3,176(%r11)
	lgr	%r2,%r10
	brasl	%r14,_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	nopr	%r0
	lmg	%r9,%r15,264(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_restore 10
	.cfi_restore 9
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE4548:
	.size	_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_, .-_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.weak	_ZNSt10shared_ptrI6sphereEC1ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.set	_ZNSt10shared_ptrI6sphereEC1ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.section	.text._ZNSt8__detail5__modImLm4294967296ELm1ELm0EEET_S1_,"axG",@progbits,_ZNSt8__detail5__modImLm4294967296ELm1ELm0EEET_S1_,comdat
	.align	8
	.weak	_ZNSt8__detail5__modImLm4294967296ELm1ELm0EEET_S1_
	.type	_ZNSt8__detail5__modImLm4294967296ELm1ELm0EEET_S1_, @function
_ZNSt8__detail5__modImLm4294967296ELm1ELm0EEET_S1_:
.LFB4587:
	.cfi_startproc
	stmg	%r11,%r15,88(%r15)
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-168(%r15)
	.cfi_def_cfa_offset 328
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,160(%r11)
	lg	%r2,160(%r11)
	brasl	%r14,_ZNSt8__detail4_ModImLm4294967296ELm1ELm0ELb1ELb1EE6__calcEm
	lgr	%r1,%r2
	lgr	%r2,%r1
	lmg	%r11,%r15,256(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE4587:
	.size	_ZNSt8__detail5__modImLm4294967296ELm1ELm0EEET_S1_, .-_ZNSt8__detail5__modImLm4294967296ELm1ELm0EEET_S1_
	.section	.text._ZNSt8__detail5__modImLm624ELm1ELm0EEET_S1_,"axG",@progbits,_ZNSt8__detail5__modImLm624ELm1ELm0EEET_S1_,comdat
	.align	8
	.weak	_ZNSt8__detail5__modImLm624ELm1ELm0EEET_S1_
	.type	_ZNSt8__detail5__modImLm624ELm1ELm0EEET_S1_, @function
_ZNSt8__detail5__modImLm624ELm1ELm0EEET_S1_:
.LFB4588:
	.cfi_startproc
	stmg	%r11,%r15,88(%r15)
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-168(%r15)
	.cfi_def_cfa_offset 328
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,160(%r11)
	lg	%r2,160(%r11)
	brasl	%r14,_ZNSt8__detail4_ModImLm624ELm1ELm0ELb1ELb1EE6__calcEm
	lgr	%r1,%r2
	lgr	%r2,%r1
	lmg	%r11,%r15,256(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE4588:
	.size	_ZNSt8__detail5__modImLm624ELm1ELm0EEET_S1_, .-_ZNSt8__detail5__modImLm624ELm1ELm0EEET_S1_
	.section	.text._ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE3minEv,"axG",@progbits,_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE3minEv,comdat
	.align	8
	.weak	_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE3minEv
	.type	_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE3minEv, @function
_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE3minEv:
.LFB4592:
	.cfi_startproc
	ldgr	%f0,%r11
	.cfi_register 11, 16
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	lghi	%r1,0
	lgr	%r2,%r1
	lgdr	%r11,%f0
	.cfi_restore 11
	.cfi_def_cfa_register 15
	br	%r14
	.cfi_endproc
.LFE4592:
	.size	_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE3minEv, .-_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE3minEv
	.section	.text._ZSt18generate_canonicalIdLm53ESt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEET_RT1_,"axG",@progbits,_ZSt18generate_canonicalIdLm53ESt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEET_RT1_,comdat
	.align	8
	.weak	_ZSt18generate_canonicalIdLm53ESt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEET_RT1_
	.type	_ZSt18generate_canonicalIdLm53ESt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEET_RT1_, @function
_ZSt18generate_canonicalIdLm53ESt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEET_RT1_:
.LFB4589:
	.cfi_startproc
	stmg	%r10,%r15,80(%r15)
	.cfi_offset 10, -80
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	larl	%r13,.L254
	lay	%r15,-240(%r15)
	.cfi_def_cfa_offset 400
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,160(%r11)
	mvghi	200(%r11),53
	ld	%f0,.L255-.L254(%r13)
	ld	%f2,.L255-.L254+8(%r13)
	std	%f0,224(%r11)
	std	%f2,232(%r11)
	mvghi	208(%r11),32
	mvghi	216(%r11),2
	lzdr	%f0
	std	%f0,176(%r11)
	ld	%f0,.L256-.L254(%r13)
	std	%f0,184(%r11)
	mvghi	192(%r11),2
.L250:
	lg	%r1,192(%r11)
	ltgr	%r1,%r1
	je	.L249
	lg	%r2,160(%r11)
	brasl	%r14,_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEclEv
	lgr	%r10,%r2
	brasl	%r14,_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE3minEv
	lgr	%r1,%r2
	sgrk	%r1,%r10,%r1
	cdlgbr	%f0,0,%r1,0
	mdb	%f0,184(%r11)
	adb	%f0,176(%r11)
	std	%f0,176(%r11)
	lxdb	%f0,184(%r11)
	ld	%f4,.L255-.L254(%r13)
	ld	%f6,.L255-.L254+8(%r13)
	mxbr	%f0,%f4
	ldxbr	%f4,%f0
	ldr	%f0,%f4
	std	%f0,184(%r11)
	agsi	192(%r11),-1
	j	.L250
.L249:
	ld	%f0,176(%r11)
	ddb	%f0,184(%r11)
	std	%f0,168(%r11)
	lhi	%r1,1
	ld	%f0,168(%r11)
	cdb	%f0,.L256-.L254(%r13)
	jhe	.L251
	lhi	%r1,0
.L251:
	llgcr	%r1,%r1
	ltgr	%r1,%r1
	je	.L252
	ld	%f0,.L257-.L254(%r13)
	std	%f0,168(%r11)
.L252:
	lg	%r1,168(%r11)
	ldgr	%f0,%r1
	lmg	%r10,%r15,320(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_restore 10
	.cfi_def_cfa 15, 160
	br	%r14
	.section	.rodata._ZSt18generate_canonicalIdLm53ESt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEET_RT1_,"aG",@progbits,_ZSt18generate_canonicalIdLm53ESt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEET_RT1_,comdat
	.align	8
.L254:
.L255:
	.long	1075773440
	.long	0
	.long	0
	.long	0
.L257:
	.long	1072693247
	.long	4294967295
.L256:
	.long	1072693248
	.long	0
	.align	2
	.section	.text._ZSt18generate_canonicalIdLm53ESt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEET_RT1_,"axG",@progbits,_ZSt18generate_canonicalIdLm53ESt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEET_RT1_,comdat
	.cfi_endproc
.LFE4589:
	.size	_ZSt18generate_canonicalIdLm53ESt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEET_RT1_, .-_ZSt18generate_canonicalIdLm53ESt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEET_RT1_
	.section	.text._ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align	8
	.weak	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev
	.type	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev:
.LFB4599:
	.cfi_startproc
	ldgr	%f2,%r11
	.cfi_register 11, 17
	ldgr	%f0,%r15
	.cfi_register 15, 16
	lay	%r15,-168(%r15)
	.cfi_def_cfa_offset 328
	cg	%r0,160(%r15)
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,160(%r11)
	larl	%r2,_ZTVSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE+16
	lg	%r1,160(%r11)
	stg	%r2,0(%r1)
	nopr	%r0
	lgdr	%r15,%f0
	.cfi_restore 15
	.cfi_def_cfa 15, 160
	lgdr	%r11,%f2
	.cfi_restore 11
	br	%r14
	.cfi_endproc
.LFE4599:
	.size	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED1Ev
	.set	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED1Ev,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED0Ev,"axG",@progbits,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align	8
	.weak	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED0Ev
	.type	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED0Ev, @function
_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED0Ev:
.LFB4601:
	.cfi_startproc
	stmg	%r11,%r15,88(%r15)
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-168(%r15)
	.cfi_def_cfa_offset 328
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,160(%r11)
	lg	%r2,160(%r11)
	brasl	%r14,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED1Ev
	lghi	%r3,16
	lg	%r2,160(%r11)
	brasl	%r14,_ZdlPvm@PLT
	lmg	%r11,%r15,256(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE4601:
	.size	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED0Ev, .-_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED0Ev
	.section	.text._ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED2Ev,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED5Ev,comdat
	.align	8
	.weak	_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED2Ev
	.type	_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED2Ev, @function
_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED2Ev:
.LFB4603:
	.cfi_startproc
	ldgr	%f2,%r11
	.cfi_register 11, 17
	ldgr	%f0,%r15
	.cfi_register 15, 16
	lay	%r15,-168(%r15)
	.cfi_def_cfa_offset 328
	cg	%r0,160(%r15)
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,160(%r11)
	nopr	%r0
	lgdr	%r15,%f0
	.cfi_restore 15
	.cfi_def_cfa 15, 160
	lgdr	%r11,%f2
	.cfi_restore 11
	br	%r14
	.cfi_endproc
.LFE4603:
	.size	_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED2Ev, .-_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED2Ev
	.weak	_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED1Ev
	.set	_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED1Ev,_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED2Ev
	.section	.text._ZNSt16allocator_traitsISaISt10shared_ptrI8hittableEEE10deallocateERS3_PS2_m,"axG",@progbits,_ZNSt16allocator_traitsISaISt10shared_ptrI8hittableEEE10deallocateERS3_PS2_m,comdat
	.align	8
	.weak	_ZNSt16allocator_traitsISaISt10shared_ptrI8hittableEEE10deallocateERS3_PS2_m
	.type	_ZNSt16allocator_traitsISaISt10shared_ptrI8hittableEEE10deallocateERS3_PS2_m, @function
_ZNSt16allocator_traitsISaISt10shared_ptrI8hittableEEE10deallocateERS3_PS2_m:
.LFB4605:
	.cfi_startproc
	stmg	%r11,%r15,88(%r15)
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-184(%r15)
	.cfi_def_cfa_offset 344
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,176(%r11)
	stg	%r3,168(%r11)
	stg	%r4,160(%r11)
	lg	%r4,160(%r11)
	lg	%r3,168(%r11)
	lg	%r2,176(%r11)
	brasl	%r14,_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEE10deallocateEPS3_m
	nopr	%r0
	lmg	%r11,%r15,272(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE4605:
	.size	_ZNSt16allocator_traitsISaISt10shared_ptrI8hittableEEE10deallocateERS3_PS2_m, .-_ZNSt16allocator_traitsISaISt10shared_ptrI8hittableEEE10deallocateERS3_PS2_m
	.section	.text._ZNSt12_Destroy_auxILb0EE9__destroyIPSt10shared_ptrI8hittableEEEvT_S6_,"axG",@progbits,_ZNSt12_Destroy_auxILb0EE9__destroyIPSt10shared_ptrI8hittableEEEvT_S6_,comdat
	.align	8
	.weak	_ZNSt12_Destroy_auxILb0EE9__destroyIPSt10shared_ptrI8hittableEEEvT_S6_
	.type	_ZNSt12_Destroy_auxILb0EE9__destroyIPSt10shared_ptrI8hittableEEEvT_S6_, @function
_ZNSt12_Destroy_auxILb0EE9__destroyIPSt10shared_ptrI8hittableEEEvT_S6_:
.LFB4606:
	.cfi_startproc
	stmg	%r11,%r15,88(%r15)
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-176(%r15)
	.cfi_def_cfa_offset 336
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,168(%r11)
	stg	%r3,160(%r11)
.L268:
	lg	%r1,168(%r11)
	cg	%r1,160(%r11)
	je	.L269
	lg	%r2,168(%r11)
	brasl	%r14,_ZSt11__addressofISt10shared_ptrI8hittableEEPT_RS3_
	lgr	%r1,%r2
	lgr	%r2,%r1
	brasl	%r14,_ZSt8_DestroyISt10shared_ptrI8hittableEEvPT_
	agsi	168(%r11),16
	j	.L268
.L269:
	nopr	%r0
	lmg	%r11,%r15,264(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE4606:
	.size	_ZNSt12_Destroy_auxILb0EE9__destroyIPSt10shared_ptrI8hittableEEEvT_S6_, .-_ZNSt12_Destroy_auxILb0EE9__destroyIPSt10shared_ptrI8hittableEEEvT_S6_
	.section	.text._ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,"axG",@progbits,_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC5ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,comdat
	.align	8
	.weak	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.type	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_, @function
_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_:
.LFB4608:
	.cfi_startproc
	stmg	%r6,%r15,48(%r15)
	.cfi_offset 6, -112
	.cfi_offset 7, -104
	.cfi_offset 8, -96
	.cfi_offset 9, -88
	.cfi_offset 10, -80
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-192(%r15)
	.cfi_def_cfa_offset 352
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,184(%r11)
	stg	%r3,176(%r11)
	stg	%r4,168(%r11)
	stg	%r5,160(%r11)
	lg	%r1,184(%r11)
	mvghi	0(%r1),0
	lg	%r1,184(%r11)
	aghik	%r10,%r1,8
	lg	%r9,184(%r11)
	lg	%r2,168(%r11)
	brasl	%r14,_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
	lgr	%r8,%r2
	lg	%r2,160(%r11)
	brasl	%r14,_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE
	lgr	%r1,%r2
	lgr	%r6,%r1
	lgr	%r5,%r8
	lg	%r4,176(%r11)
	lgr	%r3,%r9
	lgr	%r2,%r10
	brasl	%r14,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_
	lg	%r1,184(%r11)
	lg	%r1,0(%r1)
	lgr	%r3,%r1
	lg	%r2,184(%r11)
	brasl	%r14,_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EE31_M_enable_shared_from_this_withIS0_S0_EENSt9enable_ifIXntsrNS3_15__has_esft_baseIT0_vEE5valueEvE4typeEPT_
	nopr	%r0
	lmg	%r6,%r15,240(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_restore 10
	.cfi_restore 9
	.cfi_restore 8
	.cfi_restore 7
	.cfi_restore 6
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE4608:
	.size	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_, .-_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.weak	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC1ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.set	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC1ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.section	.text._ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,"axG",@progbits,_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC5ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,comdat
	.align	8
	.weak	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.type	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_, @function
_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_:
.LFB4611:
	.cfi_startproc
	stmg	%r6,%r15,48(%r15)
	.cfi_offset 6, -112
	.cfi_offset 7, -104
	.cfi_offset 8, -96
	.cfi_offset 9, -88
	.cfi_offset 10, -80
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-192(%r15)
	.cfi_def_cfa_offset 352
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,184(%r11)
	stg	%r3,176(%r11)
	stg	%r4,168(%r11)
	stg	%r5,160(%r11)
	lg	%r1,184(%r11)
	mvghi	0(%r1),0
	lg	%r1,184(%r11)
	aghik	%r10,%r1,8
	lg	%r9,184(%r11)
	lg	%r2,168(%r11)
	brasl	%r14,_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
	lgr	%r8,%r2
	lg	%r2,160(%r11)
	brasl	%r14,_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE
	lgr	%r1,%r2
	lgr	%r6,%r1
	lgr	%r5,%r8
	lg	%r4,176(%r11)
	lgr	%r3,%r9
	lgr	%r2,%r10
	brasl	%r14,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_
	lg	%r1,184(%r11)
	lg	%r1,0(%r1)
	lgr	%r3,%r1
	lg	%r2,184(%r11)
	brasl	%r14,_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EE31_M_enable_shared_from_this_withIS0_S0_EENSt9enable_ifIXntsrNS3_15__has_esft_baseIT0_vEE5valueEvE4typeEPT_
	nopr	%r0
	lmg	%r6,%r15,240(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_restore 10
	.cfi_restore 9
	.cfi_restore 8
	.cfi_restore 7
	.cfi_restore 6
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE4611:
	.size	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_, .-_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.weak	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC1ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.set	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC1ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.section	.text._ZNSt8__detail4_ModImLm4294967296ELm1ELm0ELb1ELb1EE6__calcEm,"axG",@progbits,_ZNSt8__detail4_ModImLm4294967296ELm1ELm0ELb1ELb1EE6__calcEm,comdat
	.align	8
	.weak	_ZNSt8__detail4_ModImLm4294967296ELm1ELm0ELb1ELb1EE6__calcEm
	.type	_ZNSt8__detail4_ModImLm4294967296ELm1ELm0ELb1ELb1EE6__calcEm, @function
_ZNSt8__detail4_ModImLm4294967296ELm1ELm0ELb1ELb1EE6__calcEm:
.LFB4641:
	.cfi_startproc
	ldgr	%f2,%r11
	.cfi_register 11, 17
	ldgr	%f0,%r15
	.cfi_register 15, 16
	larl	%r5,.L277
	lay	%r15,-176(%r15)
	.cfi_def_cfa_offset 336
	cg	%r0,168(%r15)
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,160(%r11)
	lg	%r1,160(%r11)
	stg	%r1,168(%r11)
	nc	168(8,%r11),.L278-.L277(%r5)
	lg	%r1,168(%r11)
	lgr	%r2,%r1
	lgdr	%r15,%f0
	.cfi_restore 15
	.cfi_def_cfa 15, 160
	lgdr	%r11,%f2
	.cfi_restore 11
	br	%r14
	.section	.rodata._ZNSt8__detail4_ModImLm4294967296ELm1ELm0ELb1ELb1EE6__calcEm,"aG",@progbits,_ZNSt8__detail4_ModImLm4294967296ELm1ELm0ELb1ELb1EE6__calcEm,comdat
	.align	8
.L277:
.L278:
	.quad	4294967295
	.align	2
	.section	.text._ZNSt8__detail4_ModImLm4294967296ELm1ELm0ELb1ELb1EE6__calcEm,"axG",@progbits,_ZNSt8__detail4_ModImLm4294967296ELm1ELm0ELb1ELb1EE6__calcEm,comdat
	.cfi_endproc
.LFE4641:
	.size	_ZNSt8__detail4_ModImLm4294967296ELm1ELm0ELb1ELb1EE6__calcEm, .-_ZNSt8__detail4_ModImLm4294967296ELm1ELm0ELb1ELb1EE6__calcEm
	.section	.rodata
	.align	8
.LC22:
	.quad	945986875574848801
	.section	.text._ZNSt8__detail4_ModImLm624ELm1ELm0ELb1ELb1EE6__calcEm,"axG",@progbits,_ZNSt8__detail4_ModImLm624ELm1ELm0ELb1ELb1EE6__calcEm,comdat
	.align	8
	.weak	_ZNSt8__detail4_ModImLm624ELm1ELm0ELb1ELb1EE6__calcEm
	.type	_ZNSt8__detail4_ModImLm624ELm1ELm0ELb1ELb1EE6__calcEm, @function
_ZNSt8__detail4_ModImLm624ELm1ELm0ELb1ELb1EE6__calcEm:
.LFB4642:
	.cfi_startproc
	ldgr	%f2,%r11
	.cfi_register 11, 17
	ldgr	%f0,%r15
	.cfi_register 15, 16
	lay	%r15,-176(%r15)
	.cfi_def_cfa_offset 336
	cg	%r0,168(%r15)
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,160(%r11)
	lg	%r1,160(%r11)
	stg	%r1,168(%r11)
	lg	%r1,168(%r11)
	srlg	%r2,%r1,4
	lgrl	%r3,.LC22
	mlgr	%r2,%r2
	srlg	%r2,%r2,1
	mghi	%r2,624
	sgr	%r1,%r2
	stg	%r1,168(%r11)
	lg	%r1,168(%r11)
	lgr	%r2,%r1
	lgdr	%r15,%f0
	.cfi_restore 15
	.cfi_def_cfa 15, 160
	lgdr	%r11,%f2
	.cfi_restore 11
	br	%r14
	.cfi_endproc
.LFE4642:
	.size	_ZNSt8__detail4_ModImLm624ELm1ELm0ELb1ELb1EE6__calcEm, .-_ZNSt8__detail4_ModImLm624ELm1ELm0ELb1ELb1EE6__calcEm
	.section	.text._ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEclEv,"axG",@progbits,_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEclEv,comdat
	.align	8
	.weak	_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEclEv
	.type	_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEclEv, @function
_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEclEv:
.LFB4643:
	.cfi_startproc
	stmg	%r11,%r15,88(%r15)
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-176(%r15)
	.cfi_def_cfa_offset 336
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,160(%r11)
	lg	%r1,160(%r11)
	lg	%r1,4992(%r1)
	clgfi	%r1,623
	jle	.L283
	lg	%r2,160(%r11)
	brasl	%r14,_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE11_M_gen_randEv
.L283:
	lg	%r1,160(%r11)
	lg	%r1,4992(%r1)
	aghik	%r3,%r1,1
	lg	%r2,160(%r11)
	stg	%r3,4992(%r2)
	lg	%r2,160(%r11)
	sllg	%r1,%r1,3
	lg	%r1,0(%r1,%r2)
	stg	%r1,168(%r11)
	lg	%r1,168(%r11)
	srlg	%r1,%r1,11
	nihf	%r1,0
	xg	%r1,168(%r11)
	stg	%r1,168(%r11)
	lg	%r1,168(%r11)
	sllg	%r1,%r1,7
	llilf	%r2,2636928640
	ngr	%r1,%r2
	xg	%r1,168(%r11)
	stg	%r1,168(%r11)
	lg	%r1,168(%r11)
	sllg	%r1,%r1,15
	llilh	%r2,61382
	ngr	%r1,%r2
	xg	%r1,168(%r11)
	stg	%r1,168(%r11)
	lg	%r1,168(%r11)
	srlg	%r1,%r1,18
	xg	%r1,168(%r11)
	stg	%r1,168(%r11)
	lg	%r1,168(%r11)
	lgr	%r2,%r1
	lmg	%r11,%r15,264(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE4643:
	.size	_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEclEv, .-_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEclEv
	.section	.text._ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEE10deallocateEPS3_m,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEE10deallocateEPS3_m,comdat
	.align	8
	.weak	_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEE10deallocateEPS3_m
	.type	_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEE10deallocateEPS3_m, @function
_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEE10deallocateEPS3_m:
.LFB4645:
	.cfi_startproc
	stmg	%r11,%r15,88(%r15)
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-184(%r15)
	.cfi_def_cfa_offset 344
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,176(%r11)
	stg	%r3,168(%r11)
	stg	%r4,160(%r11)
	lg	%r2,168(%r11)
	brasl	%r14,_ZdlPv@PLT
	nopr	%r0
	lmg	%r11,%r15,272(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE4645:
	.size	_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEE10deallocateEPS3_m, .-_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEE10deallocateEPS3_m
	.section	.text._ZSt11__addressofISt10shared_ptrI8hittableEEPT_RS3_,"axG",@progbits,_ZSt11__addressofISt10shared_ptrI8hittableEEPT_RS3_,comdat
	.align	8
	.weak	_ZSt11__addressofISt10shared_ptrI8hittableEEPT_RS3_
	.type	_ZSt11__addressofISt10shared_ptrI8hittableEEPT_RS3_, @function
_ZSt11__addressofISt10shared_ptrI8hittableEEPT_RS3_:
.LFB4646:
	.cfi_startproc
	ldgr	%f2,%r11
	.cfi_register 11, 17
	ldgr	%f0,%r15
	.cfi_register 15, 16
	lay	%r15,-168(%r15)
	.cfi_def_cfa_offset 328
	cg	%r0,160(%r15)
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,160(%r11)
	lg	%r1,160(%r11)
	lgr	%r2,%r1
	lgdr	%r15,%f0
	.cfi_restore 15
	.cfi_def_cfa 15, 160
	lgdr	%r11,%f2
	.cfi_restore 11
	br	%r14
	.cfi_endproc
.LFE4646:
	.size	_ZSt11__addressofISt10shared_ptrI8hittableEEPT_RS3_, .-_ZSt11__addressofISt10shared_ptrI8hittableEEPT_RS3_
	.section	.text._ZSt8_DestroyISt10shared_ptrI8hittableEEvPT_,"axG",@progbits,_ZSt8_DestroyISt10shared_ptrI8hittableEEvPT_,comdat
	.align	8
	.weak	_ZSt8_DestroyISt10shared_ptrI8hittableEEvPT_
	.type	_ZSt8_DestroyISt10shared_ptrI8hittableEEvPT_, @function
_ZSt8_DestroyISt10shared_ptrI8hittableEEvPT_:
.LFB4647:
	.cfi_startproc
	stmg	%r11,%r15,88(%r15)
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-168(%r15)
	.cfi_def_cfa_offset 328
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,160(%r11)
	lg	%r2,160(%r11)
	brasl	%r14,_ZNSt10shared_ptrI8hittableED1Ev
	nopr	%r0
	lmg	%r11,%r15,256(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE4647:
	.size	_ZSt8_DestroyISt10shared_ptrI8hittableEEvPT_, .-_ZSt8_DestroyISt10shared_ptrI8hittableEEvPT_
	.section	.text._ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_,"axG",@progbits,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC5I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_,comdat
	.align	8
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_
	.type	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_, @function
_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_:
.LFB4649:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA4649
	stmg	%r6,%r15,48(%r15)
	.cfi_offset 6, -112
	.cfi_offset 7, -104
	.cfi_offset 8, -96
	.cfi_offset 9, -88
	.cfi_offset 10, -80
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-248(%r15)
	.cfi_def_cfa_offset 408
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,192(%r11)
	stg	%r3,184(%r11)
	stg	%r4,176(%r11)
	stg	%r5,168(%r11)
	stg	%r6,160(%r11)
	ear	%r1,%a0
	sllg	%r1,%r1,32
	ear	%r1,%a1
	mvc	240(8,%r11),40(%r1)
	lg	%r2,176(%r11)
	aghik	%r1,%r11,206
	lgr	%r3,%r2
	lgr	%r2,%r1
	brasl	%r14,_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC1IS0_EERKSaIT_E
	aghik	%r1,%r11,224
	aghik	%r2,%r11,206
	lgr	%r3,%r2
	lgr	%r2,%r1
.LEHB20:
	brasl	%r14,_ZSt18__allocate_guardedISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEESt15__allocated_ptrIT_ERS8_
.LEHE20:
	aghik	%r1,%r11,224
	lgr	%r2,%r1
	brasl	%r14,_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE3getEv
	stg	%r2,208(%r11)
	lg	%r2,176(%r11)
	aghik	%r1,%r11,207
	lgr	%r3,%r2
	lgr	%r2,%r1
	brasl	%r14,_ZNSaI6sphereEC1ERKS0_
	aghik	%r9,%r11,207
	lg	%r2,168(%r11)
	brasl	%r14,_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
	lgr	%r8,%r2
	lg	%r2,160(%r11)
	brasl	%r14,_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE
	lgr	%r7,%r2
	lg	%r13,208(%r11)
	lgr	%r3,%r13
	lghi	%r2,72
	brasl	%r14,_ZnwmPv
	lgr	%r10,%r2
	lgr	%r5,%r7
	lgr	%r4,%r8
	lgr	%r3,%r9
	lgr	%r2,%r10
.LEHB21:
	brasl	%r14,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC1IJ4vec3dEEES1_DpOT_
.LEHE21:
	stg	%r10,216(%r11)
	aghik	%r1,%r11,207
	lgr	%r2,%r1
	brasl	%r14,_ZNSaI6sphereED1Ev
	aghik	%r1,%r11,224
	lghi	%r3,0
	lgr	%r2,%r1
	brasl	%r14,_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEaSEDn
	lg	%r1,192(%r11)
	lg	%r2,216(%r11)
	stg	%r2,0(%r1)
	lg	%r2,216(%r11)
	brasl	%r14,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv
	lg	%r1,184(%r11)
	stg	%r2,0(%r1)
	aghik	%r1,%r11,224
	lgr	%r2,%r1
	brasl	%r14,_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED1Ev
	aghik	%r1,%r11,206
	lgr	%r2,%r1
	brasl	%r14,_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED1Ev
	nopr	%r0
	ear	%r1,%a0
	sllg	%r1,%r1,32
	ear	%r1,%a1
	clc	240(8,%r11),40(%r1)
	je	.L296
	j	.L299
.L298:
	lgr	%r9,%r6
	lgr	%r3,%r13
	lgr	%r2,%r10
	brasl	%r14,_ZdlPvS_
	lgr	%r10,%r9
	aghik	%r1,%r11,207
	lgr	%r2,%r1
	brasl	%r14,_ZNSaI6sphereED1Ev
	aghik	%r1,%r11,224
	lgr	%r2,%r1
	brasl	%r14,_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED1Ev
	j	.L295
.L297:
	lgr	%r10,%r6
.L295:
	aghik	%r1,%r11,206
	lgr	%r2,%r1
	brasl	%r14,_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED1Ev
	lgr	%r1,%r10
	lgr	%r2,%r1
.LEHB22:
	brasl	%r14,_Unwind_Resume@PLT
.LEHE22:
.L299:
	brasl	%r14,__stack_chk_fail@PLT
.L296:
	lmg	%r6,%r15,296(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_restore 10
	.cfi_restore 9
	.cfi_restore 8
	.cfi_restore 7
	.cfi_restore 6
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE4649:
	.section	.gcc_except_table
.LLSDA4649:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4649-.LLSDACSB4649
.LLSDACSB4649:
	.uleb128 .LEHB20-.LFB4649
	.uleb128 .LEHE20-.LEHB20
	.uleb128 .L297-.LFB4649
	.uleb128 0
	.uleb128 .LEHB21-.LFB4649
	.uleb128 .LEHE21-.LEHB21
	.uleb128 .L298-.LFB4649
	.uleb128 0
	.uleb128 .LEHB22-.LFB4649
	.uleb128 .LEHE22-.LEHB22
	.uleb128 0
	.uleb128 0
.LLSDACSE4649:
	.section	.text._ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_,"axG",@progbits,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC5I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_,comdat
	.size	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_, .-_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_
	.set	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_
	.section	.text._ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EE31_M_enable_shared_from_this_withIS0_S0_EENSt9enable_ifIXntsrNS3_15__has_esft_baseIT0_vEE5valueEvE4typeEPT_,"axG",@progbits,_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EE31_M_enable_shared_from_this_withIS0_S0_EENSt9enable_ifIXntsrNS3_15__has_esft_baseIT0_vEE5valueEvE4typeEPT_,comdat
	.align	8
	.weak	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EE31_M_enable_shared_from_this_withIS0_S0_EENSt9enable_ifIXntsrNS3_15__has_esft_baseIT0_vEE5valueEvE4typeEPT_
	.type	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EE31_M_enable_shared_from_this_withIS0_S0_EENSt9enable_ifIXntsrNS3_15__has_esft_baseIT0_vEE5valueEvE4typeEPT_, @function
_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EE31_M_enable_shared_from_this_withIS0_S0_EENSt9enable_ifIXntsrNS3_15__has_esft_baseIT0_vEE5valueEvE4typeEPT_:
.LFB4651:
	.cfi_startproc
	ldgr	%f2,%r11
	.cfi_register 11, 17
	ldgr	%f0,%r15
	.cfi_register 15, 16
	lay	%r15,-176(%r15)
	.cfi_def_cfa_offset 336
	cg	%r0,168(%r15)
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,168(%r11)
	stg	%r3,160(%r11)
	nopr	%r0
	lgdr	%r15,%f0
	.cfi_restore 15
	.cfi_def_cfa 15, 160
	lgdr	%r11,%f2
	.cfi_restore 11
	br	%r14
	.cfi_endproc
.LFE4651:
	.size	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EE31_M_enable_shared_from_this_withIS0_S0_EENSt9enable_ifIXntsrNS3_15__has_esft_baseIT0_vEE5valueEvE4typeEPT_, .-_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EE31_M_enable_shared_from_this_withIS0_S0_EENSt9enable_ifIXntsrNS3_15__has_esft_baseIT0_vEE5valueEvE4typeEPT_
	.section	.text._ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_,"axG",@progbits,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC5I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_,comdat
	.align	8
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_
	.type	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_, @function
_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_:
.LFB4653:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA4653
	stmg	%r6,%r15,48(%r15)
	.cfi_offset 6, -112
	.cfi_offset 7, -104
	.cfi_offset 8, -96
	.cfi_offset 9, -88
	.cfi_offset 10, -80
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-248(%r15)
	.cfi_def_cfa_offset 408
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,192(%r11)
	stg	%r3,184(%r11)
	stg	%r4,176(%r11)
	stg	%r5,168(%r11)
	stg	%r6,160(%r11)
	ear	%r1,%a0
	sllg	%r1,%r1,32
	ear	%r1,%a1
	mvc	240(8,%r11),40(%r1)
	lg	%r2,176(%r11)
	aghik	%r1,%r11,206
	lgr	%r3,%r2
	lgr	%r2,%r1
	brasl	%r14,_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC1IS0_EERKSaIT_E
	aghik	%r1,%r11,224
	aghik	%r2,%r11,206
	lgr	%r3,%r2
	lgr	%r2,%r1
.LEHB23:
	brasl	%r14,_ZSt18__allocate_guardedISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEESt15__allocated_ptrIT_ERS8_
.LEHE23:
	aghik	%r1,%r11,224
	lgr	%r2,%r1
	brasl	%r14,_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE3getEv
	stg	%r2,208(%r11)
	lg	%r2,176(%r11)
	aghik	%r1,%r11,207
	lgr	%r3,%r2
	lgr	%r2,%r1
	brasl	%r14,_ZNSaI6sphereEC1ERKS0_
	aghik	%r9,%r11,207
	lg	%r2,168(%r11)
	brasl	%r14,_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
	lgr	%r8,%r2
	lg	%r2,160(%r11)
	brasl	%r14,_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE
	lgr	%r7,%r2
	lg	%r13,208(%r11)
	lgr	%r3,%r13
	lghi	%r2,72
	brasl	%r14,_ZnwmPv
	lgr	%r10,%r2
	lgr	%r5,%r7
	lgr	%r4,%r8
	lgr	%r3,%r9
	lgr	%r2,%r10
.LEHB24:
	brasl	%r14,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC1IJ4vec3iEEES1_DpOT_
.LEHE24:
	stg	%r10,216(%r11)
	aghik	%r1,%r11,207
	lgr	%r2,%r1
	brasl	%r14,_ZNSaI6sphereED1Ev
	aghik	%r1,%r11,224
	lghi	%r3,0
	lgr	%r2,%r1
	brasl	%r14,_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEaSEDn
	lg	%r1,192(%r11)
	lg	%r2,216(%r11)
	stg	%r2,0(%r1)
	lg	%r2,216(%r11)
	brasl	%r14,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv
	lg	%r1,184(%r11)
	stg	%r2,0(%r1)
	aghik	%r1,%r11,224
	lgr	%r2,%r1
	brasl	%r14,_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED1Ev
	aghik	%r1,%r11,206
	lgr	%r2,%r1
	brasl	%r14,_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED1Ev
	nopr	%r0
	ear	%r1,%a0
	sllg	%r1,%r1,32
	ear	%r1,%a1
	clc	240(8,%r11),40(%r1)
	je	.L306
	j	.L309
.L308:
	lgr	%r9,%r6
	lgr	%r3,%r13
	lgr	%r2,%r10
	brasl	%r14,_ZdlPvS_
	lgr	%r10,%r9
	aghik	%r1,%r11,207
	lgr	%r2,%r1
	brasl	%r14,_ZNSaI6sphereED1Ev
	aghik	%r1,%r11,224
	lgr	%r2,%r1
	brasl	%r14,_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED1Ev
	j	.L305
.L307:
	lgr	%r10,%r6
.L305:
	aghik	%r1,%r11,206
	lgr	%r2,%r1
	brasl	%r14,_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED1Ev
	lgr	%r1,%r10
	lgr	%r2,%r1
.LEHB25:
	brasl	%r14,_Unwind_Resume@PLT
.LEHE25:
.L309:
	brasl	%r14,__stack_chk_fail@PLT
.L306:
	lmg	%r6,%r15,296(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_restore 10
	.cfi_restore 9
	.cfi_restore 8
	.cfi_restore 7
	.cfi_restore 6
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE4653:
	.section	.gcc_except_table
.LLSDA4653:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4653-.LLSDACSB4653
.LLSDACSB4653:
	.uleb128 .LEHB23-.LFB4653
	.uleb128 .LEHE23-.LEHB23
	.uleb128 .L307-.LFB4653
	.uleb128 0
	.uleb128 .LEHB24-.LFB4653
	.uleb128 .LEHE24-.LEHB24
	.uleb128 .L308-.LFB4653
	.uleb128 0
	.uleb128 .LEHB25-.LFB4653
	.uleb128 .LEHE25-.LEHB25
	.uleb128 0
	.uleb128 0
.LLSDACSE4653:
	.section	.text._ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_,"axG",@progbits,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC5I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_,comdat
	.size	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_, .-_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_
	.set	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_
	.section	.text._ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE11_M_gen_randEv,"axG",@progbits,_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE11_M_gen_randEv,comdat
	.align	8
	.weak	_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE11_M_gen_randEv
	.type	_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE11_M_gen_randEv, @function
_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE11_M_gen_randEv:
.LFB4681:
	.cfi_startproc
	ldgr	%f2,%r11
	.cfi_register 11, 17
	ldgr	%f0,%r15
	.cfi_register 15, 16
	lay	%r15,-224(%r15)
	.cfi_def_cfa_offset 384
	cg	%r0,216(%r15)
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,160(%r11)
	lgfi	%r1,-2147483648
	stg	%r1,184(%r11)
	lgfi	%r1,2147483647
	stg	%r1,192(%r11)
	mvghi	168(%r11),0
.L315:
	lg	%r1,168(%r11)
	clgfi	%r1,226
	jh	.L312
	lg	%r1,160(%r11)
	lg	%r2,168(%r11)
	sllg	%r2,%r2,3
	lg	%r1,0(%r2,%r1)
	risbg	%r3,%r1,0,128+32,0
	lg	%r1,168(%r11)
	aghik	%r2,%r1,1
	lg	%r1,160(%r11)
	sllg	%r2,%r2,3
	lg	%r1,0(%r2,%r1)
	llgtr	%r1,%r1
	ogr	%r1,%r3
	stg	%r1,200(%r11)
	lg	%r1,168(%r11)
	aghik	%r2,%r1,397
	lg	%r1,160(%r11)
	sllg	%r2,%r2,3
	lg	%r2,0(%r2,%r1)
	lg	%r1,200(%r11)
	srlg	%r1,%r1,1
	xgr	%r2,%r1
	lg	%r1,200(%r11)
	risbg	%r1,%r1,63,128+63,0
	ltgr	%r1,%r1
	je	.L313
	llilf	%r1,2567483615
	j	.L314
.L313:
	lghi	%r1,0
.L314:
	xgrk	%r3,%r1,%r2
	lg	%r1,160(%r11)
	lg	%r2,168(%r11)
	sllg	%r2,%r2,3
	stg	%r3,0(%r2,%r1)
	agsi	168(%r11),1
	j	.L315
.L312:
	mvghi	176(%r11),227
.L319:
	lg	%r1,176(%r11)
	clgfi	%r1,622
	jh	.L316
	lg	%r1,160(%r11)
	lg	%r2,176(%r11)
	sllg	%r2,%r2,3
	lg	%r1,0(%r2,%r1)
	risbg	%r3,%r1,0,128+32,0
	lg	%r1,176(%r11)
	aghik	%r2,%r1,1
	lg	%r1,160(%r11)
	sllg	%r2,%r2,3
	lg	%r1,0(%r2,%r1)
	llgtr	%r1,%r1
	ogr	%r1,%r3
	stg	%r1,208(%r11)
	lg	%r1,176(%r11)
	aghik	%r2,%r1,-227
	lg	%r1,160(%r11)
	sllg	%r2,%r2,3
	lg	%r2,0(%r2,%r1)
	lg	%r1,208(%r11)
	srlg	%r1,%r1,1
	xgr	%r2,%r1
	lg	%r1,208(%r11)
	risbg	%r1,%r1,63,128+63,0
	ltgr	%r1,%r1
	je	.L317
	llilf	%r1,2567483615
	j	.L318
.L317:
	lghi	%r1,0
.L318:
	xgrk	%r3,%r1,%r2
	lg	%r1,160(%r11)
	lg	%r2,176(%r11)
	sllg	%r2,%r2,3
	stg	%r3,0(%r2,%r1)
	agsi	176(%r11),1
	j	.L319
.L316:
	lg	%r1,160(%r11)
	lg	%r1,4984(%r1)
	risbg	%r2,%r1,0,128+32,0
	lg	%r1,160(%r11)
	lg	%r1,0(%r1)
	llgtr	%r1,%r1
	ogr	%r1,%r2
	stg	%r1,216(%r11)
	lg	%r1,160(%r11)
	lg	%r2,3168(%r1)
	lg	%r1,216(%r11)
	srlg	%r1,%r1,1
	xgr	%r2,%r1
	lg	%r1,216(%r11)
	risbg	%r1,%r1,63,128+63,0
	ltgr	%r1,%r1
	je	.L320
	llilf	%r1,2567483615
	j	.L321
.L320:
	lghi	%r1,0
.L321:
	xgr	%r2,%r1
	lg	%r1,160(%r11)
	stg	%r2,4984(%r1)
	lg	%r1,160(%r11)
	lay	%r1,4992(%r1)
	mvghi	0(%r1),0
	nopr	%r0
	lgdr	%r15,%f0
	.cfi_restore 15
	.cfi_def_cfa 15, 160
	lgdr	%r11,%f2
	.cfi_restore 11
	br	%r14
	.cfi_endproc
.LFE4681:
	.size	_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE11_M_gen_randEv, .-_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE11_M_gen_randEv
	.section	.text._ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC2IS0_EERKSaIT_E,"axG",@progbits,_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC5IS0_EERKSaIT_E,comdat
	.align	8
	.weak	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC2IS0_EERKSaIT_E
	.type	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC2IS0_EERKSaIT_E, @function
_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC2IS0_EERKSaIT_E:
.LFB4684:
	.cfi_startproc
	stmg	%r11,%r15,88(%r15)
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-176(%r15)
	.cfi_def_cfa_offset 336
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,168(%r11)
	stg	%r3,160(%r11)
	lg	%r2,168(%r11)
	brasl	%r14,_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC2Ev
	nopr	%r0
	lmg	%r11,%r15,264(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE4684:
	.size	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC2IS0_EERKSaIT_E, .-_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC2IS0_EERKSaIT_E
	.weak	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC1IS0_EERKSaIT_E
	.set	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC1IS0_EERKSaIT_E,_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC2IS0_EERKSaIT_E
	.section	.text._ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED2Ev,"axG",@progbits,_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED5Ev,comdat
	.align	8
	.weak	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED2Ev
	.type	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED2Ev, @function
_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED2Ev:
.LFB4687:
	.cfi_startproc
	stmg	%r11,%r15,88(%r15)
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-168(%r15)
	.cfi_def_cfa_offset 328
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,160(%r11)
	lg	%r2,160(%r11)
	brasl	%r14,_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED2Ev
	nopr	%r0
	lmg	%r11,%r15,256(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE4687:
	.size	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED2Ev, .-_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED2Ev
	.weak	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED1Ev
	.set	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED1Ev,_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED2Ev
	.section	.text._ZSt18__allocate_guardedISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEESt15__allocated_ptrIT_ERS8_,"axG",@progbits,_ZSt18__allocate_guardedISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEESt15__allocated_ptrIT_ERS8_,comdat
	.align	8
	.weak	_ZSt18__allocate_guardedISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEESt15__allocated_ptrIT_ERS8_
	.type	_ZSt18__allocate_guardedISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEESt15__allocated_ptrIT_ERS8_, @function
_ZSt18__allocate_guardedISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEESt15__allocated_ptrIT_ERS8_:
.LFB4689:
	.cfi_startproc
	stmg	%r11,%r15,88(%r15)
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-176(%r15)
	.cfi_def_cfa_offset 336
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,168(%r11)
	stg	%r3,160(%r11)
	lghi	%r3,1
	lg	%r2,160(%r11)
	brasl	%r14,_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE8allocateERS6_m
	lgr	%r1,%r2
	lgr	%r4,%r1
	lg	%r3,160(%r11)
	lg	%r2,168(%r11)
	brasl	%r14,_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC1ERS6_PS5_
	lg	%r2,168(%r11)
	lmg	%r11,%r15,264(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE4689:
	.size	_ZSt18__allocate_guardedISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEESt15__allocated_ptrIT_ERS8_, .-_ZSt18__allocate_guardedISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEESt15__allocated_ptrIT_ERS8_
	.section	.text._ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED2Ev,"axG",@progbits,_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED5Ev,comdat
	.align	8
	.weak	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED2Ev
	.type	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED2Ev, @function
_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED2Ev:
.LFB4691:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA4691
	stmg	%r11,%r15,88(%r15)
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-168(%r15)
	.cfi_def_cfa_offset 328
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,160(%r11)
	lg	%r1,160(%r11)
	lg	%r1,8(%r1)
	ltgr	%r1,%r1
	je	.L332
	lg	%r1,160(%r11)
	lg	%r2,0(%r1)
	lg	%r1,160(%r11)
	lg	%r1,8(%r1)
	lghi	%r4,1
	lgr	%r3,%r1
	brasl	%r14,_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE10deallocateERS6_PS5_m
.L332:
	nopr	%r0
	lmg	%r11,%r15,256(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE4691:
	.section	.gcc_except_table
.LLSDA4691:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4691-.LLSDACSB4691
.LLSDACSB4691:
.LLSDACSE4691:
	.section	.text._ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED2Ev,"axG",@progbits,_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED5Ev,comdat
	.size	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED2Ev, .-_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED2Ev
	.weak	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED1Ev
	.set	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED1Ev,_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED2Ev
	.section	.text._ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE3getEv,"axG",@progbits,_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE3getEv,comdat
	.align	8
	.weak	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE3getEv
	.type	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE3getEv, @function
_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE3getEv:
.LFB4696:
	.cfi_startproc
	stmg	%r11,%r15,88(%r15)
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-168(%r15)
	.cfi_def_cfa_offset 328
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,160(%r11)
	lg	%r1,160(%r11)
	lg	%r1,8(%r1)
	lgr	%r2,%r1
	brasl	%r14,_ZSt12__to_addressISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEPT_S7_
	lgr	%r1,%r2
	lgr	%r2,%r1
	lmg	%r11,%r15,256(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE4696:
	.size	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE3getEv, .-_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE3getEv
	.section	.text._ZNSaI6sphereEC2ERKS0_,"axG",@progbits,_ZNSaI6sphereEC5ERKS0_,comdat
	.align	8
	.weak	_ZNSaI6sphereEC2ERKS0_
	.type	_ZNSaI6sphereEC2ERKS0_, @function
_ZNSaI6sphereEC2ERKS0_:
.LFB4698:
	.cfi_startproc
	stmg	%r11,%r15,88(%r15)
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-176(%r15)
	.cfi_def_cfa_offset 336
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,168(%r11)
	stg	%r3,160(%r11)
	lg	%r3,160(%r11)
	lg	%r2,168(%r11)
	brasl	%r14,_ZN9__gnu_cxx13new_allocatorI6sphereEC2ERKS2_
	nopr	%r0
	lmg	%r11,%r15,264(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE4698:
	.size	_ZNSaI6sphereEC2ERKS0_, .-_ZNSaI6sphereEC2ERKS0_
	.weak	_ZNSaI6sphereEC1ERKS0_
	.set	_ZNSaI6sphereEC1ERKS0_,_ZNSaI6sphereEC2ERKS0_
	.section	.text._ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED2Ev,"axG",@progbits,_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED5Ev,comdat
	.align	8
	.weak	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED2Ev
	.type	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED2Ev, @function
_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED2Ev:
.LFB4703:
	.cfi_startproc
	stmg	%r11,%r15,88(%r15)
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-168(%r15)
	.cfi_def_cfa_offset 328
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,160(%r11)
	lg	%r2,160(%r11)
	brasl	%r14,_ZNSaI6sphereED2Ev
	nopr	%r0
	lmg	%r11,%r15,256(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE4703:
	.size	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED2Ev, .-_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED2Ev
	.weak	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED1Ev
	.set	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED1Ev,_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED2Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD2Ev,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD5Ev,comdat
	.align	8
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD2Ev
	.type	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD2Ev, @function
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD2Ev:
.LFB4705:
	.cfi_startproc
	stmg	%r11,%r15,88(%r15)
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-168(%r15)
	.cfi_def_cfa_offset 328
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,160(%r11)
	lg	%r2,160(%r11)
	brasl	%r14,_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED2Ev
	nopr	%r0
	lmg	%r11,%r15,256(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE4705:
	.size	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD2Ev, .-_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD2Ev
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD1Ev
	.set	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD1Ev,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD2Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3dEEES1_DpOT_,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC5IJ4vec3dEEES1_DpOT_,comdat
	.align	8
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3dEEES1_DpOT_
	.type	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3dEEES1_DpOT_, @function
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3dEEES1_DpOT_:
.LFB4707:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA4707
	stmg	%r6,%r15,48(%r15)
	.cfi_offset 6, -112
	.cfi_offset 7, -104
	.cfi_offset 8, -96
	.cfi_offset 9, -88
	.cfi_offset 10, -80
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-208(%r15)
	.cfi_def_cfa_offset 368
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,184(%r11)
	stg	%r3,176(%r11)
	stg	%r4,168(%r11)
	stg	%r5,160(%r11)
	ear	%r1,%a0
	sllg	%r1,%r1,32
	ear	%r1,%a1
	mvc	200(8,%r11),40(%r1)
	lg	%r1,184(%r11)
	lgr	%r2,%r1
	brasl	%r14,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC2Ev
	larl	%r2,_ZTVSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE+16
	lg	%r1,184(%r11)
	stg	%r2,0(%r1)
	lg	%r1,184(%r11)
	aghik	%r10,%r1,16
	aghik	%r1,%r11,199
	lg	%r3,176(%r11)
	lgr	%r2,%r1
	brasl	%r14,_ZNSaI6sphereEC1ERKS0_
	aghik	%r1,%r11,199
	lgr	%r3,%r1
	lgr	%r2,%r10
	brasl	%r14,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC1ES1_
	aghik	%r1,%r11,199
	lgr	%r2,%r1
	brasl	%r14,_ZNSaI6sphereED1Ev
	lg	%r2,184(%r11)
	brasl	%r14,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv
	lgr	%r10,%r2
	lg	%r2,168(%r11)
	brasl	%r14,_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
	lgr	%r9,%r2
	lg	%r2,160(%r11)
	brasl	%r14,_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE
	lgr	%r1,%r2
	lgr	%r5,%r1
	lgr	%r4,%r9
	lgr	%r3,%r10
	lg	%r2,176(%r11)
.LEHB26:
	brasl	%r14,_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3dEEEvRS1_PT_DpOT0_
.LEHE26:
	j	.L347
.L346:
	lgr	%r10,%r6
	lg	%r1,184(%r11)
	aghi	%r1,16
	lgr	%r2,%r1
	brasl	%r14,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD1Ev
	lg	%r1,184(%r11)
	lgr	%r2,%r1
	brasl	%r14,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev
	lgr	%r1,%r10
	lgr	%r2,%r1
.LEHB27:
	brasl	%r14,_Unwind_Resume@PLT
.LEHE27:
.L347:
	ear	%r1,%a0
	sllg	%r1,%r1,32
	ear	%r1,%a1
	clc	200(8,%r11),40(%r1)
	je	.L345
	brasl	%r14,__stack_chk_fail@PLT
.L345:
	lmg	%r6,%r15,256(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_restore 10
	.cfi_restore 9
	.cfi_restore 8
	.cfi_restore 7
	.cfi_restore 6
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE4707:
	.section	.gcc_except_table
.LLSDA4707:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4707-.LLSDACSB4707
.LLSDACSB4707:
	.uleb128 .LEHB26-.LFB4707
	.uleb128 .LEHE26-.LEHB26
	.uleb128 .L346-.LFB4707
	.uleb128 0
	.uleb128 .LEHB27-.LFB4707
	.uleb128 .LEHE27-.LEHB27
	.uleb128 0
	.uleb128 0
.LLSDACSE4707:
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3dEEES1_DpOT_,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC5IJ4vec3dEEES1_DpOT_,comdat
	.size	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3dEEES1_DpOT_, .-_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3dEEES1_DpOT_
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC1IJ4vec3dEEES1_DpOT_
	.set	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC1IJ4vec3dEEES1_DpOT_,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3dEEES1_DpOT_
	.section	.text._ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEaSEDn,"axG",@progbits,_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEaSEDn,comdat
	.align	8
	.weak	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEaSEDn
	.type	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEaSEDn, @function
_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEaSEDn:
.LFB4709:
	.cfi_startproc
	ldgr	%f2,%r11
	.cfi_register 11, 17
	ldgr	%f0,%r15
	.cfi_register 15, 16
	lay	%r15,-176(%r15)
	.cfi_def_cfa_offset 336
	cg	%r0,168(%r15)
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,168(%r11)
	stg	%r3,160(%r11)
	lg	%r1,168(%r11)
	mvghi	8(%r1),0
	lg	%r1,168(%r11)
	lgr	%r2,%r1
	lgdr	%r15,%f0
	.cfi_restore 15
	.cfi_def_cfa 15, 160
	lgdr	%r11,%f2
	.cfi_restore 11
	br	%r14
	.cfi_endproc
.LFE4709:
	.size	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEaSEDn, .-_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEaSEDn
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv,comdat
	.align	8
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv
	.type	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv, @function
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv:
.LFB4710:
	.cfi_startproc
	stmg	%r11,%r15,88(%r15)
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-168(%r15)
	.cfi_def_cfa_offset 328
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,160(%r11)
	lg	%r1,160(%r11)
	aghi	%r1,16
	lgr	%r2,%r1
	brasl	%r14,_ZN9__gnu_cxx16__aligned_bufferI6sphereE6_M_ptrEv
	lgr	%r1,%r2
	lgr	%r2,%r1
	lmg	%r11,%r15,256(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE4710:
	.size	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv, .-_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3iEEES1_DpOT_,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC5IJ4vec3iEEES1_DpOT_,comdat
	.align	8
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3iEEES1_DpOT_
	.type	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3iEEES1_DpOT_, @function
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3iEEES1_DpOT_:
.LFB4712:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA4712
	stmg	%r6,%r15,48(%r15)
	.cfi_offset 6, -112
	.cfi_offset 7, -104
	.cfi_offset 8, -96
	.cfi_offset 9, -88
	.cfi_offset 10, -80
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-208(%r15)
	.cfi_def_cfa_offset 368
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,184(%r11)
	stg	%r3,176(%r11)
	stg	%r4,168(%r11)
	stg	%r5,160(%r11)
	ear	%r1,%a0
	sllg	%r1,%r1,32
	ear	%r1,%a1
	mvc	200(8,%r11),40(%r1)
	lg	%r1,184(%r11)
	lgr	%r2,%r1
	brasl	%r14,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC2Ev
	larl	%r2,_ZTVSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE+16
	lg	%r1,184(%r11)
	stg	%r2,0(%r1)
	lg	%r1,184(%r11)
	aghik	%r10,%r1,16
	aghik	%r1,%r11,199
	lg	%r3,176(%r11)
	lgr	%r2,%r1
	brasl	%r14,_ZNSaI6sphereEC1ERKS0_
	aghik	%r1,%r11,199
	lgr	%r3,%r1
	lgr	%r2,%r10
	brasl	%r14,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC1ES1_
	aghik	%r1,%r11,199
	lgr	%r2,%r1
	brasl	%r14,_ZNSaI6sphereED1Ev
	lg	%r2,184(%r11)
	brasl	%r14,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv
	lgr	%r10,%r2
	lg	%r2,168(%r11)
	brasl	%r14,_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
	lgr	%r9,%r2
	lg	%r2,160(%r11)
	brasl	%r14,_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE
	lgr	%r1,%r2
	lgr	%r5,%r1
	lgr	%r4,%r9
	lgr	%r3,%r10
	lg	%r2,176(%r11)
.LEHB28:
	brasl	%r14,_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3iEEEvRS1_PT_DpOT0_
.LEHE28:
	j	.L359
.L358:
	lgr	%r10,%r6
	lg	%r1,184(%r11)
	aghi	%r1,16
	lgr	%r2,%r1
	brasl	%r14,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD1Ev
	lg	%r1,184(%r11)
	lgr	%r2,%r1
	brasl	%r14,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev
	lgr	%r1,%r10
	lgr	%r2,%r1
.LEHB29:
	brasl	%r14,_Unwind_Resume@PLT
.LEHE29:
.L359:
	ear	%r1,%a0
	sllg	%r1,%r1,32
	ear	%r1,%a1
	clc	200(8,%r11),40(%r1)
	je	.L357
	brasl	%r14,__stack_chk_fail@PLT
.L357:
	lmg	%r6,%r15,256(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_restore 10
	.cfi_restore 9
	.cfi_restore 8
	.cfi_restore 7
	.cfi_restore 6
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE4712:
	.section	.gcc_except_table
.LLSDA4712:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4712-.LLSDACSB4712
.LLSDACSB4712:
	.uleb128 .LEHB28-.LFB4712
	.uleb128 .LEHE28-.LEHB28
	.uleb128 .L358-.LFB4712
	.uleb128 0
	.uleb128 .LEHB29-.LFB4712
	.uleb128 .LEHE29-.LEHB29
	.uleb128 0
	.uleb128 0
.LLSDACSE4712:
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3iEEES1_DpOT_,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC5IJ4vec3iEEES1_DpOT_,comdat
	.size	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3iEEES1_DpOT_, .-_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3iEEES1_DpOT_
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC1IJ4vec3iEEES1_DpOT_
	.set	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC1IJ4vec3iEEES1_DpOT_,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3iEEES1_DpOT_
	.section	.text._ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC2Ev,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC5Ev,comdat
	.align	8
	.weak	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC2Ev
	.type	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC2Ev, @function
_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC2Ev:
.LFB4725:
	.cfi_startproc
	ldgr	%f2,%r11
	.cfi_register 11, 17
	ldgr	%f0,%r15
	.cfi_register 15, 16
	lay	%r15,-168(%r15)
	.cfi_def_cfa_offset 328
	cg	%r0,160(%r15)
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,160(%r11)
	nopr	%r0
	lgdr	%r15,%f0
	.cfi_restore 15
	.cfi_def_cfa 15, 160
	lgdr	%r11,%f2
	.cfi_restore 11
	br	%r14
	.cfi_endproc
.LFE4725:
	.size	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC2Ev, .-_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC2Ev
	.weak	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC1Ev
	.set	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC1Ev,_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC2Ev
	.section	.text._ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED2Ev,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED5Ev,comdat
	.align	8
	.weak	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED2Ev
	.type	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED2Ev, @function
_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED2Ev:
.LFB4728:
	.cfi_startproc
	ldgr	%f2,%r11
	.cfi_register 11, 17
	ldgr	%f0,%r15
	.cfi_register 15, 16
	lay	%r15,-168(%r15)
	.cfi_def_cfa_offset 328
	cg	%r0,160(%r15)
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,160(%r11)
	nopr	%r0
	lgdr	%r15,%f0
	.cfi_restore 15
	.cfi_def_cfa 15, 160
	lgdr	%r11,%f2
	.cfi_restore 11
	br	%r14
	.cfi_endproc
.LFE4728:
	.size	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED2Ev, .-_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED2Ev
	.weak	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED1Ev
	.set	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED1Ev,_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED2Ev
	.section	.text._ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE8allocateERS6_m,"axG",@progbits,_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE8allocateERS6_m,comdat
	.align	8
	.weak	_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE8allocateERS6_m
	.type	_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE8allocateERS6_m, @function
_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE8allocateERS6_m:
.LFB4730:
	.cfi_startproc
	stmg	%r11,%r15,88(%r15)
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-176(%r15)
	.cfi_def_cfa_offset 336
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,168(%r11)
	stg	%r3,160(%r11)
	lghi	%r4,0
	lg	%r3,160(%r11)
	lg	%r2,168(%r11)
	brasl	%r14,_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8allocateEmPKv
	lgr	%r1,%r2
	lgr	%r2,%r1
	lmg	%r11,%r15,264(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE4730:
	.size	_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE8allocateERS6_m, .-_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE8allocateERS6_m
	.section	.text._ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC2ERS6_PS5_,"axG",@progbits,_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC5ERS6_PS5_,comdat
	.align	8
	.weak	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC2ERS6_PS5_
	.type	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC2ERS6_PS5_, @function
_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC2ERS6_PS5_:
.LFB4732:
	.cfi_startproc
	stmg	%r11,%r15,88(%r15)
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-184(%r15)
	.cfi_def_cfa_offset 344
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,176(%r11)
	stg	%r3,168(%r11)
	stg	%r4,160(%r11)
	lg	%r2,168(%r11)
	brasl	%r14,_ZSt11__addressofISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEPT_RS7_
	lg	%r1,176(%r11)
	stg	%r2,0(%r1)
	lg	%r1,176(%r11)
	lg	%r2,160(%r11)
	stg	%r2,8(%r1)
	nopr	%r0
	lmg	%r11,%r15,272(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE4732:
	.size	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC2ERS6_PS5_, .-_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC2ERS6_PS5_
	.weak	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC1ERS6_PS5_
	.set	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC1ERS6_PS5_,_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC2ERS6_PS5_
	.section	.text._ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE10deallocateERS6_PS5_m,"axG",@progbits,_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE10deallocateERS6_PS5_m,comdat
	.align	8
	.weak	_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE10deallocateERS6_PS5_m
	.type	_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE10deallocateERS6_PS5_m, @function
_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE10deallocateERS6_PS5_m:
.LFB4734:
	.cfi_startproc
	stmg	%r11,%r15,88(%r15)
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-184(%r15)
	.cfi_def_cfa_offset 344
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,176(%r11)
	stg	%r3,168(%r11)
	stg	%r4,160(%r11)
	lg	%r4,160(%r11)
	lg	%r3,168(%r11)
	lg	%r2,176(%r11)
	brasl	%r14,_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE10deallocateEPS5_m
	nopr	%r0
	lmg	%r11,%r15,272(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE4734:
	.size	_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE10deallocateERS6_PS5_m, .-_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE10deallocateERS6_PS5_m
	.section	.text._ZSt12__to_addressISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEPT_S7_,"axG",@progbits,_ZSt12__to_addressISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEPT_S7_,comdat
	.align	8
	.weak	_ZSt12__to_addressISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEPT_S7_
	.type	_ZSt12__to_addressISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEPT_S7_, @function
_ZSt12__to_addressISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEPT_S7_:
.LFB4735:
	.cfi_startproc
	ldgr	%f2,%r11
	.cfi_register 11, 17
	ldgr	%f0,%r15
	.cfi_register 15, 16
	lay	%r15,-168(%r15)
	.cfi_def_cfa_offset 328
	cg	%r0,160(%r15)
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,160(%r11)
	lg	%r1,160(%r11)
	lgr	%r2,%r1
	lgdr	%r15,%f0
	.cfi_restore 15
	.cfi_def_cfa 15, 160
	lgdr	%r11,%f2
	.cfi_restore 11
	br	%r14
	.cfi_endproc
.LFE4735:
	.size	_ZSt12__to_addressISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEPT_S7_, .-_ZSt12__to_addressISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEPT_S7_
	.section	.text._ZN9__gnu_cxx13new_allocatorI6sphereEC2ERKS2_,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorI6sphereEC5ERKS2_,comdat
	.align	8
	.weak	_ZN9__gnu_cxx13new_allocatorI6sphereEC2ERKS2_
	.type	_ZN9__gnu_cxx13new_allocatorI6sphereEC2ERKS2_, @function
_ZN9__gnu_cxx13new_allocatorI6sphereEC2ERKS2_:
.LFB4737:
	.cfi_startproc
	ldgr	%f2,%r11
	.cfi_register 11, 17
	ldgr	%f0,%r15
	.cfi_register 15, 16
	lay	%r15,-176(%r15)
	.cfi_def_cfa_offset 336
	cg	%r0,168(%r15)
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,168(%r11)
	stg	%r3,160(%r11)
	nopr	%r0
	lgdr	%r15,%f0
	.cfi_restore 15
	.cfi_def_cfa 15, 160
	lgdr	%r11,%f2
	.cfi_restore 11
	br	%r14
	.cfi_endproc
.LFE4737:
	.size	_ZN9__gnu_cxx13new_allocatorI6sphereEC2ERKS2_, .-_ZN9__gnu_cxx13new_allocatorI6sphereEC2ERKS2_
	.weak	_ZN9__gnu_cxx13new_allocatorI6sphereEC1ERKS2_
	.set	_ZN9__gnu_cxx13new_allocatorI6sphereEC1ERKS2_,_ZN9__gnu_cxx13new_allocatorI6sphereEC2ERKS2_
	.section	.text._ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC2Ev,"axG",@progbits,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC5Ev,comdat
	.align	8
	.weak	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC2Ev
	.type	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC2Ev, @function
_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC2Ev:
.LFB4740:
	.cfi_startproc
	ldgr	%f2,%r11
	.cfi_register 11, 17
	ldgr	%f0,%r15
	.cfi_register 15, 16
	lay	%r15,-168(%r15)
	.cfi_def_cfa_offset 328
	cg	%r0,160(%r15)
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,160(%r11)
	larl	%r2,_ZTVSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE+16
	lg	%r1,160(%r11)
	stg	%r2,0(%r1)
	lg	%r1,160(%r11)
	mvhi	8(%r1),1
	lg	%r1,160(%r11)
	mvhi	12(%r1),1
	nopr	%r0
	lgdr	%r15,%f0
	.cfi_restore 15
	.cfi_def_cfa 15, 160
	lgdr	%r11,%f2
	.cfi_restore 11
	br	%r14
	.cfi_endproc
.LFE4740:
	.size	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC2Ev, .-_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC2Ev
	.weak	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC1Ev
	.set	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC1Ev,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC2Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC2ES1_,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC5ES1_,comdat
	.align	8
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC2ES1_
	.type	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC2ES1_, @function
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC2ES1_:
.LFB4743:
	.cfi_startproc
	stmg	%r11,%r15,88(%r15)
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-176(%r15)
	.cfi_def_cfa_offset 336
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,168(%r11)
	stg	%r3,160(%r11)
	lg	%r3,160(%r11)
	lg	%r2,168(%r11)
	brasl	%r14,_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC2ERKS1_
	nopr	%r0
	lmg	%r11,%r15,264(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE4743:
	.size	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC2ES1_, .-_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC2ES1_
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC1ES1_
	.set	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC1ES1_,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC2ES1_
	.section	.text._ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3dEEEvRS1_PT_DpOT0_,"axG",@progbits,_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3dEEEvRS1_PT_DpOT0_,comdat
	.align	8
	.weak	_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3dEEEvRS1_PT_DpOT0_
	.type	_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3dEEEvRS1_PT_DpOT0_, @function
_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3dEEEvRS1_PT_DpOT0_:
.LFB4745:
	.cfi_startproc
	stmg	%r10,%r15,80(%r15)
	.cfi_offset 10, -80
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-192(%r15)
	.cfi_def_cfa_offset 352
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,184(%r11)
	stg	%r3,176(%r11)
	stg	%r4,168(%r11)
	stg	%r5,160(%r11)
	lg	%r2,168(%r11)
	brasl	%r14,_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
	lgr	%r10,%r2
	lg	%r2,160(%r11)
	brasl	%r14,_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE
	lgr	%r1,%r2
	lgr	%r5,%r1
	lgr	%r4,%r10
	lg	%r3,176(%r11)
	lg	%r2,184(%r11)
	brasl	%r14,_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_
	nopr	%r0
	lmg	%r10,%r15,272(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_restore 10
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE4745:
	.size	_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3dEEEvRS1_PT_DpOT0_, .-_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3dEEEvRS1_PT_DpOT0_
	.section	.text._ZN9__gnu_cxx16__aligned_bufferI6sphereE6_M_ptrEv,"axG",@progbits,_ZN9__gnu_cxx16__aligned_bufferI6sphereE6_M_ptrEv,comdat
	.align	8
	.weak	_ZN9__gnu_cxx16__aligned_bufferI6sphereE6_M_ptrEv
	.type	_ZN9__gnu_cxx16__aligned_bufferI6sphereE6_M_ptrEv, @function
_ZN9__gnu_cxx16__aligned_bufferI6sphereE6_M_ptrEv:
.LFB4746:
	.cfi_startproc
	stmg	%r11,%r15,88(%r15)
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-168(%r15)
	.cfi_def_cfa_offset 328
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,160(%r11)
	lg	%r2,160(%r11)
	brasl	%r14,_ZN9__gnu_cxx16__aligned_bufferI6sphereE7_M_addrEv
	lgr	%r1,%r2
	lgr	%r2,%r1
	lmg	%r11,%r15,256(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE4746:
	.size	_ZN9__gnu_cxx16__aligned_bufferI6sphereE6_M_ptrEv, .-_ZN9__gnu_cxx16__aligned_bufferI6sphereE6_M_ptrEv
	.section	.text._ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3iEEEvRS1_PT_DpOT0_,"axG",@progbits,_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3iEEEvRS1_PT_DpOT0_,comdat
	.align	8
	.weak	_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3iEEEvRS1_PT_DpOT0_
	.type	_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3iEEEvRS1_PT_DpOT0_, @function
_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3iEEEvRS1_PT_DpOT0_:
.LFB4747:
	.cfi_startproc
	stmg	%r10,%r15,80(%r15)
	.cfi_offset 10, -80
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-192(%r15)
	.cfi_def_cfa_offset 352
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,184(%r11)
	stg	%r3,176(%r11)
	stg	%r4,168(%r11)
	stg	%r5,160(%r11)
	lg	%r2,168(%r11)
	brasl	%r14,_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
	lgr	%r10,%r2
	lg	%r2,160(%r11)
	brasl	%r14,_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE
	lgr	%r1,%r2
	lgr	%r5,%r1
	lgr	%r4,%r10
	lg	%r3,176(%r11)
	lg	%r2,184(%r11)
	brasl	%r14,_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_
	nopr	%r0
	lmg	%r10,%r15,272(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_restore 10
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE4747:
	.size	_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3iEEEvRS1_PT_DpOT0_, .-_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3iEEEvRS1_PT_DpOT0_
	.section	.text._ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8allocateEmPKv,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8allocateEmPKv,comdat
	.align	8
	.weak	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8allocateEmPKv
	.type	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8allocateEmPKv, @function
_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8allocateEmPKv:
.LFB4750:
	.cfi_startproc
	stmg	%r11,%r15,88(%r15)
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-184(%r15)
	.cfi_def_cfa_offset 344
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,176(%r11)
	stg	%r3,168(%r11)
	stg	%r4,160(%r11)
	lg	%r2,176(%r11)
	brasl	%r14,_ZNK9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8max_sizeEv
	lg	%r1,168(%r11)
	clgr	%r1,%r2
	lhi	%r1,0
	alcr	%r1,%r1
	llcr	%r1,%r1
	ltr	%r1,%r1
	je	.L389
	brasl	%r14,_ZSt17__throw_bad_allocv@PLT
.L389:
	lg	%r2,168(%r11)
	lgr	%r1,%r2
	sllg	%r1,%r1,3
	agr	%r1,%r2
	sllg	%r1,%r1,3
	lgr	%r2,%r1
	brasl	%r14,_Znwm@PLT
	lgr	%r1,%r2
	lgr	%r2,%r1
	lmg	%r11,%r15,272(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE4750:
	.size	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8allocateEmPKv, .-_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8allocateEmPKv
	.section	.text._ZSt11__addressofISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEPT_RS7_,"axG",@progbits,_ZSt11__addressofISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEPT_RS7_,comdat
	.align	8
	.weak	_ZSt11__addressofISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEPT_RS7_
	.type	_ZSt11__addressofISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEPT_RS7_, @function
_ZSt11__addressofISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEPT_RS7_:
.LFB4751:
	.cfi_startproc
	ldgr	%f2,%r11
	.cfi_register 11, 17
	ldgr	%f0,%r15
	.cfi_register 15, 16
	lay	%r15,-168(%r15)
	.cfi_def_cfa_offset 328
	cg	%r0,160(%r15)
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,160(%r11)
	lg	%r1,160(%r11)
	lgr	%r2,%r1
	lgdr	%r15,%f0
	.cfi_restore 15
	.cfi_def_cfa 15, 160
	lgdr	%r11,%f2
	.cfi_restore 11
	br	%r14
	.cfi_endproc
.LFE4751:
	.size	_ZSt11__addressofISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEPT_RS7_, .-_ZSt11__addressofISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEPT_RS7_
	.section	.text._ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE10deallocateEPS5_m,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE10deallocateEPS5_m,comdat
	.align	8
	.weak	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE10deallocateEPS5_m
	.type	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE10deallocateEPS5_m, @function
_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE10deallocateEPS5_m:
.LFB4752:
	.cfi_startproc
	stmg	%r11,%r15,88(%r15)
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-184(%r15)
	.cfi_def_cfa_offset 344
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,176(%r11)
	stg	%r3,168(%r11)
	stg	%r4,160(%r11)
	lg	%r2,168(%r11)
	brasl	%r14,_ZdlPv@PLT
	nopr	%r0
	lmg	%r11,%r15,272(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE4752:
	.size	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE10deallocateEPS5_m, .-_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE10deallocateEPS5_m
	.section	.text._ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC2ERKS1_,"axG",@progbits,_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC5ERKS1_,comdat
	.align	8
	.weak	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC2ERKS1_
	.type	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC2ERKS1_, @function
_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC2ERKS1_:
.LFB4754:
	.cfi_startproc
	stmg	%r11,%r15,88(%r15)
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-176(%r15)
	.cfi_def_cfa_offset 336
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,168(%r11)
	stg	%r3,160(%r11)
	lg	%r3,160(%r11)
	lg	%r2,168(%r11)
	brasl	%r14,_ZNSaI6sphereEC2ERKS0_
	nopr	%r0
	lmg	%r11,%r15,264(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE4754:
	.size	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC2ERKS1_, .-_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC2ERKS1_
	.weak	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC1ERKS1_
	.set	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC1ERKS1_,_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC2ERKS1_
	.section	.text._ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_,comdat
	.align	8
	.weak	_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_
	.type	_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_, @function
_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_:
.LFB4756:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA4756
	stmg	%r6,%r15,48(%r15)
	.cfi_offset 6, -112
	.cfi_offset 7, -104
	.cfi_offset 8, -96
	.cfi_offset 9, -88
	.cfi_offset 10, -80
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-248(%r15)
	.cfi_def_cfa_offset 408
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,208(%r11)
	stg	%r3,200(%r11)
	stg	%r4,192(%r11)
	stg	%r5,184(%r11)
	ear	%r1,%a0
	sllg	%r1,%r1,32
	ear	%r1,%a1
	mvc	240(8,%r11),40(%r1)
	lg	%r2,192(%r11)
	brasl	%r14,_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
	lgr	%r1,%r2
	mvc	216(24,%r11),0(%r1)
	lg	%r2,184(%r11)
	brasl	%r14,_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE
	lgr	%r1,%r2
	lg	%r9,0(%r1)
	lg	%r10,200(%r11)
	lgr	%r3,%r10
	lghi	%r2,56
	brasl	%r14,_ZnwmPv
	lgr	%r13,%r2
	mvc	160(24,%r11),216(%r11)
	aghik	%r1,%r11,160
	ldgr	%f0,%r9
	lgr	%r3,%r1
	lgr	%r2,%r13
.LEHB30:
	brasl	%r14,_ZN6sphereC1E4vec3d@PLT
.LEHE30:
	j	.L403
.L402:
	lgr	%r9,%r6
	lgr	%r3,%r10
	lgr	%r2,%r13
	brasl	%r14,_ZdlPvS_
	lgr	%r1,%r9
	lgr	%r2,%r1
.LEHB31:
	brasl	%r14,_Unwind_Resume@PLT
.LEHE31:
.L403:
	ear	%r1,%a0
	sllg	%r1,%r1,32
	ear	%r1,%a1
	clc	240(8,%r11),40(%r1)
	je	.L401
	brasl	%r14,__stack_chk_fail@PLT
.L401:
	lmg	%r6,%r15,296(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_restore 10
	.cfi_restore 9
	.cfi_restore 8
	.cfi_restore 7
	.cfi_restore 6
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE4756:
	.section	.gcc_except_table
.LLSDA4756:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4756-.LLSDACSB4756
.LLSDACSB4756:
	.uleb128 .LEHB30-.LFB4756
	.uleb128 .LEHE30-.LEHB30
	.uleb128 .L402-.LFB4756
	.uleb128 0
	.uleb128 .LEHB31-.LFB4756
	.uleb128 .LEHE31-.LEHB31
	.uleb128 0
	.uleb128 0
.LLSDACSE4756:
	.section	.text._ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_,comdat
	.size	_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_, .-_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_
	.section	.text._ZN9__gnu_cxx16__aligned_bufferI6sphereE7_M_addrEv,"axG",@progbits,_ZN9__gnu_cxx16__aligned_bufferI6sphereE7_M_addrEv,comdat
	.align	8
	.weak	_ZN9__gnu_cxx16__aligned_bufferI6sphereE7_M_addrEv
	.type	_ZN9__gnu_cxx16__aligned_bufferI6sphereE7_M_addrEv, @function
_ZN9__gnu_cxx16__aligned_bufferI6sphereE7_M_addrEv:
.LFB4757:
	.cfi_startproc
	ldgr	%f2,%r11
	.cfi_register 11, 17
	ldgr	%f0,%r15
	.cfi_register 15, 16
	lay	%r15,-168(%r15)
	.cfi_def_cfa_offset 328
	cg	%r0,160(%r15)
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,160(%r11)
	lg	%r1,160(%r11)
	lgr	%r2,%r1
	lgdr	%r15,%f0
	.cfi_restore 15
	.cfi_def_cfa 15, 160
	lgdr	%r11,%f2
	.cfi_restore 11
	br	%r14
	.cfi_endproc
.LFE4757:
	.size	_ZN9__gnu_cxx16__aligned_bufferI6sphereE7_M_addrEv, .-_ZN9__gnu_cxx16__aligned_bufferI6sphereE7_M_addrEv
	.section	.text._ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_,comdat
	.align	8
	.weak	_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_
	.type	_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_, @function
_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_:
.LFB4758:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA4758
	stmg	%r6,%r15,48(%r15)
	.cfi_offset 6, -112
	.cfi_offset 7, -104
	.cfi_offset 8, -96
	.cfi_offset 9, -88
	.cfi_offset 10, -80
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lgr	%r14,%r15
	lay	%r15,-256(%r15)
	.cfi_def_cfa_offset 416
	aghi	%r14,-8
	std	%f8,0(%r14)
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	.cfi_offset 24, -168
	stg	%r2,208(%r11)
	stg	%r3,200(%r11)
	stg	%r4,192(%r11)
	stg	%r5,184(%r11)
	ear	%r1,%a0
	sllg	%r1,%r1,32
	ear	%r1,%a1
	mvc	240(8,%r11),40(%r1)
	lg	%r2,192(%r11)
	brasl	%r14,_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
	lgr	%r1,%r2
	mvc	216(24,%r11),0(%r1)
	lg	%r2,184(%r11)
	brasl	%r14,_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE
	lgr	%r1,%r2
	l	%r1,0(%r1)
	cdfbr	%f8,%r1
	lg	%r10,200(%r11)
	lgr	%r3,%r10
	lghi	%r2,56
	brasl	%r14,_ZnwmPv
	lgr	%r13,%r2
	mvc	160(24,%r11),216(%r11)
	aghik	%r1,%r11,160
	ldr	%f0,%f8
	lgr	%r3,%r1
	lgr	%r2,%r13
.LEHB32:
	brasl	%r14,_ZN6sphereC1E4vec3d@PLT
.LEHE32:
	j	.L412
.L411:
	lgr	%r9,%r6
	lgr	%r3,%r10
	lgr	%r2,%r13
	brasl	%r14,_ZdlPvS_
	lgr	%r1,%r9
	lgr	%r2,%r1
.LEHB33:
	brasl	%r14,_Unwind_Resume@PLT
.LEHE33:
.L412:
	ear	%r1,%a0
	sllg	%r1,%r1,32
	ear	%r1,%a1
	clc	240(8,%r11),40(%r1)
	je	.L410
	brasl	%r14,__stack_chk_fail@PLT
.L410:
	ld	%f8,248(%r11)
	lmg	%r6,%r15,304(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_restore 10
	.cfi_restore 9
	.cfi_restore 8
	.cfi_restore 7
	.cfi_restore 6
	.cfi_restore 24
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE4758:
	.section	.gcc_except_table
.LLSDA4758:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4758-.LLSDACSB4758
.LLSDACSB4758:
	.uleb128 .LEHB32-.LFB4758
	.uleb128 .LEHE32-.LEHB32
	.uleb128 .L411-.LFB4758
	.uleb128 0
	.uleb128 .LEHB33-.LFB4758
	.uleb128 .LEHE33-.LEHB33
	.uleb128 0
	.uleb128 0
.LLSDACSE4758:
	.section	.text._ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_,comdat
	.size	_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_, .-_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_
	.section	.rodata
	.align	8
.LC23:
	.quad	128102389400760775
	.section	.text._ZNK9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8max_sizeEv,"axG",@progbits,_ZNK9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8max_sizeEv,comdat
	.align	8
	.weak	_ZNK9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8max_sizeEv
	.type	_ZNK9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8max_sizeEv, @function
_ZNK9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8max_sizeEv:
.LFB4759:
	.cfi_startproc
	ldgr	%f2,%r11
	.cfi_register 11, 17
	ldgr	%f0,%r15
	.cfi_register 15, 16
	lay	%r15,-168(%r15)
	.cfi_def_cfa_offset 328
	cg	%r0,160(%r15)
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,160(%r11)
	lgrl	%r1,.LC23
	lgr	%r2,%r1
	lgdr	%r15,%f0
	.cfi_restore 15
	.cfi_def_cfa 15, 160
	lgdr	%r11,%f2
	.cfi_restore 11
	br	%r14
	.cfi_endproc
.LFE4759:
	.size	_ZNK9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8max_sizeEv, .-_ZNK9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8max_sizeEv
	.weak	_ZTVSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE
	.section	.data.rel.ro.local._ZTVSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE,"awG",@progbits,_ZTVSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align	8
	.type	_ZTVSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTVSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE, 56
_ZTVSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE:
	.quad	0
	.quad	_ZTISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE
	.quad	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.quad	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.quad	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.quad	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.quad	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.weak	_ZTVSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE
	.section	.data.rel.ro._ZTVSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE,"awG",@progbits,_ZTVSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align	8
	.type	_ZTVSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTVSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE, 56
_ZTVSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE:
	.quad	0
	.quad	_ZTISt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.quad	__cxa_pure_virtual
	.weak	_ZTISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE
	.section	.data.rel.ro._ZTISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE,"awG",@progbits,_ZTISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align	8
	.type	_ZTISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE, 24
_ZTISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE
	.quad	_ZTISt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE
	.weak	_ZTSSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE
	.section	.rodata._ZTSSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE,"aG",@progbits,_ZTSSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align	2
	.type	_ZTSSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTSSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE, 73
_ZTSSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE:
	.string	"St23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE"
	.weak	_ZTISt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE
	.section	.data.rel.ro._ZTISt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE,"awG",@progbits,_ZTISt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align	8
	.type	_ZTISt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTISt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE, 24
_ZTISt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE
	.quad	_ZTISt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE
	.weak	_ZTSSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE
	.section	.rodata._ZTSSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE,"aG",@progbits,_ZTSSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align	2
	.type	_ZTSSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTSSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE, 52
_ZTSSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE:
	.string	"St16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE"
.text
	.align	8
	.type	_Z41__static_initialization_and_destruction_0ii, @function
_Z41__static_initialization_and_destruction_0ii:
.LFB4776:
	.cfi_startproc
	stmg	%r11,%r15,88(%r15)
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-168(%r15)
	.cfi_def_cfa_offset 328
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	lgr	%r1,%r2
	lgr	%r2,%r3
	st	%r1,164(%r11)
	lr	%r1,%r2
	st	%r1,160(%r11)
	l	%r1,164(%r11)
	chi	%r1,1
	jne	.L419
	l	%r1,160(%r11)
	cfi	%r1,65535
	jne	.L419
	larl	%r2,_ZStL8__ioinit
	brasl	%r14,_ZNSt8ios_base4InitC1Ev@PLT
	larl	%r4,__dso_handle
	larl	%r3,_ZStL8__ioinit
	lgrl	%r1,_ZNSt8ios_base4InitD1Ev@GOTENT
	lgr	%r2,%r1
	brasl	%r14,__cxa_atexit@PLT
.L419:
	nopr	%r0
	lmg	%r11,%r15,256(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE4776:
	.size	_Z41__static_initialization_and_destruction_0ii, .-_Z41__static_initialization_and_destruction_0ii
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align	8
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.type	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED2Ev:
.LFB4778:
	.cfi_startproc
	stmg	%r11,%r15,88(%r15)
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-168(%r15)
	.cfi_def_cfa_offset 328
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,160(%r11)
	larl	%r2,_ZTVSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE+16
	lg	%r1,160(%r11)
	stg	%r2,0(%r1)
	lg	%r1,160(%r11)
	aghi	%r1,16
	lgr	%r2,%r1
	brasl	%r14,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD1Ev
	lg	%r1,160(%r11)
	lgr	%r2,%r1
	brasl	%r14,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev
	nopr	%r0
	lmg	%r11,%r15,256(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE4778:
	.size	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.set	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED1Ev,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED0Ev,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align	8
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.type	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED0Ev, @function
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED0Ev:
.LFB4780:
	.cfi_startproc
	stmg	%r11,%r15,88(%r15)
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-168(%r15)
	.cfi_def_cfa_offset 328
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,160(%r11)
	lg	%r2,160(%r11)
	brasl	%r14,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED1Ev
	lghi	%r3,72
	lg	%r2,160(%r11)
	brasl	%r14,_ZdlPvm@PLT
	lmg	%r11,%r15,256(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE4780:
	.size	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED0Ev, .-_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,comdat
	.align	8
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.type	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, @function
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv:
.LFB4781:
	.cfi_startproc
	stmg	%r10,%r15,80(%r15)
	.cfi_offset 10, -80
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-168(%r15)
	.cfi_def_cfa_offset 328
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,160(%r11)
	lg	%r1,160(%r11)
	aghi	%r1,16
	lgr	%r2,%r1
	brasl	%r14,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_Impl8_M_allocEv
	lgr	%r10,%r2
	lg	%r2,160(%r11)
	brasl	%r14,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv
	lgr	%r1,%r2
	lgr	%r3,%r1
	lgr	%r2,%r10
	brasl	%r14,_ZNSt16allocator_traitsISaI6sphereEE7destroyIS0_EEvRS1_PT_
	nopr	%r0
	lmg	%r10,%r15,248(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_restore 10
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE4781:
	.size	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, .-_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,comdat
	.align	8
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.type	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, @function
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv:
.LFB4782:
	.cfi_startproc
	stmg	%r11,%r15,88(%r15)
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-200(%r15)
	.cfi_def_cfa_offset 360
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,160(%r11)
	ear	%r1,%a0
	sllg	%r1,%r1,32
	ear	%r1,%a1
	mvc	192(8,%r11),40(%r1)
	lg	%r1,160(%r11)
	aghi	%r1,16
	lgr	%r2,%r1
	brasl	%r14,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_Impl8_M_allocEv
	aghik	%r1,%r11,175
	lgr	%r3,%r2
	lgr	%r2,%r1
	brasl	%r14,_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC1IS0_EERKSaIT_E
	aghik	%r2,%r11,175
	aghik	%r1,%r11,176
	lg	%r4,160(%r11)
	lgr	%r3,%r2
	lgr	%r2,%r1
	brasl	%r14,_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC1ERS6_PS5_
	lg	%r2,160(%r11)
	brasl	%r14,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED1Ev
	aghik	%r1,%r11,176
	lgr	%r2,%r1
	brasl	%r14,_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED1Ev
	aghik	%r1,%r11,175
	lgr	%r2,%r1
	brasl	%r14,_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED1Ev
	nopr	%r0
	ear	%r1,%a0
	sllg	%r1,%r1,32
	ear	%r1,%a1
	clc	192(8,%r11),40(%r1)
	je	.L428
	brasl	%r14,__stack_chk_fail@PLT
.L428:
	lmg	%r11,%r15,288(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE4782:
	.size	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, .-_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,comdat
	.align	8
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.type	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, @function
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info:
.LFB4783:
	.cfi_startproc
	stmg	%r11,%r15,88(%r15)
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-184(%r15)
	.cfi_def_cfa_offset 344
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,168(%r11)
	stg	%r3,160(%r11)
	lg	%r2,168(%r11)
	brasl	%r14,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv
	stg	%r2,176(%r11)
	brasl	%r14,_ZNSt19_Sp_make_shared_tag5_S_tiEv
	lg	%r1,160(%r11)
	cgr	%r1,%r2
	je	.L431
	larl	%r3,_ZTISt19_Sp_make_shared_tag
	lg	%r2,160(%r11)
	brasl	%r14,_ZNKSt9type_infoeqERKS_
	lgr	%r1,%r2
	llcr	%r1,%r1
	ltr	%r1,%r1
	je	.L432
.L431:
	lhi	%r1,1
	j	.L433
.L432:
	lhi	%r1,0
.L433:
	llcr	%r1,%r1
	ltr	%r1,%r1
	je	.L434
	lg	%r1,176(%r11)
	j	.L435
.L434:
	lghi	%r1,0
.L435:
	lgr	%r2,%r1
	lmg	%r11,%r15,272(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE4783:
	.size	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, .-_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_Impl8_M_allocEv,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_Impl8_M_allocEv,comdat
	.align	8
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_Impl8_M_allocEv
	.type	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_Impl8_M_allocEv, @function
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_Impl8_M_allocEv:
.LFB4784:
	.cfi_startproc
	stmg	%r11,%r15,88(%r15)
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-168(%r15)
	.cfi_def_cfa_offset 328
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,160(%r11)
	lg	%r2,160(%r11)
	brasl	%r14,_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EE6_S_getERS2_
	lgr	%r1,%r2
	lgr	%r2,%r1
	lmg	%r11,%r15,256(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE4784:
	.size	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_Impl8_M_allocEv, .-_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_Impl8_M_allocEv
	.section	.text._ZNSt16allocator_traitsISaI6sphereEE7destroyIS0_EEvRS1_PT_,"axG",@progbits,_ZNSt16allocator_traitsISaI6sphereEE7destroyIS0_EEvRS1_PT_,comdat
	.align	8
	.weak	_ZNSt16allocator_traitsISaI6sphereEE7destroyIS0_EEvRS1_PT_
	.type	_ZNSt16allocator_traitsISaI6sphereEE7destroyIS0_EEvRS1_PT_, @function
_ZNSt16allocator_traitsISaI6sphereEE7destroyIS0_EEvRS1_PT_:
.LFB4785:
	.cfi_startproc
	stmg	%r11,%r15,88(%r15)
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-176(%r15)
	.cfi_def_cfa_offset 336
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,168(%r11)
	stg	%r3,160(%r11)
	lg	%r3,160(%r11)
	lg	%r2,168(%r11)
	brasl	%r14,_ZN9__gnu_cxx13new_allocatorI6sphereE7destroyIS1_EEvPT_
	nopr	%r0
	lmg	%r11,%r15,264(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE4785:
	.size	_ZNSt16allocator_traitsISaI6sphereEE7destroyIS0_EEvRS1_PT_, .-_ZNSt16allocator_traitsISaI6sphereEE7destroyIS0_EEvRS1_PT_
	.section	.text._ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EE6_S_getERS2_,"axG",@progbits,_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EE6_S_getERS2_,comdat
	.align	8
	.weak	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EE6_S_getERS2_
	.type	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EE6_S_getERS2_, @function
_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EE6_S_getERS2_:
.LFB4786:
	.cfi_startproc
	ldgr	%f2,%r11
	.cfi_register 11, 17
	ldgr	%f0,%r15
	.cfi_register 15, 16
	lay	%r15,-168(%r15)
	.cfi_def_cfa_offset 328
	cg	%r0,160(%r15)
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,160(%r11)
	lg	%r1,160(%r11)
	lgr	%r2,%r1
	lgdr	%r15,%f0
	.cfi_restore 15
	.cfi_def_cfa 15, 160
	lgdr	%r11,%f2
	.cfi_restore 11
	br	%r14
	.cfi_endproc
.LFE4786:
	.size	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EE6_S_getERS2_, .-_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EE6_S_getERS2_
	.section	.text._ZN6sphereD2Ev,"axG",@progbits,_ZN6sphereD5Ev,comdat
	.align	8
	.weak	_ZN6sphereD2Ev
	.type	_ZN6sphereD2Ev, @function
_ZN6sphereD2Ev:
.LFB4789:
	.cfi_startproc
	stmg	%r11,%r15,88(%r15)
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-168(%r15)
	.cfi_def_cfa_offset 328
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,160(%r11)
	lgrl	%r1,_ZTV6sphere@GOTENT
	aghi	%r1,16
	lgr	%r2,%r1
	lg	%r1,160(%r11)
	stg	%r2,0(%r1)
	lg	%r1,160(%r11)
	aghi	%r1,40
	lgr	%r2,%r1
	brasl	%r14,_ZNSt10shared_ptrI8materialED1Ev
	nopr	%r0
	lmg	%r11,%r15,256(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE4789:
	.size	_ZN6sphereD2Ev, .-_ZN6sphereD2Ev
	.weak	_ZN6sphereD1Ev
	.set	_ZN6sphereD1Ev,_ZN6sphereD2Ev
	.section	.text._ZN9__gnu_cxx13new_allocatorI6sphereE7destroyIS1_EEvPT_,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorI6sphereE7destroyIS1_EEvPT_,comdat
	.align	8
	.weak	_ZN9__gnu_cxx13new_allocatorI6sphereE7destroyIS1_EEvPT_
	.type	_ZN9__gnu_cxx13new_allocatorI6sphereE7destroyIS1_EEvPT_, @function
_ZN9__gnu_cxx13new_allocatorI6sphereE7destroyIS1_EEvPT_:
.LFB4787:
	.cfi_startproc
	stmg	%r11,%r15,88(%r15)
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-176(%r15)
	.cfi_def_cfa_offset 336
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	stg	%r2,168(%r11)
	stg	%r3,160(%r11)
	lg	%r2,160(%r11)
	brasl	%r14,_ZN6sphereD1Ev
	nopr	%r0
	lmg	%r11,%r15,264(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE4787:
	.size	_ZN9__gnu_cxx13new_allocatorI6sphereE7destroyIS1_EEvPT_, .-_ZN9__gnu_cxx13new_allocatorI6sphereE7destroyIS1_EEvPT_
	.weak	_ZTISt19_Sp_make_shared_tag
	.section	.data.rel.ro._ZTISt19_Sp_make_shared_tag,"awG",@progbits,_ZTISt19_Sp_make_shared_tag,comdat
	.align	8
	.type	_ZTISt19_Sp_make_shared_tag, @object
	.size	_ZTISt19_Sp_make_shared_tag, 16
_ZTISt19_Sp_make_shared_tag:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSSt19_Sp_make_shared_tag
	.weak	_ZTSSt19_Sp_make_shared_tag
	.section	.rodata._ZTSSt19_Sp_make_shared_tag,"aG",@progbits,_ZTSSt19_Sp_make_shared_tag,comdat
	.align	2
	.type	_ZTSSt19_Sp_make_shared_tag, @object
	.size	_ZTSSt19_Sp_make_shared_tag, 24
_ZTSSt19_Sp_make_shared_tag:
	.string	"St19_Sp_make_shared_tag"
	.weak	_ZTISt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE
	.section	.data.rel.ro._ZTISt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE,"awG",@progbits,_ZTISt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align	8
	.type	_ZTISt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTISt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE, 16
_ZTISt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSSt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE
	.weak	_ZTSSt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE
	.section	.rodata._ZTSSt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE,"aG",@progbits,_ZTSSt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align	2
	.type	_ZTSSt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTSSt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE, 47
_ZTSSt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE:
	.string	"St11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE"
.text
	.align	8
	.type	_GLOBAL__sub_I_main, @function
_GLOBAL__sub_I_main:
.LFB4791:
	.cfi_startproc
	stmg	%r11,%r15,88(%r15)
	.cfi_offset 11, -72
	.cfi_offset 12, -64
	.cfi_offset 13, -56
	.cfi_offset 14, -48
	.cfi_offset 15, -40
	lay	%r15,-160(%r15)
	.cfi_def_cfa_offset 320
	lgr	%r11,%r15
	.cfi_def_cfa_register 11
	llill	%r3,65535
	lghi	%r2,1
	brasl	%r14,_Z41__static_initialization_and_destruction_0ii
	lmg	%r11,%r15,248(%r11)
	.cfi_restore 15
	.cfi_restore 14
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_def_cfa 15, 160
	br	%r14
	.cfi_endproc
.LFE4791:
	.size	_GLOBAL__sub_I_main, .-_GLOBAL__sub_I_main
	.section	.ctors,"aw",@progbits
	.align	8
	.quad	_GLOBAL__sub_I_main
	.weakref	_ZL28__gthrw___pthread_key_createPjPFvPvE,__pthread_key_create
	.hidden	DW.ref.__gxx_personality_v0
	.weak	DW.ref.__gxx_personality_v0
	.section	.data.rel.local.DW.ref.__gxx_personality_v0,"awG",@progbits,DW.ref.__gxx_personality_v0,comdat
	.align	8
	.type	DW.ref.__gxx_personality_v0, @object
	.size	DW.ref.__gxx_personality_v0, 8
DW.ref.__gxx_personality_v0:
	.quad	__gxx_personality_v0
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.4.0-1ubuntu1~20.04.2) 9.4.0"
	.section	.note.GNU-stack,"",@progbits
