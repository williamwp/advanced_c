	.file	"hello.cpp"
	.section	".text"
	.section	.rodata
	.type	_ZStL19piecewise_construct, @object
	.size	_ZStL19piecewise_construct, 1
_ZStL19piecewise_construct:
	.zero	1
	.lcomm	_ZStL8__ioinit,1,1
	.type	_ZStL8__ioinit, @object
	.align 3
.LC0:
	.string	"hello \n "
	.section	".text"
	.align 2
	.globl main
	.section	".opd","aw"
	.align 3
main:
	.quad	.L.main,.TOC.@tocbase,0
	.previous
	.type	main, @function
.L.main:
.LFB1524:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	std 31,-8(1)
	stdu 1,-128(1)
	.cfi_def_cfa_offset 128
	.cfi_offset 65, 16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	addis 3,2,.LC0@toc@ha
	addi 3,3,.LC0@toc@l
	bl printf
	nop
	li 9,0
	mr 3,9
	addi 1,31,128
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,1,0,1
	.cfi_endproc
.LFE1524:
	.size	main,.-.L.main
	.section	".toc","aw"
	.align 3
.LC1:
	.quad	_ZNSt8ios_base4InitD1Ev
	.section	".text"
	.align 2
	.section	".opd","aw"
	.align 3
_Z41__static_initialization_and_destruction_0ii:
	.quad	.L._Z41__static_initialization_and_destruction_0ii,.TOC.@tocbase,0
	.previous
	.type	_Z41__static_initialization_and_destruction_0ii, @function
.L._Z41__static_initialization_and_destruction_0ii:
.LFB2005:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	std 31,-8(1)
	stdu 1,-128(1)
	.cfi_def_cfa_offset 128
	.cfi_offset 65, 16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	mr 9,3
	mr 10,4
	stw 9,176(31)
	mr 9,10
	stw 9,184(31)
	lwz 9,176(31)
	cmpwi 0,9,1
	bne 0,.L5
	lwz 10,184(31)
	li 9,0
	ori 9,9,0xffff
	cmpw 0,10,9
	bne 0,.L5
	addis 3,2,_ZStL8__ioinit@toc@ha
	addi 3,3,_ZStL8__ioinit@toc@l
	bl _ZNSt8ios_base4InitC1Ev
	nop
	addis 5,2,__dso_handle@toc@ha
	addi 5,5,__dso_handle@toc@l
	addis 4,2,_ZStL8__ioinit@toc@ha
	addi 4,4,_ZStL8__ioinit@toc@l
	addis 9,2,.LC1@toc@ha
	ld 3,.LC1@toc@l(9)
	bl __cxa_atexit
	nop
.L5:
	nop
	addi 1,31,128
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,1,0,1
	.cfi_endproc
.LFE2005:
	.size	_Z41__static_initialization_and_destruction_0ii,.-.L._Z41__static_initialization_and_destruction_0ii
	.align 2
	.section	".opd","aw"
	.align 3
_GLOBAL__sub_I_main:
	.quad	.L._GLOBAL__sub_I_main,.TOC.@tocbase,0
	.previous
	.type	_GLOBAL__sub_I_main, @function
.L._GLOBAL__sub_I_main:
.LFB2006:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	std 31,-8(1)
	stdu 1,-128(1)
	.cfi_def_cfa_offset 128
	.cfi_offset 65, 16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	li 4,-1
	rldicl 4,4,0,48
	li 3,1
	bl _Z41__static_initialization_and_destruction_0ii
	addi 1,31,128
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,1,0,1
	.cfi_endproc
.LFE2006:
	.size	_GLOBAL__sub_I_main,.-.L._GLOBAL__sub_I_main
	.section	.ctors,"aw",@progbits
	.align 3
	.quad	_GLOBAL__sub_I_main
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.4.0-1ubuntu1~20.04) 9.4.0"
