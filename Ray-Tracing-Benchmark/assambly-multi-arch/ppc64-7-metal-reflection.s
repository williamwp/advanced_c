	.file	"7-metal-reflection.cpp"
	.section	".text"
	.section	.rodata
	.type	_ZStL19piecewise_construct, @object
	.size	_ZStL19piecewise_construct, 1
_ZStL19piecewise_construct:
	.zero	1
	.globl strcmp
	.section	.text._ZNKSt9type_infoeqERKS_,"axG",@progbits,_ZNKSt9type_infoeqERKS_,comdat
	.align 2
	.weak	_ZNKSt9type_infoeqERKS_
	.section	".opd","aw"
	.align 3
_ZNKSt9type_infoeqERKS_:
	.quad	.L._ZNKSt9type_infoeqERKS_,.TOC.@tocbase,0
	.previous
	.type	_ZNKSt9type_infoeqERKS_, @function
.L._ZNKSt9type_infoeqERKS_:
.LFB750:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	std 31,-8(1)
	stdu 1,-128(1)
	.cfi_def_cfa_offset 128
	.cfi_offset 65, 16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,176(31)
	std 4,184(31)
	ld 9,176(31)
	ld 10,8(9)
	ld 9,184(31)
	ld 9,8(9)
	cmpld 0,10,9
	beq 0,.L2
	ld 9,176(31)
	ld 9,8(9)
	lbz 9,0(9)
	cmplwi 0,9,42
	beq 0,.L3
	ld 9,176(31)
	ld 6,8(9)
	ld 9,184(31)
	ld 7,8(9)
	rldicl 9,6,0,52
	cmpdi 0,9,4032
	bge 0,.L7
	rldicl 9,7,0,52
	cmpdi 0,9,4032
	blt 0,.L11
.L7:
	mr 4,7
	mr 3,6
	bl strcmp
	nop
	mr 9,3
	b .L5
.L11:
	ld 9,0(6)
	ld 8,0(7)
	li 10,0
	cmpb 5,9,8
	cmpb 10,9,10
	orc 10,10,5
	cmpdi 0,10,0
	bne 0,.L8
	ld 9,8(6)
	ld 8,8(7)
	li 10,0
	cmpb 5,9,8
	cmpb 10,9,10
	orc 10,10,5
	cmpdi 0,10,0
	bne 0,.L8
	ld 9,16(6)
	ld 8,16(7)
	li 10,0
	cmpb 5,9,8
	cmpb 10,9,10
	orc 10,10,5
	cmpdi 0,10,0
	bne 0,.L8
	ld 9,24(6)
	ld 8,24(7)
	li 10,0
	cmpb 5,9,8
	cmpb 10,9,10
	orc 10,10,5
	cmpdi 0,10,0
	bne 0,.L8
	ld 9,32(6)
	ld 8,32(7)
	li 10,0
	cmpb 5,9,8
	cmpb 10,9,10
	orc 10,10,5
	cmpdi 0,10,0
	bne 0,.L8
	ld 9,40(6)
	ld 8,40(7)
	li 10,0
	cmpb 5,9,8
	cmpb 10,9,10
	orc 10,10,5
	cmpdi 0,10,0
	bne 0,.L8
	ld 9,48(6)
	ld 8,48(7)
	li 10,0
	cmpb 5,9,8
	cmpb 10,9,10
	orc 10,10,5
	cmpdi 0,10,0
	bne 0,.L8
	ld 9,56(6)
	ld 8,56(7)
	li 10,0
	cmpb 5,9,8
	cmpb 10,9,10
	orc 10,10,5
	cmpdi 0,10,0
	bne 0,.L8
	addi 9,6,64
	addi 10,7,64
	mr 4,10
	mr 3,9
	bl strcmp
	nop
	mr 9,3
	b .L5
.L8:
	cntlzd 10,10
	addi 10,10,8
	rotld 9,9,10
	rldicl 7,9,0,56
	rotld 9,8,10
	rldicl 9,9,0,56
	subf 10,9,7
	mr 9,10
.L5:
	extsw 9,9
	cmpdi 0,9,0
	bne 0,.L3
.L2:
	li 9,1
	b .L9
.L3:
	li 9,0
.L9:
	mr 3,9
	addi 1,31,128
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,1,0,1
	.cfi_endproc
.LFE750:
	.size	_ZNKSt9type_infoeqERKS_,.-.L._ZNKSt9type_infoeqERKS_
	.section	.text._ZnwmPv,"axG",@progbits,_ZnwmPv,comdat
	.align 2
	.weak	_ZnwmPv
	.section	".opd","aw"
	.align 3
_ZnwmPv:
	.quad	.L._ZnwmPv,.TOC.@tocbase,0
	.previous
	.type	_ZnwmPv, @function
.L._ZnwmPv:
.LFB792:
	.cfi_startproc
	std 31,-8(1)
	stdu 1,-64(1)
	.cfi_def_cfa_offset 64
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,112(31)
	std 4,120(31)
	ld 9,120(31)
	mr 3,9
	addi 1,31,64
	.cfi_def_cfa 1, 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,0,128,1,0,1
	.cfi_endproc
.LFE792:
	.size	_ZnwmPv,.-.L._ZnwmPv
	.section	.text._ZdlPvS_,"axG",@progbits,_ZdlPvS_,comdat
	.align 2
	.weak	_ZdlPvS_
	.section	".opd","aw"
	.align 3
_ZdlPvS_:
	.quad	.L._ZdlPvS_,.TOC.@tocbase,0
	.previous
	.type	_ZdlPvS_, @function
.L._ZdlPvS_:
.LFB794:
	.cfi_startproc
	std 31,-8(1)
	stdu 1,-64(1)
	.cfi_def_cfa_offset 64
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,112(31)
	std 4,120(31)
	nop
	addi 1,31,64
	.cfi_def_cfa 1, 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,0,128,1,0,1
	.cfi_endproc
.LFE794:
	.size	_ZdlPvS_,.-.L._ZdlPvS_
	.section	.data.rel.ro,"aw"
	.align 3
	.type	_ZZL18__gthread_active_pvE20__gthread_active_ptr, @object
	.size	_ZZL18__gthread_active_pvE20__gthread_active_ptr, 8
_ZZL18__gthread_active_pvE20__gthread_active_ptr:
	.quad	_ZL28__gthrw___pthread_key_createPjPFvPvE
	.section	".toc","aw"
	.align 3
.LC0:
	.quad	_ZL28__gthrw___pthread_key_createPjPFvPvE
	.section	.text._ZL18__gthread_active_pv,"axG",@progbits,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv,comdat
	.align 2
	.section	".opd","aw"
	.align 3
_ZL18__gthread_active_pv:
	.quad	.L._ZL18__gthread_active_pv,.TOC.@tocbase,0
	.previous
	.type	_ZL18__gthread_active_pv, @function
.L._ZL18__gthread_active_pv:
.LFB968:
	.cfi_startproc
	std 31,-8(1)
	stdu 1,-64(1)
	.cfi_def_cfa_offset 64
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	li 9,1
	addis 10,2,.LC0@toc@ha
	ld 10,.LC0@toc@l(10)
	cmpdi 0,10,0
	bne 0,.L16
	li 9,0
.L16:
	rlwinm 9,9,0,0xff
	extsw 9,9
	mr 3,9
	addi 1,31,64
	.cfi_def_cfa 1, 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,0,128,1,0,1
	.cfi_endproc
.LFE968:
	.size	_ZL18__gthread_active_pv,.-.L._ZL18__gthread_active_pv
	.section	.text._ZN9__gnu_cxxL18__exchange_and_addEPVii,"axG",@progbits,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv,comdat
	.align 2
	.section	".opd","aw"
	.align 3
_ZN9__gnu_cxxL18__exchange_and_addEPVii:
	.quad	.L._ZN9__gnu_cxxL18__exchange_and_addEPVii,.TOC.@tocbase,0
	.previous
	.type	_ZN9__gnu_cxxL18__exchange_and_addEPVii, @function
.L._ZN9__gnu_cxxL18__exchange_and_addEPVii:
.LFB997:
	.cfi_startproc
	std 31,-8(1)
	stdu 1,-64(1)
	.cfi_def_cfa_offset 64
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,112(31)
	mr 9,4
	stw 9,120(31)
	lwz 7,120(31)
	ld 9,112(31)
	lwsync
.L19:
	lwarx 10,0,9
	add 8,10,7
	stwcx. 8,0,9
	bne- 0,.L19
	isync
	rldicl 9,10,0,32
	extsw 9,9
	mr 3,9
	addi 1,31,64
	.cfi_def_cfa 1, 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,0,128,1,0,1
	.cfi_endproc
.LFE997:
	.size	_ZN9__gnu_cxxL18__exchange_and_addEPVii,.-.L._ZN9__gnu_cxxL18__exchange_and_addEPVii
	.section	.text._ZN9__gnu_cxxL25__exchange_and_add_singleEPii,"axG",@progbits,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv,comdat
	.align 2
	.section	".opd","aw"
	.align 3
_ZN9__gnu_cxxL25__exchange_and_add_singleEPii:
	.quad	.L._ZN9__gnu_cxxL25__exchange_and_add_singleEPii,.TOC.@tocbase,0
	.previous
	.type	_ZN9__gnu_cxxL25__exchange_and_add_singleEPii, @function
.L._ZN9__gnu_cxxL25__exchange_and_add_singleEPii:
.LFB999:
	.cfi_startproc
	std 31,-8(1)
	stdu 1,-80(1)
	.cfi_def_cfa_offset 80
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,128(31)
	mr 9,4
	stw 9,136(31)
	ld 9,128(31)
	lwz 9,0(9)
	stw 9,60(31)
	ld 9,128(31)
	lwa 9,0(9)
	lwz 10,136(31)
	add 9,10,9
	extsw 10,9
	ld 9,128(31)
	stw 10,0(9)
	lwa 9,60(31)
	mr 3,9
	addi 1,31,80
	.cfi_def_cfa 1, 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,0,128,1,0,1
	.cfi_endproc
.LFE999:
	.size	_ZN9__gnu_cxxL25__exchange_and_add_singleEPii,.-.L._ZN9__gnu_cxxL25__exchange_and_add_singleEPii
	.section	.text._ZN9__gnu_cxxL27__exchange_and_add_dispatchEPii,"axG",@progbits,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv,comdat
	.align 2
	.section	".opd","aw"
	.align 3
_ZN9__gnu_cxxL27__exchange_and_add_dispatchEPii:
	.quad	.L._ZN9__gnu_cxxL27__exchange_and_add_dispatchEPii,.TOC.@tocbase,0
	.previous
	.type	_ZN9__gnu_cxxL27__exchange_and_add_dispatchEPii, @function
.L._ZN9__gnu_cxxL27__exchange_and_add_dispatchEPii:
.LFB1001:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	std 31,-8(1)
	stdu 1,-128(1)
	.cfi_def_cfa_offset 128
	.cfi_offset 65, 16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,176(31)
	mr 9,4
	stw 9,184(31)
	bl _ZL18__gthread_active_pv
	mr 9,3
	cntlzw 9,9
	srwi 9,9,5
	xori 9,9,0x1
	rlwinm 9,9,0,0xff
	cmpdi 0,9,0
	beq 0,.L24
	lwa 9,184(31)
	mr 4,9
	ld 3,176(31)
	bl _ZN9__gnu_cxxL18__exchange_and_addEPVii
	mr 9,3
	b .L25
.L24:
	lwa 9,184(31)
	mr 4,9
	ld 3,176(31)
	bl _ZN9__gnu_cxxL25__exchange_and_add_singleEPii
	mr 9,3
	nop
.L25:
	mr 3,9
	addi 1,31,128
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,1,0,1
	.cfi_endproc
.LFE1001:
	.size	_ZN9__gnu_cxxL27__exchange_and_add_dispatchEPii,.-.L._ZN9__gnu_cxxL27__exchange_and_add_dispatchEPii
	.section	.rodata
	.align 2
	.type	_ZN9__gnu_cxxL21__default_lock_policyE, @object
	.size	_ZN9__gnu_cxxL21__default_lock_policyE, 4
_ZN9__gnu_cxxL21__default_lock_policyE:
	.long	2
	.type	_ZStL13allocator_arg, @object
	.size	_ZStL13allocator_arg, 1
_ZStL13allocator_arg:
	.zero	1
	.type	_ZStL6ignore, @object
	.size	_ZStL6ignore, 1
_ZStL6ignore:
	.zero	1
	.weak	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag
	.section	.rodata._ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag,"aG",@progbits,_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag,comdat
	.align 3
	.type	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag, @gnu_unique_object
	.size	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag, 16
_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag:
	.zero	16
	.section	.text._ZNSt19_Sp_make_shared_tag5_S_tiEv,"axG",@progbits,_ZNSt19_Sp_make_shared_tag5_S_tiEv,comdat
	.align 2
	.weak	_ZNSt19_Sp_make_shared_tag5_S_tiEv
	.section	".opd","aw"
	.align 3
_ZNSt19_Sp_make_shared_tag5_S_tiEv:
	.quad	.L._ZNSt19_Sp_make_shared_tag5_S_tiEv,.TOC.@tocbase,0
	.previous
	.type	_ZNSt19_Sp_make_shared_tag5_S_tiEv, @function
.L._ZNSt19_Sp_make_shared_tag5_S_tiEv:
.LFB1967:
	.cfi_startproc
	std 31,-8(1)
	stdu 1,-64(1)
	.cfi_def_cfa_offset 64
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	addis 9,2,_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag@toc@ha
	addi 9,9,_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag@toc@l
	mr 3,9
	addi 1,31,64
	.cfi_def_cfa 1, 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,0,128,1,0,1
	.cfi_endproc
.LFE1967:
	.size	_ZNSt19_Sp_make_shared_tag5_S_tiEv,.-.L._ZNSt19_Sp_make_shared_tag5_S_tiEv
	.section	.rodata
	.align 3
	.type	_ZL2pi, @object
	.size	_ZL2pi, 8
_ZL2pi:
	.long	1074340347
	.long	1413754136
	.weak	_ZZ13random_doublevE12distribution
	.section	.bss._ZZ13random_doublevE12distribution,"awG",@nobits,_ZZ13random_doublevE12distribution,comdat
	.align 3
	.type	_ZZ13random_doublevE12distribution, @gnu_unique_object
	.size	_ZZ13random_doublevE12distribution, 16
_ZZ13random_doublevE12distribution:
	.zero	16
	.weak	_ZGVZ13random_doublevE12distribution
	.section	.bss._ZGVZ13random_doublevE12distribution,"awG",@nobits,_ZGVZ13random_doublevE12distribution,comdat
	.align 3
	.type	_ZGVZ13random_doublevE12distribution, @gnu_unique_object
	.size	_ZGVZ13random_doublevE12distribution, 8
_ZGVZ13random_doublevE12distribution:
	.zero	8
	.weak	_ZZ13random_doublevE9generator
	.section	.bss._ZZ13random_doublevE9generator,"awG",@nobits,_ZZ13random_doublevE9generator,comdat
	.align 3
	.type	_ZZ13random_doublevE9generator, @gnu_unique_object
	.size	_ZZ13random_doublevE9generator, 5000
_ZZ13random_doublevE9generator:
	.zero	5000
	.weak	_ZGVZ13random_doublevE9generator
	.section	.bss._ZGVZ13random_doublevE9generator,"awG",@nobits,_ZGVZ13random_doublevE9generator,comdat
	.align 3
	.type	_ZGVZ13random_doublevE9generator, @gnu_unique_object
	.size	_ZGVZ13random_doublevE9generator, 8
_ZGVZ13random_doublevE9generator:
	.zero	8
	.section	.text._Z13random_doublev,"axG",@progbits,_Z13random_doublev,comdat
	.align 2
	.weak	_Z13random_doublev
	.section	".opd","aw"
	.align 3
_Z13random_doublev:
	.quad	.L._Z13random_doublev,.TOC.@tocbase,0
	.previous
	.type	_Z13random_doublev, @function
.L._Z13random_doublev:
.LFB3313:
	.cfi_startproc
	.cfi_personality 0x94,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x14,.LLSDA3313
	mflr 0
	std 0,16(1)
	std 29,-24(1)
	std 30,-16(1)
	std 31,-8(1)
	stdu 1,-144(1)
	.cfi_def_cfa_offset 144
	.cfi_offset 65, 16
	.cfi_offset 29, -24
	.cfi_offset 30, -16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	addis 9,2,_ZGVZ13random_doublevE12distribution@toc@ha
	addi 9,9,_ZGVZ13random_doublevE12distribution@toc@l
	lbz 9,0(9)
	cmpw 0,9,9
	bne- 0,$+4
	isync
	rlwinm 9,9,0,0xff
	cntlzw 9,9
	srwi 9,9,5
	rlwinm 9,9,0,0xff
	cmpdi 0,9,0
	beq 0,.L29
	addis 3,2,_ZGVZ13random_doublevE12distribution@toc@ha
	addi 3,3,_ZGVZ13random_doublevE12distribution@toc@l
	bl __cxa_guard_acquire
	nop
	mr 9,3
	cntlzw 9,9
	srwi 9,9,5
	xori 9,9,0x1
	rlwinm 9,9,0,0xff
	cmpdi 0,9,0
	beq 0,.L29
	li 30,0
	addis 9,2,.LC1@toc@ha
	addi 9,9,.LC1@toc@l
	lfd 0,0(9)
	fmr 2,0
	xxlxor 1,1,1
	addis 3,2,_ZZ13random_doublevE12distribution@toc@ha
	addi 3,3,_ZZ13random_doublevE12distribution@toc@l
.LEHB0:
	bl _ZNSt25uniform_real_distributionIdEC1Edd
	nop
.LEHE0:
	addis 3,2,_ZGVZ13random_doublevE12distribution@toc@ha
	addi 3,3,_ZGVZ13random_doublevE12distribution@toc@l
	bl __cxa_guard_release
	nop
.L29:
	addis 9,2,_ZGVZ13random_doublevE9generator@toc@ha
	addi 9,9,_ZGVZ13random_doublevE9generator@toc@l
	lbz 9,0(9)
	cmpw 0,9,9
	bne- 0,$+4
	isync
	rlwinm 9,9,0,0xff
	cntlzw 9,9
	srwi 9,9,5
	rlwinm 9,9,0,0xff
	cmpdi 0,9,0
	beq 0,.L30
	addis 3,2,_ZGVZ13random_doublevE9generator@toc@ha
	addi 3,3,_ZGVZ13random_doublevE9generator@toc@l
	bl __cxa_guard_acquire
	nop
	mr 9,3
	cntlzw 9,9
	srwi 9,9,5
	xori 9,9,0x1
	rlwinm 9,9,0,0xff
	cmpdi 0,9,0
	beq 0,.L30
	li 30,0
	addis 3,2,_ZZ13random_doublevE9generator@toc@ha
	addi 3,3,_ZZ13random_doublevE9generator@toc@l
.LEHB1:
	bl _ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC1Ev
	nop
.LEHE1:
	addis 3,2,_ZGVZ13random_doublevE9generator@toc@ha
	addi 3,3,_ZGVZ13random_doublevE9generator@toc@l
	bl __cxa_guard_release
	nop
.L30:
	addis 4,2,_ZZ13random_doublevE9generator@toc@ha
	addi 4,4,_ZZ13random_doublevE9generator@toc@l
	addis 3,2,_ZZ13random_doublevE12distribution@toc@ha
	addi 3,3,_ZZ13random_doublevE12distribution@toc@l
.LEHB2:
	bl _ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEEEdRT_
	nop
	fmr 0,1
	b .L38
.L36:
	mr 29,3
	cmpdi 0,30,0
	bne 0,.L33
	addis 3,2,_ZGVZ13random_doublevE12distribution@toc@ha
	addi 3,3,_ZGVZ13random_doublevE12distribution@toc@l
	bl __cxa_guard_abort
	nop
.L33:
	mr 9,29
	mr 3,9
	bl _Unwind_Resume
	nop
.L37:
	mr 29,3
	cmpdi 0,30,0
	bne 0,.L35
	addis 3,2,_ZGVZ13random_doublevE9generator@toc@ha
	addi 3,3,_ZGVZ13random_doublevE9generator@toc@l
	bl __cxa_guard_abort
	nop
.L35:
	mr 9,29
	mr 3,9
	bl _Unwind_Resume
	nop
.LEHE2:
.L38:
	fmr 1,0
	addi 1,31,144
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 29,-24(1)
	ld 30,-16(1)
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,3,0,1
	.cfi_endproc
.LFE3313:
	.globl __gxx_personality_v0
	.section	.gcc_except_table._Z13random_doublev,"aG",@progbits,_Z13random_doublev,comdat
.LLSDA3313:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE3313-.LLSDACSB3313
.LLSDACSB3313:
	.uleb128 .LEHB0-.LFB3313
	.uleb128 .LEHE0-.LEHB0
	.uleb128 .L36-.LFB3313
	.uleb128 0
	.uleb128 .LEHB1-.LFB3313
	.uleb128 .LEHE1-.LEHB1
	.uleb128 .L37-.LFB3313
	.uleb128 0
	.uleb128 .LEHB2-.LFB3313
	.uleb128 .LEHE2-.LEHB2
	.uleb128 0
	.uleb128 0
.LLSDACSE3313:
	.section	.text._Z13random_doublev,"axG",@progbits,_Z13random_doublev,comdat
	.size	_Z13random_doublev,.-.L._Z13random_doublev
	.section	.text._ZplRK4vec3S1_,"axG",@progbits,_ZplRK4vec3S1_,comdat
	.align 2
	.weak	_ZplRK4vec3S1_
	.section	".opd","aw"
	.align 3
_ZplRK4vec3S1_:
	.quad	.L._ZplRK4vec3S1_,.TOC.@tocbase,0
	.previous
	.type	_ZplRK4vec3S1_, @function
.L._ZplRK4vec3S1_:
.LFB3951:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	std 31,-8(1)
	stdu 1,-128(1)
	.cfi_def_cfa_offset 128
	.cfi_offset 65, 16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,176(31)
	std 4,184(31)
	std 5,192(31)
	ld 9,184(31)
	lfd 12,0(9)
	ld 9,192(31)
	lfd 0,0(9)
	fadd 11,12,0
	ld 9,184(31)
	lfd 12,8(9)
	ld 9,192(31)
	lfd 0,8(9)
	fadd 10,12,0
	ld 9,184(31)
	lfd 12,16(9)
	ld 9,192(31)
	lfd 0,16(9)
	fadd 0,12,0
	fmr 3,0
	fmr 2,10
	fmr 1,11
	ld 3,176(31)
	bl _ZN4vec3C1Eddd
	nop
	ld 3,176(31)
	addi 1,31,128
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,1,0,1
	.cfi_endproc
.LFE3951:
	.size	_ZplRK4vec3S1_,.-.L._ZplRK4vec3S1_
	.section	.text._ZmiRK4vec3S1_,"axG",@progbits,_ZmiRK4vec3S1_,comdat
	.align 2
	.weak	_ZmiRK4vec3S1_
	.section	".opd","aw"
	.align 3
_ZmiRK4vec3S1_:
	.quad	.L._ZmiRK4vec3S1_,.TOC.@tocbase,0
	.previous
	.type	_ZmiRK4vec3S1_, @function
.L._ZmiRK4vec3S1_:
.LFB3952:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	std 31,-8(1)
	stdu 1,-128(1)
	.cfi_def_cfa_offset 128
	.cfi_offset 65, 16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,176(31)
	std 4,184(31)
	std 5,192(31)
	ld 9,184(31)
	lfd 12,0(9)
	ld 9,192(31)
	lfd 0,0(9)
	fsub 11,12,0
	ld 9,184(31)
	lfd 12,8(9)
	ld 9,192(31)
	lfd 0,8(9)
	fsub 10,12,0
	ld 9,184(31)
	lfd 12,16(9)
	ld 9,192(31)
	lfd 0,16(9)
	fsub 0,12,0
	fmr 3,0
	fmr 2,10
	fmr 1,11
	ld 3,176(31)
	bl _ZN4vec3C1Eddd
	nop
	ld 3,176(31)
	addi 1,31,128
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,1,0,1
	.cfi_endproc
.LFE3952:
	.size	_ZmiRK4vec3S1_,.-.L._ZmiRK4vec3S1_
	.section	.text._ZmldRK4vec3,"axG",@progbits,_ZmldRK4vec3,comdat
	.align 2
	.weak	_ZmldRK4vec3
	.section	".opd","aw"
	.align 3
_ZmldRK4vec3:
	.quad	.L._ZmldRK4vec3,.TOC.@tocbase,0
	.previous
	.type	_ZmldRK4vec3, @function
.L._ZmldRK4vec3:
.LFB3954:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	std 31,-8(1)
	stdu 1,-128(1)
	.cfi_def_cfa_offset 128
	.cfi_offset 65, 16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,176(31)
	stfd 1,184(31)
	std 5,192(31)
	ld 9,192(31)
	lfd 12,0(9)
	lfd 0,184(31)
	fmul 11,12,0
	ld 9,192(31)
	lfd 12,8(9)
	lfd 0,184(31)
	fmul 10,12,0
	ld 9,192(31)
	lfd 12,16(9)
	lfd 0,184(31)
	fmul 0,12,0
	fmr 3,0
	fmr 2,10
	fmr 1,11
	ld 3,176(31)
	bl _ZN4vec3C1Eddd
	nop
	ld 3,176(31)
	addi 1,31,128
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,1,0,1
	.cfi_endproc
.LFE3954:
	.size	_ZmldRK4vec3,.-.L._ZmldRK4vec3
	.section	.text._Zdv4vec3d,"axG",@progbits,_Zdv4vec3d,comdat
	.align 2
	.weak	_Zdv4vec3d
	.section	".opd","aw"
	.align 3
_Zdv4vec3d:
	.quad	.L._Zdv4vec3d,.TOC.@tocbase,0
	.previous
	.type	_Zdv4vec3d, @function
.L._Zdv4vec3d:
.LFB3956:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	std 31,-8(1)
	stdu 1,-160(1)
	.cfi_def_cfa_offset 160
	.cfi_offset 65, 16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,120(31)
	std 4,216(31)
	std 5,224(31)
	std 6,232(31)
	stfd 1,240(31)
	ld 9,-28688(13)
	std 9,136(31)
	li 9,0
	addis 9,2,.LC1@toc@ha
	addi 9,9,.LC1@toc@l
	lfd 12,0(9)
	lfd 0,240(31)
	fdiv 0,12,0
	ld 9,120(31)
	addi 10,31,216
	mr 5,10
	fmr 1,0
	mr 3,9
	bl _ZmldRK4vec3
	nop
	ld 9,136(31)
	ld 10,-28688(13)
	xor. 9,9,10
	li 10,0
	beq 0,.L47
	bl __stack_chk_fail
	nop
.L47:
	ld 3,120(31)
	addi 1,31,160
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,1,0,1
	.cfi_endproc
.LFE3956:
	.size	_Zdv4vec3d,.-.L._Zdv4vec3d
	.section	.text._Z11unit_vector4vec3,"axG",@progbits,_Z11unit_vector4vec3,comdat
	.align 2
	.weak	_Z11unit_vector4vec3
	.section	".opd","aw"
	.align 3
_Z11unit_vector4vec3:
	.quad	.L._Z11unit_vector4vec3,.TOC.@tocbase,0
	.previous
	.type	_Z11unit_vector4vec3, @function
.L._Z11unit_vector4vec3:
.LFB3959:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	std 31,-8(1)
	stdu 1,-160(1)
	.cfi_def_cfa_offset 160
	.cfi_offset 65, 16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,120(31)
	std 4,216(31)
	std 5,224(31)
	std 6,232(31)
	ld 9,-28688(13)
	std 9,136(31)
	li 9,0
	addi 9,31,216
	mr 3,9
	bl _ZNK4vec36lengthEv
	nop
	fmr 0,1
	ld 9,120(31)
	fmr 1,0
	ld 4,216(31)
	ld 5,224(31)
	ld 6,232(31)
	mr 3,9
	bl _Zdv4vec3d
	nop
	ld 9,136(31)
	ld 10,-28688(13)
	xor. 9,9,10
	li 10,0
	beq 0,.L50
	bl __stack_chk_fail
	nop
.L50:
	ld 3,120(31)
	addi 1,31,160
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,1,0,1
	.cfi_endproc
.LFE3959:
	.size	_Z11unit_vector4vec3,.-.L._Z11unit_vector4vec3
	.lcomm	_ZStL8__ioinit,1,1
	.type	_ZStL8__ioinit, @object
	.section	.text._ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.weak	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	".opd","aw"
	.align 3
_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED2Ev:
	.quad	.L._ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED2Ev,.TOC.@tocbase,0
	.previous
	.type	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
.L._ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED2Ev:
.LFB3965:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	std 31,-8(1)
	stdu 1,-128(1)
	.cfi_def_cfa_offset 128
	.cfi_offset 65, 16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,176(31)
	ld 9,176(31)
	addi 9,9,8
	mr 3,9
	bl _ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED1Ev
	nop
	nop
	addi 1,31,128
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,1,0,1
	.cfi_endproc
.LFE3965:
	.size	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED2Ev,.-.L._ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED1Ev
	.set	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED1Ev,_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt10shared_ptrI8materialED2Ev,"axG",@progbits,_ZNSt10shared_ptrI8materialED5Ev,comdat
	.align 2
	.weak	_ZNSt10shared_ptrI8materialED2Ev
	.section	".opd","aw"
	.align 3
_ZNSt10shared_ptrI8materialED2Ev:
	.quad	.L._ZNSt10shared_ptrI8materialED2Ev,.TOC.@tocbase,0
	.previous
	.type	_ZNSt10shared_ptrI8materialED2Ev, @function
.L._ZNSt10shared_ptrI8materialED2Ev:
.LFB3967:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	std 31,-8(1)
	stdu 1,-128(1)
	.cfi_def_cfa_offset 128
	.cfi_offset 65, 16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,176(31)
	ld 9,176(31)
	mr 3,9
	bl _ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED2Ev
	nop
	nop
	addi 1,31,128
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,1,0,1
	.cfi_endproc
.LFE3967:
	.size	_ZNSt10shared_ptrI8materialED2Ev,.-.L._ZNSt10shared_ptrI8materialED2Ev
	.weak	_ZNSt10shared_ptrI8materialED1Ev
	.set	_ZNSt10shared_ptrI8materialED1Ev,_ZNSt10shared_ptrI8materialED2Ev
	.section	.text._ZN10hit_recordC2Ev,"axG",@progbits,_ZN10hit_recordC5Ev,comdat
	.align 2
	.weak	_ZN10hit_recordC2Ev
	.section	".opd","aw"
	.align 3
_ZN10hit_recordC2Ev:
	.quad	.L._ZN10hit_recordC2Ev,.TOC.@tocbase,0
	.previous
	.type	_ZN10hit_recordC2Ev, @function
.L._ZN10hit_recordC2Ev:
.LFB3969:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	std 31,-8(1)
	stdu 1,-128(1)
	.cfi_def_cfa_offset 128
	.cfi_offset 65, 16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,176(31)
	ld 9,176(31)
	mr 3,9
	bl _ZN4vec3C1Ev
	nop
	ld 9,176(31)
	addi 9,9,24
	mr 3,9
	bl _ZN4vec3C1Ev
	nop
	ld 9,176(31)
	addi 9,9,48
	mr 3,9
	bl _ZNSt10shared_ptrI8materialEC1Ev
	nop
	nop
	addi 1,31,128
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,1,0,1
	.cfi_endproc
.LFE3969:
	.size	_ZN10hit_recordC2Ev,.-.L._ZN10hit_recordC2Ev
	.weak	_ZN10hit_recordC1Ev
	.set	_ZN10hit_recordC1Ev,_ZN10hit_recordC2Ev
	.section	.text._ZN10hit_recordD2Ev,"axG",@progbits,_ZN10hit_recordD5Ev,comdat
	.align 2
	.weak	_ZN10hit_recordD2Ev
	.section	".opd","aw"
	.align 3
_ZN10hit_recordD2Ev:
	.quad	.L._ZN10hit_recordD2Ev,.TOC.@tocbase,0
	.previous
	.type	_ZN10hit_recordD2Ev, @function
.L._ZN10hit_recordD2Ev:
.LFB3972:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	std 31,-8(1)
	stdu 1,-128(1)
	.cfi_def_cfa_offset 128
	.cfi_offset 65, 16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,176(31)
	ld 9,176(31)
	addi 9,9,48
	mr 3,9
	bl _ZNSt10shared_ptrI8materialED1Ev
	nop
	nop
	addi 1,31,128
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,1,0,1
	.cfi_endproc
.LFE3972:
	.size	_ZN10hit_recordD2Ev,.-.L._ZN10hit_recordD2Ev
	.weak	_ZN10hit_recordD1Ev
	.set	_ZN10hit_recordD1Ev,_ZN10hit_recordD2Ev
	.section	".text"
	.align 2
	.section	".opd","aw"
	.align 3
_ZL9ray_colorRK3rayRK8hittablei:
	.quad	.L._ZL9ray_colorRK3rayRK8hittablei,.TOC.@tocbase,0
	.previous
	.type	_ZL9ray_colorRK3rayRK8hittablei, @function
.L._ZL9ray_colorRK3rayRK8hittablei:
.LFB3961:
	.cfi_startproc
	.cfi_personality 0x94,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x14,.LLSDA3961
	mflr 0
	std 0,16(1)
	stfd 31,-8(1)
	std 30,-24(1)
	std 31,-16(1)
	stdu 1,-448(1)
	.cfi_def_cfa_offset 448
	.cfi_offset 65, 16
	.cfi_offset 63, -8
	.cfi_offset 30, -24
	.cfi_offset 31, -16
	mr 31,1
	.cfi_def_cfa_register 31
	std 2,40(1)
	std 3,136(31)
	std 4,128(31)
	std 5,120(31)
	mr 9,6
	stw 9,520(31)
	ld 9,-28688(13)
	std 9,408(31)
	li 9,0
	addi 9,31,328
	mr 3,9
.LEHB3:
	bl _ZN10hit_recordC1Ev
	nop
.LEHE3:
	lwz 9,520(31)
	cmpwi 0,9,0
	bgt 0,.L56
	xxlxor 3,3,3
	xxlxor 2,2,2
	xxlxor 1,1,1
	ld 3,136(31)
.LEHB4:
	bl _ZN4vec3C1Eddd
	nop
	b .L57
.L56:
	addis 9,2,.LC2@toc@ha
	addi 9,9,.LC2@toc@l
	lfd 0,0(9)
	stfd 0,144(31)
	ld 9,120(31)
	ld 9,0(9)
	ld 9,0(9)
	addi 8,31,328
	addis 10,2,.LC3@toc@ha
	addi 10,10,.LC3@toc@l
	lfd 0,0(10)
	mr 7,8
	lfd 2,144(31)
	fmr 1,0
	ld 4,128(31)
	ld 3,120(31)
	ld 10,0(9)
	mtctr 10
	ld 11,16(9)
	ld 2,8(9)
	bctrl
	ld 2,40(1)
	mr 9,3
	cmpdi 0,9,0
	beq 0,.L58
	addi 10,31,256
	addi 9,31,328
	addi 8,9,24
	addi 9,31,328
	mr 5,8
	mr 4,9
	mr 3,10
	bl _ZplRK4vec3S1_
	nop
	addi 9,31,280
	mr 3,9
	bl _Z18random_unit_vectorv
	nop
	addi 9,31,208
	addi 8,31,280
	addi 10,31,256
	mr 5,8
	mr 4,10
	mr 3,9
	bl _ZplRK4vec3S1_
	nop
	addi 9,31,232
	addi 8,31,328
	addi 10,31,208
	mr 5,8
	mr 4,10
	mr 3,9
	bl _ZmiRK4vec3S1_
	nop
	addi 8,31,232
	addi 10,31,328
	addi 9,31,280
	mr 5,8
	mr 4,10
	mr 3,9
	bl _ZN3rayC1ERK4vec3S2_
	nop
	lwz 9,520(31)
	addi 9,9,-1
	extsw 8,9
	addi 9,31,256
	addi 10,31,280
	mr 6,8
	ld 5,120(31)
	mr 4,10
	mr 3,9
	bl _ZL9ray_colorRK3rayRK8hittablei
	ld 10,136(31)
	addi 8,31,256
	addis 9,2,.LC4@toc@ha
	addi 9,9,.LC4@toc@l
	lfd 0,0(9)
	mr 5,8
	fmr 1,0
	mr 3,10
	bl _ZmldRK4vec3
	nop
	b .L57
.L58:
	addi 9,31,280
	ld 4,128(31)
	mr 3,9
	bl _ZNK3ray9directionEv
	nop
	addi 9,31,160
	ld 4,280(31)
	ld 5,288(31)
	ld 6,296(31)
	mr 3,9
	bl _Z11unit_vector4vec3
	nop
	addi 9,31,160
	mr 3,9
	bl _ZNK4vec31yEv
	nop
	fmr 12,1
	addis 9,2,.LC1@toc@ha
	addi 9,9,.LC1@toc@l
	lfd 0,0(9)
	fadd 12,12,0
	addis 9,2,.LC4@toc@ha
	addi 9,9,.LC4@toc@l
	lfd 0,0(9)
	fmul 0,12,0
	stfd 0,152(31)
	addis 9,2,.LC1@toc@ha
	addi 9,9,.LC1@toc@l
	lfd 12,0(9)
	lfd 0,152(31)
	fsub 31,12,0
	addis 9,2,.LC1@toc@ha
	addi 9,9,.LC1@toc@l
	lfd 11,0(9)
	addis 9,2,.LC1@toc@ha
	addi 9,9,.LC1@toc@l
	lfd 12,0(9)
	addis 9,2,.LC1@toc@ha
	addi 9,9,.LC1@toc@l
	lfd 0,0(9)
	addi 9,31,184
	fmr 3,11
	fmr 2,12
	fmr 1,0
	mr 3,9
	bl _ZN4vec3C1Eddd
	nop
	addi 9,31,208
	addi 10,31,184
	mr 5,10
	fmr 1,31
	mr 3,9
	bl _ZmldRK4vec3
	nop
	addis 9,2,.LC1@toc@ha
	addi 9,9,.LC1@toc@l
	lfd 11,0(9)
	addis 9,2,.LC5@toc@ha
	addi 9,9,.LC5@toc@l
	lfd 12,0(9)
	addis 9,2,.LC4@toc@ha
	addi 9,9,.LC4@toc@l
	lfd 0,0(9)
	addi 9,31,232
	fmr 3,11
	fmr 2,12
	fmr 1,0
	mr 3,9
	bl _ZN4vec3C1Eddd
	nop
	addi 9,31,256
	addi 10,31,232
	mr 5,10
	lfd 1,152(31)
	mr 3,9
	bl _ZmldRK4vec3
	nop
	ld 9,136(31)
	addi 8,31,256
	addi 10,31,208
	mr 5,8
	mr 4,10
	mr 3,9
	bl _ZplRK4vec3S1_
	nop
.LEHE4:
.L57:
	addi 9,31,328
	mr 3,9
	bl _ZN10hit_recordD1Ev
	nop
	ld 9,408(31)
	ld 10,-28688(13)
	xor. 9,9,10
	li 10,0
	beq 0,.L61
	b .L63
.L62:
	mr 30,3
	addi 9,31,328
	mr 3,9
	bl _ZN10hit_recordD1Ev
	nop
	mr 9,30
	mr 3,9
.LEHB5:
	bl _Unwind_Resume
	nop
.LEHE5:
.L63:
	bl __stack_chk_fail
	nop
.L61:
	ld 3,136(31)
	addi 1,31,448
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 30,-24(1)
	ld 31,-16(1)
	lfd 31,-8(1)
	blr
	.long 0
	.byte 0,9,2,1,129,2,0,1
	.cfi_endproc
.LFE3961:
	.section	.gcc_except_table,"a",@progbits
.LLSDA3961:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE3961-.LLSDACSB3961
.LLSDACSB3961:
	.uleb128 .LEHB3-.LFB3961
	.uleb128 .LEHE3-.LEHB3
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB4-.LFB3961
	.uleb128 .LEHE4-.LEHB4
	.uleb128 .L62-.LFB3961
	.uleb128 0
	.uleb128 .LEHB5-.LFB3961
	.uleb128 .LEHE5-.LEHB5
	.uleb128 0
	.uleb128 0
.LLSDACSE3961:
	.section	".text"
	.size	_ZL9ray_colorRK3rayRK8hittablei,.-.L._ZL9ray_colorRK3rayRK8hittablei
	.section	".toc","aw"
.LC6:
	.quad	_ZTV13hittable_list+16
	.section	.text._ZN13hittable_listD2Ev,"axG",@progbits,_ZN13hittable_listD5Ev,comdat
	.align 2
	.weak	_ZN13hittable_listD2Ev
	.section	".opd","aw"
	.align 3
_ZN13hittable_listD2Ev:
	.quad	.L._ZN13hittable_listD2Ev,.TOC.@tocbase,0
	.previous
	.type	_ZN13hittable_listD2Ev, @function
.L._ZN13hittable_listD2Ev:
.LFB3976:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	std 31,-8(1)
	stdu 1,-128(1)
	.cfi_def_cfa_offset 128
	.cfi_offset 65, 16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,176(31)
	addis 9,2,.LC6@toc@ha
	ld 10,.LC6@toc@l(9)
	ld 9,176(31)
	std 10,0(9)
	ld 9,176(31)
	addi 9,9,8
	mr 3,9
	bl _ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED1Ev
	nop
	nop
	addi 1,31,128
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,1,0,1
	.cfi_endproc
.LFE3976:
	.size	_ZN13hittable_listD2Ev,.-.L._ZN13hittable_listD2Ev
	.weak	_ZN13hittable_listD1Ev
	.set	_ZN13hittable_listD1Ev,_ZN13hittable_listD2Ev
	.section	.text._ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.weak	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	".opd","aw"
	.align 3
_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED2Ev:
	.quad	.L._ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED2Ev,.TOC.@tocbase,0
	.previous
	.type	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
.L._ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED2Ev:
.LFB3980:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	std 31,-8(1)
	stdu 1,-128(1)
	.cfi_def_cfa_offset 128
	.cfi_offset 65, 16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,176(31)
	ld 9,176(31)
	addi 9,9,8
	mr 3,9
	bl _ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED1Ev
	nop
	nop
	addi 1,31,128
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,1,0,1
	.cfi_endproc
.LFE3980:
	.size	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED2Ev,.-.L._ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED1Ev
	.set	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED1Ev,_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt10shared_ptrI6sphereED2Ev,"axG",@progbits,_ZNSt10shared_ptrI6sphereED5Ev,comdat
	.align 2
	.weak	_ZNSt10shared_ptrI6sphereED2Ev
	.section	".opd","aw"
	.align 3
_ZNSt10shared_ptrI6sphereED2Ev:
	.quad	.L._ZNSt10shared_ptrI6sphereED2Ev,.TOC.@tocbase,0
	.previous
	.type	_ZNSt10shared_ptrI6sphereED2Ev, @function
.L._ZNSt10shared_ptrI6sphereED2Ev:
.LFB3982:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	std 31,-8(1)
	stdu 1,-128(1)
	.cfi_def_cfa_offset 128
	.cfi_offset 65, 16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,176(31)
	ld 9,176(31)
	mr 3,9
	bl _ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED2Ev
	nop
	nop
	addi 1,31,128
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,1,0,1
	.cfi_endproc
.LFE3982:
	.size	_ZNSt10shared_ptrI6sphereED2Ev,.-.L._ZNSt10shared_ptrI6sphereED2Ev
	.weak	_ZNSt10shared_ptrI6sphereED1Ev
	.set	_ZNSt10shared_ptrI6sphereED1Ev,_ZNSt10shared_ptrI6sphereED2Ev
	.section	.text._ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.weak	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	".opd","aw"
	.align 3
_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED2Ev:
	.quad	.L._ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED2Ev,.TOC.@tocbase,0
	.previous
	.type	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
.L._ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED2Ev:
.LFB3986:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	std 31,-8(1)
	stdu 1,-128(1)
	.cfi_def_cfa_offset 128
	.cfi_offset 65, 16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,176(31)
	ld 9,176(31)
	addi 9,9,8
	mr 3,9
	bl _ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED1Ev
	nop
	nop
	addi 1,31,128
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,1,0,1
	.cfi_endproc
.LFE3986:
	.size	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED2Ev,.-.L._ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED1Ev
	.set	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED1Ev,_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt10shared_ptrI8hittableED2Ev,"axG",@progbits,_ZNSt10shared_ptrI8hittableED5Ev,comdat
	.align 2
	.weak	_ZNSt10shared_ptrI8hittableED2Ev
	.section	".opd","aw"
	.align 3
_ZNSt10shared_ptrI8hittableED2Ev:
	.quad	.L._ZNSt10shared_ptrI8hittableED2Ev,.TOC.@tocbase,0
	.previous
	.type	_ZNSt10shared_ptrI8hittableED2Ev, @function
.L._ZNSt10shared_ptrI8hittableED2Ev:
.LFB3988:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	std 31,-8(1)
	stdu 1,-128(1)
	.cfi_def_cfa_offset 128
	.cfi_offset 65, 16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,176(31)
	ld 9,176(31)
	mr 3,9
	bl _ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED2Ev
	nop
	nop
	addi 1,31,128
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,1,0,1
	.cfi_endproc
.LFE3988:
	.size	_ZNSt10shared_ptrI8hittableED2Ev,.-.L._ZNSt10shared_ptrI8hittableED2Ev
	.weak	_ZNSt10shared_ptrI8hittableED1Ev
	.set	_ZNSt10shared_ptrI8hittableED1Ev,_ZNSt10shared_ptrI8hittableED2Ev
	.section	.rodata
	.align 3
.LC10:
	.string	"chapter8.ppm"
	.align 3
.LC11:
	.string	"P3\n"
	.align 3
.LC12:
	.string	"\n255\n"
	.align 3
.LC13:
	.string	"\rScanlines remaining: "
	.align 3
.LC18:
	.string	"\nDone.\n"
	.section	".toc","aw"
.LC14:
	.quad	_ZSt4cerr
.LC15:
	.quad	_ZSt5flushIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_
	.section	".text"
	.align 2
	.globl main
	.section	".opd","aw"
	.align 3
main:
	.quad	.L.main,.TOC.@tocbase,0
	.previous
	.type	main, @function
.L.main:
.LFB3974:
	.cfi_startproc
	.cfi_personality 0x94,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x14,.LLSDA3974
	mflr 0
	std 0,16(1)
	stfd 31,-8(1)
	std 30,-24(1)
	std 31,-16(1)
	stdu 1,-1072(1)
	.cfi_def_cfa_offset 1072
	.cfi_offset 65, 16
	.cfi_offset 63, -8
	.cfi_offset 30, -24
	.cfi_offset 31, -16
	mr 31,1
	.cfi_def_cfa_register 31
	ld 9,-28688(13)
	std 9,1032(31)
	li 9,0
	addis 9,2,.LC7@toc@ha
	addi 9,9,.LC7@toc@l
	lfd 0,0(9)
	stfd 0,160(31)
	li 9,400
	stw 9,136(31)
	li 9,225
	stw 9,140(31)
	li 9,100
	stw 9,144(31)
	li 9,50
	stw 9,148(31)
	addi 9,31,216
	mr 3,9
.LEHB6:
	bl _ZN13hittable_listC1Ev
	nop
.LEHE6:
	addis 9,2,.LC8@toc@ha
	addi 9,9,.LC8@toc@l
	lfd 0,0(9)
	addi 9,31,520
	fmr 3,0
	xxlxor 2,2,2
	xxlxor 1,1,1
	mr 3,9
.LEHB7:
	bl _ZN4vec3C1Eddd
	nop
.LEHE7:
	addis 9,2,.LC4@toc@ha
	addi 9,9,.LC4@toc@l
	lfd 0,0(9)
	stfd 0,152(31)
	addi 9,31,184
	addi 8,31,152
	addi 10,31,520
	mr 5,8
	mr 4,10
	mr 3,9
.LEHB8:
	bl _ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_
	nop
.LEHE8:
	addi 10,31,184
	addi 9,31,200
	mr 4,10
	mr 3,9
	bl _ZNSt10shared_ptrI8hittableEC1I6spherevEEOS_IT_E
	nop
	addi 10,31,200
	addi 9,31,216
	mr 4,10
	mr 3,9
.LEHB9:
	bl _ZN13hittable_list3addESt10shared_ptrI8hittableE
	nop
.LEHE9:
	addi 9,31,200
	mr 3,9
	bl _ZNSt10shared_ptrI8hittableED1Ev
	nop
	addi 9,31,184
	mr 3,9
	bl _ZNSt10shared_ptrI6sphereED1Ev
	nop
	addis 9,2,.LC8@toc@ha
	addi 9,9,.LC8@toc@l
	lfd 12,0(9)
	addis 9,2,.LC9@toc@ha
	addi 9,9,.LC9@toc@l
	lfd 0,0(9)
	addi 9,31,520
	fmr 3,12
	fmr 2,0
	xxlxor 1,1,1
	mr 3,9
.LEHB10:
	bl _ZN4vec3C1Eddd
	nop
.LEHE10:
	li 9,100
	stw 9,152(31)
	addi 9,31,184
	addi 8,31,152
	addi 10,31,520
	mr 5,8
	mr 4,10
	mr 3,9
.LEHB11:
	bl _ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_
	nop
.LEHE11:
	addi 10,31,184
	addi 9,31,200
	mr 4,10
	mr 3,9
	bl _ZNSt10shared_ptrI8hittableEC1I6spherevEEOS_IT_E
	nop
	addi 10,31,200
	addi 9,31,216
	mr 4,10
	mr 3,9
.LEHB12:
	bl _ZN13hittable_list3addESt10shared_ptrI8hittableE
	nop
.LEHE12:
	addi 9,31,200
	mr 3,9
	bl _ZNSt10shared_ptrI8hittableED1Ev
	nop
	addi 9,31,184
	mr 3,9
	bl _ZNSt10shared_ptrI6sphereED1Ev
	nop
	addi 9,31,344
	mr 3,9
.LEHB13:
	bl _ZN6cameraC1Ev
	nop
	addi 9,31,520
	mr 3,9
	bl _ZNSt14basic_ofstreamIcSt11char_traitsIcEEC1Ev
	nop
.LEHE13:
	addi 9,31,520
	li 5,16
	addis 4,2,.LC10@toc@ha
	addi 4,4,.LC10@toc@l
	mr 3,9
.LEHB14:
	bl _ZNSt14basic_ofstreamIcSt11char_traitsIcEE4openEPKcSt13_Ios_Openmode
	nop
	addi 9,31,520
	addis 4,2,.LC11@toc@ha
	addi 4,4,.LC11@toc@l
	mr 3,9
	bl _ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	nop
	mr 9,3
	li 4,400
	mr 3,9
	bl _ZNSolsEi
	nop
	mr 9,3
	li 4,32
	mr 3,9
	bl _ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_c
	nop
	mr 9,3
	li 4,225
	mr 3,9
	bl _ZNSolsEi
	nop
	mr 9,3
	addis 4,2,.LC12@toc@ha
	addi 4,4,.LC12@toc@l
	mr 3,9
	bl _ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	nop
	li 9,224
	stw 9,124(31)
.L75:
	lwz 9,124(31)
	cmpwi 0,9,0
	blt 0,.L70
	addis 4,2,.LC13@toc@ha
	addi 4,4,.LC13@toc@l
	addis 9,2,.LC14@toc@ha
	ld 3,.LC14@toc@l(9)
	bl _ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	nop
	mr 10,3
	lwa 9,124(31)
	mr 4,9
	mr 3,10
	bl _ZNSolsEi
	nop
	mr 9,3
	li 4,32
	mr 3,9
	bl _ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_c
	nop
	mr 9,3
	addis 10,2,.LC15@toc@ha
	ld 4,.LC15@toc@l(10)
	mr 3,9
	bl _ZNSolsEPFRSoS_E
	nop
	li 9,0
	stw 9,128(31)
.L74:
	lwz 9,128(31)
	cmpwi 0,9,399
	bgt 0,.L71
	addi 9,31,248
	xxlxor 3,3,3
	xxlxor 2,2,2
	xxlxor 1,1,1
	mr 3,9
	bl _ZN4vec3C1Eddd
	nop
	li 9,0
	stw 9,132(31)
.L73:
	lwz 9,132(31)
	cmpwi 0,9,99
	bgt 0,.L72
	addi 9,31,128
	lfiwax 0,0,9
	fcfid 31,0
	bl _Z13random_doublev
	nop
	fmr 0,1
	fadd 12,31,0
	addis 9,2,.LC16@toc@ha
	addi 9,9,.LC16@toc@l
	lfd 0,0(9)
	fdiv 0,12,0
	stfd 0,168(31)
	addi 9,31,124
	lfiwax 0,0,9
	fcfid 31,0
	bl _Z13random_doublev
	nop
	fmr 0,1
	fadd 12,31,0
	addis 9,2,.LC17@toc@ha
	addi 9,9,.LC17@toc@l
	lfd 0,0(9)
	fdiv 0,12,0
	stfd 0,176(31)
	addi 9,31,296
	addi 10,31,344
	lfd 2,176(31)
	lfd 1,168(31)
	mr 4,10
	mr 3,9
	bl _ZNK6camera7get_rayEdd
	nop
	addi 9,31,272
	addi 8,31,216
	addi 10,31,296
	li 6,50
	mr 5,8
	mr 4,10
	mr 3,9
	bl _ZL9ray_colorRK3rayRK8hittablei
	addi 10,31,272
	addi 9,31,248
	mr 4,10
	mr 3,9
	bl _ZN4vec3pLERKS_
	nop
	lwz 9,132(31)
	addi 9,9,1
	stw 9,132(31)
	b .L73
.L72:
	addi 9,31,520
	li 7,100
	ld 4,248(31)
	ld 5,256(31)
	ld 6,264(31)
	mr 3,9
	bl _Z11write_colorRSt14basic_ofstreamIcSt11char_traitsIcEE4vec3i
	nop
	lwz 9,128(31)
	addi 9,9,1
	stw 9,128(31)
	b .L74
.L71:
	lwz 9,124(31)
	addi 9,9,-1
	stw 9,124(31)
	b .L75
.L70:
	addis 4,2,.LC18@toc@ha
	addi 4,4,.LC18@toc@l
	addis 9,2,.LC14@toc@ha
	ld 3,.LC14@toc@l(9)
	bl _ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	nop
.LEHE14:
	li 30,0
	addi 9,31,520
	mr 3,9
	bl _ZNSt14basic_ofstreamIcSt11char_traitsIcEED1Ev
	nop
	addi 9,31,216
	mr 3,9
	bl _ZN13hittable_listD1Ev
	nop
	mr 9,30
	ld 10,1032(31)
	ld 8,-28688(13)
	xor. 10,10,8
	li 8,0
	beq 0,.L83
	b .L90
.L86:
	mr 30,3
	addi 9,31,200
	mr 3,9
	bl _ZNSt10shared_ptrI8hittableED1Ev
	nop
	addi 9,31,184
	mr 3,9
	bl _ZNSt10shared_ptrI6sphereED1Ev
	nop
	mr 9,30
	b .L78
.L85:
	mr 9,3
.L78:
	mr 30,9
	b .L79
.L88:
	mr 30,3
	addi 9,31,200
	mr 3,9
	bl _ZNSt10shared_ptrI8hittableED1Ev
	nop
	addi 9,31,184
	mr 3,9
	bl _ZNSt10shared_ptrI6sphereED1Ev
	nop
	mr 9,30
	b .L81
.L87:
	mr 9,3
.L81:
	mr 30,9
	b .L79
.L89:
	mr 30,3
	addi 9,31,520
	mr 3,9
	bl _ZNSt14basic_ofstreamIcSt11char_traitsIcEED1Ev
	nop
	b .L79
.L84:
	mr 30,3
.L79:
	addi 9,31,216
	mr 3,9
	bl _ZN13hittable_listD1Ev
	nop
	mr 9,30
	mr 3,9
.LEHB15:
	bl _Unwind_Resume
	nop
.LEHE15:
.L90:
	bl __stack_chk_fail
	nop
.L83:
	mr 3,9
	addi 1,31,1072
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 30,-24(1)
	ld 31,-16(1)
	lfd 31,-8(1)
	blr
	.long 0
	.byte 0,9,2,1,129,2,0,1
	.cfi_endproc
.LFE3974:
	.section	.gcc_except_table
.LLSDA3974:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE3974-.LLSDACSB3974
.LLSDACSB3974:
	.uleb128 .LEHB6-.LFB3974
	.uleb128 .LEHE6-.LEHB6
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB7-.LFB3974
	.uleb128 .LEHE7-.LEHB7
	.uleb128 .L84-.LFB3974
	.uleb128 0
	.uleb128 .LEHB8-.LFB3974
	.uleb128 .LEHE8-.LEHB8
	.uleb128 .L85-.LFB3974
	.uleb128 0
	.uleb128 .LEHB9-.LFB3974
	.uleb128 .LEHE9-.LEHB9
	.uleb128 .L86-.LFB3974
	.uleb128 0
	.uleb128 .LEHB10-.LFB3974
	.uleb128 .LEHE10-.LEHB10
	.uleb128 .L84-.LFB3974
	.uleb128 0
	.uleb128 .LEHB11-.LFB3974
	.uleb128 .LEHE11-.LEHB11
	.uleb128 .L87-.LFB3974
	.uleb128 0
	.uleb128 .LEHB12-.LFB3974
	.uleb128 .LEHE12-.LEHB12
	.uleb128 .L88-.LFB3974
	.uleb128 0
	.uleb128 .LEHB13-.LFB3974
	.uleb128 .LEHE13-.LEHB13
	.uleb128 .L84-.LFB3974
	.uleb128 0
	.uleb128 .LEHB14-.LFB3974
	.uleb128 .LEHE14-.LEHB14
	.uleb128 .L89-.LFB3974
	.uleb128 0
	.uleb128 .LEHB15-.LFB3974
	.uleb128 .LEHE15-.LEHB15
	.uleb128 0
	.uleb128 0
.LLSDACSE3974:
	.section	".text"
	.size	main,.-.L.main
	.section	.text._ZNSt25uniform_real_distributionIdEC2Edd,"axG",@progbits,_ZNSt25uniform_real_distributionIdEC5Edd,comdat
	.align 2
	.weak	_ZNSt25uniform_real_distributionIdEC2Edd
	.section	".opd","aw"
	.align 3
_ZNSt25uniform_real_distributionIdEC2Edd:
	.quad	.L._ZNSt25uniform_real_distributionIdEC2Edd,.TOC.@tocbase,0
	.previous
	.type	_ZNSt25uniform_real_distributionIdEC2Edd, @function
.L._ZNSt25uniform_real_distributionIdEC2Edd:
.LFB4239:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	std 31,-8(1)
	stdu 1,-128(1)
	.cfi_def_cfa_offset 128
	.cfi_offset 65, 16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,176(31)
	stfd 1,184(31)
	stfd 2,192(31)
	ld 9,176(31)
	lfd 2,192(31)
	lfd 1,184(31)
	mr 3,9
	bl _ZNSt25uniform_real_distributionIdE10param_typeC1Edd
	nop
	nop
	addi 1,31,128
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,1,0,1
	.cfi_endproc
.LFE4239:
	.size	_ZNSt25uniform_real_distributionIdEC2Edd,.-.L._ZNSt25uniform_real_distributionIdEC2Edd
	.weak	_ZNSt25uniform_real_distributionIdEC1Edd
	.set	_ZNSt25uniform_real_distributionIdEC1Edd,_ZNSt25uniform_real_distributionIdEC2Edd
	.section	.text._ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC2Ev,"axG",@progbits,_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC5Ev,comdat
	.align 2
	.weak	_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC2Ev
	.section	".opd","aw"
	.align 3
_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC2Ev:
	.quad	.L._ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC2Ev,.TOC.@tocbase,0
	.previous
	.type	_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC2Ev, @function
.L._ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC2Ev:
.LFB4242:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	std 31,-8(1)
	stdu 1,-128(1)
	.cfi_def_cfa_offset 128
	.cfi_offset 65, 16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,176(31)
	li 4,5489
	ld 3,176(31)
	bl _ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC1Em
	nop
	nop
	addi 1,31,128
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,1,0,1
	.cfi_endproc
.LFE4242:
	.size	_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC2Ev,.-.L._ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC2Ev
	.weak	_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC1Ev
	.set	_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC1Ev,_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC2Ev
	.section	.text._ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEEEdRT_,"axG",@progbits,_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEEEdRT_,comdat
	.align 2
	.weak	_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEEEdRT_
	.section	".opd","aw"
	.align 3
_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEEEdRT_:
	.quad	.L._ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEEEdRT_,.TOC.@tocbase,0
	.previous
	.type	_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEEEdRT_, @function
.L._ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEEEdRT_:
.LFB4244:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	std 31,-8(1)
	stdu 1,-128(1)
	.cfi_def_cfa_offset 128
	.cfi_offset 65, 16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,176(31)
	std 4,184(31)
	ld 9,176(31)
	mr 5,9
	ld 4,184(31)
	ld 3,176(31)
	bl _ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEEEdRT_RKNS0_10param_typeE
	nop
	fmr 0,1
	fmr 1,0
	addi 1,31,128
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,1,0,1
	.cfi_endproc
.LFE4244:
	.size	_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEEEdRT_,.-.L._ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEEEdRT_
	.section	.text._ZNSt10shared_ptrI8materialEC2Ev,"axG",@progbits,_ZNSt10shared_ptrI8materialEC5Ev,comdat
	.align 2
	.weak	_ZNSt10shared_ptrI8materialEC2Ev
	.section	".opd","aw"
	.align 3
_ZNSt10shared_ptrI8materialEC2Ev:
	.quad	.L._ZNSt10shared_ptrI8materialEC2Ev,.TOC.@tocbase,0
	.previous
	.type	_ZNSt10shared_ptrI8materialEC2Ev, @function
.L._ZNSt10shared_ptrI8materialEC2Ev:
.LFB4272:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	std 31,-8(1)
	stdu 1,-128(1)
	.cfi_def_cfa_offset 128
	.cfi_offset 65, 16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,176(31)
	ld 9,176(31)
	mr 3,9
	bl _ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC2Ev
	nop
	nop
	addi 1,31,128
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,1,0,1
	.cfi_endproc
.LFE4272:
	.size	_ZNSt10shared_ptrI8materialEC2Ev,.-.L._ZNSt10shared_ptrI8materialEC2Ev
	.weak	_ZNSt10shared_ptrI8materialEC1Ev
	.set	_ZNSt10shared_ptrI8materialEC1Ev,_ZNSt10shared_ptrI8materialEC2Ev
	.section	.text._ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	".opd","aw"
	.align 3
_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED2Ev:
	.quad	.L._ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED2Ev,.TOC.@tocbase,0
	.previous
	.type	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
.L._ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED2Ev:
.LFB4275:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	std 31,-8(1)
	stdu 1,-128(1)
	.cfi_def_cfa_offset 128
	.cfi_offset 65, 16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,176(31)
	ld 9,176(31)
	ld 9,0(9)
	cmpdi 0,9,0
	beq 0,.L98
	ld 9,176(31)
	ld 9,0(9)
	mr 3,9
	bl _ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv
	nop
.L98:
	nop
	addi 1,31,128
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,1,0,1
	.cfi_endproc
.LFE4275:
	.size	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED2Ev,.-.L._ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED1Ev
	.set	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED1Ev,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED2Ev,"axG",@progbits,_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED5Ev,comdat
	.align 2
	.weak	_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED2Ev
	.section	".opd","aw"
	.align 3
_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED2Ev:
	.quad	.L._ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED2Ev,.TOC.@tocbase,0
	.previous
	.type	_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED2Ev, @function
.L._ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED2Ev:
.LFB4278:
	.cfi_startproc
	.cfi_personality 0x94,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x14,.LLSDA4278
	mflr 0
	std 0,16(1)
	std 29,-24(1)
	std 30,-16(1)
	std 31,-8(1)
	stdu 1,-144(1)
	.cfi_def_cfa_offset 144
	.cfi_offset 65, 16
	.cfi_offset 29, -24
	.cfi_offset 30, -16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,192(31)
	ld 9,192(31)
	ld 30,0(9)
	ld 9,192(31)
	ld 29,8(9)
	ld 9,192(31)
	mr 3,9
	bl _ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE19_M_get_Tp_allocatorEv
	nop
	mr 9,3
	mr 5,9
	mr 4,29
	mr 3,30
	bl _ZSt8_DestroyIPSt10shared_ptrI8hittableES2_EvT_S4_RSaIT0_E
	nop
	ld 9,192(31)
	mr 3,9
	bl _ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED2Ev
	nop
	nop
	addi 1,31,144
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 29,-24(1)
	ld 30,-16(1)
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,3,0,1
	.cfi_endproc
.LFE4278:
	.section	.gcc_except_table
.LLSDA4278:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4278-.LLSDACSB4278
.LLSDACSB4278:
.LLSDACSE4278:
	.section	.text._ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED2Ev,"axG",@progbits,_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED5Ev,comdat
	.size	_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED2Ev,.-.L._ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED2Ev
	.weak	_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED1Ev
	.set	_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED1Ev,_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED2Ev
	.section	.text._ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_,"axG",@progbits,_ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_,comdat
	.align 2
	.weak	_ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_
	.section	".opd","aw"
	.align 3
_ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_:
	.quad	.L._ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_,.TOC.@tocbase,0
	.previous
	.type	_ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_, @function
.L._ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_:
.LFB4280:
	.cfi_startproc
	.cfi_personality 0x94,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x14,.LLSDA4280
	mflr 0
	std 0,16(1)
	std 30,-16(1)
	std 31,-8(1)
	stdu 1,-176(1)
	.cfi_def_cfa_offset 176
	.cfi_offset 65, 16
	.cfi_offset 30, -16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,136(31)
	std 4,128(31)
	std 5,120(31)
	ld 9,-28688(13)
	std 9,152(31)
	li 9,0
	addi 9,31,151
	mr 3,9
	bl _ZNSaI6sphereEC1Ev
	nop
	ld 3,128(31)
	bl _ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
	nop
	mr 30,3
	ld 3,120(31)
	bl _ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE
	nop
	mr 8,3
	ld 9,136(31)
	addi 10,31,151
	mr 6,8
	mr 5,30
	mr 4,10
	mr 3,9
.LEHB16:
	bl _ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3dEESt10shared_ptrIT_ERKT0_DpOT1_
	nop
.LEHE16:
	addi 9,31,151
	mr 3,9
	bl _ZNSaI6sphereED1Ev
	nop
	ld 9,152(31)
	ld 10,-28688(13)
	xor. 9,9,10
	li 10,0
	beq 0,.L103
	b .L105
.L104:
	mr 30,3
	addi 9,31,151
	mr 3,9
	bl _ZNSaI6sphereED1Ev
	nop
	mr 9,30
	mr 3,9
.LEHB17:
	bl _Unwind_Resume
	nop
.LEHE17:
.L105:
	bl __stack_chk_fail
	nop
.L103:
	ld 3,136(31)
	addi 1,31,176
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 30,-16(1)
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,2,0,1
	.cfi_endproc
.LFE4280:
	.section	.gcc_except_table
.LLSDA4280:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4280-.LLSDACSB4280
.LLSDACSB4280:
	.uleb128 .LEHB16-.LFB4280
	.uleb128 .LEHE16-.LEHB16
	.uleb128 .L104-.LFB4280
	.uleb128 0
	.uleb128 .LEHB17-.LFB4280
	.uleb128 .LEHE17-.LEHB17
	.uleb128 0
	.uleb128 0
.LLSDACSE4280:
	.section	.text._ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_,"axG",@progbits,_ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_,comdat
	.size	_ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_,.-.L._ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_
	.section	.text._ZNSt10shared_ptrI8hittableEC2I6spherevEEOS_IT_E,"axG",@progbits,_ZNSt10shared_ptrI8hittableEC5I6spherevEEOS_IT_E,comdat
	.align 2
	.weak	_ZNSt10shared_ptrI8hittableEC2I6spherevEEOS_IT_E
	.section	".opd","aw"
	.align 3
_ZNSt10shared_ptrI8hittableEC2I6spherevEEOS_IT_E:
	.quad	.L._ZNSt10shared_ptrI8hittableEC2I6spherevEEOS_IT_E,.TOC.@tocbase,0
	.previous
	.type	_ZNSt10shared_ptrI8hittableEC2I6spherevEEOS_IT_E, @function
.L._ZNSt10shared_ptrI8hittableEC2I6spherevEEOS_IT_E:
.LFB4282:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	std 30,-16(1)
	std 31,-8(1)
	stdu 1,-128(1)
	.cfi_def_cfa_offset 128
	.cfi_offset 65, 16
	.cfi_offset 30, -16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,176(31)
	std 4,184(31)
	ld 30,176(31)
	ld 3,184(31)
	bl _ZSt4moveIRSt10shared_ptrI6sphereEEONSt16remove_referenceIT_E4typeEOS5_
	nop
	mr 9,3
	mr 4,9
	mr 3,30
	bl _ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC2I6spherevEEOS_IT_LS2_2EE
	nop
	nop
	addi 1,31,128
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 30,-16(1)
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,2,0,1
	.cfi_endproc
.LFE4282:
	.size	_ZNSt10shared_ptrI8hittableEC2I6spherevEEOS_IT_E,.-.L._ZNSt10shared_ptrI8hittableEC2I6spherevEEOS_IT_E
	.weak	_ZNSt10shared_ptrI8hittableEC1I6spherevEEOS_IT_E
	.set	_ZNSt10shared_ptrI8hittableEC1I6spherevEEOS_IT_E,_ZNSt10shared_ptrI8hittableEC2I6spherevEEOS_IT_E
	.section	.text._ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_,"axG",@progbits,_ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_,comdat
	.align 2
	.weak	_ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_
	.section	".opd","aw"
	.align 3
_ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_:
	.quad	.L._ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_,.TOC.@tocbase,0
	.previous
	.type	_ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_, @function
.L._ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_:
.LFB4287:
	.cfi_startproc
	.cfi_personality 0x94,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x14,.LLSDA4287
	mflr 0
	std 0,16(1)
	std 30,-16(1)
	std 31,-8(1)
	stdu 1,-176(1)
	.cfi_def_cfa_offset 176
	.cfi_offset 65, 16
	.cfi_offset 30, -16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,136(31)
	std 4,128(31)
	std 5,120(31)
	ld 9,-28688(13)
	std 9,152(31)
	li 9,0
	addi 9,31,151
	mr 3,9
	bl _ZNSaI6sphereEC1Ev
	nop
	ld 3,128(31)
	bl _ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
	nop
	mr 30,3
	ld 3,120(31)
	bl _ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE
	nop
	mr 8,3
	ld 9,136(31)
	addi 10,31,151
	mr 6,8
	mr 5,30
	mr 4,10
	mr 3,9
.LEHB18:
	bl _ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3iEESt10shared_ptrIT_ERKT0_DpOT1_
	nop
.LEHE18:
	addi 9,31,151
	mr 3,9
	bl _ZNSaI6sphereED1Ev
	nop
	ld 9,152(31)
	ld 10,-28688(13)
	xor. 9,9,10
	li 10,0
	beq 0,.L110
	b .L112
.L111:
	mr 30,3
	addi 9,31,151
	mr 3,9
	bl _ZNSaI6sphereED1Ev
	nop
	mr 9,30
	mr 3,9
.LEHB19:
	bl _Unwind_Resume
	nop
.LEHE19:
.L112:
	bl __stack_chk_fail
	nop
.L110:
	ld 3,136(31)
	addi 1,31,176
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 30,-16(1)
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,2,0,1
	.cfi_endproc
.LFE4287:
	.section	.gcc_except_table
.LLSDA4287:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4287-.LLSDACSB4287
.LLSDACSB4287:
	.uleb128 .LEHB18-.LFB4287
	.uleb128 .LEHE18-.LEHB18
	.uleb128 .L111-.LFB4287
	.uleb128 0
	.uleb128 .LEHB19-.LFB4287
	.uleb128 .LEHE19-.LEHB19
	.uleb128 0
	.uleb128 0
.LLSDACSE4287:
	.section	.text._ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_,"axG",@progbits,_ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_,comdat
	.size	_ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_,.-.L._ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_
	.section	.text._ZNSt25uniform_real_distributionIdE10param_typeC2Edd,"axG",@progbits,_ZNSt25uniform_real_distributionIdE10param_typeC5Edd,comdat
	.align 2
	.weak	_ZNSt25uniform_real_distributionIdE10param_typeC2Edd
	.section	".opd","aw"
	.align 3
_ZNSt25uniform_real_distributionIdE10param_typeC2Edd:
	.quad	.L._ZNSt25uniform_real_distributionIdE10param_typeC2Edd,.TOC.@tocbase,0
	.previous
	.type	_ZNSt25uniform_real_distributionIdE10param_typeC2Edd, @function
.L._ZNSt25uniform_real_distributionIdE10param_typeC2Edd:
.LFB4414:
	.cfi_startproc
	std 31,-8(1)
	stdu 1,-64(1)
	.cfi_def_cfa_offset 64
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,112(31)
	stfd 1,120(31)
	stfd 2,128(31)
	ld 9,112(31)
	lfd 0,120(31)
	stfd 0,0(9)
	ld 9,112(31)
	lfd 0,128(31)
	stfd 0,8(9)
	nop
	addi 1,31,64
	.cfi_def_cfa 1, 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,0,128,1,0,1
	.cfi_endproc
.LFE4414:
	.size	_ZNSt25uniform_real_distributionIdE10param_typeC2Edd,.-.L._ZNSt25uniform_real_distributionIdE10param_typeC2Edd
	.weak	_ZNSt25uniform_real_distributionIdE10param_typeC1Edd
	.set	_ZNSt25uniform_real_distributionIdE10param_typeC1Edd,_ZNSt25uniform_real_distributionIdE10param_typeC2Edd
	.section	.text._ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC2Em,"axG",@progbits,_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC5Em,comdat
	.align 2
	.weak	_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC2Em
	.section	".opd","aw"
	.align 3
_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC2Em:
	.quad	.L._ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC2Em,.TOC.@tocbase,0
	.previous
	.type	_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC2Em, @function
.L._ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC2Em:
.LFB4417:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	std 31,-8(1)
	stdu 1,-128(1)
	.cfi_def_cfa_offset 128
	.cfi_offset 65, 16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,176(31)
	std 4,184(31)
	ld 4,184(31)
	ld 3,176(31)
	bl _ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE4seedEm
	nop
	nop
	addi 1,31,128
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,1,0,1
	.cfi_endproc
.LFE4417:
	.size	_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC2Em,.-.L._ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC2Em
	.weak	_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC1Em
	.set	_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC1Em,_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC2Em
	.section	.text._ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEEEdRT_RKNS0_10param_typeE,"axG",@progbits,_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEEEdRT_RKNS0_10param_typeE,comdat
	.align 2
	.weak	_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEEEdRT_RKNS0_10param_typeE
	.section	".opd","aw"
	.align 3
_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEEEdRT_RKNS0_10param_typeE:
	.quad	.L._ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEEEdRT_RKNS0_10param_typeE,.TOC.@tocbase,0
	.previous
	.type	_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEEEdRT_RKNS0_10param_typeE, @function
.L._ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEEEdRT_RKNS0_10param_typeE:
.LFB4419:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	stfd 30,-16(1)
	stfd 31,-8(1)
	std 31,-24(1)
	stdu 1,-192(1)
	.cfi_def_cfa_offset 192
	.cfi_offset 65, 16
	.cfi_offset 62, -16
	.cfi_offset 63, -8
	.cfi_offset 31, -24
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,136(31)
	std 4,128(31)
	std 5,120(31)
	ld 9,-28688(13)
	std 9,152(31)
	li 9,0
	addi 9,31,144
	ld 4,128(31)
	mr 3,9
	bl _ZNSt8__detail8_AdaptorISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEdEC1ERS2_
	nop
	addi 9,31,144
	mr 3,9
	bl _ZNSt8__detail8_AdaptorISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEdEclEv
	nop
	fmr 31,1
	ld 3,120(31)
	bl _ZNKSt25uniform_real_distributionIdE10param_type1bEv
	nop
	fmr 30,1
	ld 3,120(31)
	bl _ZNKSt25uniform_real_distributionIdE10param_type1aEv
	nop
	fmr 0,1
	fsub 0,30,0
	fmul 31,31,0
	ld 3,120(31)
	bl _ZNKSt25uniform_real_distributionIdE10param_type1aEv
	nop
	fmr 0,1
	fadd 0,31,0
	ld 9,152(31)
	ld 10,-28688(13)
	xor. 9,9,10
	li 10,0
	beq 0,.L117
	bl __stack_chk_fail
	nop
.L117:
	fmr 1,0
	addi 1,31,192
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 31,-24(1)
	lfd 30,-16(1)
	lfd 31,-8(1)
	blr
	.long 0
	.byte 0,9,2,1,130,1,0,1
	.cfi_endproc
.LFE4419:
	.size	_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEEEdRT_RKNS0_10param_typeE,.-.L._ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEEEdRT_RKNS0_10param_typeE
	.section	.text._ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC2Ev,"axG",@progbits,_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC5Ev,comdat
	.align 2
	.weak	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC2Ev
	.section	".opd","aw"
	.align 3
_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC2Ev:
	.quad	.L._ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC2Ev,.TOC.@tocbase,0
	.previous
	.type	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC2Ev, @function
.L._ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC2Ev:
.LFB4431:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	std 31,-8(1)
	stdu 1,-128(1)
	.cfi_def_cfa_offset 128
	.cfi_offset 65, 16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,176(31)
	ld 9,176(31)
	li 10,0
	std 10,0(9)
	ld 9,176(31)
	addi 9,9,8
	mr 3,9
	bl _ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1Ev
	nop
	nop
	addi 1,31,128
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,1,0,1
	.cfi_endproc
.LFE4431:
	.size	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC2Ev,.-.L._ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC2Ev
	.weak	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC1Ev
	.set	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC1Ev,_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC2Ev
	.section	.text._ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv,"axG",@progbits,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv,comdat
	.align 2
	.weak	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv
	.section	".opd","aw"
	.align 3
_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv:
	.quad	.L._ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv,.TOC.@tocbase,0
	.previous
	.type	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv, @function
.L._ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv:
.LFB4433:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	std 31,-8(1)
	stdu 1,-128(1)
	.cfi_def_cfa_offset 128
	.cfi_offset 65, 16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 2,40(1)
	std 3,176(31)
	ld 9,176(31)
	addi 9,9,8
	li 4,-1
	mr 3,9
	bl _ZN9__gnu_cxxL27__exchange_and_add_dispatchEPii
	mr 9,3
	xori 9,9,0x1
	cntlzw 9,9
	srwi 9,9,5
	rlwinm 9,9,0,0xff
	cmpdi 0,9,0
	beq 0,.L121
	ld 9,176(31)
	ld 9,0(9)
	addi 9,9,16
	ld 9,0(9)
	ld 3,176(31)
	ld 10,0(9)
	mtctr 10
	ld 11,16(9)
	ld 2,8(9)
	bctrl
	ld 2,40(1)
	ld 9,176(31)
	addi 9,9,12
	li 4,-1
	mr 3,9
	bl _ZN9__gnu_cxxL27__exchange_and_add_dispatchEPii
	mr 9,3
	xori 9,9,0x1
	cntlzw 9,9
	srwi 9,9,5
	rlwinm 9,9,0,0xff
	cmpdi 0,9,0
	beq 0,.L121
	ld 9,176(31)
	ld 9,0(9)
	addi 9,9,24
	ld 9,0(9)
	ld 3,176(31)
	ld 10,0(9)
	mtctr 10
	ld 11,16(9)
	ld 2,8(9)
	bctrl
	ld 2,40(1)
.L121:
	nop
	addi 1,31,128
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,1,0,1
	.cfi_endproc
.LFE4433:
	.size	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv,.-.L._ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv
	.section	.text._ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD2Ev,"axG",@progbits,_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD5Ev,comdat
	.align 2
	.weak	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD2Ev
	.section	".opd","aw"
	.align 3
_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD2Ev:
	.quad	.L._ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD2Ev,.TOC.@tocbase,0
	.previous
	.type	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD2Ev, @function
.L._ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD2Ev:
.LFB4436:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	std 31,-8(1)
	stdu 1,-128(1)
	.cfi_def_cfa_offset 128
	.cfi_offset 65, 16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,176(31)
	ld 3,176(31)
	bl _ZNSaISt10shared_ptrI8hittableEED2Ev
	nop
	nop
	addi 1,31,128
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,1,0,1
	.cfi_endproc
.LFE4436:
	.size	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD2Ev,.-.L._ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD2Ev
	.weak	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD1Ev
	.set	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD1Ev,_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD2Ev
	.section	.text._ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED2Ev,"axG",@progbits,_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED5Ev,comdat
	.align 2
	.weak	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED2Ev
	.section	".opd","aw"
	.align 3
_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED2Ev:
	.quad	.L._ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED2Ev,.TOC.@tocbase,0
	.previous
	.type	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED2Ev, @function
.L._ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED2Ev:
.LFB4438:
	.cfi_startproc
	.cfi_personality 0x94,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x14,.LLSDA4438
	mflr 0
	std 0,16(1)
	std 31,-8(1)
	stdu 1,-128(1)
	.cfi_def_cfa_offset 128
	.cfi_offset 65, 16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,176(31)
	ld 9,176(31)
	ld 8,0(9)
	ld 9,176(31)
	ld 10,16(9)
	ld 9,176(31)
	ld 9,0(9)
	subf 9,9,10
	sradi 9,9,4
	mr 5,9
	mr 4,8
	ld 3,176(31)
	bl _ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE13_M_deallocateEPS2_m
	nop
	ld 9,176(31)
	mr 3,9
	bl _ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD1Ev
	nop
	nop
	addi 1,31,128
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,1,0,1
	.cfi_endproc
.LFE4438:
	.section	.gcc_except_table
.LLSDA4438:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4438-.LLSDACSB4438
.LLSDACSB4438:
.LLSDACSE4438:
	.section	.text._ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED2Ev,"axG",@progbits,_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED5Ev,comdat
	.size	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED2Ev,.-.L._ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED2Ev
	.weak	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED1Ev
	.set	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED1Ev,_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED2Ev
	.section	.text._ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE19_M_get_Tp_allocatorEv,"axG",@progbits,_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE19_M_get_Tp_allocatorEv,comdat
	.align 2
	.weak	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE19_M_get_Tp_allocatorEv
	.section	".opd","aw"
	.align 3
_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE19_M_get_Tp_allocatorEv:
	.quad	.L._ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE19_M_get_Tp_allocatorEv,.TOC.@tocbase,0
	.previous
	.type	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE19_M_get_Tp_allocatorEv, @function
.L._ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE19_M_get_Tp_allocatorEv:
.LFB4440:
	.cfi_startproc
	std 31,-8(1)
	stdu 1,-64(1)
	.cfi_def_cfa_offset 64
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,112(31)
	ld 9,112(31)
	mr 3,9
	addi 1,31,64
	.cfi_def_cfa 1, 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,0,128,1,0,1
	.cfi_endproc
.LFE4440:
	.size	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE19_M_get_Tp_allocatorEv,.-.L._ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE19_M_get_Tp_allocatorEv
	.section	.text._ZSt8_DestroyIPSt10shared_ptrI8hittableES2_EvT_S4_RSaIT0_E,"axG",@progbits,_ZSt8_DestroyIPSt10shared_ptrI8hittableES2_EvT_S4_RSaIT0_E,comdat
	.align 2
	.weak	_ZSt8_DestroyIPSt10shared_ptrI8hittableES2_EvT_S4_RSaIT0_E
	.section	".opd","aw"
	.align 3
_ZSt8_DestroyIPSt10shared_ptrI8hittableES2_EvT_S4_RSaIT0_E:
	.quad	.L._ZSt8_DestroyIPSt10shared_ptrI8hittableES2_EvT_S4_RSaIT0_E,.TOC.@tocbase,0
	.previous
	.type	_ZSt8_DestroyIPSt10shared_ptrI8hittableES2_EvT_S4_RSaIT0_E, @function
.L._ZSt8_DestroyIPSt10shared_ptrI8hittableES2_EvT_S4_RSaIT0_E:
.LFB4441:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	std 31,-8(1)
	stdu 1,-128(1)
	.cfi_def_cfa_offset 128
	.cfi_offset 65, 16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,176(31)
	std 4,184(31)
	std 5,192(31)
	ld 4,184(31)
	ld 3,176(31)
	bl _ZSt8_DestroyIPSt10shared_ptrI8hittableEEvT_S4_
	nop
	nop
	addi 1,31,128
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,1,0,1
	.cfi_endproc
.LFE4441:
	.size	_ZSt8_DestroyIPSt10shared_ptrI8hittableES2_EvT_S4_RSaIT0_E,.-.L._ZSt8_DestroyIPSt10shared_ptrI8hittableES2_EvT_S4_RSaIT0_E
	.section	.text._ZNSaI6sphereEC2Ev,"axG",@progbits,_ZNSaI6sphereEC5Ev,comdat
	.align 2
	.weak	_ZNSaI6sphereEC2Ev
	.section	".opd","aw"
	.align 3
_ZNSaI6sphereEC2Ev:
	.quad	.L._ZNSaI6sphereEC2Ev,.TOC.@tocbase,0
	.previous
	.type	_ZNSaI6sphereEC2Ev, @function
.L._ZNSaI6sphereEC2Ev:
.LFB4443:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	std 31,-8(1)
	stdu 1,-128(1)
	.cfi_def_cfa_offset 128
	.cfi_offset 65, 16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,176(31)
	ld 3,176(31)
	bl _ZN9__gnu_cxx13new_allocatorI6sphereEC2Ev
	nop
	nop
	addi 1,31,128
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,1,0,1
	.cfi_endproc
.LFE4443:
	.size	_ZNSaI6sphereEC2Ev,.-.L._ZNSaI6sphereEC2Ev
	.weak	_ZNSaI6sphereEC1Ev
	.set	_ZNSaI6sphereEC1Ev,_ZNSaI6sphereEC2Ev
	.section	.text._ZNSaI6sphereED2Ev,"axG",@progbits,_ZNSaI6sphereED5Ev,comdat
	.align 2
	.weak	_ZNSaI6sphereED2Ev
	.section	".opd","aw"
	.align 3
_ZNSaI6sphereED2Ev:
	.quad	.L._ZNSaI6sphereED2Ev,.TOC.@tocbase,0
	.previous
	.type	_ZNSaI6sphereED2Ev, @function
.L._ZNSaI6sphereED2Ev:
.LFB4446:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	std 31,-8(1)
	stdu 1,-128(1)
	.cfi_def_cfa_offset 128
	.cfi_offset 65, 16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,176(31)
	ld 3,176(31)
	bl _ZN9__gnu_cxx13new_allocatorI6sphereED2Ev
	nop
	nop
	addi 1,31,128
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,1,0,1
	.cfi_endproc
.LFE4446:
	.size	_ZNSaI6sphereED2Ev,.-.L._ZNSaI6sphereED2Ev
	.weak	_ZNSaI6sphereED1Ev
	.set	_ZNSaI6sphereED1Ev,_ZNSaI6sphereED2Ev
	.section	.text._ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE,"axG",@progbits,_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE,comdat
	.align 2
	.weak	_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
	.section	".opd","aw"
	.align 3
_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE:
	.quad	.L._ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE,.TOC.@tocbase,0
	.previous
	.type	_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE, @function
.L._ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE:
.LFB4448:
	.cfi_startproc
	std 31,-8(1)
	stdu 1,-64(1)
	.cfi_def_cfa_offset 64
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,112(31)
	ld 9,112(31)
	mr 3,9
	addi 1,31,64
	.cfi_def_cfa 1, 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,0,128,1,0,1
	.cfi_endproc
.LFE4448:
	.size	_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE,.-.L._ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
	.section	.text._ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE,"axG",@progbits,_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE,comdat
	.align 2
	.weak	_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE
	.section	".opd","aw"
	.align 3
_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE:
	.quad	.L._ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE,.TOC.@tocbase,0
	.previous
	.type	_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE, @function
.L._ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE:
.LFB4449:
	.cfi_startproc
	std 31,-8(1)
	stdu 1,-64(1)
	.cfi_def_cfa_offset 64
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,112(31)
	ld 9,112(31)
	mr 3,9
	addi 1,31,64
	.cfi_def_cfa 1, 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,0,128,1,0,1
	.cfi_endproc
.LFE4449:
	.size	_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE,.-.L._ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE
	.section	.text._ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3dEESt10shared_ptrIT_ERKT0_DpOT1_,"axG",@progbits,_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3dEESt10shared_ptrIT_ERKT0_DpOT1_,comdat
	.align 2
	.weak	_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3dEESt10shared_ptrIT_ERKT0_DpOT1_
	.section	".opd","aw"
	.align 3
_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3dEESt10shared_ptrIT_ERKT0_DpOT1_:
	.quad	.L._ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3dEESt10shared_ptrIT_ERKT0_DpOT1_,.TOC.@tocbase,0
	.previous
	.type	_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3dEESt10shared_ptrIT_ERKT0_DpOT1_, @function
.L._ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3dEESt10shared_ptrIT_ERKT0_DpOT1_:
.LFB4450:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	std 29,-24(1)
	std 30,-16(1)
	std 31,-8(1)
	stdu 1,-144(1)
	.cfi_def_cfa_offset 144
	.cfi_offset 65, 16
	.cfi_offset 29, -24
	.cfi_offset 30, -16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,192(31)
	std 4,200(31)
	std 5,208(31)
	std 6,216(31)
	ld 30,200(31)
	ld 3,208(31)
	bl _ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
	nop
	mr 29,3
	ld 3,216(31)
	bl _ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE
	nop
	mr 9,3
	mr 6,9
	mr 5,29
	mr 4,30
	ld 3,192(31)
	bl _ZNSt10shared_ptrI6sphereEC1ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	nop
	ld 3,192(31)
	addi 1,31,144
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 29,-24(1)
	ld 30,-16(1)
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,3,0,1
	.cfi_endproc
.LFE4450:
	.size	_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3dEESt10shared_ptrIT_ERKT0_DpOT1_,.-.L._ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3dEESt10shared_ptrIT_ERKT0_DpOT1_
	.section	.text._ZSt4moveIRSt10shared_ptrI6sphereEEONSt16remove_referenceIT_E4typeEOS5_,"axG",@progbits,_ZSt4moveIRSt10shared_ptrI6sphereEEONSt16remove_referenceIT_E4typeEOS5_,comdat
	.align 2
	.weak	_ZSt4moveIRSt10shared_ptrI6sphereEEONSt16remove_referenceIT_E4typeEOS5_
	.section	".opd","aw"
	.align 3
_ZSt4moveIRSt10shared_ptrI6sphereEEONSt16remove_referenceIT_E4typeEOS5_:
	.quad	.L._ZSt4moveIRSt10shared_ptrI6sphereEEONSt16remove_referenceIT_E4typeEOS5_,.TOC.@tocbase,0
	.previous
	.type	_ZSt4moveIRSt10shared_ptrI6sphereEEONSt16remove_referenceIT_E4typeEOS5_, @function
.L._ZSt4moveIRSt10shared_ptrI6sphereEEONSt16remove_referenceIT_E4typeEOS5_:
.LFB4454:
	.cfi_startproc
	std 31,-8(1)
	stdu 1,-64(1)
	.cfi_def_cfa_offset 64
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,112(31)
	ld 9,112(31)
	mr 3,9
	addi 1,31,64
	.cfi_def_cfa 1, 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,0,128,1,0,1
	.cfi_endproc
.LFE4454:
	.size	_ZSt4moveIRSt10shared_ptrI6sphereEEONSt16remove_referenceIT_E4typeEOS5_,.-.L._ZSt4moveIRSt10shared_ptrI6sphereEEONSt16remove_referenceIT_E4typeEOS5_
	.section	.text._ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC2I6spherevEEOS_IT_LS2_2EE,"axG",@progbits,_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC5I6spherevEEOS_IT_LS2_2EE,comdat
	.align 2
	.weak	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC2I6spherevEEOS_IT_LS2_2EE
	.section	".opd","aw"
	.align 3
_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC2I6spherevEEOS_IT_LS2_2EE:
	.quad	.L._ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC2I6spherevEEOS_IT_LS2_2EE,.TOC.@tocbase,0
	.previous
	.type	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC2I6spherevEEOS_IT_LS2_2EE, @function
.L._ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC2I6spherevEEOS_IT_LS2_2EE:
.LFB4456:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	std 31,-8(1)
	stdu 1,-128(1)
	.cfi_def_cfa_offset 128
	.cfi_offset 65, 16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,176(31)
	std 4,184(31)
	ld 9,184(31)
	ld 10,0(9)
	ld 9,176(31)
	std 10,0(9)
	ld 9,176(31)
	addi 9,9,8
	mr 3,9
	bl _ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1Ev
	nop
	ld 9,176(31)
	addi 10,9,8
	ld 9,184(31)
	addi 9,9,8
	mr 4,9
	mr 3,10
	bl _ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EE7_M_swapERS2_
	nop
	ld 9,184(31)
	li 10,0
	std 10,0(9)
	nop
	addi 1,31,128
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,1,0,1
	.cfi_endproc
.LFE4456:
	.size	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC2I6spherevEEOS_IT_LS2_2EE,.-.L._ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC2I6spherevEEOS_IT_LS2_2EE
	.weak	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC1I6spherevEEOS_IT_LS2_2EE
	.set	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC1I6spherevEEOS_IT_LS2_2EE,_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC2I6spherevEEOS_IT_LS2_2EE
	.section	.text._ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE,"axG",@progbits,_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE,comdat
	.align 2
	.weak	_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE
	.section	".opd","aw"
	.align 3
_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE:
	.quad	.L._ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE,.TOC.@tocbase,0
	.previous
	.type	_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE, @function
.L._ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE:
.LFB4462:
	.cfi_startproc
	std 31,-8(1)
	stdu 1,-64(1)
	.cfi_def_cfa_offset 64
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,112(31)
	ld 9,112(31)
	mr 3,9
	addi 1,31,64
	.cfi_def_cfa 1, 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,0,128,1,0,1
	.cfi_endproc
.LFE4462:
	.size	_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE,.-.L._ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE
	.section	.text._ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3iEESt10shared_ptrIT_ERKT0_DpOT1_,"axG",@progbits,_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3iEESt10shared_ptrIT_ERKT0_DpOT1_,comdat
	.align 2
	.weak	_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3iEESt10shared_ptrIT_ERKT0_DpOT1_
	.section	".opd","aw"
	.align 3
_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3iEESt10shared_ptrIT_ERKT0_DpOT1_:
	.quad	.L._ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3iEESt10shared_ptrIT_ERKT0_DpOT1_,.TOC.@tocbase,0
	.previous
	.type	_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3iEESt10shared_ptrIT_ERKT0_DpOT1_, @function
.L._ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3iEESt10shared_ptrIT_ERKT0_DpOT1_:
.LFB4463:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	std 29,-24(1)
	std 30,-16(1)
	std 31,-8(1)
	stdu 1,-144(1)
	.cfi_def_cfa_offset 144
	.cfi_offset 65, 16
	.cfi_offset 29, -24
	.cfi_offset 30, -16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,192(31)
	std 4,200(31)
	std 5,208(31)
	std 6,216(31)
	ld 30,200(31)
	ld 3,208(31)
	bl _ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
	nop
	mr 29,3
	ld 3,216(31)
	bl _ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE
	nop
	mr 9,3
	mr 6,9
	mr 5,29
	mr 4,30
	ld 3,192(31)
	bl _ZNSt10shared_ptrI6sphereEC1ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	nop
	ld 3,192(31)
	addi 1,31,144
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 29,-24(1)
	ld 30,-16(1)
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,3,0,1
	.cfi_endproc
.LFE4463:
	.size	_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3iEESt10shared_ptrIT_ERKT0_DpOT1_,.-.L._ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3iEESt10shared_ptrIT_ERKT0_DpOT1_
	.section	.text._ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE4seedEm,"axG",@progbits,_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE4seedEm,comdat
	.align 2
	.weak	_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE4seedEm
	.section	".opd","aw"
	.align 3
_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE4seedEm:
	.quad	.L._ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE4seedEm,.TOC.@tocbase,0
	.previous
	.type	_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE4seedEm, @function
.L._ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE4seedEm:
.LFB4518:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	std 31,-8(1)
	stdu 1,-144(1)
	.cfi_def_cfa_offset 144
	.cfi_offset 65, 16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,192(31)
	std 4,200(31)
	ld 3,200(31)
	bl _ZNSt8__detail5__modImLm4294967296ELm1ELm0EEET_S1_
	nop
	mr 10,3
	ld 9,192(31)
	std 10,0(9)
	li 9,1
	std 9,112(31)
.L144:
	ld 9,112(31)
	cmpldi 0,9,623
	bgt 0,.L143
	ld 9,112(31)
	addi 9,9,-1
	ld 10,192(31)
	sldi 9,9,3
	add 9,10,9
	ld 9,0(9)
	std 9,120(31)
	ld 9,120(31)
	srdi 9,9,30
	ld 10,120(31)
	xor 9,10,9
	std 9,120(31)
	ld 10,120(31)
	lis 9,0x6c07
	ori 9,9,0x8965
	mulld 9,10,9
	std 9,120(31)
	ld 3,112(31)
	bl _ZNSt8__detail5__modImLm624ELm1ELm0EEET_S1_
	nop
	mr 10,3
	ld 9,120(31)
	add 9,9,10
	std 9,120(31)
	ld 3,120(31)
	bl _ZNSt8__detail5__modImLm4294967296ELm1ELm0EEET_S1_
	nop
	mr 8,3
	ld 10,192(31)
	ld 9,112(31)
	sldi 9,9,3
	add 9,10,9
	std 8,0(9)
	ld 9,112(31)
	addi 9,9,1
	std 9,112(31)
	b .L144
.L143:
	ld 9,192(31)
	li 10,624
	std 10,4992(9)
	nop
	addi 1,31,144
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,1,0,1
	.cfi_endproc
.LFE4518:
	.size	_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE4seedEm,.-.L._ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE4seedEm
	.section	.text._ZNSt8__detail8_AdaptorISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEdEC2ERS2_,"axG",@progbits,_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEdEC5ERS2_,comdat
	.align 2
	.weak	_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEdEC2ERS2_
	.section	".opd","aw"
	.align 3
_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEdEC2ERS2_:
	.quad	.L._ZNSt8__detail8_AdaptorISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEdEC2ERS2_,.TOC.@tocbase,0
	.previous
	.type	_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEdEC2ERS2_, @function
.L._ZNSt8__detail8_AdaptorISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEdEC2ERS2_:
.LFB4520:
	.cfi_startproc
	std 31,-8(1)
	stdu 1,-64(1)
	.cfi_def_cfa_offset 64
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,112(31)
	std 4,120(31)
	ld 9,112(31)
	ld 10,120(31)
	std 10,0(9)
	nop
	addi 1,31,64
	.cfi_def_cfa 1, 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,0,128,1,0,1
	.cfi_endproc
.LFE4520:
	.size	_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEdEC2ERS2_,.-.L._ZNSt8__detail8_AdaptorISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEdEC2ERS2_
	.weak	_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEdEC1ERS2_
	.set	_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEdEC1ERS2_,_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEdEC2ERS2_
	.section	.text._ZNSt8__detail8_AdaptorISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEdEclEv,"axG",@progbits,_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEdEclEv,comdat
	.align 2
	.weak	_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEdEclEv
	.section	".opd","aw"
	.align 3
_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEdEclEv:
	.quad	.L._ZNSt8__detail8_AdaptorISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEdEclEv,.TOC.@tocbase,0
	.previous
	.type	_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEdEclEv, @function
.L._ZNSt8__detail8_AdaptorISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEdEclEv:
.LFB4522:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	std 31,-8(1)
	stdu 1,-128(1)
	.cfi_def_cfa_offset 128
	.cfi_offset 65, 16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,176(31)
	ld 9,176(31)
	ld 9,0(9)
	mr 3,9
	bl _ZSt18generate_canonicalIdLm53ESt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEET_RT1_
	nop
	fmr 0,1
	fmr 1,0
	addi 1,31,128
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,1,0,1
	.cfi_endproc
.LFE4522:
	.size	_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEdEclEv,.-.L._ZNSt8__detail8_AdaptorISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEdEclEv
	.section	.text._ZNKSt25uniform_real_distributionIdE10param_type1bEv,"axG",@progbits,_ZNKSt25uniform_real_distributionIdE10param_type1bEv,comdat
	.align 2
	.weak	_ZNKSt25uniform_real_distributionIdE10param_type1bEv
	.section	".opd","aw"
	.align 3
_ZNKSt25uniform_real_distributionIdE10param_type1bEv:
	.quad	.L._ZNKSt25uniform_real_distributionIdE10param_type1bEv,.TOC.@tocbase,0
	.previous
	.type	_ZNKSt25uniform_real_distributionIdE10param_type1bEv, @function
.L._ZNKSt25uniform_real_distributionIdE10param_type1bEv:
.LFB4523:
	.cfi_startproc
	std 31,-8(1)
	stdu 1,-64(1)
	.cfi_def_cfa_offset 64
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,112(31)
	ld 9,112(31)
	lfd 0,8(9)
	fmr 1,0
	addi 1,31,64
	.cfi_def_cfa 1, 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,0,128,1,0,1
	.cfi_endproc
.LFE4523:
	.size	_ZNKSt25uniform_real_distributionIdE10param_type1bEv,.-.L._ZNKSt25uniform_real_distributionIdE10param_type1bEv
	.section	.text._ZNKSt25uniform_real_distributionIdE10param_type1aEv,"axG",@progbits,_ZNKSt25uniform_real_distributionIdE10param_type1aEv,comdat
	.align 2
	.weak	_ZNKSt25uniform_real_distributionIdE10param_type1aEv
	.section	".opd","aw"
	.align 3
_ZNKSt25uniform_real_distributionIdE10param_type1aEv:
	.quad	.L._ZNKSt25uniform_real_distributionIdE10param_type1aEv,.TOC.@tocbase,0
	.previous
	.type	_ZNKSt25uniform_real_distributionIdE10param_type1aEv, @function
.L._ZNKSt25uniform_real_distributionIdE10param_type1aEv:
.LFB4524:
	.cfi_startproc
	std 31,-8(1)
	stdu 1,-64(1)
	.cfi_def_cfa_offset 64
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,112(31)
	ld 9,112(31)
	lfd 0,0(9)
	fmr 1,0
	addi 1,31,64
	.cfi_def_cfa 1, 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,0,128,1,0,1
	.cfi_endproc
.LFE4524:
	.size	_ZNKSt25uniform_real_distributionIdE10param_type1aEv,.-.L._ZNKSt25uniform_real_distributionIdE10param_type1aEv
	.section	.text._ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2Ev,"axG",@progbits,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC5Ev,comdat
	.align 2
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2Ev
	.section	".opd","aw"
	.align 3
_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2Ev:
	.quad	.L._ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2Ev,.TOC.@tocbase,0
	.previous
	.type	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2Ev, @function
.L._ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2Ev:
.LFB4530:
	.cfi_startproc
	std 31,-8(1)
	stdu 1,-64(1)
	.cfi_def_cfa_offset 64
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,112(31)
	ld 9,112(31)
	li 10,0
	std 10,0(9)
	nop
	addi 1,31,64
	.cfi_def_cfa 1, 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,0,128,1,0,1
	.cfi_endproc
.LFE4530:
	.size	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2Ev,.-.L._ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2Ev
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1Ev
	.set	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1Ev,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2Ev
	.section	.text._ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,"axG",@progbits,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,comdat
	.align 2
	.weak	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.section	".opd","aw"
	.align 3
_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv:
	.quad	.L._ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,.TOC.@tocbase,0
	.previous
	.type	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, @function
.L._ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv:
.LFB4532:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	std 31,-8(1)
	stdu 1,-128(1)
	.cfi_def_cfa_offset 128
	.cfi_offset 65, 16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 2,40(1)
	std 3,176(31)
	ld 9,176(31)
	cmpdi 0,9,0
	beq 0,.L155
	ld 9,176(31)
	ld 9,0(9)
	addi 9,9,8
	ld 9,0(9)
	ld 3,176(31)
	ld 10,0(9)
	mtctr 10
	ld 11,16(9)
	ld 2,8(9)
	bctrl
	ld 2,40(1)
.L155:
	nop
	addi 1,31,128
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,1,0,1
	.cfi_endproc
.LFE4532:
	.size	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,.-.L._ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.section	.text._ZNSaISt10shared_ptrI8hittableEED2Ev,"axG",@progbits,_ZNSaISt10shared_ptrI8hittableEED5Ev,comdat
	.align 2
	.weak	_ZNSaISt10shared_ptrI8hittableEED2Ev
	.section	".opd","aw"
	.align 3
_ZNSaISt10shared_ptrI8hittableEED2Ev:
	.quad	.L._ZNSaISt10shared_ptrI8hittableEED2Ev,.TOC.@tocbase,0
	.previous
	.type	_ZNSaISt10shared_ptrI8hittableEED2Ev, @function
.L._ZNSaISt10shared_ptrI8hittableEED2Ev:
.LFB4534:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	std 31,-8(1)
	stdu 1,-128(1)
	.cfi_def_cfa_offset 128
	.cfi_offset 65, 16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,176(31)
	ld 3,176(31)
	bl _ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED2Ev
	nop
	nop
	addi 1,31,128
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,1,0,1
	.cfi_endproc
.LFE4534:
	.size	_ZNSaISt10shared_ptrI8hittableEED2Ev,.-.L._ZNSaISt10shared_ptrI8hittableEED2Ev
	.weak	_ZNSaISt10shared_ptrI8hittableEED1Ev
	.set	_ZNSaISt10shared_ptrI8hittableEED1Ev,_ZNSaISt10shared_ptrI8hittableEED2Ev
	.section	.text._ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE13_M_deallocateEPS2_m,"axG",@progbits,_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE13_M_deallocateEPS2_m,comdat
	.align 2
	.weak	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE13_M_deallocateEPS2_m
	.section	".opd","aw"
	.align 3
_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE13_M_deallocateEPS2_m:
	.quad	.L._ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE13_M_deallocateEPS2_m,.TOC.@tocbase,0
	.previous
	.type	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE13_M_deallocateEPS2_m, @function
.L._ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE13_M_deallocateEPS2_m:
.LFB4536:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	std 31,-8(1)
	stdu 1,-128(1)
	.cfi_def_cfa_offset 128
	.cfi_offset 65, 16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,176(31)
	std 4,184(31)
	std 5,192(31)
	ld 9,184(31)
	cmpdi 0,9,0
	beq 0,.L159
	ld 9,176(31)
	ld 5,192(31)
	ld 4,184(31)
	mr 3,9
	bl _ZNSt16allocator_traitsISaISt10shared_ptrI8hittableEEE10deallocateERS3_PS2_m
	nop
.L159:
	nop
	addi 1,31,128
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,1,0,1
	.cfi_endproc
.LFE4536:
	.size	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE13_M_deallocateEPS2_m,.-.L._ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE13_M_deallocateEPS2_m
	.section	.text._ZSt8_DestroyIPSt10shared_ptrI8hittableEEvT_S4_,"axG",@progbits,_ZSt8_DestroyIPSt10shared_ptrI8hittableEEvT_S4_,comdat
	.align 2
	.weak	_ZSt8_DestroyIPSt10shared_ptrI8hittableEEvT_S4_
	.section	".opd","aw"
	.align 3
_ZSt8_DestroyIPSt10shared_ptrI8hittableEEvT_S4_:
	.quad	.L._ZSt8_DestroyIPSt10shared_ptrI8hittableEEvT_S4_,.TOC.@tocbase,0
	.previous
	.type	_ZSt8_DestroyIPSt10shared_ptrI8hittableEEvT_S4_, @function
.L._ZSt8_DestroyIPSt10shared_ptrI8hittableEEvT_S4_:
.LFB4537:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	std 31,-8(1)
	stdu 1,-128(1)
	.cfi_def_cfa_offset 128
	.cfi_offset 65, 16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,176(31)
	std 4,184(31)
	ld 4,184(31)
	ld 3,176(31)
	bl _ZNSt12_Destroy_auxILb0EE9__destroyIPSt10shared_ptrI8hittableEEEvT_S6_
	nop
	nop
	addi 1,31,128
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,1,0,1
	.cfi_endproc
.LFE4537:
	.size	_ZSt8_DestroyIPSt10shared_ptrI8hittableEEvT_S4_,.-.L._ZSt8_DestroyIPSt10shared_ptrI8hittableEEvT_S4_
	.section	.text._ZN9__gnu_cxx13new_allocatorI6sphereEC2Ev,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorI6sphereEC5Ev,comdat
	.align 2
	.weak	_ZN9__gnu_cxx13new_allocatorI6sphereEC2Ev
	.section	".opd","aw"
	.align 3
_ZN9__gnu_cxx13new_allocatorI6sphereEC2Ev:
	.quad	.L._ZN9__gnu_cxx13new_allocatorI6sphereEC2Ev,.TOC.@tocbase,0
	.previous
	.type	_ZN9__gnu_cxx13new_allocatorI6sphereEC2Ev, @function
.L._ZN9__gnu_cxx13new_allocatorI6sphereEC2Ev:
.LFB4539:
	.cfi_startproc
	std 31,-8(1)
	stdu 1,-64(1)
	.cfi_def_cfa_offset 64
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,112(31)
	nop
	addi 1,31,64
	.cfi_def_cfa 1, 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,0,128,1,0,1
	.cfi_endproc
.LFE4539:
	.size	_ZN9__gnu_cxx13new_allocatorI6sphereEC2Ev,.-.L._ZN9__gnu_cxx13new_allocatorI6sphereEC2Ev
	.weak	_ZN9__gnu_cxx13new_allocatorI6sphereEC1Ev
	.set	_ZN9__gnu_cxx13new_allocatorI6sphereEC1Ev,_ZN9__gnu_cxx13new_allocatorI6sphereEC2Ev
	.section	.text._ZN9__gnu_cxx13new_allocatorI6sphereED2Ev,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorI6sphereED5Ev,comdat
	.align 2
	.weak	_ZN9__gnu_cxx13new_allocatorI6sphereED2Ev
	.section	".opd","aw"
	.align 3
_ZN9__gnu_cxx13new_allocatorI6sphereED2Ev:
	.quad	.L._ZN9__gnu_cxx13new_allocatorI6sphereED2Ev,.TOC.@tocbase,0
	.previous
	.type	_ZN9__gnu_cxx13new_allocatorI6sphereED2Ev, @function
.L._ZN9__gnu_cxx13new_allocatorI6sphereED2Ev:
.LFB4542:
	.cfi_startproc
	std 31,-8(1)
	stdu 1,-64(1)
	.cfi_def_cfa_offset 64
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,112(31)
	nop
	addi 1,31,64
	.cfi_def_cfa 1, 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,0,128,1,0,1
	.cfi_endproc
.LFE4542:
	.size	_ZN9__gnu_cxx13new_allocatorI6sphereED2Ev,.-.L._ZN9__gnu_cxx13new_allocatorI6sphereED2Ev
	.weak	_ZN9__gnu_cxx13new_allocatorI6sphereED1Ev
	.set	_ZN9__gnu_cxx13new_allocatorI6sphereED1Ev,_ZN9__gnu_cxx13new_allocatorI6sphereED2Ev
	.section	.text._ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,"axG",@progbits,_ZNSt10shared_ptrI6sphereEC5ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,comdat
	.align 2
	.weak	_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.section	".opd","aw"
	.align 3
_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_:
	.quad	.L._ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,.TOC.@tocbase,0
	.previous
	.type	_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_, @function
.L._ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_:
.LFB4545:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	std 29,-24(1)
	std 30,-16(1)
	std 31,-8(1)
	stdu 1,-144(1)
	.cfi_def_cfa_offset 144
	.cfi_offset 65, 16
	.cfi_offset 29, -24
	.cfi_offset 30, -16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,192(31)
	std 4,200(31)
	std 5,208(31)
	std 6,216(31)
	ld 30,192(31)
	ld 3,208(31)
	bl _ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
	nop
	mr 29,3
	ld 3,216(31)
	bl _ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE
	nop
	mr 9,3
	mr 6,9
	mr 5,29
	ld 4,200(31)
	mr 3,30
	bl _ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	nop
	nop
	addi 1,31,144
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 29,-24(1)
	ld 30,-16(1)
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,3,0,1
	.cfi_endproc
.LFE4545:
	.size	_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,.-.L._ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.weak	_ZNSt10shared_ptrI6sphereEC1ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.set	_ZNSt10shared_ptrI6sphereEC1ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.section	.text._ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EE7_M_swapERS2_,"axG",@progbits,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EE7_M_swapERS2_,comdat
	.align 2
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EE7_M_swapERS2_
	.section	".opd","aw"
	.align 3
_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EE7_M_swapERS2_:
	.quad	.L._ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EE7_M_swapERS2_,.TOC.@tocbase,0
	.previous
	.type	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EE7_M_swapERS2_, @function
.L._ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EE7_M_swapERS2_:
.LFB4550:
	.cfi_startproc
	std 31,-8(1)
	stdu 1,-80(1)
	.cfi_def_cfa_offset 80
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,128(31)
	std 4,136(31)
	ld 9,136(31)
	ld 9,0(9)
	std 9,56(31)
	ld 9,128(31)
	ld 10,0(9)
	ld 9,136(31)
	std 10,0(9)
	ld 9,128(31)
	ld 10,56(31)
	std 10,0(9)
	nop
	addi 1,31,80
	.cfi_def_cfa 1, 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,0,128,1,0,1
	.cfi_endproc
.LFE4550:
	.size	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EE7_M_swapERS2_,.-.L._ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EE7_M_swapERS2_
	.section	.text._ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,"axG",@progbits,_ZNSt10shared_ptrI6sphereEC5ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,comdat
	.align 2
	.weak	_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.section	".opd","aw"
	.align 3
_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_:
	.quad	.L._ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,.TOC.@tocbase,0
	.previous
	.type	_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_, @function
.L._ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_:
.LFB4552:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	std 29,-24(1)
	std 30,-16(1)
	std 31,-8(1)
	stdu 1,-144(1)
	.cfi_def_cfa_offset 144
	.cfi_offset 65, 16
	.cfi_offset 29, -24
	.cfi_offset 30, -16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,192(31)
	std 4,200(31)
	std 5,208(31)
	std 6,216(31)
	ld 30,192(31)
	ld 3,208(31)
	bl _ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
	nop
	mr 29,3
	ld 3,216(31)
	bl _ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE
	nop
	mr 9,3
	mr 6,9
	mr 5,29
	ld 4,200(31)
	mr 3,30
	bl _ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	nop
	nop
	addi 1,31,144
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 29,-24(1)
	ld 30,-16(1)
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,3,0,1
	.cfi_endproc
.LFE4552:
	.size	_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,.-.L._ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.weak	_ZNSt10shared_ptrI6sphereEC1ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.set	_ZNSt10shared_ptrI6sphereEC1ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.section	.text._ZNSt8__detail5__modImLm4294967296ELm1ELm0EEET_S1_,"axG",@progbits,_ZNSt8__detail5__modImLm4294967296ELm1ELm0EEET_S1_,comdat
	.align 2
	.weak	_ZNSt8__detail5__modImLm4294967296ELm1ELm0EEET_S1_
	.section	".opd","aw"
	.align 3
_ZNSt8__detail5__modImLm4294967296ELm1ELm0EEET_S1_:
	.quad	.L._ZNSt8__detail5__modImLm4294967296ELm1ELm0EEET_S1_,.TOC.@tocbase,0
	.previous
	.type	_ZNSt8__detail5__modImLm4294967296ELm1ELm0EEET_S1_, @function
.L._ZNSt8__detail5__modImLm4294967296ELm1ELm0EEET_S1_:
.LFB4591:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	std 31,-8(1)
	stdu 1,-128(1)
	.cfi_def_cfa_offset 128
	.cfi_offset 65, 16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,176(31)
	ld 3,176(31)
	bl _ZNSt8__detail4_ModImLm4294967296ELm1ELm0ELb1ELb1EE6__calcEm
	nop
	mr 9,3
	mr 3,9
	addi 1,31,128
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,1,0,1
	.cfi_endproc
.LFE4591:
	.size	_ZNSt8__detail5__modImLm4294967296ELm1ELm0EEET_S1_,.-.L._ZNSt8__detail5__modImLm4294967296ELm1ELm0EEET_S1_
	.section	.text._ZNSt8__detail5__modImLm624ELm1ELm0EEET_S1_,"axG",@progbits,_ZNSt8__detail5__modImLm624ELm1ELm0EEET_S1_,comdat
	.align 2
	.weak	_ZNSt8__detail5__modImLm624ELm1ELm0EEET_S1_
	.section	".opd","aw"
	.align 3
_ZNSt8__detail5__modImLm624ELm1ELm0EEET_S1_:
	.quad	.L._ZNSt8__detail5__modImLm624ELm1ELm0EEET_S1_,.TOC.@tocbase,0
	.previous
	.type	_ZNSt8__detail5__modImLm624ELm1ELm0EEET_S1_, @function
.L._ZNSt8__detail5__modImLm624ELm1ELm0EEET_S1_:
.LFB4592:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	std 31,-8(1)
	stdu 1,-128(1)
	.cfi_def_cfa_offset 128
	.cfi_offset 65, 16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,176(31)
	ld 3,176(31)
	bl _ZNSt8__detail4_ModImLm624ELm1ELm0ELb1ELb1EE6__calcEm
	nop
	mr 9,3
	mr 3,9
	addi 1,31,128
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,1,0,1
	.cfi_endproc
.LFE4592:
	.size	_ZNSt8__detail5__modImLm624ELm1ELm0EEET_S1_,.-.L._ZNSt8__detail5__modImLm624ELm1ELm0EEET_S1_
	.section	.text._ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE3minEv,"axG",@progbits,_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE3minEv,comdat
	.align 2
	.weak	_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE3minEv
	.section	".opd","aw"
	.align 3
_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE3minEv:
	.quad	.L._ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE3minEv,.TOC.@tocbase,0
	.previous
	.type	_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE3minEv, @function
.L._ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE3minEv:
.LFB4596:
	.cfi_startproc
	std 31,-8(1)
	stdu 1,-64(1)
	.cfi_def_cfa_offset 64
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	li 9,0
	mr 3,9
	addi 1,31,64
	.cfi_def_cfa 1, 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,0,128,1,0,1
	.cfi_endproc
.LFE4596:
	.size	_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE3minEv,.-.L._ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE3minEv
	.globl __gcc_qmul
	.section	.text._ZSt18generate_canonicalIdLm53ESt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEET_RT1_,"axG",@progbits,_ZSt18generate_canonicalIdLm53ESt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEET_RT1_,comdat
	.align 2
	.weak	_ZSt18generate_canonicalIdLm53ESt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEET_RT1_
	.section	".opd","aw"
	.align 3
_ZSt18generate_canonicalIdLm53ESt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEET_RT1_:
	.quad	.L._ZSt18generate_canonicalIdLm53ESt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEET_RT1_,.TOC.@tocbase,0
	.previous
	.type	_ZSt18generate_canonicalIdLm53ESt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEET_RT1_, @function
.L._ZSt18generate_canonicalIdLm53ESt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEET_RT1_:
.LFB4593:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	std 30,-16(1)
	std 31,-8(1)
	stdu 1,-224(1)
	.cfi_def_cfa_offset 224
	.cfi_offset 65, 16
	.cfi_offset 30, -16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,272(31)
	li 9,53
	std 9,168(31)
	addis 9,2,.LC19@toc@ha
	addi 9,9,.LC19@toc@l
	lfd 0,0(9)
	lfd 1,8(9)
	addi 9,31,192
	stfd 0,0(9)
	stfd 1,8(9)
	li 9,32
	std 9,176(31)
	li 9,2
	std 9,184(31)
	xxlxor 0,0,0
	stfd 0,144(31)
	addis 9,2,.LC1@toc@ha
	addi 9,9,.LC1@toc@l
	lfd 0,0(9)
	stfd 0,152(31)
	li 9,2
	std 9,160(31)
.L174:
	ld 9,160(31)
	cmpdi 0,9,0
	beq 0,.L173
	ld 3,272(31)
	bl _ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEclEv
	nop
	mr 30,3
	bl _ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE3minEv
	nop
	mr 9,3
	subf 9,9,30
	std 9,120(31)
	lfd 0,120(31)
	fcfidu 12,0
	lfd 0,152(31)
	fmul 0,12,0
	lfd 12,144(31)
	fadd 0,12,0
	stfd 0,144(31)
	lfd 0,152(31)
	fmr 0,0
	xxlxor 1,1,1
	addis 9,2,.LC19@toc@ha
	addi 9,9,.LC19@toc@l
	lfd 3,0(9)
	lfd 4,8(9)
	fmr 2,1
	fmr 1,0
	bl __gcc_qmul
	nop
	fmr 0,1
	fmr 1,2
	stfd 0,152(31)
	ld 9,160(31)
	addi 9,9,-1
	std 9,160(31)
	b .L174
.L173:
	lfd 12,144(31)
	lfd 0,152(31)
	fdiv 0,12,0
	stfd 0,136(31)
	lfd 12,136(31)
	addis 9,2,.LC1@toc@ha
	addi 9,9,.LC1@toc@l
	lfd 0,0(9)
	fcmpu 0,12,0
	cror 2,1,2
	mfcr 9,128
	rlwinm 9,9,3,1
	rlwinm 9,9,0,0xff
	cmpdi 0,9,0
	beq 0,.L175
	addis 9,2,.LC20@toc@ha
	addi 9,9,.LC20@toc@l
	lfd 0,0(9)
	stfd 0,136(31)
.L175:
	lfd 0,136(31)
	fmr 1,0
	addi 1,31,224
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 30,-16(1)
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,2,0,1
	.cfi_endproc
.LFE4593:
	.size	_ZSt18generate_canonicalIdLm53ESt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEET_RT1_,.-.L._ZSt18generate_canonicalIdLm53ESt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEET_RT1_
	.section	".toc","aw"
.LC21:
	.quad	_ZTVSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE+16
	.section	.text._ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.weak	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	".opd","aw"
	.align 3
_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev:
	.quad	.L._ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev,.TOC.@tocbase,0
	.previous
	.type	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
.L._ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev:
.LFB4603:
	.cfi_startproc
	std 31,-8(1)
	stdu 1,-64(1)
	.cfi_def_cfa_offset 64
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,112(31)
	addis 9,2,.LC21@toc@ha
	ld 10,.LC21@toc@l(9)
	ld 9,112(31)
	std 10,0(9)
	nop
	addi 1,31,64
	.cfi_def_cfa 1, 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,0,128,1,0,1
	.cfi_endproc
.LFE4603:
	.size	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev,.-.L._ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED1Ev
	.set	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED1Ev,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED0Ev,"axG",@progbits,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.weak	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED0Ev
	.section	".opd","aw"
	.align 3
_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED0Ev:
	.quad	.L._ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED0Ev,.TOC.@tocbase,0
	.previous
	.type	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED0Ev, @function
.L._ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED0Ev:
.LFB4605:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	std 31,-8(1)
	stdu 1,-128(1)
	.cfi_def_cfa_offset 128
	.cfi_offset 65, 16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,176(31)
	ld 3,176(31)
	bl _ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED1Ev
	nop
	li 4,16
	ld 3,176(31)
	bl _ZdlPvm
	nop
	addi 1,31,128
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,1,0,1
	.cfi_endproc
.LFE4605:
	.size	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED0Ev,.-.L._ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED0Ev
	.section	.text._ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED2Ev,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED5Ev,comdat
	.align 2
	.weak	_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED2Ev
	.section	".opd","aw"
	.align 3
_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED2Ev:
	.quad	.L._ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED2Ev,.TOC.@tocbase,0
	.previous
	.type	_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED2Ev, @function
.L._ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED2Ev:
.LFB4607:
	.cfi_startproc
	std 31,-8(1)
	stdu 1,-64(1)
	.cfi_def_cfa_offset 64
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,112(31)
	nop
	addi 1,31,64
	.cfi_def_cfa 1, 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,0,128,1,0,1
	.cfi_endproc
.LFE4607:
	.size	_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED2Ev,.-.L._ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED2Ev
	.weak	_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED1Ev
	.set	_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED1Ev,_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED2Ev
	.section	.text._ZNSt16allocator_traitsISaISt10shared_ptrI8hittableEEE10deallocateERS3_PS2_m,"axG",@progbits,_ZNSt16allocator_traitsISaISt10shared_ptrI8hittableEEE10deallocateERS3_PS2_m,comdat
	.align 2
	.weak	_ZNSt16allocator_traitsISaISt10shared_ptrI8hittableEEE10deallocateERS3_PS2_m
	.section	".opd","aw"
	.align 3
_ZNSt16allocator_traitsISaISt10shared_ptrI8hittableEEE10deallocateERS3_PS2_m:
	.quad	.L._ZNSt16allocator_traitsISaISt10shared_ptrI8hittableEEE10deallocateERS3_PS2_m,.TOC.@tocbase,0
	.previous
	.type	_ZNSt16allocator_traitsISaISt10shared_ptrI8hittableEEE10deallocateERS3_PS2_m, @function
.L._ZNSt16allocator_traitsISaISt10shared_ptrI8hittableEEE10deallocateERS3_PS2_m:
.LFB4609:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	std 31,-8(1)
	stdu 1,-128(1)
	.cfi_def_cfa_offset 128
	.cfi_offset 65, 16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,176(31)
	std 4,184(31)
	std 5,192(31)
	ld 5,192(31)
	ld 4,184(31)
	ld 3,176(31)
	bl _ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEE10deallocateEPS3_m
	nop
	nop
	addi 1,31,128
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,1,0,1
	.cfi_endproc
.LFE4609:
	.size	_ZNSt16allocator_traitsISaISt10shared_ptrI8hittableEEE10deallocateERS3_PS2_m,.-.L._ZNSt16allocator_traitsISaISt10shared_ptrI8hittableEEE10deallocateERS3_PS2_m
	.section	.text._ZNSt12_Destroy_auxILb0EE9__destroyIPSt10shared_ptrI8hittableEEEvT_S6_,"axG",@progbits,_ZNSt12_Destroy_auxILb0EE9__destroyIPSt10shared_ptrI8hittableEEEvT_S6_,comdat
	.align 2
	.weak	_ZNSt12_Destroy_auxILb0EE9__destroyIPSt10shared_ptrI8hittableEEEvT_S6_
	.section	".opd","aw"
	.align 3
_ZNSt12_Destroy_auxILb0EE9__destroyIPSt10shared_ptrI8hittableEEEvT_S6_:
	.quad	.L._ZNSt12_Destroy_auxILb0EE9__destroyIPSt10shared_ptrI8hittableEEEvT_S6_,.TOC.@tocbase,0
	.previous
	.type	_ZNSt12_Destroy_auxILb0EE9__destroyIPSt10shared_ptrI8hittableEEEvT_S6_, @function
.L._ZNSt12_Destroy_auxILb0EE9__destroyIPSt10shared_ptrI8hittableEEEvT_S6_:
.LFB4610:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	std 31,-8(1)
	stdu 1,-128(1)
	.cfi_def_cfa_offset 128
	.cfi_offset 65, 16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,176(31)
	std 4,184(31)
.L183:
	ld 10,176(31)
	ld 9,184(31)
	cmpd 0,10,9
	beq 0,.L184
	ld 3,176(31)
	bl _ZSt11__addressofISt10shared_ptrI8hittableEEPT_RS3_
	nop
	mr 9,3
	mr 3,9
	bl _ZSt8_DestroyISt10shared_ptrI8hittableEEvPT_
	nop
	ld 9,176(31)
	addi 9,9,16
	std 9,176(31)
	b .L183
.L184:
	nop
	addi 1,31,128
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,1,0,1
	.cfi_endproc
.LFE4610:
	.size	_ZNSt12_Destroy_auxILb0EE9__destroyIPSt10shared_ptrI8hittableEEEvT_S6_,.-.L._ZNSt12_Destroy_auxILb0EE9__destroyIPSt10shared_ptrI8hittableEEEvT_S6_
	.section	.text._ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,"axG",@progbits,_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC5ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,comdat
	.align 2
	.weak	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.section	".opd","aw"
	.align 3
_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_:
	.quad	.L._ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,.TOC.@tocbase,0
	.previous
	.type	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_, @function
.L._ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_:
.LFB4612:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	std 28,-32(1)
	std 29,-24(1)
	std 30,-16(1)
	std 31,-8(1)
	stdu 1,-144(1)
	.cfi_def_cfa_offset 144
	.cfi_offset 65, 16
	.cfi_offset 28, -32
	.cfi_offset 29, -24
	.cfi_offset 30, -16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,192(31)
	std 4,200(31)
	std 5,208(31)
	std 6,216(31)
	ld 9,192(31)
	li 10,0
	std 10,0(9)
	ld 9,192(31)
	addi 30,9,8
	ld 29,192(31)
	ld 3,208(31)
	bl _ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
	nop
	mr 28,3
	ld 3,216(31)
	bl _ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE
	nop
	mr 9,3
	mr 7,9
	mr 6,28
	ld 5,200(31)
	mr 4,29
	mr 3,30
	bl _ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_
	nop
	ld 9,192(31)
	ld 9,0(9)
	mr 4,9
	ld 3,192(31)
	bl _ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EE31_M_enable_shared_from_this_withIS0_S0_EENSt9enable_ifIXntsrNS3_15__has_esft_baseIT0_vEE5valueEvE4typeEPT_
	nop
	nop
	addi 1,31,144
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 28,-32(1)
	ld 29,-24(1)
	ld 30,-16(1)
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,4,0,1
	.cfi_endproc
.LFE4612:
	.size	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,.-.L._ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.weak	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC1ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.set	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC1ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.section	.text._ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,"axG",@progbits,_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC5ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,comdat
	.align 2
	.weak	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.section	".opd","aw"
	.align 3
_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_:
	.quad	.L._ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,.TOC.@tocbase,0
	.previous
	.type	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_, @function
.L._ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_:
.LFB4615:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	std 28,-32(1)
	std 29,-24(1)
	std 30,-16(1)
	std 31,-8(1)
	stdu 1,-144(1)
	.cfi_def_cfa_offset 144
	.cfi_offset 65, 16
	.cfi_offset 28, -32
	.cfi_offset 29, -24
	.cfi_offset 30, -16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,192(31)
	std 4,200(31)
	std 5,208(31)
	std 6,216(31)
	ld 9,192(31)
	li 10,0
	std 10,0(9)
	ld 9,192(31)
	addi 30,9,8
	ld 29,192(31)
	ld 3,208(31)
	bl _ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
	nop
	mr 28,3
	ld 3,216(31)
	bl _ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE
	nop
	mr 9,3
	mr 7,9
	mr 6,28
	ld 5,200(31)
	mr 4,29
	mr 3,30
	bl _ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_
	nop
	ld 9,192(31)
	ld 9,0(9)
	mr 4,9
	ld 3,192(31)
	bl _ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EE31_M_enable_shared_from_this_withIS0_S0_EENSt9enable_ifIXntsrNS3_15__has_esft_baseIT0_vEE5valueEvE4typeEPT_
	nop
	nop
	addi 1,31,144
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 28,-32(1)
	ld 29,-24(1)
	ld 30,-16(1)
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,4,0,1
	.cfi_endproc
.LFE4615:
	.size	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,.-.L._ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.weak	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC1ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.set	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC1ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.section	.text._ZNSt8__detail4_ModImLm4294967296ELm1ELm0ELb1ELb1EE6__calcEm,"axG",@progbits,_ZNSt8__detail4_ModImLm4294967296ELm1ELm0ELb1ELb1EE6__calcEm,comdat
	.align 2
	.weak	_ZNSt8__detail4_ModImLm4294967296ELm1ELm0ELb1ELb1EE6__calcEm
	.section	".opd","aw"
	.align 3
_ZNSt8__detail4_ModImLm4294967296ELm1ELm0ELb1ELb1EE6__calcEm:
	.quad	.L._ZNSt8__detail4_ModImLm4294967296ELm1ELm0ELb1ELb1EE6__calcEm,.TOC.@tocbase,0
	.previous
	.type	_ZNSt8__detail4_ModImLm4294967296ELm1ELm0ELb1ELb1EE6__calcEm, @function
.L._ZNSt8__detail4_ModImLm4294967296ELm1ELm0ELb1ELb1EE6__calcEm:
.LFB4645:
	.cfi_startproc
	std 31,-8(1)
	stdu 1,-80(1)
	.cfi_def_cfa_offset 80
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,128(31)
	ld 9,128(31)
	std 9,56(31)
	ld 9,56(31)
	rldicl 9,9,0,32
	std 9,56(31)
	ld 9,56(31)
	mr 3,9
	addi 1,31,80
	.cfi_def_cfa 1, 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,0,128,1,0,1
	.cfi_endproc
.LFE4645:
	.size	_ZNSt8__detail4_ModImLm4294967296ELm1ELm0ELb1ELb1EE6__calcEm,.-.L._ZNSt8__detail4_ModImLm4294967296ELm1ELm0ELb1ELb1EE6__calcEm
	.section	.text._ZNSt8__detail4_ModImLm624ELm1ELm0ELb1ELb1EE6__calcEm,"axG",@progbits,_ZNSt8__detail4_ModImLm624ELm1ELm0ELb1ELb1EE6__calcEm,comdat
	.align 2
	.weak	_ZNSt8__detail4_ModImLm624ELm1ELm0ELb1ELb1EE6__calcEm
	.section	".opd","aw"
	.align 3
_ZNSt8__detail4_ModImLm624ELm1ELm0ELb1ELb1EE6__calcEm:
	.quad	.L._ZNSt8__detail4_ModImLm624ELm1ELm0ELb1ELb1EE6__calcEm,.TOC.@tocbase,0
	.previous
	.type	_ZNSt8__detail4_ModImLm624ELm1ELm0ELb1ELb1EE6__calcEm, @function
.L._ZNSt8__detail4_ModImLm624ELm1ELm0ELb1ELb1EE6__calcEm:
.LFB4646:
	.cfi_startproc
	std 31,-8(1)
	stdu 1,-80(1)
	.cfi_def_cfa_offset 80
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,128(31)
	ld 9,128(31)
	std 9,56(31)
	ld 9,56(31)
	srdi 8,9,4
	addis 10,2,.LC22@toc@ha
	ld 10,.LC22@toc@l(10)
	mulhdu 10,8,10
	srdi 10,10,1
	mulli 10,10,624
	subf 9,10,9
	std 9,56(31)
	ld 9,56(31)
	mr 3,9
	addi 1,31,80
	.cfi_def_cfa 1, 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,0,128,1,0,1
	.cfi_endproc
.LFE4646:
	.size	_ZNSt8__detail4_ModImLm624ELm1ELm0ELb1ELb1EE6__calcEm,.-.L._ZNSt8__detail4_ModImLm624ELm1ELm0ELb1ELb1EE6__calcEm
	.section	.text._ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEclEv,"axG",@progbits,_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEclEv,comdat
	.align 2
	.weak	_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEclEv
	.section	".opd","aw"
	.align 3
_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEclEv:
	.quad	.L._ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEclEv,.TOC.@tocbase,0
	.previous
	.type	_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEclEv, @function
.L._ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEclEv:
.LFB4647:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	std 31,-8(1)
	stdu 1,-144(1)
	.cfi_def_cfa_offset 144
	.cfi_offset 65, 16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,192(31)
	ld 9,192(31)
	ld 9,4992(9)
	cmpldi 0,9,623
	ble 0,.L192
	ld 3,192(31)
	bl _ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE11_M_gen_randEv
	nop
.L192:
	ld 9,192(31)
	ld 9,4992(9)
	addi 8,9,1
	ld 10,192(31)
	std 8,4992(10)
	ld 10,192(31)
	sldi 9,9,3
	add 9,10,9
	ld 9,0(9)
	std 9,120(31)
	ld 9,120(31)
	srdi 9,9,11
	rldicl 9,9,0,32
	ld 10,120(31)
	xor 9,10,9
	std 9,120(31)
	ld 9,120(31)
	sldi 10,9,7
	lis 9,0x9d2c
	ori 9,9,0x5680
	rldicl 9,9,0,32
	and 9,10,9
	ld 10,120(31)
	xor 9,10,9
	std 9,120(31)
	ld 9,120(31)
	sldi 9,9,15
	andis. 9,9,0xefc6
	ld 10,120(31)
	xor 9,10,9
	std 9,120(31)
	ld 9,120(31)
	srdi 9,9,18
	ld 10,120(31)
	xor 9,10,9
	std 9,120(31)
	ld 9,120(31)
	mr 3,9
	addi 1,31,144
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,1,0,1
	.cfi_endproc
.LFE4647:
	.size	_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEclEv,.-.L._ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEclEv
	.section	.text._ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEE10deallocateEPS3_m,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEE10deallocateEPS3_m,comdat
	.align 2
	.weak	_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEE10deallocateEPS3_m
	.section	".opd","aw"
	.align 3
_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEE10deallocateEPS3_m:
	.quad	.L._ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEE10deallocateEPS3_m,.TOC.@tocbase,0
	.previous
	.type	_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEE10deallocateEPS3_m, @function
.L._ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEE10deallocateEPS3_m:
.LFB4649:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	std 31,-8(1)
	stdu 1,-128(1)
	.cfi_def_cfa_offset 128
	.cfi_offset 65, 16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,176(31)
	std 4,184(31)
	std 5,192(31)
	ld 3,184(31)
	bl _ZdlPv
	nop
	nop
	addi 1,31,128
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,1,0,1
	.cfi_endproc
.LFE4649:
	.size	_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEE10deallocateEPS3_m,.-.L._ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEE10deallocateEPS3_m
	.section	.text._ZSt11__addressofISt10shared_ptrI8hittableEEPT_RS3_,"axG",@progbits,_ZSt11__addressofISt10shared_ptrI8hittableEEPT_RS3_,comdat
	.align 2
	.weak	_ZSt11__addressofISt10shared_ptrI8hittableEEPT_RS3_
	.section	".opd","aw"
	.align 3
_ZSt11__addressofISt10shared_ptrI8hittableEEPT_RS3_:
	.quad	.L._ZSt11__addressofISt10shared_ptrI8hittableEEPT_RS3_,.TOC.@tocbase,0
	.previous
	.type	_ZSt11__addressofISt10shared_ptrI8hittableEEPT_RS3_, @function
.L._ZSt11__addressofISt10shared_ptrI8hittableEEPT_RS3_:
.LFB4650:
	.cfi_startproc
	std 31,-8(1)
	stdu 1,-64(1)
	.cfi_def_cfa_offset 64
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,112(31)
	ld 9,112(31)
	mr 3,9
	addi 1,31,64
	.cfi_def_cfa 1, 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,0,128,1,0,1
	.cfi_endproc
.LFE4650:
	.size	_ZSt11__addressofISt10shared_ptrI8hittableEEPT_RS3_,.-.L._ZSt11__addressofISt10shared_ptrI8hittableEEPT_RS3_
	.section	.text._ZSt8_DestroyISt10shared_ptrI8hittableEEvPT_,"axG",@progbits,_ZSt8_DestroyISt10shared_ptrI8hittableEEvPT_,comdat
	.align 2
	.weak	_ZSt8_DestroyISt10shared_ptrI8hittableEEvPT_
	.section	".opd","aw"
	.align 3
_ZSt8_DestroyISt10shared_ptrI8hittableEEvPT_:
	.quad	.L._ZSt8_DestroyISt10shared_ptrI8hittableEEvPT_,.TOC.@tocbase,0
	.previous
	.type	_ZSt8_DestroyISt10shared_ptrI8hittableEEvPT_, @function
.L._ZSt8_DestroyISt10shared_ptrI8hittableEEvPT_:
.LFB4651:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	std 31,-8(1)
	stdu 1,-128(1)
	.cfi_def_cfa_offset 128
	.cfi_offset 65, 16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,176(31)
	ld 3,176(31)
	bl _ZNSt10shared_ptrI8hittableED1Ev
	nop
	nop
	addi 1,31,128
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,1,0,1
	.cfi_endproc
.LFE4651:
	.size	_ZSt8_DestroyISt10shared_ptrI8hittableEEvPT_,.-.L._ZSt8_DestroyISt10shared_ptrI8hittableEEvPT_
	.section	.text._ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_,"axG",@progbits,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC5I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_,comdat
	.align 2
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_
	.section	".opd","aw"
	.align 3
_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_:
	.quad	.L._ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_,.TOC.@tocbase,0
	.previous
	.type	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_, @function
.L._ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_:
.LFB4653:
	.cfi_startproc
	.cfi_personality 0x94,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x14,.LLSDA4653
	mflr 0
	std 0,16(1)
	std 26,-48(1)
	std 27,-40(1)
	std 28,-32(1)
	std 29,-24(1)
	std 30,-16(1)
	std 31,-8(1)
	stdu 1,-240(1)
	.cfi_def_cfa_offset 240
	.cfi_offset 65, 16
	.cfi_offset 26, -48
	.cfi_offset 27, -40
	.cfi_offset 28, -32
	.cfi_offset 29, -24
	.cfi_offset 30, -16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,136(31)
	std 4,128(31)
	std 5,304(31)
	std 6,120(31)
	std 7,112(31)
	ld 9,-28688(13)
	std 9,184(31)
	li 9,0
	ld 10,304(31)
	addi 9,31,150
	mr 4,10
	mr 3,9
	bl _ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC1IS0_EERKSaIT_E
	nop
	addi 9,31,168
	addi 10,31,150
	mr 4,10
	mr 3,9
.LEHB20:
	bl _ZSt18__allocate_guardedISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEESt15__allocated_ptrIT_ERS8_
	nop
.LEHE20:
	addi 9,31,168
	mr 3,9
	bl _ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE3getEv
	nop
	std 3,152(31)
	ld 10,304(31)
	addi 9,31,151
	mr 4,10
	mr 3,9
	bl _ZNSaI6sphereEC1ERKS0_
	nop
	addi 28,31,151
	ld 3,120(31)
	bl _ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
	nop
	mr 27,3
	ld 3,112(31)
	bl _ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE
	nop
	mr 26,3
	ld 29,152(31)
	mr 4,29
	li 3,72
	bl _ZnwmPv
	nop
	mr 30,3
	mr 6,26
	mr 5,27
	mr 4,28
	mr 3,30
.LEHB21:
	bl _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC1IJ4vec3dEEES1_DpOT_
	nop
.LEHE21:
	std 30,160(31)
	addi 9,31,151
	mr 3,9
	bl _ZNSaI6sphereED1Ev
	nop
	addi 9,31,168
	li 4,0
	mr 3,9
	bl _ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEaSEDn
	nop
	ld 9,136(31)
	ld 10,160(31)
	std 10,0(9)
	ld 3,160(31)
	bl _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv
	nop
	mr 10,3
	ld 9,128(31)
	std 10,0(9)
	addi 9,31,168
	mr 3,9
	bl _ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED1Ev
	nop
	addi 9,31,150
	mr 3,9
	bl _ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED1Ev
	nop
	nop
	ld 9,184(31)
	ld 10,-28688(13)
	xor. 9,9,10
	li 10,0
	beq 0,.L201
	b .L204
.L203:
	mr 28,3
	mr 4,29
	mr 3,30
	bl _ZdlPvS_
	nop
	mr 30,28
	addi 9,31,151
	mr 3,9
	bl _ZNSaI6sphereED1Ev
	nop
	addi 9,31,168
	mr 3,9
	bl _ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED1Ev
	nop
	b .L200
.L202:
	mr 30,3
.L200:
	addi 9,31,150
	mr 3,9
	bl _ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED1Ev
	nop
	mr 9,30
	mr 3,9
.LEHB22:
	bl _Unwind_Resume
	nop
.LEHE22:
.L204:
	bl __stack_chk_fail
	nop
.L201:
	addi 1,31,240
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 26,-48(1)
	ld 27,-40(1)
	ld 28,-32(1)
	ld 29,-24(1)
	ld 30,-16(1)
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,6,0,1
	.cfi_endproc
.LFE4653:
	.section	.gcc_except_table
.LLSDA4653:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4653-.LLSDACSB4653
.LLSDACSB4653:
	.uleb128 .LEHB20-.LFB4653
	.uleb128 .LEHE20-.LEHB20
	.uleb128 .L202-.LFB4653
	.uleb128 0
	.uleb128 .LEHB21-.LFB4653
	.uleb128 .LEHE21-.LEHB21
	.uleb128 .L203-.LFB4653
	.uleb128 0
	.uleb128 .LEHB22-.LFB4653
	.uleb128 .LEHE22-.LEHB22
	.uleb128 0
	.uleb128 0
.LLSDACSE4653:
	.section	.text._ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_,"axG",@progbits,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC5I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_,comdat
	.size	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_,.-.L._ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_
	.set	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_
	.section	.text._ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EE31_M_enable_shared_from_this_withIS0_S0_EENSt9enable_ifIXntsrNS3_15__has_esft_baseIT0_vEE5valueEvE4typeEPT_,"axG",@progbits,_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EE31_M_enable_shared_from_this_withIS0_S0_EENSt9enable_ifIXntsrNS3_15__has_esft_baseIT0_vEE5valueEvE4typeEPT_,comdat
	.align 2
	.weak	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EE31_M_enable_shared_from_this_withIS0_S0_EENSt9enable_ifIXntsrNS3_15__has_esft_baseIT0_vEE5valueEvE4typeEPT_
	.section	".opd","aw"
	.align 3
_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EE31_M_enable_shared_from_this_withIS0_S0_EENSt9enable_ifIXntsrNS3_15__has_esft_baseIT0_vEE5valueEvE4typeEPT_:
	.quad	.L._ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EE31_M_enable_shared_from_this_withIS0_S0_EENSt9enable_ifIXntsrNS3_15__has_esft_baseIT0_vEE5valueEvE4typeEPT_,.TOC.@tocbase,0
	.previous
	.type	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EE31_M_enable_shared_from_this_withIS0_S0_EENSt9enable_ifIXntsrNS3_15__has_esft_baseIT0_vEE5valueEvE4typeEPT_, @function
.L._ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EE31_M_enable_shared_from_this_withIS0_S0_EENSt9enable_ifIXntsrNS3_15__has_esft_baseIT0_vEE5valueEvE4typeEPT_:
.LFB4655:
	.cfi_startproc
	std 31,-8(1)
	stdu 1,-64(1)
	.cfi_def_cfa_offset 64
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,112(31)
	std 4,120(31)
	nop
	addi 1,31,64
	.cfi_def_cfa 1, 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,0,128,1,0,1
	.cfi_endproc
.LFE4655:
	.size	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EE31_M_enable_shared_from_this_withIS0_S0_EENSt9enable_ifIXntsrNS3_15__has_esft_baseIT0_vEE5valueEvE4typeEPT_,.-.L._ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EE31_M_enable_shared_from_this_withIS0_S0_EENSt9enable_ifIXntsrNS3_15__has_esft_baseIT0_vEE5valueEvE4typeEPT_
	.section	.text._ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_,"axG",@progbits,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC5I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_,comdat
	.align 2
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_
	.section	".opd","aw"
	.align 3
_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_:
	.quad	.L._ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_,.TOC.@tocbase,0
	.previous
	.type	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_, @function
.L._ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_:
.LFB4657:
	.cfi_startproc
	.cfi_personality 0x94,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x14,.LLSDA4657
	mflr 0
	std 0,16(1)
	std 26,-48(1)
	std 27,-40(1)
	std 28,-32(1)
	std 29,-24(1)
	std 30,-16(1)
	std 31,-8(1)
	stdu 1,-240(1)
	.cfi_def_cfa_offset 240
	.cfi_offset 65, 16
	.cfi_offset 26, -48
	.cfi_offset 27, -40
	.cfi_offset 28, -32
	.cfi_offset 29, -24
	.cfi_offset 30, -16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,136(31)
	std 4,128(31)
	std 5,304(31)
	std 6,120(31)
	std 7,112(31)
	ld 9,-28688(13)
	std 9,184(31)
	li 9,0
	ld 10,304(31)
	addi 9,31,150
	mr 4,10
	mr 3,9
	bl _ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC1IS0_EERKSaIT_E
	nop
	addi 9,31,168
	addi 10,31,150
	mr 4,10
	mr 3,9
.LEHB23:
	bl _ZSt18__allocate_guardedISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEESt15__allocated_ptrIT_ERS8_
	nop
.LEHE23:
	addi 9,31,168
	mr 3,9
	bl _ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE3getEv
	nop
	std 3,152(31)
	ld 10,304(31)
	addi 9,31,151
	mr 4,10
	mr 3,9
	bl _ZNSaI6sphereEC1ERKS0_
	nop
	addi 28,31,151
	ld 3,120(31)
	bl _ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
	nop
	mr 27,3
	ld 3,112(31)
	bl _ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE
	nop
	mr 26,3
	ld 29,152(31)
	mr 4,29
	li 3,72
	bl _ZnwmPv
	nop
	mr 30,3
	mr 6,26
	mr 5,27
	mr 4,28
	mr 3,30
.LEHB24:
	bl _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC1IJ4vec3iEEES1_DpOT_
	nop
.LEHE24:
	std 30,160(31)
	addi 9,31,151
	mr 3,9
	bl _ZNSaI6sphereED1Ev
	nop
	addi 9,31,168
	li 4,0
	mr 3,9
	bl _ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEaSEDn
	nop
	ld 9,136(31)
	ld 10,160(31)
	std 10,0(9)
	ld 3,160(31)
	bl _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv
	nop
	mr 10,3
	ld 9,128(31)
	std 10,0(9)
	addi 9,31,168
	mr 3,9
	bl _ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED1Ev
	nop
	addi 9,31,150
	mr 3,9
	bl _ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED1Ev
	nop
	nop
	ld 9,184(31)
	ld 10,-28688(13)
	xor. 9,9,10
	li 10,0
	beq 0,.L209
	b .L212
.L211:
	mr 28,3
	mr 4,29
	mr 3,30
	bl _ZdlPvS_
	nop
	mr 30,28
	addi 9,31,151
	mr 3,9
	bl _ZNSaI6sphereED1Ev
	nop
	addi 9,31,168
	mr 3,9
	bl _ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED1Ev
	nop
	b .L208
.L210:
	mr 30,3
.L208:
	addi 9,31,150
	mr 3,9
	bl _ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED1Ev
	nop
	mr 9,30
	mr 3,9
.LEHB25:
	bl _Unwind_Resume
	nop
.LEHE25:
.L212:
	bl __stack_chk_fail
	nop
.L209:
	addi 1,31,240
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 26,-48(1)
	ld 27,-40(1)
	ld 28,-32(1)
	ld 29,-24(1)
	ld 30,-16(1)
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,6,0,1
	.cfi_endproc
.LFE4657:
	.section	.gcc_except_table
.LLSDA4657:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4657-.LLSDACSB4657
.LLSDACSB4657:
	.uleb128 .LEHB23-.LFB4657
	.uleb128 .LEHE23-.LEHB23
	.uleb128 .L210-.LFB4657
	.uleb128 0
	.uleb128 .LEHB24-.LFB4657
	.uleb128 .LEHE24-.LEHB24
	.uleb128 .L211-.LFB4657
	.uleb128 0
	.uleb128 .LEHB25-.LFB4657
	.uleb128 .LEHE25-.LEHB25
	.uleb128 0
	.uleb128 0
.LLSDACSE4657:
	.section	.text._ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_,"axG",@progbits,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC5I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_,comdat
	.size	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_,.-.L._ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_
	.set	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_
	.section	.text._ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE11_M_gen_randEv,"axG",@progbits,_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE11_M_gen_randEv,comdat
	.align 2
	.weak	_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE11_M_gen_randEv
	.section	".opd","aw"
	.align 3
_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE11_M_gen_randEv:
	.quad	.L._ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE11_M_gen_randEv,.TOC.@tocbase,0
	.previous
	.type	_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE11_M_gen_randEv, @function
.L._ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE11_M_gen_randEv:
.LFB4685:
	.cfi_startproc
	std 31,-8(1)
	stdu 1,-128(1)
	.cfi_def_cfa_offset 128
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,176(31)
	lis 9,0x8000
	std 9,72(31)
	li 9,-1
	rldicl 9,9,0,33
	std 9,80(31)
	li 9,0
	std 9,56(31)
.L217:
	ld 9,56(31)
	cmpldi 0,9,226
	bgt 0,.L214
	ld 10,176(31)
	ld 9,56(31)
	sldi 9,9,3
	add 9,10,9
	ld 9,0(9)
	rldicr 10,9,0,32
	ld 9,56(31)
	addi 9,9,1
	ld 8,176(31)
	sldi 9,9,3
	add 9,8,9
	ld 9,0(9)
	rldicl 9,9,0,33
	or 9,10,9
	std 9,88(31)
	ld 9,56(31)
	addi 9,9,397
	ld 10,176(31)
	sldi 9,9,3
	add 9,10,9
	ld 10,0(9)
	ld 9,88(31)
	srdi 9,9,1
	xor 10,10,9
	ld 9,88(31)
	rldicl 9,9,0,63
	cmpdi 0,9,0
	beq 0,.L215
	lis 9,0x9908
	ori 9,9,0xb0df
	rldicl 9,9,0,32
	b .L216
.L215:
	li 9,0
.L216:
	xor 10,9,10
	ld 8,176(31)
	ld 9,56(31)
	sldi 9,9,3
	add 9,8,9
	std 10,0(9)
	ld 9,56(31)
	addi 9,9,1
	std 9,56(31)
	b .L217
.L214:
	li 9,227
	std 9,64(31)
.L221:
	ld 9,64(31)
	cmpldi 0,9,622
	bgt 0,.L218
	ld 10,176(31)
	ld 9,64(31)
	sldi 9,9,3
	add 9,10,9
	ld 9,0(9)
	rldicr 10,9,0,32
	ld 9,64(31)
	addi 9,9,1
	ld 8,176(31)
	sldi 9,9,3
	add 9,8,9
	ld 9,0(9)
	rldicl 9,9,0,33
	or 9,10,9
	std 9,96(31)
	ld 9,64(31)
	addi 9,9,-227
	ld 10,176(31)
	sldi 9,9,3
	add 9,10,9
	ld 10,0(9)
	ld 9,96(31)
	srdi 9,9,1
	xor 10,10,9
	ld 9,96(31)
	rldicl 9,9,0,63
	cmpdi 0,9,0
	beq 0,.L219
	lis 9,0x9908
	ori 9,9,0xb0df
	rldicl 9,9,0,32
	b .L220
.L219:
	li 9,0
.L220:
	xor 10,9,10
	ld 8,176(31)
	ld 9,64(31)
	sldi 9,9,3
	add 9,8,9
	std 10,0(9)
	ld 9,64(31)
	addi 9,9,1
	std 9,64(31)
	b .L221
.L218:
	ld 9,176(31)
	ld 9,4984(9)
	rldicr 10,9,0,32
	ld 9,176(31)
	ld 9,0(9)
	rldicl 9,9,0,33
	or 9,10,9
	std 9,104(31)
	ld 9,176(31)
	ld 10,3168(9)
	ld 9,104(31)
	srdi 9,9,1
	xor 10,10,9
	ld 9,104(31)
	rldicl 9,9,0,63
	cmpdi 0,9,0
	beq 0,.L222
	lis 9,0x9908
	ori 9,9,0xb0df
	rldicl 9,9,0,32
	b .L223
.L222:
	li 9,0
.L223:
	xor 10,9,10
	ld 9,176(31)
	std 10,4984(9)
	ld 9,176(31)
	li 10,0
	std 10,4992(9)
	nop
	addi 1,31,128
	.cfi_def_cfa 1, 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,0,128,1,0,1
	.cfi_endproc
.LFE4685:
	.size	_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE11_M_gen_randEv,.-.L._ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE11_M_gen_randEv
	.section	.text._ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC2IS0_EERKSaIT_E,"axG",@progbits,_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC5IS0_EERKSaIT_E,comdat
	.align 2
	.weak	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC2IS0_EERKSaIT_E
	.section	".opd","aw"
	.align 3
_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC2IS0_EERKSaIT_E:
	.quad	.L._ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC2IS0_EERKSaIT_E,.TOC.@tocbase,0
	.previous
	.type	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC2IS0_EERKSaIT_E, @function
.L._ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC2IS0_EERKSaIT_E:
.LFB4688:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	std 31,-8(1)
	stdu 1,-128(1)
	.cfi_def_cfa_offset 128
	.cfi_offset 65, 16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,176(31)
	std 4,184(31)
	ld 3,176(31)
	bl _ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC2Ev
	nop
	nop
	addi 1,31,128
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,1,0,1
	.cfi_endproc
.LFE4688:
	.size	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC2IS0_EERKSaIT_E,.-.L._ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC2IS0_EERKSaIT_E
	.weak	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC1IS0_EERKSaIT_E
	.set	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC1IS0_EERKSaIT_E,_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC2IS0_EERKSaIT_E
	.section	.text._ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED2Ev,"axG",@progbits,_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED5Ev,comdat
	.align 2
	.weak	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED2Ev
	.section	".opd","aw"
	.align 3
_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED2Ev:
	.quad	.L._ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED2Ev,.TOC.@tocbase,0
	.previous
	.type	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED2Ev, @function
.L._ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED2Ev:
.LFB4691:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	std 31,-8(1)
	stdu 1,-128(1)
	.cfi_def_cfa_offset 128
	.cfi_offset 65, 16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,176(31)
	ld 3,176(31)
	bl _ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED2Ev
	nop
	nop
	addi 1,31,128
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,1,0,1
	.cfi_endproc
.LFE4691:
	.size	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED2Ev,.-.L._ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED2Ev
	.weak	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED1Ev
	.set	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED1Ev,_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED2Ev
	.section	.text._ZSt18__allocate_guardedISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEESt15__allocated_ptrIT_ERS8_,"axG",@progbits,_ZSt18__allocate_guardedISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEESt15__allocated_ptrIT_ERS8_,comdat
	.align 2
	.weak	_ZSt18__allocate_guardedISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEESt15__allocated_ptrIT_ERS8_
	.section	".opd","aw"
	.align 3
_ZSt18__allocate_guardedISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEESt15__allocated_ptrIT_ERS8_:
	.quad	.L._ZSt18__allocate_guardedISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEESt15__allocated_ptrIT_ERS8_,.TOC.@tocbase,0
	.previous
	.type	_ZSt18__allocate_guardedISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEESt15__allocated_ptrIT_ERS8_, @function
.L._ZSt18__allocate_guardedISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEESt15__allocated_ptrIT_ERS8_:
.LFB4693:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	std 31,-8(1)
	stdu 1,-128(1)
	.cfi_def_cfa_offset 128
	.cfi_offset 65, 16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,176(31)
	std 4,184(31)
	li 4,1
	ld 3,184(31)
	bl _ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE8allocateERS6_m
	nop
	mr 9,3
	mr 5,9
	ld 4,184(31)
	ld 3,176(31)
	bl _ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC1ERS6_PS5_
	nop
	ld 3,176(31)
	addi 1,31,128
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,1,0,1
	.cfi_endproc
.LFE4693:
	.size	_ZSt18__allocate_guardedISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEESt15__allocated_ptrIT_ERS8_,.-.L._ZSt18__allocate_guardedISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEESt15__allocated_ptrIT_ERS8_
	.section	.text._ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED2Ev,"axG",@progbits,_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED5Ev,comdat
	.align 2
	.weak	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED2Ev
	.section	".opd","aw"
	.align 3
_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED2Ev:
	.quad	.L._ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED2Ev,.TOC.@tocbase,0
	.previous
	.type	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED2Ev, @function
.L._ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED2Ev:
.LFB4695:
	.cfi_startproc
	.cfi_personality 0x94,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x14,.LLSDA4695
	mflr 0
	std 0,16(1)
	std 31,-8(1)
	stdu 1,-128(1)
	.cfi_def_cfa_offset 128
	.cfi_offset 65, 16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,176(31)
	ld 9,176(31)
	ld 9,8(9)
	cmpdi 0,9,0
	beq 0,.L230
	ld 9,176(31)
	ld 10,0(9)
	ld 9,176(31)
	ld 9,8(9)
	li 5,1
	mr 4,9
	mr 3,10
	bl _ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE10deallocateERS6_PS5_m
	nop
.L230:
	nop
	addi 1,31,128
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,1,0,1
	.cfi_endproc
.LFE4695:
	.section	.gcc_except_table
.LLSDA4695:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4695-.LLSDACSB4695
.LLSDACSB4695:
.LLSDACSE4695:
	.section	.text._ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED2Ev,"axG",@progbits,_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED5Ev,comdat
	.size	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED2Ev,.-.L._ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED2Ev
	.weak	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED1Ev
	.set	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED1Ev,_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED2Ev
	.section	.text._ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE3getEv,"axG",@progbits,_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE3getEv,comdat
	.align 2
	.weak	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE3getEv
	.section	".opd","aw"
	.align 3
_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE3getEv:
	.quad	.L._ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE3getEv,.TOC.@tocbase,0
	.previous
	.type	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE3getEv, @function
.L._ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE3getEv:
.LFB4700:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	std 31,-8(1)
	stdu 1,-128(1)
	.cfi_def_cfa_offset 128
	.cfi_offset 65, 16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,176(31)
	ld 9,176(31)
	ld 9,8(9)
	mr 3,9
	bl _ZSt12__to_addressISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEPT_S7_
	nop
	mr 9,3
	mr 3,9
	addi 1,31,128
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,1,0,1
	.cfi_endproc
.LFE4700:
	.size	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE3getEv,.-.L._ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE3getEv
	.section	.text._ZNSaI6sphereEC2ERKS0_,"axG",@progbits,_ZNSaI6sphereEC5ERKS0_,comdat
	.align 2
	.weak	_ZNSaI6sphereEC2ERKS0_
	.section	".opd","aw"
	.align 3
_ZNSaI6sphereEC2ERKS0_:
	.quad	.L._ZNSaI6sphereEC2ERKS0_,.TOC.@tocbase,0
	.previous
	.type	_ZNSaI6sphereEC2ERKS0_, @function
.L._ZNSaI6sphereEC2ERKS0_:
.LFB4702:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	std 31,-8(1)
	stdu 1,-128(1)
	.cfi_def_cfa_offset 128
	.cfi_offset 65, 16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,176(31)
	std 4,184(31)
	ld 4,184(31)
	ld 3,176(31)
	bl _ZN9__gnu_cxx13new_allocatorI6sphereEC2ERKS2_
	nop
	nop
	addi 1,31,128
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,1,0,1
	.cfi_endproc
.LFE4702:
	.size	_ZNSaI6sphereEC2ERKS0_,.-.L._ZNSaI6sphereEC2ERKS0_
	.weak	_ZNSaI6sphereEC1ERKS0_
	.set	_ZNSaI6sphereEC1ERKS0_,_ZNSaI6sphereEC2ERKS0_
	.section	.text._ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED2Ev,"axG",@progbits,_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED5Ev,comdat
	.align 2
	.weak	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED2Ev
	.section	".opd","aw"
	.align 3
_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED2Ev:
	.quad	.L._ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED2Ev,.TOC.@tocbase,0
	.previous
	.type	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED2Ev, @function
.L._ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED2Ev:
.LFB4707:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	std 31,-8(1)
	stdu 1,-128(1)
	.cfi_def_cfa_offset 128
	.cfi_offset 65, 16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,176(31)
	ld 3,176(31)
	bl _ZNSaI6sphereED2Ev
	nop
	nop
	addi 1,31,128
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,1,0,1
	.cfi_endproc
.LFE4707:
	.size	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED2Ev,.-.L._ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED2Ev
	.weak	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED1Ev
	.set	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED1Ev,_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED2Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD2Ev,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD5Ev,comdat
	.align 2
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD2Ev
	.section	".opd","aw"
	.align 3
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD2Ev:
	.quad	.L._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD2Ev,.TOC.@tocbase,0
	.previous
	.type	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD2Ev, @function
.L._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD2Ev:
.LFB4709:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	std 31,-8(1)
	stdu 1,-128(1)
	.cfi_def_cfa_offset 128
	.cfi_offset 65, 16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,176(31)
	ld 3,176(31)
	bl _ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED2Ev
	nop
	nop
	addi 1,31,128
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,1,0,1
	.cfi_endproc
.LFE4709:
	.size	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD2Ev,.-.L._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD2Ev
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD1Ev
	.set	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD1Ev,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD2Ev
	.section	".toc","aw"
.LC23:
	.quad	_ZTVSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE+16
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3dEEES1_DpOT_,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC5IJ4vec3dEEES1_DpOT_,comdat
	.align 2
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3dEEES1_DpOT_
	.section	".opd","aw"
	.align 3
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3dEEES1_DpOT_:
	.quad	.L._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3dEEES1_DpOT_,.TOC.@tocbase,0
	.previous
	.type	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3dEEES1_DpOT_, @function
.L._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3dEEES1_DpOT_:
.LFB4711:
	.cfi_startproc
	.cfi_personality 0x94,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x14,.LLSDA4711
	mflr 0
	std 0,16(1)
	std 29,-24(1)
	std 30,-16(1)
	std 31,-8(1)
	stdu 1,-192(1)
	.cfi_def_cfa_offset 192
	.cfi_offset 65, 16
	.cfi_offset 29, -24
	.cfi_offset 30, -16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,136(31)
	std 4,128(31)
	std 5,120(31)
	std 6,112(31)
	ld 9,-28688(13)
	std 9,152(31)
	li 9,0
	ld 9,136(31)
	mr 3,9
	bl _ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC2Ev
	nop
	addis 9,2,.LC23@toc@ha
	ld 10,.LC23@toc@l(9)
	ld 9,136(31)
	std 10,0(9)
	ld 9,136(31)
	addi 30,9,16
	addi 9,31,151
	ld 4,128(31)
	mr 3,9
	bl _ZNSaI6sphereEC1ERKS0_
	nop
	addi 9,31,151
	mr 4,9
	mr 3,30
	bl _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC1ES1_
	nop
	addi 9,31,151
	mr 3,9
	bl _ZNSaI6sphereED1Ev
	nop
	ld 3,136(31)
	bl _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv
	nop
	mr 30,3
	ld 3,120(31)
	bl _ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
	nop
	mr 29,3
	ld 3,112(31)
	bl _ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE
	nop
	mr 9,3
	mr 6,9
	mr 5,29
	mr 4,30
	ld 3,128(31)
.LEHB26:
	bl _ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3dEEEvRS1_PT_DpOT0_
	nop
.LEHE26:
	b .L240
.L239:
	mr 30,3
	ld 9,136(31)
	addi 9,9,16
	mr 3,9
	bl _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD1Ev
	nop
	ld 9,136(31)
	mr 3,9
	bl _ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev
	nop
	mr 9,30
	mr 3,9
.LEHB27:
	bl _Unwind_Resume
	nop
.LEHE27:
.L240:
	ld 9,152(31)
	ld 10,-28688(13)
	xor. 9,9,10
	li 10,0
	beq 0,.L238
	bl __stack_chk_fail
	nop
.L238:
	addi 1,31,192
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 29,-24(1)
	ld 30,-16(1)
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,3,0,1
	.cfi_endproc
.LFE4711:
	.section	.gcc_except_table
.LLSDA4711:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4711-.LLSDACSB4711
.LLSDACSB4711:
	.uleb128 .LEHB26-.LFB4711
	.uleb128 .LEHE26-.LEHB26
	.uleb128 .L239-.LFB4711
	.uleb128 0
	.uleb128 .LEHB27-.LFB4711
	.uleb128 .LEHE27-.LEHB27
	.uleb128 0
	.uleb128 0
.LLSDACSE4711:
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3dEEES1_DpOT_,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC5IJ4vec3dEEES1_DpOT_,comdat
	.size	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3dEEES1_DpOT_,.-.L._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3dEEES1_DpOT_
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC1IJ4vec3dEEES1_DpOT_
	.set	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC1IJ4vec3dEEES1_DpOT_,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3dEEES1_DpOT_
	.section	.text._ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEaSEDn,"axG",@progbits,_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEaSEDn,comdat
	.align 2
	.weak	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEaSEDn
	.section	".opd","aw"
	.align 3
_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEaSEDn:
	.quad	.L._ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEaSEDn,.TOC.@tocbase,0
	.previous
	.type	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEaSEDn, @function
.L._ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEaSEDn:
.LFB4713:
	.cfi_startproc
	std 31,-8(1)
	stdu 1,-64(1)
	.cfi_def_cfa_offset 64
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,112(31)
	std 4,120(31)
	ld 9,112(31)
	li 10,0
	std 10,8(9)
	ld 9,112(31)
	mr 3,9
	addi 1,31,64
	.cfi_def_cfa 1, 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,0,128,1,0,1
	.cfi_endproc
.LFE4713:
	.size	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEaSEDn,.-.L._ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEaSEDn
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv,comdat
	.align 2
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv
	.section	".opd","aw"
	.align 3
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv:
	.quad	.L._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv,.TOC.@tocbase,0
	.previous
	.type	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv, @function
.L._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv:
.LFB4714:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	std 31,-8(1)
	stdu 1,-128(1)
	.cfi_def_cfa_offset 128
	.cfi_offset 65, 16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,176(31)
	ld 9,176(31)
	addi 9,9,16
	mr 3,9
	bl _ZN9__gnu_cxx16__aligned_bufferI6sphereE6_M_ptrEv
	nop
	mr 9,3
	mr 3,9
	addi 1,31,128
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,1,0,1
	.cfi_endproc
.LFE4714:
	.size	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv,.-.L._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv
	.section	".toc","aw"
	.set .LC24,.LC23
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3iEEES1_DpOT_,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC5IJ4vec3iEEES1_DpOT_,comdat
	.align 2
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3iEEES1_DpOT_
	.section	".opd","aw"
	.align 3
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3iEEES1_DpOT_:
	.quad	.L._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3iEEES1_DpOT_,.TOC.@tocbase,0
	.previous
	.type	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3iEEES1_DpOT_, @function
.L._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3iEEES1_DpOT_:
.LFB4716:
	.cfi_startproc
	.cfi_personality 0x94,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x14,.LLSDA4716
	mflr 0
	std 0,16(1)
	std 29,-24(1)
	std 30,-16(1)
	std 31,-8(1)
	stdu 1,-192(1)
	.cfi_def_cfa_offset 192
	.cfi_offset 65, 16
	.cfi_offset 29, -24
	.cfi_offset 30, -16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,136(31)
	std 4,128(31)
	std 5,120(31)
	std 6,112(31)
	ld 9,-28688(13)
	std 9,152(31)
	li 9,0
	ld 9,136(31)
	mr 3,9
	bl _ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC2Ev
	nop
	addis 9,2,.LC24@toc@ha
	ld 10,.LC24@toc@l(9)
	ld 9,136(31)
	std 10,0(9)
	ld 9,136(31)
	addi 30,9,16
	addi 9,31,151
	ld 4,128(31)
	mr 3,9
	bl _ZNSaI6sphereEC1ERKS0_
	nop
	addi 9,31,151
	mr 4,9
	mr 3,30
	bl _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC1ES1_
	nop
	addi 9,31,151
	mr 3,9
	bl _ZNSaI6sphereED1Ev
	nop
	ld 3,136(31)
	bl _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv
	nop
	mr 30,3
	ld 3,120(31)
	bl _ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
	nop
	mr 29,3
	ld 3,112(31)
	bl _ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE
	nop
	mr 9,3
	mr 6,9
	mr 5,29
	mr 4,30
	ld 3,128(31)
.LEHB28:
	bl _ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3iEEEvRS1_PT_DpOT0_
	nop
.LEHE28:
	b .L249
.L248:
	mr 30,3
	ld 9,136(31)
	addi 9,9,16
	mr 3,9
	bl _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD1Ev
	nop
	ld 9,136(31)
	mr 3,9
	bl _ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev
	nop
	mr 9,30
	mr 3,9
.LEHB29:
	bl _Unwind_Resume
	nop
.LEHE29:
.L249:
	ld 9,152(31)
	ld 10,-28688(13)
	xor. 9,9,10
	li 10,0
	beq 0,.L247
	bl __stack_chk_fail
	nop
.L247:
	addi 1,31,192
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 29,-24(1)
	ld 30,-16(1)
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,3,0,1
	.cfi_endproc
.LFE4716:
	.section	.gcc_except_table
.LLSDA4716:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4716-.LLSDACSB4716
.LLSDACSB4716:
	.uleb128 .LEHB28-.LFB4716
	.uleb128 .LEHE28-.LEHB28
	.uleb128 .L248-.LFB4716
	.uleb128 0
	.uleb128 .LEHB29-.LFB4716
	.uleb128 .LEHE29-.LEHB29
	.uleb128 0
	.uleb128 0
.LLSDACSE4716:
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3iEEES1_DpOT_,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC5IJ4vec3iEEES1_DpOT_,comdat
	.size	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3iEEES1_DpOT_,.-.L._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3iEEES1_DpOT_
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC1IJ4vec3iEEES1_DpOT_
	.set	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC1IJ4vec3iEEES1_DpOT_,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3iEEES1_DpOT_
	.section	.text._ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC2Ev,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC5Ev,comdat
	.align 2
	.weak	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC2Ev
	.section	".opd","aw"
	.align 3
_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC2Ev:
	.quad	.L._ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC2Ev,.TOC.@tocbase,0
	.previous
	.type	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC2Ev, @function
.L._ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC2Ev:
.LFB4729:
	.cfi_startproc
	std 31,-8(1)
	stdu 1,-64(1)
	.cfi_def_cfa_offset 64
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,112(31)
	nop
	addi 1,31,64
	.cfi_def_cfa 1, 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,0,128,1,0,1
	.cfi_endproc
.LFE4729:
	.size	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC2Ev,.-.L._ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC2Ev
	.weak	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC1Ev
	.set	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC1Ev,_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC2Ev
	.section	.text._ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED2Ev,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED5Ev,comdat
	.align 2
	.weak	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED2Ev
	.section	".opd","aw"
	.align 3
_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED2Ev:
	.quad	.L._ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED2Ev,.TOC.@tocbase,0
	.previous
	.type	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED2Ev, @function
.L._ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED2Ev:
.LFB4732:
	.cfi_startproc
	std 31,-8(1)
	stdu 1,-64(1)
	.cfi_def_cfa_offset 64
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,112(31)
	nop
	addi 1,31,64
	.cfi_def_cfa 1, 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,0,128,1,0,1
	.cfi_endproc
.LFE4732:
	.size	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED2Ev,.-.L._ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED2Ev
	.weak	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED1Ev
	.set	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED1Ev,_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED2Ev
	.section	.text._ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE8allocateERS6_m,"axG",@progbits,_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE8allocateERS6_m,comdat
	.align 2
	.weak	_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE8allocateERS6_m
	.section	".opd","aw"
	.align 3
_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE8allocateERS6_m:
	.quad	.L._ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE8allocateERS6_m,.TOC.@tocbase,0
	.previous
	.type	_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE8allocateERS6_m, @function
.L._ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE8allocateERS6_m:
.LFB4734:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	std 31,-8(1)
	stdu 1,-128(1)
	.cfi_def_cfa_offset 128
	.cfi_offset 65, 16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,176(31)
	std 4,184(31)
	li 5,0
	ld 4,184(31)
	ld 3,176(31)
	bl _ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8allocateEmPKv
	nop
	mr 9,3
	mr 3,9
	addi 1,31,128
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,1,0,1
	.cfi_endproc
.LFE4734:
	.size	_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE8allocateERS6_m,.-.L._ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE8allocateERS6_m
	.section	.text._ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC2ERS6_PS5_,"axG",@progbits,_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC5ERS6_PS5_,comdat
	.align 2
	.weak	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC2ERS6_PS5_
	.section	".opd","aw"
	.align 3
_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC2ERS6_PS5_:
	.quad	.L._ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC2ERS6_PS5_,.TOC.@tocbase,0
	.previous
	.type	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC2ERS6_PS5_, @function
.L._ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC2ERS6_PS5_:
.LFB4736:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	std 31,-8(1)
	stdu 1,-128(1)
	.cfi_def_cfa_offset 128
	.cfi_offset 65, 16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,176(31)
	std 4,184(31)
	std 5,192(31)
	ld 3,184(31)
	bl _ZSt11__addressofISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEPT_RS7_
	nop
	mr 10,3
	ld 9,176(31)
	std 10,0(9)
	ld 9,176(31)
	ld 10,192(31)
	std 10,8(9)
	nop
	addi 1,31,128
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,1,0,1
	.cfi_endproc
.LFE4736:
	.size	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC2ERS6_PS5_,.-.L._ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC2ERS6_PS5_
	.weak	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC1ERS6_PS5_
	.set	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC1ERS6_PS5_,_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC2ERS6_PS5_
	.section	.text._ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE10deallocateERS6_PS5_m,"axG",@progbits,_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE10deallocateERS6_PS5_m,comdat
	.align 2
	.weak	_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE10deallocateERS6_PS5_m
	.section	".opd","aw"
	.align 3
_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE10deallocateERS6_PS5_m:
	.quad	.L._ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE10deallocateERS6_PS5_m,.TOC.@tocbase,0
	.previous
	.type	_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE10deallocateERS6_PS5_m, @function
.L._ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE10deallocateERS6_PS5_m:
.LFB4738:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	std 31,-8(1)
	stdu 1,-128(1)
	.cfi_def_cfa_offset 128
	.cfi_offset 65, 16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,176(31)
	std 4,184(31)
	std 5,192(31)
	ld 5,192(31)
	ld 4,184(31)
	ld 3,176(31)
	bl _ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE10deallocateEPS5_m
	nop
	nop
	addi 1,31,128
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,1,0,1
	.cfi_endproc
.LFE4738:
	.size	_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE10deallocateERS6_PS5_m,.-.L._ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE10deallocateERS6_PS5_m
	.section	.text._ZSt12__to_addressISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEPT_S7_,"axG",@progbits,_ZSt12__to_addressISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEPT_S7_,comdat
	.align 2
	.weak	_ZSt12__to_addressISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEPT_S7_
	.section	".opd","aw"
	.align 3
_ZSt12__to_addressISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEPT_S7_:
	.quad	.L._ZSt12__to_addressISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEPT_S7_,.TOC.@tocbase,0
	.previous
	.type	_ZSt12__to_addressISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEPT_S7_, @function
.L._ZSt12__to_addressISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEPT_S7_:
.LFB4739:
	.cfi_startproc
	std 31,-8(1)
	stdu 1,-64(1)
	.cfi_def_cfa_offset 64
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,112(31)
	ld 9,112(31)
	mr 3,9
	addi 1,31,64
	.cfi_def_cfa 1, 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,0,128,1,0,1
	.cfi_endproc
.LFE4739:
	.size	_ZSt12__to_addressISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEPT_S7_,.-.L._ZSt12__to_addressISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEPT_S7_
	.section	.text._ZN9__gnu_cxx13new_allocatorI6sphereEC2ERKS2_,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorI6sphereEC5ERKS2_,comdat
	.align 2
	.weak	_ZN9__gnu_cxx13new_allocatorI6sphereEC2ERKS2_
	.section	".opd","aw"
	.align 3
_ZN9__gnu_cxx13new_allocatorI6sphereEC2ERKS2_:
	.quad	.L._ZN9__gnu_cxx13new_allocatorI6sphereEC2ERKS2_,.TOC.@tocbase,0
	.previous
	.type	_ZN9__gnu_cxx13new_allocatorI6sphereEC2ERKS2_, @function
.L._ZN9__gnu_cxx13new_allocatorI6sphereEC2ERKS2_:
.LFB4741:
	.cfi_startproc
	std 31,-8(1)
	stdu 1,-64(1)
	.cfi_def_cfa_offset 64
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,112(31)
	std 4,120(31)
	nop
	addi 1,31,64
	.cfi_def_cfa 1, 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,0,128,1,0,1
	.cfi_endproc
.LFE4741:
	.size	_ZN9__gnu_cxx13new_allocatorI6sphereEC2ERKS2_,.-.L._ZN9__gnu_cxx13new_allocatorI6sphereEC2ERKS2_
	.weak	_ZN9__gnu_cxx13new_allocatorI6sphereEC1ERKS2_
	.set	_ZN9__gnu_cxx13new_allocatorI6sphereEC1ERKS2_,_ZN9__gnu_cxx13new_allocatorI6sphereEC2ERKS2_
	.section	".toc","aw"
	.set .LC25,.LC21
	.section	.text._ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC2Ev,"axG",@progbits,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC5Ev,comdat
	.align 2
	.weak	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC2Ev
	.section	".opd","aw"
	.align 3
_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC2Ev:
	.quad	.L._ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC2Ev,.TOC.@tocbase,0
	.previous
	.type	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC2Ev, @function
.L._ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC2Ev:
.LFB4744:
	.cfi_startproc
	std 31,-8(1)
	stdu 1,-64(1)
	.cfi_def_cfa_offset 64
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,112(31)
	addis 9,2,.LC25@toc@ha
	ld 10,.LC25@toc@l(9)
	ld 9,112(31)
	std 10,0(9)
	ld 9,112(31)
	li 10,1
	stw 10,8(9)
	ld 9,112(31)
	li 10,1
	stw 10,12(9)
	nop
	addi 1,31,64
	.cfi_def_cfa 1, 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,0,128,1,0,1
	.cfi_endproc
.LFE4744:
	.size	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC2Ev,.-.L._ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC2Ev
	.weak	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC1Ev
	.set	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC1Ev,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC2Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC2ES1_,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC5ES1_,comdat
	.align 2
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC2ES1_
	.section	".opd","aw"
	.align 3
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC2ES1_:
	.quad	.L._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC2ES1_,.TOC.@tocbase,0
	.previous
	.type	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC2ES1_, @function
.L._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC2ES1_:
.LFB4747:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	std 31,-8(1)
	stdu 1,-128(1)
	.cfi_def_cfa_offset 128
	.cfi_offset 65, 16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,176(31)
	std 4,184(31)
	ld 4,184(31)
	ld 3,176(31)
	bl _ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC2ERKS1_
	nop
	nop
	addi 1,31,128
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,1,0,1
	.cfi_endproc
.LFE4747:
	.size	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC2ES1_,.-.L._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC2ES1_
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC1ES1_
	.set	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC1ES1_,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC2ES1_
	.section	.text._ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3dEEEvRS1_PT_DpOT0_,"axG",@progbits,_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3dEEEvRS1_PT_DpOT0_,comdat
	.align 2
	.weak	_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3dEEEvRS1_PT_DpOT0_
	.section	".opd","aw"
	.align 3
_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3dEEEvRS1_PT_DpOT0_:
	.quad	.L._ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3dEEEvRS1_PT_DpOT0_,.TOC.@tocbase,0
	.previous
	.type	_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3dEEEvRS1_PT_DpOT0_, @function
.L._ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3dEEEvRS1_PT_DpOT0_:
.LFB4749:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	std 30,-16(1)
	std 31,-8(1)
	stdu 1,-128(1)
	.cfi_def_cfa_offset 128
	.cfi_offset 65, 16
	.cfi_offset 30, -16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,176(31)
	std 4,184(31)
	std 5,192(31)
	std 6,200(31)
	ld 3,192(31)
	bl _ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
	nop
	mr 30,3
	ld 3,200(31)
	bl _ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE
	nop
	mr 9,3
	mr 6,9
	mr 5,30
	ld 4,184(31)
	ld 3,176(31)
	bl _ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_
	nop
	nop
	addi 1,31,128
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 30,-16(1)
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,2,0,1
	.cfi_endproc
.LFE4749:
	.size	_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3dEEEvRS1_PT_DpOT0_,.-.L._ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3dEEEvRS1_PT_DpOT0_
	.section	.text._ZN9__gnu_cxx16__aligned_bufferI6sphereE6_M_ptrEv,"axG",@progbits,_ZN9__gnu_cxx16__aligned_bufferI6sphereE6_M_ptrEv,comdat
	.align 2
	.weak	_ZN9__gnu_cxx16__aligned_bufferI6sphereE6_M_ptrEv
	.section	".opd","aw"
	.align 3
_ZN9__gnu_cxx16__aligned_bufferI6sphereE6_M_ptrEv:
	.quad	.L._ZN9__gnu_cxx16__aligned_bufferI6sphereE6_M_ptrEv,.TOC.@tocbase,0
	.previous
	.type	_ZN9__gnu_cxx16__aligned_bufferI6sphereE6_M_ptrEv, @function
.L._ZN9__gnu_cxx16__aligned_bufferI6sphereE6_M_ptrEv:
.LFB4750:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	std 31,-8(1)
	stdu 1,-128(1)
	.cfi_def_cfa_offset 128
	.cfi_offset 65, 16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,176(31)
	ld 3,176(31)
	bl _ZN9__gnu_cxx16__aligned_bufferI6sphereE7_M_addrEv
	nop
	mr 9,3
	mr 3,9
	addi 1,31,128
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,1,0,1
	.cfi_endproc
.LFE4750:
	.size	_ZN9__gnu_cxx16__aligned_bufferI6sphereE6_M_ptrEv,.-.L._ZN9__gnu_cxx16__aligned_bufferI6sphereE6_M_ptrEv
	.section	.text._ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3iEEEvRS1_PT_DpOT0_,"axG",@progbits,_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3iEEEvRS1_PT_DpOT0_,comdat
	.align 2
	.weak	_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3iEEEvRS1_PT_DpOT0_
	.section	".opd","aw"
	.align 3
_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3iEEEvRS1_PT_DpOT0_:
	.quad	.L._ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3iEEEvRS1_PT_DpOT0_,.TOC.@tocbase,0
	.previous
	.type	_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3iEEEvRS1_PT_DpOT0_, @function
.L._ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3iEEEvRS1_PT_DpOT0_:
.LFB4751:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	std 30,-16(1)
	std 31,-8(1)
	stdu 1,-128(1)
	.cfi_def_cfa_offset 128
	.cfi_offset 65, 16
	.cfi_offset 30, -16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,176(31)
	std 4,184(31)
	std 5,192(31)
	std 6,200(31)
	ld 3,192(31)
	bl _ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
	nop
	mr 30,3
	ld 3,200(31)
	bl _ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE
	nop
	mr 9,3
	mr 6,9
	mr 5,30
	ld 4,184(31)
	ld 3,176(31)
	bl _ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_
	nop
	nop
	addi 1,31,128
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 30,-16(1)
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,2,0,1
	.cfi_endproc
.LFE4751:
	.size	_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3iEEEvRS1_PT_DpOT0_,.-.L._ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3iEEEvRS1_PT_DpOT0_
	.section	.text._ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8allocateEmPKv,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8allocateEmPKv,comdat
	.align 2
	.weak	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8allocateEmPKv
	.section	".opd","aw"
	.align 3
_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8allocateEmPKv:
	.quad	.L._ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8allocateEmPKv,.TOC.@tocbase,0
	.previous
	.type	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8allocateEmPKv, @function
.L._ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8allocateEmPKv:
.LFB4754:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	std 31,-8(1)
	stdu 1,-128(1)
	.cfi_def_cfa_offset 128
	.cfi_offset 65, 16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,176(31)
	std 4,184(31)
	std 5,192(31)
	ld 3,176(31)
	bl _ZNK9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8max_sizeEv
	nop
	mr 10,3
	ld 9,184(31)
	subfc 9,9,10
	subfe 9,9,9
	neg 9,9
	rlwinm 9,9,0,0xff
	cmpdi 0,9,0
	beq 0,.L266
	bl _ZSt17__throw_bad_allocv
	nop
.L266:
	ld 9,184(31)
	mulli 9,9,72
	mr 3,9
	bl _Znwm
	nop
	mr 9,3
	mr 3,9
	addi 1,31,128
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,1,0,1
	.cfi_endproc
.LFE4754:
	.size	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8allocateEmPKv,.-.L._ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8allocateEmPKv
	.section	.text._ZSt11__addressofISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEPT_RS7_,"axG",@progbits,_ZSt11__addressofISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEPT_RS7_,comdat
	.align 2
	.weak	_ZSt11__addressofISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEPT_RS7_
	.section	".opd","aw"
	.align 3
_ZSt11__addressofISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEPT_RS7_:
	.quad	.L._ZSt11__addressofISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEPT_RS7_,.TOC.@tocbase,0
	.previous
	.type	_ZSt11__addressofISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEPT_RS7_, @function
.L._ZSt11__addressofISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEPT_RS7_:
.LFB4755:
	.cfi_startproc
	std 31,-8(1)
	stdu 1,-64(1)
	.cfi_def_cfa_offset 64
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,112(31)
	ld 9,112(31)
	mr 3,9
	addi 1,31,64
	.cfi_def_cfa 1, 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,0,128,1,0,1
	.cfi_endproc
.LFE4755:
	.size	_ZSt11__addressofISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEPT_RS7_,.-.L._ZSt11__addressofISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEPT_RS7_
	.section	.text._ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE10deallocateEPS5_m,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE10deallocateEPS5_m,comdat
	.align 2
	.weak	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE10deallocateEPS5_m
	.section	".opd","aw"
	.align 3
_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE10deallocateEPS5_m:
	.quad	.L._ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE10deallocateEPS5_m,.TOC.@tocbase,0
	.previous
	.type	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE10deallocateEPS5_m, @function
.L._ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE10deallocateEPS5_m:
.LFB4756:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	std 31,-8(1)
	stdu 1,-128(1)
	.cfi_def_cfa_offset 128
	.cfi_offset 65, 16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,176(31)
	std 4,184(31)
	std 5,192(31)
	ld 3,184(31)
	bl _ZdlPv
	nop
	nop
	addi 1,31,128
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,1,0,1
	.cfi_endproc
.LFE4756:
	.size	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE10deallocateEPS5_m,.-.L._ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE10deallocateEPS5_m
	.section	.text._ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC2ERKS1_,"axG",@progbits,_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC5ERKS1_,comdat
	.align 2
	.weak	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC2ERKS1_
	.section	".opd","aw"
	.align 3
_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC2ERKS1_:
	.quad	.L._ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC2ERKS1_,.TOC.@tocbase,0
	.previous
	.type	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC2ERKS1_, @function
.L._ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC2ERKS1_:
.LFB4758:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	std 31,-8(1)
	stdu 1,-128(1)
	.cfi_def_cfa_offset 128
	.cfi_offset 65, 16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,176(31)
	std 4,184(31)
	ld 4,184(31)
	ld 3,176(31)
	bl _ZNSaI6sphereEC2ERKS0_
	nop
	nop
	addi 1,31,128
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,1,0,1
	.cfi_endproc
.LFE4758:
	.size	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC2ERKS1_,.-.L._ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC2ERKS1_
	.weak	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC1ERKS1_
	.set	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC1ERKS1_,_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC2ERKS1_
	.section	.text._ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_,comdat
	.align 2
	.weak	_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_
	.section	".opd","aw"
	.align 3
_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_:
	.quad	.L._ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_,.TOC.@tocbase,0
	.previous
	.type	_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_, @function
.L._ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_:
.LFB4760:
	.cfi_startproc
	.cfi_personality 0x94,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x14,.LLSDA4760
	mflr 0
	std 0,16(1)
	stfd 31,-8(1)
	std 28,-40(1)
	std 29,-32(1)
	std 30,-24(1)
	std 31,-16(1)
	stdu 1,-224(1)
	.cfi_def_cfa_offset 224
	.cfi_offset 65, 16
	.cfi_offset 63, -8
	.cfi_offset 28, -40
	.cfi_offset 29, -32
	.cfi_offset 30, -24
	.cfi_offset 31, -16
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,136(31)
	std 4,128(31)
	std 5,120(31)
	std 6,112(31)
	ld 9,-28688(13)
	std 9,168(31)
	li 9,0
	ld 3,120(31)
	bl _ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
	nop
	mr 9,3
	ld 8,0(9)
	ld 10,8(9)
	ld 9,16(9)
	std 8,144(31)
	std 10,152(31)
	std 9,160(31)
	ld 3,112(31)
	bl _ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE
	nop
	mr 9,3
	lfd 31,0(9)
	ld 30,128(31)
	mr 4,30
	li 3,56
	bl _ZnwmPv
	nop
	mr 29,3
	fmr 1,31
	ld 4,144(31)
	ld 5,152(31)
	ld 6,160(31)
	mr 3,29
.LEHB30:
	bl _ZN6sphereC1E4vec3d
	nop
.LEHE30:
	b .L276
.L275:
	mr 28,3
	mr 4,30
	mr 3,29
	bl _ZdlPvS_
	nop
	mr 9,28
	mr 3,9
.LEHB31:
	bl _Unwind_Resume
	nop
.LEHE31:
.L276:
	ld 9,168(31)
	ld 10,-28688(13)
	xor. 9,9,10
	li 10,0
	beq 0,.L274
	bl __stack_chk_fail
	nop
.L274:
	addi 1,31,224
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 28,-40(1)
	ld 29,-32(1)
	ld 30,-24(1)
	ld 31,-16(1)
	lfd 31,-8(1)
	blr
	.long 0
	.byte 0,9,2,1,129,4,0,1
	.cfi_endproc
.LFE4760:
	.section	.gcc_except_table
.LLSDA4760:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4760-.LLSDACSB4760
.LLSDACSB4760:
	.uleb128 .LEHB30-.LFB4760
	.uleb128 .LEHE30-.LEHB30
	.uleb128 .L275-.LFB4760
	.uleb128 0
	.uleb128 .LEHB31-.LFB4760
	.uleb128 .LEHE31-.LEHB31
	.uleb128 0
	.uleb128 0
.LLSDACSE4760:
	.section	.text._ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_,comdat
	.size	_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_,.-.L._ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_
	.section	.text._ZN9__gnu_cxx16__aligned_bufferI6sphereE7_M_addrEv,"axG",@progbits,_ZN9__gnu_cxx16__aligned_bufferI6sphereE7_M_addrEv,comdat
	.align 2
	.weak	_ZN9__gnu_cxx16__aligned_bufferI6sphereE7_M_addrEv
	.section	".opd","aw"
	.align 3
_ZN9__gnu_cxx16__aligned_bufferI6sphereE7_M_addrEv:
	.quad	.L._ZN9__gnu_cxx16__aligned_bufferI6sphereE7_M_addrEv,.TOC.@tocbase,0
	.previous
	.type	_ZN9__gnu_cxx16__aligned_bufferI6sphereE7_M_addrEv, @function
.L._ZN9__gnu_cxx16__aligned_bufferI6sphereE7_M_addrEv:
.LFB4761:
	.cfi_startproc
	std 31,-8(1)
	stdu 1,-64(1)
	.cfi_def_cfa_offset 64
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,112(31)
	ld 9,112(31)
	mr 3,9
	addi 1,31,64
	.cfi_def_cfa 1, 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,0,128,1,0,1
	.cfi_endproc
.LFE4761:
	.size	_ZN9__gnu_cxx16__aligned_bufferI6sphereE7_M_addrEv,.-.L._ZN9__gnu_cxx16__aligned_bufferI6sphereE7_M_addrEv
	.section	.text._ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_,comdat
	.align 2
	.weak	_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_
	.section	".opd","aw"
	.align 3
_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_:
	.quad	.L._ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_,.TOC.@tocbase,0
	.previous
	.type	_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_, @function
.L._ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_:
.LFB4762:
	.cfi_startproc
	.cfi_personality 0x94,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x14,.LLSDA4762
	mflr 0
	std 0,16(1)
	stfd 31,-8(1)
	std 28,-40(1)
	std 29,-32(1)
	std 30,-24(1)
	std 31,-16(1)
	stdu 1,-240(1)
	.cfi_def_cfa_offset 240
	.cfi_offset 65, 16
	.cfi_offset 63, -8
	.cfi_offset 28, -40
	.cfi_offset 29, -32
	.cfi_offset 30, -24
	.cfi_offset 31, -16
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,152(31)
	std 4,144(31)
	std 5,136(31)
	std 6,128(31)
	ld 9,-28688(13)
	std 9,184(31)
	li 9,0
	ld 3,136(31)
	bl _ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
	nop
	mr 9,3
	ld 8,0(9)
	ld 10,8(9)
	ld 9,16(9)
	std 8,160(31)
	std 10,168(31)
	std 9,176(31)
	ld 3,128(31)
	bl _ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE
	nop
	mr 9,3
	lwa 10,0(9)
	addi 9,31,124
	stw 10,0(9)
	lfiwax 0,0,9
	fcfid 31,0
	ld 30,144(31)
	mr 4,30
	li 3,56
	bl _ZnwmPv
	nop
	mr 29,3
	fmr 1,31
	ld 4,160(31)
	ld 5,168(31)
	ld 6,176(31)
	mr 3,29
.LEHB32:
	bl _ZN6sphereC1E4vec3d
	nop
.LEHE32:
	b .L283
.L282:
	mr 28,3
	mr 4,30
	mr 3,29
	bl _ZdlPvS_
	nop
	mr 9,28
	mr 3,9
.LEHB33:
	bl _Unwind_Resume
	nop
.LEHE33:
.L283:
	ld 9,184(31)
	ld 10,-28688(13)
	xor. 9,9,10
	li 10,0
	beq 0,.L281
	bl __stack_chk_fail
	nop
.L281:
	addi 1,31,240
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 28,-40(1)
	ld 29,-32(1)
	ld 30,-24(1)
	ld 31,-16(1)
	lfd 31,-8(1)
	blr
	.long 0
	.byte 0,9,2,1,129,4,0,1
	.cfi_endproc
.LFE4762:
	.section	.gcc_except_table
.LLSDA4762:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4762-.LLSDACSB4762
.LLSDACSB4762:
	.uleb128 .LEHB32-.LFB4762
	.uleb128 .LEHE32-.LEHB32
	.uleb128 .L282-.LFB4762
	.uleb128 0
	.uleb128 .LEHB33-.LFB4762
	.uleb128 .LEHE33-.LEHB33
	.uleb128 0
	.uleb128 0
.LLSDACSE4762:
	.section	.text._ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_,comdat
	.size	_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_,.-.L._ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_
	.section	.text._ZNK9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8max_sizeEv,"axG",@progbits,_ZNK9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8max_sizeEv,comdat
	.align 2
	.weak	_ZNK9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8max_sizeEv
	.section	".opd","aw"
	.align 3
_ZNK9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8max_sizeEv:
	.quad	.L._ZNK9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8max_sizeEv,.TOC.@tocbase,0
	.previous
	.type	_ZNK9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8max_sizeEv, @function
.L._ZNK9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8max_sizeEv:
.LFB4763:
	.cfi_startproc
	std 31,-8(1)
	stdu 1,-64(1)
	.cfi_def_cfa_offset 64
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,112(31)
	addis 9,2,.LC26@toc@ha
	ld 9,.LC26@toc@l(9)
	mr 3,9
	addi 1,31,64
	.cfi_def_cfa 1, 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,0,128,1,0,1
	.cfi_endproc
.LFE4763:
	.size	_ZNK9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8max_sizeEv,.-.L._ZNK9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8max_sizeEv
	.weak	_ZTVSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE
	.section	.rodata._ZTVSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE,"aG",@progbits,_ZTVSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align 3
	.type	_ZTVSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTVSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE, 56
_ZTVSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE:
	.quad	0
	.quad	_ZTISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE
	.quad	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.quad	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.quad	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.quad	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.quad	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.weak	_ZTVSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE
	.section	.data.rel.ro._ZTVSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE,"awG",@progbits,_ZTVSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align 3
	.type	_ZTVSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTVSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE, 56
_ZTVSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE:
	.quad	0
	.quad	_ZTISt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.quad	__cxa_pure_virtual
	.weak	_ZTISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE
	.section	.data.rel.ro._ZTISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE,"awG",@progbits,_ZTISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align 3
	.type	_ZTISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE, 24
_ZTISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE
	.quad	_ZTISt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE
	.weak	_ZTSSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE
	.section	.rodata._ZTSSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE,"aG",@progbits,_ZTSSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align 3
	.type	_ZTSSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTSSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE, 73
_ZTSSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE:
	.string	"St23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE"
	.weak	_ZTISt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE
	.section	.data.rel.ro._ZTISt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE,"awG",@progbits,_ZTISt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align 3
	.type	_ZTISt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTISt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE, 24
_ZTISt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE
	.quad	_ZTISt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE
	.weak	_ZTSSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE
	.section	.rodata._ZTSSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE,"aG",@progbits,_ZTSSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align 3
	.type	_ZTSSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTSSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE, 52
_ZTSSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE:
	.string	"St16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE"
	.section	".toc","aw"
.LC27:
	.quad	_ZNSt8ios_base4InitD1Ev
	.section	".text"
	.align 2
	.section	".opd","aw"
	.align 3
_Z41__static_initialization_and_destruction_0ii:
	.quad	.L._Z41__static_initialization_and_destruction_0ii,.TOC.@tocbase,0
	.previous
	.type	_Z41__static_initialization_and_destruction_0ii, @function
.L._Z41__static_initialization_and_destruction_0ii:
.LFB4780:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	std 31,-8(1)
	stdu 1,-128(1)
	.cfi_def_cfa_offset 128
	.cfi_offset 65, 16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	mr 9,3
	mr 10,4
	stw 9,176(31)
	mr 9,10
	stw 9,184(31)
	lwz 9,176(31)
	cmpwi 0,9,1
	bne 0,.L288
	lwz 10,184(31)
	li 9,0
	ori 9,9,0xffff
	cmpw 0,10,9
	bne 0,.L288
	addis 3,2,_ZStL8__ioinit@toc@ha
	addi 3,3,_ZStL8__ioinit@toc@l
	bl _ZNSt8ios_base4InitC1Ev
	nop
	addis 5,2,__dso_handle@toc@ha
	addi 5,5,__dso_handle@toc@l
	addis 4,2,_ZStL8__ioinit@toc@ha
	addi 4,4,_ZStL8__ioinit@toc@l
	addis 9,2,.LC27@toc@ha
	ld 3,.LC27@toc@l(9)
	bl __cxa_atexit
	nop
.L288:
	nop
	addi 1,31,128
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,1,0,1
	.cfi_endproc
.LFE4780:
	.size	_Z41__static_initialization_and_destruction_0ii,.-.L._Z41__static_initialization_and_destruction_0ii
	.section	".toc","aw"
	.set .LC28,.LC23
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	".opd","aw"
	.align 3
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED2Ev:
	.quad	.L._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED2Ev,.TOC.@tocbase,0
	.previous
	.type	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
.L._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED2Ev:
.LFB4782:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	std 31,-8(1)
	stdu 1,-128(1)
	.cfi_def_cfa_offset 128
	.cfi_offset 65, 16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,176(31)
	addis 9,2,.LC28@toc@ha
	ld 10,.LC28@toc@l(9)
	ld 9,176(31)
	std 10,0(9)
	ld 9,176(31)
	addi 9,9,16
	mr 3,9
	bl _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD1Ev
	nop
	ld 9,176(31)
	mr 3,9
	bl _ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev
	nop
	nop
	addi 1,31,128
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,1,0,1
	.cfi_endproc
.LFE4782:
	.size	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED2Ev,.-.L._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.set	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED1Ev,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED0Ev,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.section	".opd","aw"
	.align 3
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED0Ev:
	.quad	.L._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED0Ev,.TOC.@tocbase,0
	.previous
	.type	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED0Ev, @function
.L._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED0Ev:
.LFB4784:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	std 31,-8(1)
	stdu 1,-128(1)
	.cfi_def_cfa_offset 128
	.cfi_offset 65, 16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,176(31)
	ld 3,176(31)
	bl _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED1Ev
	nop
	li 4,72
	ld 3,176(31)
	bl _ZdlPvm
	nop
	addi 1,31,128
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,1,0,1
	.cfi_endproc
.LFE4784:
	.size	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED0Ev,.-.L._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,comdat
	.align 2
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.section	".opd","aw"
	.align 3
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv:
	.quad	.L._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,.TOC.@tocbase,0
	.previous
	.type	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, @function
.L._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv:
.LFB4785:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	std 30,-16(1)
	std 31,-8(1)
	stdu 1,-128(1)
	.cfi_def_cfa_offset 128
	.cfi_offset 65, 16
	.cfi_offset 30, -16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,176(31)
	ld 9,176(31)
	addi 9,9,16
	mr 3,9
	bl _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_Impl8_M_allocEv
	nop
	mr 30,3
	ld 3,176(31)
	bl _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv
	nop
	mr 9,3
	mr 4,9
	mr 3,30
	bl _ZNSt16allocator_traitsISaI6sphereEE7destroyIS0_EEvRS1_PT_
	nop
	nop
	addi 1,31,128
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 30,-16(1)
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,2,0,1
	.cfi_endproc
.LFE4785:
	.size	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,.-.L._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,comdat
	.align 2
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.section	".opd","aw"
	.align 3
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv:
	.quad	.L._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,.TOC.@tocbase,0
	.previous
	.type	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, @function
.L._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv:
.LFB4786:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	std 31,-8(1)
	stdu 1,-176(1)
	.cfi_def_cfa_offset 176
	.cfi_offset 65, 16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,120(31)
	ld 9,-28688(13)
	std 9,152(31)
	li 9,0
	ld 9,120(31)
	addi 9,9,16
	mr 3,9
	bl _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_Impl8_M_allocEv
	nop
	mr 10,3
	addi 9,31,135
	mr 4,10
	mr 3,9
	bl _ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC1IS0_EERKSaIT_E
	nop
	addi 10,31,135
	addi 9,31,136
	ld 5,120(31)
	mr 4,10
	mr 3,9
	bl _ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC1ERS6_PS5_
	nop
	ld 3,120(31)
	bl _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED1Ev
	nop
	addi 9,31,136
	mr 3,9
	bl _ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED1Ev
	nop
	addi 9,31,135
	mr 3,9
	bl _ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED1Ev
	nop
	nop
	ld 9,152(31)
	ld 10,-28688(13)
	xor. 9,9,10
	li 10,0
	beq 0,.L293
	bl __stack_chk_fail
	nop
.L293:
	addi 1,31,176
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,1,0,1
	.cfi_endproc
.LFE4786:
	.size	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,.-.L._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,comdat
	.align 2
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.section	".opd","aw"
	.align 3
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info:
	.quad	.L._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,.TOC.@tocbase,0
	.previous
	.type	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, @function
.L._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info:
.LFB4787:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	std 31,-8(1)
	stdu 1,-144(1)
	.cfi_def_cfa_offset 144
	.cfi_offset 65, 16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,192(31)
	std 4,200(31)
	ld 3,192(31)
	bl _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv
	nop
	std 3,120(31)
	bl _ZNSt19_Sp_make_shared_tag5_S_tiEv
	nop
	mr 10,3
	ld 9,200(31)
	cmpd 0,9,10
	beq 0,.L295
	addis 4,2,_ZTISt19_Sp_make_shared_tag@toc@ha
	addi 4,4,_ZTISt19_Sp_make_shared_tag@toc@l
	ld 3,200(31)
	bl _ZNKSt9type_infoeqERKS_
	nop
	mr 9,3
	cmpdi 0,9,0
	beq 0,.L296
.L295:
	li 9,1
	b .L297
.L296:
	li 9,0
.L297:
	cmpdi 0,9,0
	beq 0,.L298
	ld 9,120(31)
	b .L299
.L298:
	li 9,0
.L299:
	mr 3,9
	addi 1,31,144
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,1,0,1
	.cfi_endproc
.LFE4787:
	.size	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,.-.L._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_Impl8_M_allocEv,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_Impl8_M_allocEv,comdat
	.align 2
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_Impl8_M_allocEv
	.section	".opd","aw"
	.align 3
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_Impl8_M_allocEv:
	.quad	.L._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_Impl8_M_allocEv,.TOC.@tocbase,0
	.previous
	.type	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_Impl8_M_allocEv, @function
.L._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_Impl8_M_allocEv:
.LFB4788:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	std 31,-8(1)
	stdu 1,-128(1)
	.cfi_def_cfa_offset 128
	.cfi_offset 65, 16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,176(31)
	ld 3,176(31)
	bl _ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EE6_S_getERS2_
	nop
	mr 9,3
	mr 3,9
	addi 1,31,128
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,1,0,1
	.cfi_endproc
.LFE4788:
	.size	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_Impl8_M_allocEv,.-.L._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_Impl8_M_allocEv
	.section	.text._ZNSt16allocator_traitsISaI6sphereEE7destroyIS0_EEvRS1_PT_,"axG",@progbits,_ZNSt16allocator_traitsISaI6sphereEE7destroyIS0_EEvRS1_PT_,comdat
	.align 2
	.weak	_ZNSt16allocator_traitsISaI6sphereEE7destroyIS0_EEvRS1_PT_
	.section	".opd","aw"
	.align 3
_ZNSt16allocator_traitsISaI6sphereEE7destroyIS0_EEvRS1_PT_:
	.quad	.L._ZNSt16allocator_traitsISaI6sphereEE7destroyIS0_EEvRS1_PT_,.TOC.@tocbase,0
	.previous
	.type	_ZNSt16allocator_traitsISaI6sphereEE7destroyIS0_EEvRS1_PT_, @function
.L._ZNSt16allocator_traitsISaI6sphereEE7destroyIS0_EEvRS1_PT_:
.LFB4789:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	std 31,-8(1)
	stdu 1,-128(1)
	.cfi_def_cfa_offset 128
	.cfi_offset 65, 16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,176(31)
	std 4,184(31)
	ld 4,184(31)
	ld 3,176(31)
	bl _ZN9__gnu_cxx13new_allocatorI6sphereE7destroyIS1_EEvPT_
	nop
	nop
	addi 1,31,128
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,1,0,1
	.cfi_endproc
.LFE4789:
	.size	_ZNSt16allocator_traitsISaI6sphereEE7destroyIS0_EEvRS1_PT_,.-.L._ZNSt16allocator_traitsISaI6sphereEE7destroyIS0_EEvRS1_PT_
	.section	.text._ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EE6_S_getERS2_,"axG",@progbits,_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EE6_S_getERS2_,comdat
	.align 2
	.weak	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EE6_S_getERS2_
	.section	".opd","aw"
	.align 3
_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EE6_S_getERS2_:
	.quad	.L._ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EE6_S_getERS2_,.TOC.@tocbase,0
	.previous
	.type	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EE6_S_getERS2_, @function
.L._ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EE6_S_getERS2_:
.LFB4790:
	.cfi_startproc
	std 31,-8(1)
	stdu 1,-64(1)
	.cfi_def_cfa_offset 64
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,112(31)
	ld 9,112(31)
	mr 3,9
	addi 1,31,64
	.cfi_def_cfa 1, 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,0,128,1,0,1
	.cfi_endproc
.LFE4790:
	.size	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EE6_S_getERS2_,.-.L._ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EE6_S_getERS2_
	.section	".toc","aw"
.LC29:
	.quad	_ZTV6sphere+16
	.section	.text._ZN6sphereD2Ev,"axG",@progbits,_ZN6sphereD5Ev,comdat
	.align 2
	.weak	_ZN6sphereD2Ev
	.section	".opd","aw"
	.align 3
_ZN6sphereD2Ev:
	.quad	.L._ZN6sphereD2Ev,.TOC.@tocbase,0
	.previous
	.type	_ZN6sphereD2Ev, @function
.L._ZN6sphereD2Ev:
.LFB4793:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	std 31,-8(1)
	stdu 1,-128(1)
	.cfi_def_cfa_offset 128
	.cfi_offset 65, 16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,176(31)
	addis 9,2,.LC29@toc@ha
	ld 10,.LC29@toc@l(9)
	ld 9,176(31)
	std 10,0(9)
	ld 9,176(31)
	addi 9,9,40
	mr 3,9
	bl _ZNSt10shared_ptrI8materialED1Ev
	nop
	nop
	addi 1,31,128
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,1,0,1
	.cfi_endproc
.LFE4793:
	.size	_ZN6sphereD2Ev,.-.L._ZN6sphereD2Ev
	.weak	_ZN6sphereD1Ev
	.set	_ZN6sphereD1Ev,_ZN6sphereD2Ev
	.section	.text._ZN9__gnu_cxx13new_allocatorI6sphereE7destroyIS1_EEvPT_,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorI6sphereE7destroyIS1_EEvPT_,comdat
	.align 2
	.weak	_ZN9__gnu_cxx13new_allocatorI6sphereE7destroyIS1_EEvPT_
	.section	".opd","aw"
	.align 3
_ZN9__gnu_cxx13new_allocatorI6sphereE7destroyIS1_EEvPT_:
	.quad	.L._ZN9__gnu_cxx13new_allocatorI6sphereE7destroyIS1_EEvPT_,.TOC.@tocbase,0
	.previous
	.type	_ZN9__gnu_cxx13new_allocatorI6sphereE7destroyIS1_EEvPT_, @function
.L._ZN9__gnu_cxx13new_allocatorI6sphereE7destroyIS1_EEvPT_:
.LFB4791:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	std 31,-8(1)
	stdu 1,-128(1)
	.cfi_def_cfa_offset 128
	.cfi_offset 65, 16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	std 3,176(31)
	std 4,184(31)
	ld 3,184(31)
	bl _ZN6sphereD1Ev
	nop
	nop
	addi 1,31,128
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,1,0,1
	.cfi_endproc
.LFE4791:
	.size	_ZN9__gnu_cxx13new_allocatorI6sphereE7destroyIS1_EEvPT_,.-.L._ZN9__gnu_cxx13new_allocatorI6sphereE7destroyIS1_EEvPT_
	.weak	_ZTISt19_Sp_make_shared_tag
	.section	.data.rel.ro._ZTISt19_Sp_make_shared_tag,"awG",@progbits,_ZTISt19_Sp_make_shared_tag,comdat
	.align 3
	.type	_ZTISt19_Sp_make_shared_tag, @object
	.size	_ZTISt19_Sp_make_shared_tag, 16
_ZTISt19_Sp_make_shared_tag:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSSt19_Sp_make_shared_tag
	.weak	_ZTSSt19_Sp_make_shared_tag
	.section	.rodata._ZTSSt19_Sp_make_shared_tag,"aG",@progbits,_ZTSSt19_Sp_make_shared_tag,comdat
	.align 3
	.type	_ZTSSt19_Sp_make_shared_tag, @object
	.size	_ZTSSt19_Sp_make_shared_tag, 24
_ZTSSt19_Sp_make_shared_tag:
	.string	"St19_Sp_make_shared_tag"
	.weak	_ZTISt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE
	.section	.data.rel.ro._ZTISt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE,"awG",@progbits,_ZTISt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align 3
	.type	_ZTISt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTISt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE, 16
_ZTISt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSSt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE
	.weak	_ZTSSt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE
	.section	.rodata._ZTSSt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE,"aG",@progbits,_ZTSSt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align 3
	.type	_ZTSSt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTSSt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE, 47
_ZTSSt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE:
	.string	"St11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE"
	.section	".text"
	.align 2
	.section	".opd","aw"
	.align 3
_GLOBAL__sub_I_main:
	.quad	.L._GLOBAL__sub_I_main,.TOC.@tocbase,0
	.previous
	.type	_GLOBAL__sub_I_main, @function
.L._GLOBAL__sub_I_main:
.LFB4795:
	.cfi_startproc
	mflr 0
	std 0,16(1)
	std 31,-8(1)
	stdu 1,-128(1)
	.cfi_def_cfa_offset 128
	.cfi_offset 65, 16
	.cfi_offset 31, -8
	mr 31,1
	.cfi_def_cfa_register 31
	li 4,-1
	rldicl 4,4,0,48
	li 3,1
	bl _Z41__static_initialization_and_destruction_0ii
	addi 1,31,128
	.cfi_def_cfa 1, 0
	ld 0,16(1)
	mtlr 0
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,9,0,1,128,1,0,1
	.cfi_endproc
.LFE4795:
	.size	_GLOBAL__sub_I_main,.-.L._GLOBAL__sub_I_main
	.section	.ctors,"aw",@progbits
	.align 3
	.quad	_GLOBAL__sub_I_main
	.weakref	_ZL28__gthrw___pthread_key_createPjPFvPvE, __pthread_key_create
	.section	.rodata
	.align 3
.LC1:
	.long	1072693248
	.long	0
	.align 3
.LC2:
	.long	2146435072
	.long	0
	.align 3
.LC3:
	.long	1062232653
	.long	3539053052
	.align 3
.LC4:
	.long	1071644672
	.long	0
	.align 3
.LC5:
	.long	1072064102
	.long	1717986918
	.align 3
.LC7:
	.long	1073508807
	.long	477218588
	.align 3
.LC8:
	.long	-1074790400
	.long	0
	.align 3
.LC9:
	.long	-1067900928
	.long	0
	.align 3
.LC16:
	.long	1081667584
	.long	0
	.align 3
.LC17:
	.long	1080819712
	.long	0
	.align 4
.LC19:
	.long	1106247680
	.long	0
	.long	0
	.long	0
	.align 3
.LC20:
	.long	1072693247
	.long	4294967295
	.align 3
.LC22:
	.quad	945986875574848801
	.align 3
.LC26:
	.quad	128102389400760775
	.hidden	DW.ref.__gxx_personality_v0
	.weak	DW.ref.__gxx_personality_v0
	.section	.data.DW.ref.__gxx_personality_v0,"awG",@progbits,DW.ref.__gxx_personality_v0,comdat
	.align 3
	.type	DW.ref.__gxx_personality_v0, @object
	.size	DW.ref.__gxx_personality_v0, 8
DW.ref.__gxx_personality_v0:
	.quad	__gxx_personality_v0
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.4.0-1ubuntu1~20.04) 9.4.0"
	.gnu_attribute 4, 5
