	.set noreorder
	.set volatile
	.set noat
	.set nomacro
	.text
	.section	.sdata,"aws"
	.type	_ZStL19piecewise_construct, @object
	.size	_ZStL19piecewise_construct, 1
_ZStL19piecewise_construct:
	.zero	1
	.section	.text._ZNKSt9type_infoeqERKS_,"axG",@progbits,_ZNKSt9type_infoeqERKS_,comdat
	.align 2
	.weak	_ZNKSt9type_infoeqERKS_
	.ent _ZNKSt9type_infoeqERKS_
_ZNKSt9type_infoeqERKS_:
	.eflag 48
	.frame $15,32,$26,0
	.mask 0x4008000,-32
$LFB746:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!1
	lda $29,0($29)		!gpdisp!1
$_ZNKSt9type_infoeqERKS_..ng:
	lda $30,-32($30)
	.cfi_def_cfa_offset 32
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -32
	.cfi_offset 15, -24
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,16($15)
	stq $17,24($15)
	ldq $1,16($15)
	ldq $2,8($1)
	ldq $1,24($15)
	ldq $1,8($1)
	cmpeq $2,$1,$1
	bne $1,$L2
	ldq $1,16($15)
	ldq $1,8($1)
	lda $2,1($1)
	ldq_u $1,0($1)
	extqh $1,$2,$1
	sra $1,56,$1
	cmpeq $1,42,$1
	bne $1,$L3
	ldq $1,16($15)
	ldq $2,8($1)
	ldq $1,24($15)
	ldq $1,8($1)
	mov $1,$17
	mov $2,$16
	ldq $27,strcmp($29)		!literal!2
	jsr $26,($27),0		!lituse_jsr!2
	ldah $29,0($26)		!gpdisp!3
	lda $29,0($29)		!gpdisp!3
	mov $0,$1
	bne $1,$L3
$L2:
	lda $1,1($31)
	br $31,$L4
$L3:
	mov $31,$1
$L4:
	mov $1,$0
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,32($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE746:
	.end _ZNKSt9type_infoeqERKS_
	.section	.text._ZnwmPv,"axG",@progbits,_ZnwmPv,comdat
	.align 2
	.weak	_ZnwmPv
	.ent _ZnwmPv
$_ZnwmPv..ng:
_ZnwmPv:
	.eflag 48
	.frame $15,32,$26,0
	.mask 0x4008000,-32
$LFB788:
	.cfi_startproc
	lda $30,-32($30)
	.cfi_def_cfa_offset 32
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -32
	.cfi_offset 15, -24
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 0
	stq $16,16($15)
	stq $17,24($15)
	ldq $1,24($15)
	mov $1,$0
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,32($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE788:
	.end _ZnwmPv
	.section	.text._ZdlPvS_,"axG",@progbits,_ZdlPvS_,comdat
	.align 2
	.weak	_ZdlPvS_
	.ent _ZdlPvS_
$_ZdlPvS_..ng:
_ZdlPvS_:
	.eflag 48
	.frame $15,32,$26,0
	.mask 0x4008000,-32
$LFB790:
	.cfi_startproc
	lda $30,-32($30)
	.cfi_def_cfa_offset 32
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -32
	.cfi_offset 15, -24
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 0
	stq $16,16($15)
	stq $17,24($15)
	bis $31,$31,$31
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,32($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE790:
	.end _ZdlPvS_
	.section	.sdata
	.align 3
	.type	_ZZL18__gthread_active_pvE20__gthread_active_ptr, @object
	.size	_ZZL18__gthread_active_pvE20__gthread_active_ptr, 8
_ZZL18__gthread_active_pvE20__gthread_active_ptr:
	.quad	_ZL28__gthrw___pthread_key_createPjPFvPvE
	.section	.text._ZL18__gthread_active_pv,"axG",@progbits,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv,comdat
	.align 2
	.ent _ZL18__gthread_active_pv
_ZL18__gthread_active_pv:
	.eflag 48
	.frame $15,16,$26,0
	.mask 0x4008000,-16
$LFB964:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!4
	lda $29,0($29)		!gpdisp!4
$_ZL18__gthread_active_pv..ng:
	lda $30,-16($30)
	.cfi_def_cfa_offset 16
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -16
	.cfi_offset 15, -8
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	lda $1,1($31)
	ldq $2,_ZL28__gthrw___pthread_key_createPjPFvPvE($29)		!literal
	bne $2,$L10
	bis $31,$31,$1
$L10:
	and $1,0xff,$1
	addl $31,$1,$1
	mov $1,$0
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,16($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE964:
	.end _ZL18__gthread_active_pv
	.section	.text._ZN9__gnu_cxxL18__exchange_and_addEPVii,"axG",@progbits,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv,comdat
	.align 2
	.ent _ZN9__gnu_cxxL18__exchange_and_addEPVii
$_ZN9__gnu_cxxL18__exchange_and_addEPVii..ng:
_ZN9__gnu_cxxL18__exchange_and_addEPVii:
	.eflag 48
	.frame $15,32,$26,0
	.mask 0x4008000,-32
$LFB993:
	.cfi_startproc
	lda $30,-32($30)
	.cfi_def_cfa_offset 32
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -32
	.cfi_offset 15, -24
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 0
	stq $16,16($15)
	mov $17,$1
	stl $1,24($15)
	ldl $2,24($15)
	ldq $1,16($15)
	mb
$L14:
	ldl_l $3,0($1)
	addl $3,$2,$4
	stl_c $4,0($1)
	beq $4,$L14
	mb
	addl $31,$3,$1
	addl $31,$1,$1
	mov $1,$0
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,32($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE993:
	.end _ZN9__gnu_cxxL18__exchange_and_addEPVii
	.section	.text._ZN9__gnu_cxxL25__exchange_and_add_singleEPii,"axG",@progbits,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv,comdat
	.align 2
	.ent _ZN9__gnu_cxxL25__exchange_and_add_singleEPii
$_ZN9__gnu_cxxL25__exchange_and_add_singleEPii..ng:
_ZN9__gnu_cxxL25__exchange_and_add_singleEPii:
	.eflag 48
	.frame $15,48,$26,0
	.mask 0x4008000,-48
$LFB995:
	.cfi_startproc
	lda $30,-48($30)
	.cfi_def_cfa_offset 48
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -48
	.cfi_offset 15, -40
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 0
	stq $16,32($15)
	mov $17,$1
	stl $1,40($15)
	ldq $1,32($15)
	ldl $1,0($1)
	stl $1,16($15)
	ldq $1,32($15)
	ldl $1,0($1)
	ldl $2,40($15)
	addl $2,$1,$1
	addl $31,$1,$2
	ldq $1,32($15)
	stl $2,0($1)
	ldl $1,16($15)
	mov $1,$0
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,48($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE995:
	.end _ZN9__gnu_cxxL25__exchange_and_add_singleEPii
	.section	.text._ZN9__gnu_cxxL27__exchange_and_add_dispatchEPii,"axG",@progbits,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv,comdat
	.align 2
	.ent _ZN9__gnu_cxxL27__exchange_and_add_dispatchEPii
_ZN9__gnu_cxxL27__exchange_and_add_dispatchEPii:
	.eflag 48
	.frame $15,32,$26,0
	.mask 0x4008000,-32
$LFB997:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!5
	lda $29,0($29)		!gpdisp!5
$_ZN9__gnu_cxxL27__exchange_and_add_dispatchEPii..ng:
	lda $30,-32($30)
	.cfi_def_cfa_offset 32
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -32
	.cfi_offset 15, -24
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,16($15)
	mov $17,$1
	stl $1,24($15)
	ldq $27,_ZL18__gthread_active_pv($29)		!literal!6
	jsr $26,($27),0		!lituse_jsr!6
	ldah $29,0($26)		!gpdisp!7
	lda $29,0($29)		!gpdisp!7
	mov $0,$1
	cmpult $31,$1,$1
	and $1,0xff,$1
	beq $1,$L18
	ldl $1,24($15)
	mov $1,$17
	ldq $16,16($15)
	ldq $27,_ZN9__gnu_cxxL18__exchange_and_addEPVii($29)		!literal!8
	jsr $26,($27),0		!lituse_jsr!8
	ldah $29,0($26)		!gpdisp!9
	lda $29,0($29)		!gpdisp!9
	mov $0,$1
	br $31,$L19
$L18:
	ldl $1,24($15)
	mov $1,$17
	ldq $16,16($15)
	ldq $27,_ZN9__gnu_cxxL25__exchange_and_add_singleEPii($29)		!literal!10
	jsr $26,($27),0		!lituse_jsr!10
	ldah $29,0($26)		!gpdisp!11
	lda $29,0($29)		!gpdisp!11
	mov $0,$1
	bis $31,$31,$31
$L19:
	mov $1,$0
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,32($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE997:
	.end _ZN9__gnu_cxxL27__exchange_and_add_dispatchEPii
	.section	.sdata
	.align 2
	.type	_ZN9__gnu_cxxL21__default_lock_policyE, @object
	.size	_ZN9__gnu_cxxL21__default_lock_policyE, 4
_ZN9__gnu_cxxL21__default_lock_policyE:
	.long	2
	.type	_ZStL13allocator_arg, @object
	.size	_ZStL13allocator_arg, 1
_ZStL13allocator_arg:
	.zero	1
	.type	_ZStL6ignore, @object
	.size	_ZStL6ignore, 1
_ZStL6ignore:
	.zero	1
	.weak	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag
	.section	.rodata._ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag,"aG",@progbits,_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag,comdat
	.align 3
	.type	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag, @gnu_unique_object
	.size	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag, 16
_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag:
	.zero	16
	.section	.text._ZNSt19_Sp_make_shared_tag5_S_tiEv,"axG",@progbits,_ZNSt19_Sp_make_shared_tag5_S_tiEv,comdat
	.align 2
	.weak	_ZNSt19_Sp_make_shared_tag5_S_tiEv
	.ent _ZNSt19_Sp_make_shared_tag5_S_tiEv
_ZNSt19_Sp_make_shared_tag5_S_tiEv:
	.eflag 48
	.frame $15,16,$26,0
	.mask 0x4008000,-16
$LFB1963:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!12
	lda $29,0($29)		!gpdisp!12
$_ZNSt19_Sp_make_shared_tag5_S_tiEv..ng:
	lda $30,-16($30)
	.cfi_def_cfa_offset 16
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -16
	.cfi_offset 15, -8
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	ldq $1,_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag($29)		!literal
	mov $1,$0
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,16($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE1963:
	.end _ZNSt19_Sp_make_shared_tag5_S_tiEv
	.section	.sdata
	.align 3
	.type	_ZL2pi, @object
	.size	_ZL2pi, 8
_ZL2pi:
	.long	1413754136
	.long	1074340347
	.weak	_ZZ13random_doublevE12distribution
	.section	.bss._ZZ13random_doublevE12distribution,"awG",@nobits,_ZZ13random_doublevE12distribution,comdat
	.align 3
	.type	_ZZ13random_doublevE12distribution, @gnu_unique_object
	.size	_ZZ13random_doublevE12distribution, 16
_ZZ13random_doublevE12distribution:
	.zero	16
	.weak	_ZGVZ13random_doublevE12distribution
	.section	.sbss._ZGVZ13random_doublevE12distribution,"awsG",@nobits,_ZGVZ13random_doublevE12distribution,comdat
	.align 3
	.type	_ZGVZ13random_doublevE12distribution, @gnu_unique_object
	.size	_ZGVZ13random_doublevE12distribution, 8
_ZGVZ13random_doublevE12distribution:
	.zero	8
	.weak	_ZZ13random_doublevE9generator
	.section	.bss._ZZ13random_doublevE9generator,"awG",@nobits,_ZZ13random_doublevE9generator,comdat
	.align 3
	.type	_ZZ13random_doublevE9generator, @gnu_unique_object
	.size	_ZZ13random_doublevE9generator, 5000
_ZZ13random_doublevE9generator:
	.zero	5000
	.weak	_ZGVZ13random_doublevE9generator
	.section	.sbss._ZGVZ13random_doublevE9generator,"awsG",@nobits,_ZGVZ13random_doublevE9generator,comdat
	.align 3
	.type	_ZGVZ13random_doublevE9generator, @gnu_unique_object
	.size	_ZGVZ13random_doublevE9generator, 8
_ZGVZ13random_doublevE9generator:
	.zero	8
	.section	.text._Z13random_doublev,"axG",@progbits,_Z13random_doublev,comdat
	.align 2
	.weak	_Z13random_doublev
	.ent _Z13random_doublev
_Z13random_doublev:
	.eflag 48
	.frame $15,32,$26,0
	.mask 0x4008600,-32
$LFB3309:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,$LLSDA3309
	ldah $29,0($27)		!gpdisp!13
	lda $29,0($29)		!gpdisp!13
$_Z13random_doublev..ng:
	lda $30,-32($30)
	.cfi_def_cfa_offset 32
	stq $26,0($30)
	stq $9,8($30)
	stq $10,16($30)
	stq $15,24($30)
	.cfi_offset 26, -32
	.cfi_offset 9, -24
	.cfi_offset 10, -16
	.cfi_offset 15, -8
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	ldq $1,_ZGVZ13random_doublevE12distribution($29)		!literal
	ldl $1,0($1)
	extbl $1,0,$1
	mb
	and $1,0xff,$1
	cmpeq $1,0,$1
	and $1,0xff,$1
	beq $1,$L23
	ldq $16,_ZGVZ13random_doublevE12distribution($29)		!literal
	ldq $27,__cxa_guard_acquire($29)		!literal!16
	jsr $26,($27),0		!lituse_jsr!16
	ldah $29,0($26)		!gpdisp!17
	lda $29,0($29)		!gpdisp!17
	mov $0,$1
	cmpult $31,$1,$1
	and $1,0xff,$1
	beq $1,$L23
	mov $31,$9
	ldah $1,$LC0($29)		!gprelhigh
	ldt $f10,$LC0($1)		!gprellow
	cpys $f10,$f10,$f18
	cpys $f31,$f31,$f17
	ldq $16,_ZZ13random_doublevE12distribution($29)		!literal
$LEHB0:
	ldq $27,_ZNSt25uniform_real_distributionIdEC1Edd($29)		!literal!18
	jsr $26,($27),_ZNSt25uniform_real_distributionIdEC1Edd		!lituse_jsr!18
	ldah $29,0($26)		!gpdisp!19
	lda $29,0($29)		!gpdisp!19
$LEHE0:
	ldq $16,_ZGVZ13random_doublevE12distribution($29)		!literal
	ldq $27,__cxa_guard_release($29)		!literal!20
	jsr $26,($27),__cxa_guard_release		!lituse_jsr!20
	ldah $29,0($26)		!gpdisp!21
	lda $29,0($29)		!gpdisp!21
$L23:
	ldq $1,_ZGVZ13random_doublevE9generator($29)		!literal
	ldl $1,0($1)
	extbl $1,0,$1
	mb
	and $1,0xff,$1
	cmpeq $1,0,$1
	and $1,0xff,$1
	beq $1,$L24
	ldq $16,_ZGVZ13random_doublevE9generator($29)		!literal
	ldq $27,__cxa_guard_acquire($29)		!literal!22
	jsr $26,($27),0		!lituse_jsr!22
	ldah $29,0($26)		!gpdisp!23
	lda $29,0($29)		!gpdisp!23
	mov $0,$1
	cmpult $31,$1,$1
	and $1,0xff,$1
	beq $1,$L24
	mov $31,$9
	ldq $16,_ZZ13random_doublevE9generator($29)		!literal
$LEHB1:
	ldq $27,_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC1Ev($29)		!literal!24
	jsr $26,($27),_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC1Ev		!lituse_jsr!24
	ldah $29,0($26)		!gpdisp!25
	lda $29,0($29)		!gpdisp!25
$LEHE1:
	ldq $16,_ZGVZ13random_doublevE9generator($29)		!literal
	ldq $27,__cxa_guard_release($29)		!literal!26
	jsr $26,($27),__cxa_guard_release		!lituse_jsr!26
	ldah $29,0($26)		!gpdisp!27
	lda $29,0($29)		!gpdisp!27
$L24:
	ldq $17,_ZZ13random_doublevE9generator($29)		!literal
	ldq $16,_ZZ13random_doublevE12distribution($29)		!literal
$LEHB2:
	ldq $27,_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEEEdRT_($29)		!literal!28
	jsr $26,($27),0		!lituse_jsr!28
	ldah $29,0($26)		!gpdisp!29
	lda $29,0($29)		!gpdisp!29
	cpys $f0,$f0,$f10
	br $31,$L32
$L30:
	ldah $29,0($26)		!gpdisp!14
	lda $29,0($29)		!gpdisp!14
	mov $16,$10
	bne $9,$L27
	ldq $16,_ZGVZ13random_doublevE12distribution($29)		!literal
	ldq $27,__cxa_guard_abort($29)		!literal!30
	jsr $26,($27),__cxa_guard_abort		!lituse_jsr!30
	ldah $29,0($26)		!gpdisp!31
	lda $29,0($29)		!gpdisp!31
$L27:
	mov $10,$1
	mov $1,$16
	ldq $27,_Unwind_Resume($29)		!literal!32
	jsr $26,($27),_Unwind_Resume		!lituse_jsr!32
$L31:
	ldah $29,0($26)		!gpdisp!15
	lda $29,0($29)		!gpdisp!15
	mov $16,$10
	bne $9,$L29
	ldq $16,_ZGVZ13random_doublevE9generator($29)		!literal
	ldq $27,__cxa_guard_abort($29)		!literal!33
	jsr $26,($27),__cxa_guard_abort		!lituse_jsr!33
	ldah $29,0($26)		!gpdisp!34
	lda $29,0($29)		!gpdisp!34
$L29:
	mov $10,$1
	mov $1,$16
	ldq $27,_Unwind_Resume($29)		!literal!35
	jsr $26,($27),_Unwind_Resume		!lituse_jsr!35
$LEHE2:
$L32:
	cpys $f10,$f10,$f0
	mov $15,$30
	ldq $26,0($30)
	ldq $9,8($30)
	ldq $10,16($30)
	ldq $15,24($30)
	lda $30,32($30)
	.cfi_restore 15
	.cfi_restore 10
	.cfi_restore 9
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE3309:
	.globl __gxx_personality_v0
	.section	.gcc_except_table,"a",@progbits
$LLSDA3309:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 $LLSDACSE3309-$LLSDACSB3309
$LLSDACSB3309:
	.uleb128 $LEHB0-$LFB3309
	.uleb128 $LEHE0-$LEHB0
	.uleb128 $L30-$LFB3309
	.uleb128 0
	.uleb128 $LEHB1-$LFB3309
	.uleb128 $LEHE1-$LEHB1
	.uleb128 $L31-$LFB3309
	.uleb128 0
	.uleb128 $LEHB2-$LFB3309
	.uleb128 $LEHE2-$LEHB2
	.uleb128 0
	.uleb128 0
$LLSDACSE3309:
	.section	.text._Z13random_doublev,"axG",@progbits,_Z13random_doublev,comdat
	.end _Z13random_doublev
	.section	.text._ZplRK4vec3S1_,"axG",@progbits,_ZplRK4vec3S1_,comdat
	.align 2
	.weak	_ZplRK4vec3S1_
	.ent _ZplRK4vec3S1_
_ZplRK4vec3S1_:
	.eflag 48
	.frame $15,48,$26,0
	.mask 0x4008000,-48
$LFB3947:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!36
	lda $29,0($29)		!gpdisp!36
$_ZplRK4vec3S1_..ng:
	lda $30,-48($30)
	.cfi_def_cfa_offset 48
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -48
	.cfi_offset 15, -40
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,16($15)
	stq $17,24($15)
	stq $18,32($15)
	ldq $1,24($15)
	ldt $f11,0($1)
	ldq $1,32($15)
	ldt $f10,0($1)
	addt/su $f11,$f10,$f12
	trapb
	ldq $1,24($15)
	ldt $f11,8($1)
	ldq $1,32($15)
	ldt $f10,8($1)
	addt/su $f11,$f10,$f13
	trapb
	ldq $1,24($15)
	ldt $f11,16($1)
	ldq $1,32($15)
	ldt $f10,16($1)
	addt/su $f11,$f10,$f14
	trapb
	cpys $f14,$f14,$f19
	cpys $f13,$f13,$f18
	cpys $f12,$f12,$f17
	ldq $16,16($15)
	ldq $27,_ZN4vec3C1Eddd($29)		!literal!37
	jsr $26,($27),_ZN4vec3C1Eddd		!lituse_jsr!37
	ldah $29,0($26)		!gpdisp!38
	lda $29,0($29)		!gpdisp!38
	ldq $0,16($15)
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,48($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE3947:
	.end _ZplRK4vec3S1_
	.section	.text._ZmiRK4vec3S1_,"axG",@progbits,_ZmiRK4vec3S1_,comdat
	.align 2
	.weak	_ZmiRK4vec3S1_
	.ent _ZmiRK4vec3S1_
_ZmiRK4vec3S1_:
	.eflag 48
	.frame $15,48,$26,0
	.mask 0x4008000,-48
$LFB3948:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!39
	lda $29,0($29)		!gpdisp!39
$_ZmiRK4vec3S1_..ng:
	lda $30,-48($30)
	.cfi_def_cfa_offset 48
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -48
	.cfi_offset 15, -40
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,16($15)
	stq $17,24($15)
	stq $18,32($15)
	ldq $1,24($15)
	ldt $f11,0($1)
	ldq $1,32($15)
	ldt $f10,0($1)
	subt/su $f11,$f10,$f12
	trapb
	ldq $1,24($15)
	ldt $f11,8($1)
	ldq $1,32($15)
	ldt $f10,8($1)
	subt/su $f11,$f10,$f13
	trapb
	ldq $1,24($15)
	ldt $f11,16($1)
	ldq $1,32($15)
	ldt $f10,16($1)
	subt/su $f11,$f10,$f14
	trapb
	cpys $f14,$f14,$f19
	cpys $f13,$f13,$f18
	cpys $f12,$f12,$f17
	ldq $16,16($15)
	ldq $27,_ZN4vec3C1Eddd($29)		!literal!40
	jsr $26,($27),_ZN4vec3C1Eddd		!lituse_jsr!40
	ldah $29,0($26)		!gpdisp!41
	lda $29,0($29)		!gpdisp!41
	ldq $0,16($15)
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,48($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE3948:
	.end _ZmiRK4vec3S1_
	.section	.text._ZmldRK4vec3,"axG",@progbits,_ZmldRK4vec3,comdat
	.align 2
	.weak	_ZmldRK4vec3
	.ent _ZmldRK4vec3
_ZmldRK4vec3:
	.eflag 48
	.frame $15,48,$26,0
	.mask 0x4008000,-48
$LFB3950:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!42
	lda $29,0($29)		!gpdisp!42
$_ZmldRK4vec3..ng:
	lda $30,-48($30)
	.cfi_def_cfa_offset 48
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -48
	.cfi_offset 15, -40
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,16($15)
	stt $f17,24($15)
	stq $18,32($15)
	ldq $1,32($15)
	ldt $f11,0($1)
	ldt $f10,24($15)
	mult/su $f11,$f10,$f12
	trapb
	ldq $1,32($15)
	ldt $f11,8($1)
	ldt $f10,24($15)
	mult/su $f11,$f10,$f13
	trapb
	ldq $1,32($15)
	ldt $f11,16($1)
	ldt $f10,24($15)
	mult/su $f11,$f10,$f14
	trapb
	cpys $f14,$f14,$f19
	cpys $f13,$f13,$f18
	cpys $f12,$f12,$f17
	ldq $16,16($15)
	ldq $27,_ZN4vec3C1Eddd($29)		!literal!43
	jsr $26,($27),_ZN4vec3C1Eddd		!lituse_jsr!43
	ldah $29,0($26)		!gpdisp!44
	lda $29,0($29)		!gpdisp!44
	ldq $0,16($15)
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,48($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE3950:
	.end _ZmldRK4vec3
	.section	.text._Zdv4vec3d,"axG",@progbits,_Zdv4vec3d,comdat
	.align 2
	.weak	_Zdv4vec3d
	.ent _Zdv4vec3d
_Zdv4vec3d:
	.eflag 48
	.frame $15,64,$26,0
	.mask 0x4008000,-64
$LFB3952:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!45
	lda $29,0($29)		!gpdisp!45
$_Zdv4vec3d..ng:
	lda $30,-64($30)
	.cfi_def_cfa_offset 64
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -64
	.cfi_offset 15, -56
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,16($15)
	stq $17,24($15)
	stq $18,32($15)
	stq $19,40($15)
	stt $f20,48($15)
	ldah $1,$LC0($29)		!gprelhigh
	ldt $f11,$LC0($1)		!gprellow
	ldt $f10,48($15)
	divt/su $f11,$f10,$f12
	trapb
	ldq $1,16($15)
	lda $2,24($15)
	mov $2,$18
	cpys $f12,$f12,$f17
	mov $1,$16
	ldq $27,_ZmldRK4vec3($29)		!literal!46
	jsr $26,($27),_ZmldRK4vec3		!lituse_jsr!46
	ldah $29,0($26)		!gpdisp!47
	lda $29,0($29)		!gpdisp!47
	ldq $0,16($15)
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,64($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE3952:
	.end _Zdv4vec3d
	.section	.text._Z11unit_vector4vec3,"axG",@progbits,_Z11unit_vector4vec3,comdat
	.align 2
	.weak	_Z11unit_vector4vec3
	.ent _Z11unit_vector4vec3
_Z11unit_vector4vec3:
	.eflag 48
	.frame $15,48,$26,0
	.mask 0x4008000,-48
$LFB3955:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!48
	lda $29,0($29)		!gpdisp!48
$_Z11unit_vector4vec3..ng:
	lda $30,-48($30)
	.cfi_def_cfa_offset 48
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -48
	.cfi_offset 15, -40
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,16($15)
	stq $17,24($15)
	stq $18,32($15)
	stq $19,40($15)
	lda $1,24($15)
	mov $1,$16
	ldq $27,_ZNK4vec36lengthEv($29)		!literal!49
	jsr $26,($27),0		!lituse_jsr!49
	ldah $29,0($26)		!gpdisp!50
	lda $29,0($29)		!gpdisp!50
	cpys $f0,$f0,$f10
	ldq $1,16($15)
	cpys $f10,$f10,$f20
	ldq $17,24($15)
	ldq $18,32($15)
	ldq $19,40($15)
	mov $1,$16
	ldq $27,_Zdv4vec3d($29)		!literal!51
	jsr $26,($27),_Zdv4vec3d		!lituse_jsr!51
	ldah $29,0($26)		!gpdisp!52
	lda $29,0($29)		!gpdisp!52
	ldq $0,16($15)
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,48($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE3955:
	.end _Z11unit_vector4vec3
	.section	.sbss,"aw"
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.text._ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.weak	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED2Ev
	.ent _ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED2Ev
_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED2Ev:
	.eflag 48
	.frame $15,32,$26,0
	.mask 0x4008000,-32
$LFB3961:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!53
	lda $29,0($29)		!gpdisp!53
$_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED2Ev..ng:
	lda $30,-32($30)
	.cfi_def_cfa_offset 32
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -32
	.cfi_offset 15, -24
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,16($15)
	ldq $1,16($15)
	lda $1,8($1)
	mov $1,$16
	ldq $27,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED1Ev($29)		!literal!54
	jsr $26,($27),_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED1Ev		!lituse_jsr!54
	ldah $29,0($26)		!gpdisp!55
	lda $29,0($29)		!gpdisp!55
	bis $31,$31,$31
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,32($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE3961:
	.end _ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED1Ev
$_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED1Ev..ng = $_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED2Ev..ng
_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED1Ev = _ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt10shared_ptrI8materialED2Ev,"axG",@progbits,_ZNSt10shared_ptrI8materialED5Ev,comdat
	.align 2
	.weak	_ZNSt10shared_ptrI8materialED2Ev
	.ent _ZNSt10shared_ptrI8materialED2Ev
_ZNSt10shared_ptrI8materialED2Ev:
	.eflag 48
	.frame $15,32,$26,0
	.mask 0x4008000,-32
$LFB3963:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!56
	lda $29,0($29)		!gpdisp!56
$_ZNSt10shared_ptrI8materialED2Ev..ng:
	lda $30,-32($30)
	.cfi_def_cfa_offset 32
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -32
	.cfi_offset 15, -24
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,16($15)
	ldq $1,16($15)
	mov $1,$16
	ldq $27,_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED2Ev($29)		!literal!57
	jsr $26,($27),_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED2Ev		!lituse_jsr!57
	ldah $29,0($26)		!gpdisp!58
	lda $29,0($29)		!gpdisp!58
	bis $31,$31,$31
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,32($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE3963:
	.end _ZNSt10shared_ptrI8materialED2Ev
	.weak	_ZNSt10shared_ptrI8materialED1Ev
$_ZNSt10shared_ptrI8materialED1Ev..ng = $_ZNSt10shared_ptrI8materialED2Ev..ng
_ZNSt10shared_ptrI8materialED1Ev = _ZNSt10shared_ptrI8materialED2Ev
	.section	.text._ZN10hit_recordC2Ev,"axG",@progbits,_ZN10hit_recordC5Ev,comdat
	.align 2
	.weak	_ZN10hit_recordC2Ev
	.ent _ZN10hit_recordC2Ev
_ZN10hit_recordC2Ev:
	.eflag 48
	.frame $15,32,$26,0
	.mask 0x4008000,-32
$LFB3965:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!59
	lda $29,0($29)		!gpdisp!59
$_ZN10hit_recordC2Ev..ng:
	lda $30,-32($30)
	.cfi_def_cfa_offset 32
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -32
	.cfi_offset 15, -24
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,16($15)
	ldq $1,16($15)
	mov $1,$16
	ldq $27,_ZN4vec3C1Ev($29)		!literal!60
	jsr $26,($27),_ZN4vec3C1Ev		!lituse_jsr!60
	ldah $29,0($26)		!gpdisp!61
	lda $29,0($29)		!gpdisp!61
	ldq $1,16($15)
	lda $1,24($1)
	mov $1,$16
	ldq $27,_ZN4vec3C1Ev($29)		!literal!62
	jsr $26,($27),_ZN4vec3C1Ev		!lituse_jsr!62
	ldah $29,0($26)		!gpdisp!63
	lda $29,0($29)		!gpdisp!63
	ldq $1,16($15)
	lda $1,48($1)
	mov $1,$16
	ldq $27,_ZNSt10shared_ptrI8materialEC1Ev($29)		!literal!64
	jsr $26,($27),_ZNSt10shared_ptrI8materialEC1Ev		!lituse_jsr!64
	ldah $29,0($26)		!gpdisp!65
	lda $29,0($29)		!gpdisp!65
	bis $31,$31,$31
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,32($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE3965:
	.end _ZN10hit_recordC2Ev
	.weak	_ZN10hit_recordC1Ev
$_ZN10hit_recordC1Ev..ng = $_ZN10hit_recordC2Ev..ng
_ZN10hit_recordC1Ev = _ZN10hit_recordC2Ev
	.section	.text._ZN10hit_recordD2Ev,"axG",@progbits,_ZN10hit_recordD5Ev,comdat
	.align 2
	.weak	_ZN10hit_recordD2Ev
	.ent _ZN10hit_recordD2Ev
_ZN10hit_recordD2Ev:
	.eflag 48
	.frame $15,32,$26,0
	.mask 0x4008000,-32
$LFB3968:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!66
	lda $29,0($29)		!gpdisp!66
$_ZN10hit_recordD2Ev..ng:
	lda $30,-32($30)
	.cfi_def_cfa_offset 32
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -32
	.cfi_offset 15, -24
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,16($15)
	ldq $1,16($15)
	lda $1,48($1)
	mov $1,$16
	ldq $27,_ZNSt10shared_ptrI8materialED1Ev($29)		!literal!67
	jsr $26,($27),_ZNSt10shared_ptrI8materialED1Ev		!lituse_jsr!67
	ldah $29,0($26)		!gpdisp!68
	lda $29,0($29)		!gpdisp!68
	bis $31,$31,$31
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,32($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE3968:
	.end _ZN10hit_recordD2Ev
	.weak	_ZN10hit_recordD1Ev
$_ZN10hit_recordD1Ev..ng = $_ZN10hit_recordD2Ev..ng
_ZN10hit_recordD1Ev = _ZN10hit_recordD2Ev
	.text
	.align 2
	.ent _ZL9ray_colorRK3rayRK8hittablei
_ZL9ray_colorRK3rayRK8hittablei:
	.eflag 48
	.frame $15,480,$26,0
	.mask 0x4008200,-480
	.fmask 0x4,-456
$LFB3957:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,$LLSDA3957
	ldah $29,0($27)		!gpdisp!69
	lda $29,0($29)		!gpdisp!69
$_ZL9ray_colorRK3rayRK8hittablei..ng:
	lda $30,-480($30)
	.cfi_def_cfa_offset 480
	stq $26,0($30)
	stq $9,8($30)
	stq $15,16($30)
	stt $f2,24($30)
	.cfi_offset 26, -480
	.cfi_offset 9, -472
	.cfi_offset 15, -464
	.cfi_offset 34, -456
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,448($15)
	stq $17,456($15)
	stq $18,464($15)
	mov $19,$1
	stl $1,472($15)
	lda $1,312($15)
	mov $1,$16
$LEHB3:
	ldq $27,_ZN10hit_recordC1Ev($29)		!literal!71
	jsr $26,($27),_ZN10hit_recordC1Ev		!lituse_jsr!71
	ldah $29,0($26)		!gpdisp!72
	lda $29,0($29)		!gpdisp!72
$LEHE3:
	ldl $1,472($15)
	bgt $1,$L48
	cpys $f31,$f31,$f19
	cpys $f31,$f31,$f18
	cpys $f31,$f31,$f17
	ldq $16,448($15)
$LEHB4:
	ldq $27,_ZN4vec3C1Eddd($29)		!literal!73
	jsr $26,($27),_ZN4vec3C1Eddd		!lituse_jsr!73
	ldah $29,0($26)		!gpdisp!74
	lda $29,0($29)		!gpdisp!74
	br $31,$L49
$L48:
	ldah $1,$LC1($29)		!gprelhigh
	ldt $f10,$LC1($1)		!gprellow
	stt $f10,32($15)
	ldq $1,464($15)
	ldq $1,0($1)
	ldq $1,0($1)
	lda $3,312($15)
	ldah $2,$LC2($29)		!gprelhigh
	ldt $f10,$LC2($2)		!gprellow
	mov $3,$20
	ldt $f19,32($15)
	cpys $f10,$f10,$f18
	ldq $17,456($15)
	ldq $16,464($15)
	mov $1,$27
	jsr $26,($27),0
	ldah $29,0($26)		!gpdisp!75
	lda $29,0($29)		!gpdisp!75
	mov $0,$1
	beq $1,$L50
	lda $2,288($15)
	lda $1,312($15)
	lda $3,24($1)
	lda $1,312($15)
	mov $3,$18
	mov $1,$17
	mov $2,$16
	ldq $27,_ZplRK4vec3S1_($29)		!literal!76
	jsr $26,($27),_ZplRK4vec3S1_		!lituse_jsr!76
	ldah $29,0($26)		!gpdisp!77
	lda $29,0($29)		!gpdisp!77
	lda $1,264($15)
	mov $1,$16
	ldq $27,_Z18random_unit_vectorv($29)		!literal!78
	jsr $26,($27),_Z18random_unit_vectorv		!lituse_jsr!78
	ldah $29,0($26)		!gpdisp!79
	lda $29,0($29)		!gpdisp!79
	lda $1,416($15)
	lda $3,264($15)
	lda $2,288($15)
	mov $3,$18
	mov $2,$17
	mov $1,$16
	ldq $27,_ZplRK4vec3S1_($29)		!literal!80
	jsr $26,($27),_ZplRK4vec3S1_		!lituse_jsr!80
	ldah $29,0($26)		!gpdisp!81
	lda $29,0($29)		!gpdisp!81
	lda $1,168($15)
	lda $3,312($15)
	lda $2,416($15)
	mov $3,$18
	mov $2,$17
	mov $1,$16
	ldq $27,_ZmiRK4vec3S1_($29)		!literal!82
	jsr $26,($27),_ZmiRK4vec3S1_		!lituse_jsr!82
	ldah $29,0($26)		!gpdisp!83
	lda $29,0($29)		!gpdisp!83
	lda $3,168($15)
	lda $2,312($15)
	lda $1,192($15)
	mov $3,$18
	mov $2,$17
	mov $1,$16
	ldq $27,_ZN3rayC1ERK4vec3S2_($29)		!literal!84
	jsr $26,($27),_ZN3rayC1ERK4vec3S2_		!lituse_jsr!84
	ldah $29,0($26)		!gpdisp!85
	lda $29,0($29)		!gpdisp!85
	ldl $1,472($15)
	subl $1,1,$1
	addl $31,$1,$3
	lda $1,240($15)
	lda $2,192($15)
	mov $3,$19
	ldq $18,464($15)
	mov $2,$17
	mov $1,$16
	ldq $27,_ZL9ray_colorRK3rayRK8hittablei($29)		!literal!86
	jsr $26,($27),_ZL9ray_colorRK3rayRK8hittablei		!lituse_jsr!86
	ldah $29,0($26)		!gpdisp!87
	lda $29,0($29)		!gpdisp!87
	ldq $2,448($15)
	lda $3,240($15)
	ldah $1,$LC3($29)		!gprelhigh
	ldt $f10,$LC3($1)		!gprellow
	mov $3,$18
	cpys $f10,$f10,$f17
	mov $2,$16
	ldq $27,_ZmldRK4vec3($29)		!literal!88
	jsr $26,($27),_ZmldRK4vec3		!lituse_jsr!88
	ldah $29,0($26)		!gpdisp!89
	lda $29,0($29)		!gpdisp!89
	br $31,$L49
$L50:
	lda $1,144($15)
	ldq $17,456($15)
	mov $1,$16
	ldq $27,_ZNK3ray9directionEv($29)		!literal!90
	jsr $26,($27),_ZNK3ray9directionEv		!lituse_jsr!90
	ldah $29,0($26)		!gpdisp!91
	lda $29,0($29)		!gpdisp!91
	lda $1,392($15)
	ldq $17,144($15)
	ldq $18,152($15)
	ldq $19,160($15)
	mov $1,$16
	ldq $27,_Z11unit_vector4vec3($29)		!literal!92
	jsr $26,($27),_Z11unit_vector4vec3		!lituse_jsr!92
	ldah $29,0($26)		!gpdisp!93
	lda $29,0($29)		!gpdisp!93
	lda $1,392($15)
	mov $1,$16
	ldq $27,_ZNK4vec31yEv($29)		!literal!94
	jsr $26,($27),0		!lituse_jsr!94
	ldah $29,0($26)		!gpdisp!95
	lda $29,0($29)		!gpdisp!95
	cpys $f0,$f0,$f11
	ldah $1,$LC0($29)		!gprelhigh
	ldt $f10,$LC0($1)		!gprellow
	addt/su $f11,$f10,$f12
	trapb
	ldah $1,$LC3($29)		!gprelhigh
	ldt $f11,$LC3($1)		!gprellow
	mult/su $f12,$f11,$f10
	trapb
	stt $f10,40($15)
	ldah $1,$LC0($29)		!gprelhigh
	ldt $f11,$LC0($1)		!gprellow
	ldt $f10,40($15)
	subt/su $f11,$f10,$f2
	trapb
	ldah $1,$LC0($29)		!gprelhigh
	ldt $f12,$LC0($1)		!gprellow
	ldah $1,$LC0($29)		!gprelhigh
	ldt $f11,$LC0($1)		!gprellow
	ldah $1,$LC0($29)		!gprelhigh
	ldt $f10,$LC0($1)		!gprellow
	lda $1,96($15)
	cpys $f12,$f12,$f19
	cpys $f11,$f11,$f18
	cpys $f10,$f10,$f17
	mov $1,$16
	ldq $27,_ZN4vec3C1Eddd($29)		!literal!96
	jsr $26,($27),_ZN4vec3C1Eddd		!lituse_jsr!96
	ldah $29,0($26)		!gpdisp!97
	lda $29,0($29)		!gpdisp!97
	lda $1,120($15)
	lda $2,96($15)
	mov $2,$18
	cpys $f2,$f2,$f17
	mov $1,$16
	ldq $27,_ZmldRK4vec3($29)		!literal!98
	jsr $26,($27),_ZmldRK4vec3		!lituse_jsr!98
	ldah $29,0($26)		!gpdisp!99
	lda $29,0($29)		!gpdisp!99
	ldah $1,$LC0($29)		!gprelhigh
	ldt $f12,$LC0($1)		!gprellow
	ldah $1,$LC4($29)		!gprelhigh
	ldt $f11,$LC4($1)		!gprellow
	ldah $1,$LC3($29)		!gprelhigh
	ldt $f10,$LC3($1)		!gprellow
	lda $1,48($15)
	cpys $f12,$f12,$f19
	cpys $f11,$f11,$f18
	cpys $f10,$f10,$f17
	mov $1,$16
	ldq $27,_ZN4vec3C1Eddd($29)		!literal!100
	jsr $26,($27),_ZN4vec3C1Eddd		!lituse_jsr!100
	ldah $29,0($26)		!gpdisp!101
	lda $29,0($29)		!gpdisp!101
	lda $1,72($15)
	lda $2,48($15)
	mov $2,$18
	ldt $f17,40($15)
	mov $1,$16
	ldq $27,_ZmldRK4vec3($29)		!literal!102
	jsr $26,($27),_ZmldRK4vec3		!lituse_jsr!102
	ldah $29,0($26)		!gpdisp!103
	lda $29,0($29)		!gpdisp!103
	ldq $1,448($15)
	lda $3,72($15)
	lda $2,120($15)
	mov $3,$18
	mov $2,$17
	mov $1,$16
	ldq $27,_ZplRK4vec3S1_($29)		!literal!104
	jsr $26,($27),_ZplRK4vec3S1_		!lituse_jsr!104
	ldah $29,0($26)		!gpdisp!105
	lda $29,0($29)		!gpdisp!105
$LEHE4:
$L49:
	lda $1,312($15)
	mov $1,$16
	ldq $27,_ZN10hit_recordD1Ev($29)		!literal!106
	jsr $26,($27),_ZN10hit_recordD1Ev		!lituse_jsr!106
	ldah $29,0($26)		!gpdisp!107
	lda $29,0($29)		!gpdisp!107
	br $31,$L54
$L53:
	ldah $29,0($26)		!gpdisp!70
	lda $29,0($29)		!gpdisp!70
	mov $16,$9
	lda $1,312($15)
	mov $1,$16
	ldq $27,_ZN10hit_recordD1Ev($29)		!literal!108
	jsr $26,($27),_ZN10hit_recordD1Ev		!lituse_jsr!108
	ldah $29,0($26)		!gpdisp!109
	lda $29,0($29)		!gpdisp!109
	mov $9,$1
	mov $1,$16
$LEHB5:
	ldq $27,_Unwind_Resume($29)		!literal!110
	jsr $26,($27),_Unwind_Resume		!lituse_jsr!110
$LEHE5:
$L54:
	ldq $0,448($15)
	mov $15,$30
	ldq $26,0($30)
	ldq $9,8($30)
	ldt $f2,24($30)
	ldq $15,16($30)
	lda $30,480($30)
	.cfi_restore 15
	.cfi_restore 34
	.cfi_restore 9
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE3957:
	.section	.gcc_except_table
$LLSDA3957:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 $LLSDACSE3957-$LLSDACSB3957
$LLSDACSB3957:
	.uleb128 $LEHB3-$LFB3957
	.uleb128 $LEHE3-$LEHB3
	.uleb128 0
	.uleb128 0
	.uleb128 $LEHB4-$LFB3957
	.uleb128 $LEHE4-$LEHB4
	.uleb128 $L53-$LFB3957
	.uleb128 0
	.uleb128 $LEHB5-$LFB3957
	.uleb128 $LEHE5-$LEHB5
	.uleb128 0
	.uleb128 0
$LLSDACSE3957:
	.text
	.end _ZL9ray_colorRK3rayRK8hittablei
	.section	.text._ZN13hittable_listD2Ev,"axG",@progbits,_ZN13hittable_listD5Ev,comdat
	.align 2
	.weak	_ZN13hittable_listD2Ev
	.ent _ZN13hittable_listD2Ev
_ZN13hittable_listD2Ev:
	.eflag 48
	.frame $15,32,$26,0
	.mask 0x4008000,-32
$LFB3972:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!111
	lda $29,0($29)		!gpdisp!111
$_ZN13hittable_listD2Ev..ng:
	lda $30,-32($30)
	.cfi_def_cfa_offset 32
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -32
	.cfi_offset 15, -24
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,16($15)
	ldq $1,_ZTV13hittable_list($29)		!literal
	lda $2,16($1)
	ldq $1,16($15)
	stq $2,0($1)
	ldq $1,16($15)
	lda $1,8($1)
	mov $1,$16
	ldq $27,_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED1Ev($29)		!literal!112
	jsr $26,($27),_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED1Ev		!lituse_jsr!112
	ldah $29,0($26)		!gpdisp!113
	lda $29,0($29)		!gpdisp!113
	bis $31,$31,$31
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,32($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE3972:
	.end _ZN13hittable_listD2Ev
	.weak	_ZN13hittable_listD1Ev
$_ZN13hittable_listD1Ev..ng = $_ZN13hittable_listD2Ev..ng
_ZN13hittable_listD1Ev = _ZN13hittable_listD2Ev
	.section	.text._ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.weak	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED2Ev
	.ent _ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED2Ev
_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED2Ev:
	.eflag 48
	.frame $15,32,$26,0
	.mask 0x4008000,-32
$LFB3976:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!114
	lda $29,0($29)		!gpdisp!114
$_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED2Ev..ng:
	lda $30,-32($30)
	.cfi_def_cfa_offset 32
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -32
	.cfi_offset 15, -24
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,16($15)
	ldq $1,16($15)
	lda $1,8($1)
	mov $1,$16
	ldq $27,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED1Ev($29)		!literal!115
	jsr $26,($27),_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED1Ev		!lituse_jsr!115
	ldah $29,0($26)		!gpdisp!116
	lda $29,0($29)		!gpdisp!116
	bis $31,$31,$31
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,32($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE3976:
	.end _ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED1Ev
$_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED1Ev..ng = $_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED2Ev..ng
_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED1Ev = _ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt10shared_ptrI6sphereED2Ev,"axG",@progbits,_ZNSt10shared_ptrI6sphereED5Ev,comdat
	.align 2
	.weak	_ZNSt10shared_ptrI6sphereED2Ev
	.ent _ZNSt10shared_ptrI6sphereED2Ev
_ZNSt10shared_ptrI6sphereED2Ev:
	.eflag 48
	.frame $15,32,$26,0
	.mask 0x4008000,-32
$LFB3978:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!117
	lda $29,0($29)		!gpdisp!117
$_ZNSt10shared_ptrI6sphereED2Ev..ng:
	lda $30,-32($30)
	.cfi_def_cfa_offset 32
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -32
	.cfi_offset 15, -24
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,16($15)
	ldq $1,16($15)
	mov $1,$16
	ldq $27,_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED2Ev($29)		!literal!118
	jsr $26,($27),_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED2Ev		!lituse_jsr!118
	ldah $29,0($26)		!gpdisp!119
	lda $29,0($29)		!gpdisp!119
	bis $31,$31,$31
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,32($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE3978:
	.end _ZNSt10shared_ptrI6sphereED2Ev
	.weak	_ZNSt10shared_ptrI6sphereED1Ev
$_ZNSt10shared_ptrI6sphereED1Ev..ng = $_ZNSt10shared_ptrI6sphereED2Ev..ng
_ZNSt10shared_ptrI6sphereED1Ev = _ZNSt10shared_ptrI6sphereED2Ev
	.section	.text._ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.weak	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED2Ev
	.ent _ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED2Ev
_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED2Ev:
	.eflag 48
	.frame $15,32,$26,0
	.mask 0x4008000,-32
$LFB3982:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!120
	lda $29,0($29)		!gpdisp!120
$_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED2Ev..ng:
	lda $30,-32($30)
	.cfi_def_cfa_offset 32
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -32
	.cfi_offset 15, -24
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,16($15)
	ldq $1,16($15)
	lda $1,8($1)
	mov $1,$16
	ldq $27,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED1Ev($29)		!literal!121
	jsr $26,($27),_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED1Ev		!lituse_jsr!121
	ldah $29,0($26)		!gpdisp!122
	lda $29,0($29)		!gpdisp!122
	bis $31,$31,$31
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,32($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE3982:
	.end _ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED1Ev
$_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED1Ev..ng = $_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED2Ev..ng
_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED1Ev = _ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt10shared_ptrI8hittableED2Ev,"axG",@progbits,_ZNSt10shared_ptrI8hittableED5Ev,comdat
	.align 2
	.weak	_ZNSt10shared_ptrI8hittableED2Ev
	.ent _ZNSt10shared_ptrI8hittableED2Ev
_ZNSt10shared_ptrI8hittableED2Ev:
	.eflag 48
	.frame $15,32,$26,0
	.mask 0x4008000,-32
$LFB3984:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!123
	lda $29,0($29)		!gpdisp!123
$_ZNSt10shared_ptrI8hittableED2Ev..ng:
	lda $30,-32($30)
	.cfi_def_cfa_offset 32
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -32
	.cfi_offset 15, -24
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,16($15)
	ldq $1,16($15)
	mov $1,$16
	ldq $27,_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED2Ev($29)		!literal!124
	jsr $26,($27),_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED2Ev		!lituse_jsr!124
	ldah $29,0($26)		!gpdisp!125
	lda $29,0($29)		!gpdisp!125
	bis $31,$31,$31
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,32($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE3984:
	.end _ZNSt10shared_ptrI8hittableED2Ev
	.weak	_ZNSt10shared_ptrI8hittableED1Ev
$_ZNSt10shared_ptrI8hittableED1Ev..ng = $_ZNSt10shared_ptrI8hittableED2Ev..ng
_ZNSt10shared_ptrI8hittableED1Ev = _ZNSt10shared_ptrI8hittableED2Ev
	.section	.rodata
$LC8:
	.string	"chapter8.ppm"
$LC9:
	.string	"P3\n"
$LC10:
	.string	"\n255\n"
$LC11:
	.string	"\rScanlines remaining: "
$LC14:
	.string	"\nDone.\n"
	.text
	.align 2
	.globl main
	.ent main
main:
	.eflag 48
	.frame $15,1056,$26,0
	.mask 0x4008200,-1056
	.fmask 0x4,-1032
$LFB3970:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,$LLSDA3970
	ldah $29,0($27)		!gpdisp!126
	lda $29,0($29)		!gpdisp!126
$main..ng:
	lda $30,-1056($30)
	.cfi_def_cfa_offset 1056
	stq $26,0($30)
	stq $9,8($30)
	stq $15,16($30)
	stt $f2,24($30)
	.cfi_offset 26, -1056
	.cfi_offset 9, -1048
	.cfi_offset 15, -1040
	.cfi_offset 34, -1032
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	ldah $1,$LC5($29)		!gprelhigh
	ldt $f10,$LC5($1)		!gprellow
	stt $f10,48($15)
	lda $1,400($31)
	stl $1,56($15)
	lda $1,225($31)
	stl $1,60($15)
	lda $1,100($31)
	stl $1,64($15)
	lda $1,50($31)
	stl $1,68($15)
	lda $1,240($15)
	mov $1,$16
$LEHB6:
	ldq $27,_ZN13hittable_listC1Ev($29)		!literal!133
	jsr $26,($27),_ZN13hittable_listC1Ev		!lituse_jsr!133
	ldah $29,0($26)		!gpdisp!134
	lda $29,0($29)		!gpdisp!134
$LEHE6:
	ldah $1,$LC6($29)		!gprelhigh
	ldt $f10,$LC6($1)		!gprellow
	lda $1,184($15)
	cpys $f10,$f10,$f19
	cpys $f31,$f31,$f18
	cpys $f31,$f31,$f17
	mov $1,$16
$LEHB7:
	ldq $27,_ZN4vec3C1Eddd($29)		!literal!135
	jsr $26,($27),_ZN4vec3C1Eddd		!lituse_jsr!135
	ldah $29,0($26)		!gpdisp!136
	lda $29,0($29)		!gpdisp!136
$LEHE7:
	ldah $1,$LC3($29)		!gprelhigh
	ldt $f10,$LC3($1)		!gprellow
	stt $f10,176($15)
	lda $1,208($15)
	lda $3,176($15)
	lda $2,184($15)
	mov $3,$18
	mov $2,$17
	mov $1,$16
$LEHB8:
	ldq $27,_ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_($29)		!literal!137
	jsr $26,($27),_ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_		!lituse_jsr!137
	ldah $29,0($26)		!gpdisp!138
	lda $29,0($29)		!gpdisp!138
$LEHE8:
	lda $2,208($15)
	lda $1,224($15)
	mov $2,$17
	mov $1,$16
	ldq $27,_ZNSt10shared_ptrI8hittableEC1I6spherevEEOS_IT_E($29)		!literal!139
	jsr $26,($27),_ZNSt10shared_ptrI8hittableEC1I6spherevEEOS_IT_E		!lituse_jsr!139
	ldah $29,0($26)		!gpdisp!140
	lda $29,0($29)		!gpdisp!140
	lda $2,224($15)
	lda $1,240($15)
	mov $2,$17
	mov $1,$16
$LEHB9:
	ldq $27,_ZN13hittable_list3addESt10shared_ptrI8hittableE($29)		!literal!141
	jsr $26,($27),_ZN13hittable_list3addESt10shared_ptrI8hittableE		!lituse_jsr!141
	ldah $29,0($26)		!gpdisp!142
	lda $29,0($29)		!gpdisp!142
$LEHE9:
	lda $1,224($15)
	mov $1,$16
	ldq $27,_ZNSt10shared_ptrI8hittableED1Ev($29)		!literal!143
	jsr $26,($27),_ZNSt10shared_ptrI8hittableED1Ev		!lituse_jsr!143
	ldah $29,0($26)		!gpdisp!144
	lda $29,0($29)		!gpdisp!144
	lda $1,208($15)
	mov $1,$16
	ldq $27,_ZNSt10shared_ptrI6sphereED1Ev($29)		!literal!145
	jsr $26,($27),_ZNSt10shared_ptrI6sphereED1Ev		!lituse_jsr!145
	ldah $29,0($26)		!gpdisp!146
	lda $29,0($29)		!gpdisp!146
	ldah $1,$LC6($29)		!gprelhigh
	ldt $f11,$LC6($1)		!gprellow
	ldah $1,$LC7($29)		!gprelhigh
	ldt $f10,$LC7($1)		!gprellow
	lda $1,120($15)
	cpys $f11,$f11,$f19
	cpys $f10,$f10,$f18
	cpys $f31,$f31,$f17
	mov $1,$16
$LEHB10:
	ldq $27,_ZN4vec3C1Eddd($29)		!literal!147
	jsr $26,($27),_ZN4vec3C1Eddd		!lituse_jsr!147
	ldah $29,0($26)		!gpdisp!148
	lda $29,0($29)		!gpdisp!148
$LEHE10:
	lda $1,100($31)
	stl $1,112($15)
	lda $1,144($15)
	lda $3,112($15)
	lda $2,120($15)
	mov $3,$18
	mov $2,$17
	mov $1,$16
$LEHB11:
	ldq $27,_ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_($29)		!literal!149
	jsr $26,($27),_ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_		!lituse_jsr!149
	ldah $29,0($26)		!gpdisp!150
	lda $29,0($29)		!gpdisp!150
$LEHE11:
	lda $2,144($15)
	lda $1,160($15)
	mov $2,$17
	mov $1,$16
	ldq $27,_ZNSt10shared_ptrI8hittableEC1I6spherevEEOS_IT_E($29)		!literal!151
	jsr $26,($27),_ZNSt10shared_ptrI8hittableEC1I6spherevEEOS_IT_E		!lituse_jsr!151
	ldah $29,0($26)		!gpdisp!152
	lda $29,0($29)		!gpdisp!152
	lda $2,160($15)
	lda $1,240($15)
	mov $2,$17
	mov $1,$16
$LEHB12:
	ldq $27,_ZN13hittable_list3addESt10shared_ptrI8hittableE($29)		!literal!153
	jsr $26,($27),_ZN13hittable_list3addESt10shared_ptrI8hittableE		!lituse_jsr!153
	ldah $29,0($26)		!gpdisp!154
	lda $29,0($29)		!gpdisp!154
$LEHE12:
	lda $1,160($15)
	mov $1,$16
	ldq $27,_ZNSt10shared_ptrI8hittableED1Ev($29)		!literal!155
	jsr $26,($27),_ZNSt10shared_ptrI8hittableED1Ev		!lituse_jsr!155
	ldah $29,0($26)		!gpdisp!156
	lda $29,0($29)		!gpdisp!156
	lda $1,144($15)
	mov $1,$16
	ldq $27,_ZNSt10shared_ptrI6sphereED1Ev($29)		!literal!157
	jsr $26,($27),_ZNSt10shared_ptrI6sphereED1Ev		!lituse_jsr!157
	ldah $29,0($26)		!gpdisp!158
	lda $29,0($29)		!gpdisp!158
	lda $1,272($15)
	mov $1,$16
$LEHB13:
	ldq $27,_ZN6cameraC1Ev($29)		!literal!159
	jsr $26,($27),_ZN6cameraC1Ev		!lituse_jsr!159
	ldah $29,0($26)		!gpdisp!160
	lda $29,0($29)		!gpdisp!160
	lda $1,448($15)
	mov $1,$16
	ldq $27,_ZNSt14basic_ofstreamIcSt11char_traitsIcEEC1Ev($29)		!literal!161
	jsr $26,($27),_ZNSt14basic_ofstreamIcSt11char_traitsIcEEC1Ev		!lituse_jsr!161
	ldah $29,0($26)		!gpdisp!162
	lda $29,0($29)		!gpdisp!162
$LEHE13:
	lda $2,448($15)
	lda $18,16($31)
	ldah $1,$LC8($29)		!gprelhigh
	lda $17,$LC8($1)		!gprellow
	mov $2,$16
$LEHB14:
	ldq $27,_ZNSt14basic_ofstreamIcSt11char_traitsIcEE4openEPKcSt13_Ios_Openmode($29)		!literal!163
	jsr $26,($27),_ZNSt14basic_ofstreamIcSt11char_traitsIcEE4openEPKcSt13_Ios_Openmode		!lituse_jsr!163
	ldah $29,0($26)		!gpdisp!164
	lda $29,0($29)		!gpdisp!164
	lda $2,448($15)
	ldah $1,$LC9($29)		!gprelhigh
	lda $17,$LC9($1)		!gprellow
	mov $2,$16
	ldq $27,_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc($29)		!literal!165
	jsr $26,($27),0		!lituse_jsr!165
	ldah $29,0($26)		!gpdisp!166
	lda $29,0($29)		!gpdisp!166
	mov $0,$1
	lda $17,400($31)
	mov $1,$16
	ldq $27,_ZNSolsEi($29)		!literal!167
	jsr $26,($27),0		!lituse_jsr!167
	ldah $29,0($26)		!gpdisp!168
	lda $29,0($29)		!gpdisp!168
	mov $0,$1
	lda $17,32($31)
	mov $1,$16
	ldq $27,_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_c($29)		!literal!169
	jsr $26,($27),0		!lituse_jsr!169
	ldah $29,0($26)		!gpdisp!170
	lda $29,0($29)		!gpdisp!170
	mov $0,$1
	lda $17,225($31)
	mov $1,$16
	ldq $27,_ZNSolsEi($29)		!literal!171
	jsr $26,($27),0		!lituse_jsr!171
	ldah $29,0($26)		!gpdisp!172
	lda $29,0($29)		!gpdisp!172
	mov $0,$1
	ldah $2,$LC10($29)		!gprelhigh
	lda $17,$LC10($2)		!gprellow
	mov $1,$16
	ldq $27,_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc($29)		!literal!173
	jsr $26,($27),0		!lituse_jsr!173
	ldah $29,0($26)		!gpdisp!174
	lda $29,0($29)		!gpdisp!174
	lda $1,224($31)
	stl $1,32($15)
$L66:
	ldl $1,32($15)
	lda $2,-1($31)
	cmple $1,$2,$1
	bne $1,$L61
	ldah $1,$LC11($29)		!gprelhigh
	lda $17,$LC11($1)		!gprellow
	ldq $16,_ZSt4cerr($29)		!literal
	ldq $27,_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc($29)		!literal!175
	jsr $26,($27),0		!lituse_jsr!175
	ldah $29,0($26)		!gpdisp!176
	lda $29,0($29)		!gpdisp!176
	mov $0,$1
	ldl $2,32($15)
	mov $2,$17
	mov $1,$16
	ldq $27,_ZNSolsEi($29)		!literal!177
	jsr $26,($27),0		!lituse_jsr!177
	ldah $29,0($26)		!gpdisp!178
	lda $29,0($29)		!gpdisp!178
	mov $0,$1
	lda $17,32($31)
	mov $1,$16
	ldq $27,_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_c($29)		!literal!179
	jsr $26,($27),0		!lituse_jsr!179
	ldah $29,0($26)		!gpdisp!180
	lda $29,0($29)		!gpdisp!180
	mov $0,$1
	ldq $17,_ZSt5flushIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_($29)		!literal
	mov $1,$16
	ldq $27,_ZNSolsEPFRSoS_E($29)		!literal!181
	jsr $26,($27),0		!lituse_jsr!181
	ldah $29,0($26)		!gpdisp!182
	lda $29,0($29)		!gpdisp!182
	stl $31,36($15)
$L65:
	ldl $1,36($15)
	lda $2,399($31)
	cmple $1,$2,$1
	beq $1,$L62
	lda $1,960($15)
	cpys $f31,$f31,$f19
	cpys $f31,$f31,$f18
	cpys $f31,$f31,$f17
	mov $1,$16
	ldq $27,_ZN4vec3C1Eddd($29)		!literal!183
	jsr $26,($27),_ZN4vec3C1Eddd		!lituse_jsr!183
	ldah $29,0($26)		!gpdisp!184
	lda $29,0($29)		!gpdisp!184
	stl $31,40($15)
$L64:
	ldl $1,40($15)
	cmple $1,99,$1
	beq $1,$L63
	ldl $1,36($15)
	stq $1,1040($15)
	ldt $f10,1040($15)
	cvtqt $f10,$f2
	trapb
	ldq $27,_Z13random_doublev($29)		!literal!185
	jsr $26,($27),0		!lituse_jsr!185
	ldah $29,0($26)		!gpdisp!186
	lda $29,0($29)		!gpdisp!186
	cpys $f0,$f0,$f10
	addt/su $f2,$f10,$f12
	trapb
	ldah $1,$LC12($29)		!gprelhigh
	ldt $f11,$LC12($1)		!gprellow
	divt/su $f12,$f11,$f10
	trapb
	stt $f10,72($15)
	ldl $1,32($15)
	stq $1,1040($15)
	ldt $f10,1040($15)
	cvtqt $f10,$f2
	trapb
	ldq $27,_Z13random_doublev($29)		!literal!187
	jsr $26,($27),0		!lituse_jsr!187
	ldah $29,0($26)		!gpdisp!188
	lda $29,0($29)		!gpdisp!188
	cpys $f0,$f0,$f10
	addt/su $f2,$f10,$f12
	trapb
	ldah $1,$LC13($29)		!gprelhigh
	ldt $f11,$LC13($1)		!gprellow
	divt/su $f12,$f11,$f10
	trapb
	stt $f10,80($15)
	lda $1,984($15)
	lda $2,272($15)
	ldt $f19,80($15)
	ldt $f18,72($15)
	mov $2,$17
	mov $1,$16
	ldq $27,_ZNK6camera7get_rayEdd($29)		!literal!189
	jsr $26,($27),_ZNK6camera7get_rayEdd		!lituse_jsr!189
	ldah $29,0($26)		!gpdisp!190
	lda $29,0($29)		!gpdisp!190
	lda $1,88($15)
	lda $3,240($15)
	lda $2,984($15)
	lda $19,50($31)
	mov $3,$18
	mov $2,$17
	mov $1,$16
	ldq $27,_ZL9ray_colorRK3rayRK8hittablei($29)		!literal!191
	jsr $26,($27),_ZL9ray_colorRK3rayRK8hittablei		!lituse_jsr!191
	ldah $29,0($26)		!gpdisp!192
	lda $29,0($29)		!gpdisp!192
	lda $2,88($15)
	lda $1,960($15)
	mov $2,$17
	mov $1,$16
	ldq $27,_ZN4vec3pLERKS_($29)		!literal!193
	jsr $26,($27),0		!lituse_jsr!193
	ldah $29,0($26)		!gpdisp!194
	lda $29,0($29)		!gpdisp!194
	ldl $1,40($15)
	addl $1,1,$1
	stl $1,40($15)
	br $31,$L64
$L63:
	lda $1,448($15)
	lda $20,100($31)
	ldq $17,960($15)
	ldq $18,968($15)
	ldq $19,976($15)
	mov $1,$16
	ldq $27,_Z11write_colorRSt14basic_ofstreamIcSt11char_traitsIcEE4vec3i($29)		!literal!195
	jsr $26,($27),_Z11write_colorRSt14basic_ofstreamIcSt11char_traitsIcEE4vec3i		!lituse_jsr!195
	ldah $29,0($26)		!gpdisp!196
	lda $29,0($29)		!gpdisp!196
	ldl $1,36($15)
	addl $1,1,$1
	stl $1,36($15)
	br $31,$L65
$L62:
	ldl $1,32($15)
	subl $1,1,$1
	stl $1,32($15)
	br $31,$L66
$L61:
	ldah $1,$LC14($29)		!gprelhigh
	lda $17,$LC14($1)		!gprellow
	ldq $16,_ZSt4cerr($29)		!literal
	ldq $27,_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc($29)		!literal!197
	jsr $26,($27),0		!lituse_jsr!197
	ldah $29,0($26)		!gpdisp!198
	lda $29,0($29)		!gpdisp!198
$LEHE14:
	mov $31,$9
	lda $1,448($15)
	mov $1,$16
	ldq $27,_ZNSt14basic_ofstreamIcSt11char_traitsIcEED1Ev($29)		!literal!199
	jsr $26,($27),_ZNSt14basic_ofstreamIcSt11char_traitsIcEED1Ev		!lituse_jsr!199
	ldah $29,0($26)		!gpdisp!200
	lda $29,0($29)		!gpdisp!200
	lda $1,240($15)
	mov $1,$16
	ldq $27,_ZN13hittable_listD1Ev($29)		!literal!201
	jsr $26,($27),_ZN13hittable_listD1Ev		!lituse_jsr!201
	ldah $29,0($26)		!gpdisp!202
	lda $29,0($29)		!gpdisp!202
	mov $9,$1
	br $31,$L80
$L76:
	ldah $29,0($26)		!gpdisp!127
	lda $29,0($29)		!gpdisp!127
	mov $16,$9
	lda $1,224($15)
	mov $1,$16
	ldq $27,_ZNSt10shared_ptrI8hittableED1Ev($29)		!literal!203
	jsr $26,($27),_ZNSt10shared_ptrI8hittableED1Ev		!lituse_jsr!203
	ldah $29,0($26)		!gpdisp!204
	lda $29,0($29)		!gpdisp!204
	lda $1,208($15)
	mov $1,$16
	ldq $27,_ZNSt10shared_ptrI6sphereED1Ev($29)		!literal!205
	jsr $26,($27),_ZNSt10shared_ptrI6sphereED1Ev		!lituse_jsr!205
	ldah $29,0($26)		!gpdisp!206
	lda $29,0($29)		!gpdisp!206
	mov $9,$1
	br $31,$L69
$L75:
	ldah $29,0($26)		!gpdisp!128
	lda $29,0($29)		!gpdisp!128
	mov $16,$1
$L69:
	mov $1,$9
	br $31,$L70
$L78:
	ldah $29,0($26)		!gpdisp!129
	lda $29,0($29)		!gpdisp!129
	mov $16,$9
	lda $1,160($15)
	mov $1,$16
	ldq $27,_ZNSt10shared_ptrI8hittableED1Ev($29)		!literal!207
	jsr $26,($27),_ZNSt10shared_ptrI8hittableED1Ev		!lituse_jsr!207
	ldah $29,0($26)		!gpdisp!208
	lda $29,0($29)		!gpdisp!208
	lda $1,144($15)
	mov $1,$16
	ldq $27,_ZNSt10shared_ptrI6sphereED1Ev($29)		!literal!209
	jsr $26,($27),_ZNSt10shared_ptrI6sphereED1Ev		!lituse_jsr!209
	ldah $29,0($26)		!gpdisp!210
	lda $29,0($29)		!gpdisp!210
	mov $9,$1
	br $31,$L72
$L77:
	ldah $29,0($26)		!gpdisp!130
	lda $29,0($29)		!gpdisp!130
	mov $16,$1
$L72:
	mov $1,$9
	br $31,$L70
$L79:
	ldah $29,0($26)		!gpdisp!131
	lda $29,0($29)		!gpdisp!131
	mov $16,$9
	lda $1,448($15)
	mov $1,$16
	ldq $27,_ZNSt14basic_ofstreamIcSt11char_traitsIcEED1Ev($29)		!literal!211
	jsr $26,($27),_ZNSt14basic_ofstreamIcSt11char_traitsIcEED1Ev		!lituse_jsr!211
	ldah $29,0($26)		!gpdisp!212
	lda $29,0($29)		!gpdisp!212
	br $31,$L70
$L74:
	ldah $29,0($26)		!gpdisp!132
	lda $29,0($29)		!gpdisp!132
	mov $16,$9
$L70:
	lda $1,240($15)
	mov $1,$16
	ldq $27,_ZN13hittable_listD1Ev($29)		!literal!213
	jsr $26,($27),_ZN13hittable_listD1Ev		!lituse_jsr!213
	ldah $29,0($26)		!gpdisp!214
	lda $29,0($29)		!gpdisp!214
	mov $9,$1
	mov $1,$16
$LEHB15:
	ldq $27,_Unwind_Resume($29)		!literal!215
	jsr $26,($27),_Unwind_Resume		!lituse_jsr!215
$LEHE15:
$L80:
	mov $1,$0
	mov $15,$30
	ldq $26,0($30)
	ldq $9,8($30)
	ldt $f2,24($30)
	ldq $15,16($30)
	lda $30,1056($30)
	.cfi_restore 15
	.cfi_restore 34
	.cfi_restore 9
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE3970:
	.section	.gcc_except_table
$LLSDA3970:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 $LLSDACSE3970-$LLSDACSB3970
$LLSDACSB3970:
	.uleb128 $LEHB6-$LFB3970
	.uleb128 $LEHE6-$LEHB6
	.uleb128 0
	.uleb128 0
	.uleb128 $LEHB7-$LFB3970
	.uleb128 $LEHE7-$LEHB7
	.uleb128 $L74-$LFB3970
	.uleb128 0
	.uleb128 $LEHB8-$LFB3970
	.uleb128 $LEHE8-$LEHB8
	.uleb128 $L75-$LFB3970
	.uleb128 0
	.uleb128 $LEHB9-$LFB3970
	.uleb128 $LEHE9-$LEHB9
	.uleb128 $L76-$LFB3970
	.uleb128 0
	.uleb128 $LEHB10-$LFB3970
	.uleb128 $LEHE10-$LEHB10
	.uleb128 $L74-$LFB3970
	.uleb128 0
	.uleb128 $LEHB11-$LFB3970
	.uleb128 $LEHE11-$LEHB11
	.uleb128 $L77-$LFB3970
	.uleb128 0
	.uleb128 $LEHB12-$LFB3970
	.uleb128 $LEHE12-$LEHB12
	.uleb128 $L78-$LFB3970
	.uleb128 0
	.uleb128 $LEHB13-$LFB3970
	.uleb128 $LEHE13-$LEHB13
	.uleb128 $L74-$LFB3970
	.uleb128 0
	.uleb128 $LEHB14-$LFB3970
	.uleb128 $LEHE14-$LEHB14
	.uleb128 $L79-$LFB3970
	.uleb128 0
	.uleb128 $LEHB15-$LFB3970
	.uleb128 $LEHE15-$LEHB15
	.uleb128 0
	.uleb128 0
$LLSDACSE3970:
	.text
	.end main
	.section	.text._ZNSt25uniform_real_distributionIdEC2Edd,"axG",@progbits,_ZNSt25uniform_real_distributionIdEC5Edd,comdat
	.align 2
	.weak	_ZNSt25uniform_real_distributionIdEC2Edd
	.ent _ZNSt25uniform_real_distributionIdEC2Edd
_ZNSt25uniform_real_distributionIdEC2Edd:
	.eflag 48
	.frame $15,48,$26,0
	.mask 0x4008000,-48
$LFB4235:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!216
	lda $29,0($29)		!gpdisp!216
$_ZNSt25uniform_real_distributionIdEC2Edd..ng:
	lda $30,-48($30)
	.cfi_def_cfa_offset 48
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -48
	.cfi_offset 15, -40
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,16($15)
	stt $f17,24($15)
	stt $f18,32($15)
	ldq $1,16($15)
	ldt $f18,32($15)
	ldt $f17,24($15)
	mov $1,$16
	ldq $27,_ZNSt25uniform_real_distributionIdE10param_typeC1Edd($29)		!literal!217
	jsr $26,($27),_ZNSt25uniform_real_distributionIdE10param_typeC1Edd		!lituse_jsr!217
	ldah $29,0($26)		!gpdisp!218
	lda $29,0($29)		!gpdisp!218
	bis $31,$31,$31
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,48($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4235:
	.end _ZNSt25uniform_real_distributionIdEC2Edd
	.weak	_ZNSt25uniform_real_distributionIdEC1Edd
$_ZNSt25uniform_real_distributionIdEC1Edd..ng = $_ZNSt25uniform_real_distributionIdEC2Edd..ng
_ZNSt25uniform_real_distributionIdEC1Edd = _ZNSt25uniform_real_distributionIdEC2Edd
	.section	.text._ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC2Ev,"axG",@progbits,_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC5Ev,comdat
	.align 2
	.weak	_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC2Ev
	.ent _ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC2Ev
_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC2Ev:
	.eflag 48
	.frame $15,32,$26,0
	.mask 0x4008000,-32
$LFB4238:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!219
	lda $29,0($29)		!gpdisp!219
$_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC2Ev..ng:
	lda $30,-32($30)
	.cfi_def_cfa_offset 32
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -32
	.cfi_offset 15, -24
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,16($15)
	lda $17,5489($31)
	ldq $16,16($15)
	ldq $27,_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC1Em($29)		!literal!220
	jsr $26,($27),_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC1Em		!lituse_jsr!220
	ldah $29,0($26)		!gpdisp!221
	lda $29,0($29)		!gpdisp!221
	bis $31,$31,$31
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,32($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4238:
	.end _ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC2Ev
	.weak	_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC1Ev
$_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC1Ev..ng = $_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC2Ev..ng
_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC1Ev = _ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC2Ev
	.section	.text._ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEEEdRT_,"axG",@progbits,_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEEEdRT_,comdat
	.align 2
	.weak	_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEEEdRT_
	.ent _ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEEEdRT_
_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEEEdRT_:
	.eflag 48
	.frame $15,32,$26,0
	.mask 0x4008000,-32
$LFB4240:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!222
	lda $29,0($29)		!gpdisp!222
$_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEEEdRT_..ng:
	lda $30,-32($30)
	.cfi_def_cfa_offset 32
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -32
	.cfi_offset 15, -24
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,16($15)
	stq $17,24($15)
	ldq $1,16($15)
	mov $1,$18
	ldq $17,24($15)
	ldq $16,16($15)
	ldq $27,_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEEEdRT_RKNS0_10param_typeE($29)		!literal!223
	jsr $26,($27),0		!lituse_jsr!223
	ldah $29,0($26)		!gpdisp!224
	lda $29,0($29)		!gpdisp!224
	cpys $f0,$f0,$f10
	cpys $f10,$f10,$f0
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,32($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4240:
	.end _ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEEEdRT_
	.section	.text._ZNSt10shared_ptrI8materialEC2Ev,"axG",@progbits,_ZNSt10shared_ptrI8materialEC5Ev,comdat
	.align 2
	.weak	_ZNSt10shared_ptrI8materialEC2Ev
	.ent _ZNSt10shared_ptrI8materialEC2Ev
_ZNSt10shared_ptrI8materialEC2Ev:
	.eflag 48
	.frame $15,32,$26,0
	.mask 0x4008000,-32
$LFB4268:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!225
	lda $29,0($29)		!gpdisp!225
$_ZNSt10shared_ptrI8materialEC2Ev..ng:
	lda $30,-32($30)
	.cfi_def_cfa_offset 32
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -32
	.cfi_offset 15, -24
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,16($15)
	ldq $1,16($15)
	mov $1,$16
	ldq $27,_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC2Ev($29)		!literal!226
	jsr $26,($27),_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC2Ev		!lituse_jsr!226
	ldah $29,0($26)		!gpdisp!227
	lda $29,0($29)		!gpdisp!227
	bis $31,$31,$31
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,32($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4268:
	.end _ZNSt10shared_ptrI8materialEC2Ev
	.weak	_ZNSt10shared_ptrI8materialEC1Ev
$_ZNSt10shared_ptrI8materialEC1Ev..ng = $_ZNSt10shared_ptrI8materialEC2Ev..ng
_ZNSt10shared_ptrI8materialEC1Ev = _ZNSt10shared_ptrI8materialEC2Ev
	.section	.text._ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED2Ev
	.ent _ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED2Ev
_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED2Ev:
	.eflag 48
	.frame $15,32,$26,0
	.mask 0x4008000,-32
$LFB4271:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!228
	lda $29,0($29)		!gpdisp!228
$_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED2Ev..ng:
	lda $30,-32($30)
	.cfi_def_cfa_offset 32
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -32
	.cfi_offset 15, -24
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,16($15)
	ldq $1,16($15)
	ldq $1,0($1)
	beq $1,$L88
	ldq $1,16($15)
	ldq $1,0($1)
	mov $1,$16
	ldq $27,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv($29)		!literal!229
	jsr $26,($27),_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv		!lituse_jsr!229
	ldah $29,0($26)		!gpdisp!230
	lda $29,0($29)		!gpdisp!230
$L88:
	bis $31,$31,$31
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,32($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4271:
	.end _ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED1Ev
$_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED1Ev..ng = $_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED2Ev..ng
_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED1Ev = _ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED2Ev,"axG",@progbits,_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED5Ev,comdat
	.align 2
	.weak	_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED2Ev
	.ent _ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED2Ev
_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED2Ev:
	.eflag 48
	.frame $15,48,$26,0
	.mask 0x4008600,-48
$LFB4274:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,$LLSDA4274
	ldah $29,0($27)		!gpdisp!231
	lda $29,0($29)		!gpdisp!231
$_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED2Ev..ng:
	lda $30,-48($30)
	.cfi_def_cfa_offset 48
	stq $26,0($30)
	stq $9,8($30)
	stq $10,16($30)
	stq $15,24($30)
	.cfi_offset 26, -48
	.cfi_offset 9, -40
	.cfi_offset 10, -32
	.cfi_offset 15, -24
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,32($15)
	ldq $1,32($15)
	ldq $9,0($1)
	ldq $1,32($15)
	ldq $10,8($1)
	ldq $1,32($15)
	mov $1,$16
	ldq $27,_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE19_M_get_Tp_allocatorEv($29)		!literal!232
	jsr $26,($27),0		!lituse_jsr!232
	ldah $29,0($26)		!gpdisp!233
	lda $29,0($29)		!gpdisp!233
	mov $0,$1
	mov $1,$18
	mov $10,$17
	mov $9,$16
	ldq $27,_ZSt8_DestroyIPSt10shared_ptrI8hittableES2_EvT_S4_RSaIT0_E($29)		!literal!234
	jsr $26,($27),_ZSt8_DestroyIPSt10shared_ptrI8hittableES2_EvT_S4_RSaIT0_E		!lituse_jsr!234
	ldah $29,0($26)		!gpdisp!235
	lda $29,0($29)		!gpdisp!235
	ldq $1,32($15)
	mov $1,$16
	ldq $27,_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED2Ev($29)		!literal!236
	jsr $26,($27),_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED2Ev		!lituse_jsr!236
	ldah $29,0($26)		!gpdisp!237
	lda $29,0($29)		!gpdisp!237
	bis $31,$31,$31
	mov $15,$30
	ldq $26,0($30)
	ldq $9,8($30)
	ldq $10,16($30)
	ldq $15,24($30)
	lda $30,48($30)
	.cfi_restore 15
	.cfi_restore 10
	.cfi_restore 9
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4274:
	.section	.gcc_except_table
$LLSDA4274:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 $LLSDACSE4274-$LLSDACSB4274
$LLSDACSB4274:
$LLSDACSE4274:
	.section	.text._ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED2Ev,"axG",@progbits,_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED5Ev,comdat
	.end _ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED2Ev
	.weak	_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED1Ev
$_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED1Ev..ng = $_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED2Ev..ng
_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED1Ev = _ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED2Ev
	.section	.text._ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_,"axG",@progbits,_ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_,comdat
	.align 2
	.weak	_ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_
	.ent _ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_
_ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_:
	.eflag 48
	.frame $15,80,$26,0
	.mask 0x4008200,-80
$LFB4276:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,$LLSDA4276
	ldah $29,0($27)		!gpdisp!238
	lda $29,0($29)		!gpdisp!238
$_ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_..ng:
	lda $30,-80($30)
	.cfi_def_cfa_offset 80
	stq $26,0($30)
	stq $9,8($30)
	stq $15,16($30)
	.cfi_offset 26, -80
	.cfi_offset 9, -72
	.cfi_offset 15, -64
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,48($15)
	stq $17,56($15)
	stq $18,64($15)
	lda $16,32($15)
	ldq $27,_ZNSaI6sphereEC1Ev($29)		!literal!240
	jsr $26,($27),_ZNSaI6sphereEC1Ev		!lituse_jsr!240
	ldah $29,0($26)		!gpdisp!241
	lda $29,0($29)		!gpdisp!241
	ldq $16,56($15)
	ldq $27,_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE($29)		!literal!242
	jsr $26,($27),0		!lituse_jsr!242
	ldah $29,0($26)		!gpdisp!243
	lda $29,0($29)		!gpdisp!243
	mov $0,$9
	ldq $16,64($15)
	ldq $27,_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE($29)		!literal!244
	jsr $26,($27),0		!lituse_jsr!244
	ldah $29,0($26)		!gpdisp!245
	lda $29,0($29)		!gpdisp!245
	mov $0,$1
	ldq $2,48($15)
	mov $1,$19
	mov $9,$18
	lda $17,32($15)
	mov $2,$16
$LEHB16:
	ldq $27,_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3dEESt10shared_ptrIT_ERKT0_DpOT1_($29)		!literal!246
	jsr $26,($27),_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3dEESt10shared_ptrIT_ERKT0_DpOT1_		!lituse_jsr!246
	ldah $29,0($26)		!gpdisp!247
	lda $29,0($29)		!gpdisp!247
$LEHE16:
	lda $16,32($15)
	ldq $27,_ZNSaI6sphereED1Ev($29)		!literal!248
	jsr $26,($27),_ZNSaI6sphereED1Ev		!lituse_jsr!248
	ldah $29,0($26)		!gpdisp!249
	lda $29,0($29)		!gpdisp!249
	br $31,$L94
$L93:
	ldah $29,0($26)		!gpdisp!239
	lda $29,0($29)		!gpdisp!239
	mov $16,$9
	lda $16,32($15)
	ldq $27,_ZNSaI6sphereED1Ev($29)		!literal!250
	jsr $26,($27),_ZNSaI6sphereED1Ev		!lituse_jsr!250
	ldah $29,0($26)		!gpdisp!251
	lda $29,0($29)		!gpdisp!251
	mov $9,$1
	mov $1,$16
$LEHB17:
	ldq $27,_Unwind_Resume($29)		!literal!252
	jsr $26,($27),_Unwind_Resume		!lituse_jsr!252
$LEHE17:
$L94:
	ldq $0,48($15)
	mov $15,$30
	ldq $26,0($30)
	ldq $9,8($30)
	ldq $15,16($30)
	lda $30,80($30)
	.cfi_restore 15
	.cfi_restore 9
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4276:
	.section	.gcc_except_table
$LLSDA4276:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 $LLSDACSE4276-$LLSDACSB4276
$LLSDACSB4276:
	.uleb128 $LEHB16-$LFB4276
	.uleb128 $LEHE16-$LEHB16
	.uleb128 $L93-$LFB4276
	.uleb128 0
	.uleb128 $LEHB17-$LFB4276
	.uleb128 $LEHE17-$LEHB17
	.uleb128 0
	.uleb128 0
$LLSDACSE4276:
	.section	.text._ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_,"axG",@progbits,_ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_,comdat
	.end _ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_
	.section	.text._ZNSt10shared_ptrI8hittableEC2I6spherevEEOS_IT_E,"axG",@progbits,_ZNSt10shared_ptrI8hittableEC5I6spherevEEOS_IT_E,comdat
	.align 2
	.weak	_ZNSt10shared_ptrI8hittableEC2I6spherevEEOS_IT_E
	.ent _ZNSt10shared_ptrI8hittableEC2I6spherevEEOS_IT_E
_ZNSt10shared_ptrI8hittableEC2I6spherevEEOS_IT_E:
	.eflag 48
	.frame $15,48,$26,0
	.mask 0x4008200,-48
$LFB4278:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!253
	lda $29,0($29)		!gpdisp!253
$_ZNSt10shared_ptrI8hittableEC2I6spherevEEOS_IT_E..ng:
	lda $30,-48($30)
	.cfi_def_cfa_offset 48
	stq $26,0($30)
	stq $9,8($30)
	stq $15,16($30)
	.cfi_offset 26, -48
	.cfi_offset 9, -40
	.cfi_offset 15, -32
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,32($15)
	stq $17,40($15)
	ldq $9,32($15)
	ldq $16,40($15)
	ldq $27,_ZSt4moveIRSt10shared_ptrI6sphereEEONSt16remove_referenceIT_E4typeEOS5_($29)		!literal!254
	jsr $26,($27),0		!lituse_jsr!254
	ldah $29,0($26)		!gpdisp!255
	lda $29,0($29)		!gpdisp!255
	mov $0,$1
	mov $1,$17
	mov $9,$16
	ldq $27,_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC2I6spherevEEOS_IT_LS2_2EE($29)		!literal!256
	jsr $26,($27),_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC2I6spherevEEOS_IT_LS2_2EE		!lituse_jsr!256
	ldah $29,0($26)		!gpdisp!257
	lda $29,0($29)		!gpdisp!257
	bis $31,$31,$31
	mov $15,$30
	ldq $26,0($30)
	ldq $9,8($30)
	ldq $15,16($30)
	lda $30,48($30)
	.cfi_restore 15
	.cfi_restore 9
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4278:
	.end _ZNSt10shared_ptrI8hittableEC2I6spherevEEOS_IT_E
	.weak	_ZNSt10shared_ptrI8hittableEC1I6spherevEEOS_IT_E
$_ZNSt10shared_ptrI8hittableEC1I6spherevEEOS_IT_E..ng = $_ZNSt10shared_ptrI8hittableEC2I6spherevEEOS_IT_E..ng
_ZNSt10shared_ptrI8hittableEC1I6spherevEEOS_IT_E = _ZNSt10shared_ptrI8hittableEC2I6spherevEEOS_IT_E
	.section	.text._ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_,"axG",@progbits,_ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_,comdat
	.align 2
	.weak	_ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_
	.ent _ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_
_ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_:
	.eflag 48
	.frame $15,80,$26,0
	.mask 0x4008200,-80
$LFB4283:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,$LLSDA4283
	ldah $29,0($27)		!gpdisp!258
	lda $29,0($29)		!gpdisp!258
$_ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_..ng:
	lda $30,-80($30)
	.cfi_def_cfa_offset 80
	stq $26,0($30)
	stq $9,8($30)
	stq $15,16($30)
	.cfi_offset 26, -80
	.cfi_offset 9, -72
	.cfi_offset 15, -64
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,48($15)
	stq $17,56($15)
	stq $18,64($15)
	lda $16,32($15)
	ldq $27,_ZNSaI6sphereEC1Ev($29)		!literal!260
	jsr $26,($27),_ZNSaI6sphereEC1Ev		!lituse_jsr!260
	ldah $29,0($26)		!gpdisp!261
	lda $29,0($29)		!gpdisp!261
	ldq $16,56($15)
	ldq $27,_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE($29)		!literal!262
	jsr $26,($27),0		!lituse_jsr!262
	ldah $29,0($26)		!gpdisp!263
	lda $29,0($29)		!gpdisp!263
	mov $0,$9
	ldq $16,64($15)
	ldq $27,_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE($29)		!literal!264
	jsr $26,($27),0		!lituse_jsr!264
	ldah $29,0($26)		!gpdisp!265
	lda $29,0($29)		!gpdisp!265
	mov $0,$1
	ldq $2,48($15)
	mov $1,$19
	mov $9,$18
	lda $17,32($15)
	mov $2,$16
$LEHB18:
	ldq $27,_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3iEESt10shared_ptrIT_ERKT0_DpOT1_($29)		!literal!266
	jsr $26,($27),_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3iEESt10shared_ptrIT_ERKT0_DpOT1_		!lituse_jsr!266
	ldah $29,0($26)		!gpdisp!267
	lda $29,0($29)		!gpdisp!267
$LEHE18:
	lda $16,32($15)
	ldq $27,_ZNSaI6sphereED1Ev($29)		!literal!268
	jsr $26,($27),_ZNSaI6sphereED1Ev		!lituse_jsr!268
	ldah $29,0($26)		!gpdisp!269
	lda $29,0($29)		!gpdisp!269
	br $31,$L100
$L99:
	ldah $29,0($26)		!gpdisp!259
	lda $29,0($29)		!gpdisp!259
	mov $16,$9
	lda $16,32($15)
	ldq $27,_ZNSaI6sphereED1Ev($29)		!literal!270
	jsr $26,($27),_ZNSaI6sphereED1Ev		!lituse_jsr!270
	ldah $29,0($26)		!gpdisp!271
	lda $29,0($29)		!gpdisp!271
	mov $9,$1
	mov $1,$16
$LEHB19:
	ldq $27,_Unwind_Resume($29)		!literal!272
	jsr $26,($27),_Unwind_Resume		!lituse_jsr!272
$LEHE19:
$L100:
	ldq $0,48($15)
	mov $15,$30
	ldq $26,0($30)
	ldq $9,8($30)
	ldq $15,16($30)
	lda $30,80($30)
	.cfi_restore 15
	.cfi_restore 9
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4283:
	.section	.gcc_except_table
$LLSDA4283:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 $LLSDACSE4283-$LLSDACSB4283
$LLSDACSB4283:
	.uleb128 $LEHB18-$LFB4283
	.uleb128 $LEHE18-$LEHB18
	.uleb128 $L99-$LFB4283
	.uleb128 0
	.uleb128 $LEHB19-$LFB4283
	.uleb128 $LEHE19-$LEHB19
	.uleb128 0
	.uleb128 0
$LLSDACSE4283:
	.section	.text._ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_,"axG",@progbits,_ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_,comdat
	.end _ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_
	.section	.text._ZNSt25uniform_real_distributionIdE10param_typeC2Edd,"axG",@progbits,_ZNSt25uniform_real_distributionIdE10param_typeC5Edd,comdat
	.align 2
	.weak	_ZNSt25uniform_real_distributionIdE10param_typeC2Edd
	.ent _ZNSt25uniform_real_distributionIdE10param_typeC2Edd
$_ZNSt25uniform_real_distributionIdE10param_typeC2Edd..ng:
_ZNSt25uniform_real_distributionIdE10param_typeC2Edd:
	.eflag 48
	.frame $15,48,$26,0
	.mask 0x4008000,-48
$LFB4410:
	.cfi_startproc
	lda $30,-48($30)
	.cfi_def_cfa_offset 48
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -48
	.cfi_offset 15, -40
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 0
	stq $16,16($15)
	stt $f17,24($15)
	stt $f18,32($15)
	ldq $1,16($15)
	ldt $f10,24($15)
	stt $f10,0($1)
	ldq $1,16($15)
	ldt $f10,32($15)
	stt $f10,8($1)
	bis $31,$31,$31
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,48($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4410:
	.end _ZNSt25uniform_real_distributionIdE10param_typeC2Edd
	.weak	_ZNSt25uniform_real_distributionIdE10param_typeC1Edd
$_ZNSt25uniform_real_distributionIdE10param_typeC1Edd..ng = $_ZNSt25uniform_real_distributionIdE10param_typeC2Edd..ng
_ZNSt25uniform_real_distributionIdE10param_typeC1Edd = _ZNSt25uniform_real_distributionIdE10param_typeC2Edd
	.section	.text._ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC2Em,"axG",@progbits,_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC5Em,comdat
	.align 2
	.weak	_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC2Em
	.ent _ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC2Em
_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC2Em:
	.eflag 48
	.frame $15,32,$26,0
	.mask 0x4008000,-32
$LFB4413:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!273
	lda $29,0($29)		!gpdisp!273
$_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC2Em..ng:
	lda $30,-32($30)
	.cfi_def_cfa_offset 32
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -32
	.cfi_offset 15, -24
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,16($15)
	stq $17,24($15)
	ldq $17,24($15)
	ldq $16,16($15)
	ldq $27,_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE4seedEm($29)		!literal!274
	jsr $26,($27),_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE4seedEm		!lituse_jsr!274
	ldah $29,0($26)		!gpdisp!275
	lda $29,0($29)		!gpdisp!275
	bis $31,$31,$31
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,32($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4413:
	.end _ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC2Em
	.weak	_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC1Em
$_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC1Em..ng = $_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC2Em..ng
_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC1Em = _ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEC2Em
	.section	.text._ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEEEdRT_RKNS0_10param_typeE,"axG",@progbits,_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEEEdRT_RKNS0_10param_typeE,comdat
	.align 2
	.weak	_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEEEdRT_RKNS0_10param_typeE
	.ent _ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEEEdRT_RKNS0_10param_typeE
_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEEEdRT_RKNS0_10param_typeE:
	.eflag 48
	.frame $15,80,$26,0
	.mask 0x4008000,-80
	.fmask 0xc,-64
$LFB4415:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!276
	lda $29,0($29)		!gpdisp!276
$_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEEEdRT_RKNS0_10param_typeE..ng:
	lda $30,-80($30)
	.cfi_def_cfa_offset 80
	stq $26,0($30)
	stq $15,8($30)
	stt $f2,16($30)
	stt $f3,24($30)
	.cfi_offset 26, -80
	.cfi_offset 15, -72
	.cfi_offset 34, -64
	.cfi_offset 35, -56
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,48($15)
	stq $17,56($15)
	stq $18,64($15)
	ldq $17,56($15)
	lda $16,32($15)
	ldq $27,_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEdEC1ERS2_($29)		!literal!277
	jsr $26,($27),_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEdEC1ERS2_		!lituse_jsr!277
	ldah $29,0($26)		!gpdisp!278
	lda $29,0($29)		!gpdisp!278
	lda $16,32($15)
	ldq $27,_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEdEclEv($29)		!literal!279
	jsr $26,($27),0		!lituse_jsr!279
	ldah $29,0($26)		!gpdisp!280
	lda $29,0($29)		!gpdisp!280
	cpys $f0,$f0,$f3
	ldq $16,64($15)
	ldq $27,_ZNKSt25uniform_real_distributionIdE10param_type1bEv($29)		!literal!281
	jsr $26,($27),0		!lituse_jsr!281
	ldah $29,0($26)		!gpdisp!282
	lda $29,0($29)		!gpdisp!282
	cpys $f0,$f0,$f2
	ldq $16,64($15)
	ldq $27,_ZNKSt25uniform_real_distributionIdE10param_type1aEv($29)		!literal!283
	jsr $26,($27),0		!lituse_jsr!283
	ldah $29,0($26)		!gpdisp!284
	lda $29,0($29)		!gpdisp!284
	cpys $f0,$f0,$f11
	subt/su $f2,$f11,$f10
	trapb
	mult/su $f3,$f10,$f2
	trapb
	ldq $16,64($15)
	ldq $27,_ZNKSt25uniform_real_distributionIdE10param_type1aEv($29)		!literal!285
	jsr $26,($27),0		!lituse_jsr!285
	ldah $29,0($26)		!gpdisp!286
	lda $29,0($29)		!gpdisp!286
	cpys $f0,$f0,$f10
	addt/su $f2,$f10,$f11
	trapb
	cpys $f11,$f11,$f10
	cpys $f10,$f10,$f0
	mov $15,$30
	ldq $26,0($30)
	ldt $f2,16($30)
	ldt $f3,24($30)
	ldq $15,8($30)
	lda $30,80($30)
	.cfi_restore 15
	.cfi_restore 35
	.cfi_restore 34
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4415:
	.end _ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEEEdRT_RKNS0_10param_typeE
	.section	.text._ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC2Ev,"axG",@progbits,_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC5Ev,comdat
	.align 2
	.weak	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC2Ev
	.ent _ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC2Ev
_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC2Ev:
	.eflag 48
	.frame $15,32,$26,0
	.mask 0x4008000,-32
$LFB4427:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!287
	lda $29,0($29)		!gpdisp!287
$_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC2Ev..ng:
	lda $30,-32($30)
	.cfi_def_cfa_offset 32
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -32
	.cfi_offset 15, -24
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,16($15)
	ldq $1,16($15)
	stq $31,0($1)
	ldq $1,16($15)
	lda $1,8($1)
	mov $1,$16
	ldq $27,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1Ev($29)		!literal!288
	jsr $26,($27),_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1Ev		!lituse_jsr!288
	ldah $29,0($26)		!gpdisp!289
	lda $29,0($29)		!gpdisp!289
	bis $31,$31,$31
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,32($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4427:
	.end _ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC2Ev
	.weak	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC1Ev
$_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC1Ev..ng = $_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC2Ev..ng
_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC1Ev = _ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC2Ev
	.section	.text._ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv,"axG",@progbits,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv,comdat
	.align 2
	.weak	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv
	.ent _ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv
_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv:
	.eflag 48
	.frame $15,32,$26,0
	.mask 0x4008000,-32
$LFB4429:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!290
	lda $29,0($29)		!gpdisp!290
$_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv..ng:
	lda $30,-32($30)
	.cfi_def_cfa_offset 32
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -32
	.cfi_offset 15, -24
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,16($15)
	ldq $1,16($15)
	lda $1,8($1)
	lda $17,-1($31)
	mov $1,$16
	ldq $27,_ZN9__gnu_cxxL27__exchange_and_add_dispatchEPii($29)		!literal!291
	jsr $26,($27),0		!lituse_jsr!291
	ldah $29,0($26)		!gpdisp!292
	lda $29,0($29)		!gpdisp!292
	mov $0,$1
	cmpeq $1,1,$1
	and $1,0xff,$1
	beq $1,$L108
	ldq $1,16($15)
	ldq $1,0($1)
	lda $1,16($1)
	ldq $1,0($1)
	ldq $16,16($15)
	mov $1,$27
	jsr $26,($27),0
	ldah $29,0($26)		!gpdisp!293
	lda $29,0($29)		!gpdisp!293
	ldq $1,16($15)
	lda $1,12($1)
	lda $17,-1($31)
	mov $1,$16
	ldq $27,_ZN9__gnu_cxxL27__exchange_and_add_dispatchEPii($29)		!literal!294
	jsr $26,($27),0		!lituse_jsr!294
	ldah $29,0($26)		!gpdisp!295
	lda $29,0($29)		!gpdisp!295
	mov $0,$1
	cmpeq $1,1,$1
	and $1,0xff,$1
	beq $1,$L108
	ldq $1,16($15)
	ldq $1,0($1)
	lda $1,24($1)
	ldq $1,0($1)
	ldq $16,16($15)
	mov $1,$27
	jsr $26,($27),0
	ldah $29,0($26)		!gpdisp!296
	lda $29,0($29)		!gpdisp!296
$L108:
	bis $31,$31,$31
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,32($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4429:
	.end _ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv
	.section	.text._ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD2Ev,"axG",@progbits,_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD5Ev,comdat
	.align 2
	.weak	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD2Ev
	.ent _ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD2Ev
_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD2Ev:
	.eflag 48
	.frame $15,32,$26,0
	.mask 0x4008000,-32
$LFB4432:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!297
	lda $29,0($29)		!gpdisp!297
$_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD2Ev..ng:
	lda $30,-32($30)
	.cfi_def_cfa_offset 32
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -32
	.cfi_offset 15, -24
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,16($15)
	ldq $16,16($15)
	ldq $27,_ZNSaISt10shared_ptrI8hittableEED2Ev($29)		!literal!298
	jsr $26,($27),_ZNSaISt10shared_ptrI8hittableEED2Ev		!lituse_jsr!298
	ldah $29,0($26)		!gpdisp!299
	lda $29,0($29)		!gpdisp!299
	bis $31,$31,$31
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,32($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4432:
	.end _ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD2Ev
	.weak	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD1Ev
$_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD1Ev..ng = $_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD2Ev..ng
_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD1Ev = _ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD2Ev
	.section	.text._ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED2Ev,"axG",@progbits,_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED5Ev,comdat
	.align 2
	.weak	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED2Ev
	.ent _ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED2Ev
_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED2Ev:
	.eflag 48
	.frame $15,32,$26,0
	.mask 0x4008000,-32
$LFB4434:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,$LLSDA4434
	ldah $29,0($27)		!gpdisp!300
	lda $29,0($29)		!gpdisp!300
$_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED2Ev..ng:
	lda $30,-32($30)
	.cfi_def_cfa_offset 32
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -32
	.cfi_offset 15, -24
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,16($15)
	ldq $1,16($15)
	ldq $3,0($1)
	ldq $1,16($15)
	ldq $2,16($1)
	ldq $1,16($15)
	ldq $1,0($1)
	subq $2,$1,$1
	sra $1,4,$1
	mov $1,$18
	mov $3,$17
	ldq $16,16($15)
	ldq $27,_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE13_M_deallocateEPS2_m($29)		!literal!301
	jsr $26,($27),_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE13_M_deallocateEPS2_m		!lituse_jsr!301
	ldah $29,0($26)		!gpdisp!302
	lda $29,0($29)		!gpdisp!302
	ldq $1,16($15)
	mov $1,$16
	ldq $27,_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD1Ev($29)		!literal!303
	jsr $26,($27),_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD1Ev		!lituse_jsr!303
	ldah $29,0($26)		!gpdisp!304
	lda $29,0($29)		!gpdisp!304
	bis $31,$31,$31
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,32($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4434:
	.section	.gcc_except_table
$LLSDA4434:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 $LLSDACSE4434-$LLSDACSB4434
$LLSDACSB4434:
$LLSDACSE4434:
	.section	.text._ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED2Ev,"axG",@progbits,_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED5Ev,comdat
	.end _ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED2Ev
	.weak	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED1Ev
$_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED1Ev..ng = $_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED2Ev..ng
_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED1Ev = _ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED2Ev
	.section	.text._ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE19_M_get_Tp_allocatorEv,"axG",@progbits,_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE19_M_get_Tp_allocatorEv,comdat
	.align 2
	.weak	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE19_M_get_Tp_allocatorEv
	.ent _ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE19_M_get_Tp_allocatorEv
$_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE19_M_get_Tp_allocatorEv..ng:
_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE19_M_get_Tp_allocatorEv:
	.eflag 48
	.frame $15,32,$26,0
	.mask 0x4008000,-32
$LFB4436:
	.cfi_startproc
	lda $30,-32($30)
	.cfi_def_cfa_offset 32
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -32
	.cfi_offset 15, -24
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 0
	stq $16,16($15)
	ldq $1,16($15)
	mov $1,$0
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,32($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4436:
	.end _ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE19_M_get_Tp_allocatorEv
	.section	.text._ZSt8_DestroyIPSt10shared_ptrI8hittableES2_EvT_S4_RSaIT0_E,"axG",@progbits,_ZSt8_DestroyIPSt10shared_ptrI8hittableES2_EvT_S4_RSaIT0_E,comdat
	.align 2
	.weak	_ZSt8_DestroyIPSt10shared_ptrI8hittableES2_EvT_S4_RSaIT0_E
	.ent _ZSt8_DestroyIPSt10shared_ptrI8hittableES2_EvT_S4_RSaIT0_E
_ZSt8_DestroyIPSt10shared_ptrI8hittableES2_EvT_S4_RSaIT0_E:
	.eflag 48
	.frame $15,48,$26,0
	.mask 0x4008000,-48
$LFB4437:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!305
	lda $29,0($29)		!gpdisp!305
$_ZSt8_DestroyIPSt10shared_ptrI8hittableES2_EvT_S4_RSaIT0_E..ng:
	lda $30,-48($30)
	.cfi_def_cfa_offset 48
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -48
	.cfi_offset 15, -40
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,16($15)
	stq $17,24($15)
	stq $18,32($15)
	ldq $17,24($15)
	ldq $16,16($15)
	ldq $27,_ZSt8_DestroyIPSt10shared_ptrI8hittableEEvT_S4_($29)		!literal!306
	jsr $26,($27),_ZSt8_DestroyIPSt10shared_ptrI8hittableEEvT_S4_		!lituse_jsr!306
	ldah $29,0($26)		!gpdisp!307
	lda $29,0($29)		!gpdisp!307
	bis $31,$31,$31
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,48($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4437:
	.end _ZSt8_DestroyIPSt10shared_ptrI8hittableES2_EvT_S4_RSaIT0_E
	.section	.text._ZNSaI6sphereEC2Ev,"axG",@progbits,_ZNSaI6sphereEC5Ev,comdat
	.align 2
	.weak	_ZNSaI6sphereEC2Ev
	.ent _ZNSaI6sphereEC2Ev
_ZNSaI6sphereEC2Ev:
	.eflag 48
	.frame $15,32,$26,0
	.mask 0x4008000,-32
$LFB4439:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!308
	lda $29,0($29)		!gpdisp!308
$_ZNSaI6sphereEC2Ev..ng:
	lda $30,-32($30)
	.cfi_def_cfa_offset 32
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -32
	.cfi_offset 15, -24
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,16($15)
	ldq $16,16($15)
	ldq $27,_ZN9__gnu_cxx13new_allocatorI6sphereEC2Ev($29)		!literal!309
	jsr $26,($27),_ZN9__gnu_cxx13new_allocatorI6sphereEC2Ev		!lituse_jsr!309
	ldah $29,0($26)		!gpdisp!310
	lda $29,0($29)		!gpdisp!310
	bis $31,$31,$31
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,32($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4439:
	.end _ZNSaI6sphereEC2Ev
	.weak	_ZNSaI6sphereEC1Ev
$_ZNSaI6sphereEC1Ev..ng = $_ZNSaI6sphereEC2Ev..ng
_ZNSaI6sphereEC1Ev = _ZNSaI6sphereEC2Ev
	.section	.text._ZNSaI6sphereED2Ev,"axG",@progbits,_ZNSaI6sphereED5Ev,comdat
	.align 2
	.weak	_ZNSaI6sphereED2Ev
	.ent _ZNSaI6sphereED2Ev
_ZNSaI6sphereED2Ev:
	.eflag 48
	.frame $15,32,$26,0
	.mask 0x4008000,-32
$LFB4442:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!311
	lda $29,0($29)		!gpdisp!311
$_ZNSaI6sphereED2Ev..ng:
	lda $30,-32($30)
	.cfi_def_cfa_offset 32
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -32
	.cfi_offset 15, -24
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,16($15)
	ldq $16,16($15)
	ldq $27,_ZN9__gnu_cxx13new_allocatorI6sphereED2Ev($29)		!literal!312
	jsr $26,($27),_ZN9__gnu_cxx13new_allocatorI6sphereED2Ev		!lituse_jsr!312
	ldah $29,0($26)		!gpdisp!313
	lda $29,0($29)		!gpdisp!313
	bis $31,$31,$31
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,32($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4442:
	.end _ZNSaI6sphereED2Ev
	.weak	_ZNSaI6sphereED1Ev
$_ZNSaI6sphereED1Ev..ng = $_ZNSaI6sphereED2Ev..ng
_ZNSaI6sphereED1Ev = _ZNSaI6sphereED2Ev
	.section	.text._ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE,"axG",@progbits,_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE,comdat
	.align 2
	.weak	_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
	.ent _ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
$_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE..ng:
_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE:
	.eflag 48
	.frame $15,32,$26,0
	.mask 0x4008000,-32
$LFB4444:
	.cfi_startproc
	lda $30,-32($30)
	.cfi_def_cfa_offset 32
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -32
	.cfi_offset 15, -24
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 0
	stq $16,16($15)
	ldq $1,16($15)
	mov $1,$0
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,32($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4444:
	.end _ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
	.section	.text._ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE,"axG",@progbits,_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE,comdat
	.align 2
	.weak	_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE
	.ent _ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE
$_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE..ng:
_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE:
	.eflag 48
	.frame $15,32,$26,0
	.mask 0x4008000,-32
$LFB4445:
	.cfi_startproc
	lda $30,-32($30)
	.cfi_def_cfa_offset 32
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -32
	.cfi_offset 15, -24
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 0
	stq $16,16($15)
	ldq $1,16($15)
	mov $1,$0
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,32($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4445:
	.end _ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE
	.section	.text._ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3dEESt10shared_ptrIT_ERKT0_DpOT1_,"axG",@progbits,_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3dEESt10shared_ptrIT_ERKT0_DpOT1_,comdat
	.align 2
	.weak	_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3dEESt10shared_ptrIT_ERKT0_DpOT1_
	.ent _ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3dEESt10shared_ptrIT_ERKT0_DpOT1_
_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3dEESt10shared_ptrIT_ERKT0_DpOT1_:
	.eflag 48
	.frame $15,64,$26,0
	.mask 0x4008600,-64
$LFB4446:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!314
	lda $29,0($29)		!gpdisp!314
$_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3dEESt10shared_ptrIT_ERKT0_DpOT1_..ng:
	lda $30,-64($30)
	.cfi_def_cfa_offset 64
	stq $26,0($30)
	stq $9,8($30)
	stq $10,16($30)
	stq $15,24($30)
	.cfi_offset 26, -64
	.cfi_offset 9, -56
	.cfi_offset 10, -48
	.cfi_offset 15, -40
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,32($15)
	stq $17,40($15)
	stq $18,48($15)
	stq $19,56($15)
	ldq $10,40($15)
	ldq $16,48($15)
	ldq $27,_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE($29)		!literal!315
	jsr $26,($27),0		!lituse_jsr!315
	ldah $29,0($26)		!gpdisp!316
	lda $29,0($29)		!gpdisp!316
	mov $0,$9
	ldq $16,56($15)
	ldq $27,_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE($29)		!literal!317
	jsr $26,($27),0		!lituse_jsr!317
	ldah $29,0($26)		!gpdisp!318
	lda $29,0($29)		!gpdisp!318
	mov $0,$1
	mov $1,$19
	mov $9,$18
	mov $10,$17
	ldq $16,32($15)
	ldq $27,_ZNSt10shared_ptrI6sphereEC1ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_($29)		!literal!319
	jsr $26,($27),_ZNSt10shared_ptrI6sphereEC1ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_		!lituse_jsr!319
	ldah $29,0($26)		!gpdisp!320
	lda $29,0($29)		!gpdisp!320
	ldq $0,32($15)
	mov $15,$30
	ldq $26,0($30)
	ldq $9,8($30)
	ldq $10,16($30)
	ldq $15,24($30)
	lda $30,64($30)
	.cfi_restore 15
	.cfi_restore 10
	.cfi_restore 9
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4446:
	.end _ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3dEESt10shared_ptrIT_ERKT0_DpOT1_
	.section	.text._ZSt4moveIRSt10shared_ptrI6sphereEEONSt16remove_referenceIT_E4typeEOS5_,"axG",@progbits,_ZSt4moveIRSt10shared_ptrI6sphereEEONSt16remove_referenceIT_E4typeEOS5_,comdat
	.align 2
	.weak	_ZSt4moveIRSt10shared_ptrI6sphereEEONSt16remove_referenceIT_E4typeEOS5_
	.ent _ZSt4moveIRSt10shared_ptrI6sphereEEONSt16remove_referenceIT_E4typeEOS5_
$_ZSt4moveIRSt10shared_ptrI6sphereEEONSt16remove_referenceIT_E4typeEOS5_..ng:
_ZSt4moveIRSt10shared_ptrI6sphereEEONSt16remove_referenceIT_E4typeEOS5_:
	.eflag 48
	.frame $15,32,$26,0
	.mask 0x4008000,-32
$LFB4450:
	.cfi_startproc
	lda $30,-32($30)
	.cfi_def_cfa_offset 32
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -32
	.cfi_offset 15, -24
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 0
	stq $16,16($15)
	ldq $1,16($15)
	mov $1,$0
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,32($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4450:
	.end _ZSt4moveIRSt10shared_ptrI6sphereEEONSt16remove_referenceIT_E4typeEOS5_
	.section	.text._ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC2I6spherevEEOS_IT_LS2_2EE,"axG",@progbits,_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC5I6spherevEEOS_IT_LS2_2EE,comdat
	.align 2
	.weak	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC2I6spherevEEOS_IT_LS2_2EE
	.ent _ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC2I6spherevEEOS_IT_LS2_2EE
_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC2I6spherevEEOS_IT_LS2_2EE:
	.eflag 48
	.frame $15,32,$26,0
	.mask 0x4008000,-32
$LFB4452:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!321
	lda $29,0($29)		!gpdisp!321
$_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC2I6spherevEEOS_IT_LS2_2EE..ng:
	lda $30,-32($30)
	.cfi_def_cfa_offset 32
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -32
	.cfi_offset 15, -24
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,16($15)
	stq $17,24($15)
	ldq $1,24($15)
	ldq $2,0($1)
	ldq $1,16($15)
	stq $2,0($1)
	ldq $1,16($15)
	lda $1,8($1)
	mov $1,$16
	ldq $27,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1Ev($29)		!literal!322
	jsr $26,($27),_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1Ev		!lituse_jsr!322
	ldah $29,0($26)		!gpdisp!323
	lda $29,0($29)		!gpdisp!323
	ldq $1,16($15)
	lda $2,8($1)
	ldq $1,24($15)
	lda $1,8($1)
	mov $1,$17
	mov $2,$16
	ldq $27,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EE7_M_swapERS2_($29)		!literal!324
	jsr $26,($27),_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EE7_M_swapERS2_		!lituse_jsr!324
	ldah $29,0($26)		!gpdisp!325
	lda $29,0($29)		!gpdisp!325
	ldq $1,24($15)
	stq $31,0($1)
	bis $31,$31,$31
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,32($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4452:
	.end _ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC2I6spherevEEOS_IT_LS2_2EE
	.weak	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC1I6spherevEEOS_IT_LS2_2EE
$_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC1I6spherevEEOS_IT_LS2_2EE..ng = $_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC2I6spherevEEOS_IT_LS2_2EE..ng
_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC1I6spherevEEOS_IT_LS2_2EE = _ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC2I6spherevEEOS_IT_LS2_2EE
	.section	.text._ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE,"axG",@progbits,_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE,comdat
	.align 2
	.weak	_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE
	.ent _ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE
$_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE..ng:
_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE:
	.eflag 48
	.frame $15,32,$26,0
	.mask 0x4008000,-32
$LFB4458:
	.cfi_startproc
	lda $30,-32($30)
	.cfi_def_cfa_offset 32
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -32
	.cfi_offset 15, -24
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 0
	stq $16,16($15)
	ldq $1,16($15)
	mov $1,$0
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,32($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4458:
	.end _ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE
	.section	.text._ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3iEESt10shared_ptrIT_ERKT0_DpOT1_,"axG",@progbits,_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3iEESt10shared_ptrIT_ERKT0_DpOT1_,comdat
	.align 2
	.weak	_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3iEESt10shared_ptrIT_ERKT0_DpOT1_
	.ent _ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3iEESt10shared_ptrIT_ERKT0_DpOT1_
_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3iEESt10shared_ptrIT_ERKT0_DpOT1_:
	.eflag 48
	.frame $15,64,$26,0
	.mask 0x4008600,-64
$LFB4459:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!326
	lda $29,0($29)		!gpdisp!326
$_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3iEESt10shared_ptrIT_ERKT0_DpOT1_..ng:
	lda $30,-64($30)
	.cfi_def_cfa_offset 64
	stq $26,0($30)
	stq $9,8($30)
	stq $10,16($30)
	stq $15,24($30)
	.cfi_offset 26, -64
	.cfi_offset 9, -56
	.cfi_offset 10, -48
	.cfi_offset 15, -40
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,32($15)
	stq $17,40($15)
	stq $18,48($15)
	stq $19,56($15)
	ldq $10,40($15)
	ldq $16,48($15)
	ldq $27,_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE($29)		!literal!327
	jsr $26,($27),0		!lituse_jsr!327
	ldah $29,0($26)		!gpdisp!328
	lda $29,0($29)		!gpdisp!328
	mov $0,$9
	ldq $16,56($15)
	ldq $27,_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE($29)		!literal!329
	jsr $26,($27),0		!lituse_jsr!329
	ldah $29,0($26)		!gpdisp!330
	lda $29,0($29)		!gpdisp!330
	mov $0,$1
	mov $1,$19
	mov $9,$18
	mov $10,$17
	ldq $16,32($15)
	ldq $27,_ZNSt10shared_ptrI6sphereEC1ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_($29)		!literal!331
	jsr $26,($27),_ZNSt10shared_ptrI6sphereEC1ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_		!lituse_jsr!331
	ldah $29,0($26)		!gpdisp!332
	lda $29,0($29)		!gpdisp!332
	ldq $0,32($15)
	mov $15,$30
	ldq $26,0($30)
	ldq $9,8($30)
	ldq $10,16($30)
	ldq $15,24($30)
	lda $30,64($30)
	.cfi_restore 15
	.cfi_restore 10
	.cfi_restore 9
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4459:
	.end _ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3iEESt10shared_ptrIT_ERKT0_DpOT1_
	.section	.text._ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE4seedEm,"axG",@progbits,_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE4seedEm,comdat
	.align 2
	.weak	_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE4seedEm
	.ent _ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE4seedEm
_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE4seedEm:
	.eflag 48
	.frame $15,48,$26,0
	.mask 0x4008000,-48
$LFB4514:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!333
	lda $29,0($29)		!gpdisp!333
$_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE4seedEm..ng:
	lda $30,-48($30)
	.cfi_def_cfa_offset 48
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -48
	.cfi_offset 15, -40
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,32($15)
	stq $17,40($15)
	ldq $16,40($15)
	ldq $27,_ZNSt8__detail5__modImLm4294967296ELm1ELm0EEET_S1_($29)		!literal!334
	jsr $26,($27),0		!lituse_jsr!334
	ldah $29,0($26)		!gpdisp!335
	lda $29,0($29)		!gpdisp!335
	mov $0,$1
	ldq $2,32($15)
	stq $1,0($2)
	lda $1,1($31)
	stq $1,16($15)
$L131:
	ldq $1,16($15)
	lda $2,623($31)
	cmpule $1,$2,$1
	beq $1,$L130
	ldq $1,16($15)
	lda $1,-1($1)
	ldq $2,32($15)
	s8addq $1,0,$1
	addq $2,$1,$1
	ldq $1,0($1)
	stq $1,24($15)
	ldq $1,24($15)
	srl $1,30,$1
	ldq $2,24($15)
	xor $2,$1,$1
	stq $1,24($15)
	ldq $3,24($15)
	mov $3,$2
	sll $2,12,$1
	mov $1,$2
	addq $2,$3,$2
	s8addq $2,0,$1
	mov $1,$2
	addq $2,$3,$2
	sll $2,4,$1
	mov $1,$2
	subq $2,$3,$2
	s4addq $2,0,$1
	subq $1,$2,$1
	s4addq $1,0,$1
	subq $1,$3,$1
	s8addq $1,0,$2
	addq $1,$2,$1
	s8addq $1,0,$1
	addq $1,$3,$1
	s4addq $1,0,$1
	addq $1,$3,$1
	stq $1,24($15)
	ldq $16,16($15)
	ldq $27,_ZNSt8__detail5__modImLm624ELm1ELm0EEET_S1_($29)		!literal!336
	jsr $26,($27),0		!lituse_jsr!336
	ldah $29,0($26)		!gpdisp!337
	lda $29,0($29)		!gpdisp!337
	mov $0,$1
	ldq $2,24($15)
	addq $2,$1,$1
	stq $1,24($15)
	ldq $16,24($15)
	ldq $27,_ZNSt8__detail5__modImLm4294967296ELm1ELm0EEET_S1_($29)		!literal!338
	jsr $26,($27),0		!lituse_jsr!338
	ldah $29,0($26)		!gpdisp!339
	lda $29,0($29)		!gpdisp!339
	mov $0,$1
	ldq $3,32($15)
	ldq $2,16($15)
	s8addq $2,0,$2
	addq $3,$2,$2
	stq $1,0($2)
	ldq $1,16($15)
	lda $1,1($1)
	stq $1,16($15)
	br $31,$L131
$L130:
	ldq $1,32($15)
	lda $2,624($31)
	stq $2,4992($1)
	bis $31,$31,$31
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,48($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4514:
	.end _ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE4seedEm
	.section	.text._ZNSt8__detail8_AdaptorISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEdEC2ERS2_,"axG",@progbits,_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEdEC5ERS2_,comdat
	.align 2
	.weak	_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEdEC2ERS2_
	.ent _ZNSt8__detail8_AdaptorISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEdEC2ERS2_
$_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEdEC2ERS2_..ng:
_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEdEC2ERS2_:
	.eflag 48
	.frame $15,32,$26,0
	.mask 0x4008000,-32
$LFB4516:
	.cfi_startproc
	lda $30,-32($30)
	.cfi_def_cfa_offset 32
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -32
	.cfi_offset 15, -24
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 0
	stq $16,16($15)
	stq $17,24($15)
	ldq $1,16($15)
	ldq $2,24($15)
	stq $2,0($1)
	bis $31,$31,$31
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,32($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4516:
	.end _ZNSt8__detail8_AdaptorISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEdEC2ERS2_
	.weak	_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEdEC1ERS2_
$_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEdEC1ERS2_..ng = $_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEdEC2ERS2_..ng
_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEdEC1ERS2_ = _ZNSt8__detail8_AdaptorISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEdEC2ERS2_
	.section	.text._ZNSt8__detail8_AdaptorISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEdEclEv,"axG",@progbits,_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEdEclEv,comdat
	.align 2
	.weak	_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEdEclEv
	.ent _ZNSt8__detail8_AdaptorISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEdEclEv
_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEdEclEv:
	.eflag 48
	.frame $15,32,$26,0
	.mask 0x4008000,-32
$LFB4518:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!340
	lda $29,0($29)		!gpdisp!340
$_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEdEclEv..ng:
	lda $30,-32($30)
	.cfi_def_cfa_offset 32
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -32
	.cfi_offset 15, -24
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,16($15)
	ldq $1,16($15)
	ldq $1,0($1)
	mov $1,$16
	ldq $27,_ZSt18generate_canonicalIdLm53ESt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEET_RT1_($29)		!literal!341
	jsr $26,($27),0		!lituse_jsr!341
	ldah $29,0($26)		!gpdisp!342
	lda $29,0($29)		!gpdisp!342
	cpys $f0,$f0,$f10
	cpys $f10,$f10,$f0
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,32($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4518:
	.end _ZNSt8__detail8_AdaptorISt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEdEclEv
	.section	.text._ZNKSt25uniform_real_distributionIdE10param_type1bEv,"axG",@progbits,_ZNKSt25uniform_real_distributionIdE10param_type1bEv,comdat
	.align 2
	.weak	_ZNKSt25uniform_real_distributionIdE10param_type1bEv
	.ent _ZNKSt25uniform_real_distributionIdE10param_type1bEv
$_ZNKSt25uniform_real_distributionIdE10param_type1bEv..ng:
_ZNKSt25uniform_real_distributionIdE10param_type1bEv:
	.eflag 48
	.frame $15,32,$26,0
	.mask 0x4008000,-32
$LFB4519:
	.cfi_startproc
	lda $30,-32($30)
	.cfi_def_cfa_offset 32
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -32
	.cfi_offset 15, -24
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 0
	stq $16,16($15)
	ldq $1,16($15)
	ldt $f10,8($1)
	cpys $f10,$f10,$f0
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,32($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4519:
	.end _ZNKSt25uniform_real_distributionIdE10param_type1bEv
	.section	.text._ZNKSt25uniform_real_distributionIdE10param_type1aEv,"axG",@progbits,_ZNKSt25uniform_real_distributionIdE10param_type1aEv,comdat
	.align 2
	.weak	_ZNKSt25uniform_real_distributionIdE10param_type1aEv
	.ent _ZNKSt25uniform_real_distributionIdE10param_type1aEv
$_ZNKSt25uniform_real_distributionIdE10param_type1aEv..ng:
_ZNKSt25uniform_real_distributionIdE10param_type1aEv:
	.eflag 48
	.frame $15,32,$26,0
	.mask 0x4008000,-32
$LFB4520:
	.cfi_startproc
	lda $30,-32($30)
	.cfi_def_cfa_offset 32
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -32
	.cfi_offset 15, -24
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 0
	stq $16,16($15)
	ldq $1,16($15)
	ldt $f10,0($1)
	cpys $f10,$f10,$f0
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,32($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4520:
	.end _ZNKSt25uniform_real_distributionIdE10param_type1aEv
	.section	.text._ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2Ev,"axG",@progbits,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC5Ev,comdat
	.align 2
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2Ev
	.ent _ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2Ev
$_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2Ev..ng:
_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2Ev:
	.eflag 48
	.frame $15,32,$26,0
	.mask 0x4008000,-32
$LFB4526:
	.cfi_startproc
	lda $30,-32($30)
	.cfi_def_cfa_offset 32
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -32
	.cfi_offset 15, -24
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 0
	stq $16,16($15)
	ldq $1,16($15)
	stq $31,0($1)
	bis $31,$31,$31
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,32($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4526:
	.end _ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2Ev
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1Ev
$_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1Ev..ng = $_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2Ev..ng
_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1Ev = _ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2Ev
	.section	.text._ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,"axG",@progbits,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,comdat
	.align 2
	.weak	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.ent _ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv:
	.eflag 48
	.frame $15,32,$26,0
	.mask 0x4008000,-32
$LFB4528:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!343
	lda $29,0($29)		!gpdisp!343
$_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv..ng:
	lda $30,-32($30)
	.cfi_def_cfa_offset 32
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -32
	.cfi_offset 15, -24
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,16($15)
	ldq $1,16($15)
	beq $1,$L142
	ldq $1,16($15)
	ldq $1,0($1)
	lda $1,8($1)
	ldq $1,0($1)
	ldq $16,16($15)
	mov $1,$27
	jsr $26,($27),0
	ldah $29,0($26)		!gpdisp!344
	lda $29,0($29)		!gpdisp!344
$L142:
	bis $31,$31,$31
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,32($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4528:
	.end _ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.section	.text._ZNSaISt10shared_ptrI8hittableEED2Ev,"axG",@progbits,_ZNSaISt10shared_ptrI8hittableEED5Ev,comdat
	.align 2
	.weak	_ZNSaISt10shared_ptrI8hittableEED2Ev
	.ent _ZNSaISt10shared_ptrI8hittableEED2Ev
_ZNSaISt10shared_ptrI8hittableEED2Ev:
	.eflag 48
	.frame $15,32,$26,0
	.mask 0x4008000,-32
$LFB4530:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!345
	lda $29,0($29)		!gpdisp!345
$_ZNSaISt10shared_ptrI8hittableEED2Ev..ng:
	lda $30,-32($30)
	.cfi_def_cfa_offset 32
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -32
	.cfi_offset 15, -24
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,16($15)
	ldq $16,16($15)
	ldq $27,_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED2Ev($29)		!literal!346
	jsr $26,($27),_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED2Ev		!lituse_jsr!346
	ldah $29,0($26)		!gpdisp!347
	lda $29,0($29)		!gpdisp!347
	bis $31,$31,$31
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,32($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4530:
	.end _ZNSaISt10shared_ptrI8hittableEED2Ev
	.weak	_ZNSaISt10shared_ptrI8hittableEED1Ev
$_ZNSaISt10shared_ptrI8hittableEED1Ev..ng = $_ZNSaISt10shared_ptrI8hittableEED2Ev..ng
_ZNSaISt10shared_ptrI8hittableEED1Ev = _ZNSaISt10shared_ptrI8hittableEED2Ev
	.section	.text._ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE13_M_deallocateEPS2_m,"axG",@progbits,_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE13_M_deallocateEPS2_m,comdat
	.align 2
	.weak	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE13_M_deallocateEPS2_m
	.ent _ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE13_M_deallocateEPS2_m
_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE13_M_deallocateEPS2_m:
	.eflag 48
	.frame $15,48,$26,0
	.mask 0x4008000,-48
$LFB4532:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!348
	lda $29,0($29)		!gpdisp!348
$_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE13_M_deallocateEPS2_m..ng:
	lda $30,-48($30)
	.cfi_def_cfa_offset 48
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -48
	.cfi_offset 15, -40
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,16($15)
	stq $17,24($15)
	stq $18,32($15)
	ldq $1,24($15)
	beq $1,$L146
	ldq $1,16($15)
	ldq $18,32($15)
	ldq $17,24($15)
	mov $1,$16
	ldq $27,_ZNSt16allocator_traitsISaISt10shared_ptrI8hittableEEE10deallocateERS3_PS2_m($29)		!literal!349
	jsr $26,($27),_ZNSt16allocator_traitsISaISt10shared_ptrI8hittableEEE10deallocateERS3_PS2_m		!lituse_jsr!349
	ldah $29,0($26)		!gpdisp!350
	lda $29,0($29)		!gpdisp!350
$L146:
	bis $31,$31,$31
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,48($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4532:
	.end _ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE13_M_deallocateEPS2_m
	.section	.text._ZSt8_DestroyIPSt10shared_ptrI8hittableEEvT_S4_,"axG",@progbits,_ZSt8_DestroyIPSt10shared_ptrI8hittableEEvT_S4_,comdat
	.align 2
	.weak	_ZSt8_DestroyIPSt10shared_ptrI8hittableEEvT_S4_
	.ent _ZSt8_DestroyIPSt10shared_ptrI8hittableEEvT_S4_
_ZSt8_DestroyIPSt10shared_ptrI8hittableEEvT_S4_:
	.eflag 48
	.frame $15,32,$26,0
	.mask 0x4008000,-32
$LFB4533:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!351
	lda $29,0($29)		!gpdisp!351
$_ZSt8_DestroyIPSt10shared_ptrI8hittableEEvT_S4_..ng:
	lda $30,-32($30)
	.cfi_def_cfa_offset 32
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -32
	.cfi_offset 15, -24
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,16($15)
	stq $17,24($15)
	ldq $17,24($15)
	ldq $16,16($15)
	ldq $27,_ZNSt12_Destroy_auxILb0EE9__destroyIPSt10shared_ptrI8hittableEEEvT_S6_($29)		!literal!352
	jsr $26,($27),_ZNSt12_Destroy_auxILb0EE9__destroyIPSt10shared_ptrI8hittableEEEvT_S6_		!lituse_jsr!352
	ldah $29,0($26)		!gpdisp!353
	lda $29,0($29)		!gpdisp!353
	bis $31,$31,$31
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,32($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4533:
	.end _ZSt8_DestroyIPSt10shared_ptrI8hittableEEvT_S4_
	.section	.text._ZN9__gnu_cxx13new_allocatorI6sphereEC2Ev,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorI6sphereEC5Ev,comdat
	.align 2
	.weak	_ZN9__gnu_cxx13new_allocatorI6sphereEC2Ev
	.ent _ZN9__gnu_cxx13new_allocatorI6sphereEC2Ev
$_ZN9__gnu_cxx13new_allocatorI6sphereEC2Ev..ng:
_ZN9__gnu_cxx13new_allocatorI6sphereEC2Ev:
	.eflag 48
	.frame $15,32,$26,0
	.mask 0x4008000,-32
$LFB4535:
	.cfi_startproc
	lda $30,-32($30)
	.cfi_def_cfa_offset 32
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -32
	.cfi_offset 15, -24
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 0
	stq $16,16($15)
	bis $31,$31,$31
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,32($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4535:
	.end _ZN9__gnu_cxx13new_allocatorI6sphereEC2Ev
	.weak	_ZN9__gnu_cxx13new_allocatorI6sphereEC1Ev
$_ZN9__gnu_cxx13new_allocatorI6sphereEC1Ev..ng = $_ZN9__gnu_cxx13new_allocatorI6sphereEC2Ev..ng
_ZN9__gnu_cxx13new_allocatorI6sphereEC1Ev = _ZN9__gnu_cxx13new_allocatorI6sphereEC2Ev
	.section	.text._ZN9__gnu_cxx13new_allocatorI6sphereED2Ev,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorI6sphereED5Ev,comdat
	.align 2
	.weak	_ZN9__gnu_cxx13new_allocatorI6sphereED2Ev
	.ent _ZN9__gnu_cxx13new_allocatorI6sphereED2Ev
$_ZN9__gnu_cxx13new_allocatorI6sphereED2Ev..ng:
_ZN9__gnu_cxx13new_allocatorI6sphereED2Ev:
	.eflag 48
	.frame $15,32,$26,0
	.mask 0x4008000,-32
$LFB4538:
	.cfi_startproc
	lda $30,-32($30)
	.cfi_def_cfa_offset 32
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -32
	.cfi_offset 15, -24
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 0
	stq $16,16($15)
	bis $31,$31,$31
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,32($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4538:
	.end _ZN9__gnu_cxx13new_allocatorI6sphereED2Ev
	.weak	_ZN9__gnu_cxx13new_allocatorI6sphereED1Ev
$_ZN9__gnu_cxx13new_allocatorI6sphereED1Ev..ng = $_ZN9__gnu_cxx13new_allocatorI6sphereED2Ev..ng
_ZN9__gnu_cxx13new_allocatorI6sphereED1Ev = _ZN9__gnu_cxx13new_allocatorI6sphereED2Ev
	.section	.text._ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,"axG",@progbits,_ZNSt10shared_ptrI6sphereEC5ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,comdat
	.align 2
	.weak	_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.ent _ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_:
	.eflag 48
	.frame $15,64,$26,0
	.mask 0x4008600,-64
$LFB4541:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!354
	lda $29,0($29)		!gpdisp!354
$_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_..ng:
	lda $30,-64($30)
	.cfi_def_cfa_offset 64
	stq $26,0($30)
	stq $9,8($30)
	stq $10,16($30)
	stq $15,24($30)
	.cfi_offset 26, -64
	.cfi_offset 9, -56
	.cfi_offset 10, -48
	.cfi_offset 15, -40
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,32($15)
	stq $17,40($15)
	stq $18,48($15)
	stq $19,56($15)
	ldq $10,32($15)
	ldq $16,48($15)
	ldq $27,_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE($29)		!literal!355
	jsr $26,($27),0		!lituse_jsr!355
	ldah $29,0($26)		!gpdisp!356
	lda $29,0($29)		!gpdisp!356
	mov $0,$9
	ldq $16,56($15)
	ldq $27,_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE($29)		!literal!357
	jsr $26,($27),0		!lituse_jsr!357
	ldah $29,0($26)		!gpdisp!358
	lda $29,0($29)		!gpdisp!358
	mov $0,$1
	mov $1,$19
	mov $9,$18
	ldq $17,40($15)
	mov $10,$16
	ldq $27,_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_($29)		!literal!359
	jsr $26,($27),_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_		!lituse_jsr!359
	ldah $29,0($26)		!gpdisp!360
	lda $29,0($29)		!gpdisp!360
	bis $31,$31,$31
	mov $15,$30
	ldq $26,0($30)
	ldq $9,8($30)
	ldq $10,16($30)
	ldq $15,24($30)
	lda $30,64($30)
	.cfi_restore 15
	.cfi_restore 10
	.cfi_restore 9
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4541:
	.end _ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.weak	_ZNSt10shared_ptrI6sphereEC1ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
$_ZNSt10shared_ptrI6sphereEC1ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_..ng = $_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_..ng
_ZNSt10shared_ptrI6sphereEC1ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_ = _ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.section	.text._ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EE7_M_swapERS2_,"axG",@progbits,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EE7_M_swapERS2_,comdat
	.align 2
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EE7_M_swapERS2_
	.ent _ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EE7_M_swapERS2_
$_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EE7_M_swapERS2_..ng:
_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EE7_M_swapERS2_:
	.eflag 48
	.frame $15,48,$26,0
	.mask 0x4008000,-48
$LFB4546:
	.cfi_startproc
	lda $30,-48($30)
	.cfi_def_cfa_offset 48
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -48
	.cfi_offset 15, -40
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 0
	stq $16,32($15)
	stq $17,40($15)
	ldq $1,40($15)
	ldq $1,0($1)
	stq $1,16($15)
	ldq $1,32($15)
	ldq $2,0($1)
	ldq $1,40($15)
	stq $2,0($1)
	ldq $1,32($15)
	ldq $2,16($15)
	stq $2,0($1)
	bis $31,$31,$31
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,48($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4546:
	.end _ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EE7_M_swapERS2_
	.section	.text._ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,"axG",@progbits,_ZNSt10shared_ptrI6sphereEC5ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,comdat
	.align 2
	.weak	_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.ent _ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_:
	.eflag 48
	.frame $15,64,$26,0
	.mask 0x4008600,-64
$LFB4548:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!361
	lda $29,0($29)		!gpdisp!361
$_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_..ng:
	lda $30,-64($30)
	.cfi_def_cfa_offset 64
	stq $26,0($30)
	stq $9,8($30)
	stq $10,16($30)
	stq $15,24($30)
	.cfi_offset 26, -64
	.cfi_offset 9, -56
	.cfi_offset 10, -48
	.cfi_offset 15, -40
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,32($15)
	stq $17,40($15)
	stq $18,48($15)
	stq $19,56($15)
	ldq $10,32($15)
	ldq $16,48($15)
	ldq $27,_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE($29)		!literal!362
	jsr $26,($27),0		!lituse_jsr!362
	ldah $29,0($26)		!gpdisp!363
	lda $29,0($29)		!gpdisp!363
	mov $0,$9
	ldq $16,56($15)
	ldq $27,_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE($29)		!literal!364
	jsr $26,($27),0		!lituse_jsr!364
	ldah $29,0($26)		!gpdisp!365
	lda $29,0($29)		!gpdisp!365
	mov $0,$1
	mov $1,$19
	mov $9,$18
	ldq $17,40($15)
	mov $10,$16
	ldq $27,_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_($29)		!literal!366
	jsr $26,($27),_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_		!lituse_jsr!366
	ldah $29,0($26)		!gpdisp!367
	lda $29,0($29)		!gpdisp!367
	bis $31,$31,$31
	mov $15,$30
	ldq $26,0($30)
	ldq $9,8($30)
	ldq $10,16($30)
	ldq $15,24($30)
	lda $30,64($30)
	.cfi_restore 15
	.cfi_restore 10
	.cfi_restore 9
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4548:
	.end _ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.weak	_ZNSt10shared_ptrI6sphereEC1ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
$_ZNSt10shared_ptrI6sphereEC1ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_..ng = $_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_..ng
_ZNSt10shared_ptrI6sphereEC1ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_ = _ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.section	.text._ZNSt8__detail5__modImLm4294967296ELm1ELm0EEET_S1_,"axG",@progbits,_ZNSt8__detail5__modImLm4294967296ELm1ELm0EEET_S1_,comdat
	.align 2
	.weak	_ZNSt8__detail5__modImLm4294967296ELm1ELm0EEET_S1_
	.ent _ZNSt8__detail5__modImLm4294967296ELm1ELm0EEET_S1_
_ZNSt8__detail5__modImLm4294967296ELm1ELm0EEET_S1_:
	.eflag 48
	.frame $15,32,$26,0
	.mask 0x4008000,-32
$LFB4587:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!368
	lda $29,0($29)		!gpdisp!368
$_ZNSt8__detail5__modImLm4294967296ELm1ELm0EEET_S1_..ng:
	lda $30,-32($30)
	.cfi_def_cfa_offset 32
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -32
	.cfi_offset 15, -24
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,16($15)
	ldq $16,16($15)
	ldq $27,_ZNSt8__detail4_ModImLm4294967296ELm1ELm0ELb1ELb1EE6__calcEm($29)		!literal!369
	jsr $26,($27),0		!lituse_jsr!369
	ldah $29,0($26)		!gpdisp!370
	lda $29,0($29)		!gpdisp!370
	mov $0,$1
	mov $1,$0
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,32($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4587:
	.end _ZNSt8__detail5__modImLm4294967296ELm1ELm0EEET_S1_
	.section	.text._ZNSt8__detail5__modImLm624ELm1ELm0EEET_S1_,"axG",@progbits,_ZNSt8__detail5__modImLm624ELm1ELm0EEET_S1_,comdat
	.align 2
	.weak	_ZNSt8__detail5__modImLm624ELm1ELm0EEET_S1_
	.ent _ZNSt8__detail5__modImLm624ELm1ELm0EEET_S1_
_ZNSt8__detail5__modImLm624ELm1ELm0EEET_S1_:
	.eflag 48
	.frame $15,32,$26,0
	.mask 0x4008000,-32
$LFB4588:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!371
	lda $29,0($29)		!gpdisp!371
$_ZNSt8__detail5__modImLm624ELm1ELm0EEET_S1_..ng:
	lda $30,-32($30)
	.cfi_def_cfa_offset 32
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -32
	.cfi_offset 15, -24
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,16($15)
	ldq $16,16($15)
	ldq $27,_ZNSt8__detail4_ModImLm624ELm1ELm0ELb1ELb1EE6__calcEm($29)		!literal!372
	jsr $26,($27),0		!lituse_jsr!372
	ldah $29,0($26)		!gpdisp!373
	lda $29,0($29)		!gpdisp!373
	mov $0,$1
	mov $1,$0
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,32($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4588:
	.end _ZNSt8__detail5__modImLm624ELm1ELm0EEET_S1_
	.section	.text._ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE3minEv,"axG",@progbits,_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE3minEv,comdat
	.align 2
	.weak	_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE3minEv
	.ent _ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE3minEv
$_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE3minEv..ng:
_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE3minEv:
	.eflag 48
	.frame $15,16,$26,0
	.mask 0x4008000,-16
$LFB4592:
	.cfi_startproc
	lda $30,-16($30)
	.cfi_def_cfa_offset 16
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -16
	.cfi_offset 15, -8
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 0
	mov $31,$1
	mov $1,$0
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,16($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4592:
	.end _ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE3minEv
	.section	.text._ZSt18generate_canonicalIdLm53ESt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEET_RT1_,"axG",@progbits,_ZSt18generate_canonicalIdLm53ESt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEET_RT1_,comdat
	.align 2
	.weak	_ZSt18generate_canonicalIdLm53ESt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEET_RT1_
	.ent _ZSt18generate_canonicalIdLm53ESt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEET_RT1_
_ZSt18generate_canonicalIdLm53ESt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEET_RT1_:
	.eflag 48
	.frame $15,128,$26,0
	.mask 0x4008200,-128
$LFB4589:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!374
	lda $29,0($29)		!gpdisp!374
$_ZSt18generate_canonicalIdLm53ESt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEET_RT1_..ng:
	lda $30,-128($30)
	.cfi_def_cfa_offset 128
	stq $26,0($30)
	stq $9,8($30)
	stq $15,16($30)
	.cfi_offset 26, -128
	.cfi_offset 9, -120
	.cfi_offset 15, -112
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,112($15)
	lda $1,53($31)
	stq $1,64($15)
	ldah $1,$LC15($29)		!gprelhigh
	lda $1,$LC15($1)		!gprellow
	ldq $2,0($1)
	ldq $3,8($1)
	stq $2,80($15)
	stq $3,88($15)
	lda $1,32($31)
	stq $1,96($15)
	lda $1,2($31)
	stq $1,104($15)
	stt $f31,40($15)
	ldah $1,$LC0($29)		!gprelhigh
	ldt $f10,$LC0($1)		!gprellow
	stt $f10,48($15)
	lda $1,2($31)
	stq $1,56($15)
$L163:
	ldq $1,56($15)
	beq $1,$L160
	ldq $16,112($15)
	ldq $27,_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEclEv($29)		!literal!375
	jsr $26,($27),0		!lituse_jsr!375
	ldah $29,0($26)		!gpdisp!376
	lda $29,0($29)		!gpdisp!376
	mov $0,$9
	ldq $27,_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE3minEv($29)		!literal!377
	jsr $26,($27),0		!lituse_jsr!377
	ldah $29,0($26)		!gpdisp!378
	lda $29,0($29)		!gpdisp!378
	mov $0,$1
	subq $9,$1,$1
	lda $2,-1($31)
	cmple $1,$2,$2
	bne $2,$L161
	stq $1,120($15)
	ldt $f11,120($15)
	cvtqt $f11,$f10
	trapb
	br $31,$L162
$L161:
	srl $1,1,$2
	and $1,1,$1
	bis $2,$1,$2
	stq $2,120($15)
	ldt $f10,120($15)
	cvtqt $f10,$f11
	trapb
	addt/su $f11,$f11,$f10
	trapb
$L162:
	ldt $f12,48($15)
	mult/su $f10,$f12,$f11
	trapb
	ldt $f12,40($15)
	addt/su $f12,$f11,$f10
	trapb
	stt $f10,40($15)
	ldt $f10,48($15)
	cpys $f10,$f10,$f16
	ldq $27,_OtsConvertFloatTX($29)		!literal!379
	jsr $26,($27),0		!lituse_jsr!379
	ldah $29,0($26)		!gpdisp!380
	lda $29,0($29)		!gpdisp!380
	mov $16,$4
	mov $17,$5
	ldah $1,$LC15($29)		!gprelhigh
	lda $1,$LC15($1)		!gprellow
	ldq $2,0($1)
	ldq $3,8($1)
	mov $4,$16
	mov $5,$17
	mov $2,$18
	mov $3,$19
	lda $20,2($31)
	ldq $27,_OtsMulX($29)		!literal!381
	jsr $26,($27),0		!lituse_jsr!381
	ldah $29,0($26)		!gpdisp!382
	lda $29,0($29)		!gpdisp!382
	mov $16,$2
	mov $17,$3
	mov $2,$16
	mov $3,$17
	lda $18,2($31)
	ldq $27,_OtsConvertFloatXT($29)		!literal!383
	jsr $26,($27),0		!lituse_jsr!383
	ldah $29,0($26)		!gpdisp!384
	lda $29,0($29)		!gpdisp!384
	cpys $f0,$f0,$f10
	stt $f10,48($15)
	ldq $1,56($15)
	lda $1,-1($1)
	stq $1,56($15)
	br $31,$L163
$L160:
	ldt $f12,40($15)
	ldt $f11,48($15)
	divt/su $f12,$f11,$f10
	trapb
	stt $f10,32($15)
	lda $1,1($31)
	ldt $f11,32($15)
	ldah $2,$LC0($29)		!gprelhigh
	ldt $f12,$LC0($2)		!gprellow
	cmptle/su $f12,$f11,$f10
	trapb
	fbne $f10,$L164
	bis $31,$31,$1
$L164:
	and $1,0xff,$1
	beq $1,$L165
	ldah $1,$LC16($29)		!gprelhigh
	ldt $f10,$LC16($1)		!gprellow
	stt $f10,32($15)
$L165:
	ldt $f10,32($15)
	cpys $f10,$f10,$f0
	mov $15,$30
	ldq $26,0($30)
	ldq $9,8($30)
	ldq $15,16($30)
	lda $30,128($30)
	.cfi_restore 15
	.cfi_restore 9
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4589:
	.end _ZSt18generate_canonicalIdLm53ESt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEET_RT1_
	.section	.text._ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.weak	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev
	.ent _ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev
_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev:
	.eflag 48
	.frame $15,32,$26,0
	.mask 0x4008000,-32
$LFB4599:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!385
	lda $29,0($29)		!gpdisp!385
$_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev..ng:
	lda $30,-32($30)
	.cfi_def_cfa_offset 32
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -32
	.cfi_offset 15, -24
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,16($15)
	ldq $1,_ZTVSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE($29)		!literal
	lda $2,16($1)
	ldq $1,16($15)
	stq $2,0($1)
	bis $31,$31,$31
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,32($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4599:
	.end _ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED1Ev
$_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED1Ev..ng = $_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev..ng
_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED1Ev = _ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED0Ev,"axG",@progbits,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.weak	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED0Ev
	.ent _ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED0Ev
_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED0Ev:
	.eflag 48
	.frame $15,32,$26,0
	.mask 0x4008000,-32
$LFB4601:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!386
	lda $29,0($29)		!gpdisp!386
$_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED0Ev..ng:
	lda $30,-32($30)
	.cfi_def_cfa_offset 32
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -32
	.cfi_offset 15, -24
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,16($15)
	ldq $16,16($15)
	ldq $27,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED1Ev($29)		!literal!387
	jsr $26,($27),_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED1Ev		!lituse_jsr!387
	ldah $29,0($26)		!gpdisp!388
	lda $29,0($29)		!gpdisp!388
	lda $17,16($31)
	ldq $16,16($15)
	ldq $27,_ZdlPvm($29)		!literal!389
	jsr $26,($27),_ZdlPvm		!lituse_jsr!389
	ldah $29,0($26)		!gpdisp!390
	lda $29,0($29)		!gpdisp!390
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,32($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4601:
	.end _ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED0Ev
	.section	.text._ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED2Ev,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED5Ev,comdat
	.align 2
	.weak	_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED2Ev
	.ent _ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED2Ev
$_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED2Ev..ng:
_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED2Ev:
	.eflag 48
	.frame $15,32,$26,0
	.mask 0x4008000,-32
$LFB4603:
	.cfi_startproc
	lda $30,-32($30)
	.cfi_def_cfa_offset 32
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -32
	.cfi_offset 15, -24
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 0
	stq $16,16($15)
	bis $31,$31,$31
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,32($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4603:
	.end _ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED2Ev
	.weak	_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED1Ev
$_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED1Ev..ng = $_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED2Ev..ng
_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED1Ev = _ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED2Ev
	.section	.text._ZNSt16allocator_traitsISaISt10shared_ptrI8hittableEEE10deallocateERS3_PS2_m,"axG",@progbits,_ZNSt16allocator_traitsISaISt10shared_ptrI8hittableEEE10deallocateERS3_PS2_m,comdat
	.align 2
	.weak	_ZNSt16allocator_traitsISaISt10shared_ptrI8hittableEEE10deallocateERS3_PS2_m
	.ent _ZNSt16allocator_traitsISaISt10shared_ptrI8hittableEEE10deallocateERS3_PS2_m
_ZNSt16allocator_traitsISaISt10shared_ptrI8hittableEEE10deallocateERS3_PS2_m:
	.eflag 48
	.frame $15,48,$26,0
	.mask 0x4008000,-48
$LFB4605:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!391
	lda $29,0($29)		!gpdisp!391
$_ZNSt16allocator_traitsISaISt10shared_ptrI8hittableEEE10deallocateERS3_PS2_m..ng:
	lda $30,-48($30)
	.cfi_def_cfa_offset 48
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -48
	.cfi_offset 15, -40
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,16($15)
	stq $17,24($15)
	stq $18,32($15)
	ldq $18,32($15)
	ldq $17,24($15)
	ldq $16,16($15)
	ldq $27,_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEE10deallocateEPS3_m($29)		!literal!392
	jsr $26,($27),_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEE10deallocateEPS3_m		!lituse_jsr!392
	ldah $29,0($26)		!gpdisp!393
	lda $29,0($29)		!gpdisp!393
	bis $31,$31,$31
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,48($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4605:
	.end _ZNSt16allocator_traitsISaISt10shared_ptrI8hittableEEE10deallocateERS3_PS2_m
	.section	.text._ZNSt12_Destroy_auxILb0EE9__destroyIPSt10shared_ptrI8hittableEEEvT_S6_,"axG",@progbits,_ZNSt12_Destroy_auxILb0EE9__destroyIPSt10shared_ptrI8hittableEEEvT_S6_,comdat
	.align 2
	.weak	_ZNSt12_Destroy_auxILb0EE9__destroyIPSt10shared_ptrI8hittableEEEvT_S6_
	.ent _ZNSt12_Destroy_auxILb0EE9__destroyIPSt10shared_ptrI8hittableEEEvT_S6_
_ZNSt12_Destroy_auxILb0EE9__destroyIPSt10shared_ptrI8hittableEEEvT_S6_:
	.eflag 48
	.frame $15,32,$26,0
	.mask 0x4008000,-32
$LFB4606:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!394
	lda $29,0($29)		!gpdisp!394
$_ZNSt12_Destroy_auxILb0EE9__destroyIPSt10shared_ptrI8hittableEEEvT_S6_..ng:
	lda $30,-32($30)
	.cfi_def_cfa_offset 32
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -32
	.cfi_offset 15, -24
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,16($15)
	stq $17,24($15)
$L173:
	ldq $2,16($15)
	ldq $1,24($15)
	cmpeq $2,$1,$1
	bne $1,$L174
	ldq $16,16($15)
	ldq $27,_ZSt11__addressofISt10shared_ptrI8hittableEEPT_RS3_($29)		!literal!395
	jsr $26,($27),0		!lituse_jsr!395
	ldah $29,0($26)		!gpdisp!396
	lda $29,0($29)		!gpdisp!396
	mov $0,$1
	mov $1,$16
	ldq $27,_ZSt8_DestroyISt10shared_ptrI8hittableEEvPT_($29)		!literal!397
	jsr $26,($27),_ZSt8_DestroyISt10shared_ptrI8hittableEEvPT_		!lituse_jsr!397
	ldah $29,0($26)		!gpdisp!398
	lda $29,0($29)		!gpdisp!398
	ldq $1,16($15)
	lda $1,16($1)
	stq $1,16($15)
	br $31,$L173
$L174:
	bis $31,$31,$31
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,32($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4606:
	.end _ZNSt12_Destroy_auxILb0EE9__destroyIPSt10shared_ptrI8hittableEEEvT_S6_
	.section	.text._ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,"axG",@progbits,_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC5ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,comdat
	.align 2
	.weak	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.ent _ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_:
	.eflag 48
	.frame $15,80,$26,0
	.mask 0x4008e00,-80
$LFB4608:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!399
	lda $29,0($29)		!gpdisp!399
$_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_..ng:
	lda $30,-80($30)
	.cfi_def_cfa_offset 80
	stq $26,0($30)
	stq $9,8($30)
	stq $10,16($30)
	stq $11,24($30)
	stq $15,32($30)
	.cfi_offset 26, -80
	.cfi_offset 9, -72
	.cfi_offset 10, -64
	.cfi_offset 11, -56
	.cfi_offset 15, -48
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,48($15)
	stq $17,56($15)
	stq $18,64($15)
	stq $19,72($15)
	ldq $1,48($15)
	stq $31,0($1)
	ldq $1,48($15)
	lda $10,8($1)
	ldq $11,48($15)
	ldq $16,64($15)
	ldq $27,_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE($29)		!literal!400
	jsr $26,($27),0		!lituse_jsr!400
	ldah $29,0($26)		!gpdisp!401
	lda $29,0($29)		!gpdisp!401
	mov $0,$9
	ldq $16,72($15)
	ldq $27,_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE($29)		!literal!402
	jsr $26,($27),0		!lituse_jsr!402
	ldah $29,0($26)		!gpdisp!403
	lda $29,0($29)		!gpdisp!403
	mov $0,$1
	mov $1,$20
	mov $9,$19
	ldq $18,56($15)
	mov $11,$17
	mov $10,$16
	ldq $27,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_($29)		!literal!404
	jsr $26,($27),_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_		!lituse_jsr!404
	ldah $29,0($26)		!gpdisp!405
	lda $29,0($29)		!gpdisp!405
	ldq $1,48($15)
	ldq $1,0($1)
	mov $1,$17
	ldq $16,48($15)
	ldq $27,_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EE31_M_enable_shared_from_this_withIS0_S0_EENSt9enable_ifIXntsrNS3_15__has_esft_baseIT0_vEE5valueEvE4typeEPT_($29)		!literal!406
	jsr $26,($27),_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EE31_M_enable_shared_from_this_withIS0_S0_EENSt9enable_ifIXntsrNS3_15__has_esft_baseIT0_vEE5valueEvE4typeEPT_		!lituse_jsr!406
	ldah $29,0($26)		!gpdisp!407
	lda $29,0($29)		!gpdisp!407
	bis $31,$31,$31
	mov $15,$30
	ldq $26,0($30)
	ldq $9,8($30)
	ldq $10,16($30)
	ldq $11,24($30)
	ldq $15,32($30)
	lda $30,80($30)
	.cfi_restore 15
	.cfi_restore 11
	.cfi_restore 10
	.cfi_restore 9
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4608:
	.end _ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.weak	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC1ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
$_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC1ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_..ng = $_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_..ng
_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC1ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_ = _ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.section	.text._ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,"axG",@progbits,_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC5ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,comdat
	.align 2
	.weak	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.ent _ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_:
	.eflag 48
	.frame $15,80,$26,0
	.mask 0x4008e00,-80
$LFB4611:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!408
	lda $29,0($29)		!gpdisp!408
$_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_..ng:
	lda $30,-80($30)
	.cfi_def_cfa_offset 80
	stq $26,0($30)
	stq $9,8($30)
	stq $10,16($30)
	stq $11,24($30)
	stq $15,32($30)
	.cfi_offset 26, -80
	.cfi_offset 9, -72
	.cfi_offset 10, -64
	.cfi_offset 11, -56
	.cfi_offset 15, -48
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,48($15)
	stq $17,56($15)
	stq $18,64($15)
	stq $19,72($15)
	ldq $1,48($15)
	stq $31,0($1)
	ldq $1,48($15)
	lda $10,8($1)
	ldq $11,48($15)
	ldq $16,64($15)
	ldq $27,_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE($29)		!literal!409
	jsr $26,($27),0		!lituse_jsr!409
	ldah $29,0($26)		!gpdisp!410
	lda $29,0($29)		!gpdisp!410
	mov $0,$9
	ldq $16,72($15)
	ldq $27,_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE($29)		!literal!411
	jsr $26,($27),0		!lituse_jsr!411
	ldah $29,0($26)		!gpdisp!412
	lda $29,0($29)		!gpdisp!412
	mov $0,$1
	mov $1,$20
	mov $9,$19
	ldq $18,56($15)
	mov $11,$17
	mov $10,$16
	ldq $27,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_($29)		!literal!413
	jsr $26,($27),_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_		!lituse_jsr!413
	ldah $29,0($26)		!gpdisp!414
	lda $29,0($29)		!gpdisp!414
	ldq $1,48($15)
	ldq $1,0($1)
	mov $1,$17
	ldq $16,48($15)
	ldq $27,_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EE31_M_enable_shared_from_this_withIS0_S0_EENSt9enable_ifIXntsrNS3_15__has_esft_baseIT0_vEE5valueEvE4typeEPT_($29)		!literal!415
	jsr $26,($27),_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EE31_M_enable_shared_from_this_withIS0_S0_EENSt9enable_ifIXntsrNS3_15__has_esft_baseIT0_vEE5valueEvE4typeEPT_		!lituse_jsr!415
	ldah $29,0($26)		!gpdisp!416
	lda $29,0($29)		!gpdisp!416
	bis $31,$31,$31
	mov $15,$30
	ldq $26,0($30)
	ldq $9,8($30)
	ldq $10,16($30)
	ldq $11,24($30)
	ldq $15,32($30)
	lda $30,80($30)
	.cfi_restore 15
	.cfi_restore 11
	.cfi_restore 10
	.cfi_restore 9
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4611:
	.end _ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.weak	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC1ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
$_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC1ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_..ng = $_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_..ng
_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC1ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_ = _ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.section	.text._ZNSt8__detail4_ModImLm4294967296ELm1ELm0ELb1ELb1EE6__calcEm,"axG",@progbits,_ZNSt8__detail4_ModImLm4294967296ELm1ELm0ELb1ELb1EE6__calcEm,comdat
	.align 2
	.weak	_ZNSt8__detail4_ModImLm4294967296ELm1ELm0ELb1ELb1EE6__calcEm
	.ent _ZNSt8__detail4_ModImLm4294967296ELm1ELm0ELb1ELb1EE6__calcEm
$_ZNSt8__detail4_ModImLm4294967296ELm1ELm0ELb1ELb1EE6__calcEm..ng:
_ZNSt8__detail4_ModImLm4294967296ELm1ELm0ELb1ELb1EE6__calcEm:
	.eflag 48
	.frame $15,48,$26,0
	.mask 0x4008000,-48
$LFB4641:
	.cfi_startproc
	lda $30,-48($30)
	.cfi_def_cfa_offset 48
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -48
	.cfi_offset 15, -40
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 0
	stq $16,32($15)
	ldq $1,32($15)
	stq $1,16($15)
	ldq $1,16($15)
	zapnot $1,15,$1
	stq $1,16($15)
	ldq $1,16($15)
	mov $1,$0
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,48($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4641:
	.end _ZNSt8__detail4_ModImLm4294967296ELm1ELm0ELb1ELb1EE6__calcEm
	.section	.text._ZNSt8__detail4_ModImLm624ELm1ELm0ELb1ELb1EE6__calcEm,"axG",@progbits,_ZNSt8__detail4_ModImLm624ELm1ELm0ELb1ELb1EE6__calcEm,comdat
	.align 2
	.weak	_ZNSt8__detail4_ModImLm624ELm1ELm0ELb1ELb1EE6__calcEm
	.ent _ZNSt8__detail4_ModImLm624ELm1ELm0ELb1ELb1EE6__calcEm
_ZNSt8__detail4_ModImLm624ELm1ELm0ELb1ELb1EE6__calcEm:
	.eflag 48
	.frame $15,48,$26,0
	.mask 0x4008000,-48
$LFB4642:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!417
	lda $29,0($29)		!gpdisp!417
$_ZNSt8__detail4_ModImLm624ELm1ELm0ELb1ELb1EE6__calcEm..ng:
	lda $30,-48($30)
	.cfi_def_cfa_offset 48
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -48
	.cfi_offset 15, -40
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,32($15)
	ldq $1,32($15)
	stq $1,16($15)
	ldq $3,16($15)
	srl $3,4,$2
	ldah $1,$LC17($29)		!gprelhigh
	ldq $1,$LC17($1)		!gprellow
	umulh $2,$1,$1
	srl $1,1,$2
	mov $2,$1
	s4addq $1,0,$1
	addq $1,$2,$1
	s8addq $1,0,$1
	subq $1,$2,$1
	sll $1,4,$1
	subq $3,$1,$1
	stq $1,16($15)
	ldq $1,16($15)
	mov $1,$0
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,48($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4642:
	.end _ZNSt8__detail4_ModImLm624ELm1ELm0ELb1ELb1EE6__calcEm
	.section	.text._ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEclEv,"axG",@progbits,_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEclEv,comdat
	.align 2
	.weak	_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEclEv
	.ent _ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEclEv
_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEclEv:
	.eflag 48
	.frame $15,48,$26,0
	.mask 0x4008000,-48
$LFB4643:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!418
	lda $29,0($29)		!gpdisp!418
$_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEclEv..ng:
	lda $30,-48($30)
	.cfi_def_cfa_offset 48
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -48
	.cfi_offset 15, -40
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,32($15)
	ldq $1,32($15)
	ldq $1,4992($1)
	lda $2,623($31)
	cmpule $1,$2,$1
	bne $1,$L182
	ldq $16,32($15)
	ldq $27,_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE11_M_gen_randEv($29)		!literal!419
	jsr $26,($27),_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE11_M_gen_randEv		!lituse_jsr!419
	ldah $29,0($26)		!gpdisp!420
	lda $29,0($29)		!gpdisp!420
$L182:
	ldq $1,32($15)
	ldq $1,4992($1)
	lda $3,1($1)
	ldq $2,32($15)
	stq $3,4992($2)
	ldq $2,32($15)
	s8addq $1,0,$1
	addq $2,$1,$1
	ldq $1,0($1)
	stq $1,16($15)
	ldq $1,16($15)
	srl $1,11,$1
	zapnot $1,15,$1
	ldq $2,16($15)
	xor $2,$1,$1
	stq $1,16($15)
	ldq $1,16($15)
	sll $1,7,$2
	lda $1,10059($31)
	sll $1,18,$1
	lda $1,22144($1)
	and $2,$1,$1
	ldq $2,16($15)
	xor $2,$1,$1
	stq $1,16($15)
	ldq $1,16($15)
	sll $1,15,$2
	lda $1,30691($31)
	sll $1,17,$1
	and $2,$1,$1
	ldq $2,16($15)
	xor $2,$1,$1
	stq $1,16($15)
	ldq $1,16($15)
	srl $1,18,$1
	ldq $2,16($15)
	xor $2,$1,$1
	stq $1,16($15)
	ldq $1,16($15)
	mov $1,$0
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,48($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4643:
	.end _ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EEclEv
	.section	.text._ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEE10deallocateEPS3_m,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEE10deallocateEPS3_m,comdat
	.align 2
	.weak	_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEE10deallocateEPS3_m
	.ent _ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEE10deallocateEPS3_m
_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEE10deallocateEPS3_m:
	.eflag 48
	.frame $15,48,$26,0
	.mask 0x4008000,-48
$LFB4645:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!421
	lda $29,0($29)		!gpdisp!421
$_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEE10deallocateEPS3_m..ng:
	lda $30,-48($30)
	.cfi_def_cfa_offset 48
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -48
	.cfi_offset 15, -40
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,16($15)
	stq $17,24($15)
	stq $18,32($15)
	ldq $16,24($15)
	ldq $27,_ZdlPv($29)		!literal!422
	jsr $26,($27),_ZdlPv		!lituse_jsr!422
	ldah $29,0($26)		!gpdisp!423
	lda $29,0($29)		!gpdisp!423
	bis $31,$31,$31
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,48($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4645:
	.end _ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEE10deallocateEPS3_m
	.section	.text._ZSt11__addressofISt10shared_ptrI8hittableEEPT_RS3_,"axG",@progbits,_ZSt11__addressofISt10shared_ptrI8hittableEEPT_RS3_,comdat
	.align 2
	.weak	_ZSt11__addressofISt10shared_ptrI8hittableEEPT_RS3_
	.ent _ZSt11__addressofISt10shared_ptrI8hittableEEPT_RS3_
$_ZSt11__addressofISt10shared_ptrI8hittableEEPT_RS3_..ng:
_ZSt11__addressofISt10shared_ptrI8hittableEEPT_RS3_:
	.eflag 48
	.frame $15,32,$26,0
	.mask 0x4008000,-32
$LFB4646:
	.cfi_startproc
	lda $30,-32($30)
	.cfi_def_cfa_offset 32
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -32
	.cfi_offset 15, -24
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 0
	stq $16,16($15)
	ldq $1,16($15)
	mov $1,$0
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,32($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4646:
	.end _ZSt11__addressofISt10shared_ptrI8hittableEEPT_RS3_
	.section	.text._ZSt8_DestroyISt10shared_ptrI8hittableEEvPT_,"axG",@progbits,_ZSt8_DestroyISt10shared_ptrI8hittableEEvPT_,comdat
	.align 2
	.weak	_ZSt8_DestroyISt10shared_ptrI8hittableEEvPT_
	.ent _ZSt8_DestroyISt10shared_ptrI8hittableEEvPT_
_ZSt8_DestroyISt10shared_ptrI8hittableEEvPT_:
	.eflag 48
	.frame $15,32,$26,0
	.mask 0x4008000,-32
$LFB4647:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!424
	lda $29,0($29)		!gpdisp!424
$_ZSt8_DestroyISt10shared_ptrI8hittableEEvPT_..ng:
	lda $30,-32($30)
	.cfi_def_cfa_offset 32
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -32
	.cfi_offset 15, -24
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,16($15)
	ldq $16,16($15)
	ldq $27,_ZNSt10shared_ptrI8hittableED1Ev($29)		!literal!425
	jsr $26,($27),_ZNSt10shared_ptrI8hittableED1Ev		!lituse_jsr!425
	ldah $29,0($26)		!gpdisp!426
	lda $29,0($29)		!gpdisp!426
	bis $31,$31,$31
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,32($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4647:
	.end _ZSt8_DestroyISt10shared_ptrI8hittableEEvPT_
	.section	.text._ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_,"axG",@progbits,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC5I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_,comdat
	.align 2
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_
	.ent _ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_
_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_:
	.eflag 48
	.frame $15,160,$26,0
	.mask 0x400be00,-160
$LFB4649:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,$LLSDA4649
	ldah $29,0($27)		!gpdisp!427
	lda $29,0($29)		!gpdisp!427
$_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_..ng:
	lda $30,-160($30)
	.cfi_def_cfa_offset 160
	stq $26,0($30)
	stq $9,8($30)
	stq $10,16($30)
	stq $11,24($30)
	stq $12,32($30)
	stq $13,40($30)
	stq $15,48($30)
	.cfi_offset 26, -160
	.cfi_offset 9, -152
	.cfi_offset 10, -144
	.cfi_offset 11, -136
	.cfi_offset 12, -128
	.cfi_offset 13, -120
	.cfi_offset 15, -112
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,112($15)
	stq $17,120($15)
	stq $18,128($15)
	stq $19,136($15)
	stq $20,144($15)
	ldq $2,128($15)
	lda $1,81($15)
	mov $2,$17
	mov $1,$16
	ldq $27,_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC1IS0_EERKSaIT_E($29)		!literal!430
	jsr $26,($27),_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC1IS0_EERKSaIT_E		!lituse_jsr!430
	ldah $29,0($26)		!gpdisp!431
	lda $29,0($29)		!gpdisp!431
	lda $1,88($15)
	lda $2,81($15)
	mov $2,$17
	mov $1,$16
$LEHB20:
	ldq $27,_ZSt18__allocate_guardedISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEESt15__allocated_ptrIT_ERS8_($29)		!literal!432
	jsr $26,($27),_ZSt18__allocate_guardedISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEESt15__allocated_ptrIT_ERS8_		!lituse_jsr!432
	ldah $29,0($26)		!gpdisp!433
	lda $29,0($29)		!gpdisp!433
$LEHE20:
	lda $1,88($15)
	mov $1,$16
	ldq $27,_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE3getEv($29)		!literal!434
	jsr $26,($27),0		!lituse_jsr!434
	ldah $29,0($26)		!gpdisp!435
	lda $29,0($29)		!gpdisp!435
	mov $0,$1
	stq $1,64($15)
	ldq $2,128($15)
	lda $1,80($15)
	mov $2,$17
	mov $1,$16
	ldq $27,_ZNSaI6sphereEC1ERKS0_($29)		!literal!436
	jsr $26,($27),_ZNSaI6sphereEC1ERKS0_		!lituse_jsr!436
	ldah $29,0($26)		!gpdisp!437
	lda $29,0($29)		!gpdisp!437
	lda $13,80($15)
	ldq $16,136($15)
	ldq $27,_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE($29)		!literal!438
	jsr $26,($27),0		!lituse_jsr!438
	ldah $29,0($26)		!gpdisp!439
	lda $29,0($29)		!gpdisp!439
	mov $0,$11
	ldq $16,144($15)
	ldq $27,_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE($29)		!literal!440
	jsr $26,($27),0		!lituse_jsr!440
	ldah $29,0($26)		!gpdisp!441
	lda $29,0($29)		!gpdisp!441
	mov $0,$12
	ldq $10,64($15)
	mov $10,$17
	lda $16,72($31)
	ldq $27,_ZnwmPv($29)		!literal!442
	jsr $26,($27),0		!lituse_jsr!442
	ldah $29,0($26)		!gpdisp!443
	lda $29,0($29)		!gpdisp!443
	mov $0,$9
	mov $12,$19
	mov $11,$18
	mov $13,$17
	mov $9,$16
$LEHB21:
	ldq $27,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC1IJ4vec3dEEES1_DpOT_($29)		!literal!444
	jsr $26,($27),_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC1IJ4vec3dEEES1_DpOT_		!lituse_jsr!444
	ldah $29,0($26)		!gpdisp!445
	lda $29,0($29)		!gpdisp!445
$LEHE21:
	stq $9,72($15)
	lda $1,80($15)
	mov $1,$16
	ldq $27,_ZNSaI6sphereED1Ev($29)		!literal!446
	jsr $26,($27),_ZNSaI6sphereED1Ev		!lituse_jsr!446
	ldah $29,0($26)		!gpdisp!447
	lda $29,0($29)		!gpdisp!447
	lda $1,88($15)
	mov $31,$17
	mov $1,$16
	ldq $27,_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEaSEDn($29)		!literal!448
	jsr $26,($27),0		!lituse_jsr!448
	ldah $29,0($26)		!gpdisp!449
	lda $29,0($29)		!gpdisp!449
	ldq $1,112($15)
	ldq $2,72($15)
	stq $2,0($1)
	ldq $16,72($15)
	ldq $27,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv($29)		!literal!450
	jsr $26,($27),0		!lituse_jsr!450
	ldah $29,0($26)		!gpdisp!451
	lda $29,0($29)		!gpdisp!451
	mov $0,$1
	ldq $2,120($15)
	stq $1,0($2)
	lda $1,88($15)
	mov $1,$16
	ldq $27,_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED1Ev($29)		!literal!452
	jsr $26,($27),_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED1Ev		!lituse_jsr!452
	ldah $29,0($26)		!gpdisp!453
	lda $29,0($29)		!gpdisp!453
	lda $1,81($15)
	mov $1,$16
	ldq $27,_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED1Ev($29)		!literal!454
	jsr $26,($27),_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED1Ev		!lituse_jsr!454
	ldah $29,0($26)		!gpdisp!455
	lda $29,0($29)		!gpdisp!455
	br $31,$L193
$L192:
	ldah $29,0($26)		!gpdisp!428
	lda $29,0($29)		!gpdisp!428
	mov $16,$11
	mov $10,$17
	mov $9,$16
	ldq $27,_ZdlPvS_($29)		!literal!456
	jsr $26,($27),_ZdlPvS_		!lituse_jsr!456
	ldah $29,0($26)		!gpdisp!457
	lda $29,0($29)		!gpdisp!457
	mov $11,$9
	lda $1,80($15)
	mov $1,$16
	ldq $27,_ZNSaI6sphereED1Ev($29)		!literal!458
	jsr $26,($27),_ZNSaI6sphereED1Ev		!lituse_jsr!458
	ldah $29,0($26)		!gpdisp!459
	lda $29,0($29)		!gpdisp!459
	lda $1,88($15)
	mov $1,$16
	ldq $27,_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED1Ev($29)		!literal!460
	jsr $26,($27),_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED1Ev		!lituse_jsr!460
	ldah $29,0($26)		!gpdisp!461
	lda $29,0($29)		!gpdisp!461
	br $31,$L190
$L191:
	ldah $29,0($26)		!gpdisp!429
	lda $29,0($29)		!gpdisp!429
	mov $16,$9
$L190:
	lda $1,81($15)
	mov $1,$16
	ldq $27,_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED1Ev($29)		!literal!462
	jsr $26,($27),_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED1Ev		!lituse_jsr!462
	ldah $29,0($26)		!gpdisp!463
	lda $29,0($29)		!gpdisp!463
	mov $9,$1
	mov $1,$16
$LEHB22:
	ldq $27,_Unwind_Resume($29)		!literal!464
	jsr $26,($27),_Unwind_Resume		!lituse_jsr!464
$LEHE22:
$L193:
	mov $15,$30
	ldq $26,0($30)
	ldq $9,8($30)
	ldq $10,16($30)
	ldq $11,24($30)
	ldq $12,32($30)
	ldq $13,40($30)
	ldq $15,48($30)
	lda $30,160($30)
	.cfi_restore 15
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_restore 10
	.cfi_restore 9
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4649:
	.section	.gcc_except_table
$LLSDA4649:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 $LLSDACSE4649-$LLSDACSB4649
$LLSDACSB4649:
	.uleb128 $LEHB20-$LFB4649
	.uleb128 $LEHE20-$LEHB20
	.uleb128 $L191-$LFB4649
	.uleb128 0
	.uleb128 $LEHB21-$LFB4649
	.uleb128 $LEHE21-$LEHB21
	.uleb128 $L192-$LFB4649
	.uleb128 0
	.uleb128 $LEHB22-$LFB4649
	.uleb128 $LEHE22-$LEHB22
	.uleb128 0
	.uleb128 0
$LLSDACSE4649:
	.section	.text._ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_,"axG",@progbits,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC5I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_,comdat
	.end _ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_
$_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_..ng = $_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_..ng
_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_ = _ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_
	.section	.text._ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EE31_M_enable_shared_from_this_withIS0_S0_EENSt9enable_ifIXntsrNS3_15__has_esft_baseIT0_vEE5valueEvE4typeEPT_,"axG",@progbits,_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EE31_M_enable_shared_from_this_withIS0_S0_EENSt9enable_ifIXntsrNS3_15__has_esft_baseIT0_vEE5valueEvE4typeEPT_,comdat
	.align 2
	.weak	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EE31_M_enable_shared_from_this_withIS0_S0_EENSt9enable_ifIXntsrNS3_15__has_esft_baseIT0_vEE5valueEvE4typeEPT_
	.ent _ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EE31_M_enable_shared_from_this_withIS0_S0_EENSt9enable_ifIXntsrNS3_15__has_esft_baseIT0_vEE5valueEvE4typeEPT_
$_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EE31_M_enable_shared_from_this_withIS0_S0_EENSt9enable_ifIXntsrNS3_15__has_esft_baseIT0_vEE5valueEvE4typeEPT_..ng:
_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EE31_M_enable_shared_from_this_withIS0_S0_EENSt9enable_ifIXntsrNS3_15__has_esft_baseIT0_vEE5valueEvE4typeEPT_:
	.eflag 48
	.frame $15,32,$26,0
	.mask 0x4008000,-32
$LFB4651:
	.cfi_startproc
	lda $30,-32($30)
	.cfi_def_cfa_offset 32
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -32
	.cfi_offset 15, -24
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 0
	stq $16,16($15)
	stq $17,24($15)
	bis $31,$31,$31
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,32($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4651:
	.end _ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EE31_M_enable_shared_from_this_withIS0_S0_EENSt9enable_ifIXntsrNS3_15__has_esft_baseIT0_vEE5valueEvE4typeEPT_
	.section	.text._ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_,"axG",@progbits,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC5I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_,comdat
	.align 2
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_
	.ent _ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_
_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_:
	.eflag 48
	.frame $15,160,$26,0
	.mask 0x400be00,-160
$LFB4653:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,$LLSDA4653
	ldah $29,0($27)		!gpdisp!465
	lda $29,0($29)		!gpdisp!465
$_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_..ng:
	lda $30,-160($30)
	.cfi_def_cfa_offset 160
	stq $26,0($30)
	stq $9,8($30)
	stq $10,16($30)
	stq $11,24($30)
	stq $12,32($30)
	stq $13,40($30)
	stq $15,48($30)
	.cfi_offset 26, -160
	.cfi_offset 9, -152
	.cfi_offset 10, -144
	.cfi_offset 11, -136
	.cfi_offset 12, -128
	.cfi_offset 13, -120
	.cfi_offset 15, -112
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,112($15)
	stq $17,120($15)
	stq $18,128($15)
	stq $19,136($15)
	stq $20,144($15)
	ldq $2,128($15)
	lda $1,81($15)
	mov $2,$17
	mov $1,$16
	ldq $27,_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC1IS0_EERKSaIT_E($29)		!literal!468
	jsr $26,($27),_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC1IS0_EERKSaIT_E		!lituse_jsr!468
	ldah $29,0($26)		!gpdisp!469
	lda $29,0($29)		!gpdisp!469
	lda $1,88($15)
	lda $2,81($15)
	mov $2,$17
	mov $1,$16
$LEHB23:
	ldq $27,_ZSt18__allocate_guardedISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEESt15__allocated_ptrIT_ERS8_($29)		!literal!470
	jsr $26,($27),_ZSt18__allocate_guardedISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEESt15__allocated_ptrIT_ERS8_		!lituse_jsr!470
	ldah $29,0($26)		!gpdisp!471
	lda $29,0($29)		!gpdisp!471
$LEHE23:
	lda $1,88($15)
	mov $1,$16
	ldq $27,_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE3getEv($29)		!literal!472
	jsr $26,($27),0		!lituse_jsr!472
	ldah $29,0($26)		!gpdisp!473
	lda $29,0($29)		!gpdisp!473
	mov $0,$1
	stq $1,64($15)
	ldq $2,128($15)
	lda $1,80($15)
	mov $2,$17
	mov $1,$16
	ldq $27,_ZNSaI6sphereEC1ERKS0_($29)		!literal!474
	jsr $26,($27),_ZNSaI6sphereEC1ERKS0_		!lituse_jsr!474
	ldah $29,0($26)		!gpdisp!475
	lda $29,0($29)		!gpdisp!475
	lda $13,80($15)
	ldq $16,136($15)
	ldq $27,_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE($29)		!literal!476
	jsr $26,($27),0		!lituse_jsr!476
	ldah $29,0($26)		!gpdisp!477
	lda $29,0($29)		!gpdisp!477
	mov $0,$11
	ldq $16,144($15)
	ldq $27,_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE($29)		!literal!478
	jsr $26,($27),0		!lituse_jsr!478
	ldah $29,0($26)		!gpdisp!479
	lda $29,0($29)		!gpdisp!479
	mov $0,$12
	ldq $10,64($15)
	mov $10,$17
	lda $16,72($31)
	ldq $27,_ZnwmPv($29)		!literal!480
	jsr $26,($27),0		!lituse_jsr!480
	ldah $29,0($26)		!gpdisp!481
	lda $29,0($29)		!gpdisp!481
	mov $0,$9
	mov $12,$19
	mov $11,$18
	mov $13,$17
	mov $9,$16
$LEHB24:
	ldq $27,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC1IJ4vec3iEEES1_DpOT_($29)		!literal!482
	jsr $26,($27),_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC1IJ4vec3iEEES1_DpOT_		!lituse_jsr!482
	ldah $29,0($26)		!gpdisp!483
	lda $29,0($29)		!gpdisp!483
$LEHE24:
	stq $9,72($15)
	lda $1,80($15)
	mov $1,$16
	ldq $27,_ZNSaI6sphereED1Ev($29)		!literal!484
	jsr $26,($27),_ZNSaI6sphereED1Ev		!lituse_jsr!484
	ldah $29,0($26)		!gpdisp!485
	lda $29,0($29)		!gpdisp!485
	lda $1,88($15)
	mov $31,$17
	mov $1,$16
	ldq $27,_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEaSEDn($29)		!literal!486
	jsr $26,($27),0		!lituse_jsr!486
	ldah $29,0($26)		!gpdisp!487
	lda $29,0($29)		!gpdisp!487
	ldq $1,112($15)
	ldq $2,72($15)
	stq $2,0($1)
	ldq $16,72($15)
	ldq $27,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv($29)		!literal!488
	jsr $26,($27),0		!lituse_jsr!488
	ldah $29,0($26)		!gpdisp!489
	lda $29,0($29)		!gpdisp!489
	mov $0,$1
	ldq $2,120($15)
	stq $1,0($2)
	lda $1,88($15)
	mov $1,$16
	ldq $27,_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED1Ev($29)		!literal!490
	jsr $26,($27),_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED1Ev		!lituse_jsr!490
	ldah $29,0($26)		!gpdisp!491
	lda $29,0($29)		!gpdisp!491
	lda $1,81($15)
	mov $1,$16
	ldq $27,_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED1Ev($29)		!literal!492
	jsr $26,($27),_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED1Ev		!lituse_jsr!492
	ldah $29,0($26)		!gpdisp!493
	lda $29,0($29)		!gpdisp!493
	br $31,$L200
$L199:
	ldah $29,0($26)		!gpdisp!466
	lda $29,0($29)		!gpdisp!466
	mov $16,$11
	mov $10,$17
	mov $9,$16
	ldq $27,_ZdlPvS_($29)		!literal!494
	jsr $26,($27),_ZdlPvS_		!lituse_jsr!494
	ldah $29,0($26)		!gpdisp!495
	lda $29,0($29)		!gpdisp!495
	mov $11,$9
	lda $1,80($15)
	mov $1,$16
	ldq $27,_ZNSaI6sphereED1Ev($29)		!literal!496
	jsr $26,($27),_ZNSaI6sphereED1Ev		!lituse_jsr!496
	ldah $29,0($26)		!gpdisp!497
	lda $29,0($29)		!gpdisp!497
	lda $1,88($15)
	mov $1,$16
	ldq $27,_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED1Ev($29)		!literal!498
	jsr $26,($27),_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED1Ev		!lituse_jsr!498
	ldah $29,0($26)		!gpdisp!499
	lda $29,0($29)		!gpdisp!499
	br $31,$L197
$L198:
	ldah $29,0($26)		!gpdisp!467
	lda $29,0($29)		!gpdisp!467
	mov $16,$9
$L197:
	lda $1,81($15)
	mov $1,$16
	ldq $27,_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED1Ev($29)		!literal!500
	jsr $26,($27),_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED1Ev		!lituse_jsr!500
	ldah $29,0($26)		!gpdisp!501
	lda $29,0($29)		!gpdisp!501
	mov $9,$1
	mov $1,$16
$LEHB25:
	ldq $27,_Unwind_Resume($29)		!literal!502
	jsr $26,($27),_Unwind_Resume		!lituse_jsr!502
$LEHE25:
$L200:
	mov $15,$30
	ldq $26,0($30)
	ldq $9,8($30)
	ldq $10,16($30)
	ldq $11,24($30)
	ldq $12,32($30)
	ldq $13,40($30)
	ldq $15,48($30)
	lda $30,160($30)
	.cfi_restore 15
	.cfi_restore 13
	.cfi_restore 12
	.cfi_restore 11
	.cfi_restore 10
	.cfi_restore 9
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4653:
	.section	.gcc_except_table
$LLSDA4653:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 $LLSDACSE4653-$LLSDACSB4653
$LLSDACSB4653:
	.uleb128 $LEHB23-$LFB4653
	.uleb128 $LEHE23-$LEHB23
	.uleb128 $L198-$LFB4653
	.uleb128 0
	.uleb128 $LEHB24-$LFB4653
	.uleb128 $LEHE24-$LEHB24
	.uleb128 $L199-$LFB4653
	.uleb128 0
	.uleb128 $LEHB25-$LFB4653
	.uleb128 $LEHE25-$LEHB25
	.uleb128 0
	.uleb128 0
$LLSDACSE4653:
	.section	.text._ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_,"axG",@progbits,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC5I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_,comdat
	.end _ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_
$_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_..ng = $_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_..ng
_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_ = _ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_
	.section	.text._ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE11_M_gen_randEv,"axG",@progbits,_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE11_M_gen_randEv,comdat
	.align 2
	.weak	_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE11_M_gen_randEv
	.ent _ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE11_M_gen_randEv
$_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE11_M_gen_randEv..ng:
_ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE11_M_gen_randEv:
	.eflag 48
	.frame $15,96,$26,0
	.mask 0x4008000,-96
$LFB4681:
	.cfi_startproc
	lda $30,-96($30)
	.cfi_def_cfa_offset 96
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -96
	.cfi_offset 15, -88
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 0
	stq $16,80($15)
	ldah $1,-32768($31)
	stq $1,32($15)
	ldah $1,-32768($31)
	ornot $31,$1,$1
	stq $1,40($15)
	stq $31,16($15)
$L205:
	ldq $1,16($15)
	cmpule $1,226,$1
	beq $1,$L202
	ldq $2,80($15)
	ldq $1,16($15)
	s8addq $1,0,$1
	addq $2,$1,$1
	ldq $1,0($1)
	ldah $2,-32768($31)
	and $1,$2,$2
	ldq $1,16($15)
	lda $1,1($1)
	ldq $3,80($15)
	s8addq $1,0,$1
	addq $3,$1,$1
	ldq $3,0($1)
	ldah $1,-32768($31)
	ornot $31,$1,$1
	and $3,$1,$1
	bis $2,$1,$1
	stq $1,48($15)
	ldq $1,16($15)
	lda $1,397($1)
	ldq $2,80($15)
	s8addq $1,0,$1
	addq $2,$1,$1
	ldq $2,0($1)
	ldq $1,48($15)
	srl $1,1,$1
	xor $2,$1,$2
	ldq $1,48($15)
	and $1,1,$1
	beq $1,$L203
	ldah $1,-26359($31)
	lda $1,-20257($1)
	zapnot $1,15,$1
	br $31,$L204
$L203:
	mov $31,$1
$L204:
	xor $1,$2,$2
	ldq $3,80($15)
	ldq $1,16($15)
	s8addq $1,0,$1
	addq $3,$1,$1
	stq $2,0($1)
	ldq $1,16($15)
	lda $1,1($1)
	stq $1,16($15)
	br $31,$L205
$L202:
	lda $1,227($31)
	stq $1,24($15)
$L209:
	ldq $1,24($15)
	lda $2,622($31)
	cmpule $1,$2,$1
	beq $1,$L206
	ldq $2,80($15)
	ldq $1,24($15)
	s8addq $1,0,$1
	addq $2,$1,$1
	ldq $1,0($1)
	ldah $2,-32768($31)
	and $1,$2,$2
	ldq $1,24($15)
	lda $1,1($1)
	ldq $3,80($15)
	s8addq $1,0,$1
	addq $3,$1,$1
	ldq $3,0($1)
	ldah $1,-32768($31)
	ornot $31,$1,$1
	and $3,$1,$1
	bis $2,$1,$1
	stq $1,56($15)
	ldq $1,24($15)
	lda $1,-227($1)
	ldq $2,80($15)
	s8addq $1,0,$1
	addq $2,$1,$1
	ldq $2,0($1)
	ldq $1,56($15)
	srl $1,1,$1
	xor $2,$1,$2
	ldq $1,56($15)
	and $1,1,$1
	beq $1,$L207
	ldah $1,-26359($31)
	lda $1,-20257($1)
	zapnot $1,15,$1
	br $31,$L208
$L207:
	mov $31,$1
$L208:
	xor $1,$2,$2
	ldq $3,80($15)
	ldq $1,24($15)
	s8addq $1,0,$1
	addq $3,$1,$1
	stq $2,0($1)
	ldq $1,24($15)
	lda $1,1($1)
	stq $1,24($15)
	br $31,$L209
$L206:
	ldq $1,80($15)
	ldq $1,4984($1)
	ldah $2,-32768($31)
	and $1,$2,$2
	ldq $1,80($15)
	ldq $3,0($1)
	ldah $1,-32768($31)
	ornot $31,$1,$1
	and $3,$1,$1
	bis $2,$1,$1
	stq $1,64($15)
	ldq $1,80($15)
	ldq $2,3168($1)
	ldq $1,64($15)
	srl $1,1,$1
	xor $2,$1,$2
	ldq $1,64($15)
	and $1,1,$1
	beq $1,$L210
	ldah $1,-26359($31)
	lda $1,-20257($1)
	zapnot $1,15,$1
	br $31,$L211
$L210:
	mov $31,$1
$L211:
	xor $1,$2,$2
	ldq $1,80($15)
	stq $2,4984($1)
	ldq $1,80($15)
	stq $31,4992($1)
	bis $31,$31,$31
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,96($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4681:
	.end _ZNSt23mersenne_twister_engineImLm32ELm624ELm397ELm31ELm2567483615ELm11ELm4294967295ELm7ELm2636928640ELm15ELm4022730752ELm18ELm1812433253EE11_M_gen_randEv
	.section	.text._ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC2IS0_EERKSaIT_E,"axG",@progbits,_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC5IS0_EERKSaIT_E,comdat
	.align 2
	.weak	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC2IS0_EERKSaIT_E
	.ent _ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC2IS0_EERKSaIT_E
_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC2IS0_EERKSaIT_E:
	.eflag 48
	.frame $15,32,$26,0
	.mask 0x4008000,-32
$LFB4684:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!503
	lda $29,0($29)		!gpdisp!503
$_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC2IS0_EERKSaIT_E..ng:
	lda $30,-32($30)
	.cfi_def_cfa_offset 32
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -32
	.cfi_offset 15, -24
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,16($15)
	stq $17,24($15)
	ldq $16,16($15)
	ldq $27,_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC2Ev($29)		!literal!504
	jsr $26,($27),_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC2Ev		!lituse_jsr!504
	ldah $29,0($26)		!gpdisp!505
	lda $29,0($29)		!gpdisp!505
	bis $31,$31,$31
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,32($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4684:
	.end _ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC2IS0_EERKSaIT_E
	.weak	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC1IS0_EERKSaIT_E
$_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC1IS0_EERKSaIT_E..ng = $_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC2IS0_EERKSaIT_E..ng
_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC1IS0_EERKSaIT_E = _ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC2IS0_EERKSaIT_E
	.section	.text._ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED2Ev,"axG",@progbits,_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED5Ev,comdat
	.align 2
	.weak	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED2Ev
	.ent _ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED2Ev
_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED2Ev:
	.eflag 48
	.frame $15,32,$26,0
	.mask 0x4008000,-32
$LFB4687:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!506
	lda $29,0($29)		!gpdisp!506
$_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED2Ev..ng:
	lda $30,-32($30)
	.cfi_def_cfa_offset 32
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -32
	.cfi_offset 15, -24
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,16($15)
	ldq $16,16($15)
	ldq $27,_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED2Ev($29)		!literal!507
	jsr $26,($27),_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED2Ev		!lituse_jsr!507
	ldah $29,0($26)		!gpdisp!508
	lda $29,0($29)		!gpdisp!508
	bis $31,$31,$31
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,32($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4687:
	.end _ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED2Ev
	.weak	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED1Ev
$_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED1Ev..ng = $_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED2Ev..ng
_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED1Ev = _ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED2Ev
	.section	.text._ZSt18__allocate_guardedISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEESt15__allocated_ptrIT_ERS8_,"axG",@progbits,_ZSt18__allocate_guardedISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEESt15__allocated_ptrIT_ERS8_,comdat
	.align 2
	.weak	_ZSt18__allocate_guardedISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEESt15__allocated_ptrIT_ERS8_
	.ent _ZSt18__allocate_guardedISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEESt15__allocated_ptrIT_ERS8_
_ZSt18__allocate_guardedISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEESt15__allocated_ptrIT_ERS8_:
	.eflag 48
	.frame $15,32,$26,0
	.mask 0x4008000,-32
$LFB4689:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!509
	lda $29,0($29)		!gpdisp!509
$_ZSt18__allocate_guardedISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEESt15__allocated_ptrIT_ERS8_..ng:
	lda $30,-32($30)
	.cfi_def_cfa_offset 32
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -32
	.cfi_offset 15, -24
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,16($15)
	stq $17,24($15)
	lda $17,1($31)
	ldq $16,24($15)
	ldq $27,_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE8allocateERS6_m($29)		!literal!510
	jsr $26,($27),0		!lituse_jsr!510
	ldah $29,0($26)		!gpdisp!511
	lda $29,0($29)		!gpdisp!511
	mov $0,$1
	mov $1,$18
	ldq $17,24($15)
	ldq $16,16($15)
	ldq $27,_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC1ERS6_PS5_($29)		!literal!512
	jsr $26,($27),_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC1ERS6_PS5_		!lituse_jsr!512
	ldah $29,0($26)		!gpdisp!513
	lda $29,0($29)		!gpdisp!513
	ldq $0,16($15)
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,32($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4689:
	.end _ZSt18__allocate_guardedISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEESt15__allocated_ptrIT_ERS8_
	.section	.text._ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED2Ev,"axG",@progbits,_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED5Ev,comdat
	.align 2
	.weak	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED2Ev
	.ent _ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED2Ev
_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED2Ev:
	.eflag 48
	.frame $15,32,$26,0
	.mask 0x4008000,-32
$LFB4691:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,$LLSDA4691
	ldah $29,0($27)		!gpdisp!514
	lda $29,0($29)		!gpdisp!514
$_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED2Ev..ng:
	lda $30,-32($30)
	.cfi_def_cfa_offset 32
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -32
	.cfi_offset 15, -24
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,16($15)
	ldq $1,16($15)
	ldq $1,8($1)
	beq $1,$L218
	ldq $1,16($15)
	ldq $2,0($1)
	ldq $1,16($15)
	ldq $1,8($1)
	lda $18,1($31)
	mov $1,$17
	mov $2,$16
	ldq $27,_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE10deallocateERS6_PS5_m($29)		!literal!515
	jsr $26,($27),_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE10deallocateERS6_PS5_m		!lituse_jsr!515
	ldah $29,0($26)		!gpdisp!516
	lda $29,0($29)		!gpdisp!516
$L218:
	bis $31,$31,$31
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,32($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4691:
	.section	.gcc_except_table
$LLSDA4691:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 $LLSDACSE4691-$LLSDACSB4691
$LLSDACSB4691:
$LLSDACSE4691:
	.section	.text._ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED2Ev,"axG",@progbits,_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED5Ev,comdat
	.end _ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED2Ev
	.weak	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED1Ev
$_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED1Ev..ng = $_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED2Ev..ng
_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED1Ev = _ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED2Ev
	.section	.text._ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE3getEv,"axG",@progbits,_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE3getEv,comdat
	.align 2
	.weak	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE3getEv
	.ent _ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE3getEv
_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE3getEv:
	.eflag 48
	.frame $15,32,$26,0
	.mask 0x4008000,-32
$LFB4696:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!517
	lda $29,0($29)		!gpdisp!517
$_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE3getEv..ng:
	lda $30,-32($30)
	.cfi_def_cfa_offset 32
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -32
	.cfi_offset 15, -24
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,16($15)
	ldq $1,16($15)
	ldq $1,8($1)
	mov $1,$16
	ldq $27,_ZSt12__to_addressISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEPT_S7_($29)		!literal!518
	jsr $26,($27),0		!lituse_jsr!518
	ldah $29,0($26)		!gpdisp!519
	lda $29,0($29)		!gpdisp!519
	mov $0,$1
	mov $1,$0
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,32($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4696:
	.end _ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE3getEv
	.section	.text._ZNSaI6sphereEC2ERKS0_,"axG",@progbits,_ZNSaI6sphereEC5ERKS0_,comdat
	.align 2
	.weak	_ZNSaI6sphereEC2ERKS0_
	.ent _ZNSaI6sphereEC2ERKS0_
_ZNSaI6sphereEC2ERKS0_:
	.eflag 48
	.frame $15,32,$26,0
	.mask 0x4008000,-32
$LFB4698:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!520
	lda $29,0($29)		!gpdisp!520
$_ZNSaI6sphereEC2ERKS0_..ng:
	lda $30,-32($30)
	.cfi_def_cfa_offset 32
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -32
	.cfi_offset 15, -24
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,16($15)
	stq $17,24($15)
	ldq $17,24($15)
	ldq $16,16($15)
	ldq $27,_ZN9__gnu_cxx13new_allocatorI6sphereEC2ERKS2_($29)		!literal!521
	jsr $26,($27),_ZN9__gnu_cxx13new_allocatorI6sphereEC2ERKS2_		!lituse_jsr!521
	ldah $29,0($26)		!gpdisp!522
	lda $29,0($29)		!gpdisp!522
	bis $31,$31,$31
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,32($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4698:
	.end _ZNSaI6sphereEC2ERKS0_
	.weak	_ZNSaI6sphereEC1ERKS0_
$_ZNSaI6sphereEC1ERKS0_..ng = $_ZNSaI6sphereEC2ERKS0_..ng
_ZNSaI6sphereEC1ERKS0_ = _ZNSaI6sphereEC2ERKS0_
	.section	.text._ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED2Ev,"axG",@progbits,_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED5Ev,comdat
	.align 2
	.weak	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED2Ev
	.ent _ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED2Ev
_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED2Ev:
	.eflag 48
	.frame $15,32,$26,0
	.mask 0x4008000,-32
$LFB4703:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!523
	lda $29,0($29)		!gpdisp!523
$_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED2Ev..ng:
	lda $30,-32($30)
	.cfi_def_cfa_offset 32
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -32
	.cfi_offset 15, -24
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,16($15)
	ldq $16,16($15)
	ldq $27,_ZNSaI6sphereED2Ev($29)		!literal!524
	jsr $26,($27),_ZNSaI6sphereED2Ev		!lituse_jsr!524
	ldah $29,0($26)		!gpdisp!525
	lda $29,0($29)		!gpdisp!525
	bis $31,$31,$31
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,32($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4703:
	.end _ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED2Ev
	.weak	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED1Ev
$_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED1Ev..ng = $_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED2Ev..ng
_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED1Ev = _ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED2Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD2Ev,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD5Ev,comdat
	.align 2
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD2Ev
	.ent _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD2Ev
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD2Ev:
	.eflag 48
	.frame $15,32,$26,0
	.mask 0x4008000,-32
$LFB4705:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!526
	lda $29,0($29)		!gpdisp!526
$_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD2Ev..ng:
	lda $30,-32($30)
	.cfi_def_cfa_offset 32
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -32
	.cfi_offset 15, -24
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,16($15)
	ldq $16,16($15)
	ldq $27,_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED2Ev($29)		!literal!527
	jsr $26,($27),_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED2Ev		!lituse_jsr!527
	ldah $29,0($26)		!gpdisp!528
	lda $29,0($29)		!gpdisp!528
	bis $31,$31,$31
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,32($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4705:
	.end _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD2Ev
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD1Ev
$_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD1Ev..ng = $_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD2Ev..ng
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD1Ev = _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD2Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3dEEES1_DpOT_,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC5IJ4vec3dEEES1_DpOT_,comdat
	.align 2
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3dEEES1_DpOT_
	.ent _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3dEEES1_DpOT_
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3dEEES1_DpOT_:
	.eflag 48
	.frame $15,80,$26,0
	.mask 0x4008600,-80
$LFB4707:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,$LLSDA4707
	ldah $29,0($27)		!gpdisp!529
	lda $29,0($29)		!gpdisp!529
$_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3dEEES1_DpOT_..ng:
	lda $30,-80($30)
	.cfi_def_cfa_offset 80
	stq $26,0($30)
	stq $9,8($30)
	stq $10,16($30)
	stq $15,24($30)
	.cfi_offset 26, -80
	.cfi_offset 9, -72
	.cfi_offset 10, -64
	.cfi_offset 15, -56
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,48($15)
	stq $17,56($15)
	stq $18,64($15)
	stq $19,72($15)
	ldq $1,48($15)
	mov $1,$16
	ldq $27,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC2Ev($29)		!literal!531
	jsr $26,($27),_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC2Ev		!lituse_jsr!531
	ldah $29,0($26)		!gpdisp!532
	lda $29,0($29)		!gpdisp!532
	ldq $1,_ZTVSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE($29)		!literal
	lda $2,16($1)
	ldq $1,48($15)
	stq $2,0($1)
	ldq $1,48($15)
	lda $9,16($1)
	ldq $17,56($15)
	lda $16,32($15)
	ldq $27,_ZNSaI6sphereEC1ERKS0_($29)		!literal!533
	jsr $26,($27),_ZNSaI6sphereEC1ERKS0_		!lituse_jsr!533
	ldah $29,0($26)		!gpdisp!534
	lda $29,0($29)		!gpdisp!534
	lda $17,32($15)
	mov $9,$16
	ldq $27,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC1ES1_($29)		!literal!535
	jsr $26,($27),_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC1ES1_		!lituse_jsr!535
	ldah $29,0($26)		!gpdisp!536
	lda $29,0($29)		!gpdisp!536
	lda $16,32($15)
	ldq $27,_ZNSaI6sphereED1Ev($29)		!literal!537
	jsr $26,($27),_ZNSaI6sphereED1Ev		!lituse_jsr!537
	ldah $29,0($26)		!gpdisp!538
	lda $29,0($29)		!gpdisp!538
	ldq $16,48($15)
	ldq $27,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv($29)		!literal!539
	jsr $26,($27),0		!lituse_jsr!539
	ldah $29,0($26)		!gpdisp!540
	lda $29,0($29)		!gpdisp!540
	mov $0,$9
	ldq $16,64($15)
	ldq $27,_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE($29)		!literal!541
	jsr $26,($27),0		!lituse_jsr!541
	ldah $29,0($26)		!gpdisp!542
	lda $29,0($29)		!gpdisp!542
	mov $0,$10
	ldq $16,72($15)
	ldq $27,_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE($29)		!literal!543
	jsr $26,($27),0		!lituse_jsr!543
	ldah $29,0($26)		!gpdisp!544
	lda $29,0($29)		!gpdisp!544
	mov $0,$1
	mov $1,$19
	mov $10,$18
	mov $9,$17
	ldq $16,56($15)
$LEHB26:
	ldq $27,_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3dEEEvRS1_PT_DpOT0_($29)		!literal!545
	jsr $26,($27),_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3dEEEvRS1_PT_DpOT0_		!lituse_jsr!545
	ldah $29,0($26)		!gpdisp!546
	lda $29,0($29)		!gpdisp!546
$LEHE26:
	br $31,$L227
$L226:
	ldah $29,0($26)		!gpdisp!530
	lda $29,0($29)		!gpdisp!530
	mov $16,$9
	ldq $1,48($15)
	lda $1,16($1)
	mov $1,$16
	ldq $27,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD1Ev($29)		!literal!547
	jsr $26,($27),_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD1Ev		!lituse_jsr!547
	ldah $29,0($26)		!gpdisp!548
	lda $29,0($29)		!gpdisp!548
	ldq $1,48($15)
	mov $1,$16
	ldq $27,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev($29)		!literal!549
	jsr $26,($27),_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev		!lituse_jsr!549
	ldah $29,0($26)		!gpdisp!550
	lda $29,0($29)		!gpdisp!550
	mov $9,$1
	mov $1,$16
$LEHB27:
	ldq $27,_Unwind_Resume($29)		!literal!551
	jsr $26,($27),_Unwind_Resume		!lituse_jsr!551
$LEHE27:
$L227:
	mov $15,$30
	ldq $26,0($30)
	ldq $9,8($30)
	ldq $10,16($30)
	ldq $15,24($30)
	lda $30,80($30)
	.cfi_restore 15
	.cfi_restore 10
	.cfi_restore 9
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4707:
	.section	.gcc_except_table
$LLSDA4707:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 $LLSDACSE4707-$LLSDACSB4707
$LLSDACSB4707:
	.uleb128 $LEHB26-$LFB4707
	.uleb128 $LEHE26-$LEHB26
	.uleb128 $L226-$LFB4707
	.uleb128 0
	.uleb128 $LEHB27-$LFB4707
	.uleb128 $LEHE27-$LEHB27
	.uleb128 0
	.uleb128 0
$LLSDACSE4707:
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3dEEES1_DpOT_,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC5IJ4vec3dEEES1_DpOT_,comdat
	.end _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3dEEES1_DpOT_
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC1IJ4vec3dEEES1_DpOT_
$_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC1IJ4vec3dEEES1_DpOT_..ng = $_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3dEEES1_DpOT_..ng
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC1IJ4vec3dEEES1_DpOT_ = _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3dEEES1_DpOT_
	.section	.text._ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEaSEDn,"axG",@progbits,_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEaSEDn,comdat
	.align 2
	.weak	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEaSEDn
	.ent _ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEaSEDn
$_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEaSEDn..ng:
_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEaSEDn:
	.eflag 48
	.frame $15,32,$26,0
	.mask 0x4008000,-32
$LFB4709:
	.cfi_startproc
	lda $30,-32($30)
	.cfi_def_cfa_offset 32
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -32
	.cfi_offset 15, -24
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 0
	stq $16,16($15)
	stq $17,24($15)
	ldq $1,16($15)
	stq $31,8($1)
	ldq $1,16($15)
	mov $1,$0
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,32($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4709:
	.end _ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEaSEDn
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv,comdat
	.align 2
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv
	.ent _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv:
	.eflag 48
	.frame $15,32,$26,0
	.mask 0x4008000,-32
$LFB4710:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!552
	lda $29,0($29)		!gpdisp!552
$_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv..ng:
	lda $30,-32($30)
	.cfi_def_cfa_offset 32
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -32
	.cfi_offset 15, -24
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,16($15)
	ldq $1,16($15)
	lda $1,16($1)
	mov $1,$16
	ldq $27,_ZN9__gnu_cxx16__aligned_bufferI6sphereE6_M_ptrEv($29)		!literal!553
	jsr $26,($27),0		!lituse_jsr!553
	ldah $29,0($26)		!gpdisp!554
	lda $29,0($29)		!gpdisp!554
	mov $0,$1
	mov $1,$0
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,32($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4710:
	.end _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3iEEES1_DpOT_,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC5IJ4vec3iEEES1_DpOT_,comdat
	.align 2
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3iEEES1_DpOT_
	.ent _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3iEEES1_DpOT_
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3iEEES1_DpOT_:
	.eflag 48
	.frame $15,80,$26,0
	.mask 0x4008600,-80
$LFB4712:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,$LLSDA4712
	ldah $29,0($27)		!gpdisp!555
	lda $29,0($29)		!gpdisp!555
$_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3iEEES1_DpOT_..ng:
	lda $30,-80($30)
	.cfi_def_cfa_offset 80
	stq $26,0($30)
	stq $9,8($30)
	stq $10,16($30)
	stq $15,24($30)
	.cfi_offset 26, -80
	.cfi_offset 9, -72
	.cfi_offset 10, -64
	.cfi_offset 15, -56
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,48($15)
	stq $17,56($15)
	stq $18,64($15)
	stq $19,72($15)
	ldq $1,48($15)
	mov $1,$16
	ldq $27,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC2Ev($29)		!literal!557
	jsr $26,($27),_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC2Ev		!lituse_jsr!557
	ldah $29,0($26)		!gpdisp!558
	lda $29,0($29)		!gpdisp!558
	ldq $1,_ZTVSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE($29)		!literal
	lda $2,16($1)
	ldq $1,48($15)
	stq $2,0($1)
	ldq $1,48($15)
	lda $9,16($1)
	ldq $17,56($15)
	lda $16,32($15)
	ldq $27,_ZNSaI6sphereEC1ERKS0_($29)		!literal!559
	jsr $26,($27),_ZNSaI6sphereEC1ERKS0_		!lituse_jsr!559
	ldah $29,0($26)		!gpdisp!560
	lda $29,0($29)		!gpdisp!560
	lda $17,32($15)
	mov $9,$16
	ldq $27,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC1ES1_($29)		!literal!561
	jsr $26,($27),_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC1ES1_		!lituse_jsr!561
	ldah $29,0($26)		!gpdisp!562
	lda $29,0($29)		!gpdisp!562
	lda $16,32($15)
	ldq $27,_ZNSaI6sphereED1Ev($29)		!literal!563
	jsr $26,($27),_ZNSaI6sphereED1Ev		!lituse_jsr!563
	ldah $29,0($26)		!gpdisp!564
	lda $29,0($29)		!gpdisp!564
	ldq $16,48($15)
	ldq $27,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv($29)		!literal!565
	jsr $26,($27),0		!lituse_jsr!565
	ldah $29,0($26)		!gpdisp!566
	lda $29,0($29)		!gpdisp!566
	mov $0,$9
	ldq $16,64($15)
	ldq $27,_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE($29)		!literal!567
	jsr $26,($27),0		!lituse_jsr!567
	ldah $29,0($26)		!gpdisp!568
	lda $29,0($29)		!gpdisp!568
	mov $0,$10
	ldq $16,72($15)
	ldq $27,_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE($29)		!literal!569
	jsr $26,($27),0		!lituse_jsr!569
	ldah $29,0($26)		!gpdisp!570
	lda $29,0($29)		!gpdisp!570
	mov $0,$1
	mov $1,$19
	mov $10,$18
	mov $9,$17
	ldq $16,56($15)
$LEHB28:
	ldq $27,_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3iEEEvRS1_PT_DpOT0_($29)		!literal!571
	jsr $26,($27),_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3iEEEvRS1_PT_DpOT0_		!lituse_jsr!571
	ldah $29,0($26)		!gpdisp!572
	lda $29,0($29)		!gpdisp!572
$LEHE28:
	br $31,$L235
$L234:
	ldah $29,0($26)		!gpdisp!556
	lda $29,0($29)		!gpdisp!556
	mov $16,$9
	ldq $1,48($15)
	lda $1,16($1)
	mov $1,$16
	ldq $27,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD1Ev($29)		!literal!573
	jsr $26,($27),_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD1Ev		!lituse_jsr!573
	ldah $29,0($26)		!gpdisp!574
	lda $29,0($29)		!gpdisp!574
	ldq $1,48($15)
	mov $1,$16
	ldq $27,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev($29)		!literal!575
	jsr $26,($27),_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev		!lituse_jsr!575
	ldah $29,0($26)		!gpdisp!576
	lda $29,0($29)		!gpdisp!576
	mov $9,$1
	mov $1,$16
$LEHB29:
	ldq $27,_Unwind_Resume($29)		!literal!577
	jsr $26,($27),_Unwind_Resume		!lituse_jsr!577
$LEHE29:
$L235:
	mov $15,$30
	ldq $26,0($30)
	ldq $9,8($30)
	ldq $10,16($30)
	ldq $15,24($30)
	lda $30,80($30)
	.cfi_restore 15
	.cfi_restore 10
	.cfi_restore 9
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4712:
	.section	.gcc_except_table
$LLSDA4712:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 $LLSDACSE4712-$LLSDACSB4712
$LLSDACSB4712:
	.uleb128 $LEHB28-$LFB4712
	.uleb128 $LEHE28-$LEHB28
	.uleb128 $L234-$LFB4712
	.uleb128 0
	.uleb128 $LEHB29-$LFB4712
	.uleb128 $LEHE29-$LEHB29
	.uleb128 0
	.uleb128 0
$LLSDACSE4712:
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3iEEES1_DpOT_,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC5IJ4vec3iEEES1_DpOT_,comdat
	.end _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3iEEES1_DpOT_
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC1IJ4vec3iEEES1_DpOT_
$_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC1IJ4vec3iEEES1_DpOT_..ng = $_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3iEEES1_DpOT_..ng
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC1IJ4vec3iEEES1_DpOT_ = _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3iEEES1_DpOT_
	.section	.text._ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC2Ev,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC5Ev,comdat
	.align 2
	.weak	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC2Ev
	.ent _ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC2Ev
$_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC2Ev..ng:
_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC2Ev:
	.eflag 48
	.frame $15,32,$26,0
	.mask 0x4008000,-32
$LFB4725:
	.cfi_startproc
	lda $30,-32($30)
	.cfi_def_cfa_offset 32
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -32
	.cfi_offset 15, -24
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 0
	stq $16,16($15)
	bis $31,$31,$31
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,32($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4725:
	.end _ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC2Ev
	.weak	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC1Ev
$_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC1Ev..ng = $_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC2Ev..ng
_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC1Ev = _ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC2Ev
	.section	.text._ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED2Ev,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED5Ev,comdat
	.align 2
	.weak	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED2Ev
	.ent _ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED2Ev
$_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED2Ev..ng:
_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED2Ev:
	.eflag 48
	.frame $15,32,$26,0
	.mask 0x4008000,-32
$LFB4728:
	.cfi_startproc
	lda $30,-32($30)
	.cfi_def_cfa_offset 32
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -32
	.cfi_offset 15, -24
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 0
	stq $16,16($15)
	bis $31,$31,$31
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,32($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4728:
	.end _ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED2Ev
	.weak	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED1Ev
$_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED1Ev..ng = $_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED2Ev..ng
_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED1Ev = _ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED2Ev
	.section	.text._ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE8allocateERS6_m,"axG",@progbits,_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE8allocateERS6_m,comdat
	.align 2
	.weak	_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE8allocateERS6_m
	.ent _ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE8allocateERS6_m
_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE8allocateERS6_m:
	.eflag 48
	.frame $15,32,$26,0
	.mask 0x4008000,-32
$LFB4730:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!578
	lda $29,0($29)		!gpdisp!578
$_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE8allocateERS6_m..ng:
	lda $30,-32($30)
	.cfi_def_cfa_offset 32
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -32
	.cfi_offset 15, -24
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,16($15)
	stq $17,24($15)
	mov $31,$18
	ldq $17,24($15)
	ldq $16,16($15)
	ldq $27,_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8allocateEmPKv($29)		!literal!579
	jsr $26,($27),0		!lituse_jsr!579
	ldah $29,0($26)		!gpdisp!580
	lda $29,0($29)		!gpdisp!580
	mov $0,$1
	mov $1,$0
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,32($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4730:
	.end _ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE8allocateERS6_m
	.section	.text._ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC2ERS6_PS5_,"axG",@progbits,_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC5ERS6_PS5_,comdat
	.align 2
	.weak	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC2ERS6_PS5_
	.ent _ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC2ERS6_PS5_
_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC2ERS6_PS5_:
	.eflag 48
	.frame $15,48,$26,0
	.mask 0x4008000,-48
$LFB4732:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!581
	lda $29,0($29)		!gpdisp!581
$_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC2ERS6_PS5_..ng:
	lda $30,-48($30)
	.cfi_def_cfa_offset 48
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -48
	.cfi_offset 15, -40
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,16($15)
	stq $17,24($15)
	stq $18,32($15)
	ldq $16,24($15)
	ldq $27,_ZSt11__addressofISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEPT_RS7_($29)		!literal!582
	jsr $26,($27),0		!lituse_jsr!582
	ldah $29,0($26)		!gpdisp!583
	lda $29,0($29)		!gpdisp!583
	mov $0,$1
	ldq $2,16($15)
	stq $1,0($2)
	ldq $1,16($15)
	ldq $2,32($15)
	stq $2,8($1)
	bis $31,$31,$31
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,48($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4732:
	.end _ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC2ERS6_PS5_
	.weak	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC1ERS6_PS5_
$_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC1ERS6_PS5_..ng = $_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC2ERS6_PS5_..ng
_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC1ERS6_PS5_ = _ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC2ERS6_PS5_
	.section	.text._ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE10deallocateERS6_PS5_m,"axG",@progbits,_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE10deallocateERS6_PS5_m,comdat
	.align 2
	.weak	_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE10deallocateERS6_PS5_m
	.ent _ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE10deallocateERS6_PS5_m
_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE10deallocateERS6_PS5_m:
	.eflag 48
	.frame $15,48,$26,0
	.mask 0x4008000,-48
$LFB4734:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!584
	lda $29,0($29)		!gpdisp!584
$_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE10deallocateERS6_PS5_m..ng:
	lda $30,-48($30)
	.cfi_def_cfa_offset 48
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -48
	.cfi_offset 15, -40
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,16($15)
	stq $17,24($15)
	stq $18,32($15)
	ldq $18,32($15)
	ldq $17,24($15)
	ldq $16,16($15)
	ldq $27,_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE10deallocateEPS5_m($29)		!literal!585
	jsr $26,($27),_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE10deallocateEPS5_m		!lituse_jsr!585
	ldah $29,0($26)		!gpdisp!586
	lda $29,0($29)		!gpdisp!586
	bis $31,$31,$31
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,48($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4734:
	.end _ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE10deallocateERS6_PS5_m
	.section	.text._ZSt12__to_addressISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEPT_S7_,"axG",@progbits,_ZSt12__to_addressISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEPT_S7_,comdat
	.align 2
	.weak	_ZSt12__to_addressISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEPT_S7_
	.ent _ZSt12__to_addressISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEPT_S7_
$_ZSt12__to_addressISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEPT_S7_..ng:
_ZSt12__to_addressISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEPT_S7_:
	.eflag 48
	.frame $15,32,$26,0
	.mask 0x4008000,-32
$LFB4735:
	.cfi_startproc
	lda $30,-32($30)
	.cfi_def_cfa_offset 32
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -32
	.cfi_offset 15, -24
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 0
	stq $16,16($15)
	ldq $1,16($15)
	mov $1,$0
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,32($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4735:
	.end _ZSt12__to_addressISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEPT_S7_
	.section	.text._ZN9__gnu_cxx13new_allocatorI6sphereEC2ERKS2_,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorI6sphereEC5ERKS2_,comdat
	.align 2
	.weak	_ZN9__gnu_cxx13new_allocatorI6sphereEC2ERKS2_
	.ent _ZN9__gnu_cxx13new_allocatorI6sphereEC2ERKS2_
$_ZN9__gnu_cxx13new_allocatorI6sphereEC2ERKS2_..ng:
_ZN9__gnu_cxx13new_allocatorI6sphereEC2ERKS2_:
	.eflag 48
	.frame $15,32,$26,0
	.mask 0x4008000,-32
$LFB4737:
	.cfi_startproc
	lda $30,-32($30)
	.cfi_def_cfa_offset 32
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -32
	.cfi_offset 15, -24
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 0
	stq $16,16($15)
	stq $17,24($15)
	bis $31,$31,$31
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,32($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4737:
	.end _ZN9__gnu_cxx13new_allocatorI6sphereEC2ERKS2_
	.weak	_ZN9__gnu_cxx13new_allocatorI6sphereEC1ERKS2_
$_ZN9__gnu_cxx13new_allocatorI6sphereEC1ERKS2_..ng = $_ZN9__gnu_cxx13new_allocatorI6sphereEC2ERKS2_..ng
_ZN9__gnu_cxx13new_allocatorI6sphereEC1ERKS2_ = _ZN9__gnu_cxx13new_allocatorI6sphereEC2ERKS2_
	.section	.text._ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC2Ev,"axG",@progbits,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC5Ev,comdat
	.align 2
	.weak	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC2Ev
	.ent _ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC2Ev
_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC2Ev:
	.eflag 48
	.frame $15,32,$26,0
	.mask 0x4008000,-32
$LFB4740:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!587
	lda $29,0($29)		!gpdisp!587
$_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC2Ev..ng:
	lda $30,-32($30)
	.cfi_def_cfa_offset 32
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -32
	.cfi_offset 15, -24
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,16($15)
	ldq $1,_ZTVSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE($29)		!literal
	lda $2,16($1)
	ldq $1,16($15)
	stq $2,0($1)
	ldq $1,16($15)
	lda $2,1($31)
	stl $2,8($1)
	ldq $1,16($15)
	lda $2,1($31)
	stl $2,12($1)
	bis $31,$31,$31
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,32($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4740:
	.end _ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC2Ev
	.weak	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC1Ev
$_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC1Ev..ng = $_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC2Ev..ng
_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC1Ev = _ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC2Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC2ES1_,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC5ES1_,comdat
	.align 2
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC2ES1_
	.ent _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC2ES1_
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC2ES1_:
	.eflag 48
	.frame $15,32,$26,0
	.mask 0x4008000,-32
$LFB4743:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!588
	lda $29,0($29)		!gpdisp!588
$_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC2ES1_..ng:
	lda $30,-32($30)
	.cfi_def_cfa_offset 32
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -32
	.cfi_offset 15, -24
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,16($15)
	stq $17,24($15)
	ldq $17,24($15)
	ldq $16,16($15)
	ldq $27,_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC2ERKS1_($29)		!literal!589
	jsr $26,($27),_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC2ERKS1_		!lituse_jsr!589
	ldah $29,0($26)		!gpdisp!590
	lda $29,0($29)		!gpdisp!590
	bis $31,$31,$31
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,32($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4743:
	.end _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC2ES1_
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC1ES1_
$_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC1ES1_..ng = $_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC2ES1_..ng
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC1ES1_ = _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC2ES1_
	.section	.text._ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3dEEEvRS1_PT_DpOT0_,"axG",@progbits,_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3dEEEvRS1_PT_DpOT0_,comdat
	.align 2
	.weak	_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3dEEEvRS1_PT_DpOT0_
	.ent _ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3dEEEvRS1_PT_DpOT0_
_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3dEEEvRS1_PT_DpOT0_:
	.eflag 48
	.frame $15,64,$26,0
	.mask 0x4008200,-64
$LFB4745:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!591
	lda $29,0($29)		!gpdisp!591
$_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3dEEEvRS1_PT_DpOT0_..ng:
	lda $30,-64($30)
	.cfi_def_cfa_offset 64
	stq $26,0($30)
	stq $9,8($30)
	stq $15,16($30)
	.cfi_offset 26, -64
	.cfi_offset 9, -56
	.cfi_offset 15, -48
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,32($15)
	stq $17,40($15)
	stq $18,48($15)
	stq $19,56($15)
	ldq $16,48($15)
	ldq $27,_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE($29)		!literal!592
	jsr $26,($27),0		!lituse_jsr!592
	ldah $29,0($26)		!gpdisp!593
	lda $29,0($29)		!gpdisp!593
	mov $0,$9
	ldq $16,56($15)
	ldq $27,_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE($29)		!literal!594
	jsr $26,($27),0		!lituse_jsr!594
	ldah $29,0($26)		!gpdisp!595
	lda $29,0($29)		!gpdisp!595
	mov $0,$1
	mov $1,$19
	mov $9,$18
	ldq $17,40($15)
	ldq $16,32($15)
	ldq $27,_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_($29)		!literal!596
	jsr $26,($27),_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_		!lituse_jsr!596
	ldah $29,0($26)		!gpdisp!597
	lda $29,0($29)		!gpdisp!597
	bis $31,$31,$31
	mov $15,$30
	ldq $26,0($30)
	ldq $9,8($30)
	ldq $15,16($30)
	lda $30,64($30)
	.cfi_restore 15
	.cfi_restore 9
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4745:
	.end _ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3dEEEvRS1_PT_DpOT0_
	.section	.text._ZN9__gnu_cxx16__aligned_bufferI6sphereE6_M_ptrEv,"axG",@progbits,_ZN9__gnu_cxx16__aligned_bufferI6sphereE6_M_ptrEv,comdat
	.align 2
	.weak	_ZN9__gnu_cxx16__aligned_bufferI6sphereE6_M_ptrEv
	.ent _ZN9__gnu_cxx16__aligned_bufferI6sphereE6_M_ptrEv
_ZN9__gnu_cxx16__aligned_bufferI6sphereE6_M_ptrEv:
	.eflag 48
	.frame $15,32,$26,0
	.mask 0x4008000,-32
$LFB4746:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!598
	lda $29,0($29)		!gpdisp!598
$_ZN9__gnu_cxx16__aligned_bufferI6sphereE6_M_ptrEv..ng:
	lda $30,-32($30)
	.cfi_def_cfa_offset 32
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -32
	.cfi_offset 15, -24
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,16($15)
	ldq $16,16($15)
	ldq $27,_ZN9__gnu_cxx16__aligned_bufferI6sphereE7_M_addrEv($29)		!literal!599
	jsr $26,($27),0		!lituse_jsr!599
	ldah $29,0($26)		!gpdisp!600
	lda $29,0($29)		!gpdisp!600
	mov $0,$1
	mov $1,$0
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,32($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4746:
	.end _ZN9__gnu_cxx16__aligned_bufferI6sphereE6_M_ptrEv
	.section	.text._ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3iEEEvRS1_PT_DpOT0_,"axG",@progbits,_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3iEEEvRS1_PT_DpOT0_,comdat
	.align 2
	.weak	_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3iEEEvRS1_PT_DpOT0_
	.ent _ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3iEEEvRS1_PT_DpOT0_
_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3iEEEvRS1_PT_DpOT0_:
	.eflag 48
	.frame $15,64,$26,0
	.mask 0x4008200,-64
$LFB4747:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!601
	lda $29,0($29)		!gpdisp!601
$_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3iEEEvRS1_PT_DpOT0_..ng:
	lda $30,-64($30)
	.cfi_def_cfa_offset 64
	stq $26,0($30)
	stq $9,8($30)
	stq $15,16($30)
	.cfi_offset 26, -64
	.cfi_offset 9, -56
	.cfi_offset 15, -48
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,32($15)
	stq $17,40($15)
	stq $18,48($15)
	stq $19,56($15)
	ldq $16,48($15)
	ldq $27,_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE($29)		!literal!602
	jsr $26,($27),0		!lituse_jsr!602
	ldah $29,0($26)		!gpdisp!603
	lda $29,0($29)		!gpdisp!603
	mov $0,$9
	ldq $16,56($15)
	ldq $27,_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE($29)		!literal!604
	jsr $26,($27),0		!lituse_jsr!604
	ldah $29,0($26)		!gpdisp!605
	lda $29,0($29)		!gpdisp!605
	mov $0,$1
	mov $1,$19
	mov $9,$18
	ldq $17,40($15)
	ldq $16,32($15)
	ldq $27,_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_($29)		!literal!606
	jsr $26,($27),_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_		!lituse_jsr!606
	ldah $29,0($26)		!gpdisp!607
	lda $29,0($29)		!gpdisp!607
	bis $31,$31,$31
	mov $15,$30
	ldq $26,0($30)
	ldq $9,8($30)
	ldq $15,16($30)
	lda $30,64($30)
	.cfi_restore 15
	.cfi_restore 9
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4747:
	.end _ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3iEEEvRS1_PT_DpOT0_
	.section	.text._ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8allocateEmPKv,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8allocateEmPKv,comdat
	.align 2
	.weak	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8allocateEmPKv
	.ent _ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8allocateEmPKv
_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8allocateEmPKv:
	.eflag 48
	.frame $15,48,$26,0
	.mask 0x4008000,-48
$LFB4750:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!608
	lda $29,0($29)		!gpdisp!608
$_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8allocateEmPKv..ng:
	lda $30,-48($30)
	.cfi_def_cfa_offset 48
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -48
	.cfi_offset 15, -40
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,16($15)
	stq $17,24($15)
	stq $18,32($15)
	ldq $16,16($15)
	ldq $27,_ZNK9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8max_sizeEv($29)		!literal!609
	jsr $26,($27),0		!lituse_jsr!609
	ldah $29,0($26)		!gpdisp!610
	lda $29,0($29)		!gpdisp!610
	mov $0,$1
	ldq $2,24($15)
	cmpult $1,$2,$1
	and $1,0xff,$1
	beq $1,$L252
	ldq $27,_ZSt17__throw_bad_allocv($29)		!literal!611
	jsr $26,($27),_ZSt17__throw_bad_allocv		!lituse_jsr!611
$L252:
	ldq $2,24($15)
	mov $2,$1
	s8addq $1,0,$1
	addq $1,$2,$1
	s8addq $1,0,$1
	mov $1,$16
	ldq $27,_Znwm($29)		!literal!612
	jsr $26,($27),0		!lituse_jsr!612
	ldah $29,0($26)		!gpdisp!613
	lda $29,0($29)		!gpdisp!613
	mov $0,$1
	mov $1,$0
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,48($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4750:
	.end _ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8allocateEmPKv
	.section	.text._ZSt11__addressofISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEPT_RS7_,"axG",@progbits,_ZSt11__addressofISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEPT_RS7_,comdat
	.align 2
	.weak	_ZSt11__addressofISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEPT_RS7_
	.ent _ZSt11__addressofISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEPT_RS7_
$_ZSt11__addressofISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEPT_RS7_..ng:
_ZSt11__addressofISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEPT_RS7_:
	.eflag 48
	.frame $15,32,$26,0
	.mask 0x4008000,-32
$LFB4751:
	.cfi_startproc
	lda $30,-32($30)
	.cfi_def_cfa_offset 32
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -32
	.cfi_offset 15, -24
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 0
	stq $16,16($15)
	ldq $1,16($15)
	mov $1,$0
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,32($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4751:
	.end _ZSt11__addressofISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEPT_RS7_
	.section	.text._ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE10deallocateEPS5_m,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE10deallocateEPS5_m,comdat
	.align 2
	.weak	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE10deallocateEPS5_m
	.ent _ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE10deallocateEPS5_m
_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE10deallocateEPS5_m:
	.eflag 48
	.frame $15,48,$26,0
	.mask 0x4008000,-48
$LFB4752:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!614
	lda $29,0($29)		!gpdisp!614
$_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE10deallocateEPS5_m..ng:
	lda $30,-48($30)
	.cfi_def_cfa_offset 48
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -48
	.cfi_offset 15, -40
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,16($15)
	stq $17,24($15)
	stq $18,32($15)
	ldq $16,24($15)
	ldq $27,_ZdlPv($29)		!literal!615
	jsr $26,($27),_ZdlPv		!lituse_jsr!615
	ldah $29,0($26)		!gpdisp!616
	lda $29,0($29)		!gpdisp!616
	bis $31,$31,$31
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,48($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4752:
	.end _ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE10deallocateEPS5_m
	.section	.text._ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC2ERKS1_,"axG",@progbits,_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC5ERKS1_,comdat
	.align 2
	.weak	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC2ERKS1_
	.ent _ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC2ERKS1_
_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC2ERKS1_:
	.eflag 48
	.frame $15,32,$26,0
	.mask 0x4008000,-32
$LFB4754:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!617
	lda $29,0($29)		!gpdisp!617
$_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC2ERKS1_..ng:
	lda $30,-32($30)
	.cfi_def_cfa_offset 32
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -32
	.cfi_offset 15, -24
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,16($15)
	stq $17,24($15)
	ldq $17,24($15)
	ldq $16,16($15)
	ldq $27,_ZNSaI6sphereEC2ERKS0_($29)		!literal!618
	jsr $26,($27),_ZNSaI6sphereEC2ERKS0_		!lituse_jsr!618
	ldah $29,0($26)		!gpdisp!619
	lda $29,0($29)		!gpdisp!619
	bis $31,$31,$31
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,32($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4754:
	.end _ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC2ERKS1_
	.weak	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC1ERKS1_
$_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC1ERKS1_..ng = $_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC2ERKS1_..ng
_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC1ERKS1_ = _ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC2ERKS1_
	.section	.text._ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_,comdat
	.align 2
	.weak	_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_
	.ent _ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_
_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_:
	.eflag 48
	.frame $15,112,$26,0
	.mask 0x4008e00,-112
	.fmask 0x4,-72
$LFB4756:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,$LLSDA4756
	ldah $29,0($27)		!gpdisp!620
	lda $29,0($29)		!gpdisp!620
$_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_..ng:
	lda $30,-112($30)
	.cfi_def_cfa_offset 112
	stq $26,0($30)
	stq $9,8($30)
	stq $10,16($30)
	stq $11,24($30)
	stq $15,32($30)
	stt $f2,40($30)
	.cfi_offset 26, -112
	.cfi_offset 9, -104
	.cfi_offset 10, -96
	.cfi_offset 11, -88
	.cfi_offset 15, -80
	.cfi_offset 34, -72
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,80($15)
	stq $17,88($15)
	stq $18,96($15)
	stq $19,104($15)
	ldq $16,96($15)
	ldq $27,_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE($29)		!literal!622
	jsr $26,($27),0		!lituse_jsr!622
	ldah $29,0($26)		!gpdisp!623
	lda $29,0($29)		!gpdisp!623
	mov $0,$1
	ldq $3,0($1)
	ldq $2,8($1)
	ldq $1,16($1)
	stq $3,48($15)
	stq $2,56($15)
	stq $1,64($15)
	ldq $16,104($15)
	ldq $27,_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE($29)		!literal!624
	jsr $26,($27),0		!lituse_jsr!624
	ldah $29,0($26)		!gpdisp!625
	lda $29,0($29)		!gpdisp!625
	mov $0,$1
	ldt $f2,0($1)
	ldq $10,88($15)
	mov $10,$17
	lda $16,56($31)
	ldq $27,_ZnwmPv($29)		!literal!626
	jsr $26,($27),0		!lituse_jsr!626
	ldah $29,0($26)		!gpdisp!627
	lda $29,0($29)		!gpdisp!627
	mov $0,$9
	cpys $f2,$f2,$f20
	ldq $17,48($15)
	ldq $18,56($15)
	ldq $19,64($15)
	mov $9,$16
$LEHB30:
	ldq $27,_ZN6sphereC1E4vec3d($29)		!literal!628
	jsr $26,($27),_ZN6sphereC1E4vec3d		!lituse_jsr!628
	ldah $29,0($26)		!gpdisp!629
	lda $29,0($29)		!gpdisp!629
$LEHE30:
	br $31,$L261
$L260:
	ldah $29,0($26)		!gpdisp!621
	lda $29,0($29)		!gpdisp!621
	mov $16,$11
	mov $10,$17
	mov $9,$16
	ldq $27,_ZdlPvS_($29)		!literal!630
	jsr $26,($27),_ZdlPvS_		!lituse_jsr!630
	ldah $29,0($26)		!gpdisp!631
	lda $29,0($29)		!gpdisp!631
	mov $11,$1
	mov $1,$16
$LEHB31:
	ldq $27,_Unwind_Resume($29)		!literal!632
	jsr $26,($27),_Unwind_Resume		!lituse_jsr!632
$LEHE31:
$L261:
	mov $15,$30
	ldq $26,0($30)
	ldq $9,8($30)
	ldq $10,16($30)
	ldq $11,24($30)
	ldt $f2,40($30)
	ldq $15,32($30)
	lda $30,112($30)
	.cfi_restore 15
	.cfi_restore 34
	.cfi_restore 11
	.cfi_restore 10
	.cfi_restore 9
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4756:
	.section	.gcc_except_table
$LLSDA4756:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 $LLSDACSE4756-$LLSDACSB4756
$LLSDACSB4756:
	.uleb128 $LEHB30-$LFB4756
	.uleb128 $LEHE30-$LEHB30
	.uleb128 $L260-$LFB4756
	.uleb128 0
	.uleb128 $LEHB31-$LFB4756
	.uleb128 $LEHE31-$LEHB31
	.uleb128 0
	.uleb128 0
$LLSDACSE4756:
	.section	.text._ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_,comdat
	.end _ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_
	.section	.text._ZN9__gnu_cxx16__aligned_bufferI6sphereE7_M_addrEv,"axG",@progbits,_ZN9__gnu_cxx16__aligned_bufferI6sphereE7_M_addrEv,comdat
	.align 2
	.weak	_ZN9__gnu_cxx16__aligned_bufferI6sphereE7_M_addrEv
	.ent _ZN9__gnu_cxx16__aligned_bufferI6sphereE7_M_addrEv
$_ZN9__gnu_cxx16__aligned_bufferI6sphereE7_M_addrEv..ng:
_ZN9__gnu_cxx16__aligned_bufferI6sphereE7_M_addrEv:
	.eflag 48
	.frame $15,32,$26,0
	.mask 0x4008000,-32
$LFB4757:
	.cfi_startproc
	lda $30,-32($30)
	.cfi_def_cfa_offset 32
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -32
	.cfi_offset 15, -24
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 0
	stq $16,16($15)
	ldq $1,16($15)
	mov $1,$0
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,32($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4757:
	.end _ZN9__gnu_cxx16__aligned_bufferI6sphereE7_M_addrEv
	.section	.text._ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_,comdat
	.align 2
	.weak	_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_
	.ent _ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_
_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_:
	.eflag 48
	.frame $15,128,$26,0
	.mask 0x4008e00,-128
	.fmask 0x4,-88
$LFB4758:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,$LLSDA4758
	ldah $29,0($27)		!gpdisp!633
	lda $29,0($29)		!gpdisp!633
$_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_..ng:
	lda $30,-128($30)
	.cfi_def_cfa_offset 128
	stq $26,0($30)
	stq $9,8($30)
	stq $10,16($30)
	stq $11,24($30)
	stq $15,32($30)
	stt $f2,40($30)
	.cfi_offset 26, -128
	.cfi_offset 9, -120
	.cfi_offset 10, -112
	.cfi_offset 11, -104
	.cfi_offset 15, -96
	.cfi_offset 34, -88
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,80($15)
	stq $17,88($15)
	stq $18,96($15)
	stq $19,104($15)
	ldq $16,96($15)
	ldq $27,_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE($29)		!literal!635
	jsr $26,($27),0		!lituse_jsr!635
	ldah $29,0($26)		!gpdisp!636
	lda $29,0($29)		!gpdisp!636
	mov $0,$1
	ldq $3,0($1)
	ldq $2,8($1)
	ldq $1,16($1)
	stq $3,48($15)
	stq $2,56($15)
	stq $1,64($15)
	ldq $16,104($15)
	ldq $27,_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE($29)		!literal!637
	jsr $26,($27),0		!lituse_jsr!637
	ldah $29,0($26)		!gpdisp!638
	lda $29,0($29)		!gpdisp!638
	mov $0,$1
	ldl $1,0($1)
	stq $1,112($15)
	ldt $f10,112($15)
	cvtqt $f10,$f2
	trapb
	ldq $10,88($15)
	mov $10,$17
	lda $16,56($31)
	ldq $27,_ZnwmPv($29)		!literal!639
	jsr $26,($27),0		!lituse_jsr!639
	ldah $29,0($26)		!gpdisp!640
	lda $29,0($29)		!gpdisp!640
	mov $0,$9
	cpys $f2,$f2,$f20
	ldq $17,48($15)
	ldq $18,56($15)
	ldq $19,64($15)
	mov $9,$16
$LEHB32:
	ldq $27,_ZN6sphereC1E4vec3d($29)		!literal!641
	jsr $26,($27),_ZN6sphereC1E4vec3d		!lituse_jsr!641
	ldah $29,0($26)		!gpdisp!642
	lda $29,0($29)		!gpdisp!642
$LEHE32:
	br $31,$L267
$L266:
	ldah $29,0($26)		!gpdisp!634
	lda $29,0($29)		!gpdisp!634
	mov $16,$11
	mov $10,$17
	mov $9,$16
	ldq $27,_ZdlPvS_($29)		!literal!643
	jsr $26,($27),_ZdlPvS_		!lituse_jsr!643
	ldah $29,0($26)		!gpdisp!644
	lda $29,0($29)		!gpdisp!644
	mov $11,$1
	mov $1,$16
$LEHB33:
	ldq $27,_Unwind_Resume($29)		!literal!645
	jsr $26,($27),_Unwind_Resume		!lituse_jsr!645
$LEHE33:
$L267:
	mov $15,$30
	ldq $26,0($30)
	ldq $9,8($30)
	ldq $10,16($30)
	ldq $11,24($30)
	ldt $f2,40($30)
	ldq $15,32($30)
	lda $30,128($30)
	.cfi_restore 15
	.cfi_restore 34
	.cfi_restore 11
	.cfi_restore 10
	.cfi_restore 9
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4758:
	.section	.gcc_except_table
$LLSDA4758:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 $LLSDACSE4758-$LLSDACSB4758
$LLSDACSB4758:
	.uleb128 $LEHB32-$LFB4758
	.uleb128 $LEHE32-$LEHB32
	.uleb128 $L266-$LFB4758
	.uleb128 0
	.uleb128 $LEHB33-$LFB4758
	.uleb128 $LEHE33-$LEHB33
	.uleb128 0
	.uleb128 0
$LLSDACSE4758:
	.section	.text._ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_,comdat
	.end _ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_
	.section	.text._ZNK9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8max_sizeEv,"axG",@progbits,_ZNK9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8max_sizeEv,comdat
	.align 2
	.weak	_ZNK9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8max_sizeEv
	.ent _ZNK9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8max_sizeEv
_ZNK9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8max_sizeEv:
	.eflag 48
	.frame $15,32,$26,0
	.mask 0x4008000,-32
$LFB4759:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!646
	lda $29,0($29)		!gpdisp!646
$_ZNK9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8max_sizeEv..ng:
	lda $30,-32($30)
	.cfi_def_cfa_offset 32
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -32
	.cfi_offset 15, -24
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,16($15)
	ldah $1,$LC18($29)		!gprelhigh
	ldq $1,$LC18($1)		!gprellow
	mov $1,$0
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,32($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4759:
	.end _ZNK9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8max_sizeEv
	.weak	_ZTVSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE
	.section	.rodata._ZTVSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE,"aG",@progbits,_ZTVSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align 3
	.type	_ZTVSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTVSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE, 56
_ZTVSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE:
	.quad	0
	.quad	_ZTISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE
	.quad	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.quad	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.quad	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.quad	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.quad	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.weak	_ZTVSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE
	.section	.data.rel.ro._ZTVSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE,"awG",@progbits,_ZTVSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align 3
	.type	_ZTVSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTVSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE, 56
_ZTVSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE:
	.quad	0
	.quad	_ZTISt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.quad	__cxa_pure_virtual
	.weak	_ZTISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE
	.section	.data.rel.ro._ZTISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE,"awG",@progbits,_ZTISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align 3
	.type	_ZTISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE, 24
_ZTISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE
	.quad	_ZTISt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE
	.weak	_ZTSSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE
	.section	.rodata._ZTSSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE,"aG",@progbits,_ZTSSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE,comdat
	.type	_ZTSSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTSSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE, 73
_ZTSSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE:
	.string	"St23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE"
	.weak	_ZTISt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE
	.section	.data.rel.ro._ZTISt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE,"awG",@progbits,_ZTISt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align 3
	.type	_ZTISt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTISt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE, 24
_ZTISt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE
	.quad	_ZTISt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE
	.weak	_ZTSSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE
	.section	.rodata._ZTSSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE,"aG",@progbits,_ZTSSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE,comdat
	.type	_ZTSSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTSSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE, 52
_ZTSSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE:
	.string	"St16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE"
	.text
	.align 2
	.ent _Z41__static_initialization_and_destruction_0ii
_Z41__static_initialization_and_destruction_0ii:
	.eflag 48
	.frame $15,32,$26,0
	.mask 0x4008000,-32
$LFB4776:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!647
	lda $29,0($29)		!gpdisp!647
$_Z41__static_initialization_and_destruction_0ii..ng:
	lda $30,-32($30)
	.cfi_def_cfa_offset 32
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -32
	.cfi_offset 15, -24
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	mov $16,$1
	mov $17,$2
	stl $1,16($15)
	bis $31,$2,$1
	stl $1,20($15)
	ldl $1,16($15)
	cmpeq $1,1,$1
	beq $1,$L272
	ldl $2,20($15)
	ldah $1,1($31)
	lda $1,-1($1)
	cmpeq $2,$1,$1
	beq $1,$L272
	ldah $1,_ZStL8__ioinit($29)		!gprelhigh
	lda $16,_ZStL8__ioinit($1)		!gprellow
	ldq $27,_ZNSt8ios_base4InitC1Ev($29)		!literal!648
	jsr $26,($27),_ZNSt8ios_base4InitC1Ev		!lituse_jsr!648
	ldah $29,0($26)		!gpdisp!649
	lda $29,0($29)		!gpdisp!649
	ldah $1,__dso_handle($29)		!gprelhigh
	lda $18,__dso_handle($1)		!gprellow
	ldah $1,_ZStL8__ioinit($29)		!gprelhigh
	lda $17,_ZStL8__ioinit($1)		!gprellow
	ldq $16,_ZNSt8ios_base4InitD1Ev($29)		!literal
	ldq $27,__cxa_atexit($29)		!literal!650
	jsr $26,($27),0		!lituse_jsr!650
	ldah $29,0($26)		!gpdisp!651
	lda $29,0($29)		!gpdisp!651
$L272:
	bis $31,$31,$31
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,32($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4776:
	.end _Z41__static_initialization_and_destruction_0ii
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.ent _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED2Ev:
	.eflag 48
	.frame $15,32,$26,0
	.mask 0x4008000,-32
$LFB4778:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!652
	lda $29,0($29)		!gpdisp!652
$_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED2Ev..ng:
	lda $30,-32($30)
	.cfi_def_cfa_offset 32
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -32
	.cfi_offset 15, -24
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,16($15)
	ldq $1,_ZTVSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE($29)		!literal
	lda $2,16($1)
	ldq $1,16($15)
	stq $2,0($1)
	ldq $1,16($15)
	lda $1,16($1)
	mov $1,$16
	ldq $27,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD1Ev($29)		!literal!653
	jsr $26,($27),_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD1Ev		!lituse_jsr!653
	ldah $29,0($26)		!gpdisp!654
	lda $29,0($29)		!gpdisp!654
	ldq $1,16($15)
	mov $1,$16
	ldq $27,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev($29)		!literal!655
	jsr $26,($27),_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev		!lituse_jsr!655
	ldah $29,0($26)		!gpdisp!656
	lda $29,0($29)		!gpdisp!656
	bis $31,$31,$31
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,32($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4778:
	.end _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED1Ev
$_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED1Ev..ng = $_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED2Ev..ng
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED1Ev = _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED0Ev,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.ent _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED0Ev:
	.eflag 48
	.frame $15,32,$26,0
	.mask 0x4008000,-32
$LFB4780:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!657
	lda $29,0($29)		!gpdisp!657
$_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED0Ev..ng:
	lda $30,-32($30)
	.cfi_def_cfa_offset 32
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -32
	.cfi_offset 15, -24
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,16($15)
	ldq $16,16($15)
	ldq $27,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED1Ev($29)		!literal!658
	jsr $26,($27),_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED1Ev		!lituse_jsr!658
	ldah $29,0($26)		!gpdisp!659
	lda $29,0($29)		!gpdisp!659
	lda $17,72($31)
	ldq $16,16($15)
	ldq $27,_ZdlPvm($29)		!literal!660
	jsr $26,($27),_ZdlPvm		!lituse_jsr!660
	ldah $29,0($26)		!gpdisp!661
	lda $29,0($29)		!gpdisp!661
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,32($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4780:
	.end _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,comdat
	.align 2
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.ent _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv:
	.eflag 48
	.frame $15,48,$26,0
	.mask 0x4008200,-48
$LFB4781:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!662
	lda $29,0($29)		!gpdisp!662
$_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv..ng:
	lda $30,-48($30)
	.cfi_def_cfa_offset 48
	stq $26,0($30)
	stq $9,8($30)
	stq $15,16($30)
	.cfi_offset 26, -48
	.cfi_offset 9, -40
	.cfi_offset 15, -32
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,32($15)
	ldq $1,32($15)
	lda $1,16($1)
	mov $1,$16
	ldq $27,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_Impl8_M_allocEv($29)		!literal!663
	jsr $26,($27),0		!lituse_jsr!663
	ldah $29,0($26)		!gpdisp!664
	lda $29,0($29)		!gpdisp!664
	mov $0,$9
	ldq $16,32($15)
	ldq $27,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv($29)		!literal!665
	jsr $26,($27),0		!lituse_jsr!665
	ldah $29,0($26)		!gpdisp!666
	lda $29,0($29)		!gpdisp!666
	mov $0,$1
	mov $1,$17
	mov $9,$16
	ldq $27,_ZNSt16allocator_traitsISaI6sphereEE7destroyIS0_EEvRS1_PT_($29)		!literal!667
	jsr $26,($27),_ZNSt16allocator_traitsISaI6sphereEE7destroyIS0_EEvRS1_PT_		!lituse_jsr!667
	ldah $29,0($26)		!gpdisp!668
	lda $29,0($29)		!gpdisp!668
	bis $31,$31,$31
	mov $15,$30
	ldq $26,0($30)
	ldq $9,8($30)
	ldq $15,16($30)
	lda $30,48($30)
	.cfi_restore 15
	.cfi_restore 9
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4781:
	.end _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,comdat
	.align 2
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.ent _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv:
	.eflag 48
	.frame $15,64,$26,0
	.mask 0x4008000,-64
$LFB4782:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!669
	lda $29,0($29)		!gpdisp!669
$_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv..ng:
	lda $30,-64($30)
	.cfi_def_cfa_offset 64
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -64
	.cfi_offset 15, -56
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,48($15)
	ldq $1,48($15)
	lda $1,16($1)
	mov $1,$16
	ldq $27,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_Impl8_M_allocEv($29)		!literal!670
	jsr $26,($27),0		!lituse_jsr!670
	ldah $29,0($26)		!gpdisp!671
	lda $29,0($29)		!gpdisp!671
	mov $0,$1
	mov $1,$17
	lda $16,16($15)
	ldq $27,_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC1IS0_EERKSaIT_E($29)		!literal!672
	jsr $26,($27),_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC1IS0_EERKSaIT_E		!lituse_jsr!672
	ldah $29,0($26)		!gpdisp!673
	lda $29,0($29)		!gpdisp!673
	lda $1,24($15)
	ldq $18,48($15)
	lda $17,16($15)
	mov $1,$16
	ldq $27,_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC1ERS6_PS5_($29)		!literal!674
	jsr $26,($27),_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC1ERS6_PS5_		!lituse_jsr!674
	ldah $29,0($26)		!gpdisp!675
	lda $29,0($29)		!gpdisp!675
	ldq $16,48($15)
	ldq $27,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED1Ev($29)		!literal!676
	jsr $26,($27),_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED1Ev		!lituse_jsr!676
	ldah $29,0($26)		!gpdisp!677
	lda $29,0($29)		!gpdisp!677
	lda $1,24($15)
	mov $1,$16
	ldq $27,_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED1Ev($29)		!literal!678
	jsr $26,($27),_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED1Ev		!lituse_jsr!678
	ldah $29,0($26)		!gpdisp!679
	lda $29,0($29)		!gpdisp!679
	lda $16,16($15)
	ldq $27,_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED1Ev($29)		!literal!680
	jsr $26,($27),_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED1Ev		!lituse_jsr!680
	ldah $29,0($26)		!gpdisp!681
	lda $29,0($29)		!gpdisp!681
	bis $31,$31,$31
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,64($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4782:
	.end _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,comdat
	.align 2
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.ent _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info:
	.eflag 48
	.frame $15,48,$26,0
	.mask 0x4008000,-48
$LFB4783:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!682
	lda $29,0($29)		!gpdisp!682
$_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info..ng:
	lda $30,-48($30)
	.cfi_def_cfa_offset 48
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -48
	.cfi_offset 15, -40
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,32($15)
	stq $17,40($15)
	ldq $16,32($15)
	ldq $27,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv($29)		!literal!683
	jsr $26,($27),0		!lituse_jsr!683
	ldah $29,0($26)		!gpdisp!684
	lda $29,0($29)		!gpdisp!684
	mov $0,$1
	stq $1,16($15)
	ldq $27,_ZNSt19_Sp_make_shared_tag5_S_tiEv($29)		!literal!685
	jsr $26,($27),0		!lituse_jsr!685
	ldah $29,0($26)		!gpdisp!686
	lda $29,0($29)		!gpdisp!686
	mov $0,$1
	ldq $2,40($15)
	cmpeq $2,$1,$1
	bne $1,$L278
	ldq $17,_ZTISt19_Sp_make_shared_tag($29)		!literal
	ldq $16,40($15)
	ldq $27,_ZNKSt9type_infoeqERKS_($29)		!literal!687
	jsr $26,($27),0		!lituse_jsr!687
	ldah $29,0($26)		!gpdisp!688
	lda $29,0($29)		!gpdisp!688
	mov $0,$1
	beq $1,$L279
$L278:
	lda $1,1($31)
	br $31,$L280
$L279:
	mov $31,$1
$L280:
	beq $1,$L281
	ldq $1,16($15)
	br $31,$L282
$L281:
	mov $31,$1
$L282:
	mov $1,$0
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,48($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4783:
	.end _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_Impl8_M_allocEv,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_Impl8_M_allocEv,comdat
	.align 2
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_Impl8_M_allocEv
	.ent _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_Impl8_M_allocEv
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_Impl8_M_allocEv:
	.eflag 48
	.frame $15,32,$26,0
	.mask 0x4008000,-32
$LFB4784:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!689
	lda $29,0($29)		!gpdisp!689
$_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_Impl8_M_allocEv..ng:
	lda $30,-32($30)
	.cfi_def_cfa_offset 32
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -32
	.cfi_offset 15, -24
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,16($15)
	ldq $16,16($15)
	ldq $27,_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EE6_S_getERS2_($29)		!literal!690
	jsr $26,($27),0		!lituse_jsr!690
	ldah $29,0($26)		!gpdisp!691
	lda $29,0($29)		!gpdisp!691
	mov $0,$1
	mov $1,$0
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,32($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4784:
	.end _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_Impl8_M_allocEv
	.section	.text._ZNSt16allocator_traitsISaI6sphereEE7destroyIS0_EEvRS1_PT_,"axG",@progbits,_ZNSt16allocator_traitsISaI6sphereEE7destroyIS0_EEvRS1_PT_,comdat
	.align 2
	.weak	_ZNSt16allocator_traitsISaI6sphereEE7destroyIS0_EEvRS1_PT_
	.ent _ZNSt16allocator_traitsISaI6sphereEE7destroyIS0_EEvRS1_PT_
_ZNSt16allocator_traitsISaI6sphereEE7destroyIS0_EEvRS1_PT_:
	.eflag 48
	.frame $15,32,$26,0
	.mask 0x4008000,-32
$LFB4785:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!692
	lda $29,0($29)		!gpdisp!692
$_ZNSt16allocator_traitsISaI6sphereEE7destroyIS0_EEvRS1_PT_..ng:
	lda $30,-32($30)
	.cfi_def_cfa_offset 32
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -32
	.cfi_offset 15, -24
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,16($15)
	stq $17,24($15)
	ldq $17,24($15)
	ldq $16,16($15)
	ldq $27,_ZN9__gnu_cxx13new_allocatorI6sphereE7destroyIS1_EEvPT_($29)		!literal!693
	jsr $26,($27),_ZN9__gnu_cxx13new_allocatorI6sphereE7destroyIS1_EEvPT_		!lituse_jsr!693
	ldah $29,0($26)		!gpdisp!694
	lda $29,0($29)		!gpdisp!694
	bis $31,$31,$31
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,32($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4785:
	.end _ZNSt16allocator_traitsISaI6sphereEE7destroyIS0_EEvRS1_PT_
	.section	.text._ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EE6_S_getERS2_,"axG",@progbits,_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EE6_S_getERS2_,comdat
	.align 2
	.weak	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EE6_S_getERS2_
	.ent _ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EE6_S_getERS2_
$_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EE6_S_getERS2_..ng:
_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EE6_S_getERS2_:
	.eflag 48
	.frame $15,32,$26,0
	.mask 0x4008000,-32
$LFB4786:
	.cfi_startproc
	lda $30,-32($30)
	.cfi_def_cfa_offset 32
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -32
	.cfi_offset 15, -24
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 0
	stq $16,16($15)
	ldq $1,16($15)
	mov $1,$0
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,32($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4786:
	.end _ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EE6_S_getERS2_
	.section	.text._ZN6sphereD2Ev,"axG",@progbits,_ZN6sphereD5Ev,comdat
	.align 2
	.weak	_ZN6sphereD2Ev
	.ent _ZN6sphereD2Ev
_ZN6sphereD2Ev:
	.eflag 48
	.frame $15,32,$26,0
	.mask 0x4008000,-32
$LFB4789:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!695
	lda $29,0($29)		!gpdisp!695
$_ZN6sphereD2Ev..ng:
	lda $30,-32($30)
	.cfi_def_cfa_offset 32
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -32
	.cfi_offset 15, -24
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,16($15)
	ldq $1,_ZTV6sphere($29)		!literal
	lda $2,16($1)
	ldq $1,16($15)
	stq $2,0($1)
	ldq $1,16($15)
	lda $1,40($1)
	mov $1,$16
	ldq $27,_ZNSt10shared_ptrI8materialED1Ev($29)		!literal!696
	jsr $26,($27),_ZNSt10shared_ptrI8materialED1Ev		!lituse_jsr!696
	ldah $29,0($26)		!gpdisp!697
	lda $29,0($29)		!gpdisp!697
	bis $31,$31,$31
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,32($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4789:
	.end _ZN6sphereD2Ev
	.weak	_ZN6sphereD1Ev
$_ZN6sphereD1Ev..ng = $_ZN6sphereD2Ev..ng
_ZN6sphereD1Ev = _ZN6sphereD2Ev
	.section	.text._ZN9__gnu_cxx13new_allocatorI6sphereE7destroyIS1_EEvPT_,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorI6sphereE7destroyIS1_EEvPT_,comdat
	.align 2
	.weak	_ZN9__gnu_cxx13new_allocatorI6sphereE7destroyIS1_EEvPT_
	.ent _ZN9__gnu_cxx13new_allocatorI6sphereE7destroyIS1_EEvPT_
_ZN9__gnu_cxx13new_allocatorI6sphereE7destroyIS1_EEvPT_:
	.eflag 48
	.frame $15,32,$26,0
	.mask 0x4008000,-32
$LFB4787:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!698
	lda $29,0($29)		!gpdisp!698
$_ZN9__gnu_cxx13new_allocatorI6sphereE7destroyIS1_EEvPT_..ng:
	lda $30,-32($30)
	.cfi_def_cfa_offset 32
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -32
	.cfi_offset 15, -24
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	stq $16,16($15)
	stq $17,24($15)
	ldq $16,24($15)
	ldq $27,_ZN6sphereD1Ev($29)		!literal!699
	jsr $26,($27),_ZN6sphereD1Ev		!lituse_jsr!699
	ldah $29,0($26)		!gpdisp!700
	lda $29,0($29)		!gpdisp!700
	bis $31,$31,$31
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,32($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4787:
	.end _ZN9__gnu_cxx13new_allocatorI6sphereE7destroyIS1_EEvPT_
	.weak	_ZTISt19_Sp_make_shared_tag
	.section	.data.rel.ro._ZTISt19_Sp_make_shared_tag,"awG",@progbits,_ZTISt19_Sp_make_shared_tag,comdat
	.align 3
	.type	_ZTISt19_Sp_make_shared_tag, @object
	.size	_ZTISt19_Sp_make_shared_tag, 16
_ZTISt19_Sp_make_shared_tag:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSSt19_Sp_make_shared_tag
	.weak	_ZTSSt19_Sp_make_shared_tag
	.section	.rodata._ZTSSt19_Sp_make_shared_tag,"aG",@progbits,_ZTSSt19_Sp_make_shared_tag,comdat
	.type	_ZTSSt19_Sp_make_shared_tag, @object
	.size	_ZTSSt19_Sp_make_shared_tag, 24
_ZTSSt19_Sp_make_shared_tag:
	.string	"St19_Sp_make_shared_tag"
	.weak	_ZTISt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE
	.section	.data.rel.ro._ZTISt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE,"awG",@progbits,_ZTISt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align 3
	.type	_ZTISt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTISt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE, 16
_ZTISt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSSt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE
	.weak	_ZTSSt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE
	.section	.rodata._ZTSSt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE,"aG",@progbits,_ZTSSt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE,comdat
	.type	_ZTSSt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTSSt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE, 47
_ZTSSt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE:
	.string	"St11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE"
	.text
	.align 2
	.ent _GLOBAL__sub_I_main
_GLOBAL__sub_I_main:
	.eflag 48
	.frame $15,16,$26,0
	.mask 0x4008000,-16
$LFB4791:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!701
	lda $29,0($29)		!gpdisp!701
$_GLOBAL__sub_I_main..ng:
	lda $30,-16($30)
	.cfi_def_cfa_offset 16
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -16
	.cfi_offset 15, -8
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	ldah $17,1($31)
	lda $17,-1($17)
	lda $16,1($31)
	ldq $27,_Z41__static_initialization_and_destruction_0ii($29)		!literal!702
	jsr $26,($27),_Z41__static_initialization_and_destruction_0ii		!lituse_jsr!702
	ldah $29,0($26)		!gpdisp!703
	lda $29,0($29)		!gpdisp!703
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,16($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE4791:
	.end _GLOBAL__sub_I_main
	.section	.ctors,"aw",@progbits
	.align 3
	.quad	_GLOBAL__sub_I_main
	.weakref	_ZL28__gthrw___pthread_key_createPjPFvPvE,__pthread_key_create
	.section	.rodata
	.align 3
$LC0:
	.long	0
	.long	1072693248
	.align 3
$LC1:
	.long	0
	.long	2146435072
	.align 3
$LC2:
	.long	3539053052
	.long	1062232653
	.align 3
$LC3:
	.long	0
	.long	1071644672
	.align 3
$LC4:
	.long	1717986918
	.long	1072064102
	.align 3
$LC5:
	.long	477218588
	.long	1073508807
	.align 3
$LC6:
	.long	0
	.long	-1074790400
	.align 3
$LC7:
	.long	0
	.long	-1067900928
	.align 3
$LC12:
	.long	0
	.long	1081667584
	.align 3
$LC13:
	.long	0
	.long	1080819712
	.align 4
$LC15:
	.long	0
	.long	0
	.long	0
	.long	1075773440
	.align 3
$LC16:
	.long	4294967295
	.long	1072693247
	.align 3
$LC17:
	.quad	945986875574848801
	.align 3
$LC18:
	.quad	128102389400760775
	.hidden	DW.ref.__gxx_personality_v0
	.weak	DW.ref.__gxx_personality_v0
	.section	.sdata.DW.ref.__gxx_personality_v0,"awsG",@progbits,DW.ref.__gxx_personality_v0,comdat
	.align 3
	.type	DW.ref.__gxx_personality_v0, @object
	.size	DW.ref.__gxx_personality_v0, 8
DW.ref.__gxx_personality_v0:
	.quad	__gxx_personality_v0
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.4.0-1ubuntu1~20.04) 9.4.0"
	.section	.note.GNU-stack,"",@progbits
