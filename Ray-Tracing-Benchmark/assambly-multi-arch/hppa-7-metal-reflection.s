	.LEVEL 1.1
	.text
	.section	.rodata
	.type	_ZStL19piecewise_construct, @object
	.size	_ZStL19piecewise_construct, 1
_ZStL19piecewise_construct:
	.zero	1
	.section	.text._ZNKSt9type_infoeqERKS_,"axG",@progbits,_ZNKSt9type_infoeqERKS_,comdat
	.align 4
	.weak	_ZNKSt9type_infoeqERKS_
	.type	_ZNKSt9type_infoeqERKS_, @function
_ZNKSt9type_infoeqERKS_:
	.PROC
	.CALLINFO FRAME=64,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB727:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	stw %r25,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldw 4(%r28),%r19
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	ldw 0(%r28),%r28
	ldw 4(%r28),%r28
	comb,=,n %r28,%r19,.L2
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldw 4(%r28),%r28
	ldb 0(%r28),%r28
	extrs %r28,31,8,%r19
	ldi 42,%r28
	comb,=,n %r28,%r19,.L3
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldw 4(%r28),%r19
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	ldw 0(%r28),%r28
	ldw 4(%r28),%r28
	copy %r28,%r25
	copy %r19,%r26
	bl strcmp,%r2
	nop
	comib,<>,n 0,%r28,.L3
.L2:
	ldi 1,%r28
	b,n .L4
.L3:
	ldi 0,%r28
.L4:
	ldw -20(%r3),%r2
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE727:
	.size	_ZNKSt9type_infoeqERKS_, .-_ZNKSt9type_infoeqERKS_
	.section	.text._ZnwjPv,"axG",@progbits,_ZnwjPv,comdat
	.align 4
	.weak	_ZnwjPv
	.type	_ZnwjPv, @function
_ZnwjPv:
	.PROC
	.CALLINFO FRAME=64,NO_CALLS,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB769:
	.cfi_startproc
	copy %r3,%r1
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	stw %r25,0(%r28)
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	ldw 0(%r28),%r28
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE769:
	.size	_ZnwjPv, .-_ZnwjPv
	.section	.text._ZdlPvS_,"axG",@progbits,_ZdlPvS_,comdat
	.align 4
	.weak	_ZdlPvS_
	.type	_ZdlPvS_, @function
_ZdlPvS_:
	.PROC
	.CALLINFO FRAME=64,NO_CALLS,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB771:
	.cfi_startproc
	copy %r3,%r1
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	stw %r25,0(%r28)
	nop
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE771:
	.size	_ZdlPvS_, .-_ZdlPvS_
	.section	.rodata
	.align 4
	.type	_ZZL18__gthread_active_pvE20__gthread_active_ptr, @object
	.size	_ZZL18__gthread_active_pvE20__gthread_active_ptr, 4
_ZZL18__gthread_active_pvE20__gthread_active_ptr:
	.word	P%_ZL28__gthrw___pthread_key_createPjPFvPvE
	.align 4
.LC0:
	.word	P%_ZL28__gthrw___pthread_key_createPjPFvPvE
	.section	.text._ZL18__gthread_active_pv,"axG",@progbits,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv,comdat
	.align 4
	.type	_ZL18__gthread_active_pv, @function
_ZL18__gthread_active_pv:
	.PROC
	.CALLINFO FRAME=64,NO_CALLS,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB945:
	.cfi_startproc
	copy %r3,%r1
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldi 0,%r28
	ldil LR'.LC0,%r19
	ldo RR'.LC0(%r19),%r19
	ldw 0(%r19),%r19
	comiclr,= 0,%r19,%r0
	ldi 1,%r28
.L10:
	extru %r28,31,8,%r28
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE945:
	.size	_ZL18__gthread_active_pv, .-_ZL18__gthread_active_pv
.globl __sync_fetch_and_add_4
	.section	.text._ZN9__gnu_cxxL18__exchange_and_addEPVii,"axG",@progbits,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv,comdat
	.align 4
	.type	_ZN9__gnu_cxxL18__exchange_and_addEPVii, @function
_ZN9__gnu_cxxL18__exchange_and_addEPVii:
	.PROC
	.CALLINFO FRAME=128,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB974:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,128(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	stw %r25,0(%r28)
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	ldw 0(%r28),%r19
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	copy %r19,%r25
	copy %r28,%r26
	bl __sync_fetch_and_add_4,%r2
	nop
	ldw -20(%r3),%r2
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE974:
	.size	_ZN9__gnu_cxxL18__exchange_and_addEPVii, .-_ZN9__gnu_cxxL18__exchange_and_addEPVii
	.section	.text._ZN9__gnu_cxxL25__exchange_and_add_singleEPii,"axG",@progbits,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv,comdat
	.align 4
	.type	_ZN9__gnu_cxxL25__exchange_and_add_singleEPii, @function
_ZN9__gnu_cxxL25__exchange_and_add_singleEPii:
	.PROC
	.CALLINFO FRAME=64,NO_CALLS,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB976:
	.cfi_startproc
	copy %r3,%r1
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	stw %r25,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldw 0(%r28),%r28
	stw %r28,8(%r3)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldw 0(%r28),%r19
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	ldw 0(%r28),%r28
	addl %r19,%r28,%r19
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	stw %r19,0(%r28)
	ldw 8(%r3),%r28
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE976:
	.size	_ZN9__gnu_cxxL25__exchange_and_add_singleEPii, .-_ZN9__gnu_cxxL25__exchange_and_add_singleEPii
	.section	.text._ZN9__gnu_cxxL27__exchange_and_add_dispatchEPii,"axG",@progbits,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv,comdat
	.align 4
	.type	_ZN9__gnu_cxxL27__exchange_and_add_dispatchEPii, @function
_ZN9__gnu_cxxL27__exchange_and_add_dispatchEPii:
	.PROC
	.CALLINFO FRAME=64,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB978:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	stw %r25,0(%r28)
	bl _ZL18__gthread_active_pv,%r2
	nop
	comiclr,= 0,%r28,%r28
	ldi 1,%r28
	extru %r28,31,8,%r28
	comib,=,n 0,%r28,.L17
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	ldw 0(%r28),%r25
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZN9__gnu_cxxL18__exchange_and_addEPVii,%r2
	nop
	b,n .L18
.L17:
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	ldw 0(%r28),%r25
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZN9__gnu_cxxL25__exchange_and_add_singleEPii,%r2
	nop
	nop
.L18:
	ldw -20(%r3),%r2
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE978:
	.size	_ZN9__gnu_cxxL27__exchange_and_add_dispatchEPii, .-_ZN9__gnu_cxxL27__exchange_and_add_dispatchEPii
	.section	.rodata
	.align 4
	.type	_ZN9__gnu_cxxL21__default_lock_policyE, @object
	.size	_ZN9__gnu_cxxL21__default_lock_policyE, 4
_ZN9__gnu_cxxL21__default_lock_policyE:
	.word	2
	.type	_ZStL13allocator_arg, @object
	.size	_ZStL13allocator_arg, 1
_ZStL13allocator_arg:
	.zero	1
	.type	_ZStL6ignore, @object
	.size	_ZStL6ignore, 1
_ZStL6ignore:
	.zero	1
	.weak	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag
	.section	.rodata._ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag,"aG",@progbits,_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag,comdat
	.align 4
	.type	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag, @gnu_unique_object
	.size	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag, 8
_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag:
	.zero	8
	.section	.text._ZNSt19_Sp_make_shared_tag5_S_tiEv,"axG",@progbits,_ZNSt19_Sp_make_shared_tag5_S_tiEv,comdat
	.align 4
	.weak	_ZNSt19_Sp_make_shared_tag5_S_tiEv
	.type	_ZNSt19_Sp_make_shared_tag5_S_tiEv, @function
_ZNSt19_Sp_make_shared_tag5_S_tiEv:
	.PROC
	.CALLINFO FRAME=64,NO_CALLS,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB1942:
	.cfi_startproc
	copy %r3,%r1
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldil LR'_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag,%r28
	ldo RR'_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag(%r28),%r28
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE1942:
	.size	_ZNSt19_Sp_make_shared_tag5_S_tiEv, .-_ZNSt19_Sp_make_shared_tag5_S_tiEv
	.section	.rodata
	.align 8
	.type	_ZL2pi, @object
	.size	_ZL2pi, 8
_ZL2pi:
	.word	1074340347
	.word	1413754136
	.weak	_ZZ13random_doublevE12distribution
	.section	.bss._ZZ13random_doublevE12distribution,"awG",@nobits,_ZZ13random_doublevE12distribution,comdat
	.align 8
	.type	_ZZ13random_doublevE12distribution, @gnu_unique_object
	.size	_ZZ13random_doublevE12distribution, 16
_ZZ13random_doublevE12distribution:
	.zero	16
	.weak	_ZGVZ13random_doublevE12distribution
	.section	.bss._ZGVZ13random_doublevE12distribution,"awG",@nobits,_ZGVZ13random_doublevE12distribution,comdat
	.align 8
	.type	_ZGVZ13random_doublevE12distribution, @gnu_unique_object
	.size	_ZGVZ13random_doublevE12distribution, 8
_ZGVZ13random_doublevE12distribution:
	.zero	8
	.weak	_ZZ13random_doublevE9generator
	.section	.bss._ZZ13random_doublevE9generator,"awG",@nobits,_ZZ13random_doublevE9generator,comdat
	.align 4
	.type	_ZZ13random_doublevE9generator, @gnu_unique_object
	.size	_ZZ13random_doublevE9generator, 2500
_ZZ13random_doublevE9generator:
	.zero	2500
	.weak	_ZGVZ13random_doublevE9generator
	.section	.bss._ZGVZ13random_doublevE9generator,"awG",@nobits,_ZGVZ13random_doublevE9generator,comdat
	.align 8
	.type	_ZGVZ13random_doublevE9generator, @gnu_unique_object
	.size	_ZGVZ13random_doublevE9generator, 8
_ZGVZ13random_doublevE9generator:
	.zero	8
	.section	.rodata
	.align 8
.LC1:
	.word	1072693248
	.word	0
	.section	.text._Z13random_doublev,"axG",@progbits,_Z13random_doublev,comdat
	.align 4
	.weak	_Z13random_doublev
	.type	_Z13random_doublev, @function
_Z13random_doublev:
	.PROC
	.CALLINFO FRAME=128,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=5
	.ENTRY
.LFB3288:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA3288
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,128(%r30)
	stw %r5,8(%r3)
	stw %r4,12(%r3)
	.cfi_offset 3, 0
	.cfi_offset 5, 8
	.cfi_offset 4, 12
	addil LR'_ZGVZ13random_doublevE12distribution-$global$,%r27
	copy %r1,%r28
	ldo RR'_ZGVZ13random_doublevE12distribution-$global$(%r28),%r28
	ldb 0(%r28),%r28
	ldo 15(%sp),%r20
	dep %r0,31,3,%r20
	ldcw 0(%r20),%r20
	extru %r28,31,8,%r28
	comiclr,<> 0,%r28,%r28
	ldi 1,%r28
	extru %r28,31,8,%r28
	comib,=,n 0,%r28,.L22
	addil LR'_ZGVZ13random_doublevE12distribution-$global$,%r27
	copy %r1,%r28
	ldo RR'_ZGVZ13random_doublevE12distribution-$global$(%r28),%r26
	bl __cxa_guard_acquire,%r2
	nop
	comiclr,= 0,%r28,%r28
	ldi 1,%r28
	extru %r28,31,8,%r28
	comib,=,n 0,%r28,.L22
	ldi 0,%r4
	ldo -64(%r30),%r28
	ldil LR'.LC1,%r19
	ldo RR'.LC1(%r19),%r19
	fldds 0(%r19),%fr22
	fstds %fr22,8(%r28)
	fcpy,dbl %fr0,%fr7
	addil LR'_ZZ13random_doublevE12distribution-$global$,%r27
	copy %r1,%r28
	ldo RR'_ZZ13random_doublevE12distribution-$global$(%r28),%r26
.LEHB0:
	bl _ZNSt25uniform_real_distributionIdEC1Edd,%r2
	nop
.LEHE0:
	addil LR'_ZGVZ13random_doublevE12distribution-$global$,%r27
	copy %r1,%r28
	ldo RR'_ZGVZ13random_doublevE12distribution-$global$(%r28),%r26
	bl __cxa_guard_release,%r2
	nop
.L22:
	addil LR'_ZGVZ13random_doublevE9generator-$global$,%r27
	copy %r1,%r28
	ldo RR'_ZGVZ13random_doublevE9generator-$global$(%r28),%r28
	ldb 0(%r28),%r28
	ldo 15(%sp),%r19
	dep %r0,31,3,%r19
	ldcw 0(%r19),%r19
	extru %r28,31,8,%r28
	comiclr,<> 0,%r28,%r28
	ldi 1,%r28
	extru %r28,31,8,%r28
	comib,=,n 0,%r28,.L23
	addil LR'_ZGVZ13random_doublevE9generator-$global$,%r27
	copy %r1,%r28
	ldo RR'_ZGVZ13random_doublevE9generator-$global$(%r28),%r26
	bl __cxa_guard_acquire,%r2
	nop
	comiclr,= 0,%r28,%r28
	ldi 1,%r28
	extru %r28,31,8,%r28
	comib,=,n 0,%r28,.L23
	ldi 0,%r4
	addil LR'_ZZ13random_doublevE9generator-$global$,%r27
	copy %r1,%r28
	ldo RR'_ZZ13random_doublevE9generator-$global$(%r28),%r26
.LEHB1:
	bl _ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC1Ev,%r2
	nop
.LEHE1:
	addil LR'_ZGVZ13random_doublevE9generator-$global$,%r27
	copy %r1,%r28
	ldo RR'_ZGVZ13random_doublevE9generator-$global$(%r28),%r26
	bl __cxa_guard_release,%r2
	nop
.L23:
	addil LR'_ZZ13random_doublevE9generator-$global$,%r27
	copy %r1,%r28
	ldo RR'_ZZ13random_doublevE9generator-$global$(%r28),%r25
	addil LR'_ZZ13random_doublevE12distribution-$global$,%r27
	copy %r1,%r28
	ldo RR'_ZZ13random_doublevE12distribution-$global$(%r28),%r26
.LEHB2:
	bl _ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEEEdRT_,%r2
	nop
	fcpy,dbl %fr4,%fr22
	b,n .L31
.L29:
	copy %r20,%r5
	comib,<>,n 0,%r4,.L26
	addil LR'_ZGVZ13random_doublevE12distribution-$global$,%r27
	copy %r1,%r28
	ldo RR'_ZGVZ13random_doublevE12distribution-$global$(%r28),%r26
	bl __cxa_guard_abort,%r2
	nop
.L26:
	copy %r5,%r28
	copy %r28,%r26
	bl _Unwind_Resume,%r2
	nop
.L30:
	copy %r20,%r5
	comib,<>,n 0,%r4,.L28
	addil LR'_ZGVZ13random_doublevE9generator-$global$,%r27
	copy %r1,%r28
	ldo RR'_ZGVZ13random_doublevE9generator-$global$(%r28),%r26
	bl __cxa_guard_abort,%r2
	nop
.L28:
	copy %r5,%r28
	copy %r28,%r26
	bl _Unwind_Resume,%r2
	nop
.LEHE2:
.L31:
	fcpy,dbl %fr22,%fr4
	ldw -20(%r3),%r2
	ldw 8(%r3),%r5
	ldw 12(%r3),%r4
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE3288:
.globl __gxx_personality_v0
	.section	.gcc_except_table,"a",@progbits
.LLSDA3288:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE3288-.LLSDACSB3288
.LLSDACSB3288:
	.uleb128 .LEHB0-.LFB3288
	.uleb128 .LEHE0-.LEHB0
	.uleb128 .L29-.LFB3288
	.uleb128 0
	.uleb128 .LEHB1-.LFB3288
	.uleb128 .LEHE1-.LEHB1
	.uleb128 .L30-.LFB3288
	.uleb128 0
	.uleb128 .LEHB2-.LFB3288
	.uleb128 .LEHE2-.LEHB2
	.uleb128 0
	.uleb128 0
.LLSDACSE3288:
	.section	.text._Z13random_doublev,"axG",@progbits,_Z13random_doublev,comdat
	.size	_Z13random_doublev, .-_Z13random_doublev
	.section	.text._ZplRK4vec3S1_,"axG",@progbits,_ZplRK4vec3S1_,comdat
	.align 4
	.weak	_ZplRK4vec3S1_
	.type	_ZplRK4vec3S1_, @function
_ZplRK4vec3S1_:
	.PROC
	.CALLINFO FRAME=128,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=4
	.ENTRY
.LFB3924:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,128(%r30)
	stw %r4,8(%r3)
	.cfi_offset 3, 0
	.cfi_offset 4, 8
	copy %r28,%r4
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	stw %r25,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	fldds 0(%r28),%fr23
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	ldw 0(%r28),%r28
	fldds 0(%r28),%fr22
	fadd,dbl %fr23,%fr22,%fr25
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	fldds 8(%r28),%fr23
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	ldw 0(%r28),%r28
	fldds 8(%r28),%fr22
	fadd,dbl %fr23,%fr22,%fr22
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldo 32(%r28),%r28
	fldds -16(%r28),%fr24
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	ldw 0(%r28),%r28
	ldo 32(%r28),%r28
	fldds -16(%r28),%fr23
	fadd,dbl %fr24,%fr23,%fr23
	ldo -64(%r30),%r28
	fstds %fr23,0(%r28)
	ldo -64(%r30),%r28
	fstds %fr22,8(%r28)
	fcpy,dbl %fr25,%fr7
	copy %r4,%r26
	bl _ZN4vec3C1Eddd,%r2
	nop
	copy %r4,%r28
	ldw -20(%r3),%r2
	ldw 8(%r3),%r4
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE3924:
	.size	_ZplRK4vec3S1_, .-_ZplRK4vec3S1_
	.section	.text._ZmiRK4vec3S1_,"axG",@progbits,_ZmiRK4vec3S1_,comdat
	.align 4
	.weak	_ZmiRK4vec3S1_
	.type	_ZmiRK4vec3S1_, @function
_ZmiRK4vec3S1_:
	.PROC
	.CALLINFO FRAME=128,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=4
	.ENTRY
.LFB3925:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,128(%r30)
	stw %r4,8(%r3)
	.cfi_offset 3, 0
	.cfi_offset 4, 8
	copy %r28,%r4
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	stw %r25,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	fldds 0(%r28),%fr23
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	ldw 0(%r28),%r28
	fldds 0(%r28),%fr22
	fsub,dbl %fr23,%fr22,%fr25
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	fldds 8(%r28),%fr23
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	ldw 0(%r28),%r28
	fldds 8(%r28),%fr22
	fsub,dbl %fr23,%fr22,%fr22
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldo 32(%r28),%r28
	fldds -16(%r28),%fr24
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	ldw 0(%r28),%r28
	ldo 32(%r28),%r28
	fldds -16(%r28),%fr23
	fsub,dbl %fr24,%fr23,%fr23
	ldo -64(%r30),%r28
	fstds %fr23,0(%r28)
	ldo -64(%r30),%r28
	fstds %fr22,8(%r28)
	fcpy,dbl %fr25,%fr7
	copy %r4,%r26
	bl _ZN4vec3C1Eddd,%r2
	nop
	copy %r4,%r28
	ldw -20(%r3),%r2
	ldw 8(%r3),%r4
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE3925:
	.size	_ZmiRK4vec3S1_, .-_ZmiRK4vec3S1_
	.section	.text._ZmldRK4vec3,"axG",@progbits,_ZmldRK4vec3,comdat
	.align 4
	.weak	_ZmldRK4vec3
	.type	_ZmldRK4vec3, @function
_ZmldRK4vec3:
	.PROC
	.CALLINFO FRAME=128,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=4
	.ENTRY
.LFB3927:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,128(%r30)
	stw %r4,8(%r3)
	.cfi_offset 3, 0
	.cfi_offset 4, 8
	copy %r28,%r4
	ldo -32(%r3),%r28
	fstds %fr5,-8(%r28)
	ldo 0(%r3),%r28
	ldo -44(%r28),%r28
	stw %r24,0(%r28)
	ldo 0(%r3),%r28
	ldo -44(%r28),%r28
	ldw 0(%r28),%r28
	fldds 0(%r28),%fr23
	ldo -32(%r3),%r28
	fldds -8(%r28),%fr22
	fmpy,dbl %fr23,%fr22,%fr25
	ldo 0(%r3),%r28
	ldo -44(%r28),%r28
	ldw 0(%r28),%r28
	fldds 8(%r28),%fr23
	ldo -32(%r3),%r28
	fldds -8(%r28),%fr22
	fmpy,dbl %fr23,%fr22,%fr22
	ldo 0(%r3),%r28
	ldo -44(%r28),%r28
	ldw 0(%r28),%r28
	ldo 32(%r28),%r28
	fldds -16(%r28),%fr24
	ldo -32(%r3),%r28
	fldds -8(%r28),%fr23
	fmpy,dbl %fr24,%fr23,%fr23
	ldo -64(%r30),%r28
	fstds %fr23,0(%r28)
	ldo -64(%r30),%r28
	fstds %fr22,8(%r28)
	fcpy,dbl %fr25,%fr7
	copy %r4,%r26
	bl _ZN4vec3C1Eddd,%r2
	nop
	copy %r4,%r28
	ldw -20(%r3),%r2
	ldw 8(%r3),%r4
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE3927:
	.size	_ZmldRK4vec3, .-_ZmldRK4vec3
	.section	.rodata
	.align 8
.LC2:
	.word	1072693248
	.word	0
	.section	.text._Zdv4vec3d,"axG",@progbits,_Zdv4vec3d,comdat
	.align 4
	.weak	_Zdv4vec3d
	.type	_Zdv4vec3d, @function
_Zdv4vec3d:
	.PROC
	.CALLINFO FRAME=64,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=5
	.ENTRY
.LFB3929:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	stw %r5,8(%r3)
	stw %r4,12(%r3)
	.cfi_offset 3, 0
	.cfi_offset 5, 8
	.cfi_offset 4, 12
	copy %r28,%r4
	copy %r26,%r5
	ldo -32(%r3),%r28
	fstds %fr7,-16(%r28)
	ldil LR'.LC2,%r28
	ldo RR'.LC2(%r28),%r28
	fldds 0(%r28),%fr23
	ldo -32(%r3),%r28
	fldds -16(%r28),%fr22
	fdiv,dbl %fr23,%fr22,%fr22
	copy %r4,%r28
	copy %r5,%r24
	fcpy,dbl %fr22,%fr5
	bl _ZmldRK4vec3,%r2
	nop
	copy %r4,%r28
	ldw -20(%r3),%r2
	ldw 8(%r3),%r5
	ldw 12(%r3),%r4
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE3929:
	.size	_Zdv4vec3d, .-_Zdv4vec3d
	.section	.text._Z11unit_vector4vec3,"axG",@progbits,_Z11unit_vector4vec3,comdat
	.align 4
	.weak	_Z11unit_vector4vec3
	.type	_Z11unit_vector4vec3, @function
_Z11unit_vector4vec3:
	.PROC
	.CALLINFO FRAME=128,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=5
	.ENTRY
.LFB3932:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,128(%r30)
	stw %r5,32(%r3)
	stw %r4,36(%r3)
	.cfi_offset 3, 0
	.cfi_offset 5, 32
	.cfi_offset 4, 36
	copy %r28,%r5
	copy %r26,%r4
	copy %r4,%r26
	bl _ZNK4vec36lengthEv,%r2
	nop
	fcpy,dbl %fr4,%fr22
	ldw 0(%r4),%r28
	ldw 4(%r4),%r29
	stw %r28,8(%r3)
	stw %r29,12(%r3)
	ldw 8(%r4),%r28
	ldw 12(%r4),%r29
	copy %r3,%r19
	ldo 16(%r19),%r19
	stw %r28,0(%r19)
	stw %r29,4(%r19)
	ldo 8(%r3),%r28
	ldo 16(%r28),%r19
	ldo 0(%r4),%r28
	ldo 16(%r28),%r28
	ldw 4(%r28),%r29
	ldw 0(%r28),%r28
	stw %r28,0(%r19)
	stw %r29,4(%r19)
	copy %r5,%r28
	fcpy,dbl %fr22,%fr7
	ldo 8(%r3),%r19
	copy %r19,%r26
	bl _Zdv4vec3d,%r2
	nop
	copy %r5,%r28
	ldw -20(%r3),%r2
	ldw 32(%r3),%r5
	ldw 36(%r3),%r4
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE3932:
	.size	_Z11unit_vector4vec3, .-_Z11unit_vector4vec3
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.section	.text._ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 4
	.weak	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED2Ev
	.type	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED2Ev:
	.PROC
	.CALLINFO FRAME=64,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB3938:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldo 4(%r28),%r28
	copy %r28,%r26
	bl _ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED1Ev,%r2
	nop
	nop
	ldw -20(%r3),%r2
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE3938:
	.size	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED1Ev
	.set	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED1Ev,_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt10shared_ptrI8materialED2Ev,"axG",@progbits,_ZNSt10shared_ptrI8materialED5Ev,comdat
	.align 4
	.weak	_ZNSt10shared_ptrI8materialED2Ev
	.type	_ZNSt10shared_ptrI8materialED2Ev, @function
_ZNSt10shared_ptrI8materialED2Ev:
	.PROC
	.CALLINFO FRAME=64,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB3940:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	copy %r28,%r26
	bl _ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EED2Ev,%r2
	nop
	nop
	ldw -20(%r3),%r2
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE3940:
	.size	_ZNSt10shared_ptrI8materialED2Ev, .-_ZNSt10shared_ptrI8materialED2Ev
	.weak	_ZNSt10shared_ptrI8materialED1Ev
	.set	_ZNSt10shared_ptrI8materialED1Ev,_ZNSt10shared_ptrI8materialED2Ev
	.section	.text._ZN10hit_recordC2Ev,"axG",@progbits,_ZN10hit_recordC5Ev,comdat
	.align 4
	.weak	_ZN10hit_recordC2Ev
	.type	_ZN10hit_recordC2Ev, @function
_ZN10hit_recordC2Ev:
	.PROC
	.CALLINFO FRAME=64,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB3942:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	copy %r28,%r26
	bl _ZN4vec3C1Ev,%r2
	nop
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldo 24(%r28),%r28
	copy %r28,%r26
	bl _ZN4vec3C1Ev,%r2
	nop
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldo 48(%r28),%r28
	copy %r28,%r26
	bl _ZNSt10shared_ptrI8materialEC1Ev,%r2
	nop
	nop
	ldw -20(%r3),%r2
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE3942:
	.size	_ZN10hit_recordC2Ev, .-_ZN10hit_recordC2Ev
	.weak	_ZN10hit_recordC1Ev
	.set	_ZN10hit_recordC1Ev,_ZN10hit_recordC2Ev
	.section	.text._ZN10hit_recordD2Ev,"axG",@progbits,_ZN10hit_recordD5Ev,comdat
	.align 4
	.weak	_ZN10hit_recordD2Ev
	.type	_ZN10hit_recordD2Ev, @function
_ZN10hit_recordD2Ev:
	.PROC
	.CALLINFO FRAME=64,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB3945:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldo 48(%r28),%r28
	copy %r28,%r26
	bl _ZNSt10shared_ptrI8materialED1Ev,%r2
	nop
	nop
	ldw -20(%r3),%r2
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE3945:
	.size	_ZN10hit_recordD2Ev, .-_ZN10hit_recordD2Ev
	.weak	_ZN10hit_recordD1Ev
	.set	_ZN10hit_recordD1Ev,_ZN10hit_recordD2Ev
	.section	.rodata
	.align 8
.LC3:
	.word	2146435072
	.word	0
	.align 8
.LC4:
	.word	1062232653
	.word	3539053052
	.align 8
.LC5:
	.word	1071644672
	.word	0
	.align 8
.LC6:
	.word	1072693248
	.word	0
	.align 8
.LC7:
	.word	1072064102
	.word	1717986918
	.text
	.align 4
	.type	_ZL9ray_colorRK3rayRK8hittablei, @function
_ZL9ray_colorRK3rayRK8hittablei:
	.PROC
	.CALLINFO FRAME=512,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=4,ENTRY_FR=12
	.ENTRY
.LFB3934:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA3934
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,512(%r30)
	stw %r4,432(%r3)
	.cfi_offset 3, 0
	.cfi_offset 4, 432
	ldo 440(%r3),%r1
	fstds,ma %fr12,8(%r1)
	.cfi_offset 48, 440
	.cfi_offset 49, 444
	copy %r28,%r4
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	stw %r25,0(%r28)
	ldo 0(%r3),%r28
	ldo -44(%r28),%r28
	stw %r24,0(%r28)
	ldo 288(%r3),%r28
	copy %r28,%r26
.LEHB3:
	bl _ZN10hit_recordC1Ev,%r2
	nop
.LEHE3:
	ldo 0(%r3),%r28
	ldo -44(%r28),%r28
	ldw 0(%r28),%r28
	comib,<=,n 1,%r28,.L47
	ldo -64(%r30),%r28
	fcpy,dbl %fr0,%fr22
	fstds %fr22,0(%r28)
	ldo -64(%r30),%r28
	fcpy,dbl %fr0,%fr22
	fstds %fr22,8(%r28)
	fcpy,dbl %fr0,%fr7
	copy %r4,%r26
.LEHB4:
	bl _ZN4vec3C1Eddd,%r2
	nop
	b,n .L48
.L47:
	ldil LR'.LC3,%r28
	ldo RR'.LC3(%r28),%r28
	fldds 0(%r28),%fr22
	fstds %fr22,8(%r3)
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	ldw 0(%r28),%r28
	ldw 0(%r28),%r28
	ldw 0(%r28),%r20
	ldil LR'.LC4,%r28
	ldo RR'.LC4(%r28),%r28
	fldds 0(%r28),%fr23
	ldo 288(%r3),%r19
	ldo -32(%r30),%r28
	ldo -28(%r28),%r28
	stw %r19,0(%r28)
	ldo -64(%r30),%r28
	fldds 8(%r3),%fr22
	fstds %fr22,8(%r28)
	fcpy,dbl %fr23,%fr7
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r25
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	ldw 0(%r28),%r26
	copy %r20,%r22
	bl $$dyncall,%r31
	copy %r31,%r2
	comib,=,n 0,%r28,.L49
	ldo 8(%r3),%r28
	ldo 256(%r28),%r21
	ldo 288(%r3),%r28
	ldo 24(%r28),%r20
	ldo 288(%r3),%r19
	copy %r21,%r28
	copy %r20,%r25
	copy %r19,%r26
	bl _ZplRK4vec3S1_,%r2
	nop
	ldo 8(%r3),%r28
	ldo 232(%r28),%r28
	bl _Z18random_unit_vectorv,%r2
	nop
	ldo 8(%r3),%r28
	ldo 376(%r28),%r28
	ldo 240(%r3),%r20
	ldo 264(%r3),%r19
	copy %r20,%r25
	copy %r19,%r26
	bl _ZplRK4vec3S1_,%r2
	nop
	ldo 8(%r3),%r28
	ldo 136(%r28),%r28
	ldo 288(%r3),%r20
	ldo 384(%r3),%r19
	copy %r20,%r25
	copy %r19,%r26
	bl _ZmiRK4vec3S1_,%r2
	nop
	ldo 144(%r3),%r20
	ldo 288(%r3),%r19
	ldo 168(%r3),%r28
	copy %r20,%r24
	copy %r19,%r25
	copy %r28,%r26
	bl _ZN3rayC1ERK4vec3S2_,%r2
	nop
	ldo 0(%r3),%r28
	ldo -44(%r28),%r28
	ldw 0(%r28),%r28
	ldo -1(%r28),%r19
	ldo 8(%r3),%r28
	ldo 208(%r28),%r28
	ldo 168(%r3),%r20
	copy %r19,%r24
	ldo 0(%r3),%r19
	ldo -40(%r19),%r19
	ldw 0(%r19),%r25
	copy %r20,%r26
	bl _ZL9ray_colorRK3rayRK8hittablei,%r2
	nop
	ldo 216(%r3),%r19
	ldil LR'.LC5,%r28
	ldo RR'.LC5(%r28),%r28
	fldds 0(%r28),%fr22
	copy %r4,%r28
	copy %r19,%r24
	fcpy,dbl %fr22,%fr5
	bl _ZmldRK4vec3,%r2
	nop
	b,n .L48
.L49:
	ldo 8(%r3),%r28
	ldo 112(%r28),%r28
	ldo 0(%r3),%r19
	ldo -36(%r19),%r19
	ldw 0(%r19),%r26
	bl _ZNK3ray9directionEv,%r2
	nop
	ldo 8(%r3),%r28
	ldo 352(%r28),%r20
	ldo 8(%r3),%r28
	ldo 112(%r28),%r28
	ldo 8(%r3),%r19
	ldo 400(%r19),%r19
	ldw 0(%r28),%r21
	ldw 4(%r28),%r22
	stw %r21,0(%r19)
	stw %r22,4(%r19)
	ldw 8(%r28),%r21
	ldw 12(%r28),%r22
	stw %r21,8(%r19)
	stw %r22,12(%r19)
	ldo 0(%r19),%r19
	ldo 16(%r19),%r19
	ldo 0(%r28),%r28
	ldo 16(%r28),%r28
	ldw 4(%r28),%r29
	ldw 0(%r28),%r28
	stw %r28,0(%r19)
	stw %r29,4(%r19)
	ldo 408(%r3),%r19
	copy %r20,%r28
	copy %r19,%r26
	bl _Z11unit_vector4vec3,%r2
	nop
	ldo 360(%r3),%r28
	copy %r28,%r26
	bl _ZNK4vec31yEv,%r2
	nop
	fcpy,dbl %fr4,%fr23
	ldil LR'.LC6,%r28
	ldo RR'.LC6(%r28),%r28
	fldds 0(%r28),%fr22
	fadd,dbl %fr23,%fr22,%fr23
	ldil LR'.LC5,%r28
	ldo RR'.LC5(%r28),%r28
	fldds 0(%r28),%fr22
	fmpy,dbl %fr23,%fr22,%fr22
	ldo 32(%r3),%r28
	fstds %fr22,-16(%r28)
	ldil LR'.LC6,%r28
	ldo RR'.LC6(%r28),%r28
	fldds 0(%r28),%fr23
	ldo 32(%r3),%r28
	fldds -16(%r28),%fr22
	fsub,dbl %fr23,%fr22,%fr12
	ldil LR'.LC6,%r28
	ldo RR'.LC6(%r28),%r28
	fldds 0(%r28),%fr23
	ldo 72(%r3),%r20
	ldo -64(%r30),%r28
	ldil LR'.LC6,%r19
	ldo RR'.LC6(%r19),%r19
	fldds 0(%r19),%fr22
	fstds %fr22,0(%r28)
	ldo -64(%r30),%r28
	ldil LR'.LC6,%r19
	ldo RR'.LC6(%r19),%r19
	fldds 0(%r19),%fr22
	fstds %fr22,8(%r28)
	fcpy,dbl %fr23,%fr7
	copy %r20,%r26
	bl _ZN4vec3C1Eddd,%r2
	nop
	ldo 8(%r3),%r28
	ldo 88(%r28),%r28
	ldo 72(%r3),%r19
	copy %r19,%r24
	fcpy,dbl %fr12,%fr5
	bl _ZmldRK4vec3,%r2
	nop
	ldil LR'.LC5,%r28
	ldo RR'.LC5(%r28),%r28
	fldds 0(%r28),%fr23
	ldo 24(%r3),%r20
	ldo -64(%r30),%r28
	ldil LR'.LC6,%r19
	ldo RR'.LC6(%r19),%r19
	fldds 0(%r19),%fr22
	fstds %fr22,0(%r28)
	ldo -64(%r30),%r28
	ldil LR'.LC7,%r19
	ldo RR'.LC7(%r19),%r19
	fldds 0(%r19),%fr22
	fstds %fr22,8(%r28)
	fcpy,dbl %fr23,%fr7
	copy %r20,%r26
	bl _ZN4vec3C1Eddd,%r2
	nop
	ldo 8(%r3),%r28
	ldo 40(%r28),%r28
	ldo 24(%r3),%r19
	copy %r19,%r24
	ldo 32(%r3),%r19
	fldds -16(%r19),%fr5
	bl _ZmldRK4vec3,%r2
	nop
	ldo 48(%r3),%r20
	ldo 96(%r3),%r19
	copy %r4,%r28
	copy %r20,%r25
	copy %r19,%r26
	bl _ZplRK4vec3S1_,%r2
	nop
.LEHE4:
.L48:
	ldo 288(%r3),%r28
	copy %r28,%r26
	bl _ZN10hit_recordD1Ev,%r2
	nop
	b,n .L53
.L52:
	copy %r20,%r4
	ldo 288(%r3),%r28
	copy %r28,%r26
	bl _ZN10hit_recordD1Ev,%r2
	nop
	copy %r4,%r28
	copy %r28,%r26
.LEHB5:
	bl _Unwind_Resume,%r2
	nop
.LEHE5:
.L53:
	copy %r4,%r28
	ldw -20(%r3),%r2
	ldw 432(%r3),%r4
	ldo 440(%r3),%r1
	fldds,ma 8(%r1),%fr12
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE3934:
	.section	.gcc_except_table
.LLSDA3934:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE3934-.LLSDACSB3934
.LLSDACSB3934:
	.uleb128 .LEHB3-.LFB3934
	.uleb128 .LEHE3-.LEHB3
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB4-.LFB3934
	.uleb128 .LEHE4-.LEHB4
	.uleb128 .L52-.LFB3934
	.uleb128 0
	.uleb128 .LEHB5-.LFB3934
	.uleb128 .LEHE5-.LEHB5
	.uleb128 0
	.uleb128 0
.LLSDACSE3934:
	.text
	.size	_ZL9ray_colorRK3rayRK8hittablei, .-_ZL9ray_colorRK3rayRK8hittablei
	.section	.text._ZN13hittable_listD2Ev,"axG",@progbits,_ZN13hittable_listD5Ev,comdat
	.align 4
	.weak	_ZN13hittable_listD2Ev
	.type	_ZN13hittable_listD2Ev, @function
_ZN13hittable_listD2Ev:
	.PROC
	.CALLINFO FRAME=64,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB3949:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	addil LR'_ZTV13hittable_list-$global$,%r27
	copy %r1,%r28
	ldo RR'_ZTV13hittable_list-$global$+8(%r28),%r19
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	stw %r19,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldo 4(%r28),%r28
	copy %r28,%r26
	bl _ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED1Ev,%r2
	nop
	nop
	ldw -20(%r3),%r2
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE3949:
	.size	_ZN13hittable_listD2Ev, .-_ZN13hittable_listD2Ev
	.weak	_ZN13hittable_listD1Ev
	.set	_ZN13hittable_listD1Ev,_ZN13hittable_listD2Ev
	.section	.text._ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 4
	.weak	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED2Ev
	.type	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED2Ev:
	.PROC
	.CALLINFO FRAME=64,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB3953:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldo 4(%r28),%r28
	copy %r28,%r26
	bl _ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED1Ev,%r2
	nop
	nop
	ldw -20(%r3),%r2
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE3953:
	.size	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED1Ev
	.set	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED1Ev,_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt10shared_ptrI6sphereED2Ev,"axG",@progbits,_ZNSt10shared_ptrI6sphereED5Ev,comdat
	.align 4
	.weak	_ZNSt10shared_ptrI6sphereED2Ev
	.type	_ZNSt10shared_ptrI6sphereED2Ev, @function
_ZNSt10shared_ptrI6sphereED2Ev:
	.PROC
	.CALLINFO FRAME=64,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB3955:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	copy %r28,%r26
	bl _ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EED2Ev,%r2
	nop
	nop
	ldw -20(%r3),%r2
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE3955:
	.size	_ZNSt10shared_ptrI6sphereED2Ev, .-_ZNSt10shared_ptrI6sphereED2Ev
	.weak	_ZNSt10shared_ptrI6sphereED1Ev
	.set	_ZNSt10shared_ptrI6sphereED1Ev,_ZNSt10shared_ptrI6sphereED2Ev
	.section	.text._ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 4
	.weak	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED2Ev
	.type	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED2Ev:
	.PROC
	.CALLINFO FRAME=64,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB3959:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldo 4(%r28),%r28
	copy %r28,%r26
	bl _ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED1Ev,%r2
	nop
	nop
	ldw -20(%r3),%r2
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE3959:
	.size	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED1Ev
	.set	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED1Ev,_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt10shared_ptrI8hittableED2Ev,"axG",@progbits,_ZNSt10shared_ptrI8hittableED5Ev,comdat
	.align 4
	.weak	_ZNSt10shared_ptrI8hittableED2Ev
	.type	_ZNSt10shared_ptrI8hittableED2Ev, @function
_ZNSt10shared_ptrI8hittableED2Ev:
	.PROC
	.CALLINFO FRAME=64,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB3961:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	copy %r28,%r26
	bl _ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EED2Ev,%r2
	nop
	nop
	ldw -20(%r3),%r2
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE3961:
	.size	_ZNSt10shared_ptrI8hittableED2Ev, .-_ZNSt10shared_ptrI8hittableED2Ev
	.weak	_ZNSt10shared_ptrI8hittableED1Ev
	.set	_ZNSt10shared_ptrI8hittableED1Ev,_ZNSt10shared_ptrI8hittableED2Ev
	.section	.rodata
	.align 4
.LC12:
	.stringz	"chapter8.ppm"
	.align 4
.LC13:
	.stringz	"P3\n"
	.align 4
.LC14:
	.stringz	"\n255\n"
	.align 4
.LC15:
	.stringz	"\rScanlines remaining: "
	.align 4
.LC19:
	.stringz	"\nDone.\n"
	.align 8
.LC8:
	.word	1073508807
	.word	477218588
	.align 8
.LC9:
	.word	-1074790400
	.word	0
	.align 8
.LC10:
	.word	1071644672
	.word	0
	.align 8
.LC11:
	.word	-1067900928
	.word	0
	.align 4
.LC16:
	.word	P%_ZSt5flushIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_
	.align 8
.LC17:
	.word	1081667584
	.word	0
	.align 8
.LC18:
	.word	1080819712
	.word	0
	.text
	.align 4
.globl main
	.type	main, @function
main:
	.PROC
	.CALLINFO FRAME=896,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=4,ENTRY_FR=12
	.ENTRY
.LFB3947:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA3947
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,896(%r30)
	stw %r4,800(%r3)
	.cfi_offset 3, 0
	.cfi_offset 4, 800
	ldo 808(%r3),%r1
	fstds,ma %fr12,8(%r1)
	.cfi_offset 48, 808
	.cfi_offset 49, 812
	ldo 40(%r3),%r28
	ldil LR'.LC8,%r19
	ldo RR'.LC8(%r19),%r19
	fldds 0(%r19),%fr22
	fstds %fr22,-16(%r28)
	ldo 8(%r3),%r28
	ldo 24(%r28),%r28
	ldi 400,%r19
	stw %r19,0(%r28)
	ldo 8(%r3),%r28
	ldo 28(%r28),%r28
	ldi 225,%r19
	stw %r19,0(%r28)
	ldo 8(%r3),%r28
	ldo 32(%r28),%r28
	ldi 100,%r19
	stw %r19,0(%r28)
	ldo 8(%r3),%r28
	ldo 36(%r28),%r28
	ldi 50,%r19
	stw %r19,0(%r28)
	ldo 184(%r3),%r28
	copy %r28,%r26
.LEHB6:
	bl _ZN13hittable_listC1Ev,%r2
	nop
.LEHE6:
	ldo 144(%r3),%r20
	ldo -64(%r30),%r28
	ldil LR'.LC9,%r19
	ldo RR'.LC9(%r19),%r19
	fldds 0(%r19),%fr22
	fstds %fr22,0(%r28)
	ldo -64(%r30),%r28
	fcpy,dbl %fr0,%fr22
	fstds %fr22,8(%r28)
	fcpy,dbl %fr0,%fr7
	copy %r20,%r26
.LEHB7:
	bl _ZN4vec3C1Eddd,%r2
	nop
.LEHE7:
	ldo 136(%r3),%r28
	ldil LR'.LC10,%r19
	ldo RR'.LC10(%r19),%r19
	fldds 0(%r19),%fr22
	fstds %fr22,0(%r28)
	ldo 8(%r3),%r28
	ldo 160(%r28),%r28
	ldo 136(%r3),%r20
	ldo 144(%r3),%r19
	copy %r20,%r25
	copy %r19,%r26
.LEHB8:
	bl _ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_,%r2
	nop
.LEHE8:
	ldo 168(%r3),%r19
	ldo 176(%r3),%r28
	copy %r19,%r25
	copy %r28,%r26
	bl _ZNSt10shared_ptrI8hittableEC1I6spherevEEOS_IT_E,%r2
	nop
	ldo 176(%r3),%r19
	ldo 184(%r3),%r28
	copy %r19,%r25
	copy %r28,%r26
.LEHB9:
	bl _ZN13hittable_list3addESt10shared_ptrI8hittableE,%r2
	nop
.LEHE9:
	ldo 176(%r3),%r28
	copy %r28,%r26
	bl _ZNSt10shared_ptrI8hittableED1Ev,%r2
	nop
	ldo 168(%r3),%r28
	copy %r28,%r26
	bl _ZNSt10shared_ptrI6sphereED1Ev,%r2
	nop
	ldo 96(%r3),%r20
	ldo -64(%r30),%r28
	ldil LR'.LC9,%r19
	ldo RR'.LC9(%r19),%r19
	fldds 0(%r19),%fr22
	fstds %fr22,0(%r28)
	ldo -64(%r30),%r28
	ldil LR'.LC11,%r19
	ldo RR'.LC11(%r19),%r19
	fldds 0(%r19),%fr22
	fstds %fr22,8(%r28)
	fcpy,dbl %fr0,%fr7
	copy %r20,%r26
.LEHB10:
	bl _ZN4vec3C1Eddd,%r2
	nop
.LEHE10:
	ldo 8(%r3),%r28
	ldo 80(%r28),%r28
	ldi 100,%r19
	stw %r19,0(%r28)
	ldo 8(%r3),%r28
	ldo 112(%r28),%r28
	ldo 88(%r3),%r20
	ldo 96(%r3),%r19
	copy %r20,%r25
	copy %r19,%r26
.LEHB11:
	bl _ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_,%r2
	nop
.LEHE11:
	ldo 120(%r3),%r19
	ldo 128(%r3),%r28
	copy %r19,%r25
	copy %r28,%r26
	bl _ZNSt10shared_ptrI8hittableEC1I6spherevEEOS_IT_E,%r2
	nop
	ldo 128(%r3),%r19
	ldo 184(%r3),%r28
	copy %r19,%r25
	copy %r28,%r26
.LEHB12:
	bl _ZN13hittable_list3addESt10shared_ptrI8hittableE,%r2
	nop
.LEHE12:
	ldo 128(%r3),%r28
	copy %r28,%r26
	bl _ZNSt10shared_ptrI8hittableED1Ev,%r2
	nop
	ldo 120(%r3),%r28
	copy %r28,%r26
	bl _ZNSt10shared_ptrI6sphereED1Ev,%r2
	nop
	ldo 200(%r3),%r28
	copy %r28,%r26
.LEHB13:
	bl _ZN6cameraC1Ev,%r2
	nop
	ldo 384(%r3),%r28
	copy %r28,%r26
	bl _ZNSt14basic_ofstreamIcSt11char_traitsIcEEC1Ev,%r2
	nop
.LEHE13:
	ldo 384(%r3),%r19
	ldi 16,%r24
	ldil LR'.LC12,%r28
	ldo RR'.LC12(%r28),%r25
	copy %r19,%r26
.LEHB14:
	bl _ZNSt14basic_ofstreamIcSt11char_traitsIcEE4openEPKcSt13_Ios_Openmode,%r2
	nop
	ldo 384(%r3),%r19
	ldil LR'.LC13,%r28
	ldo RR'.LC13(%r28),%r25
	copy %r19,%r26
	bl _ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc,%r2
	nop
	ldi 400,%r25
	copy %r28,%r26
	bl _ZNSolsEi,%r2
	nop
	ldi 32,%r25
	copy %r28,%r26
	bl _ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_c,%r2
	nop
	ldi 225,%r25
	copy %r28,%r26
	bl _ZNSolsEi,%r2
	nop
	copy %r28,%r19
	ldil LR'.LC14,%r28
	ldo RR'.LC14(%r28),%r25
	copy %r19,%r26
	bl _ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc,%r2
	nop
	ldi 224,%r28
	stw %r28,8(%r3)
.L65:
	ldw 8(%r3),%r28
	comib,>=,n -1,%r28,.L60
	ldil LR'.LC15,%r28
	ldo RR'.LC15(%r28),%r25
	addil LR'_ZSt4cerr-$global$,%r27
	copy %r1,%r28
	ldo RR'_ZSt4cerr-$global$(%r28),%r26
	bl _ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc,%r2
	nop
	ldw 8(%r3),%r25
	copy %r28,%r26
	bl _ZNSolsEi,%r2
	nop
	ldi 32,%r25
	copy %r28,%r26
	bl _ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_c,%r2
	nop
	copy %r28,%r19
	ldil LR'.LC16,%r28
	ldo RR'.LC16(%r28),%r28
	ldw 0(%r28),%r28
	copy %r28,%r25
	copy %r19,%r26
	bl _ZNSolsEPFRSoS_E,%r2
	nop
	stw %r0,12(%r3)
.L64:
	ldw 12(%r3),%r19
	ldi 399,%r28
	comb,<,n %r28,%r19,.L61
	ldo 704(%r3),%r19
	ldo -64(%r30),%r28
	fcpy,dbl %fr0,%fr22
	fstds %fr22,0(%r28)
	ldo -64(%r30),%r28
	fcpy,dbl %fr0,%fr22
	fstds %fr22,8(%r28)
	fcpy,dbl %fr0,%fr7
	copy %r19,%r26
	bl _ZN4vec3C1Eddd,%r2
	nop
	copy %r3,%r28
	ldo 16(%r28),%r28
	stw %r0,0(%r28)
.L63:
	copy %r3,%r28
	ldo 16(%r28),%r28
	ldw 0(%r28),%r19
	ldi 99,%r28
	comb,<,n %r28,%r19,.L62
	fldws 12(%r3),%fr22L
	fcnvxf,sgl,dbl %fr22L,%fr12
	bl _Z13random_doublev,%r2
	nop
	fcpy,dbl %fr4,%fr22
	fadd,dbl %fr12,%fr22,%fr23
	ldo 40(%r3),%r28
	ldil LR'.LC17,%r19
	ldo RR'.LC17(%r19),%r19
	fldds 0(%r19),%fr22
	fdiv,dbl %fr23,%fr22,%fr22
	fstds %fr22,8(%r28)
	fldws 8(%r3),%fr22L
	fcnvxf,sgl,dbl %fr22L,%fr12
	bl _Z13random_doublev,%r2
	nop
	fcpy,dbl %fr4,%fr22
	fadd,dbl %fr12,%fr22,%fr23
	ldo 72(%r3),%r28
	ldil LR'.LC18,%r19
	ldo RR'.LC18(%r19),%r19
	fldds 0(%r19),%fr22
	fdiv,dbl %fr23,%fr22,%fr22
	fstds %fr22,-16(%r28)
	ldo 8(%r3),%r28
	ldo 720(%r28),%r22
	ldo 40(%r3),%r19
	ldo 200(%r3),%r21
	ldo 72(%r3),%r20
	ldo -64(%r30),%r28
	fldds -16(%r20),%fr22
	fstds %fr22,8(%r28)
	copy %r22,%r28
	fldds 8(%r19),%fr7
	copy %r21,%r26
	bl _ZNK6camera7get_rayEdd,%r2
	nop
	ldo 8(%r3),%r28
	ldo 56(%r28),%r28
	ldo 184(%r3),%r20
	ldo 728(%r3),%r19
	ldi 50,%r24
	copy %r20,%r25
	copy %r19,%r26
	bl _ZL9ray_colorRK3rayRK8hittablei,%r2
	nop
	ldo 64(%r3),%r19
	ldo 704(%r3),%r28
	copy %r19,%r25
	copy %r28,%r26
	bl _ZN4vec3pLERKS_,%r2
	nop
	copy %r3,%r28
	ldo 16(%r28),%r28
	ldw 0(%r28),%r28
	ldo 1(%r28),%r19
	copy %r3,%r28
	ldo 16(%r28),%r28
	stw %r19,0(%r28)
	b,n .L63
.L62:
	ldo 8(%r3),%r28
	ldo 696(%r28),%r28
	ldo 8(%r3),%r19
	ldo 768(%r19),%r19
	ldw 0(%r28),%r21
	ldw 4(%r28),%r22
	stw %r21,0(%r19)
	stw %r22,4(%r19)
	ldw 8(%r28),%r21
	ldw 12(%r28),%r22
	stw %r21,8(%r19)
	stw %r22,12(%r19)
	ldo 0(%r19),%r19
	ldo 16(%r19),%r19
	ldo 0(%r28),%r28
	ldo 16(%r28),%r28
	ldw 4(%r28),%r29
	ldw 0(%r28),%r28
	stw %r28,0(%r19)
	stw %r29,4(%r19)
	ldo 776(%r3),%r19
	ldo 384(%r3),%r28
	ldi 100,%r24
	copy %r19,%r25
	copy %r28,%r26
	bl _Z11write_colorRSt14basic_ofstreamIcSt11char_traitsIcEE4vec3i,%r2
	nop
	ldw 12(%r3),%r28
	ldo 1(%r28),%r28
	stw %r28,12(%r3)
	b,n .L64
.L61:
	ldw 8(%r3),%r28
	ldo -1(%r28),%r28
	stw %r28,8(%r3)
	b,n .L65
.L60:
	ldil LR'.LC19,%r28
	ldo RR'.LC19(%r28),%r25
	addil LR'_ZSt4cerr-$global$,%r27
	copy %r1,%r28
	ldo RR'_ZSt4cerr-$global$(%r28),%r26
	bl _ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc,%r2
	nop
.LEHE14:
	ldi 0,%r4
	ldo 384(%r3),%r28
	copy %r28,%r26
	bl _ZNSt14basic_ofstreamIcSt11char_traitsIcEED1Ev,%r2
	nop
	ldo 184(%r3),%r28
	copy %r28,%r26
	bl _ZN13hittable_listD1Ev,%r2
	nop
	copy %r4,%r28
	b,n .L79
.L75:
	copy %r20,%r4
	ldo 176(%r3),%r28
	copy %r28,%r26
	bl _ZNSt10shared_ptrI8hittableED1Ev,%r2
	nop
	ldo 168(%r3),%r28
	copy %r28,%r26
	bl _ZNSt10shared_ptrI6sphereED1Ev,%r2
	nop
	copy %r4,%r28
	b,n .L68
.L74:
	copy %r20,%r28
.L68:
	copy %r28,%r4
	b,n .L69
.L77:
	copy %r20,%r4
	ldo 128(%r3),%r28
	copy %r28,%r26
	bl _ZNSt10shared_ptrI8hittableED1Ev,%r2
	nop
	ldo 120(%r3),%r28
	copy %r28,%r26
	bl _ZNSt10shared_ptrI6sphereED1Ev,%r2
	nop
	copy %r4,%r28
	b,n .L71
.L76:
	copy %r20,%r28
.L71:
	copy %r28,%r4
	b,n .L69
.L78:
	copy %r20,%r4
	ldo 384(%r3),%r28
	copy %r28,%r26
	bl _ZNSt14basic_ofstreamIcSt11char_traitsIcEED1Ev,%r2
	nop
	b,n .L69
.L73:
	copy %r20,%r4
.L69:
	ldo 184(%r3),%r28
	copy %r28,%r26
	bl _ZN13hittable_listD1Ev,%r2
	nop
	copy %r4,%r28
	copy %r28,%r26
.LEHB15:
	bl _Unwind_Resume,%r2
	nop
.LEHE15:
.L79:
	ldw -20(%r3),%r2
	ldw 800(%r3),%r4
	ldo 808(%r3),%r1
	fldds,ma 8(%r1),%fr12
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE3947:
	.section	.gcc_except_table
.LLSDA3947:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE3947-.LLSDACSB3947
.LLSDACSB3947:
	.uleb128 .LEHB6-.LFB3947
	.uleb128 .LEHE6-.LEHB6
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB7-.LFB3947
	.uleb128 .LEHE7-.LEHB7
	.uleb128 .L73-.LFB3947
	.uleb128 0
	.uleb128 .LEHB8-.LFB3947
	.uleb128 .LEHE8-.LEHB8
	.uleb128 .L74-.LFB3947
	.uleb128 0
	.uleb128 .LEHB9-.LFB3947
	.uleb128 .LEHE9-.LEHB9
	.uleb128 .L75-.LFB3947
	.uleb128 0
	.uleb128 .LEHB10-.LFB3947
	.uleb128 .LEHE10-.LEHB10
	.uleb128 .L73-.LFB3947
	.uleb128 0
	.uleb128 .LEHB11-.LFB3947
	.uleb128 .LEHE11-.LEHB11
	.uleb128 .L76-.LFB3947
	.uleb128 0
	.uleb128 .LEHB12-.LFB3947
	.uleb128 .LEHE12-.LEHB12
	.uleb128 .L77-.LFB3947
	.uleb128 0
	.uleb128 .LEHB13-.LFB3947
	.uleb128 .LEHE13-.LEHB13
	.uleb128 .L73-.LFB3947
	.uleb128 0
	.uleb128 .LEHB14-.LFB3947
	.uleb128 .LEHE14-.LEHB14
	.uleb128 .L78-.LFB3947
	.uleb128 0
	.uleb128 .LEHB15-.LFB3947
	.uleb128 .LEHE15-.LEHB15
	.uleb128 0
	.uleb128 0
.LLSDACSE3947:
	.text
	.size	main, .-main
	.section	.text._ZNSt25uniform_real_distributionIdEC2Edd,"axG",@progbits,_ZNSt25uniform_real_distributionIdEC5Edd,comdat
	.align 4
	.weak	_ZNSt25uniform_real_distributionIdEC2Edd
	.type	_ZNSt25uniform_real_distributionIdEC2Edd, @function
_ZNSt25uniform_real_distributionIdEC2Edd:
	.PROC
	.CALLINFO FRAME=64,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4212:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo -32(%r3),%r28
	fstds %fr7,-16(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r20
	ldo -64(%r3),%r19
	ldo -64(%r30),%r28
	fldds 8(%r19),%fr22
	fstds %fr22,8(%r28)
	ldo -32(%r3),%r28
	fldds -16(%r28),%fr7
	copy %r20,%r26
	bl _ZNSt25uniform_real_distributionIdE10param_typeC1Edd,%r2
	nop
	nop
	ldw -20(%r3),%r2
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4212:
	.size	_ZNSt25uniform_real_distributionIdEC2Edd, .-_ZNSt25uniform_real_distributionIdEC2Edd
	.weak	_ZNSt25uniform_real_distributionIdEC1Edd
	.set	_ZNSt25uniform_real_distributionIdEC1Edd,_ZNSt25uniform_real_distributionIdEC2Edd
	.section	.text._ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC2Ev,"axG",@progbits,_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC5Ev,comdat
	.align 4
	.weak	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC2Ev
	.type	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC2Ev, @function
_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC2Ev:
	.PROC
	.CALLINFO FRAME=64,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4215:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldi 5489,%r25
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC1Ej,%r2
	nop
	nop
	ldw -20(%r3),%r2
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4215:
	.size	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC2Ev, .-_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC2Ev
	.weak	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC1Ev
	.set	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC1Ev,_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC2Ev
	.section	.text._ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEEEdRT_,"axG",@progbits,_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEEEdRT_,comdat
	.align 4
	.weak	_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEEEdRT_
	.type	_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEEEdRT_, @function
_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEEEdRT_:
	.PROC
	.CALLINFO FRAME=64,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4217:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	stw %r25,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	copy %r28,%r24
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	ldw 0(%r28),%r25
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEEEdRT_RKNS0_10param_typeE,%r2
	nop
	fcpy,dbl %fr4,%fr22
	fcpy,dbl %fr22,%fr4
	ldw -20(%r3),%r2
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4217:
	.size	_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEEEdRT_, .-_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEEEdRT_
	.section	.text._ZNSt10shared_ptrI8materialEC2Ev,"axG",@progbits,_ZNSt10shared_ptrI8materialEC5Ev,comdat
	.align 4
	.weak	_ZNSt10shared_ptrI8materialEC2Ev
	.type	_ZNSt10shared_ptrI8materialEC2Ev, @function
_ZNSt10shared_ptrI8materialEC2Ev:
	.PROC
	.CALLINFO FRAME=64,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4245:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	copy %r28,%r26
	bl _ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC2Ev,%r2
	nop
	nop
	ldw -20(%r3),%r2
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4245:
	.size	_ZNSt10shared_ptrI8materialEC2Ev, .-_ZNSt10shared_ptrI8materialEC2Ev
	.weak	_ZNSt10shared_ptrI8materialEC1Ev
	.set	_ZNSt10shared_ptrI8materialEC1Ev,_ZNSt10shared_ptrI8materialEC2Ev
	.section	.text._ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 4
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED2Ev
	.type	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED2Ev:
	.PROC
	.CALLINFO FRAME=64,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4248:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldw 0(%r28),%r28
	comib,=,n 0,%r28,.L87
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldw 0(%r28),%r28
	copy %r28,%r26
	bl _ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv,%r2
	nop
.L87:
	nop
	ldw -20(%r3),%r2
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4248:
	.size	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED1Ev
	.set	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED1Ev,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED2Ev,"axG",@progbits,_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED5Ev,comdat
	.align 4
	.weak	_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED2Ev
	.type	_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED2Ev, @function
_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED2Ev:
	.PROC
	.CALLINFO FRAME=64,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=5
	.ENTRY
.LFB4251:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA4251
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	stw %r5,8(%r3)
	stw %r4,12(%r3)
	.cfi_offset 3, 0
	.cfi_offset 5, 8
	.cfi_offset 4, 12
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldw 0(%r28),%r4
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldw 4(%r28),%r5
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	copy %r28,%r26
	bl _ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE19_M_get_Tp_allocatorEv,%r2
	nop
	copy %r28,%r24
	copy %r5,%r25
	copy %r4,%r26
	bl _ZSt8_DestroyIPSt10shared_ptrI8hittableES2_EvT_S4_RSaIT0_E,%r2
	nop
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	copy %r28,%r26
	bl _ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED2Ev,%r2
	nop
	nop
	ldw -20(%r3),%r2
	ldw 8(%r3),%r5
	ldw 12(%r3),%r4
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4251:
	.section	.gcc_except_table
.LLSDA4251:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4251-.LLSDACSB4251
.LLSDACSB4251:
.LLSDACSE4251:
	.section	.text._ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED2Ev,"axG",@progbits,_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED5Ev,comdat
	.size	_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED2Ev, .-_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED2Ev
	.weak	_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED1Ev
	.set	_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED1Ev,_ZNSt6vectorISt10shared_ptrI8hittableESaIS2_EED2Ev
	.section	.text._ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_,"axG",@progbits,_ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_,comdat
	.align 4
	.weak	_ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_
	.type	_ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_, @function
_ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_:
	.PROC
	.CALLINFO FRAME=128,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=5
	.ENTRY
.LFB4253:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA4253
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,128(%r30)
	stw %r5,12(%r3)
	stw %r4,16(%r3)
	.cfi_offset 3, 0
	.cfi_offset 5, 12
	.cfi_offset 4, 16
	copy %r28,%r4
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	stw %r25,0(%r28)
	ldo 8(%r3),%r28
	copy %r28,%r26
	bl _ZNSaI6sphereEC1Ev,%r2
	nop
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE,%r2
	nop
	copy %r28,%r5
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE,%r2
	nop
	copy %r28,%r19
	copy %r4,%r28
	copy %r19,%r24
	copy %r5,%r25
	ldo 8(%r3),%r19
	copy %r19,%r26
.LEHB16:
	bl _ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3dEESt10shared_ptrIT_ERKT0_DpOT1_,%r2
	nop
.LEHE16:
	ldo 8(%r3),%r28
	copy %r28,%r26
	bl _ZNSaI6sphereED1Ev,%r2
	nop
	b,n .L93
.L92:
	copy %r20,%r4
	ldo 8(%r3),%r28
	copy %r28,%r26
	bl _ZNSaI6sphereED1Ev,%r2
	nop
	copy %r4,%r28
	copy %r28,%r26
.LEHB17:
	bl _Unwind_Resume,%r2
	nop
.LEHE17:
.L93:
	copy %r4,%r28
	copy %r4,%r28
	ldw -20(%r3),%r2
	ldw 12(%r3),%r5
	ldw 16(%r3),%r4
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4253:
	.section	.gcc_except_table
.LLSDA4253:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4253-.LLSDACSB4253
.LLSDACSB4253:
	.uleb128 .LEHB16-.LFB4253
	.uleb128 .LEHE16-.LEHB16
	.uleb128 .L92-.LFB4253
	.uleb128 0
	.uleb128 .LEHB17-.LFB4253
	.uleb128 .LEHE17-.LEHB17
	.uleb128 0
	.uleb128 0
.LLSDACSE4253:
	.section	.text._ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_,"axG",@progbits,_ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_,comdat
	.size	_ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_, .-_ZSt11make_sharedI6sphereJ4vec3dEESt10shared_ptrIT_EDpOT0_
	.section	.text._ZNSt10shared_ptrI8hittableEC2I6spherevEEOS_IT_E,"axG",@progbits,_ZNSt10shared_ptrI8hittableEC5I6spherevEEOS_IT_E,comdat
	.align 4
	.weak	_ZNSt10shared_ptrI8hittableEC2I6spherevEEOS_IT_E
	.type	_ZNSt10shared_ptrI8hittableEC2I6spherevEEOS_IT_E, @function
_ZNSt10shared_ptrI8hittableEC2I6spherevEEOS_IT_E:
	.PROC
	.CALLINFO FRAME=64,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=4
	.ENTRY
.LFB4255:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	stw %r4,8(%r3)
	.cfi_offset 3, 0
	.cfi_offset 4, 8
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	stw %r25,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r4
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZSt4moveIRSt10shared_ptrI6sphereEEONSt16remove_referenceIT_E4typeEOS5_,%r2
	nop
	copy %r28,%r25
	copy %r4,%r26
	bl _ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC2I6spherevEEOS_IT_LS2_2EE,%r2
	nop
	nop
	ldw -20(%r3),%r2
	ldw 8(%r3),%r4
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4255:
	.size	_ZNSt10shared_ptrI8hittableEC2I6spherevEEOS_IT_E, .-_ZNSt10shared_ptrI8hittableEC2I6spherevEEOS_IT_E
	.weak	_ZNSt10shared_ptrI8hittableEC1I6spherevEEOS_IT_E
	.set	_ZNSt10shared_ptrI8hittableEC1I6spherevEEOS_IT_E,_ZNSt10shared_ptrI8hittableEC2I6spherevEEOS_IT_E
	.section	.text._ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_,"axG",@progbits,_ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_,comdat
	.align 4
	.weak	_ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_
	.type	_ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_, @function
_ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_:
	.PROC
	.CALLINFO FRAME=128,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=5
	.ENTRY
.LFB4260:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA4260
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,128(%r30)
	stw %r5,12(%r3)
	stw %r4,16(%r3)
	.cfi_offset 3, 0
	.cfi_offset 5, 12
	.cfi_offset 4, 16
	copy %r28,%r4
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	stw %r25,0(%r28)
	ldo 8(%r3),%r28
	copy %r28,%r26
	bl _ZNSaI6sphereEC1Ev,%r2
	nop
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE,%r2
	nop
	copy %r28,%r5
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE,%r2
	nop
	copy %r28,%r19
	copy %r4,%r28
	copy %r19,%r24
	copy %r5,%r25
	ldo 8(%r3),%r19
	copy %r19,%r26
.LEHB18:
	bl _ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3iEESt10shared_ptrIT_ERKT0_DpOT1_,%r2
	nop
.LEHE18:
	ldo 8(%r3),%r28
	copy %r28,%r26
	bl _ZNSaI6sphereED1Ev,%r2
	nop
	b,n .L99
.L98:
	copy %r20,%r4
	ldo 8(%r3),%r28
	copy %r28,%r26
	bl _ZNSaI6sphereED1Ev,%r2
	nop
	copy %r4,%r28
	copy %r28,%r26
.LEHB19:
	bl _Unwind_Resume,%r2
	nop
.LEHE19:
.L99:
	copy %r4,%r28
	copy %r4,%r28
	ldw -20(%r3),%r2
	ldw 12(%r3),%r5
	ldw 16(%r3),%r4
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4260:
	.section	.gcc_except_table
.LLSDA4260:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4260-.LLSDACSB4260
.LLSDACSB4260:
	.uleb128 .LEHB18-.LFB4260
	.uleb128 .LEHE18-.LEHB18
	.uleb128 .L98-.LFB4260
	.uleb128 0
	.uleb128 .LEHB19-.LFB4260
	.uleb128 .LEHE19-.LEHB19
	.uleb128 0
	.uleb128 0
.LLSDACSE4260:
	.section	.text._ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_,"axG",@progbits,_ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_,comdat
	.size	_ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_, .-_ZSt11make_sharedI6sphereJ4vec3iEESt10shared_ptrIT_EDpOT0_
	.section	.text._ZNSt25uniform_real_distributionIdE10param_typeC2Edd,"axG",@progbits,_ZNSt25uniform_real_distributionIdE10param_typeC5Edd,comdat
	.align 4
	.weak	_ZNSt25uniform_real_distributionIdE10param_typeC2Edd
	.type	_ZNSt25uniform_real_distributionIdE10param_typeC2Edd, @function
_ZNSt25uniform_real_distributionIdE10param_typeC2Edd:
	.PROC
	.CALLINFO FRAME=64,NO_CALLS,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4387:
	.cfi_startproc
	copy %r3,%r1
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo -32(%r3),%r28
	fstds %fr7,-16(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldo -32(%r3),%r19
	fldds -16(%r19),%fr22
	fstds %fr22,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldo -64(%r3),%r19
	fldds 8(%r19),%fr22
	fstds %fr22,8(%r28)
	nop
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4387:
	.size	_ZNSt25uniform_real_distributionIdE10param_typeC2Edd, .-_ZNSt25uniform_real_distributionIdE10param_typeC2Edd
	.weak	_ZNSt25uniform_real_distributionIdE10param_typeC1Edd
	.set	_ZNSt25uniform_real_distributionIdE10param_typeC1Edd,_ZNSt25uniform_real_distributionIdE10param_typeC2Edd
	.section	.text._ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC2Ej,"axG",@progbits,_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC5Ej,comdat
	.align 4
	.weak	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC2Ej
	.type	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC2Ej, @function
_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC2Ej:
	.PROC
	.CALLINFO FRAME=64,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4390:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	stw %r25,0(%r28)
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	ldw 0(%r28),%r25
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE4seedEj,%r2
	nop
	nop
	ldw -20(%r3),%r2
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4390:
	.size	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC2Ej, .-_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC2Ej
	.weak	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC1Ej
	.set	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC1Ej,_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEC2Ej
	.section	.text._ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEEEdRT_RKNS0_10param_typeE,"axG",@progbits,_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEEEdRT_RKNS0_10param_typeE,comdat
	.align 4
	.weak	_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEEEdRT_RKNS0_10param_typeE
	.type	_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEEEdRT_RKNS0_10param_typeE, @function
_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEEEdRT_RKNS0_10param_typeE:
	.PROC
	.CALLINFO FRAME=128,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=3,ENTRY_FR=13
	.ENTRY
.LFB4392:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,128(%r30)
	.cfi_offset 3, 0
	ldo 16(%r3),%r1
	fstds,ma %fr13,8(%r1)
	fstds,ma %fr12,8(%r1)
	.cfi_offset 50, 16
	.cfi_offset 51, 20
	.cfi_offset 48, 24
	.cfi_offset 49, 28
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	stw %r25,0(%r28)
	ldo 0(%r3),%r28
	ldo -44(%r28),%r28
	stw %r24,0(%r28)
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	ldw 0(%r28),%r25
	ldo 8(%r3),%r28
	copy %r28,%r26
	bl _ZNSt8__detail8_AdaptorISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEdEC1ERS2_,%r2
	nop
	ldo 8(%r3),%r28
	copy %r28,%r26
	bl _ZNSt8__detail8_AdaptorISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEdEclEv,%r2
	nop
	fcpy,dbl %fr4,%fr12
	ldo 0(%r3),%r28
	ldo -44(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZNKSt25uniform_real_distributionIdE10param_type1bEv,%r2
	nop
	fcpy,dbl %fr4,%fr13
	ldo 0(%r3),%r28
	ldo -44(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZNKSt25uniform_real_distributionIdE10param_type1aEv,%r2
	nop
	fcpy,dbl %fr4,%fr22
	fsub,dbl %fr13,%fr22,%fr22
	fmpy,dbl %fr12,%fr22,%fr12
	ldo 0(%r3),%r28
	ldo -44(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZNKSt25uniform_real_distributionIdE10param_type1aEv,%r2
	nop
	fcpy,dbl %fr4,%fr22
	fadd,dbl %fr12,%fr22,%fr22
	fcpy,dbl %fr22,%fr4
	ldw -20(%r3),%r2
	ldo 16(%r3),%r1
	fldds,ma 8(%r1),%fr13
	fldds,ma 8(%r1),%fr12
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4392:
	.size	_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEEEdRT_RKNS0_10param_typeE, .-_ZNSt25uniform_real_distributionIdEclISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEEEdRT_RKNS0_10param_typeE
	.section	.text._ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC2Ev,"axG",@progbits,_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC5Ev,comdat
	.align 4
	.weak	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC2Ev
	.type	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC2Ev, @function
_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC2Ev:
	.PROC
	.CALLINFO FRAME=64,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4404:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	stw %r0,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldo 4(%r28),%r28
	copy %r28,%r26
	bl _ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1Ev,%r2
	nop
	nop
	ldw -20(%r3),%r2
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4404:
	.size	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC2Ev, .-_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC2Ev
	.weak	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC1Ev
	.set	_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC1Ev,_ZNSt12__shared_ptrI8materialLN9__gnu_cxx12_Lock_policyE2EEC2Ev
	.section	.text._ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv,"axG",@progbits,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv,comdat
	.align 4
	.weak	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv
	.type	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv, @function
_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv:
	.PROC
	.CALLINFO FRAME=64,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4406:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldo 4(%r28),%r28
	ldi -1,%r25
	copy %r28,%r26
	bl _ZN9__gnu_cxxL27__exchange_and_add_dispatchEPii,%r2
	nop
	comiclr,<> 1,%r28,%r28
	ldi 1,%r28
	extru %r28,31,8,%r28
	comib,=,n 0,%r28,.L107
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldw 0(%r28),%r28
	ldo 8(%r28),%r28
	ldw 0(%r28),%r19
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r26
	copy %r19,%r22
	bl $$dyncall,%r31
	copy %r31,%r2
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldo 8(%r28),%r28
	ldi -1,%r25
	copy %r28,%r26
	bl _ZN9__gnu_cxxL27__exchange_and_add_dispatchEPii,%r2
	nop
	comiclr,<> 1,%r28,%r28
	ldi 1,%r28
	extru %r28,31,8,%r28
	comib,=,n 0,%r28,.L107
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldw 0(%r28),%r28
	ldo 12(%r28),%r28
	ldw 0(%r28),%r19
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r26
	copy %r19,%r22
	bl $$dyncall,%r31
	copy %r31,%r2
.L107:
	nop
	ldw -20(%r3),%r2
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4406:
	.size	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv, .-_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv
	.section	.text._ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD2Ev,"axG",@progbits,_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD5Ev,comdat
	.align 4
	.weak	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD2Ev
	.type	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD2Ev, @function
_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD2Ev:
	.PROC
	.CALLINFO FRAME=64,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4409:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZNSaISt10shared_ptrI8hittableEED2Ev,%r2
	nop
	nop
	ldw -20(%r3),%r2
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4409:
	.size	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD2Ev, .-_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD2Ev
	.weak	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD1Ev
	.set	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD1Ev,_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD2Ev
	.section	.text._ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED2Ev,"axG",@progbits,_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED5Ev,comdat
	.align 4
	.weak	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED2Ev
	.type	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED2Ev, @function
_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED2Ev:
	.PROC
	.CALLINFO FRAME=64,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4411:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA4411
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldw 0(%r28),%r20
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldw 8(%r28),%r19
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldw 0(%r28),%r28
	sub %r19,%r28,%r28
	extrs %r28,28,29,%r28
	copy %r28,%r24
	copy %r20,%r25
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE13_M_deallocateEPS2_j,%r2
	nop
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	copy %r28,%r26
	bl _ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE12_Vector_implD1Ev,%r2
	nop
	nop
	ldw -20(%r3),%r2
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4411:
	.section	.gcc_except_table
.LLSDA4411:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4411-.LLSDACSB4411
.LLSDACSB4411:
.LLSDACSE4411:
	.section	.text._ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED2Ev,"axG",@progbits,_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED5Ev,comdat
	.size	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED2Ev, .-_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED2Ev
	.weak	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED1Ev
	.set	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED1Ev,_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EED2Ev
	.section	.text._ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE19_M_get_Tp_allocatorEv,"axG",@progbits,_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE19_M_get_Tp_allocatorEv,comdat
	.align 4
	.weak	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE19_M_get_Tp_allocatorEv
	.type	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE19_M_get_Tp_allocatorEv, @function
_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE19_M_get_Tp_allocatorEv:
	.PROC
	.CALLINFO FRAME=64,NO_CALLS,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4413:
	.cfi_startproc
	copy %r3,%r1
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4413:
	.size	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE19_M_get_Tp_allocatorEv, .-_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE19_M_get_Tp_allocatorEv
	.section	.text._ZSt8_DestroyIPSt10shared_ptrI8hittableES2_EvT_S4_RSaIT0_E,"axG",@progbits,_ZSt8_DestroyIPSt10shared_ptrI8hittableES2_EvT_S4_RSaIT0_E,comdat
	.align 4
	.weak	_ZSt8_DestroyIPSt10shared_ptrI8hittableES2_EvT_S4_RSaIT0_E
	.type	_ZSt8_DestroyIPSt10shared_ptrI8hittableES2_EvT_S4_RSaIT0_E, @function
_ZSt8_DestroyIPSt10shared_ptrI8hittableES2_EvT_S4_RSaIT0_E:
	.PROC
	.CALLINFO FRAME=64,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4414:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	stw %r25,0(%r28)
	ldo 0(%r3),%r28
	ldo -44(%r28),%r28
	stw %r24,0(%r28)
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	ldw 0(%r28),%r25
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZSt8_DestroyIPSt10shared_ptrI8hittableEEvT_S4_,%r2
	nop
	nop
	ldw -20(%r3),%r2
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4414:
	.size	_ZSt8_DestroyIPSt10shared_ptrI8hittableES2_EvT_S4_RSaIT0_E, .-_ZSt8_DestroyIPSt10shared_ptrI8hittableES2_EvT_S4_RSaIT0_E
	.section	.text._ZNSaI6sphereEC2Ev,"axG",@progbits,_ZNSaI6sphereEC5Ev,comdat
	.align 4
	.weak	_ZNSaI6sphereEC2Ev
	.type	_ZNSaI6sphereEC2Ev, @function
_ZNSaI6sphereEC2Ev:
	.PROC
	.CALLINFO FRAME=64,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4416:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZN9__gnu_cxx13new_allocatorI6sphereEC2Ev,%r2
	nop
	nop
	ldw -20(%r3),%r2
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4416:
	.size	_ZNSaI6sphereEC2Ev, .-_ZNSaI6sphereEC2Ev
	.weak	_ZNSaI6sphereEC1Ev
	.set	_ZNSaI6sphereEC1Ev,_ZNSaI6sphereEC2Ev
	.section	.text._ZNSaI6sphereED2Ev,"axG",@progbits,_ZNSaI6sphereED5Ev,comdat
	.align 4
	.weak	_ZNSaI6sphereED2Ev
	.type	_ZNSaI6sphereED2Ev, @function
_ZNSaI6sphereED2Ev:
	.PROC
	.CALLINFO FRAME=64,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4419:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZN9__gnu_cxx13new_allocatorI6sphereED2Ev,%r2
	nop
	nop
	ldw -20(%r3),%r2
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4419:
	.size	_ZNSaI6sphereED2Ev, .-_ZNSaI6sphereED2Ev
	.weak	_ZNSaI6sphereED1Ev
	.set	_ZNSaI6sphereED1Ev,_ZNSaI6sphereED2Ev
	.section	.text._ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE,"axG",@progbits,_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE,comdat
	.align 4
	.weak	_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
	.type	_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE, @function
_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE:
	.PROC
	.CALLINFO FRAME=64,NO_CALLS,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4421:
	.cfi_startproc
	copy %r3,%r1
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4421:
	.size	_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE, .-_ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE
	.section	.text._ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE,"axG",@progbits,_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE,comdat
	.align 4
	.weak	_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE
	.type	_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE, @function
_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE:
	.PROC
	.CALLINFO FRAME=64,NO_CALLS,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4422:
	.cfi_startproc
	copy %r3,%r1
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4422:
	.size	_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE, .-_ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE
	.section	.text._ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3dEESt10shared_ptrIT_ERKT0_DpOT1_,"axG",@progbits,_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3dEESt10shared_ptrIT_ERKT0_DpOT1_,comdat
	.align 4
	.weak	_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3dEESt10shared_ptrIT_ERKT0_DpOT1_
	.type	_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3dEESt10shared_ptrIT_ERKT0_DpOT1_, @function
_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3dEESt10shared_ptrIT_ERKT0_DpOT1_:
	.PROC
	.CALLINFO FRAME=128,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=6
	.ENTRY
.LFB4423:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,128(%r30)
	stw %r6,8(%r3)
	stw %r5,12(%r3)
	stw %r4,16(%r3)
	.cfi_offset 3, 0
	.cfi_offset 6, 8
	.cfi_offset 5, 12
	.cfi_offset 4, 16
	copy %r28,%r4
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	stw %r25,0(%r28)
	ldo 0(%r3),%r28
	ldo -44(%r28),%r28
	stw %r24,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r5
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE,%r2
	nop
	copy %r28,%r6
	ldo 0(%r3),%r28
	ldo -44(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE,%r2
	nop
	copy %r28,%r23
	copy %r6,%r24
	copy %r5,%r25
	copy %r4,%r26
	bl _ZNSt10shared_ptrI6sphereEC1ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,%r2
	nop
	copy %r4,%r28
	copy %r4,%r28
	ldw -20(%r3),%r2
	ldw 8(%r3),%r6
	ldw 12(%r3),%r5
	ldw 16(%r3),%r4
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4423:
	.size	_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3dEESt10shared_ptrIT_ERKT0_DpOT1_, .-_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3dEESt10shared_ptrIT_ERKT0_DpOT1_
	.section	.text._ZSt4moveIRSt10shared_ptrI6sphereEEONSt16remove_referenceIT_E4typeEOS5_,"axG",@progbits,_ZSt4moveIRSt10shared_ptrI6sphereEEONSt16remove_referenceIT_E4typeEOS5_,comdat
	.align 4
	.weak	_ZSt4moveIRSt10shared_ptrI6sphereEEONSt16remove_referenceIT_E4typeEOS5_
	.type	_ZSt4moveIRSt10shared_ptrI6sphereEEONSt16remove_referenceIT_E4typeEOS5_, @function
_ZSt4moveIRSt10shared_ptrI6sphereEEONSt16remove_referenceIT_E4typeEOS5_:
	.PROC
	.CALLINFO FRAME=64,NO_CALLS,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4427:
	.cfi_startproc
	copy %r3,%r1
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4427:
	.size	_ZSt4moveIRSt10shared_ptrI6sphereEEONSt16remove_referenceIT_E4typeEOS5_, .-_ZSt4moveIRSt10shared_ptrI6sphereEEONSt16remove_referenceIT_E4typeEOS5_
	.section	.text._ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC2I6spherevEEOS_IT_LS2_2EE,"axG",@progbits,_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC5I6spherevEEOS_IT_LS2_2EE,comdat
	.align 4
	.weak	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC2I6spherevEEOS_IT_LS2_2EE
	.type	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC2I6spherevEEOS_IT_LS2_2EE, @function
_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC2I6spherevEEOS_IT_LS2_2EE:
	.PROC
	.CALLINFO FRAME=64,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4429:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	stw %r25,0(%r28)
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	ldw 0(%r28),%r28
	ldw 0(%r28),%r19
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	stw %r19,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldo 4(%r28),%r28
	copy %r28,%r26
	bl _ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1Ev,%r2
	nop
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldo 4(%r28),%r19
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	ldw 0(%r28),%r28
	ldo 4(%r28),%r28
	copy %r28,%r25
	copy %r19,%r26
	bl _ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EE7_M_swapERS2_,%r2
	nop
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	ldw 0(%r28),%r28
	stw %r0,0(%r28)
	nop
	ldw -20(%r3),%r2
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4429:
	.size	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC2I6spherevEEOS_IT_LS2_2EE, .-_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC2I6spherevEEOS_IT_LS2_2EE
	.weak	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC1I6spherevEEOS_IT_LS2_2EE
	.set	_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC1I6spherevEEOS_IT_LS2_2EE,_ZNSt12__shared_ptrI8hittableLN9__gnu_cxx12_Lock_policyE2EEC2I6spherevEEOS_IT_LS2_2EE
	.section	.text._ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE,"axG",@progbits,_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE,comdat
	.align 4
	.weak	_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE
	.type	_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE, @function
_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE:
	.PROC
	.CALLINFO FRAME=64,NO_CALLS,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4435:
	.cfi_startproc
	copy %r3,%r1
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4435:
	.size	_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE, .-_ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE
	.section	.text._ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3iEESt10shared_ptrIT_ERKT0_DpOT1_,"axG",@progbits,_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3iEESt10shared_ptrIT_ERKT0_DpOT1_,comdat
	.align 4
	.weak	_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3iEESt10shared_ptrIT_ERKT0_DpOT1_
	.type	_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3iEESt10shared_ptrIT_ERKT0_DpOT1_, @function
_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3iEESt10shared_ptrIT_ERKT0_DpOT1_:
	.PROC
	.CALLINFO FRAME=128,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=6
	.ENTRY
.LFB4436:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,128(%r30)
	stw %r6,8(%r3)
	stw %r5,12(%r3)
	stw %r4,16(%r3)
	.cfi_offset 3, 0
	.cfi_offset 6, 8
	.cfi_offset 5, 12
	.cfi_offset 4, 16
	copy %r28,%r4
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	stw %r25,0(%r28)
	ldo 0(%r3),%r28
	ldo -44(%r28),%r28
	stw %r24,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r5
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE,%r2
	nop
	copy %r28,%r6
	ldo 0(%r3),%r28
	ldo -44(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE,%r2
	nop
	copy %r28,%r23
	copy %r6,%r24
	copy %r5,%r25
	copy %r4,%r26
	bl _ZNSt10shared_ptrI6sphereEC1ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,%r2
	nop
	copy %r4,%r28
	copy %r4,%r28
	ldw -20(%r3),%r2
	ldw 8(%r3),%r6
	ldw 12(%r3),%r5
	ldw 16(%r3),%r4
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4436:
	.size	_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3iEESt10shared_ptrIT_ERKT0_DpOT1_, .-_ZSt15allocate_sharedI6sphereSaIS0_EJ4vec3iEESt10shared_ptrIT_ERKT0_DpOT1_
	.section	.text._ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE4seedEj,"axG",@progbits,_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE4seedEj,comdat
	.align 4
	.weak	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE4seedEj
	.type	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE4seedEj, @function
_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE4seedEj:
	.PROC
	.CALLINFO FRAME=64,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4491:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	stw %r25,0(%r28)
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZNSt8__detail5__modIjLj0ELj1ELj0EEET_S1_,%r2
	nop
	copy %r28,%r19
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	stw %r19,0(%r28)
	ldi 1,%r28
	stw %r28,8(%r3)
.L130:
	ldw 8(%r3),%r19
	ldi 623,%r28
	comb,<<,n %r28,%r19,.L129
	ldw 8(%r3),%r28
	ldo -1(%r28),%r19
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldwx,s %r19(%r28),%r28
	stw %r28,12(%r3)
	ldw 12(%r3),%r28
	extru %r28,1,2,%r28
	ldw 12(%r3),%r19
	xor %r19,%r28,%r28
	stw %r28,12(%r3)
	ldil L'1812430848,%r28
	ldo 2405(%r28),%r28
	stws %r28,-16(%sp)
	fldws -16(%sp),%fr22L
	fldws 12(%r3),%fr23L
	xmpyu %fr23L,%fr22L,%fr24
	fstds %fr24,-16(%sp)
	ldws -16(%sp),%r28
	ldws -12(%sp),%r29
	copy %r29,%r28
	stw %r28,12(%r3)
	ldw 8(%r3),%r26
	bl _ZNSt8__detail5__modIjLj624ELj1ELj0EEET_S1_,%r2
	nop
	copy %r28,%r19
	ldw 12(%r3),%r28
	addl %r28,%r19,%r28
	stw %r28,12(%r3)
	ldw 12(%r3),%r26
	bl _ZNSt8__detail5__modIjLj0ELj1ELj0EEET_S1_,%r2
	nop
	copy %r28,%r20
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r19
	ldw 8(%r3),%r28
	zdep %r28,29,30,%r28
	addl %r19,%r28,%r28
	stw %r20,0(%r28)
	ldw 8(%r3),%r28
	ldo 1(%r28),%r28
	stw %r28,8(%r3)
	b,n .L130
.L129:
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldo 0(%r28),%r28
	ldo 2496(%r28),%r28
	ldi 624,%r19
	stw %r19,0(%r28)
	nop
	ldw -20(%r3),%r2
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4491:
	.size	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE4seedEj, .-_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE4seedEj
	.section	.text._ZNSt8__detail8_AdaptorISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEdEC2ERS2_,"axG",@progbits,_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEdEC5ERS2_,comdat
	.align 4
	.weak	_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEdEC2ERS2_
	.type	_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEdEC2ERS2_, @function
_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEdEC2ERS2_:
	.PROC
	.CALLINFO FRAME=64,NO_CALLS,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4493:
	.cfi_startproc
	copy %r3,%r1
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	stw %r25,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldo 0(%r3),%r19
	ldo -40(%r19),%r19
	ldw 0(%r19),%r19
	stw %r19,0(%r28)
	nop
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4493:
	.size	_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEdEC2ERS2_, .-_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEdEC2ERS2_
	.weak	_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEdEC1ERS2_
	.set	_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEdEC1ERS2_,_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEdEC2ERS2_
	.section	.text._ZNSt8__detail8_AdaptorISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEdEclEv,"axG",@progbits,_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEdEclEv,comdat
	.align 4
	.weak	_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEdEclEv
	.type	_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEdEclEv, @function
_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEdEclEv:
	.PROC
	.CALLINFO FRAME=64,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4495:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldw 0(%r28),%r28
	copy %r28,%r26
	bl _ZSt18generate_canonicalIdLj53ESt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEET_RT1_,%r2
	nop
	fcpy,dbl %fr4,%fr22
	fcpy,dbl %fr22,%fr4
	ldw -20(%r3),%r2
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4495:
	.size	_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEdEclEv, .-_ZNSt8__detail8_AdaptorISt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEdEclEv
	.section	.text._ZNKSt25uniform_real_distributionIdE10param_type1bEv,"axG",@progbits,_ZNKSt25uniform_real_distributionIdE10param_type1bEv,comdat
	.align 4
	.weak	_ZNKSt25uniform_real_distributionIdE10param_type1bEv
	.type	_ZNKSt25uniform_real_distributionIdE10param_type1bEv, @function
_ZNKSt25uniform_real_distributionIdE10param_type1bEv:
	.PROC
	.CALLINFO FRAME=64,NO_CALLS,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4496:
	.cfi_startproc
	copy %r3,%r1
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	fldds 8(%r28),%fr22
	fcpy,dbl %fr22,%fr4
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4496:
	.size	_ZNKSt25uniform_real_distributionIdE10param_type1bEv, .-_ZNKSt25uniform_real_distributionIdE10param_type1bEv
	.section	.text._ZNKSt25uniform_real_distributionIdE10param_type1aEv,"axG",@progbits,_ZNKSt25uniform_real_distributionIdE10param_type1aEv,comdat
	.align 4
	.weak	_ZNKSt25uniform_real_distributionIdE10param_type1aEv
	.type	_ZNKSt25uniform_real_distributionIdE10param_type1aEv, @function
_ZNKSt25uniform_real_distributionIdE10param_type1aEv:
	.PROC
	.CALLINFO FRAME=64,NO_CALLS,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4497:
	.cfi_startproc
	copy %r3,%r1
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	fldds 0(%r28),%fr22
	fcpy,dbl %fr22,%fr4
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4497:
	.size	_ZNKSt25uniform_real_distributionIdE10param_type1aEv, .-_ZNKSt25uniform_real_distributionIdE10param_type1aEv
	.section	.text._ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2Ev,"axG",@progbits,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC5Ev,comdat
	.align 4
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2Ev
	.type	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2Ev, @function
_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2Ev:
	.PROC
	.CALLINFO FRAME=64,NO_CALLS,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4503:
	.cfi_startproc
	copy %r3,%r1
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	stw %r0,0(%r28)
	nop
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4503:
	.size	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2Ev, .-_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2Ev
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1Ev
	.set	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1Ev,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2Ev
	.section	.text._ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,"axG",@progbits,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,comdat
	.align 4
	.weak	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.type	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, @function
_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv:
	.PROC
	.CALLINFO FRAME=64,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4505:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	comib,=,n 0,%r28,.L141
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldw 0(%r28),%r28
	ldo 4(%r28),%r28
	ldw 0(%r28),%r19
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r26
	copy %r19,%r22
	bl $$dyncall,%r31
	copy %r31,%r2
.L141:
	nop
	ldw -20(%r3),%r2
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4505:
	.size	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, .-_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.section	.text._ZNSaISt10shared_ptrI8hittableEED2Ev,"axG",@progbits,_ZNSaISt10shared_ptrI8hittableEED5Ev,comdat
	.align 4
	.weak	_ZNSaISt10shared_ptrI8hittableEED2Ev
	.type	_ZNSaISt10shared_ptrI8hittableEED2Ev, @function
_ZNSaISt10shared_ptrI8hittableEED2Ev:
	.PROC
	.CALLINFO FRAME=64,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4507:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED2Ev,%r2
	nop
	nop
	ldw -20(%r3),%r2
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4507:
	.size	_ZNSaISt10shared_ptrI8hittableEED2Ev, .-_ZNSaISt10shared_ptrI8hittableEED2Ev
	.weak	_ZNSaISt10shared_ptrI8hittableEED1Ev
	.set	_ZNSaISt10shared_ptrI8hittableEED1Ev,_ZNSaISt10shared_ptrI8hittableEED2Ev
	.section	.text._ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE13_M_deallocateEPS2_j,"axG",@progbits,_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE13_M_deallocateEPS2_j,comdat
	.align 4
	.weak	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE13_M_deallocateEPS2_j
	.type	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE13_M_deallocateEPS2_j, @function
_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE13_M_deallocateEPS2_j:
	.PROC
	.CALLINFO FRAME=64,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4509:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	stw %r25,0(%r28)
	ldo 0(%r3),%r28
	ldo -44(%r28),%r28
	stw %r24,0(%r28)
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	ldw 0(%r28),%r28
	comib,=,n 0,%r28,.L145
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r19
	ldo 0(%r3),%r28
	ldo -44(%r28),%r28
	ldw 0(%r28),%r24
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	ldw 0(%r28),%r25
	copy %r19,%r26
	bl _ZNSt16allocator_traitsISaISt10shared_ptrI8hittableEEE10deallocateERS3_PS2_j,%r2
	nop
.L145:
	nop
	ldw -20(%r3),%r2
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4509:
	.size	_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE13_M_deallocateEPS2_j, .-_ZNSt12_Vector_baseISt10shared_ptrI8hittableESaIS2_EE13_M_deallocateEPS2_j
	.section	.text._ZSt8_DestroyIPSt10shared_ptrI8hittableEEvT_S4_,"axG",@progbits,_ZSt8_DestroyIPSt10shared_ptrI8hittableEEvT_S4_,comdat
	.align 4
	.weak	_ZSt8_DestroyIPSt10shared_ptrI8hittableEEvT_S4_
	.type	_ZSt8_DestroyIPSt10shared_ptrI8hittableEEvT_S4_, @function
_ZSt8_DestroyIPSt10shared_ptrI8hittableEEvT_S4_:
	.PROC
	.CALLINFO FRAME=64,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4510:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	stw %r25,0(%r28)
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	ldw 0(%r28),%r25
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZNSt12_Destroy_auxILb0EE9__destroyIPSt10shared_ptrI8hittableEEEvT_S6_,%r2
	nop
	nop
	ldw -20(%r3),%r2
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4510:
	.size	_ZSt8_DestroyIPSt10shared_ptrI8hittableEEvT_S4_, .-_ZSt8_DestroyIPSt10shared_ptrI8hittableEEvT_S4_
	.section	.text._ZN9__gnu_cxx13new_allocatorI6sphereEC2Ev,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorI6sphereEC5Ev,comdat
	.align 4
	.weak	_ZN9__gnu_cxx13new_allocatorI6sphereEC2Ev
	.type	_ZN9__gnu_cxx13new_allocatorI6sphereEC2Ev, @function
_ZN9__gnu_cxx13new_allocatorI6sphereEC2Ev:
	.PROC
	.CALLINFO FRAME=64,NO_CALLS,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4512:
	.cfi_startproc
	copy %r3,%r1
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	nop
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4512:
	.size	_ZN9__gnu_cxx13new_allocatorI6sphereEC2Ev, .-_ZN9__gnu_cxx13new_allocatorI6sphereEC2Ev
	.weak	_ZN9__gnu_cxx13new_allocatorI6sphereEC1Ev
	.set	_ZN9__gnu_cxx13new_allocatorI6sphereEC1Ev,_ZN9__gnu_cxx13new_allocatorI6sphereEC2Ev
	.section	.text._ZN9__gnu_cxx13new_allocatorI6sphereED2Ev,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorI6sphereED5Ev,comdat
	.align 4
	.weak	_ZN9__gnu_cxx13new_allocatorI6sphereED2Ev
	.type	_ZN9__gnu_cxx13new_allocatorI6sphereED2Ev, @function
_ZN9__gnu_cxx13new_allocatorI6sphereED2Ev:
	.PROC
	.CALLINFO FRAME=64,NO_CALLS,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4515:
	.cfi_startproc
	copy %r3,%r1
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	nop
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4515:
	.size	_ZN9__gnu_cxx13new_allocatorI6sphereED2Ev, .-_ZN9__gnu_cxx13new_allocatorI6sphereED2Ev
	.weak	_ZN9__gnu_cxx13new_allocatorI6sphereED1Ev
	.set	_ZN9__gnu_cxx13new_allocatorI6sphereED1Ev,_ZN9__gnu_cxx13new_allocatorI6sphereED2Ev
	.section	.text._ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,"axG",@progbits,_ZNSt10shared_ptrI6sphereEC5ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,comdat
	.align 4
	.weak	_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.type	_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_, @function
_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_:
	.PROC
	.CALLINFO FRAME=64,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=5
	.ENTRY
.LFB4518:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	stw %r5,8(%r3)
	stw %r4,12(%r3)
	.cfi_offset 3, 0
	.cfi_offset 5, 8
	.cfi_offset 4, 12
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	stw %r25,0(%r28)
	ldo 0(%r3),%r28
	ldo -44(%r28),%r28
	stw %r24,0(%r28)
	ldo 0(%r3),%r28
	ldo -48(%r28),%r28
	stw %r23,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r4
	ldo 0(%r3),%r28
	ldo -44(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE,%r2
	nop
	copy %r28,%r5
	ldo 0(%r3),%r28
	ldo -48(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE,%r2
	nop
	copy %r28,%r23
	copy %r5,%r24
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	ldw 0(%r28),%r25
	copy %r4,%r26
	bl _ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,%r2
	nop
	nop
	ldw -20(%r3),%r2
	ldw 8(%r3),%r5
	ldw 12(%r3),%r4
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4518:
	.size	_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_, .-_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.weak	_ZNSt10shared_ptrI6sphereEC1ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.set	_ZNSt10shared_ptrI6sphereEC1ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.section	.text._ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EE7_M_swapERS2_,"axG",@progbits,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EE7_M_swapERS2_,comdat
	.align 4
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EE7_M_swapERS2_
	.type	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EE7_M_swapERS2_, @function
_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EE7_M_swapERS2_:
	.PROC
	.CALLINFO FRAME=64,NO_CALLS,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4523:
	.cfi_startproc
	copy %r3,%r1
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	stw %r25,0(%r28)
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	ldw 0(%r28),%r28
	ldw 0(%r28),%r28
	stw %r28,8(%r3)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldw 0(%r28),%r19
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	ldw 0(%r28),%r28
	stw %r19,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldw 8(%r3),%r19
	stw %r19,0(%r28)
	nop
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4523:
	.size	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EE7_M_swapERS2_, .-_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EE7_M_swapERS2_
	.section	.text._ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,"axG",@progbits,_ZNSt10shared_ptrI6sphereEC5ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,comdat
	.align 4
	.weak	_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.type	_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_, @function
_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_:
	.PROC
	.CALLINFO FRAME=64,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=5
	.ENTRY
.LFB4525:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	stw %r5,8(%r3)
	stw %r4,12(%r3)
	.cfi_offset 3, 0
	.cfi_offset 5, 8
	.cfi_offset 4, 12
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	stw %r25,0(%r28)
	ldo 0(%r3),%r28
	ldo -44(%r28),%r28
	stw %r24,0(%r28)
	ldo 0(%r3),%r28
	ldo -48(%r28),%r28
	stw %r23,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r4
	ldo 0(%r3),%r28
	ldo -44(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE,%r2
	nop
	copy %r28,%r5
	ldo 0(%r3),%r28
	ldo -48(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE,%r2
	nop
	copy %r28,%r23
	copy %r5,%r24
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	ldw 0(%r28),%r25
	copy %r4,%r26
	bl _ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,%r2
	nop
	nop
	ldw -20(%r3),%r2
	ldw 8(%r3),%r5
	ldw 12(%r3),%r4
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4525:
	.size	_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_, .-_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.weak	_ZNSt10shared_ptrI6sphereEC1ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.set	_ZNSt10shared_ptrI6sphereEC1ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,_ZNSt10shared_ptrI6sphereEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.section	.text._ZNSt8__detail5__modIjLj0ELj1ELj0EEET_S1_,"axG",@progbits,_ZNSt8__detail5__modIjLj0ELj1ELj0EEET_S1_,comdat
	.align 4
	.weak	_ZNSt8__detail5__modIjLj0ELj1ELj0EEET_S1_
	.type	_ZNSt8__detail5__modIjLj0ELj1ELj0EEET_S1_, @function
_ZNSt8__detail5__modIjLj0ELj1ELj0EEET_S1_:
	.PROC
	.CALLINFO FRAME=64,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4564:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZNSt8__detail4_ModIjLj0ELj1ELj0ELb1ELb0EE6__calcEj,%r2
	nop
	ldw -20(%r3),%r2
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4564:
	.size	_ZNSt8__detail5__modIjLj0ELj1ELj0EEET_S1_, .-_ZNSt8__detail5__modIjLj0ELj1ELj0EEET_S1_
	.section	.text._ZNSt8__detail5__modIjLj624ELj1ELj0EEET_S1_,"axG",@progbits,_ZNSt8__detail5__modIjLj624ELj1ELj0EEET_S1_,comdat
	.align 4
	.weak	_ZNSt8__detail5__modIjLj624ELj1ELj0EEET_S1_
	.type	_ZNSt8__detail5__modIjLj624ELj1ELj0EEET_S1_, @function
_ZNSt8__detail5__modIjLj624ELj1ELj0EEET_S1_:
	.PROC
	.CALLINFO FRAME=64,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4565:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZNSt8__detail4_ModIjLj624ELj1ELj0ELb1ELb1EE6__calcEj,%r2
	nop
	ldw -20(%r3),%r2
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4565:
	.size	_ZNSt8__detail5__modIjLj624ELj1ELj0EEET_S1_, .-_ZNSt8__detail5__modIjLj624ELj1ELj0EEET_S1_
	.section	.text._ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE3minEv,"axG",@progbits,_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE3minEv,comdat
	.align 4
	.weak	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE3minEv
	.type	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE3minEv, @function
_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE3minEv:
	.PROC
	.CALLINFO FRAME=64,NO_CALLS,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4569:
	.cfi_startproc
	copy %r3,%r1
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldi 0,%r28
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4569:
	.size	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE3minEv, .-_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE3minEv
	.section	.rodata
	.align 8
.LC20:
	.word	1106247680
	.word	0
	.align 8
.LC21:
	.word	1072693248
	.word	0
	.align 8
.LC22:
	.word	1072693247
	.word	4294967295
	.section	.text._ZSt18generate_canonicalIdLj53ESt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEET_RT1_,"axG",@progbits,_ZSt18generate_canonicalIdLj53ESt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEET_RT1_,comdat
	.align 4
	.weak	_ZSt18generate_canonicalIdLj53ESt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEET_RT1_
	.type	_ZSt18generate_canonicalIdLj53ESt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEET_RT1_, @function
_ZSt18generate_canonicalIdLj53ESt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEET_RT1_:
	.PROC
	.CALLINFO FRAME=128,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=6
	.ENTRY
.LFB4566:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,128(%r30)
	stw %r6,56(%r3)
	stw %r5,60(%r3)
	stw %r4,64(%r3)
	.cfi_offset 3, 0
	.cfi_offset 6, 56
	.cfi_offset 5, 60
	.cfi_offset 4, 64
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 8(%r3),%r28
	ldo 28(%r28),%r28
	ldi 53,%r19
	stw %r19,0(%r28)
	ldo 40(%r3),%r28
	ldil LR'.LC20,%r19
	ldo RR'.LC20(%r19),%r19
	fldds 0(%r19),%fr22
	fstds %fr22,0(%r28)
	ldo 8(%r3),%r28
	ldo 40(%r28),%r28
	ldi 32,%r19
	stw %r19,0(%r28)
	ldo 8(%r3),%r28
	ldo 44(%r28),%r28
	ldi 2,%r19
	stw %r19,0(%r28)
	fcpy,dbl %fr0,%fr22
	ldo 32(%r3),%r28
	fstds %fr22,-16(%r28)
	ldo 40(%r3),%r28
	ldil LR'.LC21,%r19
	ldo RR'.LC21(%r19),%r19
	fldds 0(%r19),%fr22
	fstds %fr22,-16(%r28)
	ldo 8(%r3),%r28
	ldo 24(%r28),%r28
	ldi 2,%r19
	stw %r19,0(%r28)
.L160:
	ldo 8(%r3),%r28
	ldo 24(%r28),%r28
	ldw 0(%r28),%r28
	comib,=,n 0,%r28,.L159
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEclEv,%r2
	nop
	copy %r28,%r4
	bl _ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE3minEv,%r2
	nop
	sub %r4,%r28,%r28
	copy %r28,%r6
	ldi 0,%r5
	stws %r5,-16(%sp)
	stws %r6,-12(%sp)
	fldds -16(%sp),%fr22
	fcnvxf,dbl,dbl %fr22,%fr23
	ldo 40(%r3),%r28
	fldds -16(%r28),%fr22
	fmpy,dbl %fr23,%fr22,%fr22
	ldo 32(%r3),%r28
	fldds -16(%r28),%fr23
	fadd,dbl %fr23,%fr22,%fr22
	ldo 32(%r3),%r28
	fstds %fr22,-16(%r28)
	ldo 40(%r3),%r28
	ldo 40(%r3),%r19
	fldds -16(%r19),%fr23
	ldil LR'.LC20,%r19
	ldo RR'.LC20(%r19),%r19
	fldds 0(%r19),%fr22
	fmpy,dbl %fr23,%fr22,%fr22
	fstds %fr22,-16(%r28)
	ldo 8(%r3),%r28
	ldo 24(%r28),%r28
	ldo 8(%r3),%r19
	ldo 24(%r19),%r19
	ldw 0(%r19),%r19
	ldo -1(%r19),%r19
	stw %r19,0(%r28)
	b,n .L160
.L159:
	ldo 40(%r3),%r28
	ldo 32(%r3),%r19
	fldds -16(%r19),%fr23
	fldds -16(%r28),%fr22
	fdiv,dbl %fr23,%fr22,%fr22
	fstds %fr22,8(%r3)
	ldi 1,%r28
	fldds 8(%r3),%fr23
	ldil LR'.LC21,%r19
	ldo RR'.LC21(%r19),%r19
	fldds 0(%r19),%fr22
	fcmp,dbl,!>= %fr23,%fr22
	ftest
	b,n .L161
	ldi 0,%r28
.L161:
	extru %r28,31,8,%r28
	comib,=,n 0,%r28,.L162
	ldil LR'.LC22,%r28
	ldo RR'.LC22(%r28),%r28
	fldds 0(%r28),%fr22
	fstds %fr22,8(%r3)
.L162:
	fldds 8(%r3),%fr22
	fcpy,dbl %fr22,%fr4
	ldw -20(%r3),%r2
	ldw 56(%r3),%r6
	ldw 60(%r3),%r5
	ldw 64(%r3),%r4
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4566:
	.size	_ZSt18generate_canonicalIdLj53ESt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEET_RT1_, .-_ZSt18generate_canonicalIdLj53ESt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEET_RT1_
	.section	.text._ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 4
	.weak	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev
	.type	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev:
	.PROC
	.CALLINFO FRAME=64,NO_CALLS,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4576:
	.cfi_startproc
	copy %r3,%r1
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	addil LR'_ZTVSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE-$global$,%r27
	copy %r1,%r28
	ldo RR'_ZTVSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE-$global$+8(%r28),%r19
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	stw %r19,0(%r28)
	nop
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4576:
	.size	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED1Ev
	.set	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED1Ev,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED0Ev,"axG",@progbits,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 4
	.weak	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED0Ev
	.type	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED0Ev, @function
_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED0Ev:
	.PROC
	.CALLINFO FRAME=64,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4578:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED1Ev,%r2
	nop
	ldi 12,%r25
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZdlPvj,%r2
	nop
	ldw -20(%r3),%r2
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4578:
	.size	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED0Ev, .-_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED0Ev
	.section	.text._ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED2Ev,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED5Ev,comdat
	.align 4
	.weak	_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED2Ev
	.type	_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED2Ev, @function
_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED2Ev:
	.PROC
	.CALLINFO FRAME=64,NO_CALLS,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4580:
	.cfi_startproc
	copy %r3,%r1
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	nop
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4580:
	.size	_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED2Ev, .-_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED2Ev
	.weak	_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED1Ev
	.set	_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED1Ev,_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEED2Ev
	.section	.text._ZNSt16allocator_traitsISaISt10shared_ptrI8hittableEEE10deallocateERS3_PS2_j,"axG",@progbits,_ZNSt16allocator_traitsISaISt10shared_ptrI8hittableEEE10deallocateERS3_PS2_j,comdat
	.align 4
	.weak	_ZNSt16allocator_traitsISaISt10shared_ptrI8hittableEEE10deallocateERS3_PS2_j
	.type	_ZNSt16allocator_traitsISaISt10shared_ptrI8hittableEEE10deallocateERS3_PS2_j, @function
_ZNSt16allocator_traitsISaISt10shared_ptrI8hittableEEE10deallocateERS3_PS2_j:
	.PROC
	.CALLINFO FRAME=64,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4582:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	stw %r25,0(%r28)
	ldo 0(%r3),%r28
	ldo -44(%r28),%r28
	stw %r24,0(%r28)
	ldo 0(%r3),%r28
	ldo -44(%r28),%r28
	ldw 0(%r28),%r24
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	ldw 0(%r28),%r25
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEE10deallocateEPS3_j,%r2
	nop
	nop
	ldw -20(%r3),%r2
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4582:
	.size	_ZNSt16allocator_traitsISaISt10shared_ptrI8hittableEEE10deallocateERS3_PS2_j, .-_ZNSt16allocator_traitsISaISt10shared_ptrI8hittableEEE10deallocateERS3_PS2_j
	.section	.text._ZNSt12_Destroy_auxILb0EE9__destroyIPSt10shared_ptrI8hittableEEEvT_S6_,"axG",@progbits,_ZNSt12_Destroy_auxILb0EE9__destroyIPSt10shared_ptrI8hittableEEEvT_S6_,comdat
	.align 4
	.weak	_ZNSt12_Destroy_auxILb0EE9__destroyIPSt10shared_ptrI8hittableEEEvT_S6_
	.type	_ZNSt12_Destroy_auxILb0EE9__destroyIPSt10shared_ptrI8hittableEEEvT_S6_, @function
_ZNSt12_Destroy_auxILb0EE9__destroyIPSt10shared_ptrI8hittableEEEvT_S6_:
	.PROC
	.CALLINFO FRAME=64,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4583:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	stw %r25,0(%r28)
.L170:
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r19
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	ldw 0(%r28),%r28
	comb,=,n %r28,%r19,.L171
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZSt11__addressofISt10shared_ptrI8hittableEEPT_RS3_,%r2
	nop
	copy %r28,%r26
	bl _ZSt8_DestroyISt10shared_ptrI8hittableEEvPT_,%r2
	nop
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldo 8(%r28),%r19
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r19,0(%r28)
	b,n .L170
.L171:
	nop
	ldw -20(%r3),%r2
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4583:
	.size	_ZNSt12_Destroy_auxILb0EE9__destroyIPSt10shared_ptrI8hittableEEEvT_S6_, .-_ZNSt12_Destroy_auxILb0EE9__destroyIPSt10shared_ptrI8hittableEEEvT_S6_
	.section	.text._ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,"axG",@progbits,_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC5ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,comdat
	.align 4
	.weak	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.type	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_, @function
_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_:
	.PROC
	.CALLINFO FRAME=128,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=6
	.ENTRY
.LFB4585:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,128(%r30)
	stw %r6,8(%r3)
	stw %r5,12(%r3)
	stw %r4,16(%r3)
	.cfi_offset 3, 0
	.cfi_offset 6, 8
	.cfi_offset 5, 12
	.cfi_offset 4, 16
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	stw %r25,0(%r28)
	ldo 0(%r3),%r28
	ldo -44(%r28),%r28
	stw %r24,0(%r28)
	ldo 0(%r3),%r28
	ldo -48(%r28),%r28
	stw %r23,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	stw %r0,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldo 4(%r28),%r4
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r5
	ldo 0(%r3),%r28
	ldo -44(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE,%r2
	nop
	copy %r28,%r6
	ldo 0(%r3),%r28
	ldo -48(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE,%r2
	nop
	copy %r28,%r19
	ldo -32(%r30),%r28
	ldo -20(%r28),%r28
	stw %r19,0(%r28)
	copy %r6,%r23
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	ldw 0(%r28),%r24
	copy %r5,%r25
	copy %r4,%r26
	bl _ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_,%r2
	nop
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldw 0(%r28),%r28
	copy %r28,%r25
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EE31_M_enable_shared_from_this_withIS0_S0_EENSt9enable_ifIXntsrNS3_15__has_esft_baseIT0_vEE5valueEvE4typeEPT_,%r2
	nop
	nop
	ldw -20(%r3),%r2
	ldw 8(%r3),%r6
	ldw 12(%r3),%r5
	ldw 16(%r3),%r4
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4585:
	.size	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_, .-_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.weak	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC1ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.set	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC1ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3dEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.section	.text._ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,"axG",@progbits,_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC5ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,comdat
	.align 4
	.weak	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.type	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_, @function
_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_:
	.PROC
	.CALLINFO FRAME=128,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=6
	.ENTRY
.LFB4588:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,128(%r30)
	stw %r6,8(%r3)
	stw %r5,12(%r3)
	stw %r4,16(%r3)
	.cfi_offset 3, 0
	.cfi_offset 6, 8
	.cfi_offset 5, 12
	.cfi_offset 4, 16
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	stw %r25,0(%r28)
	ldo 0(%r3),%r28
	ldo -44(%r28),%r28
	stw %r24,0(%r28)
	ldo 0(%r3),%r28
	ldo -48(%r28),%r28
	stw %r23,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	stw %r0,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldo 4(%r28),%r4
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r5
	ldo 0(%r3),%r28
	ldo -44(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE,%r2
	nop
	copy %r28,%r6
	ldo 0(%r3),%r28
	ldo -48(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE,%r2
	nop
	copy %r28,%r19
	ldo -32(%r30),%r28
	ldo -20(%r28),%r28
	stw %r19,0(%r28)
	copy %r6,%r23
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	ldw 0(%r28),%r24
	copy %r5,%r25
	copy %r4,%r26
	bl _ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_,%r2
	nop
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldw 0(%r28),%r28
	copy %r28,%r25
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EE31_M_enable_shared_from_this_withIS0_S0_EENSt9enable_ifIXntsrNS3_15__has_esft_baseIT0_vEE5valueEvE4typeEPT_,%r2
	nop
	nop
	ldw -20(%r3),%r2
	ldw 8(%r3),%r6
	ldw 12(%r3),%r5
	ldw 16(%r3),%r4
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4588:
	.size	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_, .-_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.weak	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC1ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.set	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC1ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_,_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EEC2ISaIS0_EJ4vec3iEEESt20_Sp_alloc_shared_tagIT_EDpOT0_
	.section	.text._ZNSt8__detail4_ModIjLj0ELj1ELj0ELb1ELb0EE6__calcEj,"axG",@progbits,_ZNSt8__detail4_ModIjLj0ELj1ELj0ELb1ELb0EE6__calcEj,comdat
	.align 4
	.weak	_ZNSt8__detail4_ModIjLj0ELj1ELj0ELb1ELb0EE6__calcEj
	.type	_ZNSt8__detail4_ModIjLj0ELj1ELj0ELb1ELb0EE6__calcEj, @function
_ZNSt8__detail4_ModIjLj0ELj1ELj0ELb1ELb0EE6__calcEj:
	.PROC
	.CALLINFO FRAME=64,NO_CALLS,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4618:
	.cfi_startproc
	copy %r3,%r1
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	stw %r28,8(%r3)
	ldw 8(%r3),%r28
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4618:
	.size	_ZNSt8__detail4_ModIjLj0ELj1ELj0ELb1ELb0EE6__calcEj, .-_ZNSt8__detail4_ModIjLj0ELj1ELj0ELb1ELb0EE6__calcEj
	.section	.text._ZNSt8__detail4_ModIjLj624ELj1ELj0ELb1ELb1EE6__calcEj,"axG",@progbits,_ZNSt8__detail4_ModIjLj624ELj1ELj0ELb1ELb1EE6__calcEj,comdat
	.align 4
	.weak	_ZNSt8__detail4_ModIjLj624ELj1ELj0ELb1ELb1EE6__calcEj
	.type	_ZNSt8__detail4_ModIjLj624ELj1ELj0ELb1ELb1EE6__calcEj, @function
_ZNSt8__detail4_ModIjLj624ELj1ELj0ELb1ELb1EE6__calcEj:
	.PROC
	.CALLINFO FRAME=64,NO_CALLS,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4619:
	.cfi_startproc
	copy %r3,%r1
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	stw %r28,8(%r3)
	ldw 8(%r3),%r20
	extru %r20,27,28,%r28
	stws %r28,-16(%sp)
	fldws -16(%sp),%fr23L
	ldil L'440516608,%r28
	ldo -7141(%r28),%r28
	stws %r28,-16(%sp)
	fldws -16(%sp),%fr22L
	xmpyu %fr23L,%fr22L,%fr24
	fstds %fr24,-16(%sp)
	ldws -16(%sp),%r28
	ldws -12(%sp),%r29
	extru %r28,29,30,%r19
	copy %r19,%r28
	zdep %r28,29,30,%r28
	addl %r28,%r19,%r28
	zdep %r28,28,29,%r28
	sub %r28,%r19,%r28
	zdep %r28,27,28,%r28
	sub %r20,%r28,%r28
	stw %r28,8(%r3)
	ldw 8(%r3),%r28
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4619:
	.size	_ZNSt8__detail4_ModIjLj624ELj1ELj0ELb1ELb1EE6__calcEj, .-_ZNSt8__detail4_ModIjLj624ELj1ELj0ELb1ELb1EE6__calcEj
	.section	.text._ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEclEv,"axG",@progbits,_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEclEv,comdat
	.align 4
	.weak	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEclEv
	.type	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEclEv, @function
_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEclEv:
	.PROC
	.CALLINFO FRAME=64,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4620:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldo 0(%r28),%r28
	ldo 2496(%r28),%r28
	ldw 0(%r28),%r19
	ldi 623,%r28
	comb,>>=,n %r28,%r19,.L179
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE11_M_gen_randEv,%r2
	nop
.L179:
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldo 0(%r28),%r28
	ldo 2496(%r28),%r28
	ldw 0(%r28),%r28
	ldo 1(%r28),%r20
	ldo 0(%r3),%r19
	ldo -36(%r19),%r19
	ldw 0(%r19),%r19
	ldo 0(%r19),%r19
	ldo 2496(%r19),%r19
	stw %r20,0(%r19)
	ldo 0(%r3),%r19
	ldo -36(%r19),%r19
	ldw 0(%r19),%r19
	ldwx,s %r28(%r19),%r28
	stw %r28,8(%r3)
	ldw 8(%r3),%r28
	extru %r28,20,21,%r28
	ldw 8(%r3),%r19
	xor %r19,%r28,%r28
	stw %r28,8(%r3)
	ldw 8(%r3),%r28
	zdep %r28,24,25,%r19
	ldil L'-1658044416,%r28
	ldo 5760(%r28),%r28
	and %r19,%r28,%r28
	ldw 8(%r3),%r19
	xor %r19,%r28,%r28
	stw %r28,8(%r3)
	ldw 8(%r3),%r28
	zdep %r28,16,17,%r19
	ldil L'-272236544,%r28
	and %r19,%r28,%r28
	ldw 8(%r3),%r19
	xor %r19,%r28,%r28
	stw %r28,8(%r3)
	ldw 8(%r3),%r28
	extru %r28,13,14,%r28
	ldw 8(%r3),%r19
	xor %r19,%r28,%r28
	stw %r28,8(%r3)
	ldw 8(%r3),%r28
	ldw -20(%r3),%r2
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4620:
	.size	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEclEv, .-_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EEclEv
	.section	.text._ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEE10deallocateEPS3_j,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEE10deallocateEPS3_j,comdat
	.align 4
	.weak	_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEE10deallocateEPS3_j
	.type	_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEE10deallocateEPS3_j, @function
_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEE10deallocateEPS3_j:
	.PROC
	.CALLINFO FRAME=64,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4622:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	stw %r25,0(%r28)
	ldo 0(%r3),%r28
	ldo -44(%r28),%r28
	stw %r24,0(%r28)
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZdlPv,%r2
	nop
	nop
	ldw -20(%r3),%r2
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4622:
	.size	_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEE10deallocateEPS3_j, .-_ZN9__gnu_cxx13new_allocatorISt10shared_ptrI8hittableEE10deallocateEPS3_j
	.section	.text._ZSt11__addressofISt10shared_ptrI8hittableEEPT_RS3_,"axG",@progbits,_ZSt11__addressofISt10shared_ptrI8hittableEEPT_RS3_,comdat
	.align 4
	.weak	_ZSt11__addressofISt10shared_ptrI8hittableEEPT_RS3_
	.type	_ZSt11__addressofISt10shared_ptrI8hittableEEPT_RS3_, @function
_ZSt11__addressofISt10shared_ptrI8hittableEEPT_RS3_:
	.PROC
	.CALLINFO FRAME=64,NO_CALLS,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4623:
	.cfi_startproc
	copy %r3,%r1
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4623:
	.size	_ZSt11__addressofISt10shared_ptrI8hittableEEPT_RS3_, .-_ZSt11__addressofISt10shared_ptrI8hittableEEPT_RS3_
	.section	.text._ZSt8_DestroyISt10shared_ptrI8hittableEEvPT_,"axG",@progbits,_ZSt8_DestroyISt10shared_ptrI8hittableEEvPT_,comdat
	.align 4
	.weak	_ZSt8_DestroyISt10shared_ptrI8hittableEEvPT_
	.type	_ZSt8_DestroyISt10shared_ptrI8hittableEEvPT_, @function
_ZSt8_DestroyISt10shared_ptrI8hittableEEvPT_:
	.PROC
	.CALLINFO FRAME=64,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4624:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZNSt10shared_ptrI8hittableED1Ev,%r2
	nop
	nop
	ldw -20(%r3),%r2
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4624:
	.size	_ZSt8_DestroyISt10shared_ptrI8hittableEEvPT_, .-_ZSt8_DestroyISt10shared_ptrI8hittableEEvPT_
	.section	.text._ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_,"axG",@progbits,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC5I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_,comdat
	.align 4
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_
	.type	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_, @function
_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_:
	.PROC
	.CALLINFO FRAME=128,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=8
	.ENTRY
.LFB4626:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA4626
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,128(%r30)
	stw %r8,28(%r3)
	stw %r7,32(%r3)
	stw %r6,36(%r3)
	stw %r5,40(%r3)
	stw %r4,44(%r3)
	.cfi_offset 3, 0
	.cfi_offset 8, 28
	.cfi_offset 7, 32
	.cfi_offset 6, 36
	.cfi_offset 5, 40
	.cfi_offset 4, 44
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	stw %r25,0(%r28)
	ldo 0(%r3),%r28
	ldo -44(%r28),%r28
	stw %r24,0(%r28)
	ldo 0(%r3),%r28
	ldo -48(%r28),%r28
	stw %r23,0(%r28)
	ldo 0(%r3),%r28
	ldo -44(%r28),%r28
	ldw 0(%r28),%r19
	ldo 17(%r3),%r28
	copy %r19,%r25
	copy %r28,%r26
	bl _ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC1IS0_EERKSaIT_E,%r2
	nop
	ldo 17(%r3),%r19
	ldo 20(%r3),%r28
	copy %r19,%r26
.LEHB20:
	bl _ZSt18__allocate_guardedISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEESt15__allocated_ptrIT_ERS8_,%r2
	nop
.LEHE20:
	ldo 20(%r3),%r28
	copy %r28,%r26
	bl _ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE3getEv,%r2
	nop
	stw %r28,8(%r3)
	ldo 0(%r3),%r28
	ldo -44(%r28),%r28
	ldw 0(%r28),%r19
	ldo 16(%r3),%r28
	copy %r19,%r25
	copy %r28,%r26
	bl _ZNSaI6sphereEC1ERKS0_,%r2
	nop
	ldo 16(%r3),%r6
	ldo 0(%r3),%r28
	ldo -48(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE,%r2
	nop
	copy %r28,%r7
	ldo -32(%r3),%r28
	ldo -20(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE,%r2
	nop
	copy %r28,%r8
	ldw 8(%r3),%r5
	copy %r5,%r25
	ldi 64,%r26
	bl _ZnwjPv,%r2
	nop
	copy %r28,%r4
	copy %r8,%r23
	copy %r7,%r24
	copy %r6,%r25
	copy %r4,%r26
.LEHB21:
	bl _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC1IJ4vec3dEEES1_DpOT_,%r2
	nop
.LEHE21:
	stw %r4,12(%r3)
	ldo 16(%r3),%r28
	copy %r28,%r26
	bl _ZNSaI6sphereED1Ev,%r2
	nop
	ldo 20(%r3),%r28
	ldi 0,%r25
	copy %r28,%r26
	bl _ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEaSEDn,%r2
	nop
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldw 12(%r3),%r19
	stw %r19,0(%r28)
	ldw 12(%r3),%r26
	bl _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv,%r2
	nop
	copy %r28,%r19
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	ldw 0(%r28),%r28
	stw %r19,0(%r28)
	ldo 20(%r3),%r28
	copy %r28,%r26
	bl _ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED1Ev,%r2
	nop
	ldo 17(%r3),%r28
	copy %r28,%r26
	bl _ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED1Ev,%r2
	nop
	b,n .L190
.L189:
	copy %r20,%r6
	copy %r5,%r25
	copy %r4,%r26
	bl _ZdlPvS_,%r2
	nop
	copy %r6,%r4
	ldo 16(%r3),%r28
	copy %r28,%r26
	bl _ZNSaI6sphereED1Ev,%r2
	nop
	ldo 20(%r3),%r28
	copy %r28,%r26
	bl _ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED1Ev,%r2
	nop
	b,n .L187
.L188:
	copy %r20,%r4
.L187:
	ldo 17(%r3),%r28
	copy %r28,%r26
	bl _ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED1Ev,%r2
	nop
	copy %r4,%r28
	copy %r28,%r26
.LEHB22:
	bl _Unwind_Resume,%r2
	nop
.LEHE22:
.L190:
	ldw -20(%r3),%r2
	ldw 28(%r3),%r8
	ldw 32(%r3),%r7
	ldw 36(%r3),%r6
	ldw 40(%r3),%r5
	ldw 44(%r3),%r4
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4626:
	.section	.gcc_except_table
.LLSDA4626:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4626-.LLSDACSB4626
.LLSDACSB4626:
	.uleb128 .LEHB20-.LFB4626
	.uleb128 .LEHE20-.LEHB20
	.uleb128 .L188-.LFB4626
	.uleb128 0
	.uleb128 .LEHB21-.LFB4626
	.uleb128 .LEHE21-.LEHB21
	.uleb128 .L189-.LFB4626
	.uleb128 0
	.uleb128 .LEHB22-.LFB4626
	.uleb128 .LEHE22-.LEHB22
	.uleb128 0
	.uleb128 0
.LLSDACSE4626:
	.section	.text._ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_,"axG",@progbits,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC5I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_,comdat
	.size	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_, .-_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_
	.set	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3dEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_
	.section	.text._ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EE31_M_enable_shared_from_this_withIS0_S0_EENSt9enable_ifIXntsrNS3_15__has_esft_baseIT0_vEE5valueEvE4typeEPT_,"axG",@progbits,_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EE31_M_enable_shared_from_this_withIS0_S0_EENSt9enable_ifIXntsrNS3_15__has_esft_baseIT0_vEE5valueEvE4typeEPT_,comdat
	.align 4
	.weak	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EE31_M_enable_shared_from_this_withIS0_S0_EENSt9enable_ifIXntsrNS3_15__has_esft_baseIT0_vEE5valueEvE4typeEPT_
	.type	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EE31_M_enable_shared_from_this_withIS0_S0_EENSt9enable_ifIXntsrNS3_15__has_esft_baseIT0_vEE5valueEvE4typeEPT_, @function
_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EE31_M_enable_shared_from_this_withIS0_S0_EENSt9enable_ifIXntsrNS3_15__has_esft_baseIT0_vEE5valueEvE4typeEPT_:
	.PROC
	.CALLINFO FRAME=64,NO_CALLS,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4628:
	.cfi_startproc
	copy %r3,%r1
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	stw %r25,0(%r28)
	nop
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4628:
	.size	_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EE31_M_enable_shared_from_this_withIS0_S0_EENSt9enable_ifIXntsrNS3_15__has_esft_baseIT0_vEE5valueEvE4typeEPT_, .-_ZNSt12__shared_ptrI6sphereLN9__gnu_cxx12_Lock_policyE2EE31_M_enable_shared_from_this_withIS0_S0_EENSt9enable_ifIXntsrNS3_15__has_esft_baseIT0_vEE5valueEvE4typeEPT_
	.section	.text._ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_,"axG",@progbits,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC5I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_,comdat
	.align 4
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_
	.type	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_, @function
_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_:
	.PROC
	.CALLINFO FRAME=128,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=8
	.ENTRY
.LFB4630:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA4630
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,128(%r30)
	stw %r8,28(%r3)
	stw %r7,32(%r3)
	stw %r6,36(%r3)
	stw %r5,40(%r3)
	stw %r4,44(%r3)
	.cfi_offset 3, 0
	.cfi_offset 8, 28
	.cfi_offset 7, 32
	.cfi_offset 6, 36
	.cfi_offset 5, 40
	.cfi_offset 4, 44
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	stw %r25,0(%r28)
	ldo 0(%r3),%r28
	ldo -44(%r28),%r28
	stw %r24,0(%r28)
	ldo 0(%r3),%r28
	ldo -48(%r28),%r28
	stw %r23,0(%r28)
	ldo 0(%r3),%r28
	ldo -44(%r28),%r28
	ldw 0(%r28),%r19
	ldo 17(%r3),%r28
	copy %r19,%r25
	copy %r28,%r26
	bl _ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC1IS0_EERKSaIT_E,%r2
	nop
	ldo 17(%r3),%r19
	ldo 20(%r3),%r28
	copy %r19,%r26
.LEHB23:
	bl _ZSt18__allocate_guardedISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEESt15__allocated_ptrIT_ERS8_,%r2
	nop
.LEHE23:
	ldo 20(%r3),%r28
	copy %r28,%r26
	bl _ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE3getEv,%r2
	nop
	stw %r28,8(%r3)
	ldo 0(%r3),%r28
	ldo -44(%r28),%r28
	ldw 0(%r28),%r19
	ldo 16(%r3),%r28
	copy %r19,%r25
	copy %r28,%r26
	bl _ZNSaI6sphereEC1ERKS0_,%r2
	nop
	ldo 16(%r3),%r6
	ldo 0(%r3),%r28
	ldo -48(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE,%r2
	nop
	copy %r28,%r7
	ldo -32(%r3),%r28
	ldo -20(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE,%r2
	nop
	copy %r28,%r8
	ldw 8(%r3),%r5
	copy %r5,%r25
	ldi 64,%r26
	bl _ZnwjPv,%r2
	nop
	copy %r28,%r4
	copy %r8,%r23
	copy %r7,%r24
	copy %r6,%r25
	copy %r4,%r26
.LEHB24:
	bl _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC1IJ4vec3iEEES1_DpOT_,%r2
	nop
.LEHE24:
	stw %r4,12(%r3)
	ldo 16(%r3),%r28
	copy %r28,%r26
	bl _ZNSaI6sphereED1Ev,%r2
	nop
	ldo 20(%r3),%r28
	ldi 0,%r25
	copy %r28,%r26
	bl _ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEaSEDn,%r2
	nop
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldw 12(%r3),%r19
	stw %r19,0(%r28)
	ldw 12(%r3),%r26
	bl _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv,%r2
	nop
	copy %r28,%r19
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	ldw 0(%r28),%r28
	stw %r19,0(%r28)
	ldo 20(%r3),%r28
	copy %r28,%r26
	bl _ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED1Ev,%r2
	nop
	ldo 17(%r3),%r28
	copy %r28,%r26
	bl _ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED1Ev,%r2
	nop
	b,n .L197
.L196:
	copy %r20,%r6
	copy %r5,%r25
	copy %r4,%r26
	bl _ZdlPvS_,%r2
	nop
	copy %r6,%r4
	ldo 16(%r3),%r28
	copy %r28,%r26
	bl _ZNSaI6sphereED1Ev,%r2
	nop
	ldo 20(%r3),%r28
	copy %r28,%r26
	bl _ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED1Ev,%r2
	nop
	b,n .L194
.L195:
	copy %r20,%r4
.L194:
	ldo 17(%r3),%r28
	copy %r28,%r26
	bl _ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED1Ev,%r2
	nop
	copy %r4,%r28
	copy %r28,%r26
.LEHB25:
	bl _Unwind_Resume,%r2
	nop
.LEHE25:
.L197:
	ldw -20(%r3),%r2
	ldw 28(%r3),%r8
	ldw 32(%r3),%r7
	ldw 36(%r3),%r6
	ldw 40(%r3),%r5
	ldw 44(%r3),%r4
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4630:
	.section	.gcc_except_table
.LLSDA4630:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4630-.LLSDACSB4630
.LLSDACSB4630:
	.uleb128 .LEHB23-.LFB4630
	.uleb128 .LEHE23-.LEHB23
	.uleb128 .L195-.LFB4630
	.uleb128 0
	.uleb128 .LEHB24-.LFB4630
	.uleb128 .LEHE24-.LEHB24
	.uleb128 .L196-.LFB4630
	.uleb128 0
	.uleb128 .LEHB25-.LFB4630
	.uleb128 .LEHE25-.LEHB25
	.uleb128 0
	.uleb128 0
.LLSDACSE4630:
	.section	.text._ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_,"axG",@progbits,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC5I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_,comdat
	.size	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_, .-_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_
	.set	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2I6sphereSaIS4_EJ4vec3iEEERPT_St20_Sp_alloc_shared_tagIT0_EDpOT1_
	.section	.text._ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE11_M_gen_randEv,"axG",@progbits,_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE11_M_gen_randEv,comdat
	.align 4
	.weak	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE11_M_gen_randEv
	.type	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE11_M_gen_randEv, @function
_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE11_M_gen_randEv:
	.PROC
	.CALLINFO FRAME=128,NO_CALLS,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4658:
	.cfi_startproc
	copy %r3,%r1
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,128(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldil L'-2147483648,%r19
	copy %r3,%r28
	ldo 16(%r28),%r28
	stw %r19,0(%r28)
	zdepi -1,31,31,%r19
	copy %r3,%r28
	ldo 20(%r28),%r28
	stw %r19,0(%r28)
	stw %r0,8(%r3)
.L202:
	ldw 8(%r3),%r19
	ldi 226,%r28
	comb,<<,n %r28,%r19,.L199
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldw 8(%r3),%r19
	ldwx,s %r19(%r28),%r28
	copy %r28,%r20
	depi 0,31,31,%r20
	ldw 8(%r3),%r28
	ldo 1(%r28),%r19
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldwx,s %r19(%r28),%r28
	extru %r28,31,31,%r19
	ldo 8(%r3),%r28
	ldo 16(%r28),%r28
	or %r20,%r19,%r19
	stw %r19,0(%r28)
	ldw 8(%r3),%r28
	ldo 397(%r28),%r19
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldwx,s %r19(%r28),%r19
	ldo 8(%r3),%r28
	ldo 16(%r28),%r28
	ldw 0(%r28),%r28
	extru %r28,30,31,%r28
	xor %r19,%r28,%r19
	ldo 8(%r3),%r28
	ldo 16(%r28),%r28
	ldw 0(%r28),%r28
	extru %r28,31,1,%r28
	comib,=,n 0,%r28,.L200
	ldil L'-1727479808,%r28
	ldo -3873(%r28),%r28
	b,n .L201
.L200:
	ldi 0,%r28
.L201:
	xor %r28,%r19,%r19
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r20
	ldw 8(%r3),%r28
	zdep %r28,29,30,%r28
	addl %r20,%r28,%r28
	stw %r19,0(%r28)
	ldw 8(%r3),%r28
	ldo 1(%r28),%r28
	stw %r28,8(%r3)
	b,n .L202
.L199:
	ldi 227,%r28
	stw %r28,12(%r3)
.L206:
	ldw 12(%r3),%r19
	ldi 622,%r28
	comb,<<,n %r28,%r19,.L203
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldw 12(%r3),%r19
	ldwx,s %r19(%r28),%r28
	copy %r28,%r20
	depi 0,31,31,%r20
	ldw 12(%r3),%r28
	ldo 1(%r28),%r19
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldwx,s %r19(%r28),%r28
	extru %r28,31,31,%r19
	ldo 8(%r3),%r28
	ldo 20(%r28),%r28
	or %r20,%r19,%r19
	stw %r19,0(%r28)
	ldw 12(%r3),%r28
	ldo -227(%r28),%r19
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldwx,s %r19(%r28),%r19
	ldo 8(%r3),%r28
	ldo 20(%r28),%r28
	ldw 0(%r28),%r28
	extru %r28,30,31,%r28
	xor %r19,%r28,%r19
	ldo 8(%r3),%r28
	ldo 20(%r28),%r28
	ldw 0(%r28),%r28
	extru %r28,31,1,%r28
	comib,=,n 0,%r28,.L204
	ldil L'-1727479808,%r28
	ldo -3873(%r28),%r28
	b,n .L205
.L204:
	ldi 0,%r28
.L205:
	xor %r28,%r19,%r19
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r20
	ldw 12(%r3),%r28
	zdep %r28,29,30,%r28
	addl %r20,%r28,%r28
	stw %r19,0(%r28)
	ldw 12(%r3),%r28
	ldo 1(%r28),%r28
	stw %r28,12(%r3)
	b,n .L206
.L203:
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldo 0(%r28),%r28
	ldo 2492(%r28),%r28
	ldw 0(%r28),%r28
	copy %r28,%r20
	depi 0,31,31,%r20
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldw 0(%r28),%r28
	extru %r28,31,31,%r19
	ldo 8(%r3),%r28
	ldo 24(%r28),%r28
	or %r20,%r19,%r19
	stw %r19,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldo 0(%r28),%r28
	ldo 1584(%r28),%r28
	ldw 0(%r28),%r19
	ldo 8(%r3),%r28
	ldo 24(%r28),%r28
	ldw 0(%r28),%r28
	extru %r28,30,31,%r28
	xor %r19,%r28,%r19
	ldo 8(%r3),%r28
	ldo 24(%r28),%r28
	ldw 0(%r28),%r28
	extru %r28,31,1,%r28
	comib,=,n 0,%r28,.L207
	ldil L'-1727479808,%r28
	ldo -3873(%r28),%r28
	b,n .L208
.L207:
	ldi 0,%r28
.L208:
	xor %r28,%r19,%r19
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldo 0(%r28),%r28
	ldo 2492(%r28),%r28
	stw %r19,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldo 0(%r28),%r28
	ldo 2496(%r28),%r28
	stw %r0,0(%r28)
	nop
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4658:
	.size	_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE11_M_gen_randEv, .-_ZNSt23mersenne_twister_engineIjLj32ELj624ELj397ELj31ELj2567483615ELj11ELj4294967295ELj7ELj2636928640ELj15ELj4022730752ELj18ELj1812433253EE11_M_gen_randEv
	.section	.text._ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC2IS0_EERKSaIT_E,"axG",@progbits,_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC5IS0_EERKSaIT_E,comdat
	.align 4
	.weak	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC2IS0_EERKSaIT_E
	.type	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC2IS0_EERKSaIT_E, @function
_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC2IS0_EERKSaIT_E:
	.PROC
	.CALLINFO FRAME=64,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4661:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	stw %r25,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC2Ev,%r2
	nop
	nop
	ldw -20(%r3),%r2
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4661:
	.size	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC2IS0_EERKSaIT_E, .-_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC2IS0_EERKSaIT_E
	.weak	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC1IS0_EERKSaIT_E
	.set	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC1IS0_EERKSaIT_E,_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC2IS0_EERKSaIT_E
	.section	.text._ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED2Ev,"axG",@progbits,_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED5Ev,comdat
	.align 4
	.weak	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED2Ev
	.type	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED2Ev, @function
_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED2Ev:
	.PROC
	.CALLINFO FRAME=64,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4664:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED2Ev,%r2
	nop
	nop
	ldw -20(%r3),%r2
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4664:
	.size	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED2Ev, .-_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED2Ev
	.weak	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED1Ev
	.set	_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED1Ev,_ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED2Ev
	.section	.text._ZSt18__allocate_guardedISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEESt15__allocated_ptrIT_ERS8_,"axG",@progbits,_ZSt18__allocate_guardedISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEESt15__allocated_ptrIT_ERS8_,comdat
	.align 4
	.weak	_ZSt18__allocate_guardedISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEESt15__allocated_ptrIT_ERS8_
	.type	_ZSt18__allocate_guardedISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEESt15__allocated_ptrIT_ERS8_, @function
_ZSt18__allocate_guardedISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEESt15__allocated_ptrIT_ERS8_:
	.PROC
	.CALLINFO FRAME=64,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=4
	.ENTRY
.LFB4666:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	stw %r4,8(%r3)
	.cfi_offset 3, 0
	.cfi_offset 4, 8
	copy %r28,%r4
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldi 1,%r25
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE8allocateERS6_j,%r2
	nop
	copy %r28,%r24
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r25
	copy %r4,%r26
	bl _ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC1ERS6_PS5_,%r2
	nop
	copy %r4,%r28
	copy %r4,%r28
	ldw -20(%r3),%r2
	ldw 8(%r3),%r4
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4666:
	.size	_ZSt18__allocate_guardedISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEESt15__allocated_ptrIT_ERS8_, .-_ZSt18__allocate_guardedISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEESt15__allocated_ptrIT_ERS8_
	.section	.text._ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED2Ev,"axG",@progbits,_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED5Ev,comdat
	.align 4
	.weak	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED2Ev
	.type	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED2Ev, @function
_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED2Ev:
	.PROC
	.CALLINFO FRAME=64,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4668:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA4668
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldw 4(%r28),%r28
	comib,=,n 0,%r28,.L215
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldw 0(%r28),%r19
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldw 4(%r28),%r28
	ldi 1,%r24
	copy %r28,%r25
	copy %r19,%r26
	bl _ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE10deallocateERS6_PS5_j,%r2
	nop
.L215:
	nop
	ldw -20(%r3),%r2
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4668:
	.section	.gcc_except_table
.LLSDA4668:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4668-.LLSDACSB4668
.LLSDACSB4668:
.LLSDACSE4668:
	.section	.text._ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED2Ev,"axG",@progbits,_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED5Ev,comdat
	.size	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED2Ev, .-_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED2Ev
	.weak	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED1Ev
	.set	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED1Ev,_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED2Ev
	.section	.text._ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE3getEv,"axG",@progbits,_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE3getEv,comdat
	.align 4
	.weak	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE3getEv
	.type	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE3getEv, @function
_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE3getEv:
	.PROC
	.CALLINFO FRAME=64,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4673:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldw 4(%r28),%r28
	copy %r28,%r26
	bl _ZSt12__to_addressISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEPT_S7_,%r2
	nop
	ldw -20(%r3),%r2
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4673:
	.size	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE3getEv, .-_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE3getEv
	.section	.text._ZNSaI6sphereEC2ERKS0_,"axG",@progbits,_ZNSaI6sphereEC5ERKS0_,comdat
	.align 4
	.weak	_ZNSaI6sphereEC2ERKS0_
	.type	_ZNSaI6sphereEC2ERKS0_, @function
_ZNSaI6sphereEC2ERKS0_:
	.PROC
	.CALLINFO FRAME=64,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4675:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	stw %r25,0(%r28)
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	ldw 0(%r28),%r25
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZN9__gnu_cxx13new_allocatorI6sphereEC2ERKS2_,%r2
	nop
	nop
	ldw -20(%r3),%r2
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4675:
	.size	_ZNSaI6sphereEC2ERKS0_, .-_ZNSaI6sphereEC2ERKS0_
	.weak	_ZNSaI6sphereEC1ERKS0_
	.set	_ZNSaI6sphereEC1ERKS0_,_ZNSaI6sphereEC2ERKS0_
	.section	.text._ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED2Ev,"axG",@progbits,_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED5Ev,comdat
	.align 4
	.weak	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED2Ev
	.type	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED2Ev, @function
_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED2Ev:
	.PROC
	.CALLINFO FRAME=64,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4680:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZNSaI6sphereED2Ev,%r2
	nop
	nop
	ldw -20(%r3),%r2
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4680:
	.size	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED2Ev, .-_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED2Ev
	.weak	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED1Ev
	.set	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED1Ev,_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED2Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD2Ev,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD5Ev,comdat
	.align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD2Ev
	.type	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD2Ev, @function
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD2Ev:
	.PROC
	.CALLINFO FRAME=64,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4682:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EED2Ev,%r2
	nop
	nop
	ldw -20(%r3),%r2
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4682:
	.size	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD2Ev, .-_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD2Ev
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD1Ev
	.set	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD1Ev,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD2Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3dEEES1_DpOT_,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC5IJ4vec3dEEES1_DpOT_,comdat
	.align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3dEEES1_DpOT_
	.type	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3dEEES1_DpOT_, @function
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3dEEES1_DpOT_:
	.PROC
	.CALLINFO FRAME=128,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=5
	.ENTRY
.LFB4684:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA4684
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,128(%r30)
	stw %r5,12(%r3)
	stw %r4,16(%r3)
	.cfi_offset 3, 0
	.cfi_offset 5, 12
	.cfi_offset 4, 16
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	stw %r25,0(%r28)
	ldo 0(%r3),%r28
	ldo -44(%r28),%r28
	stw %r24,0(%r28)
	ldo 0(%r3),%r28
	ldo -48(%r28),%r28
	stw %r23,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	copy %r28,%r26
	bl _ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC2Ev,%r2
	nop
	addil LR'_ZTVSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE-$global$,%r27
	copy %r1,%r28
	ldo RR'_ZTVSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE-$global$+8(%r28),%r19
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	stw %r19,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldo 16(%r28),%r4
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	ldw 0(%r28),%r25
	ldo 8(%r3),%r28
	copy %r28,%r26
	bl _ZNSaI6sphereEC1ERKS0_,%r2
	nop
	ldo 8(%r3),%r28
	copy %r28,%r25
	copy %r4,%r26
	bl _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC1ES1_,%r2
	nop
	ldo 8(%r3),%r28
	copy %r28,%r26
	bl _ZNSaI6sphereED1Ev,%r2
	nop
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv,%r2
	nop
	copy %r28,%r4
	ldo 0(%r3),%r28
	ldo -44(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE,%r2
	nop
	copy %r28,%r5
	ldo 0(%r3),%r28
	ldo -48(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE,%r2
	nop
	copy %r28,%r23
	copy %r5,%r24
	copy %r4,%r25
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	ldw 0(%r28),%r26
.LEHB26:
	bl _ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3dEEEvRS1_PT_DpOT0_,%r2
	nop
.LEHE26:
	b,n .L224
.L223:
	copy %r20,%r4
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldo 16(%r28),%r28
	copy %r28,%r26
	bl _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD1Ev,%r2
	nop
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	copy %r28,%r26
	bl _ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev,%r2
	nop
	copy %r4,%r28
	copy %r28,%r26
.LEHB27:
	bl _Unwind_Resume,%r2
	nop
.LEHE27:
.L224:
	ldw -20(%r3),%r2
	ldw 12(%r3),%r5
	ldw 16(%r3),%r4
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4684:
	.section	.gcc_except_table
.LLSDA4684:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4684-.LLSDACSB4684
.LLSDACSB4684:
	.uleb128 .LEHB26-.LFB4684
	.uleb128 .LEHE26-.LEHB26
	.uleb128 .L223-.LFB4684
	.uleb128 0
	.uleb128 .LEHB27-.LFB4684
	.uleb128 .LEHE27-.LEHB27
	.uleb128 0
	.uleb128 0
.LLSDACSE4684:
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3dEEES1_DpOT_,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC5IJ4vec3dEEES1_DpOT_,comdat
	.size	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3dEEES1_DpOT_, .-_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3dEEES1_DpOT_
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC1IJ4vec3dEEES1_DpOT_
	.set	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC1IJ4vec3dEEES1_DpOT_,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3dEEES1_DpOT_
	.section	.text._ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEaSEDn,"axG",@progbits,_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEaSEDn,comdat
	.align 4
	.weak	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEaSEDn
	.type	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEaSEDn, @function
_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEaSEDn:
	.PROC
	.CALLINFO FRAME=64,NO_CALLS,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4686:
	.cfi_startproc
	copy %r3,%r1
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	stw %r25,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	stw %r0,4(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4686:
	.size	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEaSEDn, .-_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEaSEDn
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv,comdat
	.align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv
	.type	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv, @function
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv:
	.PROC
	.CALLINFO FRAME=64,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4687:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldo 16(%r28),%r28
	copy %r28,%r26
	bl _ZN9__gnu_cxx16__aligned_bufferI6sphereE6_M_ptrEv,%r2
	nop
	ldw -20(%r3),%r2
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4687:
	.size	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv, .-_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3iEEES1_DpOT_,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC5IJ4vec3iEEES1_DpOT_,comdat
	.align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3iEEES1_DpOT_
	.type	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3iEEES1_DpOT_, @function
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3iEEES1_DpOT_:
	.PROC
	.CALLINFO FRAME=128,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=5
	.ENTRY
.LFB4689:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA4689
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,128(%r30)
	stw %r5,12(%r3)
	stw %r4,16(%r3)
	.cfi_offset 3, 0
	.cfi_offset 5, 12
	.cfi_offset 4, 16
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	stw %r25,0(%r28)
	ldo 0(%r3),%r28
	ldo -44(%r28),%r28
	stw %r24,0(%r28)
	ldo 0(%r3),%r28
	ldo -48(%r28),%r28
	stw %r23,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	copy %r28,%r26
	bl _ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC2Ev,%r2
	nop
	addil LR'_ZTVSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE-$global$,%r27
	copy %r1,%r28
	ldo RR'_ZTVSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE-$global$+8(%r28),%r19
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	stw %r19,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldo 16(%r28),%r4
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	ldw 0(%r28),%r25
	ldo 8(%r3),%r28
	copy %r28,%r26
	bl _ZNSaI6sphereEC1ERKS0_,%r2
	nop
	ldo 8(%r3),%r28
	copy %r28,%r25
	copy %r4,%r26
	bl _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC1ES1_,%r2
	nop
	ldo 8(%r3),%r28
	copy %r28,%r26
	bl _ZNSaI6sphereED1Ev,%r2
	nop
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv,%r2
	nop
	copy %r28,%r4
	ldo 0(%r3),%r28
	ldo -44(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE,%r2
	nop
	copy %r28,%r5
	ldo 0(%r3),%r28
	ldo -48(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE,%r2
	nop
	copy %r28,%r23
	copy %r5,%r24
	copy %r4,%r25
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	ldw 0(%r28),%r26
.LEHB28:
	bl _ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3iEEEvRS1_PT_DpOT0_,%r2
	nop
.LEHE28:
	b,n .L232
.L231:
	copy %r20,%r4
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldo 16(%r28),%r28
	copy %r28,%r26
	bl _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD1Ev,%r2
	nop
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	copy %r28,%r26
	bl _ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev,%r2
	nop
	copy %r4,%r28
	copy %r28,%r26
.LEHB29:
	bl _Unwind_Resume,%r2
	nop
.LEHE29:
.L232:
	ldw -20(%r3),%r2
	ldw 12(%r3),%r5
	ldw 16(%r3),%r4
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4689:
	.section	.gcc_except_table
.LLSDA4689:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4689-.LLSDACSB4689
.LLSDACSB4689:
	.uleb128 .LEHB28-.LFB4689
	.uleb128 .LEHE28-.LEHB28
	.uleb128 .L231-.LFB4689
	.uleb128 0
	.uleb128 .LEHB29-.LFB4689
	.uleb128 .LEHE29-.LEHB29
	.uleb128 0
	.uleb128 0
.LLSDACSE4689:
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3iEEES1_DpOT_,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC5IJ4vec3iEEES1_DpOT_,comdat
	.size	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3iEEES1_DpOT_, .-_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3iEEES1_DpOT_
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC1IJ4vec3iEEES1_DpOT_
	.set	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC1IJ4vec3iEEES1_DpOT_,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEC2IJ4vec3iEEES1_DpOT_
	.section	.text._ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC2Ev,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC5Ev,comdat
	.align 4
	.weak	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC2Ev
	.type	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC2Ev, @function
_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC2Ev:
	.PROC
	.CALLINFO FRAME=64,NO_CALLS,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4702:
	.cfi_startproc
	copy %r3,%r1
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	nop
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4702:
	.size	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC2Ev, .-_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC2Ev
	.weak	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC1Ev
	.set	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC1Ev,_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEEC2Ev
	.section	.text._ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED2Ev,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED5Ev,comdat
	.align 4
	.weak	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED2Ev
	.type	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED2Ev, @function
_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED2Ev:
	.PROC
	.CALLINFO FRAME=64,NO_CALLS,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4705:
	.cfi_startproc
	copy %r3,%r1
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	nop
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4705:
	.size	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED2Ev, .-_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED2Ev
	.weak	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED1Ev
	.set	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED1Ev,_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEED2Ev
	.section	.text._ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE8allocateERS6_j,"axG",@progbits,_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE8allocateERS6_j,comdat
	.align 4
	.weak	_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE8allocateERS6_j
	.type	_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE8allocateERS6_j, @function
_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE8allocateERS6_j:
	.PROC
	.CALLINFO FRAME=64,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4707:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	stw %r25,0(%r28)
	ldi 0,%r24
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	ldw 0(%r28),%r25
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8allocateEjPKv,%r2
	nop
	ldw -20(%r3),%r2
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4707:
	.size	_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE8allocateERS6_j, .-_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE8allocateERS6_j
	.section	.text._ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC2ERS6_PS5_,"axG",@progbits,_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC5ERS6_PS5_,comdat
	.align 4
	.weak	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC2ERS6_PS5_
	.type	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC2ERS6_PS5_, @function
_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC2ERS6_PS5_:
	.PROC
	.CALLINFO FRAME=64,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4709:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	stw %r25,0(%r28)
	ldo 0(%r3),%r28
	ldo -44(%r28),%r28
	stw %r24,0(%r28)
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZSt11__addressofISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEPT_RS7_,%r2
	nop
	copy %r28,%r19
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	stw %r19,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldo 0(%r3),%r19
	ldo -44(%r19),%r19
	ldw 0(%r19),%r19
	stw %r19,4(%r28)
	nop
	ldw -20(%r3),%r2
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4709:
	.size	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC2ERS6_PS5_, .-_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC2ERS6_PS5_
	.weak	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC1ERS6_PS5_
	.set	_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC1ERS6_PS5_,_ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC2ERS6_PS5_
	.section	.text._ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE10deallocateERS6_PS5_j,"axG",@progbits,_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE10deallocateERS6_PS5_j,comdat
	.align 4
	.weak	_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE10deallocateERS6_PS5_j
	.type	_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE10deallocateERS6_PS5_j, @function
_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE10deallocateERS6_PS5_j:
	.PROC
	.CALLINFO FRAME=64,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4711:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	stw %r25,0(%r28)
	ldo 0(%r3),%r28
	ldo -44(%r28),%r28
	stw %r24,0(%r28)
	ldo 0(%r3),%r28
	ldo -44(%r28),%r28
	ldw 0(%r28),%r24
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	ldw 0(%r28),%r25
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE10deallocateEPS5_j,%r2
	nop
	nop
	ldw -20(%r3),%r2
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4711:
	.size	_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE10deallocateERS6_PS5_j, .-_ZNSt16allocator_traitsISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEE10deallocateERS6_PS5_j
	.section	.text._ZSt12__to_addressISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEPT_S7_,"axG",@progbits,_ZSt12__to_addressISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEPT_S7_,comdat
	.align 4
	.weak	_ZSt12__to_addressISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEPT_S7_
	.type	_ZSt12__to_addressISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEPT_S7_, @function
_ZSt12__to_addressISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEPT_S7_:
	.PROC
	.CALLINFO FRAME=64,NO_CALLS,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4712:
	.cfi_startproc
	copy %r3,%r1
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4712:
	.size	_ZSt12__to_addressISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEPT_S7_, .-_ZSt12__to_addressISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEPT_S7_
	.section	.text._ZN9__gnu_cxx13new_allocatorI6sphereEC2ERKS2_,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorI6sphereEC5ERKS2_,comdat
	.align 4
	.weak	_ZN9__gnu_cxx13new_allocatorI6sphereEC2ERKS2_
	.type	_ZN9__gnu_cxx13new_allocatorI6sphereEC2ERKS2_, @function
_ZN9__gnu_cxx13new_allocatorI6sphereEC2ERKS2_:
	.PROC
	.CALLINFO FRAME=64,NO_CALLS,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4714:
	.cfi_startproc
	copy %r3,%r1
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	stw %r25,0(%r28)
	nop
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4714:
	.size	_ZN9__gnu_cxx13new_allocatorI6sphereEC2ERKS2_, .-_ZN9__gnu_cxx13new_allocatorI6sphereEC2ERKS2_
	.weak	_ZN9__gnu_cxx13new_allocatorI6sphereEC1ERKS2_
	.set	_ZN9__gnu_cxx13new_allocatorI6sphereEC1ERKS2_,_ZN9__gnu_cxx13new_allocatorI6sphereEC2ERKS2_
	.section	.text._ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC2Ev,"axG",@progbits,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC5Ev,comdat
	.align 4
	.weak	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC2Ev
	.type	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC2Ev, @function
_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC2Ev:
	.PROC
	.CALLINFO FRAME=64,NO_CALLS,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4717:
	.cfi_startproc
	copy %r3,%r1
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	addil LR'_ZTVSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE-$global$,%r27
	copy %r1,%r28
	ldo RR'_ZTVSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE-$global$+8(%r28),%r19
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	stw %r19,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldi 1,%r19
	stw %r19,4(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldi 1,%r19
	stw %r19,8(%r28)
	nop
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4717:
	.size	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC2Ev, .-_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC2Ev
	.weak	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC1Ev
	.set	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC1Ev,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EEC2Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC2ES1_,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC5ES1_,comdat
	.align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC2ES1_
	.type	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC2ES1_, @function
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC2ES1_:
	.PROC
	.CALLINFO FRAME=64,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4720:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	stw %r25,0(%r28)
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	ldw 0(%r28),%r25
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC2ERKS1_,%r2
	nop
	nop
	ldw -20(%r3),%r2
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4720:
	.size	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC2ES1_, .-_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC2ES1_
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC1ES1_
	.set	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC1ES1_,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplC2ES1_
	.section	.text._ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3dEEEvRS1_PT_DpOT0_,"axG",@progbits,_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3dEEEvRS1_PT_DpOT0_,comdat
	.align 4
	.weak	_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3dEEEvRS1_PT_DpOT0_
	.type	_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3dEEEvRS1_PT_DpOT0_, @function
_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3dEEEvRS1_PT_DpOT0_:
	.PROC
	.CALLINFO FRAME=64,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=4
	.ENTRY
.LFB4722:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	stw %r4,8(%r3)
	.cfi_offset 3, 0
	.cfi_offset 4, 8
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	stw %r25,0(%r28)
	ldo 0(%r3),%r28
	ldo -44(%r28),%r28
	stw %r24,0(%r28)
	ldo 0(%r3),%r28
	ldo -48(%r28),%r28
	stw %r23,0(%r28)
	ldo 0(%r3),%r28
	ldo -44(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE,%r2
	nop
	copy %r28,%r4
	ldo 0(%r3),%r28
	ldo -48(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE,%r2
	nop
	copy %r28,%r23
	copy %r4,%r24
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	ldw 0(%r28),%r25
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_,%r2
	nop
	nop
	ldw -20(%r3),%r2
	ldw 8(%r3),%r4
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4722:
	.size	_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3dEEEvRS1_PT_DpOT0_, .-_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3dEEEvRS1_PT_DpOT0_
	.section	.text._ZN9__gnu_cxx16__aligned_bufferI6sphereE6_M_ptrEv,"axG",@progbits,_ZN9__gnu_cxx16__aligned_bufferI6sphereE6_M_ptrEv,comdat
	.align 4
	.weak	_ZN9__gnu_cxx16__aligned_bufferI6sphereE6_M_ptrEv
	.type	_ZN9__gnu_cxx16__aligned_bufferI6sphereE6_M_ptrEv, @function
_ZN9__gnu_cxx16__aligned_bufferI6sphereE6_M_ptrEv:
	.PROC
	.CALLINFO FRAME=64,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4723:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZN9__gnu_cxx16__aligned_bufferI6sphereE7_M_addrEv,%r2
	nop
	ldw -20(%r3),%r2
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4723:
	.size	_ZN9__gnu_cxx16__aligned_bufferI6sphereE6_M_ptrEv, .-_ZN9__gnu_cxx16__aligned_bufferI6sphereE6_M_ptrEv
	.section	.text._ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3iEEEvRS1_PT_DpOT0_,"axG",@progbits,_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3iEEEvRS1_PT_DpOT0_,comdat
	.align 4
	.weak	_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3iEEEvRS1_PT_DpOT0_
	.type	_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3iEEEvRS1_PT_DpOT0_, @function
_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3iEEEvRS1_PT_DpOT0_:
	.PROC
	.CALLINFO FRAME=64,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=4
	.ENTRY
.LFB4724:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	stw %r4,8(%r3)
	.cfi_offset 3, 0
	.cfi_offset 4, 8
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	stw %r25,0(%r28)
	ldo 0(%r3),%r28
	ldo -44(%r28),%r28
	stw %r24,0(%r28)
	ldo 0(%r3),%r28
	ldo -48(%r28),%r28
	stw %r23,0(%r28)
	ldo 0(%r3),%r28
	ldo -44(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE,%r2
	nop
	copy %r28,%r4
	ldo 0(%r3),%r28
	ldo -48(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE,%r2
	nop
	copy %r28,%r23
	copy %r4,%r24
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	ldw 0(%r28),%r25
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_,%r2
	nop
	nop
	ldw -20(%r3),%r2
	ldw 8(%r3),%r4
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4724:
	.size	_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3iEEEvRS1_PT_DpOT0_, .-_ZNSt16allocator_traitsISaI6sphereEE9constructIS0_J4vec3iEEEvRS1_PT_DpOT0_
	.section	.text._ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8allocateEjPKv,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8allocateEjPKv,comdat
	.align 4
	.weak	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8allocateEjPKv
	.type	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8allocateEjPKv, @function
_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8allocateEjPKv:
	.PROC
	.CALLINFO FRAME=64,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4727:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	stw %r25,0(%r28)
	ldo 0(%r3),%r28
	ldo -44(%r28),%r28
	stw %r24,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZNK9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8max_sizeEv,%r2
	nop
	copy %r28,%r19
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	ldw 0(%r28),%r28
	comclr,>>= %r19,%r28,%r28
	ldi 1,%r28
	extru %r28,31,8,%r28
	comib,=,n 0,%r28,.L249
	bl _ZSt17__throw_bad_allocv,%r2
	nop
.L249:
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	ldw 0(%r28),%r28
	zdep %r28,25,26,%r28
	copy %r28,%r26
	bl _Znwj,%r2
	nop
	ldw -20(%r3),%r2
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4727:
	.size	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8allocateEjPKv, .-_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8allocateEjPKv
	.section	.text._ZSt11__addressofISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEPT_RS7_,"axG",@progbits,_ZSt11__addressofISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEPT_RS7_,comdat
	.align 4
	.weak	_ZSt11__addressofISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEPT_RS7_
	.type	_ZSt11__addressofISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEPT_RS7_, @function
_ZSt11__addressofISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEPT_RS7_:
	.PROC
	.CALLINFO FRAME=64,NO_CALLS,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4728:
	.cfi_startproc
	copy %r3,%r1
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4728:
	.size	_ZSt11__addressofISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEPT_RS7_, .-_ZSt11__addressofISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEPT_RS7_
	.section	.text._ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE10deallocateEPS5_j,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE10deallocateEPS5_j,comdat
	.align 4
	.weak	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE10deallocateEPS5_j
	.type	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE10deallocateEPS5_j, @function
_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE10deallocateEPS5_j:
	.PROC
	.CALLINFO FRAME=64,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4729:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	stw %r25,0(%r28)
	ldo 0(%r3),%r28
	ldo -44(%r28),%r28
	stw %r24,0(%r28)
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZdlPv,%r2
	nop
	nop
	ldw -20(%r3),%r2
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4729:
	.size	_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE10deallocateEPS5_j, .-_ZN9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE10deallocateEPS5_j
	.section	.text._ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC2ERKS1_,"axG",@progbits,_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC5ERKS1_,comdat
	.align 4
	.weak	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC2ERKS1_
	.type	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC2ERKS1_, @function
_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC2ERKS1_:
	.PROC
	.CALLINFO FRAME=64,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4731:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	stw %r25,0(%r28)
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	ldw 0(%r28),%r25
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZNSaI6sphereEC2ERKS0_,%r2
	nop
	nop
	ldw -20(%r3),%r2
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4731:
	.size	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC2ERKS1_, .-_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC2ERKS1_
	.weak	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC1ERKS1_
	.set	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC1ERKS1_,_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EEC2ERKS1_
	.section	.text._ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_,comdat
	.align 4
	.weak	_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_
	.type	_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_, @function
_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_:
	.PROC
	.CALLINFO FRAME=128,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=6,ENTRY_FR=12
	.ENTRY
.LFB4733:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA4733
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,128(%r30)
	stw %r6,56(%r3)
	stw %r5,60(%r3)
	stw %r4,64(%r3)
	.cfi_offset 3, 0
	.cfi_offset 6, 56
	.cfi_offset 5, 60
	.cfi_offset 4, 64
	ldo 72(%r3),%r1
	fstds,ma %fr12,8(%r1)
	.cfi_offset 48, 72
	.cfi_offset 49, 76
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	stw %r25,0(%r28)
	ldo 0(%r3),%r28
	ldo -44(%r28),%r28
	stw %r24,0(%r28)
	ldo 0(%r3),%r28
	ldo -48(%r28),%r28
	stw %r23,0(%r28)
	ldo 0(%r3),%r28
	ldo -44(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE,%r2
	nop
	ldw 0(%r28),%r19
	ldw 4(%r28),%r20
	stw %r19,8(%r3)
	stw %r20,12(%r3)
	ldw 8(%r28),%r19
	ldw 12(%r28),%r20
	copy %r3,%r21
	ldo 16(%r21),%r21
	stw %r19,0(%r21)
	stw %r20,4(%r21)
	ldo 8(%r3),%r19
	ldo 16(%r19),%r19
	ldo 0(%r28),%r28
	ldo 16(%r28),%r28
	ldw 4(%r28),%r29
	ldw 0(%r28),%r28
	stw %r28,0(%r19)
	stw %r29,4(%r19)
	ldo 0(%r3),%r28
	ldo -48(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZSt7forwardIdEOT_RNSt16remove_referenceIS0_E4typeE,%r2
	nop
	fldds 0(%r28),%fr12
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	ldw 0(%r28),%r4
	copy %r4,%r25
	ldi 48,%r26
	bl _ZnwjPv,%r2
	nop
	copy %r28,%r6
	ldo 8(%r3),%r28
	ldo 24(%r28),%r28
	ldw 8(%r3),%r19
	ldw 12(%r3),%r20
	stw %r19,0(%r28)
	stw %r20,4(%r28)
	copy %r3,%r19
	ldo 16(%r19),%r19
	ldw 4(%r19),%r20
	ldw 0(%r19),%r19
	stw %r19,8(%r28)
	stw %r20,12(%r28)
	ldo 0(%r28),%r28
	ldo 16(%r28),%r19
	ldo 8(%r3),%r28
	ldo 16(%r28),%r28
	ldw 4(%r28),%r29
	ldw 0(%r28),%r28
	stw %r28,0(%r19)
	stw %r29,4(%r19)
	ldo 32(%r3),%r28
	fcpy,dbl %fr12,%fr7
	copy %r28,%r25
	copy %r6,%r26
.LEHB30:
	bl _ZN6sphereC1E4vec3d,%r2
	nop
.LEHE30:
	b,n .L258
.L257:
	copy %r20,%r5
	copy %r4,%r25
	copy %r6,%r26
	bl _ZdlPvS_,%r2
	nop
	copy %r5,%r28
	copy %r28,%r26
.LEHB31:
	bl _Unwind_Resume,%r2
	nop
.LEHE31:
.L258:
	ldw -20(%r3),%r2
	ldw 56(%r3),%r6
	ldw 60(%r3),%r5
	ldw 64(%r3),%r4
	ldo 72(%r3),%r1
	fldds,ma 8(%r1),%fr12
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4733:
	.section	.gcc_except_table
.LLSDA4733:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4733-.LLSDACSB4733
.LLSDACSB4733:
	.uleb128 .LEHB30-.LFB4733
	.uleb128 .LEHE30-.LEHB30
	.uleb128 .L257-.LFB4733
	.uleb128 0
	.uleb128 .LEHB31-.LFB4733
	.uleb128 .LEHE31-.LEHB31
	.uleb128 0
	.uleb128 0
.LLSDACSE4733:
	.section	.text._ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_,comdat
	.size	_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_, .-_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3dEEEvPT_DpOT0_
	.section	.text._ZN9__gnu_cxx16__aligned_bufferI6sphereE7_M_addrEv,"axG",@progbits,_ZN9__gnu_cxx16__aligned_bufferI6sphereE7_M_addrEv,comdat
	.align 4
	.weak	_ZN9__gnu_cxx16__aligned_bufferI6sphereE7_M_addrEv
	.type	_ZN9__gnu_cxx16__aligned_bufferI6sphereE7_M_addrEv, @function
_ZN9__gnu_cxx16__aligned_bufferI6sphereE7_M_addrEv:
	.PROC
	.CALLINFO FRAME=64,NO_CALLS,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4734:
	.cfi_startproc
	copy %r3,%r1
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4734:
	.size	_ZN9__gnu_cxx16__aligned_bufferI6sphereE7_M_addrEv, .-_ZN9__gnu_cxx16__aligned_bufferI6sphereE7_M_addrEv
	.section	.text._ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_,comdat
	.align 4
	.weak	_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_
	.type	_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_, @function
_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_:
	.PROC
	.CALLINFO FRAME=128,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=6,ENTRY_FR=12
	.ENTRY
.LFB4735:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA4735
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,128(%r30)
	stw %r6,56(%r3)
	stw %r5,60(%r3)
	stw %r4,64(%r3)
	.cfi_offset 3, 0
	.cfi_offset 6, 56
	.cfi_offset 5, 60
	.cfi_offset 4, 64
	ldo 72(%r3),%r1
	fstds,ma %fr12,8(%r1)
	.cfi_offset 48, 72
	.cfi_offset 49, 76
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	stw %r25,0(%r28)
	ldo 0(%r3),%r28
	ldo -44(%r28),%r28
	stw %r24,0(%r28)
	ldo 0(%r3),%r28
	ldo -48(%r28),%r28
	stw %r23,0(%r28)
	ldo 0(%r3),%r28
	ldo -44(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZSt7forwardI4vec3EOT_RNSt16remove_referenceIS1_E4typeE,%r2
	nop
	ldw 0(%r28),%r19
	ldw 4(%r28),%r20
	stw %r19,8(%r3)
	stw %r20,12(%r3)
	ldw 8(%r28),%r19
	ldw 12(%r28),%r20
	copy %r3,%r21
	ldo 16(%r21),%r21
	stw %r19,0(%r21)
	stw %r20,4(%r21)
	ldo 8(%r3),%r19
	ldo 16(%r19),%r19
	ldo 0(%r28),%r28
	ldo 16(%r28),%r28
	ldw 4(%r28),%r29
	ldw 0(%r28),%r28
	stw %r28,0(%r19)
	stw %r29,4(%r19)
	ldo 0(%r3),%r28
	ldo -48(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZSt7forwardIiEOT_RNSt16remove_referenceIS0_E4typeE,%r2
	nop
	fldws 0(%r28),%fr22L
	fcnvxf,sgl,dbl %fr22L,%fr12
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	ldw 0(%r28),%r4
	copy %r4,%r25
	ldi 48,%r26
	bl _ZnwjPv,%r2
	nop
	copy %r28,%r6
	ldo 8(%r3),%r28
	ldo 24(%r28),%r28
	ldw 8(%r3),%r19
	ldw 12(%r3),%r20
	stw %r19,0(%r28)
	stw %r20,4(%r28)
	copy %r3,%r19
	ldo 16(%r19),%r19
	ldw 4(%r19),%r20
	ldw 0(%r19),%r19
	stw %r19,8(%r28)
	stw %r20,12(%r28)
	ldo 0(%r28),%r28
	ldo 16(%r28),%r19
	ldo 8(%r3),%r28
	ldo 16(%r28),%r28
	ldw 4(%r28),%r29
	ldw 0(%r28),%r28
	stw %r28,0(%r19)
	stw %r29,4(%r19)
	ldo 32(%r3),%r28
	fcpy,dbl %fr12,%fr7
	copy %r28,%r25
	copy %r6,%r26
.LEHB32:
	bl _ZN6sphereC1E4vec3d,%r2
	nop
.LEHE32:
	b,n .L264
.L263:
	copy %r20,%r5
	copy %r4,%r25
	copy %r6,%r26
	bl _ZdlPvS_,%r2
	nop
	copy %r5,%r28
	copy %r28,%r26
.LEHB33:
	bl _Unwind_Resume,%r2
	nop
.LEHE33:
.L264:
	ldw -20(%r3),%r2
	ldw 56(%r3),%r6
	ldw 60(%r3),%r5
	ldw 64(%r3),%r4
	ldo 72(%r3),%r1
	fldds,ma 8(%r1),%fr12
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4735:
	.section	.gcc_except_table
.LLSDA4735:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4735-.LLSDACSB4735
.LLSDACSB4735:
	.uleb128 .LEHB32-.LFB4735
	.uleb128 .LEHE32-.LEHB32
	.uleb128 .L263-.LFB4735
	.uleb128 0
	.uleb128 .LEHB33-.LFB4735
	.uleb128 .LEHE33-.LEHB33
	.uleb128 0
	.uleb128 0
.LLSDACSE4735:
	.section	.text._ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_,comdat
	.size	_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_, .-_ZN9__gnu_cxx13new_allocatorI6sphereE9constructIS1_J4vec3iEEEvPT_DpOT0_
	.section	.text._ZNK9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8max_sizeEv,"axG",@progbits,_ZNK9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8max_sizeEv,comdat
	.align 4
	.weak	_ZNK9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8max_sizeEv
	.type	_ZNK9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8max_sizeEv, @function
_ZNK9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8max_sizeEv:
	.PROC
	.CALLINFO FRAME=64,NO_CALLS,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4736:
	.cfi_startproc
	copy %r3,%r1
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	zdepi -1,31,25,%r28
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4736:
	.size	_ZNK9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8max_sizeEv, .-_ZNK9__gnu_cxx13new_allocatorISt23_Sp_counted_ptr_inplaceI6sphereSaIS2_ELNS_12_Lock_policyE2EEE8max_sizeEv
	.weak	_ZTVSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE
	.section	.rodata._ZTVSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE,"aG",@progbits,_ZTVSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align 4
	.type	_ZTVSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTVSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE, 28
_ZTVSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE:
	.word	0
	.word	_ZTISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE
	.word	P%_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.word	P%_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.word	P%_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.word	P%_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.word	P%_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.weak	_ZTVSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE
	.section	.rodata._ZTVSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE,"aG",@progbits,_ZTVSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align 4
	.type	_ZTVSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTVSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE, 28
_ZTVSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE:
	.word	0
	.word	_ZTISt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE
	.word	0
	.word	0
	.word	P%__cxa_pure_virtual
	.word	P%_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.word	P%__cxa_pure_virtual
	.weak	_ZTISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE
	.section	.rodata._ZTISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE,"aG",@progbits,_ZTISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align 4
	.type	_ZTISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE, 12
_ZTISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE:
	.word	_ZTVN10__cxxabiv120__si_class_type_infoE+8
	.word	_ZTSSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE
	.word	_ZTISt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE
	.weak	_ZTSSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE
	.section	.rodata._ZTSSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE,"aG",@progbits,_ZTSSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align 4
	.type	_ZTSSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTSSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE, 73
_ZTSSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE:
	.stringz	"St23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE"
	.weak	_ZTISt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE
	.section	.rodata._ZTISt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE,"aG",@progbits,_ZTISt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align 4
	.type	_ZTISt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTISt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE, 12
_ZTISt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE:
	.word	_ZTVN10__cxxabiv120__si_class_type_infoE+8
	.word	_ZTSSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE
	.word	_ZTISt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE
	.weak	_ZTSSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE
	.section	.rodata._ZTSSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE,"aG",@progbits,_ZTSSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align 4
	.type	_ZTSSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTSSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE, 52
_ZTSSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE:
	.stringz	"St16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE"
	.section	.rodata
	.align 4
.LC23:
	.word	P%_ZNSt8ios_base4InitD1Ev
	.text
	.align 4
	.type	_Z41__static_initialization_and_destruction_0ii, @function
_Z41__static_initialization_and_destruction_0ii:
	.PROC
	.CALLINFO FRAME=64,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4753:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	stw %r25,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	comib,<>,n 1,%r28,.L269
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	ldw 0(%r28),%r19
	zdepi -1,31,16,%r28
	comb,<>,n %r28,%r19,.L269
	addil LR'_ZStL8__ioinit-$global$,%r27
	copy %r1,%r28
	ldo RR'_ZStL8__ioinit-$global$(%r28),%r26
	bl _ZNSt8ios_base4InitC1Ev,%r2
	nop
	ldil LR'.LC23,%r28
	ldo RR'.LC23(%r28),%r28
	ldw 0(%r28),%r19
	addil LR'__dso_handle-$global$,%r27
	copy %r1,%r28
	ldo RR'__dso_handle-$global$(%r28),%r24
	addil LR'_ZStL8__ioinit-$global$,%r27
	copy %r1,%r28
	ldo RR'_ZStL8__ioinit-$global$(%r28),%r25
	copy %r19,%r26
	bl __cxa_atexit,%r2
	nop
.L269:
	nop
	ldw -20(%r3),%r2
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4753:
	.size	_Z41__static_initialization_and_destruction_0ii, .-_Z41__static_initialization_and_destruction_0ii
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.type	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED2Ev:
	.PROC
	.CALLINFO FRAME=64,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4755:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	addil LR'_ZTVSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE-$global$,%r27
	copy %r1,%r28
	ldo RR'_ZTVSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE-$global$+8(%r28),%r19
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	stw %r19,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldo 16(%r28),%r28
	copy %r28,%r26
	bl _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_ImplD1Ev,%r2
	nop
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	copy %r28,%r26
	bl _ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EED2Ev,%r2
	nop
	nop
	ldw -20(%r3),%r2
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4755:
	.size	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.set	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED1Ev,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED0Ev,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.type	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED0Ev, @function
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED0Ev:
	.PROC
	.CALLINFO FRAME=64,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4757:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED1Ev,%r2
	nop
	ldi 64,%r25
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZdlPvj,%r2
	nop
	ldw -20(%r3),%r2
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4757:
	.size	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED0Ev, .-_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,comdat
	.align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.type	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, @function
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv:
	.PROC
	.CALLINFO FRAME=64,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=4
	.ENTRY
.LFB4758:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	stw %r4,8(%r3)
	.cfi_offset 3, 0
	.cfi_offset 4, 8
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldo 16(%r28),%r28
	copy %r28,%r26
	bl _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_Impl8_M_allocEv,%r2
	nop
	copy %r28,%r4
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv,%r2
	nop
	copy %r28,%r25
	copy %r4,%r26
	bl _ZNSt16allocator_traitsISaI6sphereEE7destroyIS0_EEvRS1_PT_,%r2
	nop
	nop
	ldw -20(%r3),%r2
	ldw 8(%r3),%r4
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4758:
	.size	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, .-_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,comdat
	.align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.type	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, @function
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv:
	.PROC
	.CALLINFO FRAME=128,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4759:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,128(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldo 16(%r28),%r28
	copy %r28,%r26
	bl _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_Impl8_M_allocEv,%r2
	nop
	copy %r28,%r25
	ldo 8(%r3),%r28
	copy %r28,%r26
	bl _ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEEC1IS0_EERKSaIT_E,%r2
	nop
	ldo 12(%r3),%r19
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r24
	ldo 8(%r3),%r28
	copy %r28,%r25
	copy %r19,%r26
	bl _ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEEC1ERS6_PS5_,%r2
	nop
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EED1Ev,%r2
	nop
	ldo 12(%r3),%r28
	copy %r28,%r26
	bl _ZNSt15__allocated_ptrISaISt23_Sp_counted_ptr_inplaceI6sphereSaIS1_ELN9__gnu_cxx12_Lock_policyE2EEEED1Ev,%r2
	nop
	ldo 8(%r3),%r28
	copy %r28,%r26
	bl _ZNSaISt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EEED1Ev,%r2
	nop
	nop
	ldw -20(%r3),%r2
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4759:
	.size	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, .-_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,comdat
	.align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.type	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, @function
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info:
	.PROC
	.CALLINFO FRAME=64,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4760:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	stw %r25,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE6_M_ptrEv,%r2
	nop
	stw %r28,8(%r3)
	bl _ZNSt19_Sp_make_shared_tag5_S_tiEv,%r2
	nop
	copy %r28,%r19
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	ldw 0(%r28),%r28
	comb,=,n %r19,%r28,.L275
	addil LR'_ZTISt19_Sp_make_shared_tag-$global$,%r27
	copy %r1,%r28
	ldo RR'_ZTISt19_Sp_make_shared_tag-$global$(%r28),%r25
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZNKSt9type_infoeqERKS_,%r2
	nop
	comib,=,n 0,%r28,.L276
.L275:
	ldi 1,%r28
	b,n .L277
.L276:
	ldi 0,%r28
.L277:
	comib,=,n 0,%r28,.L278
	ldw 8(%r3),%r28
	b,n .L279
.L278:
	ldi 0,%r28
.L279:
	ldw -20(%r3),%r2
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4760:
	.size	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, .-_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_Impl8_M_allocEv,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_Impl8_M_allocEv,comdat
	.align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_Impl8_M_allocEv
	.type	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_Impl8_M_allocEv, @function
_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_Impl8_M_allocEv:
	.PROC
	.CALLINFO FRAME=64,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4761:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EE6_S_getERS2_,%r2
	nop
	ldw -20(%r3),%r2
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4761:
	.size	_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_Impl8_M_allocEv, .-_ZNSt23_Sp_counted_ptr_inplaceI6sphereSaIS0_ELN9__gnu_cxx12_Lock_policyE2EE5_Impl8_M_allocEv
	.section	.text._ZNSt16allocator_traitsISaI6sphereEE7destroyIS0_EEvRS1_PT_,"axG",@progbits,_ZNSt16allocator_traitsISaI6sphereEE7destroyIS0_EEvRS1_PT_,comdat
	.align 4
	.weak	_ZNSt16allocator_traitsISaI6sphereEE7destroyIS0_EEvRS1_PT_
	.type	_ZNSt16allocator_traitsISaI6sphereEE7destroyIS0_EEvRS1_PT_, @function
_ZNSt16allocator_traitsISaI6sphereEE7destroyIS0_EEvRS1_PT_:
	.PROC
	.CALLINFO FRAME=64,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4762:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	stw %r25,0(%r28)
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	ldw 0(%r28),%r25
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZN9__gnu_cxx13new_allocatorI6sphereE7destroyIS1_EEvPT_,%r2
	nop
	nop
	ldw -20(%r3),%r2
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4762:
	.size	_ZNSt16allocator_traitsISaI6sphereEE7destroyIS0_EEvRS1_PT_, .-_ZNSt16allocator_traitsISaI6sphereEE7destroyIS0_EEvRS1_PT_
	.section	.text._ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EE6_S_getERS2_,"axG",@progbits,_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EE6_S_getERS2_,comdat
	.align 4
	.weak	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EE6_S_getERS2_
	.type	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EE6_S_getERS2_, @function
_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EE6_S_getERS2_:
	.PROC
	.CALLINFO FRAME=64,NO_CALLS,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4763:
	.cfi_startproc
	copy %r3,%r1
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4763:
	.size	_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EE6_S_getERS2_, .-_ZNSt14_Sp_ebo_helperILi0ESaI6sphereELb1EE6_S_getERS2_
	.section	.text._ZN6sphereD2Ev,"axG",@progbits,_ZN6sphereD5Ev,comdat
	.align 4
	.weak	_ZN6sphereD2Ev
	.type	_ZN6sphereD2Ev, @function
_ZN6sphereD2Ev:
	.PROC
	.CALLINFO FRAME=64,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4766:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	addil LR'_ZTV6sphere-$global$,%r27
	copy %r1,%r28
	ldo RR'_ZTV6sphere-$global$+8(%r28),%r19
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	stw %r19,0(%r28)
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	ldw 0(%r28),%r28
	ldo 40(%r28),%r28
	copy %r28,%r26
	bl _ZNSt10shared_ptrI8materialED1Ev,%r2
	nop
	nop
	ldw -20(%r3),%r2
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4766:
	.size	_ZN6sphereD2Ev, .-_ZN6sphereD2Ev
	.weak	_ZN6sphereD1Ev
	.set	_ZN6sphereD1Ev,_ZN6sphereD2Ev
	.section	.text._ZN9__gnu_cxx13new_allocatorI6sphereE7destroyIS1_EEvPT_,"axG",@progbits,_ZN9__gnu_cxx13new_allocatorI6sphereE7destroyIS1_EEvPT_,comdat
	.align 4
	.weak	_ZN9__gnu_cxx13new_allocatorI6sphereE7destroyIS1_EEvPT_
	.type	_ZN9__gnu_cxx13new_allocatorI6sphereE7destroyIS1_EEvPT_, @function
_ZN9__gnu_cxx13new_allocatorI6sphereE7destroyIS1_EEvPT_:
	.PROC
	.CALLINFO FRAME=64,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4764:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	ldo 0(%r3),%r28
	ldo -36(%r28),%r28
	stw %r26,0(%r28)
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	stw %r25,0(%r28)
	ldo 0(%r3),%r28
	ldo -40(%r28),%r28
	ldw 0(%r28),%r26
	bl _ZN6sphereD1Ev,%r2
	nop
	nop
	ldw -20(%r3),%r2
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4764:
	.size	_ZN9__gnu_cxx13new_allocatorI6sphereE7destroyIS1_EEvPT_, .-_ZN9__gnu_cxx13new_allocatorI6sphereE7destroyIS1_EEvPT_
	.weak	_ZTISt19_Sp_make_shared_tag
	.section	.rodata._ZTISt19_Sp_make_shared_tag,"aG",@progbits,_ZTISt19_Sp_make_shared_tag,comdat
	.align 4
	.type	_ZTISt19_Sp_make_shared_tag, @object
	.size	_ZTISt19_Sp_make_shared_tag, 8
_ZTISt19_Sp_make_shared_tag:
	.word	_ZTVN10__cxxabiv117__class_type_infoE+8
	.word	_ZTSSt19_Sp_make_shared_tag
	.weak	_ZTSSt19_Sp_make_shared_tag
	.section	.rodata._ZTSSt19_Sp_make_shared_tag,"aG",@progbits,_ZTSSt19_Sp_make_shared_tag,comdat
	.align 4
	.type	_ZTSSt19_Sp_make_shared_tag, @object
	.size	_ZTSSt19_Sp_make_shared_tag, 24
_ZTSSt19_Sp_make_shared_tag:
	.stringz	"St19_Sp_make_shared_tag"
	.weak	_ZTISt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE
	.section	.rodata._ZTISt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE,"aG",@progbits,_ZTISt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align 4
	.type	_ZTISt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTISt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE, 8
_ZTISt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE:
	.word	_ZTVN10__cxxabiv117__class_type_infoE+8
	.word	_ZTSSt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE
	.weak	_ZTSSt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE
	.section	.rodata._ZTSSt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE,"aG",@progbits,_ZTSSt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align 4
	.type	_ZTSSt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTSSt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE, 47
_ZTSSt11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE:
	.stringz	"St11_Mutex_baseILN9__gnu_cxx12_Lock_policyE2EE"
	.text
	.align 4
	.type	_GLOBAL__sub_I_main, @function
_GLOBAL__sub_I_main:
	.PROC
	.CALLINFO FRAME=64,CALLS,SAVE_RP,SAVE_SP,ENTRY_GR=3
	.ENTRY
.LFB4768:
	.cfi_startproc
	stw %r2,-20(%r30)
	copy %r3,%r1
	.cfi_offset 2, -20
	.cfi_register 3, 1
	copy %r30,%r3
	.cfi_def_cfa_register 3
	stwm %r1,64(%r30)
	.cfi_offset 3, 0
	zdepi -1,31,16,%r25
	ldi 1,%r26
	bl _Z41__static_initialization_and_destruction_0ii,%r2
	nop
	ldw -20(%r3),%r2
	ldo 64(%r3),%r30
	ldwm -64(%r30),%r3
	bv,n %r0(%r2)
	.EXIT
	.PROCEND
	.cfi_endproc
.LFE4768:
	.size	_GLOBAL__sub_I_main, .-_GLOBAL__sub_I_main
	.section	.ctors,"aw",@progbits
	.align 4
	.word	P%_GLOBAL__sub_I_main
	.weakref	_ZL28__gthrw___pthread_key_createPjPFvPvE,__pthread_key_create
	.hidden	DW.ref.__gxx_personality_v0
	.weak	DW.ref.__gxx_personality_v0
	.section	.data.DW.ref.__gxx_personality_v0,"awG",@progbits,DW.ref.__gxx_personality_v0,comdat
	.align 4
	.type	DW.ref.__gxx_personality_v0, @object
	.size	DW.ref.__gxx_personality_v0, 4
DW.ref.__gxx_personality_v0:
	.word	P%__gxx_personality_v0
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.4.0-1ubuntu1~20.04) 9.4.0"
