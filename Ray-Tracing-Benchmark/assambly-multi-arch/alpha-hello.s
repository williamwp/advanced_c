	.set noreorder
	.set volatile
	.set noat
	.set nomacro
	.text
	.section	.sdata,"aws"
	.type	_ZStL19piecewise_construct, @object
	.size	_ZStL19piecewise_construct, 1
_ZStL19piecewise_construct:
	.zero	1
	.section	.sbss,"aw"
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata
$LC0:
	.string	"hello \n "
	.text
	.align 2
	.globl main
	.ent main
main:
	.eflag 48
	.frame $15,16,$26,0
	.mask 0x4008000,-16
$LFB1523:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!1
	lda $29,0($29)		!gpdisp!1
$main..ng:
	lda $30,-16($30)
	.cfi_def_cfa_offset 16
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -16
	.cfi_offset 15, -8
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	ldah $1,$LC0($29)		!gprelhigh
	lda $16,$LC0($1)		!gprellow
	ldq $27,printf($29)		!literal!2
	jsr $26,($27),0		!lituse_jsr!2
	ldah $29,0($26)		!gpdisp!3
	lda $29,0($29)		!gpdisp!3
	mov $31,$1
	mov $1,$0
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,16($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE1523:
	.end main
	.align 2
	.ent _Z41__static_initialization_and_destruction_0ii
_Z41__static_initialization_and_destruction_0ii:
	.eflag 48
	.frame $15,32,$26,0
	.mask 0x4008000,-32
$LFB2004:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!4
	lda $29,0($29)		!gpdisp!4
$_Z41__static_initialization_and_destruction_0ii..ng:
	lda $30,-32($30)
	.cfi_def_cfa_offset 32
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -32
	.cfi_offset 15, -24
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	mov $16,$1
	mov $17,$2
	stl $1,16($15)
	bis $31,$2,$1
	stl $1,20($15)
	ldl $1,16($15)
	cmpeq $1,1,$1
	beq $1,$L5
	ldl $2,20($15)
	ldah $1,1($31)
	lda $1,-1($1)
	cmpeq $2,$1,$1
	beq $1,$L5
	ldah $1,_ZStL8__ioinit($29)		!gprelhigh
	lda $16,_ZStL8__ioinit($1)		!gprellow
	ldq $27,_ZNSt8ios_base4InitC1Ev($29)		!literal!5
	jsr $26,($27),_ZNSt8ios_base4InitC1Ev		!lituse_jsr!5
	ldah $29,0($26)		!gpdisp!6
	lda $29,0($29)		!gpdisp!6
	ldah $1,__dso_handle($29)		!gprelhigh
	lda $18,__dso_handle($1)		!gprellow
	ldah $1,_ZStL8__ioinit($29)		!gprelhigh
	lda $17,_ZStL8__ioinit($1)		!gprellow
	ldq $16,_ZNSt8ios_base4InitD1Ev($29)		!literal
	ldq $27,__cxa_atexit($29)		!literal!7
	jsr $26,($27),0		!lituse_jsr!7
	ldah $29,0($26)		!gpdisp!8
	lda $29,0($29)		!gpdisp!8
$L5:
	bis $31,$31,$31
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,32($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE2004:
	.end _Z41__static_initialization_and_destruction_0ii
	.align 2
	.ent _GLOBAL__sub_I_main
_GLOBAL__sub_I_main:
	.eflag 48
	.frame $15,16,$26,0
	.mask 0x4008000,-16
$LFB2005:
	.cfi_startproc
	ldah $29,0($27)		!gpdisp!9
	lda $29,0($29)		!gpdisp!9
$_GLOBAL__sub_I_main..ng:
	lda $30,-16($30)
	.cfi_def_cfa_offset 16
	stq $26,0($30)
	stq $15,8($30)
	.cfi_offset 26, -16
	.cfi_offset 15, -8
	mov $30,$15
	.cfi_def_cfa_register 15
	.prologue 1
	ldah $17,1($31)
	lda $17,-1($17)
	lda $16,1($31)
	ldq $27,_Z41__static_initialization_and_destruction_0ii($29)		!literal!10
	jsr $26,($27),_Z41__static_initialization_and_destruction_0ii		!lituse_jsr!10
	ldah $29,0($26)		!gpdisp!11
	lda $29,0($29)		!gpdisp!11
	mov $15,$30
	ldq $26,0($30)
	ldq $15,8($30)
	lda $30,16($30)
	.cfi_restore 15
	.cfi_restore 26
	.cfi_def_cfa 30, 0
	ret $31,($26),1
	.cfi_endproc
$LFE2005:
	.end _GLOBAL__sub_I_main
	.section	.ctors,"aw",@progbits
	.align 3
	.quad	_GLOBAL__sub_I_main
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.4.0-1ubuntu1~20.04) 9.4.0"
	.section	.note.GNU-stack,"",@progbits
