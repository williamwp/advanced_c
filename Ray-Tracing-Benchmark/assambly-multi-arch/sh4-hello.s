	.file	"hello.cpp"
	.text
	.little
	.text
	.section	.rodata
	.type	_ZStL19piecewise_construct, @object
	.size	_ZStL19piecewise_construct, 1
_ZStL19piecewise_construct:
	.zero	1
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.align 2
.LC0:
	.string	"hello \n "
	.text
	.align 1
	.global	main
	.type	main, @function
main:
.LFB1518:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 17, -8
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov.l	.L3,r1
	mov	r1,r4
	mov.l	.L4,r1
	jsr	@r1
	nop
	mov	#0,r1
	mov	r1,r0
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
.L5:
	.align 2
.L3:
	.long	.LC0
.L4:
	.long	printf
	.cfi_endproc
.LFE1518:
	.size	main, .-main
	.align 1
	.type	_Z41__static_initialization_and_destruction_0ii, @function
_Z41__static_initialization_and_destruction_0ii:
.LFB1999:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 17, -8
	add	#-8,r15
	.cfi_def_cfa_offset 16
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov	r14,r1
	add	#-56,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-56,r1
	mov.l	r5,@(56,r1)
	mov	r14,r1
	add	#-56,r1
	mov.l	@(60,r1),r2
	mov	#1,r1
	cmp/eq	r1,r2
	bf	.L8
	mov	r14,r1
	add	#-56,r1
	mov.l	@(56,r1),r2
	mov.l	.L9,r1
	cmp/eq	r1,r2
	bf	.L8
	mov.l	.L13,r1
	mov	r1,r4
	mov.l	.L11,r1
	jsr	@r1
	nop
	mov.l	.L12,r3
	mov.l	.L13,r2
	mov.l	.L14,r1
	mov	r3,r6
	mov	r2,r5
	mov	r1,r4
	mov.l	.L15,r1
	jsr	@r1
	nop
.L8:
	nop
	add	#8,r14
	.cfi_def_cfa_offset 8
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
.L16:
	.align 2
.L9:
	.long	65535
.L13:
	.long	_ZStL8__ioinit
.L11:
	.long	_ZNSt8ios_base4InitC1Ev
.L12:
	.long	__dso_handle
.L14:
	.long	_ZNSt8ios_base4InitD1Ev
.L15:
	.long	__cxa_atexit
	.cfi_endproc
.LFE1999:
	.size	_Z41__static_initialization_and_destruction_0ii, .-_Z41__static_initialization_and_destruction_0ii
	.align 1
	.type	_GLOBAL__sub_I_main, @function
_GLOBAL__sub_I_main:
.LFB2000:
	.cfi_startproc
	mov.l	r14,@-r15
	.cfi_def_cfa_offset 4
	.cfi_offset 14, -4
	sts.l	pr,@-r15
	.cfi_def_cfa_offset 8
	.cfi_offset 17, -8
	mov	r15,r14
	.cfi_def_cfa_register 14
	mov.l	.L18,r1
	mov	r1,r5
	mov	#1,r4
	mov.l	.L19,r1
	jsr	@r1
	nop
	mov	r14,r15
	.cfi_def_cfa_register 15
	lds.l	@r15+,pr
	.cfi_restore 17
	.cfi_def_cfa_offset 4
	mov.l	@r15+,r14
	.cfi_restore 14
	.cfi_def_cfa_offset 0
	rts	
	nop
.L20:
	.align 2
.L18:
	.long	65535
.L19:
	.long	_Z41__static_initialization_and_destruction_0ii
	.cfi_endproc
.LFE2000:
	.size	_GLOBAL__sub_I_main, .-_GLOBAL__sub_I_main
	.section	.ctors,"aw",@progbits
	.align 2
	.long	_GLOBAL__sub_I_main
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.4.0-1ubuntu1~20.04) 9.4.0"
	.section	.note.GNU-stack,"",@progbits
