	.file	"hello.cpp"
	.machine ppc
	.section	".text"
	.section	.rodata
	.type	_ZStL19piecewise_construct, @object
	.size	_ZStL19piecewise_construct, 1
_ZStL19piecewise_construct:
	.zero	1
	.lcomm	_ZStL8__ioinit,1,1
	.type	_ZStL8__ioinit, @object
	.align 2
.LC0:
	.string	"hello \n "
	.section	".text"
	.align 2
	.globl main
	.type	main, @function
main:
.LFB1520:
	.cfi_startproc
	stwu 1,-32(1)
	.cfi_def_cfa_offset 32
	mflr 0
	stw 0,36(1)
	stw 31,28(1)
	.cfi_offset 65, 4
	.cfi_offset 31, -4
	mr 31,1
	.cfi_def_cfa_register 31
	lis 9,.LC0@ha
	la 3,.LC0@l(9)
	crxor 6,6,6
	bl printf
	li 9,0
	mr 3,9
	addi 11,31,32
	lwz 0,4(11)
	mtlr 0
	lwz 31,-4(11)
	.cfi_def_cfa 11, 0
	mr 1,11
	.cfi_restore 31
	.cfi_def_cfa_register 1
	blr
	.cfi_endproc
.LFE1520:
	.size	main,.-main
	.align 2
	.type	_Z41__static_initialization_and_destruction_0ii, @function
_Z41__static_initialization_and_destruction_0ii:
.LFB2001:
	.cfi_startproc
	stwu 1,-32(1)
	.cfi_def_cfa_offset 32
	mflr 0
	stw 0,36(1)
	stw 31,28(1)
	.cfi_offset 65, 4
	.cfi_offset 31, -4
	mr 31,1
	.cfi_def_cfa_register 31
	stw 3,12(31)
	stw 4,8(31)
	lwz 9,12(31)
	cmpwi 0,9,1
	bne 0,.L5
	lwz 10,8(31)
	li 9,0
	ori 9,9,0xffff
	cmpw 0,10,9
	bne 0,.L5
	lis 9,_ZStL8__ioinit@ha
	la 3,_ZStL8__ioinit@l(9)
	bl _ZNSt8ios_base4InitC1Ev
	lis 9,__dso_handle@ha
	la 5,__dso_handle@l(9)
	lis 9,_ZStL8__ioinit@ha
	la 4,_ZStL8__ioinit@l(9)
	lis 9,_ZNSt8ios_base4InitD1Ev@ha
	la 3,_ZNSt8ios_base4InitD1Ev@l(9)
	bl __cxa_atexit
.L5:
	nop
	addi 11,31,32
	lwz 0,4(11)
	mtlr 0
	lwz 31,-4(11)
	.cfi_def_cfa 11, 0
	mr 1,11
	.cfi_restore 31
	.cfi_def_cfa_register 1
	blr
	.cfi_endproc
.LFE2001:
	.size	_Z41__static_initialization_and_destruction_0ii,.-_Z41__static_initialization_and_destruction_0ii
	.align 2
	.type	_GLOBAL__sub_I_main, @function
_GLOBAL__sub_I_main:
.LFB2002:
	.cfi_startproc
	stwu 1,-32(1)
	.cfi_def_cfa_offset 32
	mflr 0
	stw 0,36(1)
	stw 31,28(1)
	.cfi_offset 65, 4
	.cfi_offset 31, -4
	mr 31,1
	.cfi_def_cfa_register 31
	li 9,0
	ori 4,9,0xffff
	li 3,1
	bl _Z41__static_initialization_and_destruction_0ii
	addi 11,31,32
	lwz 0,4(11)
	mtlr 0
	lwz 31,-4(11)
	.cfi_def_cfa 11, 0
	mr 1,11
	.cfi_restore 31
	.cfi_def_cfa_register 1
	blr
	.cfi_endproc
.LFE2002:
	.size	_GLOBAL__sub_I_main,.-_GLOBAL__sub_I_main
	.section	.ctors,"aw",@progbits
	.align 2
	.long	_GLOBAL__sub_I_main
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.4.0-1ubuntu1~20.04.2) 9.4.0"
	.section	.note.GNU-stack,"",@progbits
