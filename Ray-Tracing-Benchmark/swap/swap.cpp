#include <iostream>

int main() {
    int a, b, temp;

    // 输入两个数
    std::cout << "请输入第一个数: ";
    std::cin >> a;
    std::cout << "请输入第二个数: ";
    std::cin >> b;

    // 输出交换前的值
    std::cout << "交换前: a = " << a << ", b = " << b << std::endl;

    // 使用临时变量交换两个数的值
    temp = a;
    a = b;
    b = temp;

    // 输出交换后的值
    std::cout << "交换后: a = " << a << ", b = " << b << std::endl;

    return 0;
}
