#pragma once
// prevent from containing two many *.h files, otherwise low efficiency

#include <stdio.h>

int global_val = 100;

static void show()
{
    printf("show() function ");

}

void show_helper()
{
    show();    

}
