#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include "test1.h"

extern int global_val;
extern void show();
extern void show_helper();
// extern is statement

int main()
{
        
        register int pass = 100;
	printf("register int pass =  %d\n", pass);
	// the condition of using the variable pass is that pass has been used frequently.  
        // NOTE: the pass variable defined by register has no address
	// register int pass =  627071844 if using "printf("register int pass =  %d\n", &pass);   "
        int a =10;
	printf("int a = %d\n", a);
	printf(" the address of variable a -- &a = %p\n", &a);
        
	
	show();
	printf("\n");
	show_helper();

	printf("test 1\n");
	
	if(0) // annotation
	{
	    printf("if(0) \n");
	}
	if(1)
	{
	    printf("if(1) \n");
	}
	

	//
	bool bool_val = false;
	printf("the size of bool_val variable is %ld\n", sizeof(bool_val));

	goto E;
        sleep(3);  //unistd.h includes sleep() function
	printf("sleep() 1 \n");

        sleep(3);  //unistd.h includes sleep() function
        printf("sleep() 2 \n");
E:
	sleep(3);  //unistd.h includes sleep() function
        printf("sleep() 3 \n");

        // execute countless times 
//E1:
        printf(" execute countless times using goto keyword \n");	
//        goto E1;

	printf("the size of void is %ld\n", sizeof(void));
	int system(const char *command);
	system("./a.sh");
	printf("NULL applications  I have\n\n");
	return 0;



}
