#include <stdio.h>

int main()
{
	//int a = 10;
	// when int a = 10; the changing of pointer *p is right. 
	const int a = 10;
	int *p = (int*)&a;

	printf("before changing the value of *p, a = %d\n", a);

	*p = 30;

	printf("after  changing the value of *p, a = %d\n", a);


	// the const string example
	char* p1 = (char*)"h";
	printf("before, p1 is %d\n", *p1);
	
//	*p1 = 'H';
//      printf("after, p1 is %d\n", *p1);
        
        const int a1 = 100;
        int arr[a1];
        printf("arr[a1] \n");	
        for(int i = 0; i < a1; i++)
	{
	     arr[i] = i;
	}
	printf(" arr[11] = %d\n", arr[11]);


	return 0;

}
